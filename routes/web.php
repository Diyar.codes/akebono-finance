<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['guest'])->group(function () {
    // OTENTIKASI START ROUTE

    Route::get('/login', 'Auth\AuthController@login')->name('login');
    Route::post('/doLogin', 'Auth\AuthController@doLogin')->name('doLogin');

    // OTENTIKASI END ROUTE

    // Not Login / Automatic login START ROUTE

    Route::get('/qpr-approval/{no}/{level}', 'QprController@approval');
    Route::post('/qpr-approval', 'QprController@approvalSend');
    Route::get('/approval-bast', 'BastController@approval')->name('approval-bast'); //supplier
    Route::get('/reject-bast', 'BastController@reject')->name('reject-bast'); //supplier
    Route::get('/bast-approval/{id}/{step}', 'BastController@bast_approval')->name('bast-approval'); //supplier
    Route::get('/print-completed-bast/{id}/{lpb}', 'BastController@print')->name('print-complete-bast'); //supplier
    Route::get('/intrans-approve-change', 'IntransChange@approveAddChange');

    // Not Login / Auto login START ROUTE
    Route::get('/intrans-approve-change/{username}/{transaksi_id}', 'IntransChange@approveAddChange');
    Route::post('/intrans-approve-approve', 'IntransChange@approveAddChangeSave')->name('intrans-approve-change');
    Route::post('/intrans-approve-reject', 'IntransChange@rejectAddChangeSave')->name('intrans-reject-change');
});

Route::middleware(['auth:pub_login', 'web'])->group(function () {
    // MY PROFILE START ROUTE

    Route::get('/edit-profile', 'ProfileController@editProfile')->name('edit-profile');
    Route::put('/update-profile', 'ProfileController@updateProfile')->name('update-profile');
    Route::get('/edit-password', 'ProfileController@editPassword')->name('edit-password');
    Route::put('/update-password', 'ProfileController@updatePassword')->name('update-password');

    // MY PROFILE END ROUTE

    // OTENTIKASI START ROUTE

    Route::get('/logout', 'Auth\AuthController@doLogout')->name('logout');

    // OTENTIKASI END ROUTE

    // 404 START ROUTE

    Route::get('/not-found', 'StatusController@notFound')->name('not-found');

    // 404 END ROUTE

    Route::middleware([])->group(function () {
        // Dashboard START ROUTE

        Route::get('/', 'DashboardController@index')->name('dashboard');

        // Dashboard END ROUTE

        // Message START ROUTE

        // Route::get('create-message', 'MessageController@index')->name('create-message');
        // Route::get('inbox-message', 'MessageController@inboxMessage')->name('inbox-message');

        // Message END ROUTE

        // GET SOAP START ROUTE

        Route::get('/get-supplier', 'getSOAPController@getSupplier')->name('get-supplier'); //SOAP Datatable Get Supplier
        Route::get('/get-supplier-local', 'getSOAPController@getSupplierLocal')->name('get-supplier-local'); //SOAP Datatable Get Supplier Local
        Route::get('/get-pt-mstr-local', 'getSOAPController@getSoapPtMstrLocal')->name('get-pt-mstr-local'); //SOAP Datatable Get pt mstr local

        // GET SOAP END ROUTE

        //WebService START ROUTE

        Route::get('/get-po-mstr', 'GetDataWebServiceController@getDataPO')->name('get-po-mstr');
        Route::get('/get-po-detail', 'GetDataWebServiceController@getPODetail')->name('get-po-detail');
        Route::get('/get-pt-mstr', 'GetDataWebServiceController@getDataPtMstr')->name('get-pt-mstr');
        Route::get('/get-pt-mstr-desc', 'GetDataWebServiceController@getPtMstrByDesc')->name('get-pt-mstr-desc');
        Route::get('/get-code-um', 'GetDataWebServiceController@getCodeUm')->name('get-code-um');

        //Web Service END ROUTE

        //DELIVERY Note START ROUTE

        //-> item kanban
        Route::resource('/item-kanbans', 'ItemKanbanController'); //whs
        Route::get('/search-item-kanban', 'ItemKanbanController@searchItemKanban')->name('search-table-item-dn'); //whs

        //-> master cycles
        Route::resource('/master-cycles', 'MasterCycleController'); //whs
        Route::get('/data-second-table-master-cycle', 'MasterCycleController@seconddatamastercycle')->name('data-second-table-master-cycle'); //whs

        //->delivery note create
        Route::resource('/delivery-notes', 'DeliveryNoteController')->parameters(['delivery-notes' => 'delivery-notes:dn_tr_id',]); //whs
        Route::get('/delivery-notes-cetak_pdf', 'DeliveryNoteController@cetak_pdf')->name('cetak_pdf'); //whs
        Route::get('/modal-delivery-notes', 'DeliveryNoteController@modaldeliverynotes')->name('modal-delivery-notes');
        Route::get('search-data-po-lookup', 'DeliveryNoteController@searchdatalookup')->name('search-data-po-lookup');
        Route::post('/store-delivery-notes', 'DeliveryNoteController@storadeliverynote')->name('store-delivery-notes');
        Route::get('/check-cycle', 'DeliveryNoteController@checkcycle')->name('check-cycle');
        Route::get('/data-second-table-dn', 'DeliveryNoteController@seconddatadeliverynote')->name('data-second-table-dn');

        //->delivery note edit
        Route::get('/search-data-dn', 'DeliveryNoteController@searchdataDN')->name('search-data-dn');
        Route::get('/edit-delivery-notes/{id}', 'DeliveryNoteController@edit')->name('edit-delivery-notes');
        Route::get('update-dn', 'DeliveryNoteController@update_dn')->name('update-dn');

        //->delivery note delete
        Route::get('/delete-delivery-notes', 'DeliveryNoteController@delete')->name('delete-delivery-notes');

        //->print delivery-note
        Route::resource('/report-delivery-note', 'PrintDeliveryNoteController'); //whs
        Route::get('cetak-dn', 'PrintDeliveryNoteController@cetak_langsung')->name('cetak-dn');
        Route::post('/get-dn', 'PrintDeliveryNoteController@index'); #belum di cek karena menggunakan method post

        //->list delivery note
        Route::get('/list-delivery-notes', 'PrintDeliveryNoteController@list_dn')->name('list-delivery-notes'); //whs
        Route::get('export-list-dn', 'PrintDeliveryNoteController@export_list_dn')->name('export-list-dn');
        Route::post('export-list-dn', 'PrintDeliveryNoteController@export_list_dn')->name('export-list-dn-store');

        //->summary delivery note
        Route::post('export-summary-dn', 'PrintDeliveryNoteController@export_summary_dn')->name('export.summary-dn');
        Route::get('/summary-delivery-notes', 'PrintDeliveryNoteController@summary_dn')->name('delivery-note-summary'); //whs
        Route::get('/search-summary-delivery_notes', 'PrintDeliveryNoteController@searchSummaryDn');

        //->dn vs packing slip
        Route::get('/dn-vs-packingslip', 'PrintDeliveryNoteController@dn_vs_pack')->name('dn-vs-packingslip'); //whs
        Route::post('/get-packing', 'PrintDeliveryNoteController@dn_vs_pack');
        Route::get('/detail-view-dn/{id}', 'PrintDeliveryNoteController@detail_view_dn')->name('detail-view-dn');

        //DELIVERY NOTE END ROUTE

        //DELIVERY ORDER / PACKING SLIP BARCODE START ROUTE

        //->delivery order by PO
        Route::get('/delivery-order-po', 'PackingSlipController@deliveryOrderPO')->name('delivery-order-po'); //supplier
        Route::get('/search-delivery-order-po', 'PackingSlipController@searchDeliveryOrderPO')->name('search-delivery-order-po'); //supplier
        Route::get('/ship-delivery-order-po/{id}', 'PackingSlipController@shipDeliveryOrderPO')->name('ship-delivery-order-po');
        Route::get('/generate-ship-do-po', 'PackingSlipController@generate_ship_order')->name('generate-ship-do-po');
        Route::get('/edit-delivery-order-po', 'PackingSlipController@editDeliveryOrderPO')->name('edit-delivery-order-po'); //supplier
        Route::get('/insert-delivery-order-po', 'PackingSlipController@insert_deliveryorder_po')->name('insert-delivery-order-po'); //supplier
        Route::get('/edit-act-delivery-order-po', 'PackingSlipController@edit_deliveryorder_po')->name('edit-act-delivery-order-po'); //supplier
        Route::get('/delete-kosong-delivery-order-po', 'PackingSlipController@delete_item_deliveryOrder_po')->name('delete-kosong-delivery-order-po'); //supplier
        Route::get('/delete-transaction-delivery-order-po', 'PackingSlipController@delete_transaction_do_po')->name('delete-transaction-delivery-order-po'); //supplier
        Route::get('/check-sheet/{po}/{item}/{trans}/{line}', 'PackingSlipController@checkSheet')->name('check-sheet'); //supplier
        Route::get('/delivery-order-po-edit/{id}', 'PackingSlipController@deliveryOrderPOEdit')->name('delivery-order-po-edit'); //supplier
        Route::get('/search-edit-delivery-order-po', 'PackingSlipController@searchEditDeliveryOrderPO')->name('search-edit-delivery-order-po'); //supplier
        //->delivery order by DN
        Route::get('/create-delivery-order-dn', 'PackingSlipController@packingSlipDN')->name('create-delivery-order-dn'); //supplier
        Route::get('/insert-delivery-order-dn', 'PackingSlipController@insert_do_dn')->name('insert-delivery-order-dn'); //supplier
        Route::get('/delivery-order-dn/{dn}/{po}', 'PackingSlipController@editpackingslip')->name('delivery-order-dn'); //noted
        Route::get('/edit-delivery-order-dn', 'PackingSlipController@editDeliveryOrderDN')->name('edit-delivery-order-dn'); //supplier
        Route::get('/delivery-order-dn-edit/{id}', 'PackingSlipController@deliveryOrderDNEdit')->name('delivery-order-dn-edit'); //supplier
        Route::get('/edit-act-delivery-order-dn', 'PackingSlipController@edit_deliveryorder_dn')->name('edit-act-delivery-order-dn'); //supplier
        Route::get('/delete-transaction-delivery-order-dn', 'PackingSlipController@delete_transaction_do_dn')->name('delete-transaction-delivery-order-dn'); //supplier
        Route::get('/search-delivery-order-dn', 'PackingSlipController@search_do_dn')->name('search-delivery-order-dn'); //supplier
        //->view outstanding PO
        Route::get('/act-print-po', 'PackingSlipController@act_print_po')->name('act-print-po'); //supplier
        Route::get('/view-outstanding-po', 'viewOutstandingPoController@index')->name('view-outstanding-po'); //supplier
        Route::get('/detail-view-outstanding-po/{po}/{kd}/{nm}', 'viewOutstandingPoController@detail')->name('detail-view-outstanding-po'); //supplier
        Route::get('/view-detail-packing-slip/{id}', 'PackingSlipController@vps_detail')->name('view-detail-packing-slip');
        Route::get('/search-outstanding-po', 'viewOutstandingPoController@search')->name('search-outstanding-po'); //supplier
        //->view receiptt
        Route::get('/view-receipt', 'ViewReceiptController@index')->name('view-receipt'); //supplier
        Route::get('/search-view-receipt', 'ViewReceiptController@search')->name('search-view-receipt'); //supplier

        //->achivement supplier
        Route::get('/achivement-delivery-supplier', 'AchivementDeliverySupplierController@index')->name('achivement-delivery-supplier'); //supplier
        Route::get('/generate-achievement', 'AchivementDeliverySupplierController@generate')->name('generate-achievement'); //supplier

        //Print Report
        Route::get('/packingslip-barcode', 'PackingSlipBarcodeController@index')->name('packing-slip-barcode'); //supplier
        Route::get('/print-barcode/{id}', 'PackingSlipBarcodeController@printbarcode')->name('print-barcode'); //supplier
        Route::get('/packing-slip-barcode-view/{id}', 'PackingSlipBarcodeController@barcodeview')->name('packing-slip-barcode-view'); //supplier

        //->print packing slip barcode
        Route::get('/print-packingslip-barcode', 'PackingSlipController@print_packingslip_barcode')->name('print-packingslip-barcode'); //supplier
        Route::get('/print-ps-supplier/{id}', 'PackingSlipController@printpssupp')->name('print-ps-supplier'); //supplier
        Route::get('/search-ps-barcode', 'PackingSlipController@search_pp')->name('search-ps-barcode'); //supplier
        Route::get('/cetak-ps-direct', 'PackingSlipController@print_direct_psb')->name('cetak-ps-direct'); //supplier
        Route::get('/cetak-ps-from-to/{from}/{to}', 'PackingSlipController@cetak_ps_from_to')->name('cetak-ps-from-to'); //
        //end packing slip supplier

        //->print lpb
        Route::get('/print-lpb', 'LpbController@print_lpb')->name('print-lpb'); //supplier
        Route::get('/search-lpb', 'LpbController@search_lpb')->name('search-lpb'); //supplier
        Route::get('/direct-print-lpb', 'LpbController@direct')->name('direct-print-lpb'); //supplier
        Route::get('/print-elpb/{po}/{ps}', 'LpbController@print_lpb2')->name('print-elpb'); //supplier
        Route::get('/preview-elpb/{po}/{ps}', 'LpbController@preview_lpb2')->name('preview-elpb'); //supplier

        //->approval lpb
        Route::get('/app-lpb/{supp}/{date}', 'LpbController@app_lpb')->name('app-lpb'); //supplier

        //->print po
        Route::get('/print-po', 'PackingSlipController@print_po')->name('print-po'); //supplier

        //->vpr
        Route::get('/vendor-performance-report', 'vendorPerformanceReport@index')->name('vendor-performance-report'); //whs

        //->Approval LPB
        Route::get('/approval-elpb', 'LpbController@index')->name('approval-elpb'); //whs
        Route::post('/approval-elpb/supplier', 'LpbController@getSupplier')->name('lpb.get-supplier'); //whs
        Route::post('/approval-elpb/table', 'LpbController@table')->name('lpb.table'); //whs

        //DELIVERY ORDER / PACKING SLIP BARCODE END ROUTE

        // QPR START ROUTE

        //->all list qpr
        Route::get('all-list-qpr', 'QprController@allListQpr')->name('all-list-qpr'); //qc
        Route::post('excel-counter-measure', 'QprController@excelCounterMeasure')->name('excel-counter-measure');
        Route::get('all-list-qpr-verifikasi/{id}', 'QprController@allListQprVerifikasi')->name('all-list-qpr-verifikasi'); //qc
        Route::put('all-list-qpr-verifikasi-update/{id}', 'QprController@allListQPRVerifikasiUpdate')->name('all-list-qpr-verifikasi-update'); //qc
        Route::get('sortir-all-list-qpr/{id}', 'QprController@sortirAllListQpr')->name('sortir-all-list-qpr'); //qc
        Route::get('print-all-list-qpr/{id}', 'QprController@printAllListQpr')->name('print-all-list-qpr'); //qc
        Route::get('report-counter-measure/{id}', 'QprController@reportCounterMeasure')->name('report-counter-measure'); // qc
        Route::get('attach-document/{id}', 'QprController@attachDocument')->name('attach-document'); //qc

        //->surat claim
        Route::get('surat-claim-qpr', 'QprController@suratclaimqpr')->name('surat-claim-qpr'); //qc
        Route::get('search-surat-claim-qpr', 'QprController@searchSuratClaimQpr')->name('search-surat-claim-qpr'); //qc
        Route::post('store-surat-claim-qpr', 'QprController@storeSuratClaimQpr')->name('store-surat-claim-qpr'); //qc

        //->view sortir
        Route::get('view-sort', 'QprController@viewSortir')->name('view-sortir'); //qc

        //->create sortir
        Route::get('sortir', 'QprController@sortir')->name('sortir'); //qc
        Route::post('store-sortir', 'QprController@storeSortir')->name('store-sortir'); //qc

        // -> Receiving Input
        Route::get('qpr-receiving', 'QprController@qprreceiving')->name('qpr-receiving'); //qc
        Route::post('qpr-receiving-save', 'QprController@qprreceivingsave')->name('qpr-receiving-save'); //qc

        // -> cutomer view
        Route::get('qpr-customer-view', 'QprController@qprcustomer')->name('qpr-customer-view'); //qc

        // -> qpr release new
        Route::get('qpr-release-new', 'QprController@qprreleasenew')->name('qpr-release-new'); //qc
        Route::post('qpr-release-new-save', 'QprController@qprreleasenewsave')->name('qpr-release-new-save'); //qc

        // -> qpr qc proses barcode
        Route::get('qc-process-bc', 'QprController@qcprocessbc')->name('qc-process-bc'); //qc
        Route::get('detail-proccess-bc/{sj}', 'QprController@detailprocessBc')->name('detail-proccess-bc'); //qc

        //QPR list not tidied up
        Route::post('store-attach-document', 'QprController@storeAttachDocument')->name('store-attach-document'); //qc
        Route::get('qc-barcode', 'QprController@index')->name('qc-barcode'); //qc
        Route::post('store-qc-barcode', 'QprController@storeqcbarcode')->name('store_qc_barcode'); //qc
        Route::get('show-qc-barcode', 'QprController@showqrbarcode')->name('show.qc_barcode');
        Route::get('item-check-qc', 'QprController@itemcheckqc')->name('item-check-qc'); //qc
        Route::get('modal-add-item-check', 'QprController@modaladditemcheck')->name('modal-add-item-check'); //qc
        Route::get('search-data-item-check', 'QprController@searchDataItemCheck')->name('search-data-item-check'); //qc
        Route::get('store-item-check-qc', 'QprController@storeItemCheckQc')->name('store-item-check-qc'); //qc
        Route::post('store-add-item', 'QprController@storeAddItemQc')->name('store-add-item'); //qc
        Route::get('qpr-receiving', 'QprController@qprreceiving')->name('qpr-receiving'); //qc
        Route::post('qpr-receiving-save', 'QprController@qprreceivingsave')->name('qpr-receiving-save'); //qc
        Route::get('qpr-customer-view', 'QprController@qprcustomer')->name('qpr-customer-view'); //qc
        Route::get('qpr-release-new', 'QprController@qprreleasenew')->name('qpr-release-new'); //qc
        Route::post('qpr-release-new-save', 'QprController@qprreleasenewsave')->name('qpr-release-new-save'); //qc
        Route::get('print-qpr-r', 'QprController@qprprintr')->name('print-qpr-r'); //qc
        Route::get('print-qpr-l', 'QprController@qprprintl')->name('print-qpr-l'); //qc
        Route::get('search-process-bc', 'QprController@searchProcessBc')->name('search-process-bc'); //qc
        Route::get('add-item-check-qc', 'QprController@addItemCheck')->name('add-item-check-qc');
        Route::get('search-add-item-check-qc', 'QprController@cari_item_master')->name('search-add-item-check-qc');
        Route::get('search-counter-measure', 'QprController@searchCounterMeasure')->name('search-counter-measure');
        Route::get('countermeasure-action', 'QprController@counterMeasureAction')->name('countermeasure-action');
        Route::post('store-measure-action', 'QprController@storeCounterMeasureAction')->name('store-measure-action');
        Route::get('print-list-qpr-supplier', 'QprController@printListQpr')->name('print-list-qpr-supplier');
        Route::post('store-sortir-qpr', 'QprController@storeAlllistqpr')->name('store-sortir-qpr');
        Route::get('mail-reminder-vendor/', 'QprController@sentmailvendorreminder')->name('mail-reminder-vendor');
        Route::get('hapus-part', 'QprController@hapus_part')->name('hapus-part'); //qc
        //End QPR list not tidied up

        // QPR END ROUTE

        // Delivery Slip START ROUTE

        //->add
        Route::get('/cover-letter', 'CoverLetterSubcountController@suratPengantar')->name('cover-letter'); //whs
        Route::post('/store-cover-letter', 'CoverLetterSubcountController@storecoverletter')->name('store-cover-letter'); //whs

        //->view, print, update, reject
        Route::get('/print-delivery-slip', 'CoverLetterSubcountController@printDeliverySlip')->name('print-delivery-slip'); //whs
        Route::get('/view-delivery-slip/{id}', 'CoverLetterSubcountController@viewDs')->name('view-delivery-slip'); //whs
        Route::get('/view-delivery-slip-main/{id}', 'CoverLetterSubcountController@viewDsMain')->name('view-delivery-slip-main'); //whs
        Route::get('/sp-print/{id}', 'CoverLetterSubcountController@sp_print')->name('sp-print'); //whs
        Route::get('/edit-delivery-slip/{id}', 'CoverLetterSubcountController@editDs')->name('edit-delivery-slip'); //whs
        Route::get('/edit-delivery-slip-update-detail-main/{id}', 'CoverLetterSubcountController@editDsUpdtDtlMain')->name('edit-delivery-slip-update-detail-main'); //whs
        Route::delete('/delete-dtl-delivery-slip/{no}/{urutan}', 'CoverLetterSubcountController@deleteDtlDs')->name('delete-dtl-delivery-slip'); //whs
        Route::put('/update-delivery-slip/{id}', 'CoverLetterSubcountController@updateDs')->name('update-delivery-slip'); //whs
        Route::put('/reject-delivery-slip/{id}', 'CoverLetterSubcountController@rejectDs')->name('reject-delivery-slip');

        //->list detail
        Route::get('/report-delivery-slip', 'CoverLetterSubcountController@reportDs')->name('report-delivery-slip'); //whs
        Route::get('/search-report-delivery-slip', 'CoverLetterSubcountController@searchReportDeliverySlip')->name('search-report-delivery-slip'); //whs
        Route::post('/excel-report-delivery-slip', 'CoverLetterSubcountController@excelReportDeliverySlip')->name('excel-report-delivery-slip'); //whs

        //->miss pack
        Route::get('/list-miss-packing', 'CoverLetterSubcountController@listMissPacking')->name('list-miss-packing'); //whs
        Route::get('/view-list-miss-packing/{id}', 'CoverLetterSubcountController@showListMissPacking')->name('view-list-miss-packing'); //whs
        Route::get('/view-list-miss-packing-main/{id}', 'CoverLetterSubcountController@showListMissPackingMain')->name('view-list-miss-packing-main'); //whs
        Route::put('/update-list-miss-packing/{id}', 'CoverLetterSubcountController@updateListMissPacking')->name('update-list-miss-packing'); //whs

        //->item pending
        Route::get('delivery-item-pending', 'DeliveryItemPendingController@index')->name('delivery-item-pending'); //whs
        Route::post('delivery-item-pending-post', 'DeliveryItemPendingController@store')->name('delivery-item-pending-post'); //whs

        //->mutasi bulanan
        Route::get('/monthly-ds-report', 'CoverLetterSubcountController@monthlyDsReport')->name('monthly-ds-report'); //whs
        Route::get('/search-monthly-ds-report', 'CoverLetterSubcountController@searchMonthlyDsReport')->name('search-monthly-ds-report'); //whs
        Route::get('/search-monthly-ds-report-detail/{kode}/{moon}/{year}/{locto}/{app}', 'CoverLetterSubcountController@searchMonthlyDsReportDetail')->name('search-monthly-ds-report-detail'); //whs
        Route::get('/search-monthly-ds-report-detail-main/{kode}/{moon}/{year}/{locto}/{app}', 'CoverLetterSubcountController@searchMonthlyDsReportDetailMain')->name('search-monthly-ds-report-detail-main'); //whs
        Route::post('/excel-monthly-ds-report-detail', 'CoverLetterSubcountController@excelMonthlyDsReportDetail')->name('excel-monthly-ds-report-detail'); //whs

        //->confirm surat pengantar
        Route::get('/confirm-delivery-slip', 'CoverLetterSubcountController@confirmDeliverySlip')->name('confirm-delivery-slip'); //Supplier
        Route::get('/confirm-delivery-slip-view/{id}', 'CoverLetterSubcountController@confirmDeliverySlipView')->name('confirm-delivery-slip-view'); //Supplier
        Route::get('/confirm-delivery-slip-view-main/{id}', 'CoverLetterSubcountController@confirmDeliverySlipViewMain')->name('confirm-delivery-slip-view-main'); //Supplier
        Route::post('/excel-confirm-delivery-slip-view-main', 'CoverLetterSubcountController@excelConfirmDeliverySlip')->name('excel-confirm-delivery-slip-view-main'); //Supplier
        Route::put('/update-confirm-delivery-slip-view/{id}', 'CoverLetterSubcountController@updateConfirmDeliverySlipView')->name('update-confirm-delivery-slip-view'); //Supplier
        Route::get('/confirm-delivery-slip-view-confirmed/{id}', 'CoverLetterSubcountController@confirmDeliverySlipViewConfirmed')->name('confirm-delivery-slip-view-confirmed'); //Supplier
        Route::get('/confirm-delivery-slip-view-confirmed-main/{id}', 'CoverLetterSubcountController@confirmDeliverySlipViewConfirmedMain')->name('confirm-delivery-slip-view-confirmed-main'); //Supplier

        // Delivery Slip END ROUTE

        //BAST ROUTE

        Route::get('/report-bast', 'BastController@report')->name('report-bast'); //supplier
        Route::get('/list-bast', 'BastController@list')->name('list-bast'); //supplier
        Route::get('/delete-bast', 'BastController@delete')->name('delete-bast'); //supplier
        Route::get('/add-bast', 'BastController@index')->name('add-bast'); //supplier
        Route::get('/get-rincian-bast', 'BastController@rincian')->name('get-rincian-bast'); //supplier
        Route::get('/print-bast/{id}/{lpb}', 'BastController@print')->name('print-bast'); //supplier
        Route::get('/lampiran-bast/{id}/{lpb}', 'BastController@lampiran')->name('lampiran-bast'); //supplier
        Route::post('/save-bast-data', 'BastController@store')->name('save-bast-data'); //supplier
        Route::post('/preview-bast-data', 'BastController@preview_bast')->name('preview-bast-data'); //supplier

        //END BAST ROUTE

        // INSTRASH START ROUTE

        // -> Intrans Change
        Route::get('/change-intrans', 'IntransChange@index')->name('change-intrans'); //whs
        Route::get('/add-change-po', 'IntransChange@addChangePo')->name('add-change-po'); //whs
        Route::post('/save-change-po', 'IntransChange@saveChangePo')->name('save-change-po'); //whs

        // -> Intrans Transfer
        Route::get('/transfer-intrans-view', 'IntransTransfer@index')->name('transfer-intrans-view'); //whs
        Route::get('/transfer-intrans/{kdsupp}/{inv}', 'IntransTransfer@transferIntrans')->name('transfer-intrans'); //whs transfer_confirm_whs_intrans
        Route::get('/transfer-intrans-barang/{kdsupp}/{inv}', 'IntransTransfer@transferIntransBarang')->name('transfer-intrans-barang'); //whs transfer_confirm_whs_intrans
        Route::get('/transfer-intrans-whs/{kdsupp}/{inv}', 'IntransTransfer@transferIntransWhs')->name('transfer-intrans-whs'); //whs transfer_confirm_whs_intrans
        Route::post('/transfer-intrans-save', 'IntransTransfer@transferIntransSave')->name('transfer-intrans-save'); //whs transfer_confirm_whs_intrans
        Route::post('/transfer-intrans-barang-save', 'IntransTransfer@transferIntransBarangSave')->name('transfer-intrans-barang-save'); //whs transfer_confirm_whs_intrans
        Route::post('/transfer-intrans-whs-save', 'IntransTransfer@transferIntransWhsSave')->name('transfer-intrans-whs-save'); //whs transfer_confirm_whs_intrans
        Route::get('/transfer-intrans-print/{kdsupp}/{inv}', 'IntransTransfer@printelpb')->name('intrans-print-elpb'); //list print
        Route::get('/transfer-intrans-preview/{kdsup}/{rc}/{intpo}', 'IntransTransfer@preview')->name('intrans-print-preview'); //list print
        Route::get('/transfer-intrans-print-elpb/{kdsup}/{rc}/{intpo}', 'IntransTransfer@print')->name('intrans-print'); //list print
        Route::get('/transfer-intrans-detail/{kdsupp}/{inv}', 'IntransTransfer@detail')->name('transfer-intrans-detail'); //whs

        // -> Intrans Pending
        Route::get('/pending-intrans', 'IntransPending@index')->name('pending-intrans'); //whs
        Route::get('/pending-intrans-detail/{no_invoice}/{kode_supp}/{no_bl}', 'IntransPending@detail')->name('pending-intrans-detail'); //whs
        Route::post('/pending-intrans-detail-proses', 'IntransPending@detailproses')->name('pending-intrans-detail-proses'); //whs

        // -> Intrans View
        Route::get('/view-intrans', 'IntransView@index')->name('view-intrans'); //whs
        Route::get('/view-intrans-container', 'IntransView@searchContainer')->name('view-intrans-container'); //whs
        Route::get('/view-intrans-detail/{kdsupp}/{inv}', 'IntransView@detail')->name('view-intrans-detail'); //whs

        // INSTRASH END ROUTE

        // BARCODE WHS START ROUTE

        // Master Barcode WHS
        Route::get('master-barcode-whs', 'MasterBarcodeWhsController@index')->name('master-barcode-whs');
        Route::get('/data-master-barcode-whs', 'MasterBarcodeWhsController@datamasterbarcodewhs')->name('data-master-barcode-whs'); //whs
        Route::get('detail-barcode-whs/{pi}/{locfrom}/{locto}/{snp}/{comon}', 'MasterBarcodeWhsController@detailbarcodewhs')->name('detail-barcode-whs');
        Route::get('print-barcode-whs/{pi}/{locfrom}/{locto}/{snp}/{comon}/{type}/{urutan}/{qtysupp}', 'MasterBarcodeWhsController@printbarcodewhs')->name('print-barcode-whs');
        Route::get('/get-loc-master', 'MasterBarcodeWhsController@getlocmaster')->name('get-loc-master');
        Route::post('update-master-barcode-whs', 'MasterBarcodeWhsController@update')->name('update-master-barcode-whs');
        Route::post('update-detail-master-barcode-whs', 'MasterBarcodeWhsController@updatedetail')->name('update-detail-master-barcode-whs');
        Route::post('print-barcode-whs-show', 'MasterBarcodeWhsController@printbarcode')->name('print-barcode-whs-show');
        // Pending Barcode WHS
        Route::get('pending-barcode-whs', 'PendingBarcodeWhs@index')->name('pending-barcode-whs');
        Route::get('detail-pending-barcode-whs/{polibox_item_nbr}/{month}/{year}', 'PendingBarcodeWhs@detailpending')->name('detail-pending-barcode-whs');
        Route::get('data-pending-barcode-whs', 'PendingBarcodeWhs@datapendingbarcode')->name('data-pending-barcode-whs');
        // Rekap BARCODE WHS
        Route::get('rekap-barcode-whs', 'RekapBarcodeWhsController@index')->name('rekap-barcode-whs');
        Route::get('data-rekap-barcode-whs', 'RekapBarcodeWhsController@datarekap')->name('data-rekap-barcode-whs');
        // Master Small Part P4
        Route::get('/p4-master', 'PoliboxController@index')->name('polibox');
        Route::get('/p4-master/get', 'PoliboxController@get_polibox')->name('polibox.get');
        Route::post('/p4-master/post', 'PoliboxController@post_polibox')->name('polibox.post');
        // Rekap Barcode WHS detail
        Route::get('/rekap-barcode-detail', 'RekapBarcodeDetailController@index')->name('rekapbarcodedetail');
        Route::get('/rekap-barcode-detail/table', 'RekapBarcodeDetailController@table')->name('rekap.table');
        //->add barcode WHS
        Route::get('/add-barcode-whs', 'AddBarcodeWhsController@index')->name('addbarcode');
        Route::get('/add-barcode-whs/get', 'AddBarcodeWhsController@get_item')->name('addbarcode.get');
        Route::get('/add-barcode-whs/getl', 'AddBarcodeWhsController@get_loc')->name('addbarcode.getloc');
        Route::post('/add-barcode-whs', 'AddBarcodeWhsController@store')->name('addbarcode.store');

        //->liss bon
        Route::get('list-bon', 'MasterBarcodeWhsController@listbon')->name('list-bon');
        Route::get('autocomplete-list', 'MasterBarcodeWhsController@autocompleteList')->name('autocomplete-list');
        Route::get('search-list-bon', 'MasterBarcodeWhsController@searchlistbon')->name('search-list-bon');
        Route::post('excel-list-bon', 'MasterBarcodeWhsController@excellistbon')->name('excel-list-bon');
        Route::get('approval-bon-whs', 'MasterBarcodeWhsController@approvalbon')->name('approval-bon-whs');
        Route::get('print-approve-bon-whs', 'MasterBarcodeWhsController@printapprovebonwhs')->name('print-approve-bon-whs');
        Route::get('edit-approve-bon-whs', 'MasterBarcodeWhsController@editaprrovalbonwhs')->name('edit-approve-bon-whs');
        Route::post('update-approve-bon-whs', 'MasterBarcodeWhsController@updateapprovebonwhs')->name('update-approve-bon-whs');
        Route::get('complete-bon-whs', 'MasterBarcodeWhsController@completebon')->name('complete-bon-whs');
        Route::post('get-data-bon', 'MasterBarcodeWhsController@getdatabon')->name('get-data-bon');

        // BARCODE WHS END ROUTE

        Route::get('/search-ps', 'PackingSlipController@search_ps')->name('search-ps');
        Route::get('/view-packing-slip', 'PackingSlipController@index')->name('view-packing-slip');
        Route::get('/get-data-packingslip', 'PackingSlipController@search')->name('get-data-packingslip');
        Route::get('/print-delivery-notes/{id}', 'PrintDeliveryNoteController@report_dn_print')->name('print-delivery-notes');
        Route::get('/view-packing-slip-barcode', 'viewPackingSlipBarcodeController@index')->name('view-packing-slip-barcode');

        Route::get('list-qpr-supp', 'QprController@countermeasure')->name('counter-measure');

        Route::get('setting-close-open', 'MasterBarcodeWhsController@settingclose')->name('setting-close-open');
        Route::post('store-setting-closeopen', 'MasterBarcodeWhsController@storecloseopen')->name('store-setting-closeopen');
    });
});

// diyar2
