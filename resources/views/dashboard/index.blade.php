{{-- @extends('layouts.main') --}}
@extends('templates.main')

@section('title', 'Dashboard')

@section('body')
<!-- Page Header -->
<div class="page-header mb-lg-n4 mb-md-n3">
    <div>
        <h2 class="main-content-title tx-24 mg-b-5">@yield('title')</h2>
    </div>
</div>
<!-- End Page Header -->

<div class="row row-sm">
    <div class="col-sm-12 col-lg-12 col-xl-8">

    </div>
</div>
@endsection
