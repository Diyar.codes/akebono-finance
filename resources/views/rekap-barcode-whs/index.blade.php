@extends('templates.main')

@section('title', 'Rekap')

    
@section('styles')
    <style>
        table.dataTable thead th, table.dataTable thead td {
            border: 1px solid #fff;
        }   
    </style>
@endsection

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    Periode Transaction:
                                </div>
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" style="width:4rem" class="form-control-sm">
                                        @php
                                            $bln = date("m");
                                            $thn = date("Y");
                                        @endphp
                                        <option value="" selected disabled>Month</option>
                                        <option value="01" <?php if($bln=="01") { echo "selected"; } ?>>01</option>
                                        <option value="02" <?php if($bln=="02") { echo "selected"; } ?>>02</option>
                                        <option value="03" <?php if($bln=="03") { echo "selected"; } ?>>03</option>
                                        <option value="04" <?php if($bln=="04") { echo "selected"; } ?>>04</option>
                                        <option value="05" <?php if($bln=="05") { echo "selected"; } ?>>05</option>
                                        <option value="06" <?php if($bln=="06") { echo "selected"; } ?>>06</option>
                                        <option value="07" <?php if($bln=="07") { echo "selected"; } ?>>07</option>
                                        <option value="08" <?php if($bln=="08") { echo "selected"; } ?>>08</option>
                                        <option value="09" <?php if($bln=="09") { echo "selected"; } ?>>09</option>
                                        <option value="10" <?php if($bln=="10") { echo "selected"; } ?>>10</option>
                                        <option value="11" <?php if($bln=="11") { echo "selected"; } ?>>11</option>
                                        <option value="12" <?php if($bln=="12") { echo "selected"; } ?>>12</option>	
                                    </select>
                                    <select name="tahun" id="tahun" style="width:8rem" class="form-control-sm">
                                        <option value="" selected disabled>Year</option>
                                        <?php
                                        $year=date("Y");
                                        $awal=$year-4;
                                        for($awal=$year-4;$awal<=$year;$awal++)
                                        {
                                            echo '<option value='.$awal.' '.(($awal==$thn)?'selected="selected"':"").'>'.$awal.'</option> ';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    Item Number:
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control-sm" id="itemnumber" name="itemnumber">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-lg-2 mt-2 mt-md-0">
                                    <button type="button" class="btn btn-info btn-block mb-2" id="search"><i class="fas fa-search"></i> Search</button>
                                </div>
                                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-lg-2 mt-2 mt-md-0">
                                    <button type="button" class="btn btn-danger btn-block btn-md" id="reset"><i class="fas fa-undo"></i> Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="secondtable mt-3" id="">
                                <table id="dataSecondTable" class="display cell-border" style="width: 100%;">
                                    <thead>
                                        <tr style="background-color: #0066CC;color:#fff;" align="center">
                                            <th id='row1' width='' colspan='13'>Periode {{$bln. ' - ' . $thn}}</th>
                                        </tr>
                                        <tr style="background-color: #0066CC;color:#fff;">
                                            <th rowspan='2'>No</th>
                                            <th rowspan='2'>Item Number</th>
                                            <th rowspan='2'>Deskripsi</th>
                                            <th rowspan='2'>Type</th>
                                            <th rowspan='2'>Loc From</th>
                                            <th rowspan='2'>Loc To</th>
                                            <th rowspan='2'>Barcode OK</th>
                                            <th rowspan='2'>Barcode Pending</th>
                                            <th rowspan='2'>Manual</th>
                                            <th rowspan='2'>Total</th>
                                            <th colspan='3' style="text-align: center">Need</th>
                                        </tr>
                                        <tr style="background-color: #0066CC;color:#fff;" align="center">
                                            <th id='row2' width =''>Minus Bom</th>
                                            <th id='row2' width =''>After Backflush</th>
                                            <th id='row2' width =''>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tblseconded">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
