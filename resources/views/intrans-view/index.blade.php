@extends('templates.main')

@section('title', 'View Intrans')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    Transaction Date :
                                </div>
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" class="form-control-sm">
                                        <option value="0" selected>Month</option>
                                        @for ($i = 1; $i <= 12; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                    <select name="tahun" id="tahun" class="form-control-sm">
                                        <option value="0" selected>Year</option>
                                        @for ($i = 2014; $i <= date('Y') + 2; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    Supplier Id
                                </div>
                                <div class="col-10">
                                    <input type="text" name="code" id="code" class="form-control-sm" style="border: 1px solid black">
                                    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#searchSupplier"><i
                                            class="fas fa-search"></i></button>
                                    <input type="text" name="name" id="name" class="form-control-sm" style="border: 1px solid black">
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    Countainer
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="countainer" id="countainer" class="form-control-sm" style="border: 1px solid black">
                                    <button class="btn btn-info btn-sm" data-toggle="modal" id="searchCountairModal" data-target="#searchCountair"><i
                                        class="fas fa-search"></i></button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-2">
                                    <button type="submit" class="btn btn-info btn-sm" id="Seacrh"
                                        onclick="SearchViewIntrans()"><i class="fas fa-search"></i> Search</button>
                                        <button class="btn btn-sm btn-danger" onclick="window.location.reload();"> <span class="fas fa-undo"></span> Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive mb-2" id="result">
                                <table class="display" id="main-table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Supplier</th>
                                            <th>Invoice Number</th>
                                            <th>ETD Date</th>
                                            <th>Lead Time Shipment</th>
                                            <th>Name of Ship</th>
                                            <th>Container</th>
                                            <th>Shift</th>
                                            <th>Penerima</th>
                                            <th>Lokasi</th>
                                            <th>Confirm Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="debug">
                                        <?php $no = 1; ?>
                                        @foreach ($data as $row)
                                            <?php
                                                $getDataDelivery = DB::table('intrans_leadtime_master')->where('lead_kode_supplier', $row->int_supp)->first();
                                                $getLeadDelivery = $getDataDelivery->lead_delivery;
                                                $estimasi_tgl_kedatangan = date('Y-m-d', strtotime('+'.$getLeadDelivery.'days', strtotime($row->int_etd)));

                                                $rInv = DB::select("SELECT distinct int_cek, int_cek from intrans_trans where
                                                int_inv= '$row->int_inv' and int_supp='$row->int_supp'");
                                                $jmlInv = count($rInv);
                                                $cek = $rInv[0]->int_cek;

                                                if ($jmlInv > 1) {
                                                    $confirm = 'Edit';
                                                } else {
                                                    if ($cek == 'T') {
                                                        $confirm = $row->int_confirm;
                                                    } else {
                                                        $confirm = '';
                                                    }
                                                }

                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{ $row->int_supp}} | {{ $row->ct_ad_name}}</td>
                                                <td>{{ $row->int_inv}}</td>
                                                <td>{{ $row->int_etd}}</td>
                                                <td style="background-color: #FFCC80">{{ $estimasi_tgl_kedatangan }}</td>
                                                <td>{{ $row->int_ship == null ? '' : $row->int_ship}}</td>
                                                <td>{{ empty($row->int_countainer_packaging) == true ? '-' :$row->int_countainer_packaging}}</td>
                                                <td>{{ empty($row->int_shift) == true ? '-' : $row->int_shift}}</td>
                                                <td>{{ empty($row->int_penerima_barang) == true ? '-' : $row->int_penerima_barang}}</td>
                                                <td>{{ empty($row->int_lokasi) == true ? '-' : $row->int_lokasi}}</td>
                                                <td>{{ $confirm }}</td>
                                                <td><a class="btn btn-sm btn-warning" href="/view-intrans-detail/{{base64_encode($row->int_supp)}}/{{base64_encode($row->int_inv)}}"> <span class="fas fa-eye"></span> </a></td>
                                            </tr>
                                        <?php $no++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal effects -->
    <div class="modal" id="searchSupplier">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header text-center">
                    <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal"
                        type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered text-center text-nowrap w-100" id="main-tablee">
                            <thead class="thead">
                                <tr>
                                    <th scope="col">Supplier Id</th>
                                    <th scope="col">Supplier Name</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="body">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal effects end-->

    <!-- Modal effects -->
    <div class="modal" id="searchCountair">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="documentContainer">
            <div class="modal-content modal-content-demo">
                <div class="modal-header text-center">
                    <h6 class="modal-title w-100">Container Lookup</h6><button aria-label="Close" class="close" data-dismiss="modal"
                        type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered text-center text-nowrap w-100 " id="main-tablee2">
                            <thead class="thead">
                                <tr>
                                    <th scope="col">Invoice NO</th>
                                    <th scope="col">Container</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="bodyContainer">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal effects end-->
@endsection
