@extends('templates.main')

@section('title', 'Detail - View Intrans')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row justify-content-start">
                        <div class="col-md-6">
                            <div>
                                <h6 class="main-content-label mb-3">Supplier : {{ $kode_supp }} ({{ $name_supp }}) -
                                    Inv : {{ $inv }}</h6>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-md-12 text-center">
                            <div class="table-responsive">
                                <table class="table table-bordered text-center w-100" id="main-table">
                                    <thead>
                                        <tr>
                                            <th>Document</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($document) > 0)
                                            @foreach ($document as $row)
                                                <tr>
                                                    <td>
                                                        <a href="{{ asset('dok_intrans_whs/' . $row->doc_file) }}"
                                                            target="_blank" rel="noopener noreferrer">{{ $row->doc_file }}</a>
                                                        {{-- @if (file_exists(public_path('file/intrans/' . $row->doc_file)))
                                                                    <a href="{{ asset('file/intrans/' . $row->doc_file) }}"
                                                                        target="_blank"
                                                                        rel="noopener noreferrer">{{ $row->doc_file }}</a>
                                                                @else
                                                                    <a href="https://purchasing.akebono-astra.co.id/dok_intrans_prc/{{ $row->doc_file }}"
                                                                        target="_blank"
                                                                        rel="noopener noreferrer">{{ $row->doc_file }}</a>
                                                                @endif --}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive mb-2">
                                <table class="table table-striped table-bordered text-center w-100" id="main-table">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">PO Number</th>
                                            <th scope="col">Line PO</th>
                                            <th scope="col">Part</th>
                                            <th scope="col">Description</th>
                                            <th scope="col">UM</th>
                                            <th scope="col">Qty PO</th>
                                            <th scope="col">No Receipt</th>
                                            <th scope="col">Lot</th>
                                            <th scope="col">Qty Receipt</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $number = 1; ?>
                                        @foreach ($data as $row)
                                            <?php
                                            /**
                                            * No RC = ''
                                            * query No_RC SOAP (error) & no column No_RC
                                            **/
                                            // dd($kode_supp);
                                            $getNoRC = prh_hist($row->int_po);
                                            if ($getNoRC == false) {
                                                    $noRC = $row->int_qxtend_stat;
                                                } else {
                                                    foreach ($getNoRC as $nor) {
                                                        if ($nor['prhPsNbr'] == $inv) {
                                                            if ($nor['prhPart'] == $row->int_part) {
                                                                $noRC = $nor['prhReceiver'];
                                                            } else {
                                                                $noRC = $row->int_qxtend_stat;
                                                            }
                                                        } else {
                                                        $noRC = $row->int_qxtend_stat;
                                                    }
                                                }
                                            }


                                            /**
                                            * data Soap PO Detail (validasi)
                                            **/
                                                $GetPoDetail = po_detail($row->int_po);
                                                
                                                if ($GetPoDetail == false) {
                                                    $item = '';
                                                    $um = '';
                                                    $qty = 0;
                                                    $price = 0;
                                                    $qtyRcp = 0;
                                                } else {
                                                    foreach ($GetPoDetail as $poDet) {
                                                        // dd($poDet);
                                                        if ($poDet['line'] == $row->int_line) {
                                                            $item = is_array($poDet['item_deskripsi']) ? '-' : $poDet['item_deskripsi'];
                                                            $um = $poDet['po_um'];
                                                            $qty = $poDet['qty_po'];
                                                            $price = $poDet['item_price'];
                                                            $qtyRcp = $poDet['qty_receive'];
                                                        } else {
                                                            $item = '';
                                                            $um = '';
                                                            $qty = 1;
                                                            $price = 1;
                                                            $qtyRcp = 1;
                                                        }
                                                    }
                                                }

                                                $total = $price * $row->int_qty_rcp;

                                                /**
                                                 * deskripsi (validasi)
                                                 **/

                                                $getDesc = pt_mstr($row->int_part);
                                                // dd($getDesc);
                                                if ($getDesc != null) {
                                                    if (is_array($getDesc['deskripsi1'])) {
                                                        $desc1 = '';
                                                    } else {
                                                        $desc1 = $getDesc['deskripsi1'];
                                                    }

                                                    if (is_array($getDesc['deskripsi2'])) {
                                                        $desc2 = '';
                                                    } else {
                                                        $desc2 = $getDesc['deskripsi2'];
                                                    }
                                                    $desc = $desc1 . ' ' . $desc2;
                                                } else {
                                                    $desc = '-';
                                                }
                                                ?>
                                                <tr>
                                                    <td>{{ $number }}</td>
                                                    <td>{{ $row->int_po }}</td>
                                                    <td>{{ $row->int_line }}</td>
                                                    <td>{{ $row->int_part }}</td>
                                                    <td>{{ $desc }}</td>
                                                    <td>{{ $um }}</td>
                                                    <td>{{ number_format($qty, 6) }}</td>
                                                    <td>{{ $noRC }}</td>
                                                    <td>{{ $row->int_lot }}</td>
                                                    <td>{{ number_format($row->int_qty_rcp, 6) }}</td>
                                                </tr>
                                                <?php $number++; ?>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <a href="/view-intrans" class="btn btn-sm btn-danger"><span class="fas fa-undo"></span> Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
