@extends('templates.main')

@section('title', 'View Intrans Approved')

@section('body')
    <div class="row">
        <div class="col-md-12">
            <div class="card custom-card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    Transaction Date
                                </div>
                                <div class="col-md-3">
                                    <select name="month" id="month" class="form-control-sm mr-1">
                                        <option value="">Month</option>
                                        @for ($i = 1; $i <= 12; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                    <select name="year" id="year" class="form-control-sm mr-1">
                                        <option value="">Year</option>
                                        @for ($i = 2014; $i < date('Y') + 2; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    Supplier ID
                                </div>
                                <div class="col-10">
                                    <input type="text" name="code" id="code" class="form-control-sm" style="border: 1px solid black">
                                    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#searchSupplier"><i
                                            class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-lg-4 col-sm-4 mb-2">
                            <button type="submit" class="btn btn-sm btn-info mb-2" width="30px" id="searchIntransView"><i class="fas fa-search"></i> Search</button>
                            <button type="button" class="btn btn-sm btn-danger mb-2" width="30px" onclick="window.location.reload();"><i class="fas fa-undo"></i> Reset</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                            <table id="example" class="display text-center" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th class="text-white">No</th>
                                        <th class="text-white">Supplier</th>
                                        <th class="text-white">Invoice Number</th>
                                        <th class="text-white">ETD Date</th>
                                        <th class="text-white">Lead Time Shipment</th>
                                        <th class="text-white">Name of Ship</th>
                                        <th class="text-white">Confirm Date</th>
                                        <th class="text-white">Approval Name</th>
                                        <th class="text-white">Approval Status</th>
                                        <th class="text-white">Approval Date</th>
                                        <th class="text-white">Transfer Status</th>
                                        <th class="text-white">Detail</th>
                                        <th class="text-white">Tranfer</th>
                                        <th class="text-white">Print LPB</th>
                                    </tr>
                                </thead>
                                <tbody id="result">
                                    <?php
                                        $no = 1;
                                        foreach ($data as $row) :
                                            $jmlInv = 0;

                                            $int_inv = $row->int_inv; #0
                                            $int_etd = $row->int_etd; #1
                                            $int_ship = $row->int_ship; #2
                                            $int_confirm = $row->int_confirm; #3
                                            $int_supp = $row->int_supp; #4
                                            $int_confirm_whs_status = $row->int_confirm_whs_status; #5
                                            $int_inventory_ket = $row->int_inventory_ket; #6
                                            $rInv = DB::select("SELECT distinct int_cek, int_cek from intrans_trans where
                                            int_inv= '$int_inv' and int_supp='$int_supp'");
                                            $jmlInv = count($rInv);
                                            $cek = $rInv[0]->int_cek;

                                            $jmlLot = DB::select("SELECT * FROM intrans_trans WHERE int_inv = '$int_inv' AND
                                            int_lot != '0'");

                                            if(count($jmlLot) > 0){
                                                $jmlLot = $jmlLot;
                                            }else{
                                                $jmlLot = 0;
                                            }

                                            $status_whs = $int_confirm_whs_status;
                                            $date_whs = $int_inventory_ket;
                                            $inventoryStatus = $int_inventory_ket;

                                            // untuk menambahkan angka + delivery
                                            $getDataDelivery = DB::table('intrans_leadtime_master')->where('lead_kode_supplier', $row->int_supp)->first();
                                            $getLeadDelivery = $getDataDelivery->lead_delivery;
                                            $estimasi_tgl_kedatangan = date('Y-m-d', strtotime('+'.$getLeadDelivery.'days', strtotime($int_etd)));

                                            if ($jmlInv > 1) {
                                                $confirm = 'Edit';
                                            } else {
                                                if ($cek == 'T') {
                                                    $confirm = $int_confirm;
                                                } else {
                                                    $confirm = '';
                                                }
                                            }

                                            $tanggal_lpb = '';
                                            if ($confirm == '') {
                                                $tampil_tgl_lpb = DB::table('SOAP_prh_hist')
                                                ->select("prhRcp_date")
                                                ->where('prhDomain', 'AAIJ')
                                                ->where('prhVend', $int_supp)
                                                ->where('prhPsNbr', $int_inv)
                                                ->first();
                                                if($tampil_tgl_lpb == null){
                                                    $tanggal_lpb = '';
                                                }else{
                                                    $tanggal_lpb = $tampil_tgl_lpb->prhRcp_date;
                                                }
                                            } else {
                                                $tanggal_lpb = $confirm;
                                            }

                                            // get supp (di native ada pada object class method getSupp)
                                            $nmSupp = DB::select("SELECT ct_ad_name FROM SOAP_Pub_Business_Relation WHERE ct_vd_addr = '$int_supp'");

                                            if($nmSupp == NULL){
                                                $nmSupp = '';
                                            }else{
                                                $nmSupp = $nmSupp[0]->ct_ad_name;
                                            }

                                            $inv = str_replace('&', '---', $int_inv);

                                            // untuk mendapat approval
                                            $dataApproval = DB::table('intrans_trans_email')
                                            ->where('email_invoice', $int_inv)
                                            ->first();

                                            $nama_section = $dataApproval->email_nama_section;
                                            $status_section = $dataApproval->email_section_status;
                                            $date_section = $dataApproval->email_section_date;

                                        ?>
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{ $int_supp.' - '.$nmSupp }}</td>
                                            <td>{{ $int_inv }}</td>
                                            <td>{{ $int_etd }}</td>
                                            <td style="background-color: #FFCC80">{{ $estimasi_tgl_kedatangan }}</td>
                                            <td>{{ $int_ship }}</td>
                                            <td>{{ $tanggal_lpb }}</td>
                                            <td>{{ $nama_section }}</td>
                                            <td>{{ $status_section }}</td>
                                            <td>{{ $date_section }}</td>
                                            <?php
                                                if($status_whs == 'OK'){
                                                    echo "<td bgcolor='#12F390' align='center'>".$status_whs."</td>";
                                                }else{
                                                    echo "<td>".$status_whs."</td>";
                                                }
                                            ?>
                                            <td><a class="btn btn-sm btn-warning" data-toggle="tooltip" title="detail" href="/transfer-intrans-detail/{{base64_encode($int_supp)}}/{{base64_encode($inv)}}"><span class="fas fa-eye"></span></a></td> {{-- detail --}}
                                            @if ($int_confirm_whs_status == "OK")
                                                <td>-</td>
                                                <td><a class="btn btn-sm btn-info" data-toggle="tooltip" title="print" href="/transfer-intrans-print/{{base64_encode($int_supp)}}/{{base64_encode($inv)}}"><span class="fas fa-print"></span></a></td> {{-- list_print_elpb --}}
                                            @else
                                                @if ($inventoryStatus == 'M')
                                                    <td><a class="btn btn-sm" style="background-color: #00af91; color:white" data-toggle="tooltip" title="terima barang" href="/transfer-intrans-barang/{{base64_encode($int_supp)}}/{{base64_encode($inv)}}"><span class="fas fa-long-arrow-alt-left"></span></a></td> {{-- transfer_confirm_whs_intransBarang --}}
                                                    <td><a class="btn btn-sm btn-info" data-toggle="tooltip" title="print" href="/transfer-intrans-print/{{base64_encode($int_supp)}}/{{base64_encode($inv)}}"><span class="fas fa-print"></span></a></td> {{-- list_print_elpb --}}
                                                @else
                                                    @if ($jmlLot == 0)
                                                        <td><a class="btn btn-sm btn-success" data-toggle="tooltip" title="transfer" href="/transfer-intrans/{{base64_encode($int_supp)}}/{{base64_encode($inv)}}"> <span class="fas fa-exchange-alt"></span> </a></td> {{-- transfer_confirm_whs_intrans --}}
                                                        <td><a class="btn btn-sm btn-info" data-toggle="tooltip" title="print" href="/transfer-intrans-print/{{base64_encode($int_supp)}}/{{base64_encode($inv)}}"><span class="fas fa-print"></span></a></td> {{-- list_print_elpb --}}
                                                    @else
                                                        <td><a class="btn btn-sm" style="background-color: #12F390; color:white" data-toggle="tooltip" title="transfer with lot" href="/transfer-intrans-whs/{{base64_encode($int_supp)}}/{{base64_encode($inv)}}"><span class="fas fa-exchange-alt"></span></a></td> {{-- transfer_confirm_whsLot_intrans --}}
                                                        <td><a class="btn btn-sm btn-primary" data-toggle="tooltip" title="print with lot" href="/transfer-intrans-print/{{base64_encode($int_supp)}}/{{base64_encode($inv)}}"><span class="fas fa-print"></span></a></td> {{-- list_print_elpb_lot --}}
                                                    @endif
                                                @endif
                                            @endif

                                        </tr>
                                        <?php $no++; endforeach  ?>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal effects -->
    <div class="modal" id="searchSupplier">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header text-center">
                    <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal"
                        type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered text-center text-nowrap w-100" id="main-tablee">
                            <thead class="thead">
                                <tr>
                                    <th scope="col">Supplier Id</th>
                                    <th scope="col">Supplier Name</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="body">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal effects end-->
@endsection
