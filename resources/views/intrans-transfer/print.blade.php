<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>E-LPB</title>
<style type="text/css">
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 12px;
}
.style3 {
	font-size: 14px;
	font-family: "Times New Roman", Times, serif;
}
.style4 {font-size: small}
.style5 {font-size: 14px}
.style7 {font-size: 12}
.style8 {font-size: 11px}
</style>
</head>
<body>
    {{-- {{ dd($data) }} --}}
    <table border="1" cellpadding="0" cellspacing="0" width="97" height="50">
        <tr>
          <td><table width="961" border="1" cellpadding="0" cellspacing="0">
            <tr>
              <td width="242" valign="top"><img src="{{ asset('img/auth') }}/akebonologo.png" width="242" height="78" /></td>
              <td width="332" valign="top"><div align="left"><span class="style1">&nbsp;<span class="style8">&nbsp;&nbsp;PT. AKEBONO BRAKE ASTRA INDONESIA</span></span></div>
                <span class="style3">&nbsp;&nbsp;&nbsp;&nbsp;<span class="style7">Jl. Pegangsaan DuaBlok A1, Km 1,6 <br />
      &nbsp;&nbsp;&nbsp;&nbsp;Pegangsaan Dua, Kelapa Gading,Jakarta Utara <br />
      &nbsp;&nbsp;&nbsp;&nbsp;DKI Jakarta Raya,14250- INDONESIA <br />
      &nbsp;&nbsp;&nbsp&nbsp;NPWP/TAX ID : 01.060.616.8-055.000</span></span> </td>
              <td colspan="2" valign="top">&nbsp;&nbsp;<span class="style4">Tel &nbsp;&nbsp;&nbsp;&nbsp;+62 21 46830075 <br />
      &nbsp;&nbsp;Fax &nbsp;&nbsp; &nbsp;+62 21 46826659 <br />
      &nbsp;&nbsp;Email&nbsp; info@akebono-astra.co.id<br />
      &nbsp;&nbsp;Url&nbsp;&nbsp;&nbsp;&nbsp; www.akebono-astra.co.id</span> </td>
              <td width="144" colspan="3" valign="top">&nbsp;No.&nbsp;&nbsp;: <?php echo $data['receiver']; ?><br />
                &nbsp;Tgl. &nbsp;: <?php echo $data['receiver_tgl']; ?><br /><br />
                &nbsp;Tgl Input : <?php echo $data['tgl_input']; ?></td>
              </tr>
              <tr>
                <td colspan="7"><div align="center"><strong>LAPORAN PENERIMAAN BARANG / JASA </strong></div></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td>
            <table width="941">
          <tr>
            <td width="496"><table width="100%">
              <tr>
                <td>DITERIMA DARI / SUPPLIER : <?php echo $data['supp_id']; ?> </td>
              </tr>
              <tr>
                <td><?php echo $data['supp_name']; ?></td>
              </tr>
              <tr>
                <td><?php echo $data['alamat1']; ?></td>
              </tr>
              <tr>
                <td><?php echo $data['alamat2']; ?></td>
              </tr>
            </table></td>
            <td width="433" valign="top"><table width="100%">
              <tr>
                <td width="118">PEMAKAI</td>
                <td width="20"><div align="center">:</div></td>
                <td width="351"><?php echo $data['pemakai']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp Transaksi Id : <?php echo $data['transaksi']; ?></td>
              </tr>
              <tr>
                <td>KPP / PP No. </td>
                <td><div align="center">:</div></td>
                <td><?php echo $data['pp']; ?></td>
              </tr>
              <tr>
                <td>PO No. </td>
                <td><div align="center">:</div></td>
                <td><?php echo $data['po_nbr']; ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
        
              </tr>
            </table></td>
          </tr>
        </table>
    </td>
</tr>
<tr>
  <td><table width="958" border="1" cellpadding="0" cellspacing="0">
    <tr>
      <td width="43"><div align="center"><span class="style5">NO.</span></div></td>
      <td width="286"><div align="center"><span class="style5">BARANG / JASA </span></div></td>
      <td width="203"><div align="center"><span class="style5">KODE BARANG </span></div></td>
      <td width="155"><div align="center"><span class="style5">JUMLAH SATUAN </span></div></td>
      <td width="259"><div align="center"><span class="style5">KETERANGAN PEMERIKSAAN </span></div></td>
    </tr>

    <?php
    $today = date('d/m/Y');
    foreach ($rs_tampil_data_rc as $row) {
        // dd($row);
        $rs_tampil_deskripsi = pt_mstr($row['prh_part']);
        
        // menampilkan data deskripsi po
        $tampil_descPO = po_detail($int_po);
        foreach ($tampil_descPO as $desc) {
            if($desc['item_number'] == $row['prh_part']){
                if($desc['line'] == $row['prh_line']){
                    if(is_array($desc['item_deskripsi'])){
                        $desPO = '';
                    }else{
                        $desPO = $desc['item_deskripsi'];
                    }
                }
            }
        }

        $rs_tampil_supplier = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr', $row['prh_vend'])->first();
        
        $desk = $rs_tampil_deskripsi['deskripsi1'];
        
        if($desk == ''){
            $deskripsi = $desPO;
        }else{
            $deskripsi = $desk;
        }

        // dd($row);

        $lokasi_item= $rs_tampil_deskripsi['lokasi_item'];
        $buyer= $rs_tampil_deskripsi['buyer_planner'];
        $supp_name = $rs_tampil_supplier->ct_ad_name;
        $part = $row['prh_part'];
        $line = $row['prh_line'];
        $qty = $row['prh_rcvd'];
        $rc = $row['prh_receiver'];
        $tgl_rc = $row['prh_rcp_date'];
        $sj = $row['prh_ps_nbr'];
        $um = $row['prh_um'];

        echo "<tr>";
        echo "<td align='center'>".$line."</td>";
        echo "<td>".$deskripsi."</td>";
        echo "<td align='center'>".$part."</td>";
        echo "<td align='right'>".number_format($qty)."&nbsp; ".$um."</td>";
        echo "<td>&nbsp;&nbsp;".$lokasi_item."&nbsp;&nbsp; ".$buyer."</td>";	
    }
    if($rs_hitung_data_rc < 14){
        $selisih = 14-$rs_hitung_data_rc;
        $tambah = 0;
        while ($tambah < $selisih) {
            echo "<tr>";
            echo "<td align='center'>&nbsp;<td>";
            echo "<td align='center'>&nbsp;<td>";
            echo "<td align='center'>&nbsp;<td>";
            echo "</tr>";
            $tambah = $tambah+1;
        }
    }else{
        echo '</tr>';
    }
    ?>
    </table></td>
</tr>
<tr>
<td><table width="903" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td width="903"><table>
    <tr>
      <td colspan="2">CATATAN</td>
      <td width="211">&nbsp;</td>
      <td width="208">&nbsp;</td>
      <td width="80">Tgl cetak </td>
      <td width="19">:</td>
      <td width="97"><?php echo $today; ?></td>
    </tr>
    <tr>
        <?php
        if($data['print_status1'] == true){
            echo "<td colspan='2' rowspan='2'><b>*DUPLICATE<b></td>";
        }else{
            echo "<td colspan='2' rowspan='2'>&nbsp;</td>";
        }
        ?>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>Surat Jalan </td>
        <td>:</td>
        <td><?php echo $sj; ?></td>
      </tr>
      {{-- batas --}}
      <?php
        
        $tampil_data_approval = collect(DB::select("SELECT DISTINCT b.section sectionDesc FROM elpb_dtl a, elpb_mstr b WHERE a.packing_slip = '$sj' AND a.lpb_code = b.lpb_code"))->first();
        if($tampil_data_approval != null){
          $sectionDesc = $tampil_data_approval->sectionDesc;
          if($sectionDesc == 'WKS'){
            $keterangan_desc ='Rizki Eka Putra';
          }else{
            $keterangan_desc ='Sapto Prayitno';
          }
        }else{
          $keterangan_desc ='Sapto Prayitno';
        }

      ?>
      {{-- batas --}}
      <tr>
        <td><font color='blue'> <b> <i> Digitally Signed </i> </b> </font><br>by <?php echo  $keterangan_desc;?></td>
        <td align="right">&nbsp;</td>
        <td align="right"><div align="left"></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="128"><div align="center"></div></td>
        <td width="128"><div align="center"></div></td>
        <td>&nbsp;</td>
        <td align="right">&nbsp;</td>
        <td align="right"><div align="left"></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</td>
  </tr>
</table>

<script>
  window.print();
</script>