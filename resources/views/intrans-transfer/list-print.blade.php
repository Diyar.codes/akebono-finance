@extends('templates.main')

@section('title', 'Transfer Intrans List print')

@section('body')

   {{-- change type --}}
   <div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div class="row justify-content-start">
                    <div class="col-md-12">
                        <div>
                            <h6 class="main-content-label mb-3">Supplier : {{$kode_supp}} ({{$ctAdName}}) - Inv : {{$inv}}</h6>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive mb-2">
                            <table class="table table-striped table-bordered text-center w-100" id="main-table">
                                <thead>
                                    <tr>
                                        <th>No PO</th>
                                        <th>No Receipt</th>
                                        <th>Preview</th>
                                        <th>Print</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $row)
                                        @if ($row['noRC'] == 'false')
                                            <tr>
                                                <td>{{$row['int_po']}}</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
                                        @else
                                        <tr>
                                            <td>{{$row['int_po']}}</td>
                                            <td>{{$row['noRC']}}</td>
                                            <td><a class="btn btn-sm btn-warning" data-toggle="tooltip" title="preview" href="/transfer-intrans-preview/{{base64_encode($kode_supp)}}/{{base64_encode($row['noRC'])}}/{{base64_encode($row['int_po'])}}" target="_blank"><span class="fas fa-eye"></span></a></td>
                                            <td><a class="btn btn-sm btn-info" data-toggle="tooltip" title="print" href="/transfer-intrans-print-elpb/{{base64_encode($kode_supp)}}/{{base64_encode($row['noRC'])}}/{{base64_encode($row['int_po'])}}" target="_blank"><span class="fas fa-print"></span></a></td>
                                        </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <a href="/transfer-intrans-view" class="btn btn-sm btn-danger"> <span class="fas fa-undo"></span> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
