@extends('templates.main')

@section('title', 'Transfer Intrans')

@section('body')
    <style>
        .tableFixHead {
            overflow: auto;
            height: 200px;
            font-size: 12px;
        }

        .tableFixHead thead th {
            position: sticky;
            top: 0;
            z-index: 1;
        }

        /* Just common table stuff. Really. */
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            padding: 8px 16px;
        }

        th {
            background: #0066CC;
        }

    </style>
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <form action="/transfer-intrans-whs-save" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 mb-2">
                                <div>
                                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                                </div>
                                {{-- supplier id --}}
                                <div class="row mb-2">
                                    <div class="col-md-2">
                                        Supplier ID
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="txtSupp" id="txtSupp"
                                            class="form-control col-sm-6 input-sm" value="{{ $data['kodesup'] }}"
                                            readonly>
                                    </div>
                                </div>
                                {{-- invoice --}}
                                <div class="row mb-2">
                                    <div class="col-md-2">
                                        Invoice
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="txtInv" id="txtInv" class="form-control col-sm-6 input-sm"
                                            value="{{ $data['inv'] }}" readonly>
                                    </div>
                                </div>
                                {{-- name of ship and etd date --}}
                                <div class="row mb-2">
                                    <div class="col-md-2">
                                        Name of Ship
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="txtKapal" id="txtKapal"
                                            class="form-control col-sm-6 input-sm" value="{{ $data['ship_name'] }}"
                                            readonly>
                                    </div>
                                    <div class="col-md-2">
                                        ETD Date
                                    </div>
                                    <div class="col-md-4">
                                        <input type="date" name="txtEtd" id="txtEtd" class="form-control col-sm-6 input-sm"
                                            value="{{ $data['etd_date'] }}" readonly>
                                    </div>
                                </div>
                                {{-- bl no and bl date --}}
                                <div class="row mb-2">
                                    <div class="col-md-2">
                                        BL No
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="txtBl" id="txtBl" value="{{ $data['no_bl'] }}"
                                            class="form-control col-sm-6 input-sm" readonly>
                                    </div>
                                    <div class="col-md-2">
                                        BL Date
                                    </div>
                                    <div class="col-md-4">
                                        <input type="date" value="{{ $data['etd_date'] }}" name="txtBl_date"
                                            id="txtBl_date" class="form-control col-sm-6 input-sm" readonly>
                                    </div>
                                </div>
                                {{-- effdate --}}
                                <div class="row mb-2">
                                    <div class="col-md-2">
                                        Effdate
                                    </div>
                                    <div class="col-md-4">
                                        <input type="date" name="txtEff" id="txtEff" class="form-control col-sm-6 input-sm">
                                    </div>
                                </div>
                                {{-- shift --}}
                                <div class="row mb-2">
                                    <div class="col-md-2">
                                        Shift
                                    </div>
                                    <div class="col-md-1">
                                        <select name="txtShift" id="txtShift" class="form-control form-control-sm">
                                            <option value="">Pilih</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                </div>
                                {{-- penerima --}}
                                <div class="row mb-2">
                                    <div class="col-md-2">
                                        Penerima
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="txtPenerima" id="txtPenerima"
                                            class="form-control col-sm-6 input-sm">
                                    </div>
                                </div>
                                {{-- lokasi --}}
                                <div class="row mb-2">
                                    <div class="col-md-2">
                                        Lokasi
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="txtLokasi" id="txtLokasi"
                                            class="form-control col-sm-6 input-sm">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered w-100" id="main-table">
                                        <thead>
                                            <tr>
                                                <th>Document Invoice</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="document_invoice">
                                            <tr>
                                                <td><input type="file" name="file[]" id="file[]" required></td>
                                                <td><button class="closeBtn btn btn-danger btn-sm"><span
                                                            class="closeBtn fa fa-times"></span></button></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2" class="text-right">
                                                    <button id="add" value="1" class="btn btn-success btn-sm">Add</button>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered text-center w-100" id="main-table">
                                        <thead>
                                            <tr>
                                                <th>Container</th>
                                                <th>Size</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data['container'] as $row)
                                                <tr>
                                                    <td>{{ $row->con_no }}</td>
                                                    <td>{{ $row->con_size }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @if (count($data['document']) > 0)
                            <div class="row mb-2">
                                <div class="col-md-12 text-center">
                                    <div class="table-responsive">
                                        <table class="table table-bordered text-center w-100" id="main-table">
                                            <thead>
                                                <tr>
                                                    <th>Document</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data['document'] as $row)
                                                    <tr>
                                                        <td>
                                                            <a href="{{ asset('file/intrans/' . $row->doc_file) }}"
                                                                target="_blank"
                                                                rel="noopener noreferrer">{{ $row->doc_file }}</a>
                                                            {{-- @if (file_exists(public_path('file/intrans/' . $row->doc_file)))
                                                                <a href="{{ asset('file/intrans/' . $row->doc_file) }}"
                                                                    target="_blank"
                                                                    rel="noopener noreferrer">{{ $row->doc_file }}</a>
                                                            @else
                                                                <a href="https://purchasing.akebono-astra.co.id/dok_intrans_prc/{{ $row->doc_file }}"
                                                                    target="_blank"
                                                                    rel="noopener noreferrer">{{ $row->doc_file }}</a>
                                                            @endif --}}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="row mb-2">
                            <div class="col-md-12">
                                <div class="tableFixHead mb-2">
                                    <table class="table-bordered" id="mainTable">
                                        <thead>
                                            <tr>
                                                <th scope="col">PO Number</th>
                                                <th scope="col">Line PO</th>
                                                <th scope="col">Part</th>
                                                <th scope="col">Desciption</th>
                                                <th scope="col">UM</th>
                                                <th scope="col">Qty PO</th>
                                                <th scope="col">Qty Open</th>
                                                <th scope="col">Lot</th>
                                                <th scope="col">Type</th>
                                                <th scope="col">LPB</th>
                                                <th scope="col" width="10%">Qty Receipt</th>
                                                <th scope="col">Konversi</th>
                                                <th scope="col">Qty Actual</th>
                                                <th scope="col">From</th>
                                                <th scope="col" width="10%">To</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1; ?>
                                            @foreach ($data['intrans'] as $row)
                                            <?php

                                            $inv = $data['inv'];
                                            $from_loc = 'INTRANS';
                                            $part = $row->int_part;
                                            
                                            $soapPoDetail = po_detail($row->int_po);
                                            // dd($soapPoDetail);
                                            if($soapPoDetail == null){
                                                $descPO = '';
                                                $um = '';
                                                $qty = 0;
                                                $price = 0;
                                                $qtyRcp = 0;
                                            }else{
                                                $descPO = $soapPoDetail[0]['item_deskripsi'];
                                                $um = $soapPoDetail[0]['po_um'];
                                                $qty = $soapPoDetail[0]['qty_po'];
                                                $price = $soapPoDetail[0]['item_price'];
                                                $qtyRcp = $soapPoDetail[0]['qty_receive'];
                                            }

                                            $tampil_item_qad = pt_mstr($part);
                                            if($tampil_item_qad != null){
                                                if(is_array($tampil_item_qad['lokasi_item'])){
                                                    $lokasi_to = '';
                                                }else{
                                                    $lokasi_to = $tampil_item_qad['lokasi_item'];
                                                }
                                                if(is_array($tampil_item_qad['deskripsi1'])){
                                                    $desc1 = '';
                                                }else{
                                                    $desc1 = $tampil_item_qad['deskripsi1'];
                                                }
                                                if(is_array($tampil_item_qad['deskripsi2'])){
                                                    $desc2 = '';
                                                }else{
                                                    $desc2 = $tampil_item_qad['deskripsi2'];
                                                }
                                                $descItem = $desc1.' '.$desc2;
                                            }else{
                                                $lokasi_to = '';
                                            }

                                            if($descPO != '' && $descPO != "-"){
                                                $desc = $descPO;
                                            }else{
                                                $desc = $descItem;
                                            }

                                            $cekKode = collect(DB::select("SELECT DISTINCT substring(no_pp,1,1) AS kode, item_number, item_type FROM SOAP_po_detail WHERE po_domain = 'AAIJ' AND no_po = '$row->int_po' AND line = '$row->int_line' AND item_number = '$row->int_part'"))->first();

                                            if($cekKode == null){
                                                $kode_pp = '-';
                                                $kode_part = '-';
                                                $type_memo = '-';
                                            }else{
                                                $kode_pp = $cekKode->kode;
                                                $kode_part = $cekKode->item_number;
                                                $type_memo = $cekKode->item_type;
                                            }
                                            
                                            $noRCAll = getPrhHist($row->int_po);
                                            $noRCAll = DB::table('SOAP_prh_hist')
                                            ->where('prhNbr', $row->int_po)->where('prhPsNbr', $inv)
                                            ->where('prhPart', $row->int_part)->where('prhLine',$row->int_line)->first();
                                            if($noRCAll != null){
                                                $noRC = $noRCAll->prhReceiver;
                                                $typeRC = '';
                                                $prh_konversi = 1;
                                            }else{
                                                $noRC = '-';
                                                $typeRC = '';
                                                $prh_konversi = 1;
                                            }
                                            $qtyOpen = $qty - $qtyRcp;
                                            $qtySisa = $qtyOpen - $data['int_qty_rcp'];

                                            $qty_actual = $data['int_qty_rcp'] * $prh_konversi;

                                            ?>
                                                <input type="hidden" name="po{{$no}}" value="{{$row->int_po}}">
                                                <tr>
                                                    <td>{{ $row->int_po }}</td>
                                                    <td>{{ $row->int_line }}</td>
                                                    <td>{{ $row->int_part }}</td>
                                                    <td>{{ $desc }}</td>
                                                    <td>{{ $um }}</td>
                                                    <td>{{$qty}}</td>
                                                    <td>{{$qtyOpen}}</td>
                                                    <td>{{ $row->int_lot }}</td>
                                                    <td>{{$typeRC}}</td>
                                                    <td>{{$noRC}}</td>
                                                    <td>{{ number_format($row->int_qty_rcp, 6) }}</td>
                                                    <td>{{ number_format($prh_konversi)}}</td>
                                                    <td><input type="text" class="form-control input-sm" name="txtActual{{$no}}" id="txtActual{{$no}}" value="{{$qty_actual}}"></td>
                                                    <td>{{$from_loc}}</td>
                                                    @if ($kode_pp == 'C' || $kode_pp == 'D')
                                                        <td><input type="text" name="txtLokasi_to{{$no}}" class="form-control input-sm" id="txtLokasi_to{{$no}}" value="{{$lokasi_to}}" readonly></td>
                                                    @else
                                                        <td><input type="text" name="txtLokasi_to{{$no}}" class="form-control input-sm" id="txtLokasi_to{{$no}}" value="{{$lokasi_to}}" readonly></td>
                                                    @endif
                                                </tr>
                                                <input type="hidden" name="hdSup{{$no}}" value="{{$row->int_supp}}">
                                                <input type="hidden" name="hdInv{{$no}}" value="{{$row->int_inv}}">
                                                <input type="hidden" name="hdPO{{$no}}" value="{{$row->int_po}}">
                                                <input type="hidden" name="hdLine{{$no}}" value="{{$row->int_line}}">
                                                <input type="hidden" name="hdPart{{$no}}" value="{{$row->int_part}}">
                                                <input type="hidden" name="hdLot{{$no}}" value="{{$row->int_lot}}">
                                            <?php $no++; ?>
                                            @endforeach
                                            <input type="hidden" name="txtJml" value="{{$no - 1}}">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <a href="/transfer-intrans-view" class="btn btn-sm btn-danger"> <span class="fas fa-undo"></span> Back</a>
                                <button type="submit" name="submit" value="reject" class="btn btn-sm btn-danger"> <span class="fas fa-close"></span> Reject</button>
                                <button type="submit" name="submit" value="confirm" class="btn btn-sm btn-success">  <span class="fas fa-check"></span>Confirm</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
