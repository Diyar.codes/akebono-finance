@extends('templates.main')

@section('title', 'Message - Create Message')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div>
                        <h6 class="main-content-label mb-3">@yield('title')</h6>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="to">To :</label>
                                <input type="text" name="to" id="to" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="to">Message :</label>
                                <textarea name="message" class="form-control" id="message" cols="30" rows="10"></textarea>
                            </div>
                            <button class="btn btn-success">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
