@extends('templates.main')

@section('title', 'Message - Inbox Message')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div>
                        <h6 class="main-content-label mb-3">@yield('title')</h6>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header bg-primary text-white">To : josua</div>
                                <div class="card-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed sequi delectus eaque praesentium minima fugit libero! Earum reiciendis velit nihil, ut veniam pariatur consequatur. Eaque, dolorem praesentium. Beatae, eos corporis?</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
