@extends('templates.main')

@section('title', 'Login')

@section('body')
<img src="{{asset('img/auth/gradient.png')}}" alt="wave" class="wave">

<header>
    <h5>PT. Akebono Brake Astra Indonesia</h5>
    {{-- <img src="{{asset('img/auth/akebonologo.png')}}" alt="logo akebono" class="logo"> --}}
    <h5 class="cta">Butuh Bantuan? <span>Hubungi Kami</span></h5>
</header>

<div class="container">
    <div class="login-container">
        <form action="{{ route('doLogin') }}" method="post" autocomplete="off">
            @csrf
            <img src="{{asset('img/auth/akebonologo.png')}}" alt="logo akebono" class="avatar">
            @if(session()->has('error'))
            <div class="alert">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                <p>{{ session()->get('error') }}</p>
            </div>
            @endif
            <h2>Selamat datang di PORTAL SUPPLIER</h2>
            <div class="input-div one">
                <div class="i">
                    <i class="fas fa-user"></i>
                </div>
                <div>
                    <h5>Username</h5>
                    <input type="text" class="input" name="username">
                </div>
            </div>
            @error('username')
                <p class="error-username">{{ $message }}</p>
            @enderror
            <div class="input-div two">
                <div class="i">
                    <i class="fas fa-lock"></i>
                </div>
                <div>
                    <h5>Password</h5>
                    <input type="password" class="input" name="password">
                </div>
            </div>
            @error('password')
                <p class="error-password">{{ $message }}</p>
            @enderror
            <div class="btnform">
                <input type="submit" value="Login" class="btn">
            </div>
        </form>
    </div>
    <div class="img">
        <div class="img-illustration">
            <img src="{{asset('img/auth/loginimage.png')}}" alt="login image akebono">
        </div>
        <div class="announcement">
            <div class="title">
                <img src="{{asset('img/auth/icon.png')}}" alt="icon akebono">
                <h5>Pengumuman</h5>
            </div>
            <div class="list">
                <p>Sehubungan dengan adanya pengembangan system di Akebono, maka dengan ini kami memberitahukan bahwa :</p>
                <p>1. Supplier dapat mencetak langsung <b>LPB</b> di PORTAL(Supplier yang sudah disosialisasikan).</p>
                <p>2. Supplier diwajibkan untuk membuat <b>Packing Slip Barcode</b> di PORTAL(Supplier yang sudah disosialisasikan).</p>
                <p>3. Menambahkan <b>Line PO pada setiap</b> <b>Surat Jalan Supplier</b> dan urutkan <b>Line PO.</b></p>
            </div>
        </div>
    </div>
</div>
@endsection