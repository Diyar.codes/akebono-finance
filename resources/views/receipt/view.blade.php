@extends('templates.main')

@section('title', 'View Receipt')

@section('body')
    <div class="row mb-4">
        <div class="col-12 mb-4">
            <div class="card mt-n3">
                <div class="card-header mt-4 mb-n4">
                    <div class="col-lg-6">
                        <h2 style="font-weight: bold;" class="page-title">@yield('title')</h2>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4 mr-n4">
                            <div class="input-group">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Periode</span>
                                <div class="row">
                                    <div class="col-md-5">
                                        <select class="form-control" name="bulan" id="bulan">
                                            @for ($i = 1; $i <= 12; $i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-7">
                                        <select class="form-control" name="bulan" id="bulan">
                                            @for ($i = 2014; $i <= date('Y')+2; $i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mr-n5">
                            <div class="input-group">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">PO Number</span>
                                <input type="text" class="form-control" id="transaction" aria-label="po" aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-4 mr-n4">
                            <div class="input-group">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Item Number</span>
                                <input type="text" class="form-control" id="packing-slip" aria-label="item" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">RC</span>
                                <input type="text" class="form-control" id="packing-slip" aria-label="rc" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-2 mt-lg-0">
                            <button type="button" class="btn btn-info btn-block mb-2" id="filter"><i class="fas fa-search"></i> Search</button>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-sm-2 mt-lg-0">
                            <button type="button" class="btn btn-danger btn-block mb-2" id="reset"><i class="fas fa-undo"></i> Reset</button>
                        </div>
                    </div>
                    <div class="row table-responsive">
                        <table class="table table-striped table-bordered text-center w-100" id="main-table">
                            <thead class="exthead-custom">
                                <tr>
                                    <th scope="col" class="align-middle">Line</th>
                                    <th scope="col" class="align-middle">PO Number</th>
                                    <th scope="col" class="align-middle">Supplier ID</th>
                                    <th scope="col" class="align-middle">Item Number</th>
                                    <th scope="col" class="align-middle">Description</th>
                                    <th scope="col" class="align-middle">Receiver</th>
                                    <th scope="col" class="align-middle">Packing Slip</th>
                                    <th scope="col" class="align-middle">Qty Receipt</th>
                                    <th scope="col" class="align-middle">Tanggal Receipt</th>
                                    <th scope="col" class="align-middle">Retur RC</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
