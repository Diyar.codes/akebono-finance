Dear {{$username}} -San, <br> <br>

You have request from {{$pengirim}} -San to approve the new change type of Purchase Order that have been created on {{Date('d-M-Y H:m')}} with Transaction Number
<font size='4' color='blue'> <b> {{$transaksi_id}} </b> </font> and following item(s) below : <br> <br>

{!!$data!!}

<br> <br>

Please follow this link to respond as soon as possible : <br>
<font size='5' color='blue'> <i> <u> <a href="{{$url}}"> Click Here For Approval </a> </u> </i> </font> <br> <br>

If you need further information, please login to <a href='http://supplier.akebono-astra.co.id'> <font color='blue' size='3'> <b> <i> <u>Supplier Portal</u> </i> </b> </font> </a> or contact Information Technology Department of AAIJ on extension 525. <br>
Thank you. <br> <br> <br>

Regards, <br> <br>

Admin AAIJ Supplier Portal"
