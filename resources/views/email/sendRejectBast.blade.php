Dear {{ $data_email['dear'] }}, <br> <br>
    BAST number {{ $data_email['no_bast'] }} from {{ $data_email['nama_supp'] }} has REJECTED by {{ $data_email['nama'] }} with detail below : <br/><br/> 
    
    <table width='70%' border='0' cellspacing = '0' cellpadding='0'> 
        <tr align=''> 
            <td width='30%'>BAST Number</td>
            <td width='3%'>:</td>
            <td > {{ $data_email['no_bast'] }} </td>
        </tr>
        <tr align=''> 
            <td width=''>PO Number</td>
            <td>:</td>
            <td width=''> {{ $data_email['no_po'] }}</td>
        </tr>
        <tr align=''> 
            <td width=''>Supplier Name</td>
            <td>:</td>
            <td width=''> {{ $data_email['name'] }}</td>
        </tr>
        <tr align=''> 
            <td width=''>Company</td>
            <td>:</td>
            <td width=''> {{ $data_email['nama_supp'] }}</td>
        </tr>
    </table>

    <br/> <u>Reason rejected : {{ $data_email['reason'] }} </u></br>
    
    <br/>Thank you . <br> <br> <br>
    
    Regards, <br> <br>

    Admin