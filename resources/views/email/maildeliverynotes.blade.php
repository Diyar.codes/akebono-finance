<div>
	<div class="panel-body">
		To {{$data_email['namesupplier']}}

	    <p>You have a new Delivery Note from PT. Akebono Brake Astra Indonesia (AAIJ) that have been submitted on {{ date('d-m-Y',strtotime($data_email['date'])) }} with Delivery Note Number : </p>
		<h3><b> {{$data_email['numberDN']}} </b></h3>
		<p>Please refer to attachment for view detail of delivery note.</p>
		<p><a href="{{ $data_email['address'] }}"> Click Here For View Detail DN </a></p>

		<p>		
		 	If you need further information, please login to <a href='http://supplier.akebono-astra.co.id'><b> <i> <u>Supplier Portal</u></i></b> </a> or contact Information Technology Department of AAIJ on (021) 46830075 ext 525.
		</p>
		<p> Thank you. </p>

		<p>Regards, </p>

		<p>Admin AAIJ Purchasing Portal</p>
		
	</div>
					
</div>