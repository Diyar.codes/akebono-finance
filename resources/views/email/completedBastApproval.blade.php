Dear {{ $data_email['nama'] }}, <br> <br>
The BAST from {{ $data_email['nama_from'] }} with detail below : <br/><br/>
    <table width='70%' border='0' cellspacing = '0' cellpadding='0'> 
        <tr align=''> 
            <td width='30%'>BAST Number</td>
            <td width='3%'>:</td>
            <td >{{ $data_email['no_bast'] }} </td>
        </tr>
        <tr align=''> 
            <td width=''>PO Number</td>
            <td>:</td>
            <td width=''>{{ $data_email['no_po'] }}</td>
        </tr>
        <tr align=''> 
            <td width=''>Supplier Name</td>
            <td>:</td>
            <td width=''>{{ $data_email['name'] }}</td>
        </tr>
        <tr align=''> 
            <td width=''>Company</td>
            <td>:</td>
            <td width=''>{{ $data_email['nama_supp'] }}</td>
        </tr>
	</table>
<br/>has <b>complete signed</b>.
Please generate LPB Number.<br><br>
<br/>Please Follow this link to <a href="{{ $data_email['print_url'] }}">PRINT BAST </a><br>
<br/>Please Follow this link to <a href="{{ $data_email['url'] }}">VIEW BAST </a><br>

<br/>Thank you. <br> <br> <br>
    
Regards, <br> <br>

Admin";