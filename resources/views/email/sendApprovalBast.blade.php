<b>From : </b>{{ $data_email['from'] }} <br>
<b>Sent : </b> {{date('d-M-Y H:i')}} <br>
<b>To : </b> {{ $data_email['nama'] }} <{{ $data_email['nama_supp'] }}><br>
<b>Subject : </b> {{ $data_email['subject'] }}
<br><br>

<b> Dear {{ $data_email['nama'] }}</b> <br> <br>

You have request from aaij to approve <b>Supplier</b> BAST with detail below : <br/><br/>
        <table width='70%' border='0' cellspacing = '0' cellpadding='0'> 
                <tr align=''> 
                <td width='30%'>BAST Number</td>
                <td width='3%'>:</td>
                <td>{{ $data_email['no_bast'] }}</td>
            </tr>
            <tr align=''> 
                <td width=''>PO Number</td>
                <td>:</td>
                <td width=''>{{ $data_email['no_po'] }}</td>
            </tr>
            <tr align=''> 
                <td width=''>Supplier Name</td>
                <td>:</td>
                <td width=''>{{ $data_email['name'] }}</td>
            </tr>
            <tr align=''> 
                <td width=''>Company</td>
                <td>:</td>
                <td width=''>{{ $data_email['nama_supp'] }}</td>
            </tr>
        </table>
	<br/>Please follow this link to respond as soon as possible : <br><br/>
<font size='5' color='blue'> <u> <a href="{{ $data_email['url'] }}"> >> Click Here For Approval << </a> </u> </font> <br> <br>
<br>Thank you. <br> <br> <br>

Regards, <br> <br>

Admin AAIJ Supplier Portal
