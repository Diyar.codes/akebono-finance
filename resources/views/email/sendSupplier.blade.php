<?php date_default_timezone_set('Asia/Jakarta'); ?>
<b>From:</b>QPR PT. AKEBONO BRAKE ASTRA INDONESIA < qpr@akebono-astra.co.id> <br>
<b>CC:</b>qpr@akebono-astra.co.id <br>
<br><br>

<b> To  {{ $accountEmail['supp_name'] }}</b> <br> <br>

You have a new Quality Problem Report from PT. Akebono Brake Astra Indonesia (AAIJ) that have been submitted on {{date('D-M-Y')}}
<br/>QPR Number : <font size='4' color='blue'> <b> {{$no}} </b> </font>
<br/>Revise : <font size='4' color='blue'> <b> {{$accountEmail['revisi']}} </b> </font> <br> <br>

Please follow this link to respond as soon as possible : <br>
<font size='5' color='blue'> <i> <u> <a href="{{$url}}"> Click Here For Respon </a> </u> </i> </font> <br> <br>

If you need further information, please login to <a href='http://supplier.akebono-astra.co.id'> <font color='blue' size='3'> <b> <i> <u>Supplier Portal</u> </i> </b> </font> </a> or contact Information Technology Department of AAIJ on (021) 46830075 ext 525. <br>
Thank you. <br> <br> <br>

Regards, <br> <br>

Admin AAIJ Supplier Portal
