<div>
	<div class="panel-body">
		
		<b> Dear  PIC</b> <br> <br>
				
		You have a new notification about Quality Problem Report from {{$supp_name}} that have been respond on ".date('d-M-Y')." 
		<br/>QPR Number : <font size='4' color='blue'> <b> {{$no_qpr}} </b> </font>
		<br/>Revise : <font size='4' color='blue'> <b>{{$revisi}} </b> </font>
		<br/>Status : <font size='4' color='blue'> <b> COUNTERMEASURE </b> </font> <br> <br>
		
		Follow this link to view detail respond from Supplier : <br>
		<font size='5' color='blue'> <i> <u> <a href='<?= $address; ?>'> Click Here </a> </u> </i> </font> <br> <br>
		
		Regards, <br> <br>

		Admin AAIJ Supplier Portal
		
	</div>
					
</div>