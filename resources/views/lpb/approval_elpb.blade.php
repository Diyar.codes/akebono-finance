@extends('templates.main')

@section('title', 'Approval E-LPB')

@section('styles')
<style>
    #divSearch
    {
        border: thin gray solid;
        background-color:#0066CC;
        width:100%;

    }
    #divReport
    {
        width:100%;
        height:100%;
        position:fixed;
        z-index:999999;
    }
    .tblSearch {
        margin:0px 20px;
    }
    .tblGrafik td{
        border:none;
    }
    .tblSearch th{
        border:none;
        color:white;
        font-weight:bold;
        padding:5px;
    }
    .tblSearch td{

        border:none;


    }
    .TbllphHeader
    {
        margin:10px!important;
        border:thin black solid;
    }
    .TbllphHeader
    {
        text-align:center;
    }
    .lphHeader td{

        background-color:#CAE99D;
        font-weight:bold;
        text-align:center;


    }
     .tdHd
     {
        font-weight:bold;
        border:none;
        text-align:left!important;
     }
     #divScreenSaver
     {
        width:100%;
        height:100%;
        position:absolute;
        z-index:999999;
     }
     #fontTitle
     {
        font-size:22px;
        color:white;
        margin:20px;
        font-weight:bold;
     }
     #divScreenSaver td{
     font-size:14px;
     font-weight:bold;

     text-align:center;
     }
     #tbodyScreenSaver
     {
        background-color:white;
     }
     #tbodyScreenSaver td
     {
         color:black;

     }
     .TbllphHeader td
     {
        font-size:9px;
        text-shadow:none;
        font-family:arial;
     }
     .lphHeader td{
        font-size:11px;
        text-shadow:none;
        font-weight:999!important;
     }
</style>
@endsection
@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h6 class="main-content-label mb-3">@yield('title')</h6>
                    </div>
                </div>
                <form action="" method="GET">
                <div class="row">
                    <table>
                        <tr>
                            <td>
                                <div class="input-group mb-2">
                                    <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Date From</span>
                                    <input type="date" id="txtdate" name="dateFrom" value="{{$dateFrom}}" class="form-control" placeholder="" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </td>
                            <td>
                                <div class="input-group mb-2">
                                    <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Date To</span>
                                    <input type="date" id=txtdate2 name="dateTo" value="{{$dateTo}}" class="form-control" placeholder="" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </td>
                            <td>
                                <div class="input-group mb-2">
                                    <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Supplier</span>
                                    <select name="supp" class="form-control select-supp" id="selsupp">
                                        <option value="All">~~  All ~~</option>
                                        @foreach ($data as $dt)
                                        <option value='{{$dt->ct_vd_addr}}'>{{$dt->ct_ad_name}}</option>
                                        @endforeach 
                                    </select>
                                </div>        
                            </td>
                            <td>
                                <div class="input-group mb-2 ml-3">
                                    <button class="btn btn-success" id="btnShow">Show</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </form>
            <div id="RptGrafik" style='margin-left:10px'>

            </div>
                {{-- <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center w-100" id="main-table">
                        <thead>
                            <tr>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Category</th>
                                <th scope="col">27</th>
                                <th scope="col">28</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody class="tbodymain">
                            @foreach ($data as $item)
                            <tr>
                                <td>
                                {{$item->ct_vd_addr}}<br/>
                                {{$item->ct_ad_name}}
                                </td>
                                <td>
                                    kk
                                </td>
                                <td>
                                    Not Aproved
                                </td>
                                <td>
                                    Out Standing
                                </td>
                                <td>
                                    tot
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<!-- <script src="{{ asset('js/main.js') }}"></script> -->
    <script>
        // $(".select-supp").select2({
        //     tags: true
        // });
        // CSRF Token
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function(){
	$("#btnShow").bind("click", ShowData);


      $( "#selsupp" ).select2({
        ajax: {
          url: "{{route('lpb.get-supplier')}}",
          type: "post",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              _token: CSRF_TOKEN,
              search: params.term // search term
            };
          },
          processResults: function (response) {

            return {
              results: response
            };
          },
          cache: true
        }

      });

    });
    ShowData = function()
	{
        getLoader();
		// $(".loading_bar").show();
		var date = $("#txtdate").val().split("-");
		var date2 = $("#txtdate2").val().split("-");
		var Supplier = $("#selsupp").val();
		if(Supplier == "All")
			Supplier = "";
		var DateFrom = date[2]+"-"+date[1]+"-"+date[0];
		var DateTo = date2[2]+"-"+date2[1]+"-"+date2[0];

		var dataSend = "Status=1&DateFrom="+DateFrom+"&DateTo="+DateTo+"&Supplier="+Supplier;
		// alert(dataSend);
		$.ajax({
				url : "{{route('lpb.table')}}",
				type: "POST",
				data : dataSend,
				async:false,
				success: function(response)
				{
					$("#RptGrafik").html(response);
				}
			});

		$(".loading_bar").hide();
		return false;
	}
    </script>
@endsection
