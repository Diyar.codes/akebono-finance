@extends('templates.main')

@section('title', 'Print E-LPB')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="row">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Receiver</span>
                            <input type="text" class="form-control" placeholder="" id="rc" autocomplete="off">
                            <button class="btn btn-info" onclick="search()">Search</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h6 class="main-content-label mb-3">Last LPB</h6>
                        </div>
                        <div class="table-responsive mb-4 mt-3 secondtable">
                            <table class="table table-bordered text-center" id="tablenya"> 
                                <thead>
                                    <tr>
                                        <th class="wd-10p">Receiver</th>
                                        <th class="wd-20p">Purchase Order</th>
                                        <th class="wd-20p">Supplier Name</th>
                                        <th class="wd-20p">Packing Slip</th>
                                        <th class="wd-10p">Status Print</th>
                                        <th class="wd-20p">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tbodymain">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
