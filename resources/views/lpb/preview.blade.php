
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>E-LPB - Akebono</title>
            <style type="text/css">
            .style1 {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-weight: bold;
                font-size: 12px;
            }
            .style3 {
                font-size: 14px;
                font-family: "Times New Roman", Times, serif;
            }
            .style4 {font-size: small}
            .style5 {font-size: 14px}
            .style7 {font-size: 12}
            .style8 {font-size: 11px}
            </style>
    </head>
    <body>
          <?php 
          $today    = date('d-m-Y'); 
          $kode     = Auth::guard('pub_login')->user()->kode_supplier;
          $tampung  = $tangkap[0]['prhReceiver'];
            
          $print_status  = $tampil_data_rc[0]['prhPrint'];
			    $print_status1 = $tampil_data_rc[0]['prhLog01'];
          
          
          // dd($hitung_rc[0]->hitung);
        $data1 = $hitung_rc; ?>

        <table border="1" cellpadding="0" cellspacing="0" width="97" height="50">
            <tr>
                <td>
                    <table width="961" border="1" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="242" valign="top"><img src="{{ asset('img/auth') }}/akebonologo.png" width="242" height="78" /></td>
                            <td width="332" valign="top"><div align="left"><span class="style1">&nbsp;<span class="style8">&nbsp;&nbsp;PT. AKEBONO BRAKE ASTRA INDONESIA</span></span></div>
                            <span class="style3">&nbsp;&nbsp;&nbsp;&nbsp;<span class="style7">Jl. Pegangsaan DuaBlok A1, Km 1,6 <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;Pegangsaan Dua, Kelapa Gading,Jakarta Utara <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;DKI Jakarta Raya,14250- INDONESIA <br />
                    &nbsp;&nbsp;&nbsp&nbsp;NPWP/TAX ID : 01.060.616.8-055.000</span></span> </td>
                            <td colspan="2" valign="top">&nbsp;&nbsp;<span class="style4">Tel &nbsp;&nbsp;&nbsp;&nbsp;+62 21 46830075 <br />
                    &nbsp;&nbsp;Fax &nbsp;&nbsp; &nbsp;+62 21 46826659 <br />
                    &nbsp;&nbsp;Email&nbsp; info@akebono-astra.co.id<br />
                    &nbsp;&nbsp;Url&nbsp;&nbsp;&nbsp;&nbsp; www.akebono-astra.co.id</span> </td>
                            <td width="144" colspan="3" valign="top">&nbsp;No.&nbsp;&nbsp;: {{ $tangkap[0]['prhReceiver'] }} <br />
                            &nbsp;Tgl. &nbsp;: {{ date('d-F-Y',strtotime($tangkap[0]['prhRcp_date'] )) }}<br /><br />
                            &nbsp;Tgl Input : </td>
                        </tr>  
                        <tr>
                            <td colspan="7"><div align="center"><strong>LAPORAN PENERIMAAN BARANG / JASA </strong></div></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
              <td>
                <table width="941">
                  <tr>
                    <td width="496"><table width="100%">
                      <tr>
                        <td>DITERIMA DARI / SUPPLIER : {{ $tampil_supplier->ct_vd_addr }}</td>
                      </tr>
                      <tr>
                        <td>{{ $tampil_supplier->ct_ad_name }}</td>
                      </tr>
                      <tr>
                        <td>{{ $tampil_supplier->ct_ad_line1 }}</td>
                      </tr>
                      <tr>
                        <td>{{ $tampil_supplier->ct_ad_line2 }}</td>
                    </tr>
                </table>
              </td>

              <td width="433" valign="top"><table width="100%">
                <tr>
                  <td width="118">PEMAKAI</td>
                  <td width="20"><div align="center">:</div></td>
                  <td width="351">{{ $tampil_data_po_detail[0]['podRequest'] }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp Transaksi Id : {{ $tampil_tr->portal_tr_id }}</td>
                </tr>
                <tr>
                    <td>KPP / PP No. </td>
                    <td><div align="center">:</div></td>
                    <td><?= $tampil_data_po_detail[0]['no_pp'] ?></td>
                </tr>
                <tr>
                    <td>PO No. </td>
                    <td><div align="center">:</div></td>
                    <td>{{ $tampil_tr->portal_po_nbr }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
	    </td>
  </tr>
  <tr>
    <td>
      <table width="958" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td width="43"><div align="center"><span class="style5">NO.</span></div></td>
          <td width="286"><div align="center"><span class="style5">BARANG / JASA </span></div></td>
          <td width="203"><div align="center"><span class="style5">KODE BARANG </span></div></td>
          <td width="155"><div align="center"><span class="style5">JUMLAH SATUAN </span></div></td>
          <td width="259"><div align="center"><span class="style5">KETERANGAN PEMERIKSAAN </span></div></td>
        </tr>
            
        <?php foreach($tampil_data_rc as $tmp){
          foreach($tampil_data_po_detail as $po){
            if($po['item_number'] == $tmp['prhPart'] && $po['no_po'] == $tmp['prhNbr'] && $po['line'] == $tmp['prhLine']){
              $rs_tampil_descPO = array(
                  'item_deskripsi' => $po['item_deskripsi'],
              );
            }
          }

				  $desPO = $rs_tampil_descPO['item_deskripsi'];
          $rs_tampil_deskripsi = pt_mstr($tmp['prhPart']);

          $desk= $rs_tampil_deskripsi['deskripsi1'];

				  if ($desk == "-" || $desk == ""){
					    $deskripsi = $desPO;
				  }else{
					    $deskripsi = $desk;
				  }
            
          $lokasi_item  = $rs_tampil_deskripsi['lokasi_item'];
          $buyer        = $rs_tampil_deskripsi['buyer_planner'];
          $part         = $tmp['prhPart'];
          $line         = $tmp['prhLine'];
          $qty          = $tmp['prhRcvd'];
          $rc           = $tmp['prhReceiver'];
          $tgl_rc       = $tmp['prhRcp_date'];
          $sj           = $tmp['prhPsNbr'];
          $um           = $tampil_data_po_detail[0]['po_um'];
          ?>
			    <tr>
            <td align='center'><?= $line ?></td>
            <td><?= $deskripsi ?></td>
            <td align='center'><?= $part ?></td>
            <td align='right'><?= number_format($qty)."&nbsp; ".$um ?></td>
            <td>&nbsp;&nbsp; <?= $lokasi_item."&nbsp;&nbsp; ".$buyer ?></td>		
	
          <?php
            if ($data1 < 14){
              $selisih = 14-$data1;
              for( $tambah = 0; $tambah < $selisih; $tambah++){ ?>
                <tr>
                  <td align='center'>&nbsp;<td>
                  <td align='center'>&nbsp;<td>
                  <td align='center'>&nbsp;<td>
                </tr>
          <?php }
            }else{ ?>
              </tr>	
            <?php }
          
        } ?>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table width="903" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="903"><table>
            <tr>
              <td colspan="2">CATATAN</td>
              <td width="211">&nbsp;</td>
              <td width="208">&nbsp;</td>
              <td width="80">Tgl cetak </td>
              <td width="19">:</td>
              <td width="97"><?php echo $today; ?></td>
            </tr>
            <tr>
		          <td colspan='2' rowspan='2'><b>
              <?php
		            if($print_status1 == true){
			            echo "<b>*DUPLICATE</b>";
		            }else{
			            echo "&nbsp;";
		            }
              ?>
              </td>
          </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>Surat Jalan </td>
          <td>:</td>
          <td>{{ $tampil_tr->portal_ps_nbr }}</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="right"><font size='4'><b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp PREVIEW<b></font></td>
          <td align="right"><div align="left"></div></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="128"><div align="center"></div></td>
          <td width="128"><div align="center"></div></td>
          <td>&nbsp;</td>
          <td align="right">&nbsp;</td>
          <td align="right"><div align="left"></div></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</td>
  </tr>
</table>
<br/>
<br/>
</body>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>E-LPB - Akebono</title>
            <style type="text/css">
            .style1 {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-weight: bold;
                font-size: 12px;
            }
            .style3 {
                font-size: 14px;
                font-family: "Times New Roman", Times, serif;
            }
            .style4 {font-size: small}
            .style5 {font-size: 14px}
            .style7 {font-size: 12}
            .style8 {font-size: 11px}
            </style>
    </head>
    <body>
          <?php 
          $today    = date('d-m-Y'); 
          $kode     = Auth::guard('pub_login')->user()->kode_supplier;
          $tampung  = $tangkap[0]['prhReceiver'];
          
          $print_status  = $tampil_data_rc[0]['prhPrint'];
			    $print_status1 = $tampil_data_rc[0]['prhLog01'];
          
          
          // dd($hitung_rc[0]->hitung);
        $data1 = $hitung_rc; ?>

        <table border="1" cellpadding="0" cellspacing="0" width="97" height="50">
            <tr>
                <td>
                    <table width="961" border="1" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="242" valign="top"><img src="{{ asset('img/auth') }}/akebonologo.png" width="242" height="78" /></td>
                            <td width="332" valign="top"><div align="left"><span class="style1">&nbsp;<span class="style8">&nbsp;&nbsp;PT. AKEBONO BRAKE ASTRA INDONESIA</span></span></div>
                            <span class="style3">&nbsp;&nbsp;&nbsp;&nbsp;<span class="style7">Jl. Pegangsaan DuaBlok A1, Km 1,6 <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;Pegangsaan Dua, Kelapa Gading,Jakarta Utara <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;DKI Jakarta Raya,14250- INDONESIA <br />
                    &nbsp;&nbsp;&nbsp&nbsp;NPWP/TAX ID : 01.060.616.8-055.000</span></span> </td>
                            <td colspan="2" valign="top">&nbsp;&nbsp;<span class="style4">Tel &nbsp;&nbsp;&nbsp;&nbsp;+62 21 46830075 <br />
                    &nbsp;&nbsp;Fax &nbsp;&nbsp; &nbsp;+62 21 46826659 <br />
                    &nbsp;&nbsp;Email&nbsp; info@akebono-astra.co.id<br />
                    &nbsp;&nbsp;Url&nbsp;&nbsp;&nbsp;&nbsp; www.akebono-astra.co.id</span> </td>
                            <td width="144" colspan="3" valign="top">&nbsp;No.&nbsp;&nbsp;: {{ $tangkap[0]['prhReceiver'] }} <br />
                            &nbsp;Tgl. &nbsp;: {{ date('d-F-Y',strtotime($tangkap[0]['prhRcp_date'] )) }}<br /><br />
                            &nbsp;Tgl Input : </td>
                        </tr>  
                        <tr>
                            <td colspan="7"><div align="center"><strong>LAPORAN PENERIMAAN BARANG / JASA </strong></div></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
              <td>
                <table width="941">
                  <tr>
                    <td width="496"><table width="100%">
                      <tr>
                        <td>DITERIMA DARI / SUPPLIER : {{ $tampil_supplier->ct_vd_addr }}</td>
                      </tr>
                      <tr>
                        <td>{{ $tampil_supplier->ct_ad_name }}</td>
                      </tr>
                      <tr>
                        <td>{{ $tampil_supplier->ct_ad_line1 }}</td>
                      </tr>
                      <tr>
                        <td>{{ $tampil_supplier->ct_ad_line2 }}</td>
                    </tr>
                </table>
              </td>

              <td width="433" valign="top"><table width="100%">
                <tr>
                  <td width="118">PEMAKAI</td>
                  <td width="20"><div align="center">:</div></td>
                  <td width="351">{{ $tampil_data_po_detail[0]['podRequest'] }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp Transaksi Id : {{ $tampil_tr->portal_tr_id }}</td>
                </tr>
                <tr>
                    <td>KPP / PP No. </td>
                    <td><div align="center">:</div></td>
                    <td><?= $tampil_data_po_detail[0]['no_pp'] ?></td>
                </tr>
                <tr>
                    <td>PO No. </td>
                    <td><div align="center">:</div></td>
                    <td>{{ $tampil_tr->portal_po_nbr }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
	    </td>
  </tr>
  <tr>
    <td>
      <table width="958" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td width="43"><div align="center"><span class="style5">NO.</span></div></td>
          <td width="286"><div align="center"><span class="style5">BARANG / JASA </span></div></td>
          <td width="203"><div align="center"><span class="style5">KODE BARANG </span></div></td>
          <td width="155"><div align="center"><span class="style5">JUMLAH SATUAN </span></div></td>
          <td width="259"><div align="center"><span class="style5">KETERANGAN PEMERIKSAAN </span></div></td>
        </tr>
            
        <?php foreach($tampil_data_rc as $tmp){
          foreach($tampil_data_po_detail as $po){
            if($po['item_number'] == $tmp['prhPart'] && $po['no_po'] == $tmp['prhNbr'] && $po['line'] == $tmp['prhLine']){
              $rs_tampil_descPO = array(
                  'item_deskripsi' => $po['item_deskripsi'],
              );
            }
          }

				  $desPO = $rs_tampil_descPO['item_deskripsi'];
          $rs_tampil_deskripsi = pt_mstr($tmp['prhPart']);

          $desk= $rs_tampil_deskripsi['deskripsi1'];

				  if ($desk == "-" || $desk == ""){
					    $deskripsi = $desPO;
				  }else{
					    $deskripsi = $desk;
				  }
            
          $lokasi_item  = $rs_tampil_deskripsi['lokasi_item'];
          $buyer        = $rs_tampil_deskripsi['buyer_planner'];
          $part         = $tmp['prhPart'];
          $line         = $tmp['prhLine'];
          $qty          = $tmp['prhRcvd'];
          $rc           = $tmp['prhReceiver'];
          $tgl_rc       = $tmp['prhRcp_date'];
          $sj           = $tmp['prhPsNbr'];
          $um           = $tampil_data_po_detail[0]['po_um'];
          ?>
			    <tr>
            <td align='center'><?= $line ?></td>
            <td><?= $deskripsi ?></td>
            <td align='center'><?= $part ?></td>
            <td align='right'><?= number_format($qty)."&nbsp; ".$um ?></td>
            <td>&nbsp;&nbsp; <?= $lokasi_item."&nbsp;&nbsp; ".$buyer ?></td>		
	
          <?php
            if ($data1 < 14){
              $selisih = 14-$data1;
              for( $tambah = 0; $tambah < $selisih; $tambah++){ ?>
                <tr>
                  <td align='center'>&nbsp;<td>
                  <td align='center'>&nbsp;<td>
                  <td align='center'>&nbsp;<td>
                </tr>
          <?php }
            }else{ ?>
              </tr>	
            <?php }
          
        } ?>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table width="903" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="903"><table>
            <tr>
              <td colspan="2">CATATAN</td>
              <td width="211">&nbsp;</td>
              <td width="208">&nbsp;</td>
              <td width="80">Tgl cetak </td>
              <td width="19">:</td>
              <td width="97"><?php echo $today; ?></td>
            </tr>
            <tr>
		          <td colspan='2' rowspan='2'><b>
              <?php
		            if($print_status1 == true){
			            echo "<b>*DUPLICATE</b>";
		            }else{
			            echo "&nbsp;";
		            }
              ?>
              </td>
          </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>Surat Jalan </td>
          <td>:</td>
          <td>{{ $tampil_tr->portal_ps_nbr }}</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="right"><font size='4'><b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp PREVIEW<b></font></td>
          <td align="right"><div align="left"></div></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="128"><div align="center"></div></td>
          <td width="128"><div align="center"></div></td>
          <td>&nbsp;</td>
          <td align="right">&nbsp;</td>
          <td align="right"><div align="left"></div></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</td>
  </tr>
</table>
<br/>
<br/>
</body>

