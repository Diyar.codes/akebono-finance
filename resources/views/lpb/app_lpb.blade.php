<html>
	<head>
	<script type="text/javascript"src="{{ asset('akebono_asset/js/jquery-1.9.1.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('akebono_asset/css/jquery-ui.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('akebono_asset/css/app_lpb2.css') }}">
	<script src="{{ asset('akebono_asset/js/jquery-ui.js') }}" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('akebono_asset/css/progressBar.css') }}">
	<script type="text/javascript">
	
	$(document).ready(function(){
		raiseDatePicker();
		$("#divHideApp").hide();
		$(".loading_bar").hide();
		$("#divAddItem").hide();
		$(".imgThumb").bind("click", thumbClick);
		$(".trLPB").bind("click", LPBDetail);
		$(".divApp").bind("click", Approval);
		$(".divHide").bind("click", HideApproval);
		$("#btnHide").bind("click", hideData);
		$("#imgClose").bind("click", closeHide);
		$("#tbodyContentAddItem").delegate(".txtQty","change",changeQty);
		$(".btnItem").bind("click", showPopUp);
		$("#tbodyAddItem").delegate("tr","click",selectRow);
		$("#btnSelectItem").bind("click",selectDataItem); 
		$("#btnCancel").bind("click",cancelAddItem); 
		$("#txtEffDate").bind("change",changeEffDate); 
		$("#txtPS").bind("change",changePS); 
		$("#tbodyContentAddItem").delegate(".trData","click",clickTr); 
		$("#tbodyContentAddItem").delegate(".txtQty","keypress",keypressQty); 
		$("#tbodyContentAddItem").delegate(".txtQty","focus",focusQty); 
		$(".txtQty").eq(0).focus().select();
	});
	clickTr = function(){
		$(this).find(".txtQty").focus().select();
	}

	focusQty = function(){
		$(this).select();
	}

	keypressQty = function(e){
		if(e.which === 13){
			var curIdx = $('.txtQty').index(this);
			var nextIdx = parseInt(curIdx) + 1;
			
			if( $('.txtQty').eq(nextIdx).length) {
				$(".txtQty").eq(nextIdx).focus().select();
			}else{
				if(confirm("Are you sure to approve this LPB ? ")){
					$(".divApp").trigger("click");
				}
			}
		}
	}
	raiseDatePicker = function(){
		$( "#txtEffDate").datepicker({
			changeMonth : true,
			changeYear : true,
			dateFormat : 'dd-mm-yy',
			yearRange : '-2:+10'
		});
	}
	cancelAddItem = function()
	{
		$("#divAddItem").hide();
	}
	selectDataItem = function(){
		var ItemType = $(".selectRow").find(".divPopUP_Item").text().split(' : ');
		var Desc = $(".selectRow").find(".tdPopUp_Desc").attr("desc");
		var PO = $(".selectRow").find(".tdPopUp_Desc").attr("po");
		var PS = $(".selectRow").find(".tdPopUp_Desc").attr("ps");
		var Line = $(".selectRow").find(".tdPopUp_Line").text();
		var Loc = $(".selectRow").find(".tdPopUp_Desc").attr("loc");
		var UM = $(".selectRow").find(".tdPopUp_Desc").attr("um");
		var Memo = $(".selectRow").find(".tdPopUp_Desc").attr("memo");
		var PMCode = $(".selectRow").find(".tdPopUp_Desc").attr("pm_code");
		var OpenPO = $(".selectRow").find(".tdPopUp_Desc").attr("open_po_qad");
		var PP = $(".selectRow").find(".tdPopUp_Desc").attr("pp");
		var PPDesc = $(".selectRow").find(".tdPopUp_Desc").attr("pp_desc");
		var PartType = $(".selectRow").find(".tdPopUp_Desc").attr("part_type");
		var Item = ItemType[0];
		var Type = ItemType[1];
		
		var Content = "";
		Content += "<tr class='trData trNewData' item='"+ Item +"' desc='"+ Desc +"' type='"+ Type +"' loc='"+ Loc +"' um='"+ UM +"' memo='"+ Memo +"' pm_code='"+ PMCode +"' pp='"+ PP +"' pp_desc='"+ PPDesc +"' part_type='"+ PartType +"'>";
			Content += "<td style='width:200px;font-size:15px;'><div class='divItem'>"+Item +"</div>"+ Desc +" <b>"+ Type +"</b></td>";
			Content += "<td style='text-align:center; width:50px;font-size:15px;' >"+UM+"</td>";
			Content += "<td style='text-align:center;background-color:#feca57;color:black;width:50px;font-size:15px;' class='tdLine'>"+ Line +"</td>";
			Content += "<td style='text-align:right;width:60px;font-size:15px;'></td>";
			Content += "<td style='width:60px;text-align:right;font-size:15px;'></td>";
			Content += "<td style='width:60px;text-align:right;font-size:15px;'>"+ OpenPO +"</td>";
			Content += "<td style='width:60px;text-align:right;background-color:#40739e; color:white;'><input type='text' po='"+ PO +"' ps='"+ PS +"' open_qty='"+ OpenPO +"' item='"+ Item +"' line='"+ Line +"'  qty='0' class='txtQty' value='' style='font-size:15px;background-color:transparent;width:70px;margin:0px;text-align:right;height:30px;border:thin white solid;padding:0px 3px;'></td>";
		Content += "</tr>";
		$('#tbodyContentAddItem tr:last').after(Content);
		$("#divAddItem").hide();
		$(".txtQty:last").focus();
	}
	selectRow = function(){
		$(".trDataPopUP").removeClass("selectRow");
		$(this).addClass("selectRow");
	}
	changePS = function(){
		var LPBCode = $(".trSelected").text();
		var PSPrev = $("#hddPS").val();
		var PS = $(this).val();
		var dataSend = "Status=7&LPBCode="+LPBCode+"&PS="+PS+"&PS_Old="+PSPrev;
		
		$.ajax({
			url : "/pc-app-lpb",
			type: "GET",
			data: dataSend,
			crossDomain: true,
			cache: false,
			success: function(response)
			{
				$("#hddPS").val(PS);
				alert("Update Packing Slip Success !!");
			},
			error: function (request, status, error) {
				alert("Update Packing Slip Error !!");
				$("#txtPS").val(PSPrev);
			}
		}); 
		
	}
	changeEffDate = function(){
		var EffDatePrev = $("#hddEddDate").val();
		var Date = $("#txtEffDate").val().split("-");
		var LPBCode = $(".trSelected").text();
		var EffDate = Date[2]  +"-" + Date[1] + "-" + Date[0];
		var EffDate2 = Date[0]  +"-" + Date[1] + "-" + Date[2];
		
		var dataSend = "Status=6&LPBCode="+LPBCode+"&EffDate="+EffDate;
		 $.ajax({
			url : "/pc-app-lpb",
			type: "GET",
			data: dataSend,
			crossDomain: true,
			cache: false,
			success: function(response)
			{
				$("#hddEddDate").val(EffDate2);
				alert("Update Effdate Success !!");
			},
			error: function (request, status, error) {
				alert("Update Effdate Error !!");
				$("#txtEffDate").val(EffDatePrev);
			}
		}); 
		
	}
	showPopUp = function(){
		$("#divAddItem").show();
		var ListLine = "";
		$(".tdLine").each(function(){
			ListLine = ListLine + $(this).text() + ",";
		})
		ListLine = ListLine.slice(0,-1);
		
		var PO = $("#tdPO").text();
			
		var dataSend = "Status=3&PO="+PO+"&ListLine="+ListLine;
		 $.ajax({
			url : "/pc-app-lpb",
			type: "GET",
			data: dataSend,
			crossDomain: true,
			cache: false,
			success: function(response)
			{
				$("#tbodyAddItem").html(response);
			}
		}); 
		
	}
	changeQty = function(){
		var _this = this;
		var LPBCode = $(".trSelected").text();
		var PO = $(this).attr("po");
		var PS = $(this).attr("ps");
		var OpenQty = $(this).attr("open_qty");
		var ItemNumber = $(this).attr("item");
		var Line = $(this).attr("line");
		var OldQty = $(this).attr("qty").replace(/,/g,"");
		var NewQty = $(this).val().replace(/,/g,"");
		
		if(parseFloat(OpenQty) < parseFloat(NewQty)){
			$(_this).parents(".trData").css({"background-color": "#d63031", "color": "white"});
			$(_this).parents(".trData").find(".divItem").css({"background-color": "#d63031", "color": "white"});
		}
		else{
			$(_this).parents(".trData").removeAttr("style");
			$(_this).parents(".trData").find(".divItem").removeAttr("style");
		}
		var dataSend = "Status=2&LPBCode="+LPBCode+"&PO="+PO+"&Line="+Line+"&OldQty="+OldQty+"&NewQty="+NewQty+"&PS="+PS+"&ItemNumber="+ItemNumber;
		// alert(dataSend);
		$.ajax({
			url : "/pc-app-lpb",
			type: "GET",
			data : dataSend,
			async:false,
			success: function(response)
			{
				$(_this).attr("qty",NewQty);
				var bg = $(_this).css('background-color'); // store original background
				$(_this).css('background-color', '#10ac84'); //change second element background
				setTimeout(function() {
					$(_this).css('background', bg); // change it back after ...
					
				}, 2000); // waiting one second
				
				$(_this).val(formatNumber(NewQty));
				
			}
		});
	}
	closeHide = function(){
		$("#divHideApp").hide();
	}
	hideData = function(){
		$(".loading_bar").show();
		if($(".trSelected").text() !== undefined && $(".trSelected").text() != ""){
			var LPBCode = $(".trSelected").text();
			var PS = $("#hddPS").val();
			var Reason = $("#txtReason").val();
			var SuppCode = $("#hddSuppCode").val();
			var SuppName = $("#hddSuppName").val();
			var EffDate = $("#hddEddDate2").val();
			
			if(Reason.replace(/\s/g,"") != "")
			{
				var dataSend = "Status=8&LPBCode="+LPBCode+"&PS="+PS+"&Reason="+Reason+"&SuppCode="+SuppCode+"&SuppName="+SuppName;
				// alert(dataSend);
					$.ajax({
						url : "/pc-app-lpb",
						type: "GET",
						data : dataSend,
						async:false,
						success: function(response)
						{
							if(response == "")
							{
								alert("ID "+ LPBCode + " already hide !!");
								$("#divHideApp").hide();
								window.location = "app_lpb.php?s="+SuppCode+"&d="+EffDate;
							}
							else
							{
								alert(response);
							}
						}
					});
			}
			else
			{
				alert("Please entry the reason !!");
			}
		}
		
		$(".loading_bar").hide();
	}
	HideApproval = function(){
		$("#txtReason").val("");
		setTimeout(function() {
		 $("#txtReason").focus();
		}, 0);
		$("#divHideApp").show();
	}
	checkApprovalPO = function(PO){
		var returnStatus = false;
		var dataSend = "Status=9&PO="+PO;
		$.ajax({
			url : "/pc-app-lpb",
			type: "GET",
			data : dataSend,
			async:false,
			success: function(response){
				if(parseInt(response) > 0)
					returnStatus = true;
			}
		});
		
		return returnStatus;
	}
	Approval = function(){
		$(".loading_bar").show();
		if($(".trSelected").text() !== undefined && $(".trSelected").text() != ""){
			var LPBCode = $(".trSelected").text();
			var Supplier = $("#hddSupp").val();
			var Date = $("#hddDate").val();
			var curPO = $("#tdPO").html();
			if(checkApprovalPO(curPO)){
				$(".trNewData").each(function(){
					var PO = $(this).find(".txtQty").attr("po");
					var Line = $(this).find(".txtQty").attr("line");
					var ItemNumber = $(this).attr("item");
					var Desc = encodeURIComponent($(this).attr("desc"));
					var Type = encodeURIComponent($(this).attr("type"));
					var Loc = $(this).attr("loc");
					var Qty = $(this).find(".txtQty").val().replace(/,/g,"");
					var UM = $(this).attr("um");
					var Memo = $(this).attr("memo");
					var PMCode = $(this).attr("pm_code");
					var PP = $(this).attr("pp");
					var PPDesc = encodeURIComponent($(this).attr("pp_desc"));
					var PartType = encodeURIComponent($(this).attr("part_type"));
					
					var dataSend = "Status=4&LPBCode="+LPBCode+"&PO="+PO+"&Line="+Line+"&ItemNumber="+ItemNumber+"&Desc="+Desc+"&Type="+Type+"&Loc="+Loc+"&UM="+UM+"&Memo="+Memo+"&PMCode="+PMCode+"&Qty="+Qty+"&PP="+PP+"&PPDesc="+PPDesc+"&PartType="+PartType;
					$.ajax({
						url : "/pc-app-lpb",
						type: "GET",
						data : dataSend,
						async:false,
						success: function(response){
							// alert(response);
						}
					});
				})
				var dataSend = "Status=1&LPBCode="+LPBCode;
				$.ajax({
					url : "/pc-app-lpb",
					type: "GET",
					data : dataSend,
					async:false,
					dataType:'json',
					success: function(response){
						if(response.status == "Success"){
							if(response.rc == ""){
								var RC_Ulang = "";
								for(var i=1;i<=100;i++){
									if(RC_Ulang == ""){
										RC_Ulang = checkRC(LPBCode,i);
									}
								}
								alert("LPB Code : " + LPBCode + " already approved !!\rStatus RC/BF : Success\r\rNo RC : "+ RC_Ulang );
								window.location = "app_lpb.php?s="+Supplier+"&d="+Date;
							}else{
								alert("LPB Code : " + LPBCode + " already approved !!\rStatus RC/BF : " + response.status +"\r\rNo RC : "+ response.rc );
								window.location = "app_lpb.php?s="+Supplier+"&d="+Date;
							}
						}else{
							alert("LPB Code : " + LPBCode + " already approved !!\rStatus RC/BF : " + response.status +"\r\rNo RC : "+ response.rc + "\r\r"+ response.message );
							window.location = "app_lpb.php?s="+Supplier+"&d="+Date;
						}
					}
				});
			}else{
				alert("Approval Gagal !! PO Belum di approve sampai Manager PRC sehingga tidak bisa dibuat LPB !! ");
			}
		}else{
			alert("No Data Approved !!");
		}

		$(".loading_bar").hide();
	}
	
	checkRC = function(LPBCode,i){
		var RC_Ulang = "";
		var dataSend = "Status=1b&LPBCode="+LPBCode+"&i="+i;
			$.ajax({
				url : "/pc-app-lpb",
				type: "GET",
				data : dataSend,
				async:false,
				success: function(response)
				{
					RC_Ulang = response;
				}
			});
		return RC_Ulang;
	}
	
	LPBDetail = function(){
		var LPB = $(this).text();
		window.location = "app_lpb.php?l="+LPB;
	}

	thumbClick = function(){
		var src = "/detail-img-lpb/"+$(this).attr("src_simple");
		$(".imgThumb").removeClass("imageSelected");
		$(this).addClass("imageSelected");
		$("#imgbox").attr("src",src);
	}
	formatNumber = function(num) {
	  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
	}
	</script>
	<style>
	.divItem
	{
		font-size:12px;
		font-weight:bold;
		font-family:sans-serif;
	}
	.txtQty:focus
	{
		background-color:white!important;
		color:black!important;
	}
	.btnItem
	{
		padding:5px;
		width:100px;
		float:right;
		margin-right:10px;
	}
	#divAddItem
	{
		position:fixed;
		top:0px;
		leftr:0px;
		height:100vh;
		width:100%;
		 background: rgba(76, 175, 80, 0.3);
		z-index:9999999;
	}
	#ContentItem
	{
		margin-top:50px;
		width:500px;
		height:100px;
		background-color:white;
	}
	#tdHeaderPopUp
	{
		background-color:#3d3d3d;
		color:white;
		font-size:35px;
		text-align:left;
		vertical-align:middle;
		font-weight:bold;
		padding:5px;
		padding-left:15px;
		width:500px;
	}
	#trHeaderPopUPData tr,#trHeaderPopUPData td
	{
		border:none;
	}
	.tdHeaderPopUPData
{
	
	text-align:center;
	font-size:20px;
	vertical-align:middle;
	font-family:sans serif;
	font-weight:bold;
	background-color:#34ace0;
	color:#3d3d3d;
	padding:10px!important;
	
}
#divAddItemDetail
{
	width:100%;
	height:400px;
	overflow-y:scroll;
}
.tableContent2
{
	
	margin:0 auto;
	width:100%;
	border-collapse:collapse;
	
}
.btnSubmit
{
	font-size:40px;
	font-family:sans serif;
	font-weight:bold;
	text-align:center;
	vertical-align:middle;
	background-color:#34ace0;
	color:#f1f2f6;
	height:60px;
	width:100%;
	padding:6px;
	border-radius:0px 0px 0px 5px;
	border:none;
	margin:0px;
	border-top:thin #bbbbbb solid;
}
.btnCancel
{
	background-color:#f1f2f6;
	color:#34ace0;
	border-radius:0px 0px 5px 0px;
}
.imgWHS
{
	width:13px;
	height:16px;
	margin-left:-16px!important;
	margin-top:2px;
}
.tdLine,.tdPopUp_Line,.tdPopUp_PO
{
	font-size:22px;
	text-align:center;
	font-weight:bold;
	color:white;
	background-color:#227093;
	width:40px;
	padding:5px 0px;
	border:none!important;
}
.tdPopUp_PO
{
	background-color:#10ac84!important;
	border-bottom:thin #eeeeee solid!important;
}
.tdPopUp_Line
{
	border-bottom:thin #eeeeee solid!important;
}
.divLocDetail,.divPopUP_LocDetail
{
	margin-top:-18px;
}
.divItem,.divPopUP_Item
{
	font-size:15px;
	font-weight:bold;
	color:#227093;
	
}
.tdDesc,.tdPopUp_Desc
{
	position:relative;
	font-size:24px;
	padding:5px 5px;
	font-family:sans serif;
	border:none!important;
	vertical-align:middle;
	height:50px;
}
.divLocDetail,.divPopUP_LocDetail
{
	margin-top:-18px;
}
.divLocation,.divPopUP_Location
{
	position:absolute;
	top:3px;
	right:10px;
	font-size:13px;
	font-weight:bold;
	color:#6D214F;
	
}
#tbodyAddItem
{
	background-color:white;
}
.selectRow
{
	background-color:#ffeaa7!important;
}
	</style>
	</head>
	<?php
	// $Cond = "";
	// if(ISSET($_GET['s']))
	// {
	// 	$Supplier = $_GET['s'];
	// 	$Date = $_GET['d'];
	// 	$Cond = "LEFT(supp_code,6)='".$Supplier."' AND eff_date='".$Date."' AND  ";
	// }
	// else if(ISSET($_GET['l']))
	// {
	// 	$LPBCode = $_GET['l'];
	// 	$Cond = "A.lpb_code='".$LPBCode."' AND  ";
	// }
	
	$query = DB::SELECT("SELECT  TOP 1 A.lpb_code,A.trans_id, A.invoice, B.po,B.packing_slip, A.supp_code, A.supp_name, A.eff_date, A.eff_time, A.delivery_note FROM elpb_mstr A
		INNER JOIN elpb_dtl B ON A.lpb_code=B.lpb_code
		WHERE supp_code ='".$supplier."' AND eff_date='".$date."' AND  status_approval='Not Approved' AND status_qc in ('Need Check','Finished','Not Check') ORDER BY A.lpb_code");
	foreach($query as $row){
		$LPBCode = $row->lpb_code;
		$TransID = $row->trans_id;
		$Invoice = $row->invoice;
		$SuppCode = $row->supp_code;
		$SuppName = $row->supp_name;
		$PO = $row->po;
		$PS = $row->packing_slip;
		$DN = $row->delivery_note;
		$EffDate = $row->eff_date;
		$EffTime = $row->eff_time;
		$Date = $EffDate;
		$Supplier = substr($SuppCode,0,6);
		
		$TransID_Invoice = $TransID;
		if($TransID == "") $TransID_Invoice = $Invoice;
	}
	?>
<body style='width:100%;'>
<div id="divAddItem" >
<center>
	<div id='ContentItem'>
	<table class='tableContent2'>
						<tr>
							<td colspan='4' id='tdHeaderPopUp'>ADD ITEM</td>
						</tr>
						<tr id='trHeaderPopUPData'>
							<td  class='tdHeaderPopUPData' style='width:15%; border:thin black solid'>LINE</td>
							<td  class='tdHeaderPopUPData' style='width:85%'>ITEM</td>
						</tr>
						<tbody >
							<tr>
								<td colspan='3' style='background-color:white;'>
									<div id='divAddItemDetail'>
										<table class='tableContent2'>
											<tbody style='width:100%;' id='tbodyAddItem'>
											</tbody>
										</table>
									</div>
								</td>
							</tr>
						</tbody>
						<tr>
							<td colspan='4' style='background-color:transparent;'>
								<table  class='tableContent2'>
									<tr><td style='width:50%;background-color:transparent;'>
								<input type='submit' value='CANCEL' class='btnSubmit btnCancel' id='btnCancel'>
								</td><td style='width:50%;background-color:transparent;'>
								
								<input type='submit' value='SELECT' class='btnSubmit' id='btnSelectItem'>
								</td></tr></table>
							</td>
						</tr>
						
					</table>
	</div>
</center>

</div>
<div class="loading_bar" >
	<span>
	<img src="{{ asset('akebono_asset/loading.gif') }}" >
	<center>
	<h2>Please wait ...</h2>
	</center>
	</span>
</div>
	<table class='table' style='height:100vh;'>
		<tr>
			<td class='tdHeader'>LPB CODE</td>
			<td class='tdHeader' id='tdHeader'>TRANS ID</td>
			<td class='tdHeader'>PURCHASE ORDER</td>
			<td class='tdHeader'>PACKING SLIP</td>
			<td class='tdHeader' style='width:200px;' >LIST LPB</td>
		</tr>
		<tr>
			<td class='tdData'><?php echo  $LPBCode; ?></td>
			<td class='tdData'><?php echo  $TransID_Invoice; ?></td>
			<td class='tdData' id='tdPO'><?php echo  $PO; ?></td>
			<td class='tdData'>
			<input type='text' id='txtPS' value='<?php echo $PS; ?>' style='width:470px;font-size:40px ' >
			<input type='hidden' id='hddPS' value='<?php echo $PS; ?>' style='width:400px; ' >
			</td>
			<td class='' rowspan='3' style='vertical-align:top;'>
				<div class='trListLPB'>
				<table class='table table2'>
					<?php
					
					$query4 = DB::SELECT("SELECT  TOP 1 A.lpb_code,A.supp_code, A.supp_name, A.eff_date FROM elpb_mstr A
						WHERE supp_code ='".$supplier."' AND status_approval='Not Approved' ORDER BY A.lpb_code");
					foreach($query4 as $row4){
						$LPBCode = $row4->lpb_code;
						$SuppCode = $row4->supp_code;
						$EffDate = $row4->eff_date;
						$SuppName = $row4->supp_name;
						$Supplier = substr($SuppCode,0,6);
						$Date = $EffDate;
					}
					$query3 = DB::SELECT("SELECT lpb_code FROM elpb_mstr WHERE LEFT(supp_code,6)='".$Supplier."' AND eff_date='".$Date."' AND  status_approval='Not Approved' AND status_qc in ('Need Check','Finished','Not Check') ORDER BY lpb_code");
					
					foreach($query3 as $row3){
						$Selected = "";
						if($LPBCode === $row3->lpb_code){
							$Selected ="trSelected";
						}
						
						echo "<tr class='trData2 $Selected'>";
							echo "<td class='trLPB'>".$row3->lpb_code."</td>";
						echo "</tr>";
					}
					?>
				</table>
				</div>
			</td>
		</tr>
		<tr>
			<td class='tdHeader' colspan='2'>SUPPLIER</td>
			<td class='tdHeader'>DELIVERY NOTE</td>
			<td class='tdHeader'>EFFECTIVE DATE</td>
		</tr>
		<tr>
			<td class='tdData' style='border-bottom:none;font-size:28px;'  colspan='2'><?php echo  $SuppCode ." - " .$SuppName; ?></td>
			<td class='tdData' style='border-bottom:none;'><?php echo  $DN; ?>
			<input type='hidden' id='hddEddDate' value='<?php echo date("d-m-Y",strtotime($EffDate)); ?>' >
			<input type='hidden' id='hddEddDate2' value='<?php echo date("Y-m-d",strtotime($EffDate)); ?>' >
			<input type='hidden' id='hddSuppCode' value='<?php echo substr($SuppCode,0,6); ?>' >
			<input type='hidden' id='hddSuppName' value='<?php echo $SuppName; ?>' >
			<input type='hidden' id='hddEddTime' value='<?php echo $EffTime; ?>' >
			</td>
			<td class='tdData' id='tdDate' style='border-bottom:none;'>
			<input type='text' id='txtEffDate' value='<?php echo date("d-m-Y",strtotime($EffDate)); ?>' >
			<input type='text' id='txtEffTime' value='<?php echo $EffTime; ?>' disabled> 
		</tr>
		<tr>
			<td colspan='5' class='tdImage'  style='border:none;'>
				<table class='table table2'>
					<tr>
						<td class='tdImageList' style=''>
							<center><div class='listImage'><div style='margin:5px;'>LIST IMAGE</div>
								
								<?php
								$i=1;
								$query_foto = DB::SELECT("SELECT * FROM elpb_mstr_image WHERE lpb_code='".$LPBCode."' ORDER by image_name");
								foreach($query_foto as $qf){
									$Selected ="";
                                
									if($i == 1)
									{
										$Selected ="imageSelected";
										$Image = $qf->image_name;
									}
									echo "<img  src='http://webapi.akebono-astra.co.id/LPB/lpb_image/".$qf->image_name."' src_simple='".$qf->image_name."' class='imgThumb $Selected'>";
									$i++;
								}
								?>
								
							</div></center>
						</td>
						<td style='width:45%;text-align:center; vertical-align:middle;background-color:white;vertical-align:top;'>
							<center>
								<iframe frameborder="1"  width="100%" style='height:450px; padding:0px;'
										    src="/detail-img-lpb/'.<?php echo base64_encode($Image); ?>" 
											name="imgbox" id="imgbox"> 
								</iframe>
							</center>
						</td>
						<td style='width:45%;background-color:#cccccc;vertical-align:top;border:thin #dddddd solid;'>
							<div class='divDetail'>
							<table class='table'>
								<tr>
									<td class='tdHeaderDetail' colspan='7'> DATA DETAIL <input type='submit' value='ADD ITEM' class='btnItem'> </td>
								</tr>
								<tr>
									<td class='tdHeaderDetail' style='text-align:center;color:white;'> ITEM</td>
									<td class='tdHeaderDetail' style='text-align:center;color:white;'> UM</td>
									<td class='tdHeaderDetail' style='width:50px;text-align:center;color:white;padding:3px;'> LINE</td>
									<td class='tdHeaderDetail' style='text-align:center;color:white;width:60px;padding:3px;'> PS</td>
									<td class='tdHeaderDetail' style='text-align:center;color:white;width:60px;padding:3px;'> NG</td>
									<td class='tdHeaderDetail' style='text-align:center;color:white;width:60px;padding:3px;'> OPEN</td>
									<td class='tdHeaderDetail' style='text-align:center;color:white;width:60px;padding:3px;'> OK</td>
									
								</tr>
								<tbody id='tbodyContentAddItem'>
								<?php
								$i=1;
								$QtyChangeApproval = array();
								$ListOpenPO = array();
								$ListOpenPO = get_open_po($PO,0);
								
								$ecqa = DB::SELECT("SELECT po, line,old_qty FROM
										(
											SELECT ROW_NUMBER() OVER(PARTITION BY po, line ORDER BY created_date) RowID, * FROM elpb_change_qty_approval  WHERE lpb_code='".$LPBCode."'
										) B WHERE RowID=1");
							
								$a = 0;
								foreach($ecqa as $ec){
									$PO = $ec->po;
									$Line = $ec->line;
									$OldQty = $ec->old_qty;
									$QtyChangeApproval[$a++] = $OldQty;
								}
						
								$dtl = DB::SELECT("SELECT * FROM elpb_dtl WHERE lpb_code='".$LPBCode."' AND ISNULL(qty_received,0) > 0");
								$z = 0;
								$qq= 0;
								foreach($dtl as $d){
						
									$PO = $d->po;
									$Line = $d->line;
									$OldQty = $QtyChangeApproval[$z++];

									if($OldQty == "")
									$OldQty = $d->qty_after_qc;
									$QtyAfterQC = $d->qty_after_qc;
									$QtyOpen = $ListOpenPO[$qq++]['ct_pod_qty_open'];
									// dd($QtyOpen);
									$bColor = "";
									if($QtyOpen < $QtyAfterQC)
										$bColor = "Background-color:#d63031;color:white";
									echo "<tr class='trData' style='$bColor'>";
									echo "<td style='width:200px;font-size:15px;'><div class='divItem' style='$bColor'>". strtoupper($d->item_number)."</div>".$d->item_desc." <b>".$d->type."</b></td>";
									echo "<td style='text-align:center; width:50px;font-size:15px;' >".$d->um."</td>";
									echo "<td style='text-align:center;background-color:#feca57;color:black;width:50px;font-size:15px;' class='tdLine'>".$d->line."</td>";
									
									echo "<td style='text-align:right;width:60px;font-size:15px;'>".number_format($d->qty_received)."</td>";
									$hitung = $d->qty_received - $QtyAfterQC;
									echo "<td style='width:60px;text-align:right;font-size:15px;'>". number_format($hitung) ."</td>";
									echo "<td style='width:60px;text-align:right;font-size:15px;' class='tdQtyOpen'>".number_format($QtyOpen)."</td>";
									echo "<td style='width:60px;text-align:right;background-color:#40739e; color:white;'>
									<input type='text' po='".$d->po."' open_qty='".$QtyOpen."'  ps='".$d->packing_slip."' item='".$d->item_number."' line='".$d->line."'  qty='".$d->qty_after_qc."' class='txtQty' value='".number_format($d->qty_after_qc)."' style='font-size:15px;background-color:transparent;width:70px;margin:0px;text-align:right;height:30px;border:thin white solid;padding:0px 3px;'></td>";
									echo "</tr>";
								}
								?>
								</div>
							</table>
							</div>
							<div id='divParentApp'>
							<table style='margin:0 auto;width:100%;'><tr><td><div class='divHide'><center>HIDE</center></div></td><td>
							<?php
							// $EffDate = "2020-12-30";
							// $EffTime = "07:31";
							// if(date("Y-m-d",strtotime($EffDate)) . " " . $EffTime > '2020-12-30 07:30')
							// {
							// }
							// else
							// {
							?>
							<div class='divApp'><center>APPROVE</center></div>
							<?php
							// }
							?>
							</td></tr></table></div>
						</td>
					</tr>
				</table>
<input type='hidden' id='hddSupp' value='<?php echo $Supplier; ?>'>				
<input type='hidden' id='hddDate' value='<?php echo $Date; ?>'>				
			</td>
		</tr>
		
	</table>
	<div id='divHideApp'>
		<div id='divReason'>
			<div id='divHeaderReason'>REASON HIDE APPROVAL <img id='imgClose' src='image/close.png'></div>
			<textarea id='txtReason'>
			</textarea>
			<input type='submit' value='HIDE' id='btnHide'>
			
		</div>
	</div>
</body>
</html>