@extends('templates.main')

@section('title', 'Vendor Performance Report')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <h6 class="main-content-label mb-3">VPR (Vendor Performance Report)</h6>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-2">Periode</div>
                        <div class="col-md-1 mb-2">
                            <select name="tgl" id="tgl" class="form-control">
                                <option value=""></option>
                                <option value="01">1</option>
                                <option value="02">2</option>
                                <option value="03">3</option>
                                <option value="04">4</option>
                                <option value="05">5</option>
                                <option value="06">6</option>
                                <option value="07">7</option>
                                <option value="08">8</option>
                                <option value="09">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select name="thn" id="thn" class="form-control">
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-1">
                            <button class="btn btn-success">Search</button>
                        </div>
                    </div>
                    <div class="container secondtable mt-3">
                        <button type="button" onclick="export_excel()" id="export_data" class="btn ripple btn-outline-success btn-sm btn-icon mb-2 btnlist" data-toggle="tooltip" data-original-title="Download Excel"><i class="si si-cloud-download"></i></button>
                        <br>
                    </div>
                    <br>
                    <div class="table-responsive mb-4 mt-3 secondtable">
                        <table class="table table-striped table-sm table-bordered text-center text-nowrap w-100" id="tablenya">
                        {!! $tablenya !!}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">

            
            $(document).ready(function() {
                $('#tablenya').DataTable({
                    scrollY:        "300px",
                    scrollX:        true,
                    scrollCollapse: true,
                    paging:         false,
                    search : true,
                    fixedColumns:   {
                        leftColumns: 1,
                    }
                });

                $('#export_data').hide();
               
            });
            function search(){
                getLoader();
                var item  = $("#item").val();
                var po    = $("#po").val();
                var bulan = $("#bulan").val();
                var tahun = $("#tahun").val();
                if(bulan == ""){
                    SW.warning({
                        message: "Month Can't Empty",
                        
                    });
                }else if(tahun == ""){
                    SW.warning({
                        message: "Year Can't Empty",
                        
                    });
                }else{
                    $.ajax({
                        type: "GET",
                        url: "/generate-achievement",
                        data: {
                            item: item,
                            po: po,
                            bulan: bulan,
                            tahun: tahun,
                        },
                        success: function(data) {
                            // $('#how').DataTable().destroy();
                            $('#item').val("");
                            $('#po').val("");
                            $('.secondtable').find('#how').html(data);
                            // $('#tablenya').DataTable({
                            //     scrollY:        "300px",
                            //     scrollX:        true,
                            //     scrollCollapse: true,
                            //     paging:         false,
                            //     fixedColumns:   {
                            //         leftColumns: 1,
                            //     }
                            // });
                        },
                        error: function(error) {
                            alert('Data is not Defined!!!');
                        }
                    });
                }
                   
            }
    </script>
@endsection