@php
    $currentRouteName = Route::currentRouteName();
@endphp

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
		<meta name="description" content="Akebono Astra Supplier Portal">
		<meta name="author" content="Majapahit Teknologi">
		<meta name="keywords" content="Akebono Astra Supplier Portal">
        <meta name="base-url" content="{{url('/')}}/">
        <meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- Title -->
        <title>@yield('title') - Akebono Astra</title>

        @include('templates.css')
	</head>
    @if ($currentRouteName == 'login')
        @php
            $classBody = '';
            $idBody = '';
        @endphp
    @else
        @php
            $classBody = 'main-body leftmenu';
            $idBody = '';
        @endphp
    @endif
	<body id="{{ $idBody }}" class="{{ $classBody }}">
        @if ($currentRouteName == 'login')
            @yield('body')
        @else
            <!-- Loader -->
            <div id="global-loader">
                <img id="loading-image" src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/img/loader.svg" class="loader-img" alt="Loader">
            </div>
            <!-- End Loader -->

            {{-- loader get data --}}
            <div class="preloader d-none">
                <div class="loading">
                    <img src="{{ asset('img/main/loading/loading.gif') }}" width="150">
                    <p class="text-center">waiting to retrieve data</p>
                </div>
            </div>
            {{-- end loader get data --}}

            <!-- Page -->
            <div class="page">
                @include('templates.sidemenu')

                @include('templates.header')

                <!-- Main Content-->
                <div class="main-content side-content pt-0">
                    <div class="container-fluid">
                        <div class="inner-body">

                            @yield('body')
                        </div>
                    </div>
                </div>
                <!-- End Main Content-->

                @include('templates.footer')
            </div>
            <!-- End Page -->

            <!-- Back-to-top -->
            <a href="#top" id="back-to-top"><i class="fe fe-arrow-up"></i></a>
        @endif

		@include('templates.js')
        @include('sweetalert::alert')
	</body>
</html>
