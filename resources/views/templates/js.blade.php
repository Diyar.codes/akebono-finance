<!-- Jquery js-->
<!-- <script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/jquery/jquery.min.js"></script> -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

@if ($currentRouteName == 'login')

@else

<!-- Bootstrap js-->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<!-- Internal Chart.Bundle js-->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/chart.js/Chart.bundle.min.js"></script>

<!-- Peity js-->
<!-- <script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/peity/jquery.peity.min.js"></script> -->

<!-- Select2 js-->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/select2/js/select2.min.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/js/select2.js"></script>

<!-- Internal Daternagepicker js-->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Internal Datepicker js -->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/model-datepicker/js/datepicker.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/model-datepicker/js/main.js"></script>


<!-- New Datatable -->
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js"></script>
<script src="https://cdn.datatables.net/fixedcolumns/3.3.2/js/dataTables.fixedColumns.min.js"></script>

<!-- Internal Data Table js -->
<!-- <script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/jquery.dataTables.min.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/dataTables.responsive.min.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/fileexport/dataTables.buttons.min.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/fileexport/buttons.bootstrap4.min.js"></script> -->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/fileexport/jszip.min.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/fileexport/pdfmake.min.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/fileexport/vfs_fonts.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/fileexport/buttons.html5.min.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/fileexport/buttons.print.min.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/fileexport/buttons.colVis.min.js"></script>
<!-- <script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/js/table-data.js"></script> -->

<!-- Perfect-scrollbar js -->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

<!-- Sidemenu js -->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/sidemenu/sidemenu2.js"></script>

<!-- Sidebar js -->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/sidebar/sidebar.js"></script>

<!-- Internal Morris js -->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/raphael/raphael.min.js"></script>
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/morris.js/morris.min.js"></script>

<!-- Circle Progress js-->
<!-- <script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/js/circle-progress.min.js"></script> -->
<!-- <script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/js/chart-circle.js"></script>  -->

<!-- Internal Dashboard js-->
<!-- <script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/js/index.js"></script> -->

<!-- Sticky js -->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/js/sticky.js"></script>

<!-- Modal js-->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/js/modal.js"></script>

<!-- Custom js -->
<script src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/js/custom.js"></script>

@endif

{{-- sweet alert2 --}}
{{-- exsweetalert2 --}}
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <!-- <script>
        function myFunction() {
            var input, filter, ul, li, a, i;
            input = document.getElementById("mySearch");
            filter = input.value.toUpperCase();
            ul = document.getElementById("myMenu");
            li = ul.getElementsByTagName("li");
            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName("a")[0];
                if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
                } else {
                li[i].style.display = "none";
                }
            }
        }
    </script> -->
    <script>
        // $('.searchinput').keypress(function() {
        //     var dInput = this.value;
        //     console.log(dInput);
        // });
    </script>

{{-- js custom me --}}
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/lib/swal.js') }}"></script>
{{-- md5 hash for jquery --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/md5.js"></script>
@if (!empty($currentRouteName))
    <script src="{{ asset('js/' . explode('.', $currentRouteName)[0] . '.js') }}"></script>
@endif

@yield("scripts")
