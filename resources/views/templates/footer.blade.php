<!-- Main Footer-->
<div class="main-footer text-center">
    <div class="container">
        <div class="row row-sm">
            <div class="col-md-12">
                <span>Copyright &copy; {{ date('Y') }} <a href="https://akebono-astra.co.id/">Akebono Astra</a> All rights reserved.</span>
            </div>
        </div>
    </div>
</div>
<!--End Footer-->