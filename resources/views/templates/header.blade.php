<!-- Main Header-->
<div class="main-header side-header sticky" id="fixed">
    <div class="container-fluid">
        <div class="main-header-left">
            <a class="main-header-menu-icon" href="#" id="mainSidebarToggle"><span></span></a>
        </div>
        <div class="main-header-right">
            <div class="dropdown d-md-flex">
                <a class="nav-link icon full-screen-link" href="">
                    <i class="fe fe-maximize fullscreen-button fullscreen header-icons"></i>
                    <i class="fe fe-minimize fullscreen-button exit-fullscreen header-icons"></i>
                </a>
            </div>
            <div class="dropdown main-profile-menu">
                <a class="d-flex" href="#">
                    <span class="main-img-user" ><img alt="avatar" src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/img/users/avatar.png"></span>
                </a>
                <div class="dropdown-menu">
                    <div class="header-navheading">
                        <h6 class="main-notification-title">{{ Auth::guard('pub_login')->user()->username }}</h6>
                        <p class="main-notification-text">{{ Auth::guard('pub_login')->user()->previllage }}</p>
                    </div>
                    {{-- <a class="dropdown-item border-top" href="/edit-profile">
                        <i class="fe fe-user"></i> Change Profile
                    </a> --}}
                    <a class="dropdown-item border-top" href="/edit-password">
                        <i class="fe fe-lock"></i> Change Password
                    </a>
                    <a class="dropdown-item" id="buttonLogout" style="color: black">
                        <i class="fe fe-power"></i> Sign Out
                    </a>
                </div>
            </div>
            <button class="navbar-toggler navresponsive-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
                aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fe fe-more-vertical header-icons navbar-toggler-icon"></i>
            </button><!-- Navresponsive closed -->
        </div>
    </div>
</div>
<!-- End Main Header-->

<!-- Mobile-header -->
<div class="mobile-main-header">
    <div class="mb-1 navbar navbar-expand-lg  nav nav-item  navbar-nav-right responsive-navbar navbar-dark  ">
        <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
            <div class="d-flex order-lg-2 ml-auto">
                <div class="dropdown main-header-notification flag-dropdown">
            </div>
            <div class="dropdown main-profile-menu">
                <a class="d-flex" href="#">
                    <span class="main-img-user" ><img alt="avatar" src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/img/users/avatar.png"></span>
                </a>
                <div class="dropdown-menu">
                    <div class="header-navheading">
                        <h6 class="main-notification-title">{{ Auth::guard('pub_login')->user()->username }}</h6>
                        <p class="main-notification-text">{{ Auth::guard('pub_login')->user()->previllage }}</p>
                    </div>
                    {{-- <a class="dropdown-item border-top" href="/edit-profile">
                        <i class="fe fe-user"></i> Change Profile
                    </a> --}}
                    <a class="dropdown-item border-top" href="/edit-password">
                        <i class="fe fe-lock"></i> Change Password
                    </a>
                    <a class="dropdown-item" id="buttonLogoutt" style="color: black">
                        <i class="fe fe-power"></i> Sign Out
                    </a>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- Mobile-header closed -->
