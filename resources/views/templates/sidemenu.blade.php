{{-- <style type="text/css">
    <style>
* {
  box-sizing: border-box;
}

body {
  font: 16px Arial;
}

/*the container must be positioned relative:*/
.autocomplete {
  position: relative;
  display: inline-block;
}

input {
  border: 1px solid transparent;
  background-color: #1d212f;
  padding: 10px;
  font-size: 16px;
}

input[type=text] {
  background-color: #7776;
  width: 100%;
}

input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
  cursor: pointer;
}

.autocomplete-items {
  position: absolute;
  border: 1px solid #1d212f;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}

.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #1d212f;
  border-bottom: 1px solid #1d212f;
}

/*when hovering an item:*/
.autocomplete-items div:hover {
  background-color: #1d212f;
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
  background-color: DodgerBlue !important;
  color: #ffffff;
}
</style> --}}


<!-- Sidemenu -->
<div class="main-sidebar main-sidebar-sticky side-menu">
    <div class="sidemenu-logo" style="background-color:white">
        <a class="main-logo" href="#">
            <img src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/img/brand/logo-light.png" class="header-brand-img desktop-logo" alt="logo">
            <img src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/img/brand/icon-light.png" class="header-brand-img icon-logo" alt="logo">
            <img src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/img/brand/logo.png" class="header-brand-img desktop-logo theme-logo" alt="logo">
            <img src="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/img/brand/icon.png" class="header-brand-img icon-logo theme-logo" alt="logo">
        </a>
    </div>
    <div class="main-sidebar-body">
        <ul class="nav" id="myMenu">
            {{-- <li class="nav-item">
                <div class="autocomplete">
                <a class="nav-link" href="#"><i class="fe fe-search sidemenu-icon"></i><input type="text" class="form-control" name="myCountry" id="myInput" style="background:transparent;border:none;color:white;"></a>
                </div>
            </li> --}}

            {{-- <li class="nav-item">
                <div class="input-group">
                    <div class="form-outline">
                      <input type="search" id="form1" class="form-control" />
                      <label class="form-label" for="form1"></label>
                    </div>
                    <button type="button" class="btn border-0">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
            </li> --}}

            <li class="nav-item @if ('dashboard' == explode('.', $currentRouteName)[0]) active @endif">
                <a class="nav-link" href="{{ route('dashboard') }}"><span class="shape1"></span><span class="shape2"></span><i class="ti-home sidemenu-icon"></i><span class="sidemenu-label">Dashboard</span></a>
            </li>

            <?php
                $sidemenu = DB::table('sidemenu')->get();
            ?>

            <?php foreach ($sidemenu as $item) :
                $roleArray = preg_split("/[,]/",$item->role);
                if (in_array(Auth::guard('pub_login')->user()->previllage, $roleArray)) : ?>
                    <li class="nav-item">
                        <a class="nav-link with-sub" href="{{ $item->route }}"><span class="shape1"></span><span class="shape2"></span><i class="{{ $item->icon }}"></i><span class="sidemenu-label">{{ $item->name }}</span><i class="angle fe fe-chevron-right"></i></a>
                        <?php
                            $submenu = DB::table('submenu')
                                    ->where('id_menu', '=', $item->id)
                                    ->get();
                        ?>

                        {{-- @php --}}
                            // $submenuSelect = DB::table('submenu')
                            //         ->get();
                            // $a = [];
                            // foreach ($submenuSelect as $value) {
                            //     $a[] = $value->nama;
                            // }
                            // dd($a);
                            // $b = json_encode($a);
                            // $c = json_decode($b);
                            // dd($submenuSelect);
                            // dd(json_decode($b));
                            // dd(json_encode($a));
                        {{-- @endphp --}}
                            {{-- <p>{{ $submenuSelect }}</p> --}}
                            {{-- <input type="text" name="DiseaseDiagnosed[]" value="{{ $a }}"> --}}
                            {{-- <p>{{ json_encode($a) }}</p> --}}
                        {{-- @php --}}
                        {{-- @endphp --}}

                        <?php foreach ($submenu as $items) :
                        $subRoleArray = preg_split("/[,]/",$items->role);
                        if (in_array(Auth::guard('pub_login')->user()->previllage, $subRoleArray)) : ?>
                            <ul class="nav-sub">
                                <li class="nav-sub-item @if ($items->rute === '/' . Request::path())active @endif">
                                    <a class="nav-sub-link" href="{{ $items->rute }}">{{ $items->nama }}</a>
                                </li>
                            </ul>
                        <?php endif ?>
                        <?php endforeach ?>
                    </li>
                <?php endif ?>
            <?php endforeach ?>
        </ul>
    </div>
</div>

{{-- <textarea name="" id="coyyy" cols="30" rows="10">{{ json_encode($a) }}</textarea> --}}
{{-- <input type="hidden" id="coyyy" cols="30" rows="10" value="{{ json_encode($a, TRUE) }}"> --}}
{{-- <input type="hidden" id="coyyy" cols="30" rows="10" value="{{ $a }}"> --}}
{{-- <input type="hidden" id="coyyy" cols="30" rows="10" value="{{ $a->toJSON() }}"> --}}
{{-- <script type="text/javascript">
    function autocomplete(inp, arr) {
      /*the autocomplete function takes two arguments,
      the text field element and an array of possible autocompleted values:*/
      var currentFocus;
      /*execute a function when someone writes in the text field:*/
      inp.addEventListener("input", function(e) {
          var a, b, i, val = this.value;
          /*close any already open lists of autocompleted values*/
          closeAllLists();
          if (!val) { return false;}
          currentFocus = -1;
          /*create a DIV element that will contain the items (values):*/
          a = document.createElement("DIV");
          a.setAttribute("id", this.id + "autocomplete-list");
          a.setAttribute("class", "autocomplete-items");
          /*append the DIV element as a child of the autocomplete container:*/
          this.parentNode.appendChild(a);
          /*for each item in the array...*/
          for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
              /*create a DIV element for each matching element:*/
              b = document.createElement("DIV");
              /*make the matching letters bold:*/
              b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
              b.innerHTML += arr[i].substr(val.length);
              /*insert a input field that will hold the current array item's value:*/
              b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
              /*execute a function when someone clicks on the item value (DIV element):*/
              b.addEventListener("click", function(e) {
                  /*insert the value for the autocomplete text field:*/
                  inp.value = this.getElementsByTagName("input")[0].value;
                  /*close the list of autocompleted values,
                  (or any other open lists of autocompleted values:*/
                  closeAllLists();
              });
              a.appendChild(b);
            }
          }
      });
      /*execute a function presses a key on the keyboard:*/
      inp.addEventListener("keydown", function(e) {
          var x = document.getElementById(this.id + "autocomplete-list");
          if (x) x = x.getElementsByTagName("div");
          if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
          } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
          } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
              /*and simulate a click on the "active" item:*/
              if (x) x[currentFocus].click();
            }
          }
      });
      function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
      }
      function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
          x[i].classList.remove("autocomplete-active");
        }
      }
      function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
          if (elmnt != x[i] && elmnt != inp) {
            x[i].parentNode.removeChild(x[i]);
          }
        }
      }
      /*execute a function when someone clicks in the document:*/
      document.addEventListener("click", function (e) {
          closeAllLists(e.target);
      });
    }

    /*An array containing all the country names in the world:*/
    var countries = ["Transaction","View","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

    /*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
    autocomplete(document.getElementById("myInput"), countries);
</script> --}}
