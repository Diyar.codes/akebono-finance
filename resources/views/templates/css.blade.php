<!-- Favicon -->
<link rel="icon" href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/img/brand/favicon.png" type="image/x-icon"/>

{{-- font awesome --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />

@if ($currentRouteName == 'login')
    <link rel="stylesheet" href="{{ asset('css/auth/auth.css') }}">
@else
<!-- Bootstrap css-->
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>

<!-- daterange picker -->
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>

<!-- Icons css-->
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/web-fonts/icons.css" rel="stylesheet"/>
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/web-fonts/font-awesome/font-awesome.min.css" rel="stylesheet">
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/web-fonts/plugin.css" rel="stylesheet"/>

<!-- New Datatables -->
<link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/fixedheader/3.1.8/css/fixedHeader.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/fixedcolumns/3.3.2/css/fixedColumns.dataTables.min.css" rel="stylesheet">

<!-- Internal DataTables css-->
<!-- <link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/datatable/fileexport/buttons.bootstrap4.min.css" rel="stylesheet" /> -->

<!-- Style css-->
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/css/style.css" rel="stylesheet">
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/css/skins.css" rel="stylesheet">
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/css/dark-style.css" rel="stylesheet">
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/css/colors/default.css" rel="stylesheet">

<link href="" rel="stylesheet">


<!-- Color css-->
<link id="theme" rel="stylesheet" type="text/css" media="all" href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/css/colors/color.css">

<!-- Select2 css-->
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/select2/css/select2.min.css" rel="stylesheet">

<!-- Datepicker css -->
<link rel="stylesheet" href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/model-datepicker/css/datepicker.css">

<!-- Mutipleselect css-->
<link rel="stylesheet" href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/plugins/multipleselect/multiple-select.css">

<!-- Sidemenu css-->
<link href="{{ asset('spruha/HTML-LTR/vertical-Icontext') }}/../../assets/css/sidemenu/sidemenu2.css" rel="stylesheet">

{{-- main css --}}
<link rel="stylesheet" href="{{ asset('css/main/main.css') }}">
@endif
@yield("styles")
