<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<link rel="stylesheet" href="{{ asset('css/main/main.css') }}">
	<title>Approval</title>
	


	<script>				
		function cek_reject()
		{
			if(document.form1.desc_reject.value=='')
		    {
				alert('Please Fill in Remarks !');
				document.form1.desc_reject.focus();
				return false;
		    }
			if(document.form1.checkkecelakaan.value=='0')
		    {
				alert('Please Check Safety Result!');
				document.form1.accident.focus();
				return false;
		    }
			$('#reas').show();
		}
		
		function cek_approve()
		{
			if(document.form.status.value='Supplier' || document.form.status.value='SHE_MGR'){
				if(document.form1.checkkecelakaan.value=='0')
				{
					alert('Please Check Safety Result!');
					document.form1.accident.focus();
					return false;
				}
			}
		}
	</script>
	<script language="javascript">		
		$(document).ready(function(){
			$("#tr_reason").hide();
			$("#btn_cancel").hide();
			$("#btn_reject2").hide();
			
			$("#btn_reject").click(function(){
				$("#tr_reason").show();
				$("#btn_reject2").show();
				$("#btn_cancel").show();
				$("#btn_approve").hide();
				$(this).hide();
			});
			
			$("#btn_cancel").click(function(){
				$("#tr_reason").hide();
				$("#btn_approve").show();
				$("#btn_reject").show();
				$("#btn_reject2").hide();
				$(this).hide();
			});
			$('#kecelakaanlist').hide();
			$('#reas').hide();
	
			$("#accident").click(function(){
				if ($(this).is(':checked')) {
					$("#kecelakaanlist").show();
					$("#noaccident").prop("checked", false);
					$("#checkkecelakaan").val("1");
				}else{
					$("#kecelakaanlist").hide();
					$("#checkkecelakaan").val("0");
				}
			  
			});
			$("#noaccident").click(function(){
				if ($(this).is(':checked')) {
					$("#kecelakaanlist").hide();
					$("#accident").prop("checked", false);
					$("#checkkecelakaan").val("0");
				}		  
			});
			listKecelakaan(1,2);
			$("#btnAddBudget").bind("click",function(){
				addKecelakaan();
			});
			$("#kecelakaanlist").delegate(".imgDeleteBudget","click",deleteGridLain);	
		});	
		
		deleteGridLain = function()
		{
			var ID = $(this).attr("id").replace("imgDeleteBudget","");
			if(ID=="1"){
				alert("Can't delete data!");
			}else{
				if(confirm("Are you sure to delete this row ?")){
					$("#trKec"+ID).remove();
				}
			}
		}
		
		addKecelakaan = function()
		{
			var addRow = 1;
			var begin =  parseInt($(".txtkecelakaan:last").attr("id").replace("txtkecelakaan","")) + parseInt(1);
			var end = parseInt(begin) + parseInt(addRow);
			
			listKecelakaan(begin,end);
			return false;
		}
		
		listKecelakaan = function(begin,end)
		{
			var tBody="";
			
			for(var i =begin; i<end; i++)
			{	
				tBody="";
				tBody+="<tr id='trKec"+i+"'>";
				tBody+="<td width='20' style align='center' bgcolor='#FFFFFF' style='color:black;'>"+i+"</td>";
				tBody+="<td width='250px' style align='center' bgcolor='#FFFFFF' style='color:black;float:center;'><textarea class='inputField txtkecelakaan' name='txtkecelakaan[]' type='text' id='txtkecelakaan"+i+"' style='width:500px;' /></td>";
				tBody+="<td style align='center' bgcolor='#FFFFFF' style='color:black;float:center;'><img src='img/delete.png' width='20px' height='20px' class='imgDeleteBudget' id='imgDeleteBudget"+i+"'/></td>";
				tBody+="</tr>";
				$("#tbkecelakaan").append(tBody);			
			}
		}
	</script>
</head>
<body>
			{{-- loader get data --}}
            <div class="preloader" style="display:none">
                <div class="loading">
                    <img src="{{ asset('img/main/loading/loading.gif') }}" width="150">
                    <p class="text-center">Waiting to sending email</p>
                </div>
            </div>
            {{-- end loader get data --}}
	<div id="bodyContainer">
	    <div id="pageBodyTitle" style="text-align:center"> BAST System Approval</div>
			<form onsubmit='' name="form1" method="post" action="" enctype="multipart/form-data">
			    <center>
				    <table style="border: none;" width="80%">
				    <tr>
					    <td rowspan="4" class="">
						<?php

						// if($no_lpb!=""){
						// 	$updateStatus = mssql_query("UPDATE bast_mstr2 SET status='CLOSE' WHERE no_nbr_acr= '".$nbr_acara."'");
						// }	

                        if($step=="supplier"){	
                            $statusApp	= $get_bast->status_she2; //she
                            $dateApp	= $get_bast->app_she2; //she
                        }elseif($step=="she_mgr"){	
                            $statusApp	= $get_bast->status_she; //div user
                            $dateApp	= $get_bast->app_she; //she
                        }elseif($step=="sh_user" || $step == "she"){	
                            $statusApp	= $get_bast->status_user; //div user
                            $dateApp	= $get_bast->app_user; //she
                        }elseif($step=="mgr_user"){	
                            $statusApp	= $get_bast->status_mgr_user;
                            $dateApp	= $get_bast->app_mgr_user; //she
                        }elseif($step=="div_user"){	
                            $statusApp	= $get_bast->status_div_user;
                            $dateApp	= $get_bast->app_div_user; //she
                        }elseif($step=="sh_prc"){	
                            $statusApp	= $get_bast->status_sh_prc;
                            $dateApp	= $get_bast->app_sh_prc; //she
                        }elseif($step=="mgr_prc"){	
                            $statusApp	= $get_bast->status_mgr_prc;
                            $dateApp	= $get_bast->app_mgr_prc; //she
                        }		

                            $nbr_acara 		= $get_bast->no_nbr_acr;
                            $no_po 			= $get_bast->no_po;
                            $ref_penawaran 	= $get_bast->ref_penawaran;
                            $period 		= date("Y-m-d",strtotime($get_bast->period_kerja));
                            $period1 		= date("Y-m-d",strtotime($get_bast->period_kerja1));
                            $gr_pkrj 		= $get_bast->garansi_pekerjaan;
                            $nama    		= $get_bast->p_nama1;
                            $npr 			= $get_bast->nama_pr;
                            $alamat 		= $get_bast->alamat;
                            $Jabatan 		= $get_bast->jabatan;
                            $nama1 			= $get_bast->p_nama;
                            $jabatan1 		= $get_bast->jabatan1;
                            $po             = $get_bast->no_po;
                            // $qba 			= $get_bast->quantity_ba;
                            $note 			= $get_bast->note;
                            $kd_sp 			= $get_bast->kode_sup;
								
                            // $selItem 	= mssql_fetch_array(mssql_query("SELECT TOP 1 * FROM bast_det2 WHERE no_nbr_acr='".$nbr_acara."'"));
                            // $items		= $selItem['deskripsi;
								
                            if($get_bast->status_bast == NULL){
                                $st_bast		= "NEW";
                            }else{
                                $st_bast		= $get_bast->status_bast;
                            }

                            $desc_reject    = ' ';
								//checklist
                            $copy_po 		= $get_bast->copy_po;
                            $check 			= $get_bast->check_list;
                            $check1 		= $get_bast->check_list1;
                            $check2 		= $get_bast->check_list2;
                            $risk 			= $get_bast->risk;
                            $drawing 		= $get_bast->drawing;
                            $kesimpulan 	= $get_bast->kesimpulan;
                            $manual_book 	= $get_bast->manual_book;
                            $training 		= $get_bast->training;
								//detail po_nbr 
                            // $line			= $get_bast->line;
                            // $desc			= $get_bast->deskripsi;
                            // $satuan			= $get_bast->satuan;
                            // $qty_open		= $get_bast->quantity_po;	

                            $app_sup 		= date("Y-m-d H:m",strtotime($get_bast->app_sup));
                            if ($get_bast->app_sup == 'NULL'){
                                $app_sup ='';
                            }
                            $app_div_user = date("Y-m-d H:m",strtotime($get_bast->app_div_user));	
                            if ($get_bast->app_div_user == ''){
                                $app_div_user ='';
                            }
                            $app_mgr_prc = date("Y-m-d H:m",strtotime($get_bast->app_mgr_prc));	
                            if ($get_bast->app_mgr_prc == ''){
                                $app_mgr_prc ='';
                            }
								
                            $status_sup= $get_bast->status_sup;
                            if ($status_sup == 'OK'){
                                $status_sup ='Digitally Signed';
                            }else{
                                $status_sup =' ';
                            }
                            
                            $status_mgr_prc= $get_bast->status_mgr_prc;
                            if ($status_mgr_prc == 'OK'){
                                $status_mgr_prc ='Digitally Signed';
                            }else{
                                $status_mgr_prc =' ';
                            }
                            
                            $status_div_user= $get_bast->status_div_user;
                            if ($status_div_user == 'OK'){
                                $status_div_user ='Digitally Signed';
                            }else{
                                $status_div_user =' ';
                            }
							
                            $div_head 		= $get_bast->p_nama1;
                            $nama_section 	= $get_bast->jabatan1;							
						
					?>	
		<div style='float:right!important;font-size:20px;margin:20px;'><?php echo $st_bast?></div>
			<div style='text-align:center;!important;font-size:20px'><?php echo $npr?></div>
			    <div style='text-align:center;!important;font-size:14px'>BERITA ACARA SERAHTERIMA</div>	
					<table style='border: none;text-align:right;' class="noBorder">							
                        <tr >
                            <td class="noBorder" width='' style='text-align:left;' >No. Berita Acara</td>	
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'><?php echo $nbr_acara?></td>
                        </tr>							
                        <tr >
                            <td class="noBorder" width='' style='text-align:left;'>No. Purchase Order (PO)</td>	
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'><?php echo $no_po?></td>
                        </tr>								
                        <tr >
                            <td class="noBorder" width='' style='text-align:left;'>No. Ref. Penawaran</td>	
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'><?php echo $ref_penawaran?></td>
                        </tr>
                        <tr >
                            <td class="noBorder" width='' style='text-align:left;'>Periode Pekerjaan</td>	
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'><?php echo $period."&nbsp; s/d &nbsp;".$period1?></td>				
                        </tr>
                        <tr >
                            <td class="noBorder" width='' style='text-align:left;'>Garansi Pekerjaan</td>	
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'><?php echo $gr_pkrj?></td>
                        </tr>
						<tr>
							<td>
								<br/>
							</td>
						</tr>							
                        <tr>
                            <td class="noBorder" width='' style='font-style:bold;'><b>Yang bertandatangan dibawah ini</b></td>			
                        </tr>	
                        <tr>
                            <td width='' class="noBorder" style='text-align:left;'>Nama</td>
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'><?php echo strtoupper($nama1)?></td>
                        </tr>
                        <tr>
                            <td class="noBorder" width='' style='text-align:left;'>Nama Perusahaan</td>
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='300' style='text-align:left;'><?php echo $npr?></td>
                        </tr>
                        <tr>
                            <td class="noBorder" width='' style='text-align:left;'>Alamat</td>
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'><?php echo $alamat?></td>
                        </tr>
                        <tr>
                            <td class="noBorder" width='' style='text-align:left;'>Jabatan</td>
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'><?php echo $Jabatan?></td>
                        </tr>
						
                        <tr>
                            <td class="noBorder" width='' style='text-align:left;'>Selanjutnya disebut sebagai</td>
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'>Pihak Pertama</td>
                        </tr>
						<tr>
							<td>
								<br/>
							</td>
						</tr>
                        <tr>
                            <td class="noBorder" width='' style='text-align:left;'>Nama</td>
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'><?php echo strtoupper($div_head) ?></td>
                        </tr>
                        <tr>
                            <td class="noBorder" width='' style='text-align:left;'>Nama Perusahaan</td>
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'>PT. Akebono Brake Astra Indonesia</td>
                        </tr>
                        
                        <tr>
                            <td class="noBorder" width='' style='text-align:left;'>Alamat</td>
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='300' style='text-align:left;'>Jl. Pegangsaan Dua Bloak A1 Km. 1,6 Kelapa Gading Jakarta 14250</td>
                        </tr>
                            
                        <tr>
                            <td class="noBorder" width='' style='text-align:left;'>Jabatan</td>
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'><?php echo $jabatan1?></td>
                        </tr>
                        
                        <tr>
                            <td class="noBorder" width='' style='text-align:left;' >Selanjutnya disebut sebagai</td>
                            <td class="noBorder">:</td>
                            <td class="noBorder" width='' style='text-align:left;'>Pihak Kedua</td>
                        </tr> 
                    </table>
                    <br>
					<div type='text' style='text-align:left;'>Menerangkan bahwa telah selesai dilakukan pekerjaan (penulisan sesuai di PO) :</div><br/>
						<table border='1' cellpadding='0' cellspacing='0'  >
                            <tr>
                                <td width='50' style align='center' bgcolor='#FFFFFF' style='color:black;'>Line</td>
                                <td width='350' style align='center' bgcolor='#FFFFFF' style='color:black;'>Deskripsi</td>
                                <td width='60' style align='center' bgcolor='#FFFFFF' style='color:black;'>Satuan</td>
                                <td width='100' style align='center' bgcolor='#FFFFFF' style='color:black;'>Quantity PO</td>
                                <td width='100' style align='center' bgcolor='#FFFFFF' style='color:black;'>Quantity BA </td>
                            </tr>
                            <?php 
                            foreach($bast_det as $bd){
										$line		= $bd->line;
										$deskripsi 	= $bd->deskripsi;
										$satuan 	= $bd->satuan;
										$qty_open 	= $bd->quantity_po;
										$qba 		= $bd->quantity_ba; ?>
                                <tr>
                                    <td style align='center'><?php echo $line ?></td>
                                    <td style align='left'><?php echo wordwrap($deskripsi,70,"<br>\n"); ?></td> 
                                    <td style align='center'><?php echo $satuan ?></td>				
                                    <td style align='center' ><?php echo number_format($qty_open) ?></td>
                                    <td style align='center'><?php echo number_format($qba) ?></td>				
                                </tr>	
							<?php } ?>
						
							</table>
							<br>
							<table>
								<tr>
									<td class="noBorder"><label>Note:</label></td>
									<td class="noBorder"><u><?php echo $note ?></u></td>
								</tr>
							</table>
							<br>
							<p style="text-align:left">dengan rincian sebagaimana terlampir(beri tanda v) :</p>
								<?php
									if($copy_po == 'OK'){
										$copy_po  = "v";
									}else{
										$copy_po = "v";
									}
									
									if($check == 'OK'){
										$check = "v";
									}else{
										$check = "v";
									}
									
									if($check1 == 'OK'){
										$check1 = "v";
									}else{
										$check1 = "v";
									}
									
									if($check2 == 'OK'){
										$check2 = "v";
									}else{
										$check2 = "v";
									}
									
									if($risk == 'OK'){
										$risk = "v";
									}else{
										$risk = "v";
									}
									
									if($drawing == 'OK' || $kesimpulan == 'OK' || $manual_book == 'OK' || $training == 'OK'){
										$laincheck = "v";
									}else{
										$laincheck = "";
									}
								?>		
								<table cellspacing='0' cellpadding='0' class="noBorder">
									<tr>
										<td class="noBorder"><?php echo $copy_po ?></td>				
										<td class="noBorder" style="text-align:left">Copy Purchase Order</td>				
									</tr>
									<tr>					
										<td class="noBorder"><?php echo $check ?></td>
										<td class="noBorder" style="text-align:left">Check list item pekerjaan</td>
									</tr>
									<tr>					
										<td class="noBorder"><?php echo $check1 ?></td>
										<td class="noBorder" style="text-align:left">Foto sebelum dan sesudah pekerjaan</td>
									</tr>
									<tr>					
										<td class="noBorder"><?php echo $check2 ?></td>
										<td class="noBorder" style="text-align:left">Service Report</td>
									</tr>
									<tr>
										<td class="noBorder"><?php echo $risk ?></td>
										<td class="noBorder" style="text-align:left">Job Safety Analysis</td>
									</tr>
									<tr>					
										<td class="noBorder"><?php echo $laincheck ?></td>
										<td class="noBorder" style="text-align:left">Lain-lain</td>
									</tr>
												
								</table>	
									
						<br>
						<div type='text'>Demikian Berita Acara Serah Terima ini dibuat untuk menyatakan bahwa pekerjaan tersebut telah dapat diselesaikan sesuai dengan permintaan.</div>
							<br/>

							<table class="noBorder" cellpadding='0' cellspacing='0'>
								<tr>
									<td class="noBorder" width='250' style align='left' bgcolor='#FFFFFF'>Pihak Pertama <br/> yang menyerahkan,</td>
									<td class="noBorder" width='130' style align='center' bgcolor='#FFFFFF'></td>
									<td class="noBorder" width='250' style align='center' bgcolor='#FFFFFF'><br/><br/>Mengetahui,</td>
									<td class="noBorder" width='130' style align='center' bgcolor='#FFFFFF'></td>
									<td class="noBorder" width='250' style align='right' bgcolor='#FFFFFF'>Pihak Kedua <br/> yang menerima,</td>
								</tr>

								<tr>				
									<td class="noBorder" style align='left'>
										<br/><br/>
										<?php echo $status_sup?><br/>
										<?php echo $app_sup?><br>
										<!--<br/><font style='color:blue'>[Materai & tanda tangan]</font><br/>-->
										<br/><?php echo strtoupper($nama);?>
										<br/><?php echo $Jabatan?>
										
									</td>
									<td class="noBorder" width='130' style align='center' bgcolor='#FFFFFF'></td>
									<td class="noBorder" style align='center'>
										<br/><br/>
										<?php echo $status_mgr_prc?><br/>
										<?php echo $app_mgr_prc?>
										<br/><br/>
										<br/>Darmawan
										<br/>Purchasing
									</td>
									
									<td class="noBorder" style align='center'></td>
									<td class="noBorder" style align='right'>
										<br/><br/>
										<?php echo $status_div_user?><br/>
										<?php echo $app_div_user?>
										<br/><br/>
										<br/><?php echo strtoupper($nama1);?>
										<br/><?php echo $jabatan1?>
									</td>			
								</tr>
							</table>
							<p style='color:blue;text-align:left;'>PIC :<?php echo " ".$nama_section; ?></p><br>
							<i>This e-BAST is valid without sign (generated by system)</i><br><br>
							<?php
							foreach($checklist as $cks){
								$pooder		= $cks->doc1;
								$jsa		= $cks->doc2;
								$sereport	= $cks->doc3;
							}
							?>
							<hr style="width: 100%;height: 3px;margin-left: auto;margin-right: auto;background-color:#000000;border: 0 none;margin-top: 10px;margin-bottom:10px;">							
							<div style='text-align:center;!important;font-size:14px'>Copy Purchase Order</div><br>
							<?php								
								$GetMaxrevisi = DB::select("SELECT MAX(po_revisi) as revisi, month(po_app2_effdate) as bulan, year(po_app2_effdate) as tahun from po_trans_mstr where po_nbr='".$po."'
											 group by po_app2_effdate");

								$po_rev 	= $GetMaxrevisi[0]->revisi;
								$bulan 		= $GetMaxrevisi[0]->bulan;
								$tahun 		= $GetMaxrevisi[0]->tahun;
								$date 		= $bulan.$tahun;
								echo "<iframe src='https://purchasing.akebono-astra.co.id/po/".$date."/".$po."-".$po_rev.".pdf' width='100%' style='height:700px'></iframe>";
								
							?>
							<hr style="width: 100%;height: 3px;margin-left: auto;margin-right: auto;background-color:#000000;border: 0 none;margin-top: 10px;margin-bottom:10px;">							
							<div style='text-align:center;!important;font-size:14px'>Check list item pekerjaan</div>	
							
							<?php
									$tahun 	= date("Y");
									$bulan 	= date("m");
									$today 	= date("Y-m-d");
							?>
						</br>
						<center>
						<table id='messageTable' width='90%'>
								<tr>
									<td class="heading" width="5%" style="text-align:center">Jenis Pekerjaan</td>
									<td class="heading" width="5%" style="text-align:center">Area</td>
								</tr>
								
							<?php 
								$no =1;
								foreach($checklist as $cek){
									$idpkj = $cek->id;
									echo "<tr>
											<td width='5%' style='text-align:center'>".$cek->jenis_pekerjaan."</td>
											<td width='5%' style='text-align:center'>".$cek->area."</td>								
										</tr>";
									$no++;
								}
							?>
						</table>
						</center><br>
						<hr style="width: 100%;height: 3px;margin-left: auto;margin-right: auto;background-color:#000000;border: 0 none;margin-top: 10px;margin-bottom:10px;">							
						<div style='text-align:center;!important;font-size:14px'>Foto sebelum dan sesudah pekerjaan</div>	
						<br>
						<center>
						<table id='messageTable' width='90%'>
								<tr>
									<td class="heading" width="1%">No</td>									
									<td class="heading" width="5%" style="text-align:center">Lokasi</td>
									<td class="heading" width="15%" style="text-align:center">Uraian</td>									
									<td class="heading" width="5%" style="text-align:center">Foto Sebelum</td>
									<td class="heading" width="5%" style="text-align:center">Status</td>
									<td class="heading" width="5%" style="text-align:center">Foto Sesudah</td>
									<td class="heading" width="5%" style="text-align:center">Status</td>
									<td class="heading" width="15%" style="text-align:center">Keterangan</td>
								</tr>
								
							<?php 
								$qry2	= DB::table('detail_pekerjaan')
                                ->where('id_pekerjaan',$idpkj)->orderBy('id','DESC')->get();
								$n=1;
								foreach($qry2 as $r){
									echo "<tr>
											<td width='1%' style='text-align:center'>".$n."</td>
											<td width='1%' style='text-align:center'>".$r->lokasi."</td>
											<td width='1%' style='text-align:center'>".$r->uraian."</td>
											<td width='5%' style='text-align:center;color:blue;'>";
											
									if(substr($r->foto1,-4)=='.pdf'){
                                        $tampung_foto1 = url('images/bast/'.str_replace(' ', '', $r->foto1)); 
                // dd($tampung_foto1);
                                    echo '<a href="'.$tampung_foto1.'" target="_blank">
                                            <u>'.$r->foto1.'</u>										
                                    </a>';
									}else{
                                        $tampung_foto1 = url('images/bast/'.str_replace(' ', '', $r->foto1));
										echo '<a href="'.$tampung_foto1.'" target="blank">
                                        <img src="'.$tampung_foto1.'" style="filter: brightness(120%);width:300px;"/>
													<br>Klik to zoom image
											  </a>';
                                        
									}
										echo "</td>
											<td width='1%' style='text-align:center'>".$r->status."</td>
											<td width='5%' style='text-align:center;color:blue;'>";				
									if(substr($r->foto2,-4)=='.pdf'){
                                        
                                        $tampung_foto2 = url('images/bast/sesudah/'.str_replace(' ', '', $r->foto2));
										echo '<a href="'.$tampung_foto2.'" target="_blank">
													<u>'.$r->foto2.'</u>										
                                            </a>';
									}else{
                                        $tampung_foto2 = url('images/bast/sesudah/'.str_replace(' ', '', $r->foto2));
										echo '<a href="'.$tampung_foto2.'" target="blank">
                                        <img src="'.$tampung_foto2.'" style="filter: brightness(120%);width:300px;"/>
													<br>Klik to zoom image
											  </a>';
									}
										echo "</td>
											<td width='5%' style='text-align:center'>".$r->status2."</td>
											<td width='25%' style='text-align:center;'>".$r->ket."</td>";									
									echo "</tr>";
									$n++;
								}
							?>
						</table>
							<hr style="width: 100%;height: 3px;margin-left: auto;margin-right: auto;background-color:#000000;border: 0 none;margin-top: 10px;margin-bottom:10px;">							
							<div style='text-align:center;!important;font-size:14px'>Service Report</div>	
							<?php
								if(substr($sereport,-4)=='.pdf'){
									// $ur = 'https://docs.google.com/gview?url='.url("images/bast/service/'.$sereport).'"&embedded=true';
									echo '<embed src="'.url("images/bast/service/".$sereport).'" type="application/pdf" height="500px" width="100%">';
								}else{
									echo '<br>
											<a href="'.url("images/bast/service/".$sereport).'" target="_blank">
												<img src="'.url("images/bast/service/".$sereport).'" style="filter: brightness(130%);width:800px";height:1000px  />
											</a>
										<br><font style="font-size:10px;color:red;">Click to zoom</font>';
								}
							?>
							<hr style="width: 100%;height: 3px;margin-left: auto;margin-right: auto;background-color:#000000;border: 0 none;margin-top: 10px;margin-bottom:10px;">							
							<div style='text-align:center;!important;font-size:14px'>Job Safety Analysis</div>	
							<?php
                                $url_jsa = url("images/bast/jsa/".$jsa);
								if(substr($jsa,-4)=='.pdf'){
									echo '<embed src="'.$url_jsa.'" type="application/pdf"   height="500px" width="100%">';
								}else{
									echo "<br><a href='".$url_jsa."' target='_blank'>
												<img src='".$url_jsa."' style='filter: brightness(130%);width:800px';height: 1000px  />
											  </a>
											  <br><font style='font-size:10px;color:red;'>Click to zoom</font>";
								}
							?>
							<br>
							<hr style="width: 100%;height: 3px;margin-left: auto;margin-right: auto;background-color:#000000;border: 0 none;margin-top: 10px;margin-bottom:10px;">							
							
								<table id="messageTable" align="center">		
								<?php				
									//}			
									echo "<tr><td colspan='5' style='border: none;'><br></td></tr>";
									if($step == "FINISH"){
												
									}else{
									if($statusApp== null && $step != "FINISH")
										{ ?>								
											<tr class="reas" id="tr_reason">
												<td colspan="2"  class="heading" type='text' id='reject_id'><font>Reject Reason</font></td>										
												<td colspan="3" ><input type='text' class='inputField' size='45' name='desc_reject' id='desc_reject' value='<?php echo $desc_reject ?>'></td>
											</tr>
											<?php 
											if($step=="supplier" || $step=="she_mgr"){
											echo "<tr class='kecelakaan'>
													<td style='border: none;' colspan='5'>
													<br>
														<input type='hidden' name='status' id='status' value='".$step."'>
														<input type='checkbox' checked='checked' value='OK' name='noaccident' id='noaccident'>
														&nbspTidak Terjadi Kecelakaan / Abnormality
														</input>
													</td>
												</tr>
												<tr class='kecelakaan'>
													<td colspan='5' style='border: none;'>
														<input type='hidden' name='checkkecelakaan' id='checkkecelakaan'>
														<input type='checkbox' name='accident' id='accident'>
														&nbspTerjadi Kecelakaan / Abnormality, Yaitu :
														</input>
													</td>
												</tr>
												<tr id='kecelakaanlist'>
													<td colspan='5' style='border: none;'>
													<table>
													<tr>
														<td class='heading' style='text-align:center;'>No</td>
														<td class='heading' style='text-align:center;'>Uraian Kecelakaan</td>
														<td class='heading' style='text-align:center;'>Action</td>
													</tr>
													<tbody id='tbkecelakaan'>
													</tbody>
													<tr>
													<td colspan='3'>
														<input type='button' id='btnAddBudget' value='Add Row' style='text-align:center;float:center;' class='formButton2'>
													</td>
													</tr>
													</table>
													</td>										
												</tr>";
												
												echo "<tr>
														<td colspan='2' class='heading' type='text' id='noteshe'><font>SHE Note</font></td>										
														<td colspan='2'><textarea type='text' class='inputField' name='noteshe' style='width:350px;' id='shenotenew'></textarea></td>
													</tr>";
											}
											?>
											
										<tr><td style="border: none;" colspan="5"><br></td></tr>
										<tr>
											<td colspan="5" style="align:center;float:center; border: none;">
											<center>
                                                <input type="hidden" id="bast_hidden" value="<?= base64_encode($nbr_acara) ?>">
                                                <input type="hidden" id="step_hidden" value="<?= base64_encode($step) ?>">
												<input type="button" class="formButton2" value="Approve" onclick="approve()" />
												<input type="button" id="btn_reject" value="Reject" onclick="reject()" class="formButton2"/>
												<input type="button" id="btn_cancel" value="Cancel" class="formButton2"/>
				
											</center>
											</td>
										</tr>	
													
										<?php 
										}else{										
											echo "<td colspan='2'>";
											if($statusApp=="OK"){
												echo "<center><br/>Already Approved<br/>at ".$dateApp."</center><br>";
											}else{
												echo "<center><br/>Already Reject<br/>at ".$dateApp."</center><br>";
											}
											echo "</td>";
										}	
									}										
								?>				
								</table>
							</center>
						</td>
					</tr>
				</table>
				</center>
			</form>
		</div>
	</div>
	<div class="modal"><!-- this is element for ajax loading --></div>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>


    function approve(id,step) {
        var id      = document.getElementById("bast_hidden").value;
        var step    = document.getElementById("step_hidden").value;
        if(step == 'supplier' || step == 'she_mgr'){
            
            var acc     = document.getElementById("noaccident").value;
            var shenote = document.getElementById("shenotenew").value;

				if($('#noaccident').is(":checked")){
					swal({
                        title: "Warning",
                        text: "Please Check Safety Result !",
                        icon: "warning",
                        button: "Okey",
                    });
                    return false;
                }
		}else{
            swal({
                title: "Are you want to Approve this BAST ?",
                icon: "warning",
                buttons: true,
				
                })
                .then((willDelete) => {
                if (willDelete) {
					getLoader();
                    $.ajax({
                        type: "GET",
                        url: "/approval-bast",
                        data:{
                            id : id,
                            step :step,
                            shenote:shenote
                        },
                        success: function (data) {
                            swal({
                                title: "Approved Successfully !",
                                icon: "success",
                                button: "Oke",
                            });
                            setTimeout(function(){ 
                                location.reload(); 
                            }, 1000);
                        },
                        error: function(error) {
                            alert('Data is not Defined!!!');
                        }
                    });
                } else {
                    swal("Approve canceled");
                }
            });
        } 
    }

	
		function getLoader() {
			if($.ajax) {
				$(document).ajaxStart(function() {
					$(".preloader").removeClass('d-none');
					$(".preloader").fadeIn();
				});

				$(document).ajaxStop(function() {
					$(".preloader").addClass('d-none')
					$(".preloader").fadeOut();
				});
			}
		}

	function reject(id,step) {
        var id      = document.getElementById("bast_hidden").value;
        var step    = document.getElementById("step_hidden").value;
		// var reason  = document.getElementById("step_hidden").value;

		var acc     	= document.getElementById("noaccident").value;
		var reject_note = document.getElementById("desc_reject").value;

		if($('#noaccident').is(":checked")){
			// alert(reject_note);
			swal({
                title: "Are you want to Reject this BAST ?",
                icon: "warning",
                buttons: true,
                })
                .then((willDelete) => {
                if (willDelete) {
					getLoader();
                    $.ajax({
                        type: "GET",
                        url: "/reject-bast",
                        data:{
                            id : id,
                            step :step,
                            reject_note:reject_note
                        },
                        success: function (data) {
                            swal({
                                title: "Reject Successfully !",
                                icon: "success",
                                button: "Oke",
                            });
                            setTimeout(function(){ 
                                location.reload(); 
                            }, 1000);
                        },
                        error: function(error) {
                            alert('Data is not Defined!!!');
                        }
                    });
                } else {
                    swal("Reject canceled");
                }
            });
			
		}else{
			swal({
				title: "Warning",
				text: "Please Check Safety Result !",
				icon: "warning",
				button: "Okey",
			});
			return false;
		}      
    }


</script>
</html>