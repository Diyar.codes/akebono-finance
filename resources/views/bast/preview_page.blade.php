<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Preview BAST Formulir</title>
    <style>
        #background{
    position:absolute;
    z-index:0;
    background:transparent;
    display:block;
    min-height:50%; 
    min-width:50%;
    color:yellow;
}

#content{
    position:absolute;
    z-index:1;
}

#bg-text{
    color:lightgrey;
    font-size:150px;
    opacity: 0.4;
    transform:rotate(300deg);
    -webkit-transform:rotate(300deg);
}
    </style>
</head>
<body>
<div id="background">
  <p id="bg-text">Preview</p>
	</div>
    <div class="container">
    <div class="col-md-12">
    <div class="col-md-3">
    
    </div>
    <div class="col-md-6">
        <!-- PREVIEW START -->
<div id="preview_bast">
    <div class="container content">
        <div style="text-align:right;!important;font-size:20px;margin-right:30px;" id="preview_status_bast">{{ $data['status'] }}</div>
	    <div style='text-align:center;!important;font-size:20px' id="preview_npr"></div>
	    <div style='text-align:center;!important;font-size:18px'><b>BERITA ACARA SERAH TERIMA</b></div>	
		<br/>
	<table style='border: none;!important;' cellspacing='0' cellpadding='0'>		
		<tr >
			<td width = '' >No. Berita Acara</td>	
				<td>:</td>
			<td width = '' id="preview_nbr"> {{ $data['no_ba'] }}</td>
		</tr>
		
		<tr >
			<td width = '' >No. Purchase Order (PO)</td>	
				<td>:</td>
			<td width = '' id="preview_po">{{ $data['no_po'] }}</td>
		</tr>
			
		<tr >
			<td width = '' >No. Ref. Penawaran</td>	
				<td>:</td>
			<td width = '' id="preview_ref">{{ $data['no_ref'] }}</td>
		</tr>
		
		<tr >
			<td width = '' >Periode Pekerjaan</td>	
				<td>:</td>
			<td width = '' id="preview_periode">{{ $data['periode'] }} - {{ $data['to'] }}</td>				
		</tr>
			
		<tr >
			<td width = '' >Garansi Pekerjaan</td>	
				<td>:</td>
			<td width = '' id="preview_garansi">{{ $data['garansi'] }}</td>
		</tr>
		
		<tr>
			<td>
				<br/>
			</td>
		</tr>
		
		<tr>
			<td width = '' ><b>Yang bertandatangan dibawah ini</b></td>				
		</tr>
		<tr>
			<td>
				<p></p>
			</td>
		</tr>
		<tr>
			<td width='' >Nama</td>
			<td>:</td>
			<td width='' id="preview_nama1">{{ $data['nama1'] }}</td>
		</tr>
		
		<tr>
			<td width='' >Nama Perusahaan</td>
			<td>:</td>
			<td width='300' id="preview_perusahaan1">{{ $data['perusahaan1'] }}</td>
		</tr>
		
		<tr>
			<td width='' >Alamat</td>
			<td>:</td>
			<td width='' id="preview_alamat1">{{ $data['alamat1'] }}</td>
		</tr>
			
		<tr>
			<td width='' >Jabatan</td>
			<td>:</td>
			<td width='' id="preview_jabatan1">{{ $data['jabatan1'] }}</td>
		</tr>
		<tr>
			<td width='' >Selanjutnya disebut sebagai</td>
			<td>:</td>
			<td width='' id="preview_pihak1">Pihak Pertama</td>
		</tr>
		
        <tr>
			<td>
				<br/>
			</td>
		</tr>
		<tr>
			<td width='' >Nama</td>
			<td>:</td>
			<td width='' id="preview_nama2">{{ $data['nama2'] }}</td>
		</tr>
		
		<tr>
			<td width='' >Nama Perusahaan</td>
			<td>:</td>
			<td width=''>PT. Akebono Brake Astra Indonesia</td>
		</tr>
		
		<tr>
			<td width='' >Alamat</td>
			<td>:</td>
			<td width='300'>Jl. Pegangsaan Dua Bloak A1 Km. 1,6 Kelapa Gading Jakarta 14250</td>
		</tr>
			
		<tr>
			<td width='' >Jabatan</td>
			<td>:</td>
			<td width='' id="preview_jabatan2">{{ $data['jabatan2'] }}</td>
		</tr>
		
		<tr>
			<td width='' >Selanjutnya disebut sebagai</td>
			<td>:</td>
			<td width=''>Pihak Kedua</td>
		</tr> 
	</table>
		<br>
		<br>
		<div type='text'>Menerangkan bahwa telah selesai dilakukan pekerjaan <i>(penulisan sesuai di PO)</i> :</div><br/>
		<table border='1' cellpadding='0' cellspacing='0'  >
			<tr>
				<td width='30' style align='center' bgcolor='#FFFFFF' style='color:black;'>Line</td>
				<td width='420'style align='center' bgcolor='#FFFFFF' style='color:black;'>Deskripsi</td>
				<td width='50' style align='center' bgcolor='#FFFFFF' style='color:black;'>Satuan</td>
				<td width='50' style align='center' bgcolor='#FFFFFF' style='color:black;'>Quantity PO</td>
				<td width='50' style align='center' bgcolor='#FFFFFF' style='color:black;'>Quantity BA </td>

			 </tr>
            @foreach($isi_po as $p)
            <tr>
                <td style align='center'>{{ $p['line'] }}</td>							
                <td style align='left' size='7' >{{ $p['desc'] }}</td>		
                <td style align='center'>{{ $p['satuan'] }}</td>				
                <td style align='center' >{{ $p['qty_open'] }}</td>
                <td style align='center'>{{ $p['qba'] }}</td>				
            </tr>	
            @endforeach
		</table>
        <br/>
		
		<table>
			<tr>
				<td><label>Note:</label></td>
				<td><u>{{ $data['note'] }}</u></td>
			</tr>
		</table>
		<p type='text'>dengan rincian sebagaimana terlampir(beri tanda v) :</p>
					
			<table cellspacing='0' cellpadding='0' style="margin-top : -90px">
				<tr>
					<td>v &nbsp; Copy Purchase Order</td>				
				</tr>
				<tr>					
					<td>v &nbsp; Check list item pekerjaan</td><br/>
				</tr>
				<tr>					
					<td>v &nbsp; Foto sebelum dan sesudah pekerjaan</td><br/>
				</tr>
				<tr>					
					<td>v &nbsp; Service Report</td><br/>
				</tr>
				<tr>
					<td>v &nbsp; Job Safety Analysis</td><br/>
				</tr>
				<tr>					
					<td> &nbsp; Lain-lain</td><br/>
				</tr>
			</table>	
		<br/>	
		<div type='text'>Demikian Berita Acara Serah Terima ini dibuat untuk menyatakan bahwa pekerjaan tersebut telah dapat diselesaikan sesuai dengan permintaan.</div>
		<br/>

		<table class="noBorder" cellpadding='0' cellspacing='0'>
            <tr>
                <td width='250' class="noBorder" style align='left' bgcolor='#FFFFFF'>Pihak Pertama <br/> yang menyerahkan,</td>
                <td width='600' class="noBorder" style align='center' bgcolor='#FFFFFF'></td>
                <td width='140' class="noBorder" style align='right' bgcolor='#FFFFFF'>Pihak Kedua <br/> yang menerima,</td>
            </tr>

            <tr>				
                <td style align='left' class="noBorder">
                    <br/><br/><br/>
                    <!-- <font id="preview_status_supp"></font><br/>
                    <font id="preview_app_supp"></font><br/> -->
                    <font id="preview_nama_supp">{{ $data['nama1'] }}</font><br/>
                    <font id="preview_jabatan_supp">{{ $data['jabatan1'] }}</font><br/>
                    
                </td>
                
                <td style align='center' class="noBorder"></td>
                <td style align='right' class="noBorder">
                    <br/><br/><br/>
                    <!-- <font id="preview_status_user"></font><br/>
                    <font id="preview_app_user"></font><br/> -->
                    <font id="preview_nama_user">{{ $data['nama2'] }}</font><br/>
                    <font id="preview_jabatan_user">{{ $data['jabatan2'] }}</font><br/>
                </td>
                            
            </tr>
        </table>
		
		<br/><br/>
		<font style='color:blue'>PIC : <font color="blue" id="preview_section"></font> </font><br><br>
		<i>This e-BAST is valid without sign (generated by system)</i><br>
    </div>
    </div>
<!-- PREVIEW END -->
    </div>
    <div class="col-md-3">
    
    </div>
    </div>
    </div>
</body>
</html>
