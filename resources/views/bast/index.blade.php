@extends('templates.main')

@section('title', 'Add BAST')

@section('body')
<div class="inner-body">
    <div class="row row-sm my-md-2 mt-5">
        
    </div>
<form action="/save-bast-data" method="POST" enctype="multipart/form-data" id="upload_form">
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <div class="row row-sm">
        <div class="col-lg-12 col-md-12">
            <div class="card custom-card">
                <div class="card-body">
                    <div>
                        <h6 class="main-content-label mb-1">@yield('title')</h6>
                        <p class="text-muted card-sub-title"><font color="blue">PORTAL BERITA ACARA SERAH TERIMA</font></p>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>No Berita Acara : </b></label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" name="no_ba" id="no_ba" value="{{ $Ba_nomor }}" style="color:black" readonly>
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>No Purchase Order (PO) : </b></label>
                        </div>
                        <div class="col-md-5 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" name="no_po" id="no_po" style="color:black" readonly>
                        </div>
                        <div class="col-md-3 mg-t-5 mg-md-t-0">
                            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#modaldemo8part2">List</a>
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Status BAST : </b></label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <select name="status" id="status" class="form-control" style="color:black">
                                <option value="">Pilih Status BAST</option>
                                <option value='NEW'>NEW</option>
                                <option value='REVISION'>REVISION</option>
                                <option value='RESEND'>RESEND</option>
                            </select>
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>No Ref Penawaran : </b></label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" name="no_ref" id="no_ref" style="color:black">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Periode Pekerjaan : </b></label>
                        </div>
                        <div class="col-md-3 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="date" name="periode" id="periode" style="color:black">
                        </div>
                        <div class="col-md-1 mg-t-5 mg-md-t-0">
                            <label class="mg-b-0"><b>to : </b></label>
                        </div>
                        <div class="col-md-3 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="date" name="to" id="to" style="color:black">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Garansi Pekerjaan : </b></label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" name="garansi" id="garansi" style="color:black">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row row-sm">
        <div class="col-lg-6 col-md-12">
            <div class="card custom-card">
                <div class="card-body">
                    <div>
                        <h6 class="main-content-label mb-1">Yang bertanda tangan dibawah ini : </h6>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Nama : </b></label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" placeholder="(Input Nama Direktur)" name="nama1" id="nama1" style="color:black">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Nama Perusahaan : </b></label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" name="perusahaan1" id="perusahaan1" style="color:black" value="{{ $data_supp->ct_ad_name }}">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Alamat : </b></label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <textarea class="form-control" type="text" name="alamat1" id="alamat1" style="color:black" value="{{ $data_supp->ct_ad_line1 }}">{{ $data_supp->ct_ad_line1 }}</textarea>
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Jabatan : </b></label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" placeholder="(Direktur)" name="jabatan1" id="jabatan1" style="color:black">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-5">
                            <label class="mg-b-0"><b>Selanjutnya disebut sebagai : </b></label>
                        </div>
                        <div class="col-md-7 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" name="pihak1" id="pihak1" value="Pihak Pertama" style="color:black">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="card custom-card">
                <div class="card-body">
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Nama : </b></label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
        
                            <input class="form-control" type="text" name="nama2" id="nama2" style="color:black">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Nama Perusahaan : </b></label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" name="perusahaan2" id="perusahaan2" style="color:black" value="PT.Akebono Brake Astra Indonesia">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Alamat : </b></label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <textarea class="form-control" type="text" name="alamat2" id="alamat2" style="color:black" value="Jl. Pegangsaan Dua Blok A1 Km. 1,6 Kelapa Gading Jakarta 14250">Jl. Pegangsaan Dua Blok A1 Km. 1,6 Kelapa Gading Jakarta 14250</textarea>
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Jabatan : </b></label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" name="jabatan2" id="jabatan2" style="color:black" value="Division Head">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-5">
                            <label class="mg-b-0"><b>Selanjutnya disebut sebagai : </b></label>
                        </div>
                        <div class="col-md-7 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" name="pihak2" id="pihak2" value="Pihak Kedua" style="color:black">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <div class="row row-sm">
        <div class="col-lg-12">
            <div class="card custom-card">
                <div class="card-body">
                    <div>
                        <p class="text-muted card-sub-title"><font color="black">Menerangkan bahwa telah selesai dilakukan pekerjaan (<i>penulisan sesuai di PO</i>) : </font></p>
                    </div>
                    <div class="table-responsive secondtable">
                        <table class="table table-bordered text-center" id="list_pengerjaan" width="100%">
                            <thead>
                                <tr>
                                    <th class="wd-10p">Line</th>
                                    <th class="wd-25p">Deskripsi</th>
                                    <th class="wd-15p">Satuan</th>
                                    <th class="wd-15p">Quantity-PO</th>
                                    <th class="wd-15p">Quantity-BA</th>
                                    <th class="wd-20p">Action</th>
                                </tr>
                            </thead>
                            <tbody id="result">
                                
                            </tbody>
                        </table>
                    </div>
                    <br/>
                    <font color="red">PERHATIAN!!!! <br/> Setelah Quantity-BA dimasukkan. Centang pada CHECK BOX  untuk menambahkan detail data!</font> <br/>
                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->
    <div class="row row-sm">
        <div class="col-lg-12 col-md-12">
            <div class="card custom-card">
                <div class="card-body">
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Note : </b></label>
                        </div>
                        <div class="col-md-4 mg-t-5 mg-md-t-0">
                            <textarea class="form-control" type="text" name="note" id="note" style="color:black"></textarea>
                        </div>
                    </div>
                    <div>
                        <p class="text-muted card-sub-title"><font color="black">Dengan rincian sebagaimana terlampir (<i>beri tanda checklist</i>) : </font></p>
                    </div>
                    <div class="row row-sm mg-b-10">
                        <div class="col-lg-12">
                            <label class="ckbox"><input checked type="checkbox"><span class="tx-13">Copy Purchase Order</span></label>
                        </div>
                    </div>
                    <div class="row row-sm mg-b-10">
                        <div class="col-lg-12">
                            <label class="ckbox"><input checked type="checkbox"><span class="tx-13">Check List item pekerjaan & foto sebelum dan sesudah pekerjaan</span></label>
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Jenis Pekerjaan : </b></label>
                        </div>
                        <div class="col-md-4 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" name="jenis" id="jenis" style="color:black">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-10">
                        <div class="col-md-4">
                            <label class="mg-b-0"><b>Area : </b></label>
                        </div>
                        <div class="col-md-4 mg-t-5 mg-md-t-0">
                            <input class="form-control" type="text" name="area" id="area" style="color:black">
                        </div>
                    </div>
                    <div class="control-group after-add-more mb-2">
                        <table class="table table-bordered text-center" id="list_lokasi" width="100%">
                            <thead>
                                <tr>
                                    <th class="wd-10p">Lokasi</th>
                                    <th class="wd-17p">Uraian Pekerjaan</th>
                                    <th class="wd-13p">Status</th>
                                    <th class="wd-10p">Foto Sebelum</th>
                                    <th class="wd-10p">Foto Sesudah</th>
                                    <th class="wd-15p">Keterangan</th>
                                    <th class="wd-13p">Status</th>
                                    <th class="wd-5p">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="tbody">
                                <tr>
                                    <input type="hidden" name="hitung" id="hitung" value="1">
                                    <td><input class="form-control" type="text" name="lokasi[]" id="lokasi[]" style="color:black"></td>
                                    <td><textarea class="form-control" type="text" name="uraian[]" id="uraian[]" style="color:black"></textarea></td>
                                    <td>
                                        <select name="status1[]" id="status1[]" class="form-control" style="color:black">
                                            <option value="Finished">Finished</option>
                                            <option value="Unfinished">Unfinished</option>
                                        </select>
                                    </td>
                                    <td><input class="form-control" type="file" name="foto_sebelum[]" id="foto_sebelum[]"></td>
                                    <td><input class="form-control" type="file" name="foto_sesudah[]" id="foto_sesudah[]"></td>
                                    <td><textarea class="form-control" type="text" name="keterangan[]" id="keterangan[]" style="color:black"></textarea></td>
                                    <td>
                                        <select name="status2[]" id="status2[]" class="form-control" style="color:black">
                                            <option value="Finished">Finished</option>
                                            <option value="Unfinished">Unfinished</option>
                                        </select>
                                    </td>
                                    <td></td>            
                                </tr>
                            </tbody>
                        </table>
                        <div class="d-flex flex-row-reverse">
                            <button class="btn btn-success add-more" id="addRow" type="button">
                                <i class="fas fa-plus"></i> Add Item
                            </button>
                        </div>
                    </div>
                    <div class="row row-sm mg-b-10">
                        <div class="col-lg-12">
                            <label class="ckbox"><input type="checkbox" id="sr"><span class="tx-13">Service Report</span></label>
                        </div>
                    </div>
                    <div class="table-responsive sr">
                        <table class="table table-bordered text-center" id="sr" width="100%">
                            <thead>
                                <tr>
                                    <th class="wd-30p">File/gambar Service Report</th>
                                    <th class="wd-70p">Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input class="form-control" type="file" name ="service_img" id="service_img" style="color:black"></td>
                                    <td><textarea class="form-control" type="text" name ="service_ket" id="service_ket" style="color:black"></textarea></td>         
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row row-sm mg-b-10">
                        <div class="col-lg-12">
                            <label class="ckbox"><input type="checkbox" id="jsa"><span class="tx-13">Job Safety Analysis</span></label>
                        </div>
                    </div>
                    <div class="table-responsive jsa">
                        <table class="table table-bordered text-center" id="jsa" width="100%">
                            <thead>
                                <tr>
                                    <th class="wd-30p">File/gambar JSA</th>
                                    <th class="wd-70p">Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input class="form-control" type="file" name ="jsa_img" id="jsa_img" style="color:black"></td>
                                    <td><textarea class="form-control" type="text" name ="jsa_ket" id="jsa_ket" style="color:black"></textarea></td>         
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <input type="hidden" name="no_pp" id="no_pp">
                    <div class="row row-sm mg-b-10">
                        <div class="col-lg-12">
                            <label class="ckbox"><input type="checkbox"><span class="tx-13">Lain-lain</span></label>
                        </div>
                    </div>

                    <div class="form-group row justify-content-end mb-0">
                        <div class="col-md-8 pl-md-2">
                            <button class="btn ripple btn-success pd-x-30 mg-r-5" type="submit" name="btn_sub" value="save_btn">Save BAST</button>
                            <button class="btn ripple btn-info pd-x-30 mg-r-5" type="button" id="preview_bast_form">Preview</button>
                            <!-- <a href="javascript:;" class="btn ripple btn-info pd-x-30"  id="preview_bast_button">Preview</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<!-- PO Modal -->
<!-- Modal effects -->
<div class="modal" id="modaldemo8part2">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table-bordered text-center" id="list_po" width="100%">
                        <thead class="thead">
                            <?php $i = 1; ?>
                           
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">PO Number</th>
                                <th scope="col">Date</th>
                            </tr>
                            
                        </thead>
                        <tbody class="tbodyselect">
                            @foreach($getpo as $p)
                            <?php
                            if($p['kode_supplier'] == $supp && $p['po_status'] != 'c'){ $po = $p['po_number'];
                                echo '
                                <tr>
                                    <td>'.$i++.'</td>
                                    <td><a href="#" onclick="select_po(\''.$po.'\')">'.$p['po_number'].'</a></td>
                                    <td>'.date('d-F-Y',strtotime($p['POdue_date'])).'</td>
                                </tr>';
                            } ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->

<!-- PREVIEW START -->
    <div id="preview_bast">
    <div class="container">
        <div style="text-align:right;!important;font-size:20px;margin-right:30px;" id="preview_status_bast"></div>
	    <div style='text-align:center;!important;font-size:20px' id="preview_npr"></div>
	    <div style='text-align:center;!important;font-size:18px'><b>BERITA ACARA SERAH TERIMA</b></div>	
		<br/>
	<table style='border: none;!important;' cellspacing='0' cellpadding='0'>		
		<tr >
			<td width = '' >No. Berita Acara</td>	
				<td>:</td>
			<td width = '' id="preview_nbr"></td>
		</tr>
		
		<tr >
			<td width = '' >No. Purchase Order (PO)</td>	
				<td>:</td>
			<td width = '' id="preview_po"></td>
		</tr>
			
		<tr >
			<td width = '' >No. Ref. Penawaran</td>	
				<td>:</td>
			<td width = '' id="preview_ref"></td>
		</tr>
		
		<tr >
			<td width = '' >Periode Pekerjaan</td>	
				<td>:</td>
			<td width = '' id="preview_periode"></td>				
		</tr>
			
		<tr >
			<td width = '' >Garansi Pekerjaan</td>	
				<td>:</td>
			<td width = '' id="preview_garansi"></td>
		</tr>
		
		<tr>
			<td>
				<br/>
			</td>
		</tr>
		
		<tr>
			<td width = '' ><b>Yang bertandatangan dibawah ini</b></td>				
		</tr>
		<tr>
			<td>
				<p></p>
			</td>
		</tr>
		<tr>
			<td width='' >Nama</td>
			<td>:</td>
			<td width='' id="preview_nama1"></td>
		</tr>
		
		<tr>
			<td width='' >Nama Perusahaan</td>
			<td>:</td>
			<td width='300' id="preview_perusahaan1"></td>
		</tr>
		
		<tr>
			<td width='' >Alamat</td>
			<td>:</td>
			<td width='' id="preview_alamat1"></td>
		</tr>
			
		<tr>
			<td width='' >Jabatan</td>
			<td>:</td>
			<td width='' id="preview_jabatan1"></td>
		</tr>
		<tr>
			<td width='' >Selanjutnya disebut sebagai</td>
			<td>:</td>
			<td width='' id="preview_pihak1">Pihak Pertama</td>
		</tr>
		
        <tr>
			<td>
				<br/>
			</td>
		</tr>
		<tr>
			<td width='' >Nama</td>
			<td>:</td>
			<td width='' id="preview_nama2"></td>
		</tr>
		
		<tr>
			<td width='' >Nama Perusahaan</td>
			<td>:</td>
			<td width=''>PT. Akebono Brake Astra Indonesia</td>
		</tr>
		
		<tr>
			<td width='' >Alamat</td>
			<td>:</td>
			<td width='300'>Jl. Pegangsaan Dua Bloak A1 Km. 1,6 Kelapa Gading Jakarta 14250</td>
		</tr>
			
		<tr>
			<td width='' >Jabatan</td>
			<td>:</td>
			<td width='' id="preview_jabatan2"></td>
		</tr>
		
		<tr>
			<td width='' >Selanjutnya disebut sebagai</td>
			<td>:</td>
			<td width=''>Pihak Kedua</td>
		</tr> 
	</table>
		<br>
		<br>
		<div type='text'>Menerangkan bahwa telah selesai dilakukan pekerjaan <i>(penulisan sesuai di PO)</i> :</div><br/>
		<table border='1' cellpadding='0' cellspacing='0'  >
			<tr>
				<td width='30' style align='center' bgcolor='#FFFFFF' style='color:black;'>Line</td>
				<td width='420'style align='center' bgcolor='#FFFFFF' style='color:black;'>Deskripsi</td>
				<td width='50' style align='center' bgcolor='#FFFFFF' style='color:black;'>Satuan</td>
				<td width='50' style align='center' bgcolor='#FFFFFF' style='color:black;'>Quantity PO</td>
				<td width='50' style align='center' bgcolor='#FFFFFF' style='color:black;'>Quantity BA </td>

			 </tr>
	
            <tr>
                <td style align='center'>line</td>							
                <td style align='left' size='7' >da</td>		
                <td style align='center'>asa</td>				
                <td style align='center' >as</td>
                <td style align='center'>as</td>				
            </tr>	
	
		</table>
        <br/>
		
		<table>
			<tr>
				<td><label>Note:</label></td>
				<td><u>note</u></td>
			</tr>
		</table>
		<br>
		<p type='text'>dengan rincian sebagaimana terlampir(beri tanda v) :</p>
					
			<table cellspacing='0' cellpadding='0'>
				<tr>
					<td>v &nbsp; Copy Purchase Order</td>				
				</tr>
				<tr>					
					<td>v &nbsp; Check list item pekerjaan</td><br/>
				</tr>
				<tr>					
					<td>v &nbsp; Foto sebelum dan sesudah pekerjaan</td><br/>
				</tr>
				<tr>					
					<td>v &nbsp; Service Report</td><br/>
				</tr>
				<tr>
					<td>v &nbsp; Job Safety Analysis</td><br/>
				</tr>
				<tr>					
					<td> &nbsp; Lain-lain</td><br/>
				</tr>
			</table>	
		<br/>	
		<div type='text'>Demikian Berita Acara Serah Terima ini dibuat untuk menyatakan bahwa pekerjaan tersebut telah dapat diselesaikan sesuai dengan permintaan.</div>
		<br/>

		<table class="noBorder" cellpadding='0' cellspacing='0'>
            <tr>
                <td width='250' class="noBorder" style align='left' bgcolor='#FFFFFF'>Pihak Pertama <br/> yang menyerahkan,</td>
                <td width='600' class="noBorder" style align='center' bgcolor='#FFFFFF'></td>
                <td width='140' class="noBorder" style align='right' bgcolor='#FFFFFF'>Pihak Kedua <br/> yang menerima,</td>
            </tr>

            <tr>				
                <td style align='left' class="noBorder">
                    <br/><br/><br/>
                    <!-- <font id="preview_status_supp"></font><br/>
                    <font id="preview_app_supp"></font><br/> -->
                    <font id="preview_nama_supp"></font><br/>
                    <font id="preview_jabatan_supp"></font><br/>
                    
                </td>
                
                <td style align='center' class="noBorder"></td>
                <td style align='right' class="noBorder">
                    <br/><br/><br/>
                    <!-- <font id="preview_status_user"></font><br/>
                    <font id="preview_app_user"></font><br/> -->
                    <font id="preview_nama_user"></font><br/>
                    <font id="preview_jabatan_user"></font><br/>
                </td>
                            
            </tr>
        </table>
		
		<br/><br/>
		<font style='color:blue'>PIC : <font color="blue" id="preview_section"></font> </font><br><br>
		<i>This e-BAST is valid without sign (generated by system)</i><br>
    </div>
    </div>
<!-- PREVIEW END -->

<!-- <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src="{{ URL::to('/') }}/img/loading/loader.gif" width="64" height="64" /><br>Loading..</div> -->
@endsection