@extends('templates.main')

@section('title', 'Outstanding BAST')
@section('styles')
    <style>
        table.dataTable thead th, table.dataTable thead td {
            border: 1px solid #fff;
        }   
    </style>
@endsection
@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <h6 class="main-content-label mb-3">@yield('title')</h6>
                        </div>
                        <div class="row mb-2">
                            <button type="button" class="badge badge-success mb-1">
                                Finish BAST <span class="badge badge-light"><b>{{ $sqlout[0]->finish }}</b></span>
                            </button>
                            <button type="button" class="badge badge-primary mb-1">
                                Not Approve By SHE <span class="badge badge-light"><b>{{ $sqlout[0]->she }}</b></span>
                            </button>
                            <button type="button" class="badge badge-primary mb-1">
                                Not Approve By User <span class="badge badge-light"><b>{{ $sqlout[0]->user }}</b></span>
                            </button>
                            <button type="button" class="badge badge-primary mb-1">
                                Not Approve By Mgr User <span class="badge badge-light"><b>{{ $sqlout[0]->mgr_user }}</b></span>
                            </button>
                            <button type="button" class="badge badge-primary mb-1">
                                Not Approve By Div User <span class="badge badge-light"><b>{{ $sqlout[0]->div_user }}</b></span>
                            </button>
                            <button type="button" class="badge badge-primary mb-1">
                                Not Approve By SH Prc <span class="badge badge-light"><b>{{ $sqlout[0]->sh_prc }}</b></span>
                            </button>
                            <button type="button" class="badge badge-primary mb-1">
                                Not Approve By Mgr Prc <span class="badge badge-light"><b>{{ $sqlout[0]->mgr_prc }}</b></span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="secondtable mt-3">
                            <table id="dataSecondTable" class="display cell-border" style="width: 100%;">
                                <thead>
                                <tr> 
                                    <th class="heading" rowspan="2"><font align="center">No.</font></th>
                                    <th class="heading" rowspan="2"><font align="center">No. Berita Acara</font></th>
                                    <th class="heading" rowspan="2"><font align="center">No. PO</font></th>
                                    <th class="heading" rowspan="2"><font align="center">Supplier</font></th>
                                    <th class="heading" rowspan="2"><font align="center">Supplier Approval</font></th>		
                                    <th class="heading" colspan="4"><font align="center">AAIJ Approval</font></th>
                                    <th class="heading" colspan="2"><font align="center">LPB</font></th>
                                    <th class="heading" rowspan="2"><font align="center">Print BA</font></th>
                                    <th class="heading" rowspan="2"><font align="center">Print Lampiran</font></th>
                                    <th class="heading" rowspan="2"><font align="center">Action</font></th>
                                    <th class="heading" colspan="2"><font align="center">Status</font></th>
                                </tr>	
                                <tr>
                                    <!--<th class="heading">JSA Check SHE</th>				
                                    <th class="heading">JSA Check Mgr SHE</th>-->				
                                    <th class="heading">SH User</th>
                                    <th class="heading">User PRC</th>
                                    <th class="heading">Not Yet Approve</th>
                                    <th class="heading">Reject Status</th>
                                    <!--<th class="heading">MGR User</th>
                                    <th class="heading">Div User</th>
                                    
                                    <th class="heading">MGR PRC</th>-->
                                    <th class="heading">No. LPB</th>
                                    <th class="heading">Approval</th>
                                    <th class="heading">Close/ Open</th>
                                </tr>		
                                </thead>
                                <tbody id="bodynya">
                            <?php $no = 1; ?> 
                            @foreach($bast_master as $bm)
                            <?php 
                                if($bm->status_she == "NOK"){
                                    $rej = $bm->nama_she."<br>".$bm->app_she."<br>Reason : ".$bm->note_she.".<br>";
                                }else{
                                    $rej = "-";
                                }
                                if($bm->status_she2=="NOK"){
                                    $rej = $bm->nama_she2."<br>".$bm->app_she2."<br>Reason : ".$bm->note_she2.".<br>";
                                }else{
                                    $rej = "-";
                                }
                                if($bm->status_user=="NOK"){
                                    $rej = $bm->nama_user."<br>".$bm->app_user."<br>Reason : ".$bm->reject_user.".<br>";
                                }else{
                                    $rej = "-";
                                }
                                if($bm->status_mgr_user=="NOK"){
                                    $rej = $bm->nama_mgr_user."<br>".$bm->app_mgr_user."<br>Reason : ".$bm->reject_mgr_user.".<br>";
                                }else{
                                    $rej = "-";
                                }
                                if($bm->status_div_user=="NOK"){
                                    $rej = $bm->nama_div_user."<br>".$bm->app_div_user."<br>Reason : ".$bm->reject_div_user.".<br>";
                                }else{
                                    $rej = "-";
                                }
                                if($bm->status_sh_prc=="NOK"){
                                    $rej = $bm->nama_sh_prc."<br>".$bm->app_sh_prc."<br>Reason : ".$bm->reject_sh_prc.".<br>";
                                }else{
                                    $rej = "-";
                                }
                                if($bm->status_mgr_prc=="NOK"){
                                    $rej = $bm->nama_mgr_prc."<br>".$bm->app_mgr_prc."<br>Reason : ".$bm->reject_mgr_prc.".<br>";
                                }else{
                                    $rej = "-";
                                }

                                $notyetapp ="-";
                                if($rej=="-"){
                                    if($bm->app_she == NULL){
                                        $notyetapp .= 'Section SHE';
                                    }
                                    if($bm->app_she2 == NULL){
                                        $notyetapp .= '<br>-Manager SHE';
                                    }
                                    if($bm->app_user == NULL){
                                        $notyetapp .= '<br>-SH User';
                                    }
                                    if($bm->app_mgr_user == NULL){
                                        $notyetapp .= '<br>-Manager User';
                                    }
                                    if($bm->app_div_user == NULL){
                                        $notyetapp .= '<br>-Division User';
                                    }
                                    if($bm->app_sh_prc == NULL){
                                        $notyetapp .= '<br>-SH PRC';
                                    }
                                    if($bm->app_mgr_prc == NULL){
                                        $notyetapp .= '<br>-Manager PRC';
                                    }
                                }
                                
                                if($bm->app_sup != NULL){
                                    $app_sup=date("Y-m-d",strtotime($bm->app_sup));
                                }else{
                                    $app_sup='';
                                }
                                
                                if($bm->app_she != NULL){
                                    $app_she= date("Y-m-d",strtotime($bm->app_she));
                                }else{
                                    $app_she='';
                                }
                                
                                if($bm->app_user != NULL){
                                    $app_user= date("Y-m-d",strtotime($bm->app_user));
                                }else{
                                    $app_user='';
                                }

                                if($bm->app_staff_prc!=NULL){
                                    $app_staff_prc= date("Y-m-d",strtotime($bm->app_staff_prc));				
                                }else{
                                    $app_staff_prc='';
                                }
                                
                                if($bm->app_mgr_user !=NULL){
                                    $app_mgr_user= date("Y-m-d",strtotime($bm->app_mgr_user));						
                                }else{
                                    $app_mgr_user='';
                                }
                                
                                if($bm->app_div_user!=NULL){
                                    $app_div_user= date("Y-m-d",strtotime($bm->app_div_user));
                                }else{
                                    $app_div_user='';
                                }
                                
                                if($bm->app_sh_prc!=NULL){
                                    $app_sh_prc= date("Y-m-d",strtotime($bm->app_sh_prc));				
                                }else{
                                    $app_sh_prc='';
                                }
                                
                                if($bm->app_mgr_prc!=NULL){
                                    $app_mgr_prc= date("Y-m-d",strtotime($bm->app_mgr_prc));				
                                }else{
                                    $app_mgr_prc='';
                                }
                                if($bm->app_she2!=NULL){
                                    $app_she2= date("Y-m-d",strtotime($bm->app_she2));				
                                }else{
                                    $app_she2='';
                                }

                                if($bm->day_she2 == 2){
                                    $backgroundcolor1 = "#fbc531";
                                    $dayshe2 = "+".$bm->day_she2." hari";
                                }else if($bm->day_she2>2){
                                    $backgroundcolor1="#e84118";
                                    $dayshe2 = $bm->day_she2." hari";
                                }else{
                                    $backgroundcolor1="#FFFFFF";
                                    $dayshe2 = "";
                                }
                                
                                if($bm->day_she==2){
                                    $backgroundcolor2="#fbc531";
                                    $dayshe = $bm->day_she." hari";
                                }else if($bm->day_she>2){
                                    $backgroundcolor2="#e84118";
                                    $dayshe = $bm->day_she." hari";
                                }else{
                                    $backgroundcolor2="#FFFFFF";
                                    $dayshe = "";
                                }
                                
                                if($bm->day_user==2){
                                    $backgroundcolor3="#fbc531";
                                    $dayuser = $bm->day_user." hari";
                                }else if($bm->day_user>2){
                                    $backgroundcolor3="#e84118";
                                    $dayuser = $bm->day_user." hari";
                                }else{
                                    $backgroundcolor3="#FFFFFF";
                                    $dayuser ="";
                                }
                                
                                if($bm->day_mgr_user==2){
                                    $backgroundcolor4="#fbc531";
                                    $daymgrusr = $bm->day_mgr_user." hari";
                                }else if($bm->day_mgr_user>2){
                                    $backgroundcolor4="#e84118";
                                    $daymgrusr = $bm->day_mgr_user." hari";
                                }else{
                                    $backgroundcolor4="#FFFFFF";
                                    $daymgrusr = "";
                                }
                                
                                if($bm->day_div_user==2){
                                    $backgroundcolor5="#fbc531";
                                    $daydivusr = $bm->day_div_user." hari";
                                }else if($bm->day_div_user>2){
                                    $backgroundcolor5="#e84118";
                                    $daydivusr = $bm->day_div_user." hari";
                                }else{
                                    $backgroundcolor5="#FFFFFF";
                                    $daydivusr = "";
                                }
                                
                                if($bm->day_prc==2){
                                    $backgroundcolor6="#fbc531";
                                    $dayprc = $bm->day_prc." hari";
                                }else if($bm->day_prc>2){
                                    $backgroundcolor6="#e84118";
                                    $dayprc = $bm->day_prc." hari";
                                }else{
                                    $backgroundcolor6="#FFFFFF";
                                    $dayprc = "";
                                }
                                
                                if($bm->day_mgr_prc==2){
                                    $backgroundcolor7="#fbc531";
                                    $daymgrprc = $bm->day_mgr_prc." hari";
                                }else if($bm->day_mgr_prc>2){
                                    $backgroundcolor7="#e84118";
                                    $daymgrprc = $bm->day_mgr_prc." hari";
                                }else{
                                    $backgroundcolor7="#FFFFFF";
                                    $daymgrprc = "";
                                }
                            
                                //status approval supplier (pembuat BAST)
                                $status_sup= $bm->status_sup;
                                if ($status_sup == 'OK'){
                                    $statusSup ='Approved';
                                }elseif ($status_sup == 'NOK'){
                                    $statusSup ='Rejected';
                                }else if ($status_sup == NULL){
                                    $statusSup ='Not yet Approved';	
                                }
                                    
                                    //status approval pic purch
                                $status_she= $bm->status_she;
                                if ($status_she == 'OK'){
                                    $statusSHE ='Approved';
                                }else if ($status_she == 'NOK'){
                                    $statusSHE ='Rejected';
                                }else if ($status_she == NULL){
                                    $statusSHE ='Not yet Approved';
                                }

                                $status_user= $bm->status_user;
                                if ($status_user == 'OK'){
								    $statusUser ='Approved';
							    }else{
                                    if ($status_user == 'NOK'){
                                        $statusUser ='Rejected';
                                    }else
                                        if ($status_user == NULL){
                                            $statusUser ='Not yet Approved';
                                        }
							    }

                                $status_staff_prc= $bm->status_staff_prc;
                                if ($status_staff_prc == 'OK')
                                {
                                    $statusPRC ='Approved';
                                }
                                else
                                {
                                    if ($status_staff_prc == 'NOK')
                                    {
                                        $statusPRC ='Rejected';
                                    }
                                    else
                                        if ($status_staff_prc == NULL)
                                        {
                                            $statusPRC ='Not yet Approved';
                                        }
                                }
                                
                                //status approval mgr user
                                $status_mgr_user= $bm->status_mgr_user;
                                if ($status_mgr_user == 'OK'){
                                    $statusMgrUser ='Approved';
                                }else if ($status_mgr_user == 'NOK'){
                                    $statusMgrUser ='Rejected';
                                }else if ($status_mgr_user == NULL){
                                    $statusMgrUser ='Not yet Approved';
                                }
                                                        
                                //status approval div user
                                $status_div_user=$bm->status_div_user;
                                if($status_div_user == 'OK'){
                                    $statusDivUser = 'Approved';
                                }else if($status_div_user == 'NOK'){
                                    $statusDivUser ='Rejected';
                                }else if($status_div_user == NULL){
                                    $statusDivUser ='Not yet Approved';
                                }
                                
                                //status approval sh purchasing
                                $status_sh_prc= $bm->status_sh_prc;
                                if ($status_sh_prc == 'OK')
                                {
                                    $statusShPrc ='Approved';
                                }
                                else
                                {
                                    if ($status_sh_prc == 'NOK')
                                    {
                                        $statusShPrc ='Rejected';
                                    }
                                    else
                                        if ($status_sh_prc == NULL)
                                        {
                                            $statusShPrc ='Not yet Approved';
                                        }
                                }
                                
                                //status approval menejer purchasing
                                $status_mgr_prc= $bm->status_mgr_prc;
                                if ($status_mgr_prc == 'OK')
                                {
                                    $statusMgrPrc ='Approved';
                                }
                                else
                                {
                                    if ($status_mgr_prc == 'NOK')
                                    {
                                        $statusMgrPrc ='Rejected';
                                    }
                                    else
                                        if ($status_mgr_prc == NULL)
                                        {
                                            $statusMgrPrc ='Not yet Approved';
                                        }
                                }
                                //status approval she sect
                                $status_she2= $bm->status_she2;
                                if ($status_she2 == 'OK')
                                {
                                    $statusShe2 ='Approved';
                                }
                                else
                                {
                                    if ($status_she2 == 'NOK')
                                    {
                                        $statusShe2 ='Rejected';
                                    }
                                    else
                                        if ($status_she2 == NULL)
                                        {
                                            $statusShe2 ='Not yet Approved';
                                        }
                                }
                                
                                //get PO Login PRC
                                $temp_prc = DB::table('PO_Trans_Mstr')->select('po_login','po_app0','po_app1')
                                ->where('po_nbr',$bm->no_po)
                                ->first();
                                $app_nama_prc = $temp_prc->po_login;
                                $nama_sh_prc  = $temp_prc->po_app0;
                                $nama_mgr_prc = $temp_prc->po_app1;

                                if ($nama_mgr_prc == 'd.risang'){
                                    $nama_mgr_prc = 'Darmawan';
                                }else{
                                    $nama_mgr_prc = 'Darmawan';
                                }
                                
                                //untuk mendapatkan no PP
                                $sql_cek_mfg  = po_detail($bm->no_po);
                                $no_pp        = $sql_cek_mfg[0]['no_pp'];
                                // dd($no_pp);
                                // untuk mendapatkan nama section PP
                                $row_section = DB::table('PP_Trans_Mstr')
                                ->select('ppm_app0','ppm_app1','ppm_app2')
                                ->where('ppm_no_pp',$no_pp)->first();
              
                                $nama_section = $row_section->ppm_app0;
                                if($nama_section == 'ba'){
                                    $nama_section = 'Agung Budi Arianto';
                                }

                                $dept_head = $row_section->ppm_app1;
                                $div_head  = $row_section->ppm_app2;
                                if($div_head == 'Surya'){
                                    $div_head ='DANIEL SURYANANTA';
                                }
               						
                                //untuk nama dari SHE 
                                $temp_she = DB::table('Portal_Login')
                                ->select('login_username')
                                ->where('login_username', 'like', '%thomas%')
                                ->first();

                                $nama_she = $temp_she->login_username;

                                $lpb = prh_hist($bm->no_po);
                                $no_lpb = $lpb[0]['prhReceiver'];
                                $tgl_lpb = $lpb[0]['prhRcp_date'];

                                $url = '<a href="/print-bast/'.base64_encode($bm->no_nbr_acr).'/'.base64_encode($no_lpb).'" target="_blank" title="Print Bast" class="btn btn-sm btn-info"><i class="fas fa-print"></i></a>';
                                $lampiran = '<a href="/lampiran-bast/'.base64_encode($bm->no_nbr_acr).'/'.base64_encode($no_lpb).'" title="Print Lampiran" target="_blank" class="btn btn-sm btn-warning"><font color="white"><i class="fas fa-print"></i></font></a>';  
                               
                            ?> 
                            <tr>
                                <td><?= $no++ ?></td>
								<td style="text-align:center" width="150">{{ $bm->no_nbr_acr }}</td>
								<td style="text-align:center" width="130">{{ $bm->no_po }}</td>
								<td style="text-align:center" width="180">{{ $bm->nama_pr }}</td>
								<td style="text-align:center" width="35"><?php echo strtoupper($bm->p_nama1)."<br/>".$app_sup."<br/>".$statusSup?></td>
								<td style="text-align:center;background-color:<?php echo $backgroundcolor3;?>;" width="150"><?php echo strtoupper($nama_section)."<br/>".$app_user."<br/>".$statusUser."<br/>".$dayuser?></td>
								<td style="text-align:center;background-color:<?php echo $backgroundcolor6;?>;" width="130"><?php echo strtoupper($nama_sh_prc)."</br>".$app_sh_prc."<br/>".$statusShPrc."<br/>".$dayprc?></td>
								<td style="text-align:center;background-color:<?php echo $backgroundcolor6;?>;" width="130"><?php echo $notyetapp; ?></td>
								<td style="text-align:center;" width="130"><?php echo $rej;?></td>
                                <td><?= $no_lpb; ?></td>
                                <td><?= date('d-m-Y',strtotime($tgl_lpb)); ?></td>
                                <td style="text-align:center"><?= $url; ?></td>
                                <td style="text-align:center"><?= $lampiran ?></td>
                                <!-- <input type="hidden" value="{{ base64_encode($bm->no_nbr_acr) }}" id="nobast"> -->
								<td style="text-align:center" width="10"><a class="btn btn-sm btn-danger" title="Delete Bast" onclick="hapus_bast('<?= base64_encode($bm->no_nbr_acr) ?>')"><font color="white"><i class="fas fa-trash"></font></i></a></td>
                                <td>{{ $bm->status }}</td>
                            </tr>
                            @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Row -->

@endsection
@section('scripts')
<script type="text/javascript">

    function hapus_bast(nobast){
        Swal.fire({
            title: 'Are you want to delete BAST ?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "GET",
                    url: "/delete-bast",
                    data:{
                        nobast : nobast,
                    },
                    success: function (data) {
                        SW.success({
                            message: 'Delete BAST Successfully',
                            
                        });
                        setTimeout(function(){ 
                            location.reload(); 
                        }, 1000);
                    },
                    error: function(error) {
                        alert('Data is not Defined!!!');
                    }
                });
            }
        })
    }

    $(document).ready(function() {

        $('#dataSecondTable').DataTable({
            responsive: false,
            paging: true,
            ordering : false,
        });
        $("table thead th").css("background-color", "#0066CC");
        $("table thead th").css("color", "#fff");

    });


</script>
@endsection