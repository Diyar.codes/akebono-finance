<?php
	$nbr_acara 	= $ba;
	$no_lpb 	= $no_lpb;
	    
		if($no_lpb != ""){
			DB::table('bast_mstr2')
            ->where('no_nbr_acr', $nbr_acara)
            ->update([
                'status'   => 'CLOSE'
            ]);
	    }		
	
        $nbr_acara 		= $bast_master->no_nbr_acr;
        $no_po 			= $bast_master->no_po;
        $ref_penawaran 	= $bast_master->ref_penawaran;
        $period 		= date('d-m-Y',strtotime($bast_master->period_kerja));
        $period1 		= date('d-m-Y',strtotime($bast_master->period_kerja1));
        $gr_pkrj		= $bast_master->garansi_pekerjaan;
        $nama    		= $bast_master->p_nama;
        $npr 			= $bast_master->nama_pr;
        $alamat 		= $bast_master->alamat;
        $Jabatan 		= $bast_master->jabatan;
        $nama1 			= $bast_master->p_nama1;
        
        $selItem 	= DB::table('bast_det2')
        ->where('no_nbr_acr',$nbr_acara)->first();

        $items		= $selItem->deskripsi;
			
        if($bast_master->status_bast == NULL || $bast_master->status_bast == ""){
            $stsbast		= 'NEW';
        }else{
            $stsbast		= $bast_master->status_bast;
        }
        
        $jabatan1 		= $bast_master->jabatan1;
        // $qba 			= $bast_master->quantity_ba;
        $note 			= $bast_master->note;
        $copy_po 		= $bast_master->copy_po;
        $check 			= $bast_master->check_list;
        $check1 		= $bast_master->check_list1;
        $check2 		= $bast_master->check_list2;
        $risk 			= $bast_master->risk;
        $drawing 		= $bast_master->drawing;
        $kesimpulan 	= $bast_master->kesimpulan;
        $manual_book 	= $bast_master->manual_book;
        $training 		= $bast_master->training;
        $pjb 			= $bast_master->pjb;
        // $line			= $bast_master->line;
        // $desc			= $bast_master->deskripsi;
        // $satuan			= $bast_master->satuan;
        // $qty_open		= $bast_master->quantity_po;		
        $app_sup 		= date("Y-m-d H:m",strtotime($bast_master->app_sup));
			
        if ($bast_master->app_sup == 'NULL' || $bast_master->app_sup == ""){
            $app_sup = '';
        }
			
        if ($nama1 == 'Surya'){
            $nama11 ='JAMES';
            $app_div_user 	= date("Y-m-d H:m",strtotime($bast_master->app_mgr_user));	
        }else{
            $nama11=$nama1;
            $app_div_user 	= date("Y-m-d H:m",strtotime($bast_master->app_div_user));	
            
            if ($bast_master->app_div_user == ''){
                $app_div_user = '';
            }
        }
        
        $app_mgr_prc 	= date("Y-m-d H:m",strtotime($bast_master->app_mgr_prc));	
        if ($bast_master->app_mgr_prc == ''){
            $app_mgr_prc = '';
        }
        
        $status_sup= $bast_master->status_sup;
        if ($status_sup == 'OK'){
            $status_sup = 'Digitally Signed';
        }else{
            $status_sup = ' ';
        }
			
        $status_mgr_prc = $bast_master->status_mgr_prc;
        if ($status_mgr_prc == 'OK'){
            $status_mgr_prc = 'Digitally Signed';
        }else{
            $status_mgr_prc = ' ';
        }
			
        $status_div_user = $bast_master->status_div_user;
        if ($status_div_user == 'OK'){
            $status_div_user = 'Digitally Signed';
        }else{
            $status_div_user = ' ';
        }					
		

        $div_head 		= $bast_master->p_nama1;
        $nama_section 	= $bast_master->jabatan1;
		
?>	
	<div style="text-align:right;!important;font-size:20px;margin-right:30px;"><?php echo $stsbast?></div>
	<div style='text-align:center;!important;font-size:20px'><?php echo $npr?></div>
	<div style='text-align:center;!important;font-size:18px'><b>BERITA ACARA SERAH TERIMA</b></div>	
		<br/>
	<table border='' style='width:200' cellspacing='0' cellpadding='0'>		
		<tr >
			<td width = '' >No. Berita Acara</td>	
				<td>:</td>
			<td width = ''><?php echo $nbr_acara?></td>
		</tr>
		
		<tr >
			<td width = '' >No. Purchase Order (PO)</td>	
				<td>:</td>
			<td width = ''><?php echo $no_po?></td>
		</tr>
			
		<tr >
			<td width = '' >No. Ref. Penawaran</td>	
				<td>:</td>
			<td width = ''><?php echo $ref_penawaran?></td>
		</tr>
		
		<tr >
			<td width = '' >Periode Pekerjaan</td>	
				<td>:</td>
			<td width = ''><?php echo $period."&nbsp; s/d &nbsp;".$period1?></td>				
		</tr>
			
		<tr >
			<td width = '' >Garansi Pekerjaan</td>	
				<td>:</td>
			<td width = ''><?php echo $gr_pkrj?></td>
		</tr>
		
		<tr>
			<td>
				<br/>
			</td>
		</tr>
		
		<tr>
			<td width = '' ><b>Yang bertandatangan dibawah ini</b></td>				
		</tr>
		<tr>
			<td>
				<p></p>
			</td>
		</tr>
		<tr>
			<td width='' >Nama</td>
			<td>:</td>
			<td width=''><?php echo strtoupper($nama)?></td>
		</tr>
		
		<tr>
			<td width='' >Nama Perusahaan</td>
			<td>:</td>
			<td width='300'><?php echo $npr?></td>
		</tr>
		
		<tr>
			<td width='' >Alamat</td>
			<td>:</td>
			<td width=''><?php echo $alamat?></td>
		</tr>
			
		<tr>
			<td width='' >Jabatan</td>
			<td>:</td>
			<td width=''><?php echo $Jabatan?></td>
		</tr>
		<tr>
			<td width='' >Selanjutnya disebut sebagai</td>
			<td>:</td>
			<td width=''>Pihak Pertama</td>
		</tr>
		
        <tr>
			<td>
				<br/>
			</td>
		</tr>
		<tr>
			<td width='' >Nama</td>
			<td>:</td>
			<td width=''><?php echo strtoupper($nama11) ?></td>
		</tr>
		
		<tr>
			<td width='' >Nama Perusahaan</td>
			<td>:</td>
			<td width=''>PT. Akebono Brake Astra Indonesia</td>
		</tr>
		
		<tr>
			<td width='' >Alamat</td>
			<td>:</td>
			<td width='300'>Jl. Pegangsaan Dua Bloak A1 Km. 1,6 Kelapa Gading Jakarta 14250</td>
		</tr>
			
		<tr>
			<td width='' >Jabatan</td>
			<td>:</td>
			<td width=''><?php echo $jabatan1?></td>
		</tr>
		
		<tr>
			<td width='' >Selanjutnya disebut sebagai</td>
			<td>:</td>
			<td width=''>Pihak Kedua</td>
		</tr> 
	</table>
		<br>
		<br>
		<div type='text'>Menerangkan bahwa telah selesai dilakukan pekerjaan <i>(penulisan sesuai di PO)</i> :</div><br/>
		<table border='1' cellpadding='0' cellspacing='0'  >
			<tr>
				<td width='30' style align='center' bgcolor='#FFFFFF' style='color:black;'>Line</td>
				<td width='420'style align='center' bgcolor='#FFFFFF' style='color:black;'>Deskripsi</td>
				<td width='50' style align='center' bgcolor='#FFFFFF' style='color:black;'>Satuan</td>
				<td width='50' style align='center' bgcolor='#FFFFFF' style='color:black;'>Quantity PO</td>
				<td width='50' style align='center' bgcolor='#FFFFFF' style='color:black;'>Quantity BA </td>

			 </tr>
			 <?php		
				$tampil_data = DB::select("SELECT quantity_ba, quantity_po, line, satuan, deskripsi
								FROM bast_det2 WHERE no_nbr_acr = '$nbr_acara'");
				// dd($tampil_data);
				$hitung = 0; 
				foreach($tampil_data as $row_app){
					$line		= $row_app->line;
					$deskripsi 	= $row_app->deskripsi;
					$satuan 	= $row_app->satuan;
					$qty_open 	= $row_app->quantity_po;
					$qba 		= $row_app->quantity_ba;
			 ?>
				<tr>
					<td style align='center'><?php echo $line ?></td>							
					<td style align='left' size='7' ><?php echo wordwrap($deskripsi,56,"<br>\n"); ?></td>		
					<td style align='center'><?php echo $satuan ?></td>				
					<td style align='center' ><?php echo number_format($qty_open) ?></td>
					<td style align='center'><?php echo number_format($qba) ?></td>				
				</tr>	
			
				<?php
				}
				?>
	
		</table><br/>
		
		<table>
			<tr>
				<td><label>Note:</label></td>
				<td><u><?php echo $note ?></u></td>
			</tr>
		</table>
		<br>
		<div type='text'>dengan rincian sebagaimana terlampir(beri tanda v) :</div>
			<?php
				if($copy_po == 'OK'){
					$copy_po  = "v";
				}else{
					$copy_po = "v";
				}
				
				if($check == 'OK'){
					$check = "v";
				}else{
					$check = "v";
				}
				
				if($check1 == 'OK'){
					$check1 = "v";
				}else{
					$check1 = "v";
				}
				
				if($check2 == 'OK'){
					$check2 = "v";
				}else{
					$check2 = "v";
				}
				
				if($risk == 'OK'){
					$risk = "v";
				}else{
					$risk = "v";
				}
				
				if($drawing == 'OK'){
					$drawing = "v";
				}else{
					$drawing = "";
				}
				
				if($kesimpulan == 'OK'){
					$kesimpulan = "v";
				}else{
					$kesimpulan = "";
				}
				
				if($manual_book == 'OK'){
					$manual_book = "v";
				}else{
					$manual_book = "";
				}
				
				if($training == 'OK'){
					$training = "v";
				}else{
					$training = "";
				}
				
			?>		
			<table cellspacing='0' cellpadding='0'>
				<tr>
					<td><?php echo $copy_po ?>&nbsp; Copy Purchase Order</td>				
				</tr>
				<tr>					
					<td><?php echo $check ?>&nbsp; Check list item pekerjaan</td><br/>
				</tr>
				<tr>					
					<td><?php echo $check1 ?>&nbsp; Foto sebelum dan sesudah pekerjaan</td><br/>
				</tr>
				<tr>					
					<td><?php echo $check2 ?>&nbsp; Service Report</td><br/>
				</tr>
				<tr>
					<td><?php echo $risk ?>&nbsp; Job Safety Analysis</td><br/>
				</tr>
				<tr>					
					<td><?php echo $drawing ?>&nbsp; Drawing</td><br/>
				</tr>
				<tr>
					<td><?php echo $kesimpulan ?>&nbsp; Kesimpulan Hasil Trial</td><br/>
				</tr>
				<tr>
					<td><?php echo $manual_book ?>&nbsp; Bukti Diserahkan Manual Book</td><br/>
				</tr>
				<tr>
					<td><?php echo $training ?>&nbsp; Bukti Dilakukan Training</td><br/>
				</tr>
				<tr>
					<td><?php echo $pjb ?>&nbsp; PJB (Perjanjian Jual Beli)</td><br/>
				</tr>	
			</table>	
		<br/>	
		<div type='text'>Demikian Berita Acara Serah Terima ini dibuat untuk menyatakan bahwa pekerjaan tersebut telah dapat diselesaikan sesuai dengan permintaan.</div>
		<br/>

		<table border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td width='250' style align='left' bgcolor='#FFFFFF'>Pihak Pertama <br/> yang menyerahkan,</td>
				<td width='180' style align='center' bgcolor='#FFFFFF'><br/><br/>Mengetahui,</td>
				<td width='130' style align='center' bgcolor='#FFFFFF'></td>
				<td width='140' style align='right' bgcolor='#FFFFFF'>Pihak Kedua <br/> yang menerima,</td>
				<td width='40' style align='center' bgcolor='#FFFFFF'> </td>
			</tr>

			<tr>				
				<td style align='left'>
					<br/><br/>
					<?php echo "<i>".$status_sup."</i>"?><br/>
					<?php echo $app_sup?><br/>
					<!--<br/><font style='color:blue'>[Materai & tanda tangan]</font><br/>-->
					<br/><?php echo strtoupper($nama);?>
					<br/><?php echo $Jabatan?>
				</td>
				
				<td style align='center'>
					<br/><br/>
					<?php echo "<i>".$status_mgr_prc."</i>";?><br/>
					<?php echo $app_mgr_prc?>
					<br/><br/>
					<br/>Darmawan
					<br/>Purchasing
				</td>
				
				<td style align='center'></td>
				<td style align='right'>
					<br/><br/>
					<?php echo "<i>".$status_div_user."</i>";?><br/>
					<?php echo $app_div_user?>
					<br/><br/>
					<br/><?php echo strtoupper($nama11);?>
					<br/><?php echo $jabatan1?>
				</td>
				
				<td style align='center'></td>
				<td style align='center'></td>				
			</tr>
		</table>
		
		<br/><br/>
		<font style='color:blue'>PIC :<?php echo " ".$nama_section; ?></font><br><br>
		<i>This e-BAST is valid without sign (generated by system)</i><br>
		
		
    <?php
	//}
    
		
// }
		
?>
