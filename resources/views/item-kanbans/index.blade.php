@extends('templates.main')

@section('title', 'Add Item WHS')

@section('body')
<div class="row my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div class="row mt-2 mb-n3">
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <hr>
                <div class="row mb-2 ml-2">
                    <div class="col-xl-1 col-lg-2 col-3 p-0 mb-xl-0 mb-2 mb-2-responsive">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Item Number</span>
                        </div>
                    </div>
                    <div class="col-xl-2 col-8 p-0 mb-xl-0 mb-2 mb-2-responsive">
                        <input type="text" class="form-control form-control-sm  form-control-sm-responsive text-dark" id="input-search-item_number" name="filter_dn_number" autocomplete="off">
                    </div>
                    <div class="col-lg-2 d-xl-none">
                    </div>
                    <div class="col-xl-1 col-lg-2 col-3 p-0 pl-xl-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Supplier ID</span>
                        </div>
                    </div>
                    <div class="col-xl-1 col-2 p-0">
                        <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="input-search-supplier_id" disabled>
                    </div>
                    <div class="ml-1 mr-1">
                        <button type="submit" class="btn btn-sm btn-sm-responsive btn-info" data-toggle="modal" data-target="#getSupplierTarget" id="seacrhSOAPBusinesRelation"><i class="fas fa-search"></i></button>
                    </div>
                    <div class="col-xl-2 col-4 p-0">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="input-search-supplier_name" disabled>
                        </div>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-xl-2 col-lg-3 col-sm-4 mb-2">
                        <button type="button" class="btn btn-info btn-block btn-sm" id="search"><i class="fas fa-search"></i> Search</button>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-sm-4 mb-2">
                        <button type="button" class="btn btn-danger btn-block btn-sm" id="reset"><i class="fas fa-undo"></i> Reset</button>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-sm-4 mb-2">
                        <button type="button" class="modal-effect btn btn-success btn-block btn-sm" id="add-item" data-effect="effect-rotate-left" data-toggle="modal" data-target="#modaldemo8">
                            <i class="fas fa-plus"></i> Add Item
                        </button>
                    </div>
                </div>
                <div class="secondtable mt-3">
                    <form>
                        <table class="display main-table" style="width: 100%" id="main-table">
                            <thead>
                                <tr style="background-color: #0066CC;">
                                    <th scope="col">Item Number</th>
                                    <th scope="col">UM</th>
                                    <th scope="col">Supplier ID</th>
                                    <th scope="col">Supplier Name</th>
                                    <th scope="col">Description 1</th>
                                    <th scope="col">Description 2</th>
                                    <th scope="col">Back No</th>
                                    <th scope="col">SNP Kanban</th>
                                    <th scope="col">Kanban Pallet</th>
                                </tr>
                            </thead>
                            <tbody class="bodytable">
                            </tbody>
                        </table>
                        <div class="row my-4 t-automatical" style="display: none">
                            <div class="col-lg-2 col-sm-4">
                                <button type="submit" class="btn btn-success btn-block btn-sm mb-2" id="updateItemKanban"><i class="fas fa-save"></i> Save</button>
                            </div>
                            <div class="col-lg-2 col-sm-4">
                                <button type="reset" class="btn btn-danger btn-block btn-sm mb-2"><i class="fas fa-undo"></i> Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="modaldemo8">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            {{-- <form action="{{ route('item-kanbans.store') }}" method="POST"> --}}
            <form action="" method="POST">
                {{-- @csrf --}}
                <div class="modal-header text-center">
                    <h6 class="modal-title w-100">Add Item</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="itemNumber" class="col-sm-3 col-form-label">Item Number</label>
                        <div class="col-sm-8 col-9">
                            <input type="text" class="form-control text-dark" id="item_number" name="item_number" autocomplete="off" readonly>
                            <small class="text-danger" id="uitem_number_validation"></small>
                        </div>
                        <div class="col-sm-1 col-2 box">
                            <button type="button" class="modal-effect btn btn-info btn-block searchSupplier searchPTMstr" data-effect="effect-rotate-left" data-toggle="modal" data-target="#modalptmstrbydesc"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="itemNumber" class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-8 col-9">
                            <input type="text" class="form-control text-dark" id="desc" name="desc" autocomplete="off" readonly>
                            <small class="text-danger" id="udesc_validation"></small>
                        </div>
                        {{-- <div class="col-sm-3 col-3 box">
                            <button type="button" class="modal-effect btn btn-info btn-block searchSupplier searchPTMstr" data-effect="effect-rotate-left" data-toggle="modal" data-target="#modalptmstrbydesc"><i class="fas fa-search"></i> Search</button>
                        </div> --}}
                    </div>
                    <div class="form-group row">
                        <label for="code" class="col-sm-3 col-form-label">Supplier</label>
                        <div class="col-sm-8 col-9">
                            <input type="text" class="form-control text-dark" id="code" name="code" autocomplete="off" readonly>
                            <small class="text-danger" id="ucode_validation"></small>
                        </div>
                        <div class="col-sm-1 col-2 box">
                            <button type="button" class="modal-effect btn btn-info btn-block searchSupplier" data-effect="effect-rotate-left" id="seacrhSOAPBusinesRelationAdd" data-toggle="modal" data-target="#getSupplierTargetAdd"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="back_no" class="col-sm-3 col-form-label">Back No</label>
                        <div class="col-sm-8 col-9">
                            <input type="text" class="form-control text-dark" id="back_no" name="back_no" autocomplete="off">
                            <small class="text-danger" id="uback_no_validation"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="snp_kanban" class="col-sm-3 col-form-label">SNP Kanban</label>
                        <div class="col-sm-8 col-9">
                            <input type="text" class="form-control text-dark" id="snp_kanban" name="snp_kanban" autocomplete="off">
                            <small class="text-danger" id="usnp_kanban_validation"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kanban_pall" class="col-sm-3 col-form-label">Kanban Pallet</label>
                        <div class="col-sm-8 col-9">
                            <input type="text" class="form-control  @error('kanban_pall') is-invalid @enderror text-dark" id="kanban_pall" name="kanban_pall" autocomplete="off">
                            <small class="text-danger" id="ukanban_pall_validation"></small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-4 col-5">
                        <button type="submit" class="btn btn-success btn-block btn-md mb-2" id="itemkanbaninsertbtn"><i class="fas fa-save"></i> Save</button>
                    </div>
                    <div class="col-sm-4 col-5 ml-n3">
                        <button type="reset" class="btn btn-danger btn-block btn-md mb-2"><i class="fas fa-undo"></i> Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal effects-->

<!-- get supplier Modal effects -->
<div class="modal" id="getSupplierTarget">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="get-supplier">
                        <thead class="thead">
                            <tr class="backgroudrowblue">
                                <th scope="col">Supplier Id</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->

<!-- get supplier Modal effects -->
<div class="modal" id="getSupplierTargetAdd">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="get-supplierAdd">
                        <thead class="thead">
                            <tr class="backgroudrowblue">
                                <th scope="col">Supplier Id</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->

<!-- pt mstr Modal effects -->
<div class="modal" id="modalptmstrbydesc">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="display text-center get-ptmstr" style="width: 100%" id="get-ptmstr">
                        <thead>
                            <tr style="background-color: #0066CC;">
                                <th scope="col">Item Number</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="bodytable1">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection

@section('scripts')
    <script type="text/javascript">
        @if (count($errors) > 0)
            $('#modaldemo8').modal('show');
        @endif
    </script>
@endsection
