<div class="table-responsive">
    <table id="tableItem" class="display" style="width: 100%;">
        <thead>
            <tr style="background-color: #0066CC;">
                <th scope="col"><font color="white">Supplier ID</font></th>
                <th scope="col"><font color="white">Supplier Name</font></th>
                <th scope="col"><font color="white">Action</font></th>
            </tr>
        </thead>
        <tbody class="firsttable">
            @foreach($data as $key)
            <tr>
                <td> {{ $key->ct_vd_addr }}</td>
                <td> {{ $key->ct_ad_name }}</td>
                <td> <button class="btn  btn-warning btn-sm btn-with-icon selected"><i class="fas fa-check"></i> Click</button></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script type="text/javascript">

$(document).ready(function() {

    var table = $('#tableItem').DataTable( {
        ordering : false,
        fixedHeader: {
            header: false,
            footer: true
        }
    });

    $("#tableItem").on('click','.selected',function(){

        var currentRow=$(this).closest("tr");

        var col1=currentRow.find("td:eq(0)").text();
        var col2=currentRow.find("td:eq(1)").text();
        var data=col1+"\n"+col2;

        $("#supplierId").val(col1);

        $("#mdItemCheck").modal('hide');

    });

});


</script>
