@extends('templates.main')

@section('title', 'View Barcode')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-header">
                <h3 class="main-content-label mb-3">View Packing Slip</h3>
            </div>
            <div class="card-body">
                {{-- <div class="col-lg-6 mb-3">
                    <label class="sr-only" for="inlineFormInputGroup">Effective Date</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text">Effective Date</div>
                        </div>
                            <input class="form-control" type="date" id="stratDate">
                            <input class="form-control" type="date" id="endDate">
                    </div>
                    <div class="btn btn-list mt-2">
                        <button type="button" class="btn btn-info mb-2" id="saveProcess" onclick="searchData();"><small><i class="fas fa-search"></i></small> Search</i></button>
                        <button type="button" class="btn btn-danger mb-2" id="cancelProcess" onclick="window.location.reload();"><small><i class="fas fa-undo"></i></small> Reset</button>
                    </div>
                </div> --}}
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-3 p-0 ml-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Transaction ID</span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6 p-0">
                        <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="transaction_id">
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-3 p-0 ml-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Supplier ID</span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6 p-0">
                        <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="supplier_id">
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-3 p-0 ml-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Effective Date</span>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-8 p-0">
                        <div class="row">
                            <div class="col-6">
                                <select class="form-control form-control-sm form-control-sm-responsive text-dark" id="filter_moon">
                                    <option value="">Month</option>
                                    @for($i = 1; $i <= 12 ; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-6 ml-n2">
                                <select class="form-control form-control-sm form-control-sm-responsive text-dark" id="filter_year">
                                    <option value="">Year</option>
                                    @for($t = date('Y')-5; $t <= date('Y') ; $t++)
                                        <option value="{{ $t }}">{{ $t }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-sm btn-info btn-block mb-2" id="filter"><i class="fas fa-search"></i> Search</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <button type="button" class="btn btn-sm btn-danger btn-block btn-md" id="reset"><i class="fas fa-undo"></i> Reset</button>
                    </div>
                </div>
                <div class="table-responsive mb-2 mt-2">
                    <table class="table table-striped table-bordered text-center w-100" id="main-table">
                        <thead>
                            <tr>
                                <th scope="col">Transaction ID</th>
                                <th scope="col">Delivery Date</th>
                                <th scope="col">PO Number</th>
                                <th scope="col">Supplier ID</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Packing Slip</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodymain">
                            {{-- @foreach($data  as $key) --}}
                            {{-- <tr> --}}
                                {{-- <td> {{ $key['portald_tr_id'] }}  </td> --}}
                                {{-- <td> {{ $key['portal_dlv_date'] }}  </td> --}}
                                {{-- <td> {{ $key['portald_po_nbr'] }}  </td> --}}
                                {{-- <td> {{ $key['portal_vend'] }}  </td> --}}
                                {{-- <td> {{ $key['supp_name'] }}  </td> --}}
                                {{-- <td> {{ $key['portal_ps_nbr'] }}  </td> --}}
                                {{-- <td> <a href=" {{ route('detail-proccess-bc', ['sj' => $key['portal_ps_nbr']] ) }} ">Show</a> </td> --}}
                            {{-- </tr> --}}
                            {{-- @endforeach --}}
                        </tbody>
                        {{-- <tbody id="rangedata"></tbody> --}}
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

{{-- @section('scripts')
<script type="text/javascript">

    $(document).ready(function() {
        $('#tableProccesBc').DataTable({
            ordering:false,
            responsive:true
        });
    });

    function searchData() {
        let date = new Date(document.getElementById("stratDate").value);
        let secondate = new Date(document.getElementById("endDate").value);
        let monthdate = date.getMonth() + 1;
        let yearsdate = secondate.getFullYear();
        let enddate = secondate.toLocaleDateString();

        $.ajax({
            type: "GET",
            url:"/search-process-bc",
            data:{
                mondate : monthdate,
                years : yearsdate
            },
            success: function(data){
                $('.tbodymain').hide();
                var tdata ='';
                $.each(data, function(key, value){

                        tdata += "<tr>";
                        tdata += "<td>"+ value["portald_tr_id"] +"</td>";
                        tdata += "<td>"+ value["portal_dlv_date"] +"</td>";
                        tdata += "<td>"+ value["portald_po_nbr"] +"</td>";
                        tdata += "<td>"+ value["portal_vend"] +"</td>";
                        tdata += "<td>"+ value["portald_tr_id"] +"</td>";
                        tdata += "<td>"+ value["portal_ps_nbr"] +"</td>";
                        tdata += "<td>"+ '<a href='+ value["portal_ps_nbr"] +'>Show</a>' +"</td>";
                        tdata += "</tr>"
                });
                document.getElementById("rangedata").innerHTML = tdata;
            },
            error: function(error){
                SW.error({
                    message: 'Data is not Defined!!!',
                });
            }
        });
    }

</script>
@endsection --}}
