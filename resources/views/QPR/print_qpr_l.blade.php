@extends('templates.main')

@section('title', 'QC Barcode')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
                <div class="card-header">
                    <h3 class="main-content-label mb-3">@yield('title')</h3>
                </div>
            <div class="card-body">

				<div class="card col-lg-6 mb-3">
					<h3 class="card-header">QPR Number</h3>
						<div class="form-group card-body">
							<label class="main-content-label tx-11 tx-medium">Transcation ID</label>
							<input class="form-control" required="" type="password">
					    	<button type="button" class="btn btn-info mt-3" id="search"></i> show </button>
						</div>
				</div>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="modaldemo8">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100">Add Item</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form action="" action="" id="finsertitemkanban">
                    <div class="form-group row">
                        <label for="itemNumber" class="col-sm-3 col-form-label">item Number</label>
                        <div class="col-9">
                            <input type="text" class="form-control" id="item_number" name="item_number">
                        </div>
                        <small class="text-danger ml-3" id="vc-item_number"></small>
                    </div>
                    <div class="form-group row">
                        <label for="supplier" class="col-sm-3 col-form-label">supplier</label>
                        <div class="col-7">
                            <input type="text" class="form-control" id="supplier" name="supplier">
                        </div>
                        <div class="col-2 box">
                            <button type="button" class="modal-effect btn btn-light btn-block searchSupplier" data-effect="effect-rotate-left" data-toggle="modal" data-target="#modaldemo8part2">
                                Search
                            </button>
                        </div>
                        <small class="text-danger ml-3" id="vc-supplier"></small>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div id="fprocessinsertitemkanban">
                    <button class="btn ripple btn-light" type="button" id="itemkanbaninsertbtn">Save</button>
                </div>
                <button class="btn ripple btn-light" data-dismiss="modal" type="button">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->

<!-- Modal effects -->
<div class="modal" id="modaldemo8part2">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered text-center w-100" id="main-table2">
                    <thead class="thead">
                        <tr>
                            <th scope="col">Supplier Id</th>
                            <th scope="col">Supplier Name</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="tbodyselect">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection