@extends('templates.main')

@section('title', 'QPR Receiving Input')

@section('body')

    <div class="row row-sm my-md-2 mt-5">
        @if ($errors->any())
            <div class="col-md-12">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-header">
                    <h3 class="main-content-label mb-3">@yield('title')</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3 ml-xl-2 ml-lg-3 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Supplier
                                    Name</span>
                                <input type="text" class="form-control form-control-sm" id="name_supp" autocomplete="off" readonly>
                            </div>
                        </div>
                        <div class="col-1 text-center mr-lg-n5 ml-lg-n4 mt-n3 mt-lg-0 ml-3 ml-sm-0 box">
                            <button class="btn btn-info btn-sm" id="searchSupplier" data-toggle="modal"
                                data-target="#modaldemo8part2"><i class="fas fa-search"></i></button>
                        </div>
                        <div class="col-lg-3 ml-xl-1 ml-lg-1 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Supplier
                                    ID</span>
                                <input type="text" class="form-control form-control-sm" id="code" autocomplete="off" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 ml-xl-2 ml-lg-3 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Found
                                    Date</span>
                                <input type="date" class="form-control form-control-sm" id="found_date" autocomplete="off"
                                    required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-sm my-md-2 mt-2" hidden id="content">
        <div class="col-12">
            <form action="{{ route('qpr-receiving-save') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card card-body">
                    <div class="row mt-4" id="dataGet">
                        <!-- -->
                    </div>
                </div>
            </form>
        </div>
    </div>


    <!-- Modal effects -->
    <div class="modal" id="modaldemo8part2">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header text-center">
                    <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal"
                        type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered text-center text-nowrap w-100" id="main-tablee">
                            <thead class="thead">
                                <tr>
                                    <th scope="col">Supplier Id</th>
                                    <th scope="col">Supplier Name</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="body">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal effects end-->

    <!-- Modal effects -->
    <div class="modal" id="modalPartName">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header text-center">
                    <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal"
                        type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered text-center text-nowrap w-100" id="main-tablee-2">
                            <thead class="thead">
                                <tr>
                                    <th scope="col">Item Number</th>
                                    <th scope="col">Deskripsi 1</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="bodyPartName">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal effects end-->
@endsection
