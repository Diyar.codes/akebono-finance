 @extends('templates.main')

@section('title', 'View Item Check QC')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <input type="hidden" name="total_data" id="totalData">
        <div class="card custom-card overflow-hidden">
                <div class="card-header">
                    <h3 class="main-content-label mb-3">@yield('title')</h3>
                </div>
            <div class="card-body">

            <form>
              <div class="form-row col-md-6">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Item Number</label>
                    <input type="text" class="form-control textblack" id="itemnnumber" placeholder="Item Number">
                    <button type="button" class="btn btn-info mt-3" id="ItemNum" onclick="searchItemNum()"></i> Search </button>
                    <button type="button" class="btn btn-danger mt-3" onclick="reset_data()"></i> Reset </button>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Supplier ID</label>
                    <div class="input-group">
                        <input type="text" class="form-control textblack" id="supplierId" placeholder="Supplier ID">
                        <span class="input-group-btn"><button class="btn btn-info" type="button" id="modalItemCheck">
                        <span class="input-group-btn"><i class="fa fa-search"></i></span></button></span>
                    </div>
                    <a href=" {{route('add-item-check-qc')}} " class="btn btn-success ml-2 mt-3"><i class="fas fa-plus"></i> Add Item Check</a> 
                </div>


              </div>
            </form>

            <div class="table-responsive mb-2">
                <table id="tCheckData" class="table table-striped table-bordered text-center w-100" id="main-table">
                    <thead>
                        <tr style="background-color: #0066CC;">
                            <th scope="col">Item Number</th>
                            <th scope="col">Description 1</th>
                            <th scope="col">Description 2</th>
                            <th scope="col">Old Description</th>
                            <th scope="col">UM</th>
                            <th scope="col">Supplier ID</th>
                            <th scope="col">Supllier Name</th>
                            <th scope="col">CHECK</th>
                            <th scope="col">Frekuensi Check</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tbodyCheck">
                        {!! $html !!}
                    </tbody>
                </table>
                <div class="row mb-2" id="buttonsave">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-success btn-block mb-2" id="btnSaveItem" onclick="saveDataItem()"><i class="fas fa-save"></i> Save</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <button type="button" class="btn btn-danger btn-block mb-2" onclick="window.location.reload();"><i class="fas fa-undo"></i> Cancel</button>
                    </div>
                </div>
            </div>
            <!-- <div class="row mb-2">
                <div class="col-lg-2 col-sm-4">
                    <button type="submit" class="btn btn-success btn-block mb-2" id="btnSaveItem" onclick="saveDataItem()"><i class="fas fa-save"></i> Save</button>
                </div>
                <div class="col-lg-2 col-sm-4">
                    <button type="button" class="btn btn-danger btn-block mb-2" onclick="window.location.reload();"><i class="fas fa-undo"></i> Cancel</button>
                </div>
            </div> -->

            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="mdItemCheck">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header">
                <h6 class="modal-title w-100">Supplier Lookup</h6>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered text-center w-100" id="main-table2">
                    <thead class="thead">
                        <tr>
                            <th scope="col">Supplier Id</th>
                            <th scope="col">Supplier Name</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="tbodyselect"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection

@section('scripts')
<script type="text/javascript">
    
$(document).ready(function() {
    $('#tCheckData').DataTable({
        ordering : false,
        paging: false
    });
    // $("#buttonsave").hide();
    $("#modalItemCheck").click(function() {

        getLoader();
        $.ajax({
            type: "GET",
            url: "/modal-add-item-check",
            success: function (data) {
                $('#mdItemCheck').modal('show')
                $('#mdItemCheck').find('.modal-body').html(data)
            }
        });
    });

});

function hapus_part_qc(part,supplier){
    Swal.fire({
        title: 'Are you want to delete this part QC ? ',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "GET",
                url: "/hapus-part",
                data:{
                    part : part,
                    supplier : supplier,
                },
                success: function (data) {
                    SW.success({
                        message: 'Delete Part QC Successfully',
                        
                    });
                    setTimeout(function(){ 
                        location.reload(); 
                    }, 1000);
                },
                error: function(error) {
                    alert('Data is not Defined!!!');
                }
            });
        }
    });
}

function searchItemNum() {
    var itemNumber = document.getElementById("itemnnumber").value;
    var idsupp = document.getElementById("supplierId").value;
    getLoader();
    $.ajax({
        type: "GET",
        url: "/search-data-item-check",
        data:{
            numItem : itemNumber,
            suppID : idsupp
        },
        success: function (data) {
            $('#tCheckData').DataTable().destroy();
            $('#tbodyCheck').html(data);
            // $("#buttonsave").show();
            // document.getElementById("tbodyCheck").innerHTML = tdata;

            $('#tCheckData').DataTable({
                ordering : false,
                paging: false
            });
        },
        error: function(error){
            SW.error({
                message: 'Data is not Defined!!!',
            });
        }
    });
}

function reset_data() {
    var itemNumber = '';
    var idsupp = '';
    getLoader();
    $.ajax({
        type: "GET",
        url: "/search-data-item-check",
        data:{
            numItem : itemNumber,
            suppID : idsupp
        },
        success: function (data) {
            $('#tCheckData').DataTable().destroy();
            $('#tbodyCheck').html(data);
            // $("#buttonsave").hide();
            // document.getElementById("tbodyCheck").innerHTML = tdata;

            $('#tCheckData').DataTable({
                ordering : false,
                paging: false
            });
        },
        error: function(error){
            SW.error({
                message: 'Data is not Defined!!!',
            });
        }
    });
}

function checkcok(urut){
    var checkqc = $('[id^="checkqc"]').map(function () { return $(this).val(); }).get();
    var hitung = urut-1;
    if(checkqc[hitung] == 'off'){
        document.getElementById("checkqc["+parseInt(urut)+"]").value = 'on';
    }else{
        document.getElementById("checkqc["+parseInt(urut)+"]").value = 'off';
    }
}

function saveDataItem() {
    var part    = $('[id^="part"]').map(function () { return $(this).val(); }).get();
    var frek    = $('[id^="frek"]').map(function () { return $(this).val(); }).get();
    var check   = $('[id^="checkqc"]').map(function () { return $(this).val(); }).get();
    var supp    = $('[id^="supp"]').map(function () { return $(this).val(); }).get();
    
    var Dtotal = document.getElementById("jml_data").value;
    getLoader();
    $.ajax({
        type:"GET",
        url: "/store-item-check-qc",
        data : {
            part: part,
            frek: frek,
            check: check,
            supp: supp,
            Dtotal : Dtotal,
        },
        success: function(data){
            
            SW.success({
                message: 'Save Data Successfully!',
            });
            setTimeout(function(){ 
                window.location.reload();
            }, 1400);
        },
        error: function(error){
            SW.error({
                message: 'Failed to Save Data!!',
            });
        }

    })
}


</script>
@endsection