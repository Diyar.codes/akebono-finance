@extends('templates.main')

@section('title', 'Input Sortir')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-header mt-2">
                <h3 class="main-content-label mb-3">@yield('title')</h3>
            </div>
            <div class="card-body">
                <form action="{{ route('store-sortir') }}" method="POST">
                @csrf
                    <div class="table-responsive mb-2">
                        <table class="table table-striped table-bordered text-center" id="main-table" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Tgl</th>
                                    <th>Supplier</th>
                                    <th>Problem</th>
                                    <th>Item Number</th>
                                    <th>OK</th>
                                    <th>NG</th>
                                    <th>Total Sortir</th>
                                    <th>Jam Start</th>
                                    <th>Jam End</th>
                                    <th>PIC Sortir</th>
                                    <th>PIC QC</th>
                                </tr>
                            </thead>
                            <tbody class="tbodymain">
                                <tr>
                                    <td class="p-1"><input type="date" name="tgl" class="form-control form-control-sm" autocomplete="off" style="width: 135px"></td>
                                    <td class="spacer p-1" style="padding-right: 3%">
                                        <input type="text" id="code" name="supplier" class="form-control form-control-sm" readonly style="min-width: 65px">
                                        <button type="button" class="btn btn-info btn-search pl-1 pr-1" data-effect="effect-rotate-left" data-toggle="modal" data-target="#modaldemo8part2" id="seacrhSOAPBusinesRelationModal"><i class="fas fa-search"></i></button>
                                    </td>
                                    <td class="p-1"><input type="text" name="problem" class="form-control form-control-sm" autocomplete="off" style="min-width: 65px"></td>
                                    <td class="spacer p-1" style="padding-right: 3%">
                                        <input type="text" id="item_number" class="form-control form-control-sm" name="item_number" autocomplete="off" readonly style="min-width: 65px">
                                        <button type="button" class="btn btn-info btn-search pl-1 pr-1" data-effect="effect-rotate-left" data-toggle="modal" data-target="#modalptmstrbydesc" id="search-get-pt-mstr"><i class="fas fa-search"></i></button>
                                    </td>
                                    <td class="p-1"><input type="number" class="form-control form-control-sm" name="ok" id="ok" autocomplete="off" value="0" style="min-width: 60px"></td>
                                    <td class="p-1"><input type="number" class="form-control form-control-sm" name="ng" id="ng" autocomplete="off" value="0" style="min-width: 60px"></td>
                                    <td class="p-1"><input type="number" class="form-control form-control-sm" name="total_sortir" id="total_sortir" autocomplete="off" value="0" readonly style="min-width: 65px"></td>
                                    <td class="p-1"><input type="time" class="form-control form-control-sm" name="jam_start" autocomplete="off" style="min-width: 65px"></></td>
                                    <td class="p-1"><input type="time" class="form-control form-control-sm" name="jam_end" autocomplete="off" style="min-width: 65px"></></td>
                                    <td class="p-1"><input type="text" class="form-control form-control-sm" name="pic_sortir" autocomplete="off" style="min-width: 65px"></td>
                                    <td class="p-1">{{ Auth::guard('pub_login')->user()->username }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row mb-2 d-flex justify-content-center">
                        <div class="col-8 col-sm-7 col-md-6 col-lg-3 col-xl-2">
                            <button type="submit" class="btn btn-success btn-block mb-2" id="updateItemKanban"><i class="fas fa-save"></i> Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="modaldemo8part2">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered text-center w-100" id="get-supplier">
                    <thead class="thead">
                        <tr style="background-color: #0066CC;">
                            <th scope="col">Supplier Id</th>
                            <th scope="col">Supplier Name</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="get-supplier">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->

<!-- Modal effects -->
<div class="modal" id="modalptmstrbydesc">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="secondtablee mt-3">
                    <table class="table table-striped table-bordered text-center w-100" id="get-pt-mstr">
                        <thead class="thead">
                            <tr style="background-color: #0066CC;">
                                <th scope="col">Item Number</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tblsecondd">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->

<!-- pt mstr Modal effects -->
<div class="modal" id="modalptmstrbydesc">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="display text-center" style="width: 100%" id="get-pt-mstr">
                        <thead>
                            <tr style="background-color: #0066CC;">
                                <th scope="col">Item Number</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection
