<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset('js/lib/swal.js') }}"></script>
@if ($status == 'approval')
    <script>
        $(() => {
            Swal.fire({
                title: 'Success',
                text: "Thank you for Approval",
                icon: 'success',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = location.origin
                } else {
                    window.location.href = location.origin
                }
            })
        })

    </script>
@elseif($status == 'unread')
    <script>
        $(() => {
            Swal.fire({
                title: 'Success',
                text: "Thank you for Read",
                icon: 'success',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = location.origin
                } else {
                    window.location.href = location.origin
                }
            })
        })

    </script>
@elseif($status == 'read')
    <script>
        $(() => {
            Swal.fire({
                title: 'Success',
                text: "Data has been Read before, Please Click Button Countermeasure for Respon the Problem!",
                icon: 'success',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            }).then((result) => {
                if (result.isConfirmed) {
                    window.history.back();
                } else {
                    window.history.back();
                }
            })
        })

    </script>
@else
    <script>
        $(() => {
            Swal.fire({
                title: 'Success',
                text: "QPR has been rejected!",
                icon: 'error',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = location.origin
                } else {
                    window.location.href = location.origin
                }
            })
        })

    </script>

@endif
