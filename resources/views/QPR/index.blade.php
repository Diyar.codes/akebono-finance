@extends('templates.main')

@section('title', 'QC Barcode')

@section('body')
<div class="inner-body">
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="card custom-card overflow-hidden">
                <div class="card-header">
                    <h3 class="main-content-label mb-3">@yield('title')</h3>
                </div>

                <div class="card-body">
                    @if ($errors->has('transcationid'))
                        <div class="alert alert-danger mg-b-0 mb-2" role="alert">
                            <button aria-label="Close" class="close" data-dismiss="alert" type="button">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>{{ $errors->first('transcationid') }}</strong>
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success mg-b-0 mb-2" role="alert">
                            <button aria-label="Close" class="close" data-dismiss="alert" type="button">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>{{ session('status') }}</strong>
                        </div>
                    @endif
                        <div class="card col-lg-6 mb-3">
                            <h4 class="card-header">Packing Slip Barcode</h4>
                            <div class="form-group card-body">
                                    <label class="main-content-label tx-11 tx-medium">Transcation ID <span class="tx-danger">*</span></label>
                                    <input class="form-control text-dark" type="text" name="transcationid" id="idtransaction" required="">
                                    <input type="hidden" name="datatotal" id="totalData" class="form-control">
                                    <button onclick="slipbarcode()" class="btn btn-info mt-3">Show</button>
                            </div>
                        </div>

                        <table class="table table-bordered firsttableqc">
                            <tbody>
                                <tr>
                                    <th scope="row">Delivery Note</th>
                                    <td id="delivNote"></td>
                                    <th scope="row">Delivery Date</th>
                                    <td id="delivDate"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Purchase Order</th>
                                    <td id="purOrder"></td>
                                    <th scope="row">Packing Slip</th>
                                    <td id="packingSlip"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Date</th>
                                    <td><input type="date" class="form-control" style="color:black" name="datebarcode" value="<?= date("Y-m-d") ?>"></td>
                                    <th scope="row">Time</th>
                                    <td><input type="time" class="form-control" style="color:black" name="datebarcode" value="<?= date("H:i") ?>"></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="mb-3" id="tableqcbarcode">
                            <table id="databarcode" class="table-bordered display" style="width: 100%">
                                <thead>
                                    <tr style="background-color: #0066CC;">
                                        <th>Line</th>
                                        <th>Item Number</th>
                                        <th>Description</th>
                                        <th>Old Description</th>
                                        <th>UM</th>
                                        <th>QTY Receive</th>
                                        <th>OK/NG</th>
                                        <th>QTY Confirm</th>
                                        <th>QTY NG</th>
                                        <th>Inderect?</th>
                                        <th>Note</th>
                                        <th>Inspection Supplier ID</th>
                                        <th>Inspection AAIJ ID</th>
                                        <th>Remark</th>
                                    </tr>
                                </thead>
                                <tbody id="tbarcode"></tbody>
                            </table>
                                <div class="row mb-2 mt-3">
                                    <div class="col-lg-2 col-sm-4">
                                        <button class="btn btn-success btn-block mb-2" id="submitBarcode" onclick="saveBarcode()"><i class="fas fa-save"></i> Save</button>
                                    </div>
                                    <div class="col-lg-2 col-sm-4">
                                        <button class="btn btn-danger btn-block mb-2" onclick="window.location.reload();"><i class="fas fa-undo"></i> Cancel</button>
                                    </div>
                                </div>
                        </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">

    $('#tableqcbarcode').hide();

    function slipbarcode() {

        var idtrans = document.getElementById("idtransaction").value;
        if (idtrans == '') {
            SW.error({
                message: 'Please Input Transcation ID!!',
            });
        }else{
            getLoader();
            $.ajax({
                type : "GET",
                url : "/show-qc-barcode",
                data :{
                    idtrans:idtrans
                },
                success: function(data){
                    $('#databarcode').DataTable().destroy();
                    $('#tbarcode').html(data.html);
                    
                    document.getElementById("delivNote").innerHTML  = data.dNnumber;
                    document.getElementById("purOrder").innerHTML  = data.po;
                    document.getElementById("packingSlip").innerHTML  = data.packingslip;
                    document.getElementById("delivDate").innerHTML  = data.deliveryDate;
                    document.getElementById("totalData").value  = data.jumlahData;

                    $('#databarcode').DataTable( {
                        ordering : false,
                        paging: false
                    } );

                    $('#tableqcbarcode').show();
                },
                error: function(data){
                    SW.error({
                        message: 'Data is not defined!',
                    });
                }
            });
        }
    }

    function changeQtyConfirm(listReceive) {

        var listqty = $('[id^="listNG"]').map(function () { return $(this).val(); }).get();
        var qtyconfirm = $('[id^="listConfirm"]').map(function () { return $(this).val(); }).get();
        var sortListReceive = listReceive - 1;

        if (Number(listqty[sortListReceive]) > Number(qtyconfirm[sortListReceive])) {
            SW.error({
                message: 'QTY NG not greater than QTY Confirm!',
            });
            document.getElementById("listNG["+Number(listReceive)+"]").value = '';
        }else{
            var result = Number(qtyconfirm[sortListReceive] - listqty[sortListReceive]);
            document.getElementById("listConfirm["+ listReceive +"]").value = result;
        }

    }

    function saveBarcode() {

        var transid     = document.getElementById('idtransaction').value;
        var dataTotal   = document.getElementById('totalData').value;
        var listqty = $('[id^="listNG"]').map(function () { return $(this).val(); }).get();
        var line = $('[id^="line"]').map(function () { return $(this).val(); }).get();
        var part = $('[id^="part"]').map(function () { return $(this).val(); }).get();
        // var numitem = $('[id^="NumItem"]').map(function () { return $(this).val(); }).get();

        // var line   = document.getElementById('tLine').innerHTML;
        var item         = document.getElementById('tItem').innerHTML;
        var dataConfirm  = document.getElementById('dataConfirm').value;
        var remark       = document.getElementById('remark').value;
        // var qtyNg       = document.getElementById('listNG').value;
        // alert(line); alert(part);
        getLoader();
        $.ajax({
            type : "POST",
            url: "/store-qc-barcode",
            data:{
                _token: $('#token').val(),
                transid : transid,
                qty:listqty,
                tline : line,
                part:part,
                titem : item,
                qtyConfirm : dataConfirm,
                description : remark,
                dataTotal : dataTotal,
            },
            success: function(data){
                if (data == 'Success') {
                    SW.success({
                        message: 'Create Barcode Successfully',
                    });
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                }else{
                    SW.error({
                        message: 'Data Not Defined!',
                    });
                }
            },
            error: function(error){
                SW.error({
                    message: 'Failed Create Data',
                });
            }
        })

    }

</script>
@endsection
