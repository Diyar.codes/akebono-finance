@extends('templates.main')

@section('title', 'QPR LIST')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-header mt-2">
                <h3 class="main-content-label mb-3">List Of QPR</h3>
            </div>
            <div class="card-body">
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-3 p-0 ml-3">
                        <div class="input-group mb-1">
                            <span class="exinput-custom-responsive">Type</span>
                        </div>
                    </div>
                    <div class="col-lg-2 col-3 p-0">
                        <select class="form-control form-control-sm form-control-sm-responsive text-dark" id="type">
                           <option value="">ALL</option>
                            <option value="Customer">Customer</option>
                            <option value="Line">Line</option>
                            <option value="Receiving">Receiving</option>
                        </select>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-3 p-0 ml-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">QPR NO</span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6 p-0">
                        <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="qprl_no">
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-3 p-0 ml-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Supplier</span>
                        </div>
                    </div>
                    <div class="col-lg-1 col-2 p-0">
                        <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="code" disabled>
                    </div>
                    <div class="ml-1 mr-1">
                        <button type="submit" class="btn btn-sm btn-sm-responsive btn-info" data-toggle="modal" data-target="#modaldemo8part2" id="seacrhSOAPBusinesRelationModal"><i class="fas fa-search"></i></button>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-4 p-0">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="name" disabled>
                        </div>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-3 p-0 ml-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">From</span>
                        </div>
                    </div>
                    <div class="col-lg-2 col-3 p-0">
                        <input type="date" class="form-control form-control-sm form-control-sm-responsive text-dark" name="date-from" id="date-from">
                    </div>
                    <div class="ml-md-3 mr-md-3 ml-1 mr-1">
                        <span class="exinput-custom-responsive"><b>To</b></span>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-3 p-0">
                        <div class="input-group">
                            <input type="date" class="form-control form-control-sm form-control-sm-responsive text-dark" name="date-to" id="date-to">
                        </div>
                    </div>
                </div>
                <div class="row mt-2 mb-n3">
                    <div class="col-xl-2 col-lg-3 offset-md-0 offset-sm-2 col-sm-4 offset-1 col-5 mr-1">
                            <button type="button" class="btn btn-info btn-block btn-sm btn-sm-responsive" id="search"><i class="fas fa-search"></i> Search</button>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-sm-4 col-5 ml-1">
                            <button type="button" class="btn btn-danger btn-block btn-sm btn-sm-responsive" id="reset"><i class="fas fa-undo"></i> Reset</button>
                        </div>
                    </div>
                </div>
                <div class="row mt-2 mx-3 t-automatical" style="display: none">
                    <form action="{{ route('excel-counter-measure')}}" method="POST">
                        @csrf
                        <input type="hidden" value="" name="typeExcel" id="typeExcel">
                        <input type="hidden" value="" name="qprNoExcel" id="qprNoExcel">
                        <input type="hidden" value="" name="supplierIdExcel" id="supplierIdExcel">
                        <input type="hidden" value="" name="fromExcel" id="fromExcel">
                        <input type="hidden" value="" name="toExcel" id="toExcel">
                        <button type="submit" class="btn ripple btn-outline-success btn-sm btn-icon mb-2 counterexcel"data-toggle="tooltip" data-placement="top" title="Download Excel"><i class="si si-cloud-download"></i></button>
                    </form>
                </div>
                <div class="row mr-1 ml-1">
                    <div class="col-12">
                        <div class="mb-2">
                            <table class="table table-striped table-bordered text-center display w-100" id="main-table" width="100%">
                                <thead>
                                    <tr>
                                        <th class="align-middle" scope="col" width="5px">No</th>
                                        <th class="align-middle" scope="col" width="5px">No QPR</th>
                                        <th class="align-middle" scope="col" width="5px">Supp. Name</th>
                                        <th class="align-middle" scope="col" width="5px">Item Number</th>
                                        <th class="align-middle" scope="col" width="5px">Item Desc</th>
                                        <th class="align-middle" scope="col" width="5px">QTY NG</th>
                                        @if(Auth::guard('pub_login')->user()->previllage == 'Finance')
                                            <th class="align-middle" scope="col" width="5px">Invoice <br> Replacement</th>
                                            <th class="align-middle" scope="col" width="5px">Invoice</th>
                                            <th class="align-middle" scope="col" width="5px">Paid</th>
                                            <th class="align-middle" scope="col" width="5px">Actual Amount</th>
                                        @else
                                            <th class="align-middle" scope="col" width="5px">Check</th>
                                            <th class="align-middle" scope="col" width="5px">Approval</th>
                                            <th class="align-middle" scope="col" width="5px">Counter <br> Measure</th>
                                            <th class="align-middle" scope="col" width="5px">Verification</th>
                                            @endif
                                        <th class="align-middle" scope="col" width="5px">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="tbodymain">
                                </tbody>
                            </table>
                            <div class="row my-4">
                                {{-- @if(Auth::guard('pub_login')->user()->previllage == 'Finance')
                                    <div class="col-lg-2 col-sm-4">
                                        <button type="button" class="btn btn-success btn-block btn-sm mb-2" id="updateItemKanban"><i class="fas fa-save"></i> Save</button>
                                    </div>
                                @elseif(Auth::guard('pub_login')->user()->previllage == 'QC')
                                    <div class="col-lg-2 col-sm-4">
                                        <button type="button" class="btn btn-success btn-block btn-sm mb-2"><i class="fas fa-undo"></i> Save</button>
                                    </div>
                                @endif --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="modaldemo8part2">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="main-supplier">
                        <thead class="thead">
                            <tr style="background-color: #0066CC;">
                                <th scope="col">Supplier Id</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection
