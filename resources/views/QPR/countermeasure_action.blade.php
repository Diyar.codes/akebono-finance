@extends('templates.main')

@section('title', 'Countremeasure')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">        
        <center>     
            <form name="form1" id="form1" method="POST" enctype="multipart/form-data" action="{{ route('store-measure-action')}}">
                @csrf
                <input class="inputField" size='10'  name="qpr_no" type="hidden" id="qpr_no" value='<?= $result['qpr'] ?>' />
                <input class="inputField" size='10'  name="revisi" type="hidden" id="revisi" value='<?= $result['revisi'] ?>' />
                <table style='font-size:8px;font-family:Times' id="messageTable" border="1" cellpadding="0" cellspacing="0" width="700px" height="100px" align="center">
                    <tr>
                        <td style='align:center' colspan="2"><b>SUPPLIER ACTION<b> <br><br></td>
                    </tr>
                    <tr>
                        <td align="left">&nbsp;&nbsp;&nbsp; <u><b>Five Principle for Problem Solving</b></u> <br><br> &nbsp;&nbsp;&nbsp;&nbsp;Problem Analysis Manual</td>
                        <td>
                            <table border="" cellpadding="0" cellspacing="0" width="80%" height="30%">
                                <tr>
                                    <td>Date : <?php echo date("m/d/Y"); ?></td>
                                    <!--<script language='JavaScript'>new tcal ({'formname': 'form1','controlname': 'deliv'});</script>-->
                                    <td style='text-align:center'>Approved by</td>
                                    <td style='text-align:center'>Prepared by</td>
                                </tr>
                                <tr>
                                    <td height="30px">No. : {{ $result['qpr']}}  <br> Quality Assurance Departement</td>
                                    <td height="30px" align=''><input class="inputField" size='25'  name="approved" type="text" id="approved" value='<?= $result['qprl_rp_approved'] ?>' /></td>
                                    <td height="30px" align=''><input class="inputField" size='25'  name="prepared" type="text" id="prepared" value='<?= $result['qprl_rp_prepared'] ?>' /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br>
                            <table  border="0" cellpadding="0" cellspacing="0" width="" height="30%" >
                            <tr>
                                <td><font size="2">&nbsp;&nbsp;&nbsp;<b>DESCRIPTION OF PROBLEM </b></font><br><font size="1">&nbsp;&nbsp;&nbsp;&nbsp;(Phenomenon/ Description/ Occurrence Quality/ Measures)</font></td>
                                <td colspan="2"><font size="2">&nbsp;&nbsp;&nbsp;<b>RECOGNATION OF PROBLEM </b></font><br><font size="1">&nbsp;&nbsp;&nbsp;&nbsp;(Check Result of The part/ Cause Analysis/ Quality Status of Products)</font></td>
                            </tr>
                            <tr>
                                <td height="170px"><textarea class="inputField" name="problem_desc" id="problem_desc" value="<?= $result['qprl_rp_desc_problem'] ?>" cols="50" rows="9">{{$result['qprl_rp_desc_problem']}}</textarea></td>
                                <td height="170px"  colspan="2"><textarea class="inputField" name="problem_rec" id="problem_rec" value="<?= $result['qprl_rp_rec_problem'] ?>" cols="50" rows="9">{{$result['qprl_rp_rec_problem']}}</textarea></td>
                                        
                            </tr>
                            <tr>
                                <td ><font size="2">&nbsp;&nbsp;&nbsp;<b>ANALYSIS OF CAUSE</b></font><br><font size="1"><br/>(Mechanism of Occurrence/ Re-producting of Nonconformance/Why &amp; Why Analysis)</font></td>
                                <td ><font size="2">&nbsp;&nbsp;&nbsp;<b>APPRORIATE COUNTERMEASURE</b></font><br><font size="1">&nbsp;&nbsp;&nbsp;&nbsp;(Description/Estimated/PPA)</font></td>
                                <td><font size="2">&nbsp;&nbsp;&nbsp;<b>CONFIRMATION OF EFFECT</b></font><br><font size="1">&nbsp;&nbsp;&nbsp;&nbsp;(Actual Result of Effect)</font></td>

                            </tr>
                            <tr>
                                <td height="160px"><textarea class="inputField" name="analysis" id="analysis" value="<?= $result['qprl_rp_analysis'] ?>" cols="50" rows="9">{{ $result['qprl_rp_analysis']}}</textarea></td>
                                <td height="160px">
                                    <table border=''>
                                        <tr>
                                            <td>Description</td>
                                            <td>PIC</td>
                                            <td>Due Date (yyyy-mm-dd)</td>
                                        </tr>
                                        <tr>
                                            <td><input class="inputField" size='25'  name="desc" type="text" id="desc" value='<?= $result['qprl_rp_approriate'] ?>' /></td>
                                            <td><input class="inputField" size='8'  name="pic" type="text" id="pic" value='<?= $result['qprl_rp_approriate_pic'] ?>' /></td>
                                            <td><input class="inputField" size='10'  name="duedate" type="text" id="duedate" value='<?= $result['duedate'] ?>' onClick='raiseDatePicker()' readonly /></td>
                                        </tr>
                                        <tr>
                                            <td><input class="inputField" size='25'  name="desc1" type="text" id="desc1" value='<?= $result['qprl_rp_approriate1'] ?>' /></td>
                                            <td><input class="inputField" size='8'  name="pic1" type="text" id="pic1" value='<?= $result['qprl_rp_approriate_pic1'] ?>' /></td>
                                            <td><input class="inputField" size='10'  name="duedate1" type="text" id="duedate1" value='<?= $result['duedate1'] ?>' onClick='raiseDatePicker()'readonly /></td>
                                        </tr>
                                        <tr>
                                            <td><input class="inputField" size='25'  name="desc2" type="text" id="desc2" value='<?= $result['qprl_rp_approriate2'] ?>' /></td>
                                            <td><input class="inputField" size='8'  name="pic2" type="text" id="pic2" value='<?= $result['qprl_rp_approriate_pic2'] ?>' /></td>
                                            <td><input class="inputField" size='10'  name="duedate2" type="text" id="duedate2" value='<?= $result['duedate2'] ?>' onClick='raiseDatePicker()' readonly /></td>
                                        </tr>
                                        <tr>
                                            <td><input class="inputField" size='25'  name="desc3" type="text" id="desc3" value='<?= $result['qprl_rp_approriate3'] ?>' /></td>
                                            <td><input class="inputField" size='8'  name="pic3" type="text" id="pic3" value='<?= $result['qprl_rp_approriate_pic3'] ?>' /></td>
                                            <td><input class="inputField" size='10'  name="duedate3" type="text" id="duedate3" value='<?= $result['duedate3'] ?>' onClick='raiseDatePicker()' readonly /></td>
                                        </tr>
                                        <tr>
                                            <td><input class="inputField" size='25'  name="desc4" type="text" id="desc4" value='<?= $result['qprl_rp_approriate4'] ?>' /></td>
                                            <td><input class="inputField" size='8'  name="pic4" type="text" id="pic4" value='<?= $result['qprl_rp_approriate_pic4'] ?>' /></td>
                                            <td><input class="inputField" size='10'  name="duedate4" type="text" id="duedate4" value='<?= $result['duedate4'] ?>' onClick='raiseDatePicker()' readonly /></td>
                                        </tr>
                                    </table>
                                </td>
                                <td height="90px" valign="midle"><textarea class="inputField ml-2" name="confirmation" id="confirmation" value="<?= $result['qprl_rp_confirm'] ?>" cols="30" rows="9">{{ $result['qprl_rp_confirm'] }}</textarea></td>  
                            </tr>
                            <tr>
                                <td colspan="2" valign="midle"><font size="2">&nbsp;&nbsp;&nbsp;Why &amp; Why Analysis </font></td>
                                <td  valign="top"><font size="2"><b>FEED BACK TO THE ORIGINATING OPERATION</b></font><br><font size="1">(Reflection to the System &amp; Organization)</font></td>                               
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table  border="" cellpadding="0" cellspacing="0" width="97%" height="80%" >
                                    <tr>
                                        <td align="center" width="30px" rowspan='2'>Step</td>
                                        <td style='text-align:center' width="">1</td>
                                        <td style='text-align:center' width="">2</td>
                                        <td style='text-align:center' width="">3</td>
                                        <td style='text-align:center' width="">4</td>
                                        <td style='text-align:center' width="">5</td>
                                    </tr>
                                    <tr>
                                        <td height="60px"><textarea class="inputField" name="step1" id="step1" value="<?= $result['qprl_rp_why1'] ?>" cols="15" rows="8">{{ $result['qprl_rp_why1'] }}</textarea></td>
                                        <td height="60px"><textarea class="inputField" name="step2" id="step2" value="<?= $result['qprl_rp_why2'] ?>" cols="15" rows="8">{{ $result['qprl_rp_why2'] }}</textarea></td>
                                        <td height="60px"><textarea class="inputField" name="step3" id="step3" value="<?= $result['qprl_rp_why3'] ?>" cols="15" rows="8">{{ $result['qprl_rp_why3'] }}</textarea></td>
                                        <td height="60px"><textarea class="inputField" name="step4" id="step4" value="<?= $result['qprl_rp_why4'] ?>" cols="15" rows="8">{{ $result['qprl_rp_why4'] }}</textarea></td>
                                        <td height="60px"><textarea class="inputField" name="step5" id="step5" value="<?= $result['qprl_rp_why5'] ?>" cols="15" rows="8">{{ $result['qprl_rp_why5'] }}</textarea></td>
                                    </tr>
                                    </table>
                                </td>
                                <td height="90px" align='center'><textarea align='center' class="inputField" name="fedback" id="fedback" value="<?= $result['qprl_rp_feed'] ?>" cols="30" rows="8">{{ $result['qprl_rp_feed']}}</textarea></td>   
                            </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2'>Attachment : <input type='file' name='im_attc_dok' id='im_attc_dok' value=''>
                            {{ $result['qprl_rp_attachment']}}
                        <input type='hidden' name='im_attc_dok_hid' value='<?= $result['qprl_rp_attachment'] ?>'>
                        </td>
                    </tr>
                    
                </table>
                <br/><br/>
                <input type='submit' name='save' class="formButton2" value='Save'> <input type='submit' name='send' class="formButton2" value='Send'><br/><br/>
                * Save Button is only save countermeasure temporary (not yet send to AAIJ)<br/>
                * Send Button is send countermeasure to AAIJ (completed)
                <br><br>
            </form>
        </center>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    

</script>
@endsection