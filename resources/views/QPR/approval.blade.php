<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<table border="1" cellpadding="0" cellspacing="0" width="1000px" height="100px" align="center"
    style='table-layout:fixed;border-collapse: collapse;'>
    <tr>
        <td width="40%" align='center' colspan='8'>
            <font size="6">PT. AKEBONO BRAKE ASTRA INDONESIA</font><br>
            <font size='2'>Jl. Pegangsaan Dua Blok 1A, Km. 1.6 Pegangsaan Dua, Kelapa Gading, Jakarta Utara, 14250 - Fax
                No. 46822732</font>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <table border="0" cellpadding="0" cellspacing="0" width="90%" height="40%">
                <tr>
                    <td width="90%" nowrap style='padding:0px 5px 0px 5px'>
                        <font size="6"><b>Quality Problem Report [QPR]</b></font>
                    </td>
                </tr>
            </table>
        </td>
        <td colspan="4">
            <table border="0" cellpadding="0" cellspacing="0" width="90%" height="40%">
                <tr>
                    <td style='padding:0px 3px 0px 3px'>
                        <font size='3'>Revise : {{ $data['qpr_revise'] }}</font>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <font size='3'>ID : {{ $data['id'] }}</font>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3"><input name="checkbox" type="checkbox" value="checkbox" checked="checked" />
                        <span>Information for Investigation</span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td rowspan="2" colspan="3" valign="top" style='padding:0px 3px 0px 3px'>
            <font size="2">Supplier :</font>{{ $data['qpr_supplier'] }}
        </td>
        <td colspan="2" style='padding:0px 3px 0px 3px'>
            <font size="2">QPR No :{{ $data['qpr_no'] }} </font>
        </td>
        <td rowspan="2" colspan="2" align="center" valign="top" style='padding:0px 3px 0px 3px'>
            <font size="2">Quality Rank : </font><br><b> {{ $data['qpr_rank'] }}</b><br>
        </td>
        <td rowspan="3" align="center" style='padding:0px 3px 0px 3px'>
            <font size="2">Originator : </font><br>
            <font align="center"><b> QC {{ $data['found_at'] }} AAIJ </b></font>
        </td>
    </tr>
    <td>
        <font size="2" style='padding:0px 3px 0px 3px'>Date : {{ date_format(date_create($data['qpr_date_issue']), 'd-M-Y') }}</font>
    </td>
    <td>
        <font size="2" style='padding:0px 3px 0px 3px'>Found At : {{ $data['found_at'] }}</font>
    </td>
    <tr>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Material Name : {{ $data['qpr_part_name'] }} </font>
        </td>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Type : {{ $data['qpr_type'] }}</font>
        </td>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Part Number : {{ $data['qpr_item'] }}</font>
        </td>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Qty Ng : {{ $data['qpr_qty_ng'] }}</font>
        </td>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Qty Delivery : {{ $data['qpr_qty_dev'] }} </font>
        </td>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Ratio (%) : {{-- $data['qpr_qty_ng'] --}}</font>
        </td>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Occur (in years) : {{ $data['qpr_occ'] }}</font>
        </td>
    </tr>
    <tr>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Lot No./batch No./case No. : {{ $data['qpr_lot'] }} </font>
        </td>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Invoice No : {{ $data['qpr_inv_no'] }}</font>
        </td>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Invoice Date : {{ date_format(date_create($data['qpr_date_invoice']), 'd-M-Y') }}</font>
        </td>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Delivery Date : {{ date_format(date_create($data['qpr_date_delivery']), 'd-M-Y') }}</font>
        </td>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Receiving Date : {{ date_format(date_create($data['qpr_date_receiving']), 'd-M-Y') }} </font>
        </td>
        <td style='padding:0px 3px 0px 3px'>
            <font size="2">Found Date : {{ date_format(date_create($data['qpr_date_found']), 'd-M-Y') }} </font>
        </td>
        <td colspan="2" style='padding:0px 3px 0px 3px'>
            <font size="2">Point of Detection : {{ $data['qpr_point'] }}</font>
        </td>
    </tr>
    <tr>
        <td colspan="4" height="50%" valign="top" style='padding:0px 3px 0px 3px'>
            <font size="2"><b><i>Description of Nonconformity </i></b><br><br> {{ $data['qpr_deskripsi'] }}</font>
        </td>
        <td colspan="4" height="50%" align="center" style='padding:0px 3px 0px 3px'>
            <font size="2"><i>Picture/Sketch</i><br> <img align="center" width="300"
                    src="{{ asset($data['gambar_qpr']) }}" /> </font><br /><br />
        </td>
    </tr>
    <tr>
        <td colspan="8" height="50%" style='padding:0px 3px 0px 3px'>
            <font size="2"><b>Qty Sort :</b> {{ $data['qpr_qty_sortir'] }}</font>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font size="2"><b>NG Sort :</b>
                {{ $data['qpr_ng_sortir'] }}</font>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font size="2"><b>Mhr Sort :</b>
                {{ $data['qpr_mh_sortir'] }}
        </td>
        </font>

    </tr>
    <tr>
        <td colspan="4" style='padding:0px 3px 0px 3px'>
            <font color="#FF0000"> &nbsp;&nbsp;<b><i><u>Attention :</u></i></b> <br>&nbsp;&nbsp;&nbsp;&nbsp;NG Parts
                should be taken from AAI Warehouse within 7 days <br>&nbsp;&nbsp;&nbsp; after the problem founded, if
                not Parts will be Scrap.</font>
        </td>
        <td rowspan="2" valign="top" align="center" style='padding:0px 3px 0px 3px'>Disposition : <br><br><br><b>
                <font size="4">{{ $data['qpr_disposition'] }}</font>
            </b></td>
        <td colspan="3" style='padding:0px 3px 0px 3px' align='center'>
            <font size="2"><b><i> AAIJ Quality Authorized Digitally Signature :</i></b></font>
        </td>
    </tr>
    <tr>
        <td colspan="4" style='padding:0px 3px 0px 3px'>
            <font color="#0000FF">&nbsp;&nbsp;Please re-send your countermeasures within 7 days after received
        </td>

        <?php if ($level == 'supplier') {
        echo "<td valign='top' align='center' style='padding:0px 3px 0px 3px'>
            <font size='2'><b>Approved by</b><br><br>" . $data['qpr_approved'] . '<br />on ' . date_format(date_create($data['qpr_date_charge']), 'd-M-Y')
                . '</font>
        </td>';

        echo "<td valign='top' align='center' style='padding:0px 3px 0px 3px'>
            <font size='2'><b>Checked by</b><br><br>" . $data['qpr_checked'] . '<br />on ' . date_format(date_create($data['qpr_date_charge']), 'd-M-Y') .
                '</font>
        </td>';

        echo "<td valign='top' align='center' style='padding:0px 3px 0px 3px'>
            <font size='2'><b>In Charge by</b><br><br>" . $data['qpr_charge'] . '<br />on ' . date_format(date_create($data['qpr_date_charge']), 'd-M-Y') .
                '
        </td>
        </font>';
        } else {
        if ($data['qpr_status_approved'] == 'OK') {
        echo "<td valign='top' align='center' style='padding:0px 3px 0px 3px'>
            <font size='2'><b>Approved by</b><br><br>" . $data['qpr_approved'] . '<br />on ' .
                $data['qpr_date_approved'] . '</font>
        </td>';
        } else {
        echo "<td valign='top' align='center' style='padding:0px 3px 0px 3px'>
            <font size='2'><b>Not Yet Approved by</b><br>" . $data['qpr_approved'] . '<br /></font>
        </td>';
        }
        if ($data['qpr_status_checked'] == 'OK') {
        echo "<td valign='top' align='center' style='padding:0px 3px 0px 3px'>
            <font size='2'><b>Checked by</b><br><br>" . $data['qpr_checked'] . '<br />on ' . date_format(date_create($data['qpr_date_checked']), 'd-M-Y') .
                '</font>
        </td>';
        } else {
        echo "<td valign='top' align='center' style='padding:0px 3px 0px 3px'>
            <font size='2'><b>Not yet Checked by</b><br />" . $data['qpr_checked'] . '<br /></font>
        </td>';
        }
        echo "<td valign='top' align='center' style='padding:0px 3px 0px 3px'>
            <font size='2'><b>In Charge by</b><br><br>" . $data['qpr_charge'] . '<br />on ' . date_format(date_create($data['qpr_date_charge']), 'd-M-Y') .
                '
        </td>
        </font>';
        } ?>
    </tr>
</table>
<br><br>
<center>
    <?php
    echo "<form action='/qpr-approval' method='post' name='form1'>";
        ?>
        @csrf
        <?php
        if ($level == 'charge') {
        echo "<td colspan='4'><input type='submit' name='charge' value='Approve'></td>";
        echo "<td colspan='4'><input type='submit' name='reject_charge' value='Reject'></td>";
        } elseif ($level == 'check') {
        echo "<input type='hidden' name='supp_name' value='$data[qpr_supplier]''>";
        echo "<input type='hidden' name='pn' value='$data[qpr_item]'>";
        echo "<input type='hidden' name='qty' value='$data[qpr_qty_ng]'>";
        echo "<input type='hidden' name='effdate' value='$data[qpr_date_issue]'>";
        echo "<input type='hidden' name='qpr_no' value='$data[qpr_no]'>";
        echo "<input type='hidden' name='loc_from' value='$data[found_at_line]'>";
        echo "<input type='hidden' name='loc_to' value='$data[supp_kode]'>";
        echo "<input type='hidden' name='approved' value='$data[qpr_approved]'>";
        echo "<td colspan='4'><input type='submit' name='check' class='btn btn-success' value='Approve'></td>";
        echo "<td colspan='4'><input type='submit' name='reject_check' value='Reject' class='btn btn-danger'></td>";
        } elseif ($level == 'approve') {
        echo "<input type='hidden' name='qpr_no' value='$data[qpr_no]'>";
        echo "<td colspan='4'><input type='submit' name='approve' class='btn btn-success' value='Approve'></td>";
        echo "<td colspan='4'><input type='submit' name='reject_approve' class='btn btn-danger' value='Reject'></td>";
        }

        if ($level == 'supplier') {
            echo "<input type='hidden' name='qpr_no' value='$data[qpr_no]'>";
            echo "<input class='btn btn-success' type='submit' name='qpr_data' value='Read' style='width:140px'>";

            // echo "<td colspan='4'><input type='submit' name='approve' class='btn btn-success' value='Approve'></td>";
            // echo "<td colspan='4'><input type='submit' name='reject_approve' value='Reject'></td>";

            // echo "<form name='form_read' action='qpr-approval?qpr_no=" . $data['qpr_no'] . ' &revisi=' . $data['revise'] . "'method='POST'>";
            // echo "<input class='formButton' type='submit' name='qpr_data' value='Read' style='width:140px'> </form>";
            // echo '<br />';
            // echo "<form name='form_respon' action='update_status_respon.php?" . paramEncrypt(' qpr_no=' . $data['qpr_no'] . '
            // &revisi=' . $data['revise']) . "' method='POST'>";
            // echo "<input class='formButton' type='submit' name='save_data' value='Countermeasure' style='width:140px'>
            // </form>";
        }
        echo '</form>';
    ?>
</center>
