@extends('templates.main')

@section('title', 'List Of QPR Customer (Non Release)')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
                <div class="card-header">
                    <h3 class="main-content-label mb-3">@yield('title')</h3>
                </div>
            <div class="card-body">

                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center w-100" id="main-table">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Claim No</th>
                                <th scope="col">Part Name</th>
                                <th scope="col">Part No</th>
                                <th scope="col">Model</th>
                                <th scope="col">Lot no assy</th>
                                <th scope="col">Info Date</th>
                                <th scope="col">Out Of Problem</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodymain">
                            <?php $i = 1; foreach ($data as $row) { ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $row->customer_no_claim }}</td>
                                    <td>{{ $row->customer_name_part }}</td>
                                    <td>{{ $row->customer_no_part }}</td>
                                    <td>{{ $row->customer_model }}</td>
                                    <td>{{ $row->customer_lot_assy }}</td>
                                    <td>{{ $row->customer_occur_date }}</td>
                                    <td>{{ $row->customer_problem }}</td>
                                    <td> Release </td>
                                </tr>
                                <?php $i++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
