<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Quality Problem Report</title>
</head>
<body>
	<table border="1" cellpadding="0" cellspacing="0" width="1000px" height="100px" align="center" style='table-layout:fixed;border-collapse: collapse;'>
		<tr>
			<td width="40%" align='center' colspan='8'><font size="6">PT. AKEBONO BRAKE ASTRA INDONESIA</font><br> <font size='2'>Jl. Pegangsaan Dua Blok 1A, Km. 1.6 Pegangsaan Dua, Kelapa Gading, Jakarta Utara, 14250 - Fax No. 46822732</font></td>
		</tr>
		<tr>
			<td colspan="4">
				<table border="0" cellpadding="0" cellspacing="0" width="90%" height="40%">
					<tr>
						<td width="90%" nowrap style='padding:0px 5px 0px 5px'><font size="6" ><b>Quality Problem Report [QPR]</b></font></td>
					</tr>
				</table>
			</td>
			<td colspan="4">
				<table border="0" cellpadding="0" cellspacing="0" width="90%" height="40%">
					<tr>
						<td style='padding:0px 3px 0px 3px'><font size='3'>Revise : {{ $data->qprl_revise }}</font></td>
						<td>&nbsp;</td>
						<td><font size='3'>ID : {{ $data->qprl_id }}</font></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3"><input name="checkbox" type="checkbox" value="checkbox" checked="checked" />
						<span>Information for Investigation</span></td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td rowspan="2" colspan="3" valign="top" style='padding:0px 3px 0px 3px'><font size="2">Supplier :</font> {{ $data->qprl_supplier_name }}</td>
			<td colspan="2" style='padding:0px 3px 0px 3px'><font size="2" >QPR No : {{ $data->qprl_no }} </font></td>
			<td rowspan="2" colspan="2" align="center" valign="top" style='padding:0px 3px 0px 3px'><font size="2">Quality Rank : </font><br><b> {{ $data->qprl_rank }}</b><br></td>
			<td rowspan="3" align="center" style='padding:0px 3px 0px 3px'><font size="2">Originator : </font><br><font align="center"><b> QC {{ $data->qprl_found_at }} AAIJ </b></font></td>
		</tr>
			<td><font size="2" style='padding:0px 3px 0px 3px'>Date : {{ $data->qprl_date_issue }}</font> </td>
			<td><font size="2" style='padding:0px 3px 0px 3px'>Found At : {{ $data->qprl_found_at }}</font></td>
		<tr>
			<td style='padding:0px 3px 0px 3px'><font size="2">Material Name :  {{ $data->qprl_part_name }} </font></td>
			<td style='padding:0px 3px 0px 3px'><font size="2">Type :  {{ $data->qprl_type }}</font></td>
			<td style='padding:0px 3px 0px 3px'><font size="2">Part Number :   {{ $data->qprl_item }}</font></td>
			<td style='padding:0px 3px 0px 3px'><font size="2">Qty Ng : {{ $data->qprl_qty_ng }}</font></td>
			<td style='padding:0px 3px 0px 3px'><font size="2">Qty Delivery : {{ $data->qprl_qty_det }} </font></td>
			{{-- <td style='padding:0px 3px 0px 3px'><font size="2">Ratio (%) : {{ $data->qprl_qty_ng }} </font></td> --}}
			<td style='padding:0px 3px 0px 3px'><font size="2">Ratio (%) : </font></td>
			<td style='padding:0px 3px 0px 3px'><font size="2">Occur (in years) : {{ $data->qprl_occ }}</font></td>
		</tr>
		<tr>
			<td style='padding:0px 3px 0px 3px'><font size="2">Lot No./batch No./case No. :  {{ $data->qprl_lot }} </font></td>
			<td style='padding:0px 3px 0px 3px'><font size="2">Invoice No :  {{ $data->qprl_inv_no }}</font></td>

            @php
                if(substr($data->qprl_date_invoice,0,4) < 2014) {
                    $qpr_date_invoice = "";
                } else {
                    $qpr_date_invoice = $data->qprl_date_invoice;
                }

                if(substr($data->qprl_date_delivery,0,4) < 2014) {
                    $qpr_date_delivery = "";
                } else {
                    $qpr_date_delivery = $data->qprl_date_delivery;
                }

                if(substr($data->qprl_date_receiving,0,4) < 2014) {
                    $qprl_date_receiving = "";
                } else {
                    $qprl_date_receiving = $data->qprl_date_receiving;
                }

                if(substr($data->qprl_date_found,0,4) < 2014) {
                    $qprl_date_found = "";
                } else {
                    $qprl_date_found = $data->qprl_date_found;
                }
            @endphp

			<td style='padding:0px 3px 0px 3px'><font size="2">Invoice Date :  {{ $qpr_date_invoice }}</font></td>
			<td style='padding:0px 3px 0px 3px'><font size="2">Delivery Date :{{ $qpr_date_delivery }}</font></td>
			<td style='padding:0px 3px 0px 3px'><font size="2">Receiving Date : {{ $qprl_date_receiving }} </font></td>
			<td style='padding:0px 3px 0px 3px'><font size="2">Found Date : {{ $qprl_date_found }} </font></td>
			<td colspan="2" style='padding:0px 3px 0px 3px'><font size="2">Point of Detection : {{ $data->qprl_point }}</font></td>
		</tr>
		<tr>
			<td colspan="4" height="50%" valign="top" style='padding:0px 3px 0px 3px'><font size="2"><b><i>Description of Nonconformity </i></b><br><br> {{ $data->qprl_deskripsi }} </font></td>
			<td colspan="4" height="50%" align="center" style='padding:0px 3px 0px 3px'><font size="2"><i>Picture/Sketch</i><br> <img align="center" width="300" src="{{ asset($data->qprl_picture) }}"/> </font><br/><br/></td>
		</tr>
		<tr>
            @php
                if($qpr_sortir == 0) {
                    $qpr_qty_sortirend = "-";
                    $qpr_ng_sortirend = "-";
                    $qpr_mh_sortirend = "-";
                } else {
                    $qpr_qty_sortirend = $qpr_qty_sortir;
                    $qpr_ng_sortirend = $qpr_ng_sortir;
                    $qpr_mh_sortirend = $qpr_mh_sortir;
                }
            @endphp
			<td colspan="8" height="50%" style='padding:0px 3px 0px 3px'><font size="2"><b>Qty Sort :</b> {{ $qpr_qty_sortirend }}</font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font size="2"><b>NG Sort :</b> {{ $qpr_ng_sortirend }}</font>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font size="2"><b>Mhr Sort :</b> {{ $qpr_mh_sortirend }}</td></font>

		</tr>
		<tr>
			<td colspan="4" style='padding:0px 3px 0px 3px'><font color="#FF0000"> &nbsp;&nbsp;<b><i><u>Attention :</u></i></b> <br>&nbsp;&nbsp;&nbsp;&nbsp;NG Parts should be taken from AAI Warehouse within 7 days <br>&nbsp;&nbsp;&nbsp; after the problem founded, if not Parts will be Scrap.</font></td>
			<td rowspan="2" valign="top" align="center" style='padding:0px 3px 0px 3px'>Disposition : <br><br><br><b><font size="4">{{ $data->qprl_disposition }}</font></b></td>
			<td colspan="3" style='padding:0px 3px 0px 3px' align='center'><font size="2"><b><i> AAIJ Quality Authorized Digitally Signature :</i></b></font></td>
		</tr>
		<tr>
			<td colspan="4" style='padding:0px 3px 0px 3px'><font color="#0000FF">&nbsp;&nbsp;Please re-send your countermeasures within 7 days after received </td>

            @if(Auth::guard('pub_login')->user()->previllage == 'Supplier')
                <td valign='top' align='center' style='padding:0px 3px 0px 3px'><font size='2'><b>Approved by</b><br><br>{{ $data->qprl_approved }}<br/>on {{ $data->qprl_charge_date }}</font></td>;

                <td valign='top' align='center' style='padding:0px 3px 0px 3px'><font size='2'><b>Checked by</b><br><br>"{{ $data->qprl_checked }}"<br/>on {{ $data->qprl_charge_date }}</font></td>;

                <td valign='top' align='center' style='padding:0px 3px 0px 3px'><font size='2'><b>In Charge by</b><br><br>{{ $data->qprl_charge }} <br/>on {{ $data->qpr_charge_date }}</td></font>;
            @else
                @if($data->qprl_approved_status == 'OK')
                    <td valign='top' align='center' style='padding:0px 3px 0px 3px'><font size='2'><b>Approved by</b><br><br>{{ $data->qprl_approved }}<br/>on {{ $data->qprl_approved_date }}</font></td>
                @else
                    <td valign='top' align='center' style='padding:0px 3px 0px 3px'><font size='2'><b>Not Yet Approved by</b><br>{{ $data->qprl_approved }}<br/></font></td>
                @endif

                @if($data->qprl_checked_status == 'OK')
                    <td valign='top' align='center' style='padding:0px 3px 0px 3px'><font size='2'><b>Checked by</b><br><br>{{ $data->qprl_checked }}<br/>on {{ $data->qprl_checked_date }}</font></td>
                @else
                    <td valign='top' align='center' style='padding:0px 3px 0px 3px'><font size='2'><b>Not yet Checked by</b><br/>{{ $data->qprl_checked }}<br/></font></td>
                @endif
                <td valign='top' align='center' style='padding:0px 3px 0px 3px'><font size='2'><b>In Charge by</b><br><br>{{ $data->qprl_charge }}<br/>on {{ $data->qprl_charge_date }}</td></font>
            @endif

		</tr>
	</table>
</body>
<script language="javascript">
    window.print();
</script>
