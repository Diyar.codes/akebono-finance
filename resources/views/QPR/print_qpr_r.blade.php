@extends('templates.main')

@section('title', 'Print Quality Problem Report')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
                <div class="card-header">
                    <h3 class="main-content-label mb-3">@yield('title')</h3>
                </div>
            <div class="card-body">

				<div class="card col-lg-6 mb-3">
                    <h3 class="card-header">QPR Number</h3>
                        
                      <form class="form-inline mt-3">
                        <div class="col-auto">
                            <label class="sr-only" for="inlineFormInputGroup">Supplier</label>
                            <div class="input-group mb-2">
                                <input type="text" class="form-control ml-2" id="inlineFormInputGroup" placeholder="QPR Number">
                                <button type="button" class="btn ripple btn-info btn-with-icon ml-2" id="updateItemKanban" data-target="#modaldemo1" data-toggle="modal"><i class="fas fa-search"></i> Search</button>
                            </div>
                        </div>   
                    </form>

                    <div class="card-footer">
                        <button class="btn ripple btn-success  btn-with-icon" id="printQprR"><i class="fe fe-printer"></i> Cetak</button>
                    </div>
				</div>

            </div>
        </div>
    </div>
</div>

<!-- Basic modal -->
<div class="modal" id="modaldemo1">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header">
                <h6 class="modal-title">QPR Lookup</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center w-100" id="main-table">
                        <thead>
                            <tr>
                                <th scope="col">QPR No</th>
                                <th scope="col">QPR Id</th>
                                <th scope="col">Suppluer Name</th>
                                <th scope="col">Part Name</th>
                            </tr>
                        </thead>
                        <tbody class="tbodymain">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn ripple btn-primary" type="button">Save changes</button>
                <button class="btn ripple btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Basic modal -->
@endsection