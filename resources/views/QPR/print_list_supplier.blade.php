 @extends('templates.main')

@section('title', 'Countremeasure')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        
        <div id="printList">
            
            <table border="1" cellpadding="0" cellspacing="0" width="1000px" height="100px" align="center" style='table-layout:fixed;border-collapse: collapse;'>
                <tr>
                    <td width="40%" align='center' colspan='8'><font size="6">PT. AKEBONO BRAKE ASTRA INDONESIA</font><br> <font size='2'>Jl. Pegangsaan Dua Blok 1A, Km. 1.6 Pegangsaan Dua, Kelapa Gading, Jakarta Utara, 14250 - Fax No. 46822732</font></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table border="0" cellpadding="0" cellspacing="0" width="90%" height="40%">
                            <tr>
                                <td width="90%" nowrap style='padding:0px 5px 0px 5px'><font size="6" ><b>Quality Problem Report [QPR]</b></font></td>
                            </tr>
                        </table>
                    </td>
                    <td colspan="4">
                        <table border="0" cellpadding="0" cellspacing="0" width="90%" height="40%">
                            <tr>
                                <td style='padding:0px 3px 0px 3px'><font size='3'>Revise : {{ $data['qpr_revise']}} </font></td>
                                <td>&nbsp;</td>
                                <td><font size='3'>ID : {{ $data['id']}} </font></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3"><input name="checkbox" type="checkbox" value="checkbox" checked="checked" />
                                <span>Information for Investigation</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td rowspan="2" colspan="3" valign="top" style='padding:0px 3px 0px 3px'><font size="2">Supplier : {{$data['qpr_supplier']}} </font></td>
                    <td colspan="2" style='padding:0px 3px 0px 3px'><font size="2" >QPR No : {{$data['qpr_no']}} </font></td>
                    <td rowspan="2" colspan="2" align="center" valign="top" style='padding:0px 3px 0px 3px'><font size="2">Quality Rank : {{ $data['qpr_rank']}} </font><br><b> </b><br></td>
                    <td rowspan="3" align="center" style='padding:0px 3px 0px 3px'><font size="2">Originator : {{ $data['found_at']}} </font><br><font align="center"><b> QC  AAIJ </b></font></td>
                </tr>
                    <td><font size="2" style='padding:0px 3px 0px 3px'>Date : {{ $data['qpr_date_issue']}}  </font> </td>
                    <td><font size="2" style='padding:0px 3px 0px 3px'>Found At : {{ $data['found_at']}} </font></td>
                <tr>
                    <td style='padding:0px 3px 0px 3px'><font size="2">Material Name : {{ $data['qpr_part_name']}}</font></td>   
                    <td style='padding:0px 3px 0px 3px'><font size="2">Type : {{ $data['qpr_type']}} </font></td>
                    <td style='padding:0px 3px 0px 3px'><font size="2">Part Number :{{ $data['qpr_item']}}</font></td>
                    <td style='padding:0px 3px 0px 3px'><font size="2">Qty Ng : {{ $data['qpr_qty_ng']}}</font></td>
                    <td style='padding:0px 3px 0px 3px'><font size="2">Qty Delivery :{{ $data['qpr_qty_dev']}}</font></td>
                    <td style='padding:0px 3px 0px 3px'><font size="2">Ratio (%) :  </font></td>
                    <td style='padding:0px 3px 0px 3px'><font size="2">Occur (in years) :{{ $data['qpr_occ']}} </font></td>
                </tr>
                <tr>
                    <td style='padding:0px 3px 0px 3px'><font size="2">Lot No./batch No./case No. :{{ $data['qpr_lot']}}   </font></td>    
                    <td style='padding:0px 3px 0px 3px'><font size="2">Invoice No :{{ $data['qpr_inv_no']}}  </font></td>
                    <td style='padding:0px 3px 0px 3px'><font size="2">Invoice Date :{{ $data['qpr_date_invoice']}} </font></td>
                    <td style='padding:0px 3px 0px 3px'><font size="2">Delivery Date :{{ $data['qpr_date_delivery']}} </font></td>
                    <td style='padding:0px 3px 0px 3px'><font size="2">Receiving Date :{{ $data['qpr_date_receiving']}} </font></td>
                    <td style='padding:0px 3px 0px 3px'><font size="2">Found Date :{{ $data['qpr_date_found']}} </font></td>
                    <td colspan="2" style='padding:0px 3px 0px 3px'><font size="2">Point of Detection : {{ $data['qpr_point']}}</font></td>
                </tr>
                <tr>
                    <td colspan="4" height="50%" valign="top" style='padding:0px 3px 0px 3px'><font size="2"><b><i>Description of Nonconformity </i></b><br><br> {{ $data['qpr_deskripsi']}} </font></td> 
                    <td colspan="4" height="50%" align="center" style='padding:0px 3px 0px 3px'><font size="2"><i>Picture/Sketch</i><br> <img align="center" width="300" src=""/> </font><br/><br/></td>  
                </tr>
                <tr>
                    <td colspan="8" height="50%" style='padding:0px 3px 0px 3px'>
                        @if($data['qpr_qty_sortir'] == 0)
                        <font size="2"><b>Qty Sort : {{ $data['qpr_qty_sortir'] }} </b></font> 
                        @else
                        <font size="2"><b>Qty Sort : - </b></font> 
                        @endif
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        @if($data['qpr_ng_sortir'] == 0)
                        <font size="2"><b>NG Sort : {{ $data['qpr_ng_sortir'] }}</b> </font>  
                        @else
                        <font size="2"><b>NG Sort : - </b> </font>  
                        @endif
                        @if($data['qpr_mh_sortir'] == 0)
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        <font size="2"><b>Mhr Sort : {{ $data['qpr_mh_sortir'] }}</b> </font></td>
                        @else
                        <font size="2"><b>Mhr Sort : - </b> </font></td>
                        @endif
                    
                </tr>
                <tr>    
                    <td colspan="4" style='padding:0px 3px 0px 3px'><font color="#FF0000"> &nbsp;&nbsp;<b><i><u>Attention :</u></i></b> <br>&nbsp;&nbsp;&nbsp;&nbsp;NG Parts should be taken from AAI Warehouse within 7 days <br>&nbsp;&nbsp;&nbsp; after the problem founded, if not Parts will be Scrap.</font></td>
                    <td rowspan="2" valign="top" align="center" style='padding:0px 3px 0px 3px'>Disposition : {{ $data['qpr_disposition'] }} <br><br><br><b><font size="4"></font></b></td>
                    <td colspan="3" style='padding:0px 3px 0px 3px' align='center'><font size="2"><b><i> AAIJ Quality Authorized Digitally Signature :</i></b></font></td>
                </tr>
                <tr>    
                    <td colspan="4" style='padding:0px 3px 0px 3px'><font color="#0000FF">&nbsp;&nbsp;Please re-send your countermeasures within 7 days after received </td>

                    
                </tr>
            </table>
        </div>

        <center>
            <button type="button" class="btn btn-secondary mt-3" onclick="tablePrint('printList')">Print</button>
        </center>
    </div>
    <div class="btn btn-list mt-2">        
        <a href="{{ route('counter-measure')}}" class="btn btn-danger btn-md">
            Back
        </a>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    function tablePrint(printList){
        var printContents = document.getElementById(printList).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;

    }
</script>
@endsection