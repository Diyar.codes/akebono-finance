@extends('templates.main')

@section('title', 'QPR LINE')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
                <div class="card-header">
                    <h3 class="main-content-label mb-3">SURAT CLAIM</h3>
                </div>
            <div class="card-body">
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-3 p-0 ml-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Supplier</span>
                        </div>
                    </div>
                    <div class="col-lg-1 col-2 p-0">
                        <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="code" disabled>
                    </div>
                    <div class="ml-1 mr-1">
                        <button type="submit" class="btn btn-sm btn-sm-responsive btn-info" data-toggle="modal" data-target="#modaldemo8part2" id="seacrhSOAPBusinesRelationModal"><i class="fas fa-search"></i></button>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-4 p-0">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="name" disabled>
                        </div>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-3 p-0 ml-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">From</span>
                        </div>
                    </div>
                    <div class="col-lg-2 col-3 p-0">
                        <input type="date" class="form-control form-control-sm form-control-sm-responsive text-dark" name="date-from" id="date-from">
                    </div>
                    <div class="ml-md-3 mr-md-3 ml-1 mr-1">
                        <span class="exinput-custom-responsive"><b>To</b></span>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-3 p-0">
                        <div class="input-group">
                            <input type="date" class="form-control form-control-sm form-control-sm-responsive text-dark" name="date-to" id="date-to">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-md-3 col-sm-4">
                        <button type="button" class="btn btn-sm btn-sm-responsive btn-success btn-block my-sm-2 my-1" id="search"><i class="fas fa-search"></i> Search</button>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-4">
                        <button type="button" class="btn btn-sm btn-sm-responsive btn-danger btn-block my-sm-2 my-1" id="reset"><i class="fas fa-undo"></i> Reset</button>
                    </div>
                </div>
                <form action="store-surat-claim-qpr" method="POST">
                    @csrf
                    <div class="table-responsive secondtable mt-3">
                        <table id="dataSecondTable" class="display text-center" style="width: 100%">
                            <thead>
                                <tr style="background-color: #0066CC;">
                                    <th scope="col">No</th>
                                    <th scope="col">No QPR</th>
                                    <th scope="col">Supp. Code</th>
                                    <th scope="col">Supp. Name</th>
                                    <th scope="col">Item Number</th>
                                    <th scope="col">Item Desc</th>
                                    <th scope="col">Counter measure</th>
                                    <th scope="col">Claim</th>
                                </tr>
                            </thead>
                            <tbody class="tblseconded">
                            </tbody>
                        </table>
                        <div class="row my-4 t-automatical" style="display: none">
                            <div class="col-lg-2 col-sm-4">
                                <button type="submit" class="btn btn-sm btn-sm-responsive btn-success btn-block mb-2" id="suratclaiminsertbtn"><i class="fas fa-save"></i> Create</button>
                            </div>
                            <div class="col-lg-2 col-sm-4">
                                <button type="reset" class="btn btn-sm btn-sm-responsive btn-danger btn-block mb-2"><i class="fas fa-undo"></i> Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="modaldemo8part2">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="get-supplier">
                        <thead class="thead">
                            <tr style="background-color: #0066CC;">
                                <th scope="col">Supplier Id</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection
