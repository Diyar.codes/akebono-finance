@extends('templates.main')

@section('title', 'Lampiran Verifikasi')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <form action="/all-list-qpr-verifikasi-update/{{ base64_encode($data->qprl_no) }}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="row mb-2 ml-2">
                        <div class="col-xl-1 col-lg-2 col-3 p-0 mb-xl-0 mb-2 mb-2-responsive">
                            <div class="input-group">
                                <span class="exinput-custom-responsive">File</span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-8 p-0 mb-xl-0 mb-2 mb-2-responsive">
                            {{-- <input type="file" class="form-control form-control-sm  form-control-sm-responsive text-dark" id="fileverifikasi" name="fileverifikasi" autocomplete="off"> --}}
                            <div class="form-group">
                                <input type="file" class="form-control-file form-control-sm" id="vimagename" name="vimagename" autocomplete="off">
                            </div>
                            @error('vimagename')
                                <div class="error text-danger mt-n2">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-2 ml-2">
                        <div class="col-xl-1 col-lg-2 col-3 p-0 mb-xl-0 mb-2 mb-2-responsive">
                            <div class="input-group">
                                <span class="exinput-custom-responsive">Date</span>
                            </div>
                        </div>
                        {{-- @dd($data->qprl_ver_date) --}}
                        <div class="col-xl-2 col-4 p-0 mb-xl-0 mb-2 mb-2-responsive">
                            {{-- <input type="date" class="form-control form-control-sm  form-control-sm-responsive text-dark" id="vtanggal" name="vtanggal" autocomplete="off" value="{{ old($data->qprl_ver_date) }}<?= date('Y-m-d') ?>"> --}}
                            <input type="date" class="form-control form-control-sm  form-control-sm-responsive text-dark" id="vtanggal" name="vtanggal" autocomplete="off" value="{{ !empty($data->qprl_ver_date) ? \Carbon\Carbon::parse($data->qprl_ver_date)->format('Y-m-d') : date('Y-m-d') }}">
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-lg-2 col-sm-4 ml-3">
                            <button type="submit" class="btn btn-sm btn-success btn-block mb-2" id="updateItemKanban"><i class="fas fa-save"></i> Save</button>
                        </div>
                        <div class="col-lg-2 col-sm-4">
                            <a href="{{ route('all-list-qpr') }}"><button type="button" class="btn btn-sm btn-danger btn-block btn-md"><i class="fas fa-undo"></i> Back</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
