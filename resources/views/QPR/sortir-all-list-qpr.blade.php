@extends('templates.main')

@section('title', 'Quality Problem Report')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-header">
                <h3 class="main-content-label mb-3">@yield('title')</h3>
            </div>
            <form action="{{ route('store-sortir-qpr') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <input type="hidden" name="var" value="<?= $data['var'] ?>">

                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">from</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" name="supp_name" autocomplete="off" value="<?= $data['qprl_supplier_name']?>">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">ID</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" name="supp" autocomplete="off" value="<?= $data['qprl_supplier']?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">No QPR</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" name="no" autocomplete="off" value="<?= $data['qprl_no']?>">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Issue Date</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" name="issu" autocomplete="off" value="<?= $data['qprl_date_issue']?> ">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Fax</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" name="fax" autocomplete="off" value="<?= $data['qprl_fax']?>">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Found Date</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" name="found" autocomplete="off" value="<?= $data['qprl_date_found'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Part Name</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" name="name" autocomplete="off" value="<?= $data['qprl_part_name']?>">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Delivery Date</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" name="deliv" autocomplete="off">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Part Number</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_date_receiving']?>">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Receiving Date</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_date_receiving']?>" name="receive">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Material Name</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_material']?>">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Invoice Date</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_date_invoice']?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Material Type</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Ocurence</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_occ'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Qty NG</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_qty_ng'] . $data['qprl_qty_ng_satuan'] ?>">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Qty Det</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_qty_det'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Invoice No</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_inv_no'] ?>">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">lot No</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?=  $data['qprl_lot']?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Disposition</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_disposition'] ?>">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Information</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_informasi'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Revise</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" name='revise' autocomplete="off" value="<?= $data['jml'] ?>">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Description</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_deskripsi'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">image</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <img src="" alt="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Found At</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_found_at'] ?>">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">In Charge</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_charge'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2 box mr-sm-3">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Checked</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_checked'] ?>">
                        </div>
                        <div class="col-2 box mb-1">
                            <div class="input-group mb-1">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Approved</span>
                            </div>
                        </div>
                        <div class="col-2 box mb-1">
                            <input type="text" class="form-control form-control-sm text-dark" autocomplete="off" value="<?= $data['qprl_approved'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Supplier</th>
                                        <th>AAIJ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Qty Sortir</td>
                                        <td><input type="text" name="qty" value="<?= $data['qprl_qty_sortir_vd'] ?>"></td>
                                        <td><input type="text" name="qty1" value="<?= $data['qprl_qty_sortir_aaij'] ?>"></td>
                                    </tr>
                                    <tr>
                                        <td>NG Sortir</td>
                                        <td><input type="text" name="sortir" value="<?= $data['qprl_ng_sortir_vd'] ?>"></td>
                                        <td><input type="text" name="sortir1" value="<?= $data['qprl_ng_sortir_aaij'] ?>"></td>
                                    </tr>
                                    <tr>
                                        <td>MHR Sortir</td>
                                        <td><input type="text" name="mhr" value="<?= $data['qprl_mhr_sortir_vd'] ?>"></td>
                                        <td><input type="text" name="mhr1" value="<?= $data['qprl_mhr_sortir_aaij'] ?>"></td>
                                    </tr>
                                    <tr>
                                        <td>Periode Sortir</td>
                                        <td><input type="text" name="start_date" value="<?= $data['qprl_start_date'] ?>"></td>
                                        <td><input type="text" name="finish_date" value="<?= $data['qprl_end_date'] ?>"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                        <div class="btn btn-list mt-2">
                            <button type="submit" class="btn btn-success my-sm-2 my-1" id="search"><i class="fas fa-search"></i> Save </button>
                            <button type="button" class="btn btn-danger my--sm-2 my-1" id="reset" onclick="window.location.reload();"><i class="fas fa-undo"></i> cancel</button>
                        </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
