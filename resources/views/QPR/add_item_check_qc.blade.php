 @extends('templates.main')

@section('title', 'Add Item Check QC')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <input type="hidden" name="total_data" id="totalData">
        <div class="card custom-card overflow-hidden">
                <div class="card-header">
                    <h3 class="main-content-label mb-3">@yield('title')</h3>
                </div>
            <div class="card-body">

              <div class="form-row col-md-6">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Item Number</label>
                    <input type="text" class="form-control" style="color:black" id="itemnnumber" placeholder="Item Number">
                    <button type="button" class="btn btn-info mt-3" id="ItemNum" onclick="searchItemNum()"></i> Search </button>
                </div>
              </div>

            <div class="table-responsive mb-2">
                <table id="tAddCheckData" class="table table-striped table-bordered text-center w-100" id="main-table">
                    <thead>
                        <tr>
                            <th scope="col">Item Number</th>
                            <th scope="col">Description 1</th>
                            <th scope="col">Description 2</th>
                            <th scope="col">UM</th>
                            <th scope="col">Supplier ID</th>
                            <th scope="col">Supllier Name</th>
                            <th scope="col">Check</th>
                        </tr>
                    </thead>
                    <tbody id="tbodyAddCheck"></tbody>
                </table>
            </div>
            <div class="row mb-2">
                <div class="col-lg-2 col-sm-4">
                    <button type="submit" class="btn btn-success btn-block mb-2" id="btnSaveItem" onclick="saveDataItem()"><i class="fas fa-save"></i> Save</button>
                </div>
                <div class="col-lg-2 col-sm-4">
                    <button type="button" class="btn btn-danger btn-block mb-2" onclick="window.location.reload();"><i class="fas fa-undo"></i> Cancel</button>
                </div>
            </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">

$(document).ready(function() {

    $("table thead th").css("background-color", "#0066CC");
    $("table thead th").css("color", "#fff");
    $('#tAddCheckData').DataTable({
        responsive: true,
        ordering:false,
    });
});
    
function searchItemNum() {

    var itemNumber = document.getElementById("itemnnumber").value;
    getLoader();
    $.ajax({
        type: "GET",
        url: "/search-add-item-check-qc",
        data:{
            numItem : itemNumber
        },
        success: function (data) {
            $('#tAddCheckData').DataTable().destroy();
            $('#tbodyAddCheck').html(data);
            // $("#buttonsave").show();

            $('#tAddCheckData').DataTable({
                ordering : false,
                scrollY: "400px",
                paging : false
            });
        },
        error: function(error){
            SW.error({
                message: 'Data is not Defined!!!',
            });
        }
    });
}

function checkedf(urut){
    // alert(urut);
    var checkqc = $('[id^="checkqc"]').map(function () { return $(this).val(); }).get();
    var hitung = urut-1;
    if(checkqc[hitung] == 'off'){
        document.getElementById("checkqc["+parseInt(urut)+"]").value = 'on';
    }else{
        document.getElementById("checkqc["+parseInt(urut)+"]").value = 'off';
    }
}

function saveDataItem() {
    var part = $('[id^="part"]').map(function () { return $(this).val(); }).get();
    // var suppname = $('[id^="SuppName"]').map(function () { return $(this).val(); }).get();
    var checkqc = $('[id^="checkqc"]').map(function () { return $(this).val(); }).get();
    // var checkedValue = $('#checkqc:checked').val();
    var total = document.getElementById("jml").value;

    $.ajax({
        type:"POST",
        url: "/store-add-item",
        data : {
            _token: $('#token').val(),
            part: part,
            // nameSupplier: suppname,
            checkqc: checkqc,
            total: total
        },
        success: function(data){
            // alert(data);
            SW.success({
                message: 'Save Data Successfully!',
            });
            var url = `${BASEURL}item-check-qc`;
                    
            setTimeout(function(){ 
                window.location.href = url;
            }, 1400);
        },
        error: function(error){
            SW.error({
                message: 'Failed to Save Data!!',
            });
        }

    })
}




</script>
@endsection
