 @extends('templates.main')

@section('title', 'Countremeasure')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <input type="hidden" name="total_data" id="totalData">
        <div class="card custom-card overflow-hidden">
                <div class="card-header">
                    <h2 class="main-content-label mb-3">@yield('title')</h2>
                </div>
            @if($errors->any())
                <h4>{{$errors->first()}}</h4>
            @endif
            <div class="card-body">

            <div class="card col-lg-4 mb-3">
                <div class="form-group card-body">
                        <label class="main-content-label tx-11 tx-medium">QPR No</label>
                        <input class="form-control textblack" type="text" name="noQpr" id="QPRNo" required="">
                        <button type="button" class="btn btn-info btn-md mt-3" id="ItemNum" onclick="searchItemCounterM()"><i class="fas fa-search"></i> Search </button>
                        <button type="button" class="btn btn-danger btn-md mt-3" id="ItemNum" onclick="window.location.reload();"><i class="fas fa-undo"></i> Reset </button>
                </div>
            </div>

            <div class="table-responsive mb-2 mt-3">
                <form action="{{ route('excel-counter-measure')}}" method="POST">
                    @csrf
                    <button type="submit" class="btn ripple btn-outline-success btn-sm btn-icon mb-2 counterexcel"data-toggle="tooltip" data-placement="top" title="Download Excel"><i class="si si-cloud-download"></i></button>
                </form>
                <table id="tcounterMeasure" class="display text-center" style="width: 100%">
                    <thead>
                        <tr style="background-color: #0066CC;">
                            <th style="color: white;">No QPR</th>
                            <th style="color: white;">Supp Code</th>
                            <th style="color: white;">Supp Name</th>
                            <th style="color: white;">Item Number</th>
                            <th style="color: white;">Item Desc</th>
                            <th style="color: white;">Countermeasure</th>
                            <th style="color: white;">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tbodyCounter">
                        @foreach($data as $key)
                        <tr>
                            <td> {{$key->qprl_no}} </td>
                            <td> {{$key->qprl_supplier}} </td>
                            <td> {{$key->qprl_supplier_name}} </td>
                            <td> {{$key->qprl_item}} </td>
                            <td> {{$key->qprl_part_name}} </td>
                            <td> {{$key->qprl_part_name}} </td>
                            <td> 
                                <a href="{{ route('print-list-qpr-supplier',['noQpr' => $key->qprl_no])}}"><i class="si si-printer" data-toggle="tooltip" title="" data-original-title="Print" style="color:green"></i></a>
                                <a href="{{ route('countermeasure-action',['noQpr' => $key->qprl_no, 'revisi' => $key->qprl_revise])}}"> <i class="si si-refresh" data-toggle="tooltip" title="" data-original-title="Countermeasure" style="color:blue"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tbody id="tsearchResult"></tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    
$(document).ready(function() {

    $('#tcounterMeasure').DataTable({
        ordering : false
    });

});

function searchItemCounterM() {

    var QprNo = document.getElementById("QPRNo").value;
    $.ajax({
        type: "GET",
        url: "/search-counter-measure",
        data:{
            noqpr  : QprNo
        },
        success: function (data) {

            $('#tbodyCounter').hide();

            var tdata ='';
            $.each(data, function(key, value){
                    tdata += "<tr>";
                    tdata += "<td>"+ value["qprl_no"] +"</td>";
                    tdata += "<td>"+ value["qprl_supplier"] +"</td>";
                    tdata += "<td>"+ value["qprl_supplier_name"] +"</td>";
                    tdata += "<td>"+ value["qprl_item"] +"</td>";
                    tdata += "<td>"+ value["qprl_part_name"] +"</td>";
                    tdata += "<td>"+ value["qprl_part_name"] +"</td>";
                    tdata += "<td>"+ '<a href="{{ route("print-list-qpr-supplier",["noQpr" => $key->qprl_no])}}"><i class="si si-printer" data-toggle="tooltip" title="" data-original-title="Print" style="color:green"></i></a><a href="{{ route("countermeasure-action",["noQpr" => $key->qprl_no, "revisi" => $key->qprl_revise])}}"> <i class="si si-refresh" data-toggle="tooltip" title="" data-original-title="Countermeasure" style="color:blue"></i></a>' +"</td>";
                    tdata += "</tr>"
            });
            document.getElementById("tsearchResult").innerHTML = tdata;

        },
        error: function(error){
            SW.error({
                message: 'Data is not Defined!!!',
            });
        }
    });
}

function saveDataItem() {
    var numitem = $('[id^="NumItem"]').map(function () { return $(this).val(); }).get();
    var suppname = $('[id^="SuppName"]').map(function () { return $(this).val(); }).get();
    var checkbox = $('[id^="checkBox"]').map(function () { return $(this).val(); }).get();
    
    var Dtotal = document.getElementById("totalData").value;

    $.ajax({
        type:"POST",
        url: "/store-item-check-qc",
        data : {
            _token: $('#token').val(),
            itemNum: numitem,
            nameSupplier: suppname,
            checkBox: checkbox,
            total: Dtotal
        },
        success: function(data){
            SW.success({
                message: 'Save Data Successfully!',
            });
        },
        error: function(error){
            SW.error({
                message: 'Failed to Save Data!!',
            });
        }

    })
}


</script>
@endsection