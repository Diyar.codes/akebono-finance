<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Quality Problem Report</title>
</head>
<body>
	<table border="1" cellpadding="0" cellspacing="0" width="900px" height="100px" align="center">
		<tr>
			<td width="40%" align='center' colspan='8'><font size="6">PT. AKEBONO BRAKE ASTRA INDONESIA</font><br> <font size='2'>Jl. Pegangsaan Dua Blok 1A, Km. 1.6 Pegangsaan Dua, Kelapa Gading, Jakarta Utara, 14250 - Fax No. 46822732</font></td>
		</tr>
		<tr>
			<td colspan="4">
				<table border="0" cellpadding="0" cellspacing="0" width="90%" height="40%">
					<tr>
						<td width="90%" nowrap><font size="6" ><b>Quality Problem Report [QPR]</b></font></td>
					</tr>
				</table>
			</td>
			<td colspan="4">
				<table border="0" cellpadding="0" cellspacing="0" width="90%" height="40%">
					<tr>
						<td><font size='3'>Revise : {{ $data->qprl_revise }}</font></td>
						<td>&nbsp;</td>
						<td><font size='3'>ID : {{ $data->qprl_id }}</font></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3"><input name="checkbox" type="checkbox" value="checkbox" checked="checked" />
						<span>Information for Investigation</span></td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td rowspan="2" colspan="3"  valign="top"><font size="2">Supplier :</font> {{ $data->qprl_supplier }}</td>
			<td colspan="2" ><font size="2">QPR No :</font> {{ $data->qprl_no }} </td>
			<td rowspan="2" colspan="2" align="center" valign="top"><font size="2">Quality Rank : </font><br><b> {{ $data->qprl_rank }}</b><br></td>
			<td rowspan="3" align="center" ><font size="2">Originator : </font><br><font align="center"><b> QC receiving AAIJ </b></font></td>
		</tr>
			<td><font size="2">Date : </font>{{ $data->qprl_date_issue }}<br> </td>
			<td><font size="2">Found At : </font></td>
		<tr>
			<td ><font size="2">Material Name : </font><br> {{ $data->qprl_material }} </td>
			<td ><font size="2">Type :</font> <br> {{ $data->qprl_type }}</td>
			<td ><font size="2">Part Number : </font> <br> {{ $data->qprl_part }}</td>
			<td ><font size="2">Qty Ng : </font><br>{{ $data->qprl_qty_ng }}</td>
			<td ><font size="2">Qty Delivery : </font><br>{{ $data->qprl_qty_ng }} </td>
			<td ><font size="2">Ratio (%) : </font><br>{{ $data->qprl_qty_ng }} </td>
			<td ><font size="2">Occur (in years) : </font><br>{{ $data->qprl_qty_ng }}</td>
		</tr>
			<tr>
			<td ><font size="2">Lot No./batch No./case No. :</font> <br> {{ $data->qprl_lot }} </td>
			<td ><font size="2">Invoice No :</font> <br> {{ $data->qprl_inv_no }}</td>
			<td ><font size="2">Invoice Date : </font><br> {{ $data->qprl_date_invoice }}</td>
			<td ><font size="2">Delivery Date : </font><br>{{ $data->qprl_date_delivery }}</td>
			<td ><font size="2">Receiving Date : </font><br>{{ $data->qprl_date_receiving }} </td>
			<td ><font size="2">Found Date : </font><br>{{ $data->qprl_date_found }} </td>
			<td  colspan="2"><font size="2">Point of Detection : </font><br>{{ $data->qprl_disposition }}</td>
		</tr>
		<tr>
			<td  colspan="4" height="50%" valign="top"><font size="2"><b><i>Description of Nonconformity </i></b></font><br><br> {{ $data->qprl_deskripsi }} </td>
			<td  colspan="4" height="50%" align="center"><font size="2"><i>Picture/Sketch</i></font><br> <img align="center" width="300" height="200" src="{{ $data->qprl_picture }}"/> </td>
		</tr>
		<tr>
			<td  colspan="8" height="50%" ><font size="2"><b>Qty Sort :</b></font> {{ $qpr_sortir }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font size="2"><b>NG Sort :</b></font> {{ $qpr_ng_sortir }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font size="2"><b>Mhr Sort :</b></font> {{ $qpr_mh_sortir }}</td>
		</tr>
		<tr>
			<td colspan="4"><font color="#FF0000"> &nbsp;&nbsp;<b><i><u>Attention :</u></i></b> <br>&nbsp;&nbsp;&nbsp;&nbsp;NG Parts Should be taken from AAI Warehouse within 7 days <br>&nbsp;&nbsp;&nbsp; after the problem founded,if not Parts will be Scrap.</font></td>
			<td rowspan="2" valign="top" align="center">Disposition : <br><br><br><b><font size="4">{{ $data->qprl_disposition }}</font></b></td>
			<td colspan="3"><font size="2"><b><i> AAIJ Quality Authorized Signature :</i></b></font></td>
		</tr>
		<tr>
			<td colspan="4"><font color="#0000FF">&nbsp;&nbsp;Please re-send your countermeasures within 7 days after received <br>&nbsp;&nbsp;this form and must present 5 why analysis problem via email to : <br>&nbsp;&nbsp;sunu.t@akebono-astra.co.id; aris.w@akebono-astra.co.id; <br>&nbsp;&nbsp;agus.r@akebono-astra.co.id and sucipto@akebono-astra.co.id </b></font></td>

            <td valign='top' align='center' nowrap ><b>Approved by</b><br>{{ $data->qprl_approved }}<br/>on {{ $data->creadate }}</td>

            <td valign='top' align='center' nowrap ><b>Checked by</b><br><br>{{ $data->qprl_checked }}<br/>on {{ $data->creadate }}</td>

			<td valign='top' align='center' nowrap ><b>In Charge by</b><br>{{ $data->qprl_charge }}<br/>on {{ $data->creadate }}</td>
		</tr>
		<tr>
			<td colspan="8" valign="top">&nbsp;
				<table	border="0" cellpadding="0" cellspacing="0" width="100%" height="40%" >
					<tr>
						<td align="center" colspan="2"><u><b>SUPPLIER ACTION<b></u><br><br></td>
					</tr>
					<tr>
						<td align="left">&nbsp;&nbsp;&nbsp; <u><b>Five Principle for Problem Solving</b></u> <br><br> &nbsp;&nbsp;&nbsp;&nbsp;Problem Analysis Manual</td>
						<td>
							<table border="1" cellpadding="0" cellspacing="0" width="95%" height="40%">
								<tr>
									<td style='padding-left: 5px;'>Date : {{ $data->qprl_rp_date }}</td>
									<td align="center">Approved</td>
									<td align="center">Prepared</td>
								</tr>
								<tr>
									<td height="50px" style='padding-left: 5px;'>No. : {{ $data->qprl_no }}<br> Quality Assurance Departement</td>
									<td height="50px" style='padding-left: 5px;'>by {{ $data->qprl_rp_approved }} <br> on {{ $data->qprl_rp_date }}</td>
                                    <td height="50px" style='padding-left: 5px;'>by {{ $data->qprl_rp_prepared }} <br> on {{ $data->qprl_rp_date }}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<br>
							<table	border="0" cellpadding="0" cellspacing="0" width="100%" height="40%" >
							<tr>
								<td><font size="2">&nbsp;&nbsp;&nbsp;<b>DESCRIPTION OF PROBLEM </b></font><br><font size="1">&nbsp;&nbsp;&nbsp;&nbsp;(Phenomenon/ Description/ Occurrence Quality/ Measures)</font></td>
								<td colspan="2"><font size="2">&nbsp;&nbsp;&nbsp;<b>RECOGNATION OF PROBLEM </b></font><br><font size="1">&nbsp;&nbsp;&nbsp;&nbsp;(Check Result of The part/ Cause Analysis/ Quality Status of Products)</font></td>
							</tr>
							<tr>
								<td valign="top">
									<table	border="1" cellpadding="0" cellspacing="0" width="97%" height="80%" >
										<tr>
											<td height="170px" style='padding-left: 5px;'>{{ $data->qprl_rp_desc_problem }}</td>
										</tr>
									</table>
								</td>
								<td colspan="2" valign="top">
									<table	border="1" cellpadding="0" cellspacing="0" width="98%" height="80%"  >
										<tr>
											<td height="170px"  colspan="2" style='padding-left: 5px;'>{{ $data->qprl_rp_rec_problem }}</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td nowrap><font size="2">&nbsp;&nbsp;&nbsp;<b>ANALYSIS OF CAUSE</b></font><br><font size="1">&nbsp;&nbsp;&nbsp;(Mechanism of Occurrence/ Re-producting of Nonconformance/Why &amp; Why Analysis)</font></td>
								<td nowrap><font size="2">&nbsp;&nbsp;&nbsp;<b>APPRORIATE COUNTERMEASURE</b></font><br><font size="1">&nbsp;&nbsp;&nbsp;&nbsp;(Description/Estimated/PPA)</font></td>
								<td><font size="2">&nbsp;&nbsp;&nbsp;<b>CONFIRMATION OF EFFECT</b></font><br><font size="1">&nbsp;&nbsp;&nbsp;&nbsp;(Actual Result of Effect)</font></td>

							</tr>
							<tr>
								<td valign="top">
									<table	border="1" cellpadding="0" cellspacing="0" width="" height="80%" >
										<tr>
											<td height="160px" style='padding-left: 5px;'>{{ $data->qprl_rp_analysis }}</td>
										</tr>
									</table>
								</td>
								<td valign="top" width='600px'>
									<table border='1' cellpadding="0" cellspacing="0" width="400px">
										<tr>
											<td>Description</td>
											<td>PIC</td>
											<td>Due Date</td>
										</tr>
										<tr>
											<td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate }}</td>
											<td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate_pic }}</td>
											<td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate_date }}</td>
										</tr>
										<tr>
                                            <td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate1 }}</td>
											<td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate_pic1 }}</td>
											<td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate_date1 }}</td>
										</tr>
										<tr>
                                            <td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate2 }}</td>
											<td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate_pic2 }}</td>
											<td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate_date2 }}</td>
										</tr>
										<tr>
											<td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate3 }}</td>
											<td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate_pic3 }}</td>
											<td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate_date3 }}</td>
										</tr>
										<tr>
                                            <td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate4 }}</td>
											<td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate_pic4 }}</td>
											<td height="" style='padding-left: 5px;'>{{ $data->qprl_rp_approriate_date4 }}</td>
										</tr>
									</table>
								</td>
								<td valign="top">
									<table	border="1" cellpadding="0" cellspacing="0" width="300px" height="80%" >
										<tr>
											<td height="90px" valign="" style='padding-left: 5px;'>{{ $data->qprl_rp_confirm }}</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2" valign="bottom"><font size="2">&nbsp;&nbsp;&nbsp;Why &amp; Why Analysis </font></td>
								<td nowrap valign="top"><font size="2"><b>FEED BACK TO THE ORIGINATING OPERATION</b></font><br><font size="1">(Reflection to the System &amp; Organization)</font></td>
							</tr>
							<tr>
								<td colspan="2" valign="top">
									<table	border="1" cellpadding="0" cellspacing="0" width="97%" height="80%" >
									<tr>
										<td align="center" width="30px">Step</td>
										<td align="center" width="30px">1</td>
										<td align="center" width="30px">2</td>
										<td align="center" width="30px">3</td>
										<td align="center" width="30px">4</td>
										<td align="center" width="30px">5</td>
									</tr>
									<tr>
										<td height="100px">&nbsp;</td>
										<td height="100px" style='padding-left: 5px;'>{{ $data->qprl_rp_why1 }}</td>
										<td height="100px" style='padding-left: 5px;'>{{ $data->qprl_rp_why2 }}</td>
										<td height="100px" style='padding-left: 5px;'>{{ $data->qprl_rp_why3 }}</td>
										<td height="100px" style='padding-left: 5px;'>{{ $data->qprl_rp_why4 }}</td>
										<td height="100px" style='padding-left: 5px;'>{{ $data->qprl_rp_why5 }}</td>
									</tr>
									</table>
								</td>
								<td valign="top">
									<table	border="1" cellpadding="0" cellspacing="0" width="97%" height="80%" >
										<tr>
											<td height="90px" style='padding-left: 5px;'>{{ $data->qprl_rp_feed }}</td>
										</tr>
									</table>
								</td>
							</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan='2'>Attachment : <a href='Attachment_qpr/{{ $data->qprl_rp_attachment }}'>{{ $data->qprl_rp_attachment }}</a></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
<script>
window.print();
</script>
