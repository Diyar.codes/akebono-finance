@extends('templates.main')

@section('title', 'Detail Packing Slip')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
                <div class="card-header">
                    <h3 class="main-content-label mb-3">@yield('title')</h3>
                </div>
            <div class="card-body">

                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center w-100" id="main-table">
                        <thead>
                            <tr>
                                <th scope="col">Line</th>
                                <th scope="col">Item Number</th>
                                <th scope="col">Deskripsi</th>
                                <th scope="col">QTY NG</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody class="tbodymain">
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
