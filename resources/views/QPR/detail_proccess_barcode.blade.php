@extends('templates.main')

@section('title', 'Detail Packing Slip')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-header">
                <h3 class="main-content-label mb-3">@yield('title')</h3>
            </div>
            <div class="card-body">
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center w-100" id="tabledetailBc">
                        <thead>
                            <tr style="background-color: #0066CC;" class="backgroudrowblue">
                                <th scope="col" class="text-white">Line</th>
                                <th scope="col" class="text-white">Item Number</th>
                                <th scope="col" class="text-white">Deskripsi</th>
                                <th scope="col" class="text-white">QTY NG</th>
                                <th scope="col" class="text-white">Status</th>
                            </tr>
                        </thead>
                        <tbody class="tbodydetail">

                            @foreach($result as $key => $val)
                            <tr>
                                <td> {{ $val['line'] }} </td>
                                <td> {{ $val['item_number'] }} </td>
                                <td> {{ $val['deskripsi'] }} </td>
                                <td> {{ $val['qty_ng'] }} </td>
                                @if($val['status'] == 'NG')
                                <td><button class="btn ripple btn-danger btn-sm btn-rounded">{{ $val['status'] }}</button></td>
                                @elseif($val['status'] == '')
                                <td> - </td>
                                @else
                                <td><button class="btn ripple btn-success btn-sm btn-rounded">
                                    {{ $val['status'] }}
                                </button></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-xl-2 col-lg-3 col-sm-4 col-5 my-2 mt-md-0">
                        <a href="{{ route('qc-process-bc') }}" class="btn btn-sm btn-danger btn-block"><i class="fas fa-undo"></i> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">

    $(document).ready(function() {
        $('#tabledetailBc').DataTable({
            ordering:false,
            responsive:true
        });
    });

</script>
@endsection
