@extends('templates.main')

@section('title', 'View Sortir')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-header">
                <h3 class="main-content-label mb-3">@yield('title')</h3>
            </div>
            <div class="card-body">
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center w-100" id="main-table">
                        <thead>
                            <tr>
                                <th scope="col">Tgl</th>
                                <th scope="col">Supplier</th>
                                <th scope="col">Problem</th>
                                <th scope="col">Item Number</th>
                                <th scope="col">OK</th>
                                <th scope="col">NG</th>
                                <th scope="col">Total Sortir</th>
                                <th scope="col">Jam Start</th>
                                <th scope="col">Jam End</th>
                                <th scope="col">Pic Sortir</th>
                                <th scope="col">Pic QC</th>
                            </tr>
                        </thead>
                        <tbody class="tbodymain">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="modaldemo8part2">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="get-supplier">
                        <thead class="thead">
                            <tr style="background-color: #0066CC;">
                                <th scope="col">Supplier Id</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->

@endsection
