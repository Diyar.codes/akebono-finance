@extends('templates.main')
@php
    use Illuminate\Support\Facades\DB;
@endphp
@section('title', 'P4 Small Part')
@section('stylest')
    {{-- <style>
        .tableFixHead tbody {
            height: 100px;       /* Just for the demo          */
            overflow-y: auto;    /* Trigger vertical scroll    */
            overflow-x: hidden;  /* Hide the horizontal scroll */
        }
        .tableFixHead {
            overflow: auto;
            height: 200px;
            font-size: 12px;
        }

        .tableFixHead thead th {
            position: sticky;
            top: 0;
            z-index: 1;
        }

        /* Just common table stuff. Really. */
        table {
            border-collapse: collapse;
            width: 100% !important;
        }

        th,
        td {
            padding: 8px 16px;
        }

        th {
            background: #0066CC;
        }

    </style> --}}
    {{-- <style>
        table.tableFixHead {
    width: 100%;
    display:block;
}
.tableFixHead thead {
    display: inline-block;
    width: 100%;
    height: 20px;
}
.tableFixHead tbody {
    height: 200px;
    display: inline-block;
    width: 100%;
    overflow: auto;
}
    </style> --}}
@endsection
@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body mt-3">
                <div>
                    <h6 class="main-content-label mb-3">Data Rak</h6>
                </div>
                <hr class="mg-b-40">
                {{-- <div class="row mb-1">
                    <div class="col-lg-4 col-md-6 box">
                        <div class="input-group">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">DN Number</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" id="filter_dn_number" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row mb-1 mt-2">
                    <div class="col-lg-3 box">
                        <div class="input-group">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Supplier</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" placeholder="" id="code" disabled>
                        </div>
                    </div>
                    <div class="col-lg-3 box">
                        <div class="input-group">
                            <span class="input-group-text spanbox exinput-custom d-sm-none" id="basic-addon1"></span>
                            <input type="text" style="color:black" class="form-control form-control-sm" placeholder="" id="name" disabled>
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-1 box ml-2">
                        <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modaldemo8part2"><i class="fas fa-search"></i></button>
                    </div>
                </div>

                <div class="row mt-2 mb-2">
                    <div class="col-lg-4 box">
                        <div class="input-group mt-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Arrival Date</span>
                            <select class="form-control form-control-sm" id="filter_moon" style="color:black">
                                <option value="" selected>Choose Mounth</option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                            <select class="form-control form-control-sm ml-2" id="filter_year" style="color:black">
                                <option value="" selected>Choose Year</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-8 box"></div>
                    <div class="btn btn-list">
                        <button type="button" class="btn btn-info btn-md ml-2 mt-2" id="filter"><i class="fas fa-search"></i> Search</button>
                        <button type="button" class="btn btn-danger btn-md ml-2 mt-2" id="reset"><i class="fas fa-undo"></i> Reset</button>
                    </div>
                    <div class="col-lg-2 box mt-2">
                        <div class="input-group mb-1">
                        </div>
                    </div>

                </div> --}}
                <div class="table-responsive mb-2 mt-2" style="height:500px;overflow:auto;">
                    <form action="{{route('polibox.post')}}" method="post">
                    @csrf
                    <table class="table table-striped table-bordered text-center w-100 mt-2" width="100%" id="main-table">
                        <thead>
                            <tr>
                                <th style="position: sticky;top: 0;z-index: 1;" scope="col">No</th>
                                <th style="position: sticky;top: 0;z-index: 1;" scope="col">Rak Number</th>
                                <th style="position: sticky;top: 0;z-index: 1;" scope="col">Pin</th>
                                <th style="position: sticky;top: 0;z-index: 1;" scope="col">Raspi</th>
                                <th style="position: sticky;top: 0;z-index: 1;" scope="col">Part Number</th>
                                <th style="position: sticky;top: 0;z-index: 1;" scope="col">Part Desc</th>
                                <th style="position: sticky;top: 0;z-index: 1;" scope="col">Loc To</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $key=>$dt)
                            <tr>
                                <td align="center">{{$key+1}}</td>
                                <td>{{$dt->rak_num}}</td>
                                <td>{{$dt->pin}}</td>
                                <td>{{$dt->raspi}}</td>
                                <input type='hidden' name='id[{{$key+1}}]' value='{{$dt->id}}'>
                                <td>
                                    <input type="text" name="part_num[{{$key+1}}]" id="part_num{{$key+1}}" value="{{$dt->part_num}}">
                                    <button type="button" name="edit" data-id="part_num{{$key+1}}" data-desc="desc{{$key+1}}" data-locto="locto{{$key+1}}"  class="edit btn btn-info btn-sm"><i class="fas fa-search"></i></button></td>
                                <td>
                                @php
                                    $desc = DB::table('polibox_mstr')->where('polibox_item',$dt->part_num)->value('polibox_deskripsi');
                                @endphp
                                <input type="text" name="desc[{{$key+1}}]"  id="desc{{$key+1}}" readonly size="25"  value="{{$desc}}">
                                </td>
                                <td>
                                    <input type="text" name="locto[{{$key+1}}]"  id="locto{{$key+1}}" value="{{$dt->locto}}">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <button type="submit" class="update btn btn-info">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="modaledit">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="main-table2">
                        <thead class="thead">
                            <tr>
                                <input type="text" style="display:none" id="getid">
                                <input type="text" style="display:none" id="getdesc">
                                <input type="text" style="display:none" id="getlocto">
                                <th scope="col">No</th>
                                <th scope="col">Item</th>
                                <th scope="col">Deskripsi</th>
                                <th scope="col">Loc To</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->


@endsection

@section('scripts')
{{-- <script>
    $(document).ready(function() {
        var table = $('#main-table').DataTable({
            // "autoWidth": true,
            "scrollY": "500px",
            "scrollCollapse" : true,
            // "responsive": true,
            "paginate":false,
            "ordering":false,
        });
    });
</script> --}}
<script type="text/javascript">
    $(()=> {
     // gate date
    // fill_datatable()
    fill_datatable2()

    // $(document).on('click', '#seacrhSOAPBusinesRelation', function(){
    //     $('#main-table').DataTable().clear().destroy()
    //     getSupplier();
    // })
    function update(item) {
        $('#modaledit').modal('hide');
    }
    function fill_datatable2() {
        $('#main-table2').DataTable({
            processing: true,
            serverSide: true,
            // paginate: false,
            // searching: false,
            ajax: {
                url: `${BASEURL}p4-master/get`,
            },
            columns:[
                {
                    data: "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'polibox_item',
                    name: 'polibox_item',
                },
                {
                    data: 'polibox_deskripsi',
                    name: 'polibox_deskripsi',
                },
                {
                    data: 'polibox_loc_to',
                    name: 'polibox_loc_to',
                },
                {
                    data: 'actione',
                    name: 'actione',
                },
                // {
                //     data: 'action',
                //     name: 'action',
                // },
                // {
                //     data: 'locto',
                //     name: 'locto',
                // },
            ],
        })
    }
    // view date
    function fill_datatable() {
        $('#main-table').DataTable({
            processing: true,
            serverSide: true,
            "scrollY": "200px",
            scrollCollapse : true,
            // paginate: true,
            "searching": false,
            ajax: {
                url: `${BASEURL}p4-master`,
            },
            columns:[
                {
                    data: "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'rak_num',
                    name: 'rak_num',
                },
                {
                    data: 'pin',
                    name: 'pin',
                },
                {
                    data: 'raspi',
                    name: 'raspi',
                },
                {
                    data: 'part_nume',
                    name: 'part_nume',
                },
                {
                    data: 'action',
                    name: 'action',
                },
                {
                    data: 'loctoe',
                    name: 'loctoe',
                },
            ],
        })
    }

    // change thead th datatable color
    $(".table-bordered thead th").css("background-color", "#0066CC");

    // change thead th datatable font color
    $("table thead th").css("color", "#fff");
})
$(document).on('click', '.update', function () {
    $(this).html("Loading....");
        });
$(document).on('click', '.edit', function () {
            dataId = $(this).data('id');
            dataDesc = $(this).data('desc');
            dataLocto = $(this).data('locto');
            $('#getid').val(dataId);
            $('#getdesc').val(dataDesc);
            $('#getlocto').val(dataLocto);
            $('#modaledit').modal('show');
        });
$(document).on('click', '#added', function () {
    idka = $('#getid').val();
    data = $(this).data('itema');
    console.log(data);
    $(`#${idka}`).val(data);
    idkb = $('#getdesc').val();
    datb = $(this).data('itemb');
    $(`#${idkb}`).val(datb);
    idkc = $('#getlocto').val();
    datc = $(this).data('itemc');
    $(`#${idkc}`).val(datc);
    $('#modaledit').modal('hide');
});
</script>
@endsection
