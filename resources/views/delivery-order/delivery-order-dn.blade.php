@extends('templates.main')

@section('title', 'Detail Transaksi')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="row">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-lg-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">Transaction ID</span>
                            <p class="mt-3">{{ $p_baru }}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-lg-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">Delivery Date</span>
                            <input type="date" name="tgl" id="tgl" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row mt-lg-n4 mb-2">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">DN Number</span>
                            <p class="mt-3">{{ $atas->dn_tr_id }}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3 mt-lg-2">
                            <span class="input-group-text spanbox exinput-custom mr-3">Packing Slip</span>
                            <input type="text" name="ps" id="ps" style="color:black" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row mt-lg-n4">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">Purchase Order</span>
                            <p class="mt-3">{{ $atas->dn_po_nbr }}</p>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-3 col-12">
                        <p><font color="red"><b>Purchase Order List</b></font></p>
                    </div>
                </div>
                <div class="table-responsive mb-2">
                    <table class="table table-bordered text-center text-nowrap w-100" id="tabelnya" width="100%">
                        <thead>
                            <tr>
                                <th scope="col">Line</th>
                                <th scope="col">Item Number</th>
                                <th scope="col">UM</th>
                                <th scope="col">Deskripsi</th>
                                <th scope="col">Type</th>
                                <th scope="col">Qty PO</th>
                                <th scope="col">Qty<br>Received</th>
                                <th scope="col">Qty<br>Outstanding<br>SJ</th>
                                <th scope="col">Qty DN</th>
                                <th scope="col">Sisa DN</th>
                                <th scope="col" width="7%">QTY to Ship</th>
                                <th scope="col">UM</th>
                                <th scope="col">Lot No</th>
                                <th scope="col">Production Date</th>
                                <th scope="col">PO<br>Kurang</th>
                            </tr>
                        </thead>
                        <tbody style="font-family: 10px">
                         {!! $tablenya !!}
                        </tbody>
                    </table>
                </div>
                <div class="row my-4">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-success btn-block mb-2" onclick="save()"><i class="fas fa-save"></i> Save</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <a href="/create-delivery-order-dn" class="btn btn-danger btn-block mb-2"><i class="fas fa-undo"></i> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
