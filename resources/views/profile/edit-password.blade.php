@extends('templates.main')

@section('title', 'Update Password')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="jumbotron" style="background-color: yellow">
                    <h1 class="mt-n5">INFORMATION!</h1>
                    <div style="border: 0.5px solid black"></div>
                    <h3 class="mt-1 font-weight-normal">Please Change your Password.</h3>
                    <div class="row ml-1 mb-1">
                        <div class="pass">
                            <p class="border-bottom border-dark font-weight-bold mb-n1 mt-3">Password must have criteria as below.</p>
                        </div>
                    </div>
                    <p class="font-weight-bold mb-n1">1. Must e a minimum of 8 characters</p>
                    <p class="font-weight-bold mb-n1">2. Must contain at least 1 number</p>
                    <p class="font-weight-bold mb-n1">3. Must contain at least one uppercase character</p>
                    <p class="font-weight-bold mb-n1">4. Must contain at least one lowercase characters</p>
                  </div>
                <form action="{{ route('update-password') }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row mb-2 ml-2">
                        <div class="col-xl-1 col-lg-2 col-3 p-0 mb-xl-0 mb-2 mb-2-responsive">
                            <div class="input-group">
                                <span class="exinput-custom-responsive">Username</span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-8 p-0 mb-xl-0 mb-2 mb-2-responsive">
                            <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark border-0 bg-transparent" readonly value="{{ Auth::guard('pub_login')->user()->username }}">
                        </div>
                    </div>
                    <div class="row mb-2 ml-2">
                        <div class="col-xl-1 col-lg-2 col-3 p-0 mb-xl-0 mb-2 mb-2-responsive">
                            <div class="input-group">
                                <span class="exinput-custom-responsive">Old Password</span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-8 p-0 mb-xl-0 mb-2 mb-2-responsive">
                            <input type="password" class="form-control form-control-sm form-control-sm-responsive text-dark @error('old_password') is-invalid @enderror" name="old_password">
                            @error('old_password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-2 ml-2">
                        <div class="col-xl-1 col-lg-2 col-3 p-0 mb-xl-0 mb-2 mb-2-responsive">
                            <div class="input-group">
                                <span class="exinput-custom-responsive">New Password</span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-8 p-0 mb-xl-0 mb-2 mb-2-responsive">
                            <input type="password" class="form-control form-control-sm form-control-sm-responsive text-dark @error('new_password') is-invalid @enderror" name="new_password">
                            @error('new_password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-2 ml-2">
                        <div class="col-xl-1 col-lg-2 col-3 p-0 mb-xl-0 mb-2 mb-2-responsive">
                            <div class="input-group">
                                <span class="exinput-custom-responsive">Confirm Password</span>
                            </div>
                        </div>
                        <div class="col-xl-2 col-8 p-0 mb-xl-0 mb-2 mb-2-responsive">
                            <input type="password" class="form-control form-control-sm form-control-sm-responsive text-dark @error('confirm_password') is-invalid @enderror" name="confirm_password">
                            @error('confirm_password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-2">
                            <button type="submit" class="btn btn-success btm-sm btn-block mb-2"><i class="fas fa-save"></i> Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
