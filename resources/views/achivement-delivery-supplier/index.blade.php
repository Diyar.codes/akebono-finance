@extends('templates.main')

@section('title', 'Achivement Delivery Supplier')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <h6 class="main-content-label mb-3">Summary Report @yield('title')</h6>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-2"><b>Part Number</b></div>
                        <div class="col-md-3">
                            <input type="text" name="text" id="item" class="form-control" style="color:black">
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-2"><b>Purchase Order</b></div>
                        <div class="col-md-3">
                            <input type="text" name="text" id="po" class="form-control" style="color:black">
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-2"><b>Periode</b></div>
                        <div class="col-md-1">
                            <select class="form-control select" id="bulan" style="color:black">
                                <option value="" selected></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control select" id="tahun" style="color:black">
                                <option value="" selected></option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-1">
                            <button class="btn btn-info" onclick="search()"><b>Search</b></button>
                        </div>
                    </div>
                    <br>
                    <div class="container secondtable mt-3">
                        <button type="button" onclick="export_excel()" id="export_data" class="btn ripple btn-outline-success btn-sm btn-icon mb-2 btnlist" data-toggle="tooltip" data-original-title="Download Excel"><i class="si si-cloud-download"></i></button>
                        <br>
                        <div class="table-responsive mb-4 mt-3 secondtable">
                            <table border="1" cellpadding="0" cellspacing="0" width="100%" align="center" id="how">                        
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

            
            $(document).ready(function() {
                $("#example thead th").css("background-color", "#0066CC");
                $('#export_data').hide();
                // change thead th datatable font color
                $("#example thead th").css("color", "#fff");
               
            });

            function export_excel(){
                var item  = $("#item").val();
                var po    = $("#po").val();
                var bulan = $("#bulan").val();
                var tahun = $("#tahun").val();
                getLoader();
                $.ajax({
                    type: "GET",
                    url: "/export-achievement",
                    data: {
                        item: item,
                        po: po,
                        bulan: bulan,
                        tahun: tahun,
                    },
                    success: function(data) {
                        $('.secondtable').find('#how').html(data);
                        $('#export_data').show();
                    },
                    error: function(error) {
                        alert('Data is not Defined!!!');
                    }
                });
            }
            function search(){
                
                var item  = $("#item").val();
                var po    = $("#po").val();
                var bulan = $("#bulan").val();
                var tahun = $("#tahun").val();
                if(po == ""){
                    SW.warning({
                        message: "PO Number Can't Empty",
                        
                    });
                }else if(bulan == ""){
                    SW.warning({
                        message: "Month Can't Empty",
                        
                    });
                }else if(tahun == ""){
                    SW.warning({
                        message: "Year Can't Empty",
                        
                    });
                }else{
                    getLoader();
                    $.ajax({
                        type: "GET",
                        url: "/generate-achievement",
                        data: {
                            item: item,
                            po: po,
                            bulan: bulan,
                            tahun: tahun,
                        },
                        success: function(data) {
                            // $('#how').DataTable().destroy();
                            $('.secondtable').find('#how').html(data);
                            $('#export_data').show();
                            // $('#how').DataTable({
                            //     responsive: false,
                            //     language: {
                            //         searchPlaceholder: 'Search...',
                            //         sSearch: '',
                            //         lengthMenu: '_MENU_ items/page',
                            //     }
                            // });
                        },
                        error: function(error) {
                            alert('Data is not Defined!!!');
                        }
                    });
                }
                   
            }
    </script>
@endsection