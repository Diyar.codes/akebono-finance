@extends('templates.main')

@section('title', 'Delivery - Pending Transfer')

@section('body')
<style>
    .tableFixHead {
        overflow: auto;
        height: 400px;
        font-size: 12px;
    }

    .tableFixHead thead th {
        position: sticky;
        top: 0;
        z-index: 1;
    }

    /* Just common table stuff. Really. */
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th,
    td {
        padding: 8px 16px;
    }

    th {
        background: #0066CC;
    }

</style>
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card">
                <div class="card-body minstylebody">
                    <div>
                        <h6 class="main-content-label mb-3">@yield('title')</h6>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label text-bold">Supplier ID</label>
                                <div class="col-sm-10">
                                    <div class="row">
                                        <div class="col-5">
                                            <input type="text" class="form-control form-control-sm" autocomplete="off"
                                                id="code" disabled>
                                        </div>
                                        <div class="col-1 p-0 m-0">
                                            <button type="button" class="btn btn-sm btn-info" data-toggle="modal"
                                                data-target="#modaldemo8part2"><i class="fas fa-search"></i></button>
                                        </div>
                                        <div class="col-5">
                                            <input type="text" class="form-control form-control-sm" autocomplete="off"
                                                id="name" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="text-bold">Or</div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group row">
                                <label for="staticEmail" class="col-3 col-form-label">No surat</label>
                                <div class="col-9">
                                    <input type="text" class="form-control form-control-sm" autocomplete="off"
                                        id="no_surat">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-sm btn-info mb-2" id="searchUp"
                                onclick="searchSupp()"><i class="fas fa-search"></i> Search</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-3 col-sm-6 box">
                            <div class="input-group mb-2">
                                <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Periode
                                    Transaction</span>
                                <select class="form-control form-control-sm mr-1" id="month">
                                    <option value="" selected>Month</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                                <select class="form-control form-control-sm" id="year">
                                    <option value="">Year</option>
                                    @for ($t = 2014; $t <= date('Y') + 2; $t++)
                                        <option value="{{ $t }}">{{ $t }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-sm-4">
                            <button type="submit" onclick="searchSupp()" class="btn btn-sm btn-info mb-2"
                                id="searchDown"><i class="fas fa-search"></i> Search</button>
                                <button type="button" class="btn btn-sm btn-danger mb-2" width="30px" onclick="window.location.reload();"><i class="fas fa-undo"></i> Reset</button>
                        </div>
                    </div>
                    <form action="{{ route('delivery-item-pending-post') }}" method="post">
                        {{ csrf_field() }}
                        <div class="tableFixHead mb-2">
                            <table class="table table-striped table-bordered text-center w-100" id="main-table">
                                <thead>
                                    <tr>
                                        <th style="font-size: 11px">No</th>
                                        <th style="font-size: 11px">No Surat</th>
                                        <th style="font-size: 11px">Item</th>
                                        <th style="font-size: 11px">Qty</th>
                                        <th style="font-size: 11px">Supplier</th>
                                        <th style="font-size: 11px">Date</th>
                                        <th style="font-size: 11px">Keterangan</th>
                                        <th style="font-size: 11px">Remark</th>
                                        <th style="font-size: 11px">Check</th>
                                    </tr>
                                </thead>
                                <tbody id="result">
                                    <?php $no = 0; ?>
                                    @foreach ($data as $row)
                                        <tr>
                                            <td>{{ $no + 1 }}</td>
                                            <td>{{ $row->no }} <input type="hidden" name="no{{ $no + 1 }}"
                                                    value="{{ $row->no }}"></td>
                                            <td>{{ $row->kode }} <input type="hidden" name="kode{{ $no + 1 }}"
                                                    value="{{ $row->kode }}"></td>
                                            <td>{{ $row->act_qty }} <input type="hidden" name="qty{{ $no + 1 }}"
                                                    value="{{ $row->act_qty }}"></td>
                                            <td>{{ $row->ct_ad_name }}</td>
                                            <td>{{ $row->tanggal }} <input type="hidden" name="tgl{{ $no + 1 }}"
                                                    value="{{ $row->tanggal }}"></td>
                                            <td>{{ $row->keterangan }} <input type="hidden"
                                                    name="tgl{{ $no + 1 }}" value="{{ $row->keterangan }}">
                                            </td>
                                            <td>{{ $row->remarks == null ? '-' : $row->remarks }}</td>
                                            <td><input type="checkbox" name="check{{ $no + 1 }}"
                                                    id="check{{ $no + 1 }}" value="true"></td>
                                        </tr>
                                        <?php $no++; ?>
                                    @endforeach
                                </tbody>
                            </table>
                            <input type="hidden" id="jumlah" name="jumlah" value="{{ $no }}">
                        </div>
                        <button type="submit" class="btn btn-sm btn-success"><i class="fas fa-save"></i> Process</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal effects -->
    <div class="modal" id="modaldemo8part2">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header text-center">
                    <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal"
                        type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered text-center text-nowrap w-100" id="main-tablee">
                            <thead class="thead">
                                <tr>
                                    <th scope="col">Supplier Id</th>
                                    <th scope="col">Supplier Name</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody class="tbodyselect">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Eend Modal effects-->
@endsection
