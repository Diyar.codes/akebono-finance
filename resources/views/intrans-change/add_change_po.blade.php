@extends('templates.main')

@section('title', 'Change Type')

@section('body')

    {{-- change type --}}
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 box">
                            <div class="input-group mb-3">
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                        </div>
                    </div>
                    {{-- batas --}}
                    <div class="row">
                        <div class="col-lg-4 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text exinput-custom spanbox" id="basic-addon1">Effective
                                    Date</span>
                                <input type="date" name="effdate" id="effdate" class="form-control-sm" style="border: 1px solid black">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text exinput-custom spanbox" id="basic-addon1">Approval PO</span>
                                <input type="text" name="name" id="name" placeholder="name" class="form-control-sm mr-1" style="border: 1px solid black" readonly>
                                <input type="email" name="email" id="email" placeholder="email"
                                    class="form-control-sm mr-1" style="border: 1px solid black" readonly>
                                <button class="btn btn-info btn-sm ripple" data-toggle="modal"
                                    data-target="#searchApproval"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text exinput-custom spanbox" id="basic-addon1">Purchase
                                    Order</span>
                                <input type="text" name="purchase_order" id="purchase_order" class="form-control-sm" style="border: 1px solid black">
                            </div>
                        </div>
                    </div>
                    {{-- batas --}}
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-info btn-sm" width="30px" onclick="SeacchData()"
                                id="updateItemKanban"><i class="fas fa-search"></i> Search</button>
                            <a href="/change-intrans" class="btn btn-danger btn-sm" width="30px"><i class="fas fa-undo"></i> Back</a>
                            <button type="button" class="btn btn-sm btn-danger" width="30px" onclick="window.location.reload();"><i class="fas fa-undo"></i> Reset</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('save-change-po') }}" method="post" id="form_data">
                                <div class="table-responsive mb-2">
                                    @csrf
                                    <div id="result"></div>
                                    <div id="button"></div>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal effects -->
    <div class="modal" id="searchApproval">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header text-center">
                    <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal"
                        type="button"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered text-center text-nowrap w-100" id="main-table">
                            <thead class="thead">
                                <tr>
                                    <th scope="col">Approval Name</th>
                                    <th scope="col">Approval Email</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="bodyApproval">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal effects end-->

@endsection
