@extends('templates.main')

@section('title', 'List Perubahan Type PO')

@section('body')

    {{-- List Perubahan Type PO --}}
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div>
                        <h6 class="main-content-label mb-3">@yield('title')</h6>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text exinput-custom spanbox" id="basic-addon1">Date :</span>
                                <select id="bulan" class="form-control-sm mr-1">
                                    <option value="0">Month</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                                <select id="tahun" class="form-control-sm mr-1">
                                    <option value="0">Year</option>
                                    @for ($i = 2014; $i <= date('Y')+2; $i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                                <button type="button" id="search" onclick="searchChangeIntrans()" class="btn ripple btn-info btn-sm"><i class="fas fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-1">
                        <div class="col-12">
                            <a href="{{ route('add-change-po') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus"></i> Add Change PO
                            </a>
                            <button type="button" class="btn btn-sm btn-danger" width="30px" onclick="window.location.reload();"><i class="fas fa-undo"></i> Reset</button>
                        </div>
                    </div>
                    <div class="table-responsive mb-2">

                        <table class="table table-striped table-bordered text-center" id="main-table" width="100%">
                            <thead>
                                <tr>
                                    <th scope="col">Transaction ID</th>
                                    <th scope="col">Effdate</th>
                                    <th scope="col">Purchase Order</th>
                                    <th scope="col">Line</th>
                                    <th scope="col">Item Number</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">No Type</th>
                                    <th scope="col">PO Type Revision</th>
                                    <th scope="col">Approval</th>
                                </tr>
                            </thead>
                            <tbody id="result">
                                @foreach ($data as $row)
                                    <?php
                                        $approval_status = $row->approval_status;
                                        $status = '';
                                        if($approval_status == 'OK'){
                                            $status ='Approved';
                                        }else{
                                            if($approval_status == 'NOK'){
                                                $status ='Rejected';
                                            }else{
                                                $status ='Not yet Approved';
                                            }
                                        }
                                    ?>
                                    <tr>
                                        <td>{{$row->transaksi_id}}</td>
                                        <td>{{date_format(date_create($row->effdate), 'M d Y')}} <br> {{date_format(date_create($row->effdate), 'h:i:s A')}}</td>
                                        <td>{{$row->no_po}}</td>
                                        <td>{{$row->line_po}}</td>
                                        <td>{{$row->po_item}}</td>
                                        <td>{{$row->po_deskripsi}}</td>
                                        <td>{{$row->type_po}}</td>
                                        <td>{{$row->type_po_rev}}</td>
                                        <td>{{$row->approval_po}} <br> {{ date_format(date_create($row->approval_date), 'M d Y h:i:s A')}} <br> <strong>{{$status}}</strong>  </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Batas A. Change PO M or No M --}}

@endsection
