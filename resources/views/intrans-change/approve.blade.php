<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title> AAIJ Supplier - Portal </title>
    <style type="text/css">
        #bodyContainer {
            background-color: white;
            margin: 10px;
            position: relative;
            clear: both;
        }

        #resultTable {
            width: 100%;
        }

    </style>
    <script language="JavaScript">
        function copyValue() {
            if (document.form_reason.txtRemark.value == '') {
                alert('Please fill the reason before reject this requisition !');
                document.form_reason.txtRemark.focus();
                return false;
            } else {
                document.form_2.txtRemark2.value = document.form_reason.txtRemark.value;
                return true;
            }
        }

        function remarkApprove() {
            document.form_1.txtReason2.value = document.form_reason.txtReasonn.value;
        }

    </script>
</head>

<body>
    <div class="container mt-4 mb-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header bg-primary text-white">
                        Transaction Number: {{ $transaksi_id }}
                    </div>
                    <div class="card-body">
                        <form name='form_reason'>
                            <div class="form-group row">
                                <label for="txt_transaksi" class="col-sm-2 col-form-label">Transaction ID</label>
                                <div class="col-sm-10">
                                    <input type='text' class="form-control" name='txt_transaksi' id='txt_transaksi' size="35" readonly
                                    value="{{ $transaksi_id }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="txt_transaksi" class="col-sm-2 col-form-label">Reason if you want to <br> reject this transaction</label>
                                <div class="col-sm-10">
                                    <textarea rows="2" cols="80" class="form-control" name="txtRemark" id="txtRemark"
                                            class="inputField"></textarea>
                                </div>
                            </div>
                        </form>
                        <br>
                        <?php
                        echo '<div class="table-responsive">';
                        echo "<table id='resultTable' class='table table-bordered'>";
                        echo '<thead class="bg-primary text-white">';
                        echo "<tr> ";
                            echo "<th>No.</th>";
                            echo "<th>Purchase Order</th>";
                            echo "<th>Line</th>";
                            echo "<th>Item Number</th>";
                            echo "<th>Description</th>";
                            echo "<th>Type</th>";
                            echo "<th>Request by</th>";
                            echo "<th>Qty PO</th>";
                            echo "<th>Qty LPB</th>";
                            echo "<th>Type PO</th>";
                            echo "<th>Revision Type PO</th>";
                        echo "</tr>" ;
                        echo '</thead>';
                    
                    $tampil_data = DB::select("SELECT no_po,line_po,type_po,po_deskripsi,effdate,type_po_rev,po_item,no_pp,line_pp,type_pp,type_pp_revisi FROM po_Change_type WHERE transaksi_id = '$transaksi_id'");
                                            
                    $no = 1;
                    echo '<tbody>';
                    foreach ($tampil_data as $getdata) {
                        $po_number = $getdata->no_po;
                        $line = $getdata->line_po;
                        $type_po = $getdata->type_po;
                        $deskripsi = $getdata->po_deskripsi;
                        $effdate = $getdata->effdate;
                        $type_po_revisi = $getdata->type_po_rev;
                        $item_number = $getdata->po_item;
                        
                        $no_pp = $getdata->no_pp;
                        $line_pp = $getdata->line_pp;
                        $type_pp = $getdata->type_pp;
                        $type_pp_revisi = $getdata->type_pp_revisi;
        
                        // untuk menampilkan type PO
                        $tampilItemType = pt_mstr($item_number);
                        if(is_array($tampilItemType['desc_type'])){
                            $type_part = '-';
                        }else{
                            if($tampilItemType['desc_type'] == ''){
                                $type_part = '-';
                            }else{
                                $type_part = $tampilItemType['desc_type'];
                            }
                        }
                        $tampil_request_by = po_detail($po_number);
                        
                        $no = 1;
                        foreach ($tampil_request_by as $value) {
                            // dd($value['item_number'].' '.$item_number);
                            if($value['line'] == $line){
                                if($value['item_number'] == $item_number){
                                    $request_by = $value['podRequest'];
                                    $qty_po = $value['qty_po'];
                                    $qty_lpb = $value['qty_receive'];
                                }else{
                                    $request_by = '-';
                                    $qty_po = 0;
                                    $qty_lpb = 0;
                                }
                            }
                            $no++;
                        }
                        
                        echo "<tr>";
                        echo "<td align='center'>".$no."</td>";
                        echo "<td align='center'>".$po_number."</td>";
                        echo "<td style='text-align:center;'>".$line."</td>";	
                        echo "<td style='text-align:center;'>".$item_number."</td>";						
                        echo "<td style='text-align:center;'>".$deskripsi."</td>";						
                        echo "<td style='text-align:center;'>".$type_part."</td>";						
                        echo "<td style='text-align:center;'>".$request_by."</td>";						
                        echo "<td style='text-align:center;'>".number_format($qty_po,0)."</td>";						
                        echo "<td style='text-align:center;'>".number_format($qty_lpb,0)."</td>";						
                        echo "<td style='text-align:center;'>".$type_po."</td>";						
                        echo "<td style='text-align:center;'>".$type_po_revisi."</td>";	
                        echo "</tr>";
                        $no = $no+1;
                    }
                    echo '</tbody>';
                    echo "</table> </div> <br>";
                    echo "<table id='resultTable'>";
                    echo "<tr> ";
                        echo "<th class='text-center'>Approval</th>";
                    echo "</tr>" ;
                    echo "<tr>";
                    
                    $statusApprove = collect(DB::select("SELECT distinct approval_po,approval_status,approval_date FROM PO_change_type WHERE transaksi_id = '$transaksi_id'"))->first();
                    // dd($statusApprove);
                    if($statusApprove != null){
                        $nama_approval = $statusApprove->approval_po == null ? '-' : $statusApprove->approval_po;
                        $status_approval = $statusApprove->approval_status == null ? '' : $statusApprove->approval_status;
                        $date_approval = $statusApprove->approval_date == null ? '-' : $statusApprove->approval_date;
                    }else{
                        $nama_approval = '-';
                        $status_approval = '';
                        $date_approval = '-';
                    }
        
                    echo "<td width='25%' style='text-align:center;'>";
                    if ($nama_approval == '-') {
                        echo '-';
                    }else{
                        if($status_approval == ''){
                            echo '<div class="row mt-4">';
                                echo '<div class="col-6 text-right">';
                                    echo "<form name='form_1' method='POST' action='".route('intrans-approve-change')."' onsubmit='return remarkApprove();'>"; ?> @csrf <?php
                                    echo '<input type="hidden" name="transaksi_id" value="'.$transaksi_id.'">';
                                    echo "<input class='btn btn-sm btn-success' type='submit' name='save_data' value='Approve' style='width:140px' onclick=\"return confirm('Are you sure you want to Approve?')\">  <textarea rows='2' cols='103' name='txtReason2' id='txtReason2' class='inputField' hidden> </textarea></form>";
                                echo '</div>';
                                echo '<div class="col-6 text-left">';
                                    echo "<form name='form_2' method='POST' action='".route('intrans-reject-change')."' onsubmit='return copyValue();'>";
                                    ?> @csrf <?php
                                    echo '<input type="hidden" name="transaksi_id" value="'.$transaksi_id.'">';
                                    echo "<input class='btn btn-sm btn-danger' type='submit' name='reject_data' value='Reject' style='width:140px' onclick=\"return confirm('Are you sure you want to Reject?')\"> <textarea rows='2' cols='103' name='txtRemark2' id='txtRemark2' class='inputField' hidden> </textarea> </form>";
                                echo '</div>';
                            echo '</div>';
                        }
        
                        if($status_approval == 'OK'){
                            echo '<div class="text-center">';
                            echo "<font color='blue'> <b> <i> Digitally Signed </i> </b> </font> <br> <br> by ".ucfirst($nama_approval)." on ".$date_approval;
                            echo '</div>';
                        }
                    }
                    echo "</td>";
                        
                        echo "</tr>";
                        echo "</table>";
                    ?>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>
</body>

</html>
