@extends('templates.main')

@section('title', 'Rekap Barcode Detail')

@section('styles')
<style>
    #divSearch
    {
        border: thin gray solid;
        background-color:#0066CC;
        width:100%;

    }
    #divReport
    {
        width:100%;
        height:100%;
        position:fixed;
        z-index:999999;
    }
    .tblSearch {
        margin:0px 20px;
    }
    .tblGrafik td{
        border:none;
    }
    .tblSearch th{
        border:none;
        color:white;
        font-weight:bold;
        padding:5px;
    }
    .tblSearch td{

        border:none;


    }
    .TbllphHeader
    {
        margin:10px!important;
        border:thin black solid;
    }
    .TbllphHeader
    {
        text-align:center;
    }
    .lphHeader td{

        background-color:#CAE99D;
        font-weight:bold;
        text-align:center;


    }
     .tdHd
     {
        font-weight:bold;
        border:none;
        text-align:left!important;
     }
     #divScreenSaver
     {
        width:100%;
        height:100%;
        position:absolute;
        z-index:999999;
     }
     #fontTitle
     {
        font-size:22px;
        color:white;
        margin:20px;
        font-weight:bold;
     }
     #divScreenSaver td{
     font-size:14px;
     font-weight:bold;

     text-align:center;
     }
     #tbodyScreenSaver
     {
        background-color:white;
     }
     #tbodyScreenSaver td
     {
         color:black;

     }
     .TbllphHeader td
     {
        font-size:9px;
        text-shadow:none;
        font-family:arial;
     }
     .lphHeader td{
        font-size:11px;
        text-shadow:none;
        font-weight:999!important;
     }
</style>
<style type="text/css">
	#bodyContainer{
		background-color:white;
		margin:10px;
		position:relative;
		clear:both;
	}
	#resultTable{
		width:100%;
	}
	</style>
@endsection
@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h6 class="main-content-label mb-3">@yield('title')</h6>
                    </div>
                </div>
                <form action="" method="">
                <div class="row">
                    <div class="col-md-4">
                        <div class="input-group mb-2">
                            <span class="input-group-text exinput-custom" id="basic-addon1">Periode Transaction</span>
                            <select name="month" id="search-month" class="form-control inputbox_down" id="inputField">
								<option value="01">01</option>
								<option value="02">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="09">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
							</select>
                            <select name="year" id="search-year" class="form-control inputbox_down" id="inputField">
								@php
                                    $thn = date("Y");
                                    $year=date("Y");
                                    $awal=$year-2;
                                    for($awal=$year-2;$awal<=$year;$awal++)
                                    {
                                        echo '<option value='.$awal.' '.(($awal==$thn)?'selected="selected"':"").'>'.$awal.'</option> ';
                                    }
                                @endphp
							</select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Item Number</span>
                            <input type="text" id=txtdate2 name="itm_nbr" value="" class="form-control" placeholder="" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                    </div>

                    <div class="col-md-3 mb-2">
                        <button type="button" class="btn btn-info" id="btnShow">Search</button>
                    </div>
                </div>
            </form>
                @php
				$bln = date("m");
				echo "<table id='resultTable' class='table table-striped table-bordered text-center text-nowrap w-100 mt-2' width=>";
                echo "<thead>";
				echo "<tr> ";
				echo "<th id='row1' width='' colspan='44'>Periode ".$bln." - ".$thn."</th>";
				echo "</tr> ";
				echo "<tr> ";
				echo "<th id='row1' width='' rowspan='2'>No</th>";
				echo "<th id='row1' width='' rowspan='2'>Item Number</th>";
				echo "<th id='row1' width='' rowspan='2'>Deskripsi</th>";
				echo "<th id='row1' width='' rowspan='2'>Tipe</th>";
				echo "<th id='row2' width ='' rowspan='2'>Loc From</th>";
				echo "<th id='row2' width ='' rowspan='2'>Loc To</th>";
				echo "<th id='row1' width='' rowspan='2'>Barcode OK</th>";
				echo "<th id='row2' width ='' rowspan='2'>Barcode Pending</th>";
				echo "<th id='row2' width ='' rowspan='2'>Manual</th>";
				echo "<th id='row2' width ='' rowspan='2'>Total</th>";
				echo "<th id='row2' width ='' colspan='3'>Need</th>";
				echo "<th id='row2' width ='' colspan='31'>Date</th>";
				echo "</tr>" ;
				echo "<tr>" ;
				echo "<th id='row2' width =''>Minus Bom</th>";
				echo "<th id='row2' width =''>After Backflush</th>";
				echo "<th id='row2' width =''>Total</th>";
				for($a=1;$a<=31;$a++){
					echo "<th id='row2' width =''>$a</th>";
				}
				echo "</tr>" ;
                echo "</thead>";
                echo "<tbody class='tbodymain'>";
                echo "</tbody>";
				echo "</table>";
			@endphp
                {{-- <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center w-100" id="main-table">
                        <thead>
                            <tr>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Category</th>
                                <th scope="col">27</th>
                                <th scope="col">28</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody class="tbodymain">
                            @foreach ($data as $item)
                            <tr>
                                <td>
                                {{$item->ct_vd_addr}}<br/>
                                {{$item->ct_ad_name}}
                                </td>
                                <td>
                                    kk
                                </td>
                                <td>
                                    Not Aproved
                                </td>
                                <td>
                                    Out Standing
                                </td>
                                <td>
                                    tot
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        var table = $('#resultTable').DataTable({
            // "autoWidth": false,
            "seaching":false,
            // "responsive": true,
            "ordering":false,
            "paginate":false,
            "scrollX": true,
            "scrollCollapse": false
        });
    });
</script>
<script>
    // change thead th datatable color
    $(".table-bordered thead th").css("background-color", "#0066CC");

    // change thead th datatable font color
    $("table thead th").css("color", "#fff");
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).on('click', '#btnShow', function() {
        var month = $('#search-month').val();
        var year = $('#search-year').val();
        ShowData(month, year)
    })
    function ShowData(month,year)
	{
		$.ajax({
				url : "{{route('rekap.table')}}",
				type: "GET",
				data:{
                month : month,
                year : year,
            },
				async:false,
				success: function(response)
				{
					// $("#RptGrafik").html(response);
                    console.log(response);false;
				}
			});

		$(".loading_bar").hide();
		return false;
	}
</script>
@endsection
