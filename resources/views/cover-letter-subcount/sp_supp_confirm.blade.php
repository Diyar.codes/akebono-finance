@extends('templates.main')

@section('title', 'Detail Surat Jalan')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">Detail Surat Jalan : {{ $nosur }}</h6>
                </div>
                <div class="row">
                    <div class="table-responsive mb-2">
                        <table class="table table-striped table-bordered text-center w-100" id="initable">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">Desc</th>
                                    <th scope="col">QTY</th>
                                    <th scope="col">Actual QTY</th>
                                    <th scope="col" width="50%">Remark</th>
                                </tr>
                            </thead>
                            <tbody id="result">
                                <?php $q = 1; $jml_row = count($data_item); ?>
                                @foreach($data_item as $di)
                                <tr>
                                    <td>{{ $q }}</td>
                                    <td>{{ $di->kode }}</td>
                                    <td>{{ $di->namabarang }}</td>
                                    <td>{{ $di->jumlah }}</td>
                                    <input type="hidden" name="nosur" id="nosur" value="{{ $nosur }}">
                                    <input type="hidden" name="id_item" id="id_item" value="{{ $di->kode }}">
                                    <input type="hidden" name="jml_row" id="jml_row" value="{{ $jml_row }}">
                                    <td><input type="text" name="act_qty[]" id="act_qty[]" class="form-control" value="{{ $di->act_qty }}"></td>
                                    <td><input type="text" name="remark[]" id="remark[]" class="form-control" value="{{ $di->remark }}"></td>
                                </tr>
                                <?php $q++ ?>
                                @endforeach
                                <tr>
                                    <td colspan="3">Attachment (pdf, image, excel)</td>
                                    <td colspan="3"><input type="file"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card custom-card">
                        <div class="card-body component-item">
                            <nav class="nav flex-column">
                                <h5>Note : </h5>
                                <li class ="nav-link"><font color="red">- Remark di isi jika Qty dari akebono berbeda dengan actual qty yang diterima supplier</font></li>
                                <li class ="nav-link">- Actual Qty di isi jika jumlah QTY dengan surat jalan berbeda, jika sama abaikan saja</li>
                                <li class ="nav-link"><font color="red">- Attachment di isi jika masih ada yang ingin di konfirmasikan</font></li>
                            </nav>
                        </div>
                    </div>
                </div>
                <a href="#" class="btn btn-success" onclick="confirmSp()">Save</a>
                    <a href="{{ route('sp-supp-view') }}" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </div>
</div>  

<!-- Modal effects -->
<div class="modal" id="modaldemo8">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100">Add Item</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form action="" action="" id="finsertitemkanban">
                    <div class="form-group row">
                        <label for="itemNumber" class="col-sm-3 col-form-label">item Number</label>
                        <div class="col-9">
                            <input type="text" class="form-control" id="item_number" name="item_number">
                        </div>
                        <small class="text-danger ml-3" id="vc-item_number"></small>
                    </div>
                    
                    <div class="form-group row">
                        <label for="supplier" class="col-sm-3 col-form-label">supplier</label>
                        <div class="col-7">
                            <input type="text" class="form-control" id="supplier" name="supplier">
                        </div>
                        <div class="col-2 box">
                            <button type="button" class="modal-effect btn btn-light btn-block searchSupplier" data-effect="effect-rotate-left" data-toggle="modal" data-target="#modaldemo8part2">
                                Search
                            </button>
                        </div>
                        <small class="text-danger ml-3" id="vc-supplier"></small>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div id="fprocessinsertitemkanban">
                    <button class="btn ripple btn-light" type="button" id="itemkanbaninsertbtn">Save</button>
                </div>
                <button class="btn ripple btn-light" data-dismiss="modal" type="button">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->

<!-- Modal effects -->
<div class="modal" id="modaldemo8part2">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered text-center w-100">
                    <thead class="thead">
                        <tr>
                            <th scope="col">Supplier Id</th>
                            <th scope="col">Supplier Name</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="tbodyselect">
                        <tr>
                            <td></td>
                            <td></td>
                            <td><button type="button" class="btn btn-success" id="selectbusiness">select</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->

@endsection
