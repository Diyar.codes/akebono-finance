@extends('templates.main')

@section('title', 'List Surat Pengantar')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-4 p-0 pl-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Supplier ID</span>
                        </div>
                    </div>
                    <div class="col-lg-1 col-2 p-0">
                        <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="supp_id" disabled>
                    </div>
                    <div class="ml-1 mr-1">
                        <button type="submit" class="btn btn-sm btn-sm-responsive btn-info" data-toggle="modal" data-target="#getSupplierTarget" id="seacrhSOAPBusinesRelationModal"><i class="fas fa-search"></i></button>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-4 p-0">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="supp_name" disabled>
                        </div>
                    </div>
                    <div class="col-1 p-0 text-center d-none d-lg-block">
                        <span class="exinput-custom-responsive">Or</span>
                    </div>
                    <div class="col-1 p-0 d-none d-lg-block">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">No surat</span>
                        </div>
                    </div>
                    <div class="col-2 p-0 d-none d-lg-block">
                        <input type="text" class="form-control form-control-sm  form-control-sm-responsive text-dark" id="filter_dn_number" name="filter_dn_number" autocomplete="off">
                    </div>
                </div>
                <div class="row mb-1 d-lg-none">
                    <div class="col-xl-2 col-md-3 col-4 p-0 pl-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">No surat</span>
                        </div>
                    </div>
                    <div class="col-sm-5 col-6 box mb-1">
                        <input type="text" class="form-control form-control-sm  form-control-sm-responsive text-dark" id="filter_dn_number" name="filter_dn_number" autocomplete="off">
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-4 p-0 pl-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Periode Transaction</span>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-8 p-0">
                        <div class="row">
                            <div class="col-6 mr-n2">
                                <select class="form-control form-control-sm form-control-sm-responsive text-dark" id="filter_moon">
                                    <option value="">Month</option>
                                    @for($i = 1; $i <= 12 ; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-6 ml-n2">
                                <select class="form-control form-control-sm form-control-sm-responsive text-dark" id="filter_year">
                                    <option value="">Year</option>
                                    @for($t = date('Y')-5; $t <= date('Y') ; $t++)
                                        <option value="{{ $t }}">{{ $t }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-sm btn-info btn-block mb-2" id="filter"><i class="fas fa-search"></i> Search</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <button type="button" class="btn btn-sm btn-danger btn-block btn-md" id="reset"><i class="fas fa-undo"></i> Reset</button>
                    </div>
                </div>
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center w-100" id="main-table">
                        <thead>
                            <tr class="backgroudrowblue">
                                <th scope="col" class="text-white">No</th>
                                <th scope="col" class="text-white">No Surat</th>
                                <th scope="col" class="text-white">Supplier</th>
                                <th scope="col" class="text-white">Date</th>
                                <th scope="col" class="text-white">Cycle</th>
                                <th scope="col" class="text-white">Pengirim</th>
                                <th scope="col" class="text-white">Kendaraan</th>
                                <th scope="col" class="text-white">Status</th>
                                <th scope="col" class="text-white">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="getSupplierTarget">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="get-supplier">
                        <thead class="thead">
                            <tr class="backgroudrowblue">
                                <th scope="col" class="text-white">Supplier Id</th>
                                <th scope="col" class="text-white">Supplier Name</th>
                                <th scope="col" class="text-white">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection
