@extends('templates.main')

@section('title', 'List Delivery Slip')

@section('body')
    <style>
       tr.spaceUnder>td {
        padding-bottom: 1em;
        }
</style>
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="row">
                    <table style="margin-left: 15px" class="tablekuh">
                        <tr>
                            <td><span class="input-group-text spanbox exinput-custom" id="basic-addon1">No Surat  </span></td>
                            <td><input type="text" class="form-control" name="nosur" id="nosur"></td>
                            <td><span class="badge badge-warning">OR</span></td>
                            <td width="25%"><span class="input-group-text spanbox exinput-custom" id="basic-addon1">Periode Transaction  </span></td>
                            <td>
                                <select name="select_bulan" id="select_bulan" class="form-control select2">
                                    <option value=""></option>
                                    @for($i = 1; $i <= 12 ; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </td>
                            
                            <td>
                                <select name="select_tahun" id="select_tahun" class="form-control select2">
                                    <option value=""></option>
                                    @for($t = date('Y')-4; $t <= date('Y') ; $t++)
                                    <option value="{{ $t }}">{{ $t }}</option>
                                    @endfor
                                </select>
                            </td>
                        </tr>
                        <tr class="spaceUnder">
                            <td><input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <a href="#" class="btn btn-success" style="margin-top:10px" onclick="search_nosur()">Search</a>  </span></td>
                            <td></td>
                            <td></td>
                            <td><a href="#" class="btn btn-success" style="margin-top:10px" onclick="search_periode()">Search</a>  </span></td>
                            
                        </tr>
                    </table>
                </div>
                
                <br><br>
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center w-100" id="initable">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">No Surat</th>
                                <th scope="col">Date</th>
                                <th scope="col">Kendaraan</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody id="result">
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="modaldemo8">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100">Add Item</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form action="" action="" id="finsertitemkanban">
                    <div class="form-group row">
                        <label for="itemNumber" class="col-sm-3 col-form-label">item Number</label>
                        <div class="col-9">
                            <input type="text" class="form-control" id="item_number" name="item_number">
                        </div>
                        <small class="text-danger ml-3" id="vc-item_number"></small>
                    </div>
                    
                    <div class="form-group row">
                        <label for="supplier" class="col-sm-3 col-form-label">supplier</label>
                        <div class="col-7">
                            <input type="text" class="form-control" id="supplier" name="supplier">
                        </div>
                        <div class="col-2 box">
                            <button type="button" class="modal-effect btn btn-light btn-block searchSupplier" data-effect="effect-rotate-left" data-toggle="modal" data-target="#modaldemo8part2">
                                Search
                            </button>
                        </div>
                        <small class="text-danger ml-3" id="vc-supplier"></small>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div id="fprocessinsertitemkanban">
                    <button class="btn ripple btn-light" type="button" id="itemkanbaninsertbtn">Save</button>
                </div>
                <button class="btn ripple btn-light" data-dismiss="modal" type="button">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->

<!-- Modal effects -->
<div class="modal" id="modaldemo8part2">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered text-center w-100">
                    <thead class="thead">
                        <tr>
                            <th scope="col">Supplier Id</th>
                            <th scope="col">Supplier Name</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="tbodyselect">
                        <tr>
                            <td></td>
                            <td></td>
                            <td><button type="button" class="btn btn-success" id="selectbusiness">select</button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->

@endsection
