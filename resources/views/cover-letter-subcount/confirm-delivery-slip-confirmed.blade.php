@extends('templates.main')

@section('title', 'List Surat Pengantar')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h2 class="main-content-label mb-3">DETAIL SURAT JALAN: {{ $no }}</h2>
                </div>
                <form action="" method="POST">
                    <div class="row main-table">
                        <div class="table-responsive mb-2">
                            <table class="table table-striped table-bordered text-center w-100 main-table" id="main-table">
                                <thead>
                                    <tr class="backgroudrowblue">
                                        <th scope="col" class="text-white">No</th>
                                        <th scope="col" class="text-white">Item</th>
                                        <th scope="col" class="text-white">Desc</th>
                                        <th scope="col" class="text-white">QTY</th>
                                        <th scope="col" class="text-white">Actual QTY</th>
                                    </tr>
                                </thead>
                                <tbody class="bodytable">
                                </tbody>
                            </table>
                            <p>Note : <i>Baris warna kuning menunjukan perbedaan QTY dengan Actual QTY</i></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 col-sm-4 col-5 my-2 mt-md-0">
                            <a href="{{ route('confirm-delivery-slip') }}" class="btn btn-sm btn-danger btn-block"><i class="fas fa-undo"></i> Back</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
