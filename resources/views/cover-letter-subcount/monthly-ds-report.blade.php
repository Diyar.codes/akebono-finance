@extends('templates.main')

@section('title', 'Laporan Mutasi Bulanan')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-4 p-0 pl-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Supplier ID</span>
                        </div>
                    </div>
                    <div class="col-lg-1 col-2 p-0">
                        <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="code" disabled>
                    </div>
                    <div class="ml-1 mr-1">
                        <button type="submit" class="btn btn-sm btn-sm-responsive btn-info" data-toggle="modal" data-target="#modal-businesrelation" id="search-businessrelation"><i class="fas fa-search"></i></button>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-4 p-0">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="name" disabled>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-xl-2 col-md-3 col-4 p-0 pl-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Periode Transaction</span>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-8 p-0">
                        <div class="row">
                            <div class="col-6 mr-n2">
                                <select class="form-control form-control-sm form-control-sm-responsive text-dark" id="moon">
                                    <option value="">Month</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                            <div class="col-6 ml-n2">
                                <select class="form-control form-control-sm form-control-sm-responsive text-dark" id="year">
                                    <option value="">Year</option>
                                    @for($t = date('Y')-5; $t <= date('Y') ; $t++)
                                        <option value="{{ $t }}">{{ $t }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-sm btn-info btn-block mb-2" id="search"><i class="fas fa-search"></i> Search</button>
                    </div>
                    {{-- <div class="col-lg-2 col-sm-4">
                        <button type="button" class="btn btn-sm btn-danger btn-block btn-md" id="reset"><i class="fas fa-undo"></i> Reset</button>
                    </div> --}}
                </div>
                <div class="row mb-2">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered text-center w-100" id="main-table">
                            <thead>
                                <tr class="backgroudrowblue">
                                    <th scope="col" rowspan="3" class="text-wrap align-middle text-white">No</th>
                                    <th scope="col" rowspan="3" class="text-wrap align-middle text-white">Part Number</th>
                                    <th scope="col" rowspan="3" class="text-wrap align-middle text-white">Description</th>
                                    <th scope="col" rowspan="3" class="text-wrap align-middle text-white">Type</th>
                                    <th scope="col" rowspan="3" class="text-wrap align-middle text-white">UM</th>
                                    <th scope="col" rowspan="3" class="text-wrap align-middle text-white">Buyer</th>
                                    <th scope="col" class="text-wrap align-middle text-white">B.Stock</th>
                                    <th scope="col" colspan="3" class="text-wrap align-middle text-white">IN</th>
                                    <th scope="col" class="text-wrap align-middle text-white">OUT</th>
                                    <th scope="col" rowspan="2" class="text-wrap align-middle text-white">ASJUST</th>
                                    <th scope="col" class="text-wrap align-middle text-white">END STOCK</th>
                                    <th scope="col" rowspan="3" class="text-wrap align-middle text-white">Ket</th>
                                </tr>
                                <tr class="backgroudrowblue">
                                    <th scope="col" class="text-wrap align-middle text-white">QAD</th>
                                    <th scope="col" class="text-wrap align-middle text-white">QAD</th>
                                    <th scope="col" class="text-wrap align-middle text-white">Portal</th>
                                    <th scope="col" class="text-wrap align-middle text-white">Selisih</th>
                                    <th scope="col" class="text-wrap align-middle text-white">QAD</th>
                                    <th scope="col" class="text-wrap align-middle text-white">QAD</th>
                                </tr>
                                <tr class="backgroudrowblue">
                                    <th scope="col" class="text-wrap align-middle text-white">(A)</th>
                                    <th scope="col" class="text-wrap align-middle text-white">(B)</th>
                                    <th scope="col" class="text-wrap align-middle text-white">(C)</th>
                                    <th scope="col" class="text-wrap align-middle text-white">(B-C)</th>
                                    <th scope="col" class="text-wrap align-middle text-white">(E)</th>
                                    <th scope="col" class="text-wrap align-middle text-white">(F)</th>
                                    <th scope="col" class="text-wrap align-middle text-white">(A+B) - (E-F)</th>
                                </tr>
                            </thead>
                            <tbody id="bodytable">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="modal-businesrelation">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="get-supplier">
                        <thead class="thead">
                            <tr class="backgroudrowblue">
                                <th scope="col" class="text-white">Supplier Id</th>
                                <th scope="col" class="text-white">Supplier Name</th>
                                <th scope="col" class="text-white">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection
