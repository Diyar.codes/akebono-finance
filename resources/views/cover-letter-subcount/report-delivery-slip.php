@extends('templates.main')

@section('title', 'List Surat Pengantar')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body minstylebody">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-5 col-5 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Supplier ID</span>
                            <input type="text" class="form-control form-control-sm" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-1 box">
                        <button type="submit" class="btn btn-info btn-search mb-2 ml-1"><i class="fas fa-search"></i></button>
                    </div>
                    <div class="col-lg-3 col-md-4 col-5 box">
                        <div class="input-group mb-3 ml-lg-n5 ml-md-n4 ml-sm-n3">
                            <input type="text" class="form-control form-control-sm" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-sm-6 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Periode Transaction</span>
                            <select class="form-control form-control-sm mr-1">
                                <option>Default select</option>
                                <option>Default select</option>
                                <option>Default select</option>
                            </select>
                            <select class="form-control form-control-sm">
                                <option>Default select</option>
                                <option>Default select</option>
                                <option>Default select</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-success btn-block mb-2" id="updateItemKanban"><i class="fas fa-search"></i> Search</button>
                    </div>
                </div>
                <table class="table table-striped table-bordered text-center text-nowrap w-100">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">No Surat</th>
                            <th scope="col">Supplier</th>
                            <th scope="col">Date</th>
                            <th scope="col">Cycle</th>
                            <th scope="col">Pengirim</th>
                            <th scope="col">Kendaraan</th>
                            <th scope="col">Status</th>
                            <th scope="col">type</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>SP21010690/WHS-PPC-AAIJ</td>
                            <td>AT INDONESIA, PT</td>
                            <td>26 Jan 2021 23:07:35:000</td>
                            <td>2</td>
                            <td>Muhaimin</td>
                            <td>b 9473 pxr</td>
                            <td>Not Confirmed</td>
                            <td>Box/Palet</td>
                            <td>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-info" ><i class="far fa-eye"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-primary" ><i class="fas fa-print"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-warning" ><i class="fas fa-edit"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-danger" ><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>SP21010690/WHS-PPC-AAIJ</td>
                            <td>AT INDONESIA, PT</td>
                            <td>26 Jan 2021 23:07:35:000</td>
                            <td>2</td>
                            <td>Muhaimin</td>
                            <td>b 9473 pxr</td>
                            <td>Not Confirmed</td>
                            <td>Box/Palet</td>
                            <td>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-info" ><i class="far fa-eye"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-primary" ><i class="fas fa-print"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-warning" ><i class="fas fa-edit"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-danger" ><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>SP21010690/WHS-PPC-AAIJ</td>
                            <td>AT INDONESIA, PT</td>
                            <td>26 Jan 2021 23:07:35:000</td>
                            <td>2</td>
                            <td>Muhaimin</td>
                            <td>b 9473 pxr</td>
                            <td>Not Confirmed</td>
                            <td>Box/Palet</td>
                            <td>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-info" ><i class="far fa-eye"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-primary" ><i class="fas fa-print"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-warning" ><i class="fas fa-edit"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-danger" ><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>SP21010690/WHS-PPC-AAIJ</td>
                            <td>AT INDONESIA, PT</td>
                            <td>26 Jan 2021 23:07:35:000</td>
                            <td>2</td>
                            <td>Muhaimin</td>
                            <td>b 9473 pxr</td>
                            <td>Not Confirmed</td>
                            <td>Box/Palet</td>
                            <td>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-info" ><i class="far fa-eye"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-primary" ><i class="fas fa-print"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-warning" ><i class="fas fa-edit"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-danger" ><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>SP21010690/WHS-PPC-AAIJ</td>
                            <td>AT INDONESIA, PT</td>
                            <td>26 Jan 2021 23:07:35:000</td>
                            <td>2</td>
                            <td>Muhaimin</td>
                            <td>b 9473 pxr</td>
                            <td>Not Confirmed</td>
                            <td>Box/Palet</td>
                            <td>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-info" ><i class="far fa-eye"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-primary" ><i class="fas fa-print"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-warning" ><i class="fas fa-edit"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-danger" ><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>SP21010690/WHS-PPC-AAIJ</td>
                            <td>AT INDONESIA, PT</td>
                            <td>26 Jan 2021 23:07:35:000</td>
                            <td>2</td>
                            <td>Muhaimin</td>
                            <td>b 9473 pxr</td>
                            <td>Not Confirmed</td>
                            <td>Box/Palet</td>
                            <td>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-info" ><i class="far fa-eye"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-primary" ><i class="fas fa-print"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-warning" ><i class="fas fa-edit"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-danger" ><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>SP21010690/WHS-PPC-AAIJ</td>
                            <td>AT INDONESIA, PT</td>
                            <td>26 Jan 2021 23:07:35:000</td>
                            <td>2</td>
                            <td>Muhaimin</td>
                            <td>b 9473 pxr</td>
                            <td>Not Confirmed</td>
                            <td>Box/Palet</td>
                            <td>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-info" ><i class="far fa-eye"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-primary" ><i class="fas fa-print"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-warning" ><i class="fas fa-edit"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-danger" ><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>SP21010690/WHS-PPC-AAIJ</td>
                            <td>AT INDONESIA, PT</td>
                            <td>26 Jan 2021 23:07:35:000</td>
                            <td>2</td>
                            <td>Muhaimin</td>
                            <td>b 9473 pxr</td>
                            <td>Not Confirmed</td>
                            <td>Box/Palet</td>
                            <td>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-info" ><i class="far fa-eye"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-primary" ><i class="fas fa-print"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-warning" ><i class="fas fa-edit"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-danger" ><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>SP21010690/WHS-PPC-AAIJ</td>
                            <td>AT INDONESIA, PT</td>
                            <td>26 Jan 2021 23:07:35:000</td>
                            <td>2</td>
                            <td>Muhaimin</td>
                            <td>b 9473 pxr</td>
                            <td>Not Confirmed</td>
                            <td>Box/Palet</td>
                            <td>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-info" ><i class="far fa-eye"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-primary" ><i class="fas fa-print"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-warning" ><i class="fas fa-edit"></i></a>
                                <a href="javascript:void(0)" data-id="" class="btn btn-sm btn-danger" ><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
