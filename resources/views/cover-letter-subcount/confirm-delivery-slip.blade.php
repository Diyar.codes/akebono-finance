@extends('templates.main')

@section('title', 'List Surat Pengantar')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="row mb-2">
                    <div class="col-xl-2 col-md-3 col-4 p-0 pl-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">No Surat</span>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-7 p-0">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="no_surat">
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-sm btn-info btn-block mb-2" id="searchbusinessrelation"><i class="fas fa-search"></i> Search</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <button type="button" class="btn btn-sm btn-danger btn-block btn-md" id="resetbusinessrelation"><i class="fas fa-undo"></i> Reset</button>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-xl-2 col-md-3 col-4 p-0 pl-3">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Periode Transaction</span>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-8 p-0">
                        <div class="row">
                            <div class="col-6 mr-n2">
                                <select class="form-control form-control-sm form-control-sm-responsive text-dark" id="moon">
                                    <option value="">Month</option>
                                    @for($i = 1; $i <= 12 ; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-6 ml-n2">
                                <select class="form-control form-control-sm form-control-sm-responsive text-dark" id="year">
                                    <option value="">Year</option>
                                    @for($t = date('Y')-5; $t <= date('Y') ; $t++)
                                        <option value="{{ $t }}">{{ $t }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-sm btn-info btn-block mb-2" id="searchperiode"><i class="fas fa-search"></i> Search</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <button type="button" class="btn btn-sm btn-danger btn-block btn-md" id="resetperiode"><i class="fas fa-undo"></i> Reset</button>
                    </div>
                </div>
                <div class="row mt-2 t-automatical" style="display: none">
                    <form action="{{ route('excel-confirm-delivery-slip-view-main') }}" method="POST">
                        @csrf
                        <div class="col-1 mr-n5">
                            <input type="hidden" value="" name="moonExcel" id="moonExcel">
                            <input type="hidden" value="" name="yearExcel" id="yearExcel">
                            <input type="hidden" value="{{ Auth::guard('pub_login')->user()->kode_supplier }}" name="userExcel" id="userExcel">
                            <button type="submit" class="btn btn-outline-success btn-sm btn-icon mb-2 counterexcel" data-toggle="tooltip" data-placement="top" title="Download Excel"><i class="si si-cloud-download"></i></button>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="table-responsive mb-2">
                        <table class="table table-striped table-bordered text-center w-100" id="main-table">
                            <thead>
                                <tr class="backgroudrowblue">
                                    <th scope="col" class="text-white">No</th>
                                    <th scope="col" class="text-white">No Surat</th>
                                    <th scope="col" class="text-white">Date</th>
                                    <th scope="col" class="text-white">Kendaraan</th>
                                    <th scope="col" class="text-white">Sub Tujuan</th>
                                    <th scope="col" class="text-white">Action</th>
                                </tr>
                            </thead>
                            <tbody >
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="modal-businesrelation">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered text-center w-100" id="main-businessrelation">
                    <thead class="thead">
                        <tr class="backgroudrowblue">
                            <th scope="col" class="text-white">Supplier Id</th>
                            <th scope="col" class="text-white">Supplier Name</th>
                            <th scope="col" class="text-white">Action</th>
                        </tr>
                    </thead>
                    <tbody class="tbodyselect">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection
