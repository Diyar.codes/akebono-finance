@extends('templates.main')

@section('title', 'List Surat Pengantar')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h2 class="main-content-label mb-3">DETAIL SURAT JALAN : {{ $no }}</h2>
                </div>
                <form method="POST" enctype="multipart/form-data">
                    {{-- @csrf --}}
                    {{-- @method('PUT') --}}
                    <div class="row main-table">
                        <div class="table-responsive mb-2">
                            <table class="table table-striped table-bordered text-center w-100 main-table" id="main-table">
                                <thead>
                                    <tr class="backgroudrowblue">
                                        <th scope="col" class="text-white">No</th>
                                        <th scope="col" class="text-white">Item</th>
                                        <th scope="col" class="text-white">Desc</th>
                                        <th scope="col" class="text-white">QTY</th>
                                        <th scope="col" class="text-white">Actual QTY</th>
                                        <th scope="col" class="text-white">Remark</th>
                                    </tr>
                                </thead>
                                <tbody class="bodytable">
                                </tbody>
                                @if(Auth::guard('pub_login')->user()->kode_supplier == '106' )
                                <tbody>
                                    <tr>
                                        <td colspan='3'><span class="font-weight-bold">Attachment</span>(pdf atau image)</td>
                                        <td colspan='3'>
                                            <form>
                                                <div class="form-group">
                                                    <input type="file" class="form-control-file" id="attachment" name="attachment">
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                                @else
                                <tbody style="display: none">
                                    <tr>
                                        <td colspan='3'><span class="font-weight-bold">Attachment</span>(pdf atau image)</td>
                                        <td colspan='3'>
                                            <form>
                                                <div class="form-group">
                                                    <input type="file" class="form-control-file" id="attachment" name="attachment">
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                                @endif
                            </table>
                            <h6>Note : </h6>
                            <h6 style="color: red">- Remark di isi jika Qty delivery dari akebono berbeda dengan actual qty yang diterima supplier / untuk catatan.</h6>
                            <h6>- Actual Qty di isi jika jumlah QTY dengan surat jalan berbeda, jika sama abaikan saja.</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-lg-2 my-2 mt-md-0">
                            <button type="submit" class="btn btn-sm btn-success btn-block" id="save"><i class="fas fa-save"></i> Save</button>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-lg-2 my-2 mt-md-0">
                            <a href="{{ route('confirm-delivery-slip') }}"><button type="button" class="btn btn-sm btn-danger btn-block"><i class="fas fa-undo"></i> Back</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
