<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
	<title>Cetak Surat Jalan</title>
	<style type="text/css">
	#bodyContainer{
		background-color:white;
		margin:10px;
		position:relative;
		clear:both;
	}
	table{
		width:700px;
		font-family: Arial;
		border: 0px solid black;
		font-weight:;
		text-align:;
		vertical-align:middle;
		/*background-color:#FFFFCC ;*/
		background-color:#FFFFFF;
		border-left:0px solid #EDF1F8;
		border-top:0px solid #EDF1F8;
		border-bottom:0px solid #B5C7E3;
		border-right:0px solid #B5C7E3;
		inherit:no;
	}
	table.borderless {
	border-width: 0px;
	text-transform: none;
	}
	table.borderless th {
	border-width: 0px;
	}
	table.borderless td {
    border-width: 0px;
    }
	table.border tr {
	border-width: 0px;
	text-transform: none;
	}
	table.bordertr th {
	border-width: 0px;
	}
	table.bordertr td {
    border-width: 0px;
    }
	</style>
</head>
<body>
    <center>
    <table border="0" cellspacing="0" cellpadding="0" class='' style="border: 1px solid black">
        <td>
        <TABLE class="borderless">
            <tr>
                <td><img src="{{ asset('img/auth') }}/akebonologo.png" width="150px"></td>
                <td><b>PT. AKEBONO BRAKE ASTRA INDONESIA</b><br/><font style="font-size:10px">Jl.Pegangsaan Dua Blok A1, Km. 1,6<br/>Pegangsaan Dua, Kelapa Gading, Jakarta Utara<br/>DKI Jakarta Raya, 14250-INDONESIA</font></td>
                <TD><font style="font-size:10px">Tel +62 21 46830075<br/>Fax +62 21 46826659<br/>Email info@akebono-astra.co.id<br/>Url www.akebono-astra.co.id</font></td>
            </tr>
        </TABLE>
        <!--<hr style="color=black; background-color:black; height=1px"/>-->
        ________________________________________________________________________________________
        <table class="borderless">
            <tr>
                <td style="font-size:11px" width="80" style="border:0">Di kirim ke</td>
                <td>:</td>
                <td colspan="" width="220" style="font-size:11px">{{ $data_supp->ct_ad_name }}</td>
                {{-- <td rowspan="3" style="font-size:15px" align="center"><b><u>SURAT PENGANTAR</u></b><br/><font size="25"><b>NO : <u> {{ $id }}."(".{{ $data_supp->cycle }}.")";</u></b></font></td> --}}
				<td rowspan="3" style="font-size:15px" align="center"><b><u>SURAT PENGANTAR</u></b><br/><font size=""><b>NO : <u> {{ $id }} ( {{ $data_supp->cycle }} )</u></b></font></td>
				{{-- <td rowspan="3" style="font-size:15px" align="center"><b><u>SURAT PENGANTAR</u></b><br/><font size="25"><b>NO : <u><?echo $no."(".$cycle.")";?></u></b></font></td> --}}

            </tr>
            <tr>
                <td style="font-size:11px" width="">Tanggal</td>
                <td>:</td>
                <td colspan="" width="" style="font-size:11px" >{{ \Carbon\Carbon::parse($data_supp->tanggal)->format('d-m-Y') }}</td>
            </tr>
            <tr>
                <td style="font-size:11px">Kendaraan</td>
                <td>:</td>
                <td width="" style="font-size:11px">{{ $data_supp->kendaraan }}</td>
            </tr>
        </table>
        <p/>

        <table class="borderless" >
            <tr align="left" border='' >
                <td style="font-size:11px">NO</td>
                <td style="font-size:11px">KODE</td>
                <td style="font-size:11px">NAMA BARANG</td>
                <td style="font-size:11px">JUMLAH</td>
                <td style="font-size:11px">KETERANGAN</td>
            </tr>
            @php
                $no = 1;
            @endphp
            @foreach($data_item as $i => $data)
            <tr>
                <td style="font-size:11px;" width="15" align="center">{{ $no++ }}</td>
                <td style="font-size:11px;" width="120">{{ $data->kode }}</td>
                <td style="font-size:11px;" width="250">{{ $data->namabarang }}</td>
                <td style="font-size:11px;" width="50">{{ $data->jumlah }}; &nbsp; {{ $data->satuan }}</td>
                @if($data_item_count <= 8)
                    @if($i == 0)
                    <td style="font-size:11px;" width="130" VALIGN='top'>{{ $data_supp->keterangan }}</td>
                    @endif
                @endif
            </tr>
            @endforeach
        </table>
        ________________________________________________________________________________________
        <table class="borderless">
            <tr>
                <td width="200px" style="font-size:11px">Note :</td>
                <td align="center" style="font-size:11px">Security</td>
                <td align="center" style="font-size:11px">Sopir</td>
                <td align="center" style="font-size:11px">Penerima</td>
                <td width="100px" align="center" style="font-size:11px">Pengirim</td>
            </tr>
            <tr>
                <td rowspan="6" style="font-size:11px" width="20">Lembar pertama <br/>harap dikembalikan ke AAIJ<br/> *Nama Terang</td>
                <td rowspan="3"><br/><br/>*(____________)</td>
                <td rowspan="3"><br/><br/>*(____________)</td>
                <td rowspan="3"><br/><br/>*(____________)</td>
                @if($data_supp->pengirim == "")
                    <td rowspan='3'><br/><br/>*(____________)</td>
                @else
                    <td rowspan="3" align='center'><br/><br/>*(<u>{{ $data_supp->pengirim }}</u>)</td>
                @endif
            </tr>
        </table>
        </td>
    </table>
    <br>
    <br>
    <br>
    <br>
    </center>
</body>
</html>
<script language="javascript">
    window.print();
</script>
