@extends('templates.main')

@section('title', 'List Surat Pengantar')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body minstylebody">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-5 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text exinput-custom spanbox mr-4" id="basic-addon1">Supplier ID</span>
                            <input type="text" class="form-control form-control-sm" placeholder="">
                        </div>
                    </div>
                    <div class="col-1 box">
                        <button type="submit" class="btn btn-info btn-search mb-2 ml-xl-3 ml-lg-2 ml-md-1 ml-2"><i class="fas fa-search"></i></button>
                    </div>
                    <div class="col-lg-3 col-md-4 col-5 box">
                        <div class="input-group mb-3 ml-lg-n5 ml-md-n4 ml-sm-n3">
                            <input type="text" class="form-control form-control-sm" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row mt-lg-n2">
                    <div class="col-md-6 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Periode Transaction</span>
                            <select class="form-control form-control-sm mr-1" id="exampleFormControlSelect1">
                                <option selected>Choose Moon</option>
                                <option>01</option>
                                <option>02</option>
                                <option>03</option>
                                <option>04</option>
                                <option>05</option>
                                <option>06</option>
                                <option>07</option>
                                <option>08</option>
                                <option>09</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                            </select>
                            <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                                <option selected>Choose Year</option>
                                <option>2018</option>
                                <option>2019</option>
                                <option>2020</option>
                                <option>2021</option>
                                <option>2022</option>
                                <option>2023</option>
                                <option>2024</option>
                                <option>2025</option>
                                <option>2026</option>
                                <option>2027</option>
                                <option>2028</option>
                                <option>2029</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row mt-lg-n2">
                    <div class="col-md-6 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom mr-4" id="basic-addon1">Status</span>
                            <select class="form-control form-control-sm mr-1" id="exampleFormControlSelect1">
                                <option selected>status</option>
                                <option>Confirmed</option>
                                <option>Not Confirmed</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row mt-lg-n2">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-success btn-block mb-2" id="updateItemKanban"><i class="fas fa-search"></i> Search</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <button type="button" class="btn btn-danger btn-block mb-2"><i class="fas fa-download"></i> Download</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Item</th>
                                <th scope="col">Desc</th>
                                <th scope="col">1</th>
                                <th scope="col">2</th>
                                <th scope="col">3</th>
                                <th scope="col">4</th>
                                <th scope="col">5</th>
                                <th scope="col">6</th>
                                <th scope="col">7</th>
                                <th scope="col">8</th>
                                <th scope="col">9</th>
                                <th scope="col">10</th>
                                <th scope="col">11</th>
                                <th scope="col">12</th>
                                <th scope="col">13</th>
                                <th scope="col">14</th>
                                <th scope="col">15</th>
                                <th scope="col">16</th>
                                <th scope="col">17</th>
                                <th scope="col">18</th>
                                <th scope="col">19</th>
                                <th scope="col">20</th>
                                <th scope="col">21</th>
                                <th scope="col">22</th>
                                <th scope="col">23</th>
                                <th scope="col">24</th>
                                <th scope="col">25</th>
                                <th scope="col">26</th>
                                <th scope="col">27</th>
                                <th scope="col">28</th>
                                <th scope="col">29</th>
                                <th scope="col">30</th>
                                <th scope="col">31</th>
                                <th scope="col">TOtal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>S1-23302-27124-M</td>
                                <td>BODY CALIPER ORG Ry</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>300</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>300</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>S1-23302-27124-M</td>
                                <td>BODY CALIPER ORG Ry</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>300</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>300</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>S1-23302-27124-M</td>
                                <td>BODY CALIPER ORG Ry</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>300</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>300</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>S1-23302-27124-M</td>
                                <td>BODY CALIPER ORG Ry</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>300</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>300</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>S1-23302-27124-M</td>
                                <td>BODY CALIPER ORG Ry</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>300</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>300</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>S1-23302-27124-M</td>
                                <td>BODY CALIPER ORG Ry</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>300</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>300</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
