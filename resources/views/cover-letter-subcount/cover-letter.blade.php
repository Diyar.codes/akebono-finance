@extends('templates.main')

@section('title', 'Surat Pengantar')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body minstylebody">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <form action="{{route('store-cover-letter')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-lg-3 col-sm-6 box">
                            <div class="input-group mb-2">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Pilih Type</span>
                                <select id="selecttype" name="typesurat" class="form-control form-control-sm"
                                    onchange="selectType(this.value)">
                                    <option>Select One</option>
                                    <option value="item">Item</option>
                                    <option value="box/palet">Box/Palet</option>
                                    <option value="kosong">Kosong</option>
                                    <option value="ng">NG</option>
                                    <option value="return">Return</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="parsData"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="searchSupplier">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal"
                    type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="main-tablee">
                        <thead class="thead">
                            <tr>
                                <th scope="col">Supplier Id</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody id="body">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects end-->
@endsection

