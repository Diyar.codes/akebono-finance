@extends('templates.main')

@section('title', 'Laporan Mutasi Bulanan')

@section('body')

<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
               <div class="row mt-2">
                    <form action="{{ route('excel-monthly-ds-report-detail') }}" method="POST">
                        @csrf
                        <div class="col-1 mr-n5">
                            <input type="hidden" value="" name="kodeExcel" id="kodeExcel">
                            <input type="hidden" value="" name="moonExcel" id="moonExcel">
                            <input type="hidden" value="" name="yearExcel" id="yearExcel">
                            <input type="hidden" value="" name="loctoExcel" id="loctoExcel">
                            <input type="hidden" value="" name="appExcel" id="appExcel">
                            <button type="submit" class="btn btn-outline-success btn-sm btn-icon mb-2 counterexcel" data-toggle="tooltip" data-placement="top" title="Download Excel"><i class="si si-cloud-download"></i></button>
                        </div>
                    </form>
                </div>
                <div class="row mb-2">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered text-center w-100 main-table" id="main-table">
                            <thead>
                                <tr class="backgroudrowblue">
                                    <th scope="col" rowspan="3" class="text-wrap align-middle text-white">TANGGAL</th>
                                    <th scope="col" rowspan="3" class="text-wrap align-middle text-white">Part Number</th>
                                    <th scope="col" colspan="3" class="text-wrap align-middle text-white">JENIS TRANSAKSI</th>
                                </tr>
                                <tr class="backgroudrowblue">
                                    <th scope="col" class="text-wrap align-middle text-white">PORTAL</th>
                                    <th scope="col" colspan="2" class="text-wrap align-middle text-white">IN</th>
                                </tr>
                                <tr class="backgroudrowblue">
                                    <th scope="col" class="text-wrap align-middle text-white">QAD</th>
                                    <th scope="col" class="text-wrap align-middle text-white">IN</th>
                                    <th scope="col" class="text-wrap align-middle text-white">OUT</th>
                                </tr>
                            </thead>
                            <tbody id="bodytable">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
