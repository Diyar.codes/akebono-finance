@extends('templates.main')

@section('title', 'View Delivery Slip')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body minstylebody">
                <div>
                    <h4 class="font-weight-normal no-surat"><b>DETAIL SURAT JALAN : </b><span><b>{{ $ambil_no->no }}</b></span></h4>
                </div>
                <hr>
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100 main-table" id="main-table">
                        <thead>
                            <tr class="backgroudrowblue">
                                <th scope="col" class="text-white">No</th>
                                <th scope="col" class="text-white">Item</th>
                                <th scope="col" class="text-white">Desc</th>
                                <th scope="col" class="text-white">QTY</th>
                                <th scope="col" class="text-white">Actual QTY</th>
                            </tr>
                        </thead>
                        <tbody class="bodytable">
                        </tbody>
                        <tbody>
                            <tr>
                                <td colspan="2">Attachment</td>
                                <td colspan="3"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p><h6>Note :<i> Baris warna kuning menunjukan perbedaan QTY dengan Actual QTY</i></h6></p>
                <div class="row">
                    <div class="col-lg-2 col-sm-4">
                        <a href="{{ route('print-delivery-slip') }}"><button type="reset" class="btn btn-danger btn-block btn-sm mb-2"><i class="fas fa-undo"></i> Back</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
