@extends('templates.main')

@section('title', 'View List Miss Packing')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body minstylebody">
                {{-- <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div> --}}
                <div>
                    <h4 class="font-weight-normal no-surat">DIFFERENT QTY SURAT JALAN : {{ $misspackings->no }}</h4>
                </div>
                <hr>
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100 main-table" id="main-table">
                        <thead>
                            <tr class="backgroudrowblue">
                                <th scope="col" class="text-white">No</th>
                                <th scope="col" class="text-white">Item</th>
                                <th scope="col" class="text-white">Desc</th>
                                <th scope="col" class="text-white">QTY</th>
                                <th scope="col" class="text-white">Actual QTY</th>
                            </tr>
                        </thead>
                        <tbody class="bodytable">
                        </tbody>
                    </table>
                    @if($misspackings->remarks == null)
                    <div class="row mb-1">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="remark">Remark</label>
                                <textarea class="form-control text-dark" id="remarks" name="remarks" rows="3" cols="1"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-success btn-block mb-2" id="updateRemarks"><i class="fas fa-save"></i> Save</button>
                                </div>
                                <div class="col-6">
                                    <a href="{{ route('list-miss-packing') }}"><button type="button" class="btn btn-danger btn-block mb-2"><i class="fas fa-undo"></i> Back</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="row mb-1">
                        <div class="col-md-4">
                            Remarks : Checked by {{ $misspackings->remarks_by }}, {{ $misspackings->remarks_date }}<BR/><br/>
                            <textarea class="form-control text-dark" rows="3" cols="1" readonly>{{ $misspackings->remarks }}</textarea>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <a href="{{ route('list-miss-packing') }}"><button type="button" class="btn btn-danger btn-block mb-2"><i class="fas fa-undo"></i> Back</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
