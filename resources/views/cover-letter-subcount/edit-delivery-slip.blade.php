@extends('templates.main')

@section('title', 'Edit Delivery Slip')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body minstylebody">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div>
                    <h4 class="font-weight-normal no-surat"><b>EDIT SJ KOSONG :</b><span><b>{{ $ambil_no->no }}</b></span></h4>
                </div>
                <hr>
                <form action="/update-delivery-slip/{{ $id }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="table-responsive mb-2">
                        <table class="table table-striped table-bordered text-center text-nowrap w-100 main-table" id="main-table">
                            <thead>
                                <tr class="backgroudrowblue">
                                    <th scope="col" class="text-white">No</th>
                                    <th scope="col" class="text-white">Item</th>
                                    <th scope="col" class="text-white">Desc</th>
                                    <th scope="col" class="text-white">QTY</th>
                                    <th scope="col" class="text-white">Action</th>
                                </tr>
                            </thead>
                            <tbody class="bodytable">
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <h4 class="font-weight-normal">Add Line Surat Jalan</h4>
                    </div>
                    <hr>
                    <div class="table-responsive after-add-more mb-2">
                        <table class="table table-striped table-bordered text-center text-nowrap w-100">
                            <thead>
                                <tr class="backgroudrowblue">
                                    <th scope="col" class="text-white">Item</th>
                                    <th scope="col" class="text-white">Desc</th>
                                    <th scope="col" class="text-white">UM</th>
                                    <th scope="col" class="text-white">QTY</th>
                                    <th scope="col" class="text-white">Action</th>
                                </tr>
                            </thead>
                            <tbody class="tbody">
                                <tr>
                                    <td class="spacer p-3" style="padding-right: 3%">
                                        <input type="text" id="item[]" name="item[]" class="form-control form-control-sm item text-dark" style="min-width: 80px" readonly>
                                        <button type="button" class="btn btn-info btn-search pl-1 pr-1" data-effect="effect-rotate-left" data-toggle="modal" data-target="#getPtMstrTarget" id="seacrhSOAPPTMstrModal"><i class="fas fa-search"></i></button>
                                    </td>
                                    <td><input type="text" class="form-control form-control-sm text-dark desc" style="min-width:80px" name="desc[]" id="desc[]" readonly></td>
                                    <td><input type="text" class="form-control form-control-sm text-dark um" style="min-width:80px" name="um[]" id="um[]" readonly></td>
                                    <td><input type="number" class="form-control form-control-sm text-dark qty" style="min-width:80px" name="qty[]" id="qty[]"></td>
                                    <td><a href=""></a></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="d-flex flex-row-reverse">
                            <button class="btn btn-success btn-md add-more my-2 px-5" id="addRow" type="button">
                                <i class="fas fa-plus"></i> Add Item
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-sm-4">
                            <button type="submit" class="btn btn-success btn-block btn-sm mb-2"><i class="fas fa-save"></i> Save</button>
                        </div>
                        <div class="col-lg-2 col-sm-4">
                            <a href="{{ route('print-delivery-slip') }}"><button type="button" class="btn btn-danger btn-block btn-sm mb-2"><i class="fas fa-undo"></i> Back</button></a>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- get supplier Modal effects -->
<div class="modal" id="getPtMstrTarget">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="get-ptmstr">
                        <thead class="thead">
                            <tr class="backgroudrowblue">
                                <th scope="col" class="text-white">Item Number</th>
                                <th scope="col" class="text-white">Description</th>
                                <th scope="col" class="text-white">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection
