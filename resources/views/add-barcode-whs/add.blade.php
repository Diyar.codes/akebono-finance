@extends('templates.main')
@php
    use Illuminate\Support\Facades\DB;
@endphp
@section('title', 'Add Barcode WHS')
@section('stylest')
    <style>
        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            background-color: rgb(0, 102, 204);
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button {
            background-color: rgb(0, 102, 204);
        }
    </style>
@endsection
@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body mt-3">
                <div>
                    <h6 class="main-content-label mb-3">Registrasi Barcodes</h6>
                </div>
                <hr class="mg-b-40">
                <form action="{{route('addbarcode.store')}}" method="POST">
                    @csrf
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Item Number</span>
                            <input type='text' required name='itm_nbr' id='itm_nbr' size="20" class="form-control form-control-sm" value='' readonly />
                            <button type="button" class="edit btn btn-info btn-sm mr-2"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Deskripsi</span>
                            <input type='text' required name='itm_desc' id='itm_desc' size="30" class="form-control form-control-sm" value='' readonly />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Type</span>
                            <input type='text' required name='type' id='type' size="25" class="form-control form-control-sm"   />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-12 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom">SNP</span>
                            <input type='number' required name='snp' id='snp' size="10" class="form-control form-control-sm"   />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-12 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom">Total Box</span>
                            <input type='number' required name='box' id='box' size="10" class="form-control form-control-sm"   />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-sm-6 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom">Location From</span>
                            <input type='text' readonly required name='loc_from' id='loc_from' size="10" class="form-control form-control-sm"   />
                            <button type="button" class="addloc btn btn-info btn-sm mr-2" data-id="loc_from"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-sm-6 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom">Location To</span>
                            <input type='text' readonly required name='loc_to' id='loc_to' size="10" class="form-control form-control-sm"   />
                            <button type="button" class="addloc btn btn-info btn-sm mr-2" data-id="loc_to"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-sm-6 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom">Common</span>
                            <select required id="selecttype" name="common" class="form-control form-control-sm">
                                <option value='False' >NO</option>
                                <option value='True' >Yes</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-12 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom">Back No</span>
                            <input required type='text' name='backno' id='backno' size="25" class="form-control form-control-sm"   />
                        </div>
                    </div>
                </div>
                <div id="info">
                    <button class="btn btn-info" id="next">Save</button>
                </div>
            </form>
            </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal item -->
<div class="modal" id="modalitem">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="main-table2">
                        <thead class="thead">
                            <tr>
                                <th scope="col">Item Number</th>
                                <th scope="col">Description 1</th>
                                <th scope="col">Description 2</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
<!-- Modal loc -->
<div class="modal" id="modalloc">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="main-table1">
                        <thead class="thead">
                            <input type="text" style="display:none;" id="getid">
                            <tr>
                                <th scope="col">Location</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @foreach ($data as $key=>$dt)
                            <tr>
                                <td>@if($dt['locLoc'] == empty) - @else {!!$dt['locLoc']!!}@endif</td>
                                <td>@if($dt['locLoc'] == empty) -@else {!!$dt['locDesc']!!} @endif</td>
                                <td>
                                    <button type="button" name="edit"  class="edit btn btn-info btn-sm"><i class="fas fa-search-plus"></i></button></td>
                                <td>
                            </tr>
                            @endforeach --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->


@endsection

@section('scripts')
{{-- <script>
    $(document).ready(function() {
        var table = $('#main-table1').DataTable({
            // "autoWidth": false,
            "responsive": true,
            "paginate":false,
        });
    });
</script> --}}
<script>
    $(()=> {
     // gate date
    fill_datatable1()
    fill_datatable2()

    // $(document).on('click', '#seacrhSOAPBusinesRelation', function(){
    //     $('#main-table').DataTable().clear().destroy()
    //     getSupplier();
    })
    function update(item) {
        $('#modaledit').modal('hide');
    }
    function fill_datatable2() {
        $('#main-table2').DataTable({
            processing: true,
            serverSide: true,
            // paginate: false,
            // searching: false,
            ajax: {
                url: `${BASEURL}add-barcode-whs/get`,
            },
            columns:[
                // {
                //     data: "id",
                //     render: function(data, type, row, meta) {
                //         return meta.row + meta.settings._iDisplayStart + 1;
                //     }
                // },
                {
                    data: 'item_number',
                    name: 'item_number',
                },
                {
                    data: 'deskripsi1',
                    name: 'deskripsi1',
                },
                {
                    data: 'deskripsi2',
                    name: 'deskripsi2',
                },
                {
                    data: 'actione',
                    name: 'actione',
                },
                // {
                //     data: 'action',
                //     name: 'action',
                // },
                // {
                //     data: 'locto',
                //     name: 'locto',
                // },
            ],
        })

    // change thead th datatable color
    $(".table-bordered thead th").css("background-color", "#0066CC");

    // change thead th datatable font color
    $("table thead th").css("color", "#fff");
}
function fill_datatable1() {
        $('#main-table1').DataTable({
            processing: true,
            serverSide: true,
            // paginate: false,
            // searching: false,
            ajax: {
                url: `${BASEURL}add-barcode-whs/getl`,
            },
            columns:[
                // {
                //     data: "id",
                //     render: function(data, type, row, meta) {
                //         return meta.row + meta.settings._iDisplayStart + 1;
                //     }
                // },
                {
                    data: 'locloc',
                    name: 'locloc',
                },
                {
                    data: 'locdesc',
                    name: 'locdesc',
                },
                {
                    data: 'actione',
                    name: 'actione',
                },
                // {
                //     data: 'action',
                //     name: 'action',
                // },
                // {
                //     data: 'locto',
                //     name: 'locto',
                // },
            ],
        })
        // change thead th datatable color
    $(".table-bordered thead th").css("background-color", "#0066CC");

    // change thead th datatable font color
    $("table thead th").css("color", "#fff");
    }
$(document).on('click', '.edit', function () {
    $('#modalitem').modal('show');
});
$(document).on('click', '.addloc', function () {
    dataId = $(this).data('id');
    $('#getid').val(dataId);
    $('#modalloc').modal('show');
});
// $(document).on('click', '.update', function () {
//     $(this).html("Loading....");
// });
$(document).on('click', '#added', function () {
    data = $(this).data('itema');
    console.log(data);
    $('#itm_nbr').val(data);
    datb = $(this).data('itemb');
    $('#itm_desc').val(datb);
    $('#modalitem').modal('hide');
});
$(document).on('click', '.add', function () {
    console.log("ok");
    idka = $('#getid').val();
    data = $(this).data('itema');
    $(`#${idka}`).val(data);
    $('#modalloc').modal('hide');
});
$(document).on('click', '#next', function () {
    $(this).html('Loading...')
});
</script>
@endsection
