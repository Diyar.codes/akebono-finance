@extends('templates.main')

@section('title', 'Outstanding PO')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <h6 class="main-content-label mb-3">Welcome to AAIJ Purchasing Portal - @yield('title') {{date('Y')}}</h6>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <table>
                            <tr>
                                <td class="wd-30p">
                                    <span class="input-group-text spanbox exinput-custom" id="basic-addon1">PO Due Date : </span>
                                </td>
                                <td class="wd-35p">
                                    <select class="form-control form-control" id="bulan" style="color:black">
                                        <option value="" selected>Choose Month</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </td>
                                <td class="wd-32p">
                                    <select class="form-control form-control" id="tahun" style="color:black">
                                        <option value="" selected>Choose Year</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                <div class="row my-1">
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
                        <button type="button" class="btn btn-info btn-block mb-2" id="search" onclick="search()"><i class="fas fa-search"></i> <b>Search</b></button>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
                        <button type="button" class="btn btn-danger btn-block mb-2" id="search" onclick="reset()"><i class="fas fa-undo"></i> <b>Reset</b></button>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive mb-4 mt-3 secondtable">
                                <table class="table table-striped table-bordered text-center text-nowrap w-100" id="tablenya">
                                    <thead>
                                        <tr>
                                            <th scope="col">PO Number</th>
                                            <th scope="col">Vendor Code</th>
                                            <th scope="col">Vendor Name</th>
                                            <th scope="col">PO Due Date (dd/mm/yyyy)</th>
                                            <th scope="col">Lead Time (Days)</th>
                                            <th scope="col">PO Currency</th>
                                            <th scope="col">Task</th>
                                            <th scope="col">Print PO</th>
                                        </tr>
                                    </thead>
                                    <tbody id="bodynya">
                                        {!! $tablenya !!}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
