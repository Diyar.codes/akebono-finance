@extends('templates.main')

@section('title', 'Detail View Outstanding PO')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body minstylebody">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div>
                    <h4 class="font-weight-normal no-surat">Purchase Order Detail For : <span><b>{{ $detail_judul }}</b></span></h4>
                </div>
                <hr>
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="tablenya" width="100%">
                        <thead>
                            <tr>
                                <th>Line</th>
                                <th>Item Number</th>
                                <th>Description</th>
                                <th>Type</th>
                                <th>UM </th>					
                                <th>Qty PO </th>
                                <th>Qty Received </th>
                                <th>Qty Outstanding</th>
                                <th>Progress Bar</th>
                            </tr>
                        </thead>
                        <tbody>
                            {!! $tablenya !!}
                        </tbody>
                    </table>
                </div>
                <div class="row my-1">
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
                        <a href="/view-outstanding-po" class="btn btn-danger btn-block mb-2" ><i class="fas fa-undo"></i> <b>Back</b></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
