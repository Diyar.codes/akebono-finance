@extends('templates.main')

@section('title', 'Delivery Order (DN)')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div>
                        <h6 class="main-content-label mb-3">Edit Packing Slip</h6>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-2">
                            <div class="input-group">
                                Delivery Date
                            </div>
                        </div>
                        <div class="col-md-1 mb-2">
                            <select id="select_month" class="form-control">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </div>
                        <div class="col-md-2 mb-2">
                            <div class="input-group">
                                <select id="select_year" class="form-control">
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-lg-2">
                            Purchase Order Number
                        </div>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" id="dp input-search-date">
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-lg-2">
                            <div class="input-group mb-3">
                                <button type="button" class="btn btn-success btn-block"><i class="fas fa-search"></i>
                                    Search</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive mb-2">
                                <table class="table table-striped table-bordered text-center w-100" id="main-table">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">PO Number</th>
                                            <th scope="col">PO Date</th>
                                            <th scope="col">Print</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tbodymain">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
