@extends('templates.main')

@section('title', 'Detail Transaksi')
@section('styles')
    <style>
        table.dataTable thead th, table.dataTable thead td {
            border: 1px solid #fff;
        }   
    </style>
@endsection
@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <h6 class="main-content-label mb-3">@yield('title')</h6>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 box">
                                <div class="input-group mb-lg-3">
                                    <span class="input-group-text spanbox exinput-custom mr-3">Transaction ID</span>
                                    <p class="mt-3">{{ $tr_id }}</p>
                                    <input type="hidden" name="tr_id_enc" id="tr_id_enc" value="{{ base64_encode($tr_id) }}">
                                </div>
                            </div>
                            <div class="col-lg-4 box">
                                <div class="input-group mb-lg-3">
                                    <span class="input-group-text spanbox exinput-custom mr-3">Delivery Date</span>
                                    <input type="date" style="color:black" class="form-control" name="tgl" id="tgl" value="<?= date('Y-m-d') ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-lg-n4">
                            <div class="col-lg-4 box">
                                <div class="input-group mb-3">
                                    <span class="input-group-text spanbox exinput-custom mr-3">Purchase Order</span>
                                    <p class="mt-3">{{ $tampung }}</p>
                                    <input type="hidden" name="nomor_po" id="nomor_po" value="{{ base64_encode($tampung) }}">
                                </div>
                            </div>
                            <div class="col-lg-4 box">
                                <div class="input-group mb-3 mt-lg-2">
                                    <span class="input-group-text spanbox exinput-custom mr-3">Packing Slip</span>
                                    <input type="text" style="color:black" class="form-control form-control-sm" name="packingslip" id="packingslip">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-lg-n4 mt-3">
                            <div class="col-lg-3 box">
                                <div class="input-group mb-3">
                                    <span class="input-group-text spanbox exinput-custom mr-3">Cycle</span>
                                    <select class="form-control form-control-sm mr-1" style="color:black" id="cycle">
                                        <option value="" selected>Cycle</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="EXTRA">EXTRA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-3 col-12">
                                <p><font color="red"><b>Purchase Order List</b></font></p>
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive secondtable">
                            <table class="table table-bordered text-center" id="tabelnya" width="100%">
                            <thead>
                                <tr>
                                    <th>Line</th>
                                    <th>Item Number</th>
                                    <th>UM</th>
                                    <th>Deskripsi</th>
                                    <th>Type</th>
                                    <th>Qty PO</th>
                                    <th>Qty<br>Received</th>
                                    <th>Qty<br>Outstanding sj</th>
                                    <th>LPB<br>Pending</th>
                                    <th>Qty<br>Open</th>
                                    <th>Qty<br>To Ship</th>
                                    <th>UM</th>
                                    <th>PO<br>Kurang</th>
                                    <th>Check<br>Sheet</th>
                                </tr>
                            </thead>
                            <tbody id="result">
                                
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row my-4">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-success btn-block mb-2" onclick="save()"><i class="fas fa-save"></i> Save</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <a href="/delivery-order-po" class="btn btn-danger btn-block mb-2"><i class="fas fa-chevron-circle-left"></i> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
