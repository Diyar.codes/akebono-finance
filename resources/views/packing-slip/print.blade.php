@extends('templates.main')

@section('title', 'Print Packing Slip Barcode')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div>
                        <h6 class="main-content-label mb-3">@yield('title')</h6>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">From</span>
                                <input type="text" class="form-control" placeholder="" id="input-from" autocomplete="off">
                                <button class="btn btn-info" onclick="search()">Search</button>
                            </div>
                        </div>
                        <div class="col-lg-4 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">To</span>
                                <input type="text" class="form-control" placeholder="" id="input-to" autocomplete="off">
                                <button class="btn btn-info" onclick="search()">Search</button>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-1">
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
                            <button type="button" class="btn btn-success btn-block mb-2" onclick="cetak()"><i
                                    class="fas fa-print"></i> Cetak</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h6 class="main-content-label mb-3">Transaction Lookup</h6>
                        </div>
                        <div class="col-md-12">
                        <div class="table-responsive mb-4 mt-3 secondtable">
                            <table class="table table-striped table-bordered text-center text-nowrap w-100" id="tablenya"> 
                                <thead>
                                        <tr>
                                            <th scope="col">Transaction ID</th>
                                            <th scope="col">Purchase Order</th>
                                            <th scope="col">Packing Slip</th>
                                            <th scope="col">Delivery Date</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodymain">
                                        @foreach($tampil as $t)
                                        <?php $url = '<a href="/print-ps-supplier/'.base64_encode($t->portal_tr_id).'" target="_blank">'.$t->portal_tr_id.'</a>'; ?>
                                        <tr>
                                            <td><?= $url ?></td>
                                            <td>{{ $t->portal_po_nbr }}</td>
                                            <td>{{ $t->portal_ps_nbr }}</td>
                                            <td>{{ date('d-F-Y',strtotime($t->portal_dlv_date)) }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
