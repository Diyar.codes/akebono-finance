@extends('templates.main')

@section('title', 'View Packing Slip')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="row">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-lg-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">PO Number</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" name="item" id="item">
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-lg-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">Transaction ID</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" name="tr_id" id="tr_id">
                        </div>
                    </div>
                </div>
                <p></p> <p></p>
                <div class="row mt-lg-n4">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3 mt-lg-2"">
                            <span class="input-group-text spanbox exinput-custom mr-3">Packing Slip</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" name="packingslip" id="packingslip">
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3 mt-lg-2">
                            <span class="input-group-text spanbox exinput-custom mr-3">Delivery Date</span>
                            <input type="date" style="color:black" class="form-control form-control-sm" name="dlv_date" id="dlv_date">
                        </div>
                    </div>
                </div>
                <div class="row my-4">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-success btn-block mb-2" onclick="search()"><i class="fas fa-save"></i> Search</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                    <button type="submit" class="btn btn-danger btn-block mb-2" onclick="reset()"><i class="fas fa-undo"></i> Reset</button>
                    </div>
                </div>
                <div class="table-responsive mb-2 secondtable">
                        <table class="table table-bordered text-center" id="tabelnya" width="100%">
                        <thead>
                            <tr>
                                <th class="wd-20p">Transaction ID</th>
                                <th class="wd-10p">PO Number</th>
                                <th class="wd-10p">Packing Slip</th>
                                <th class="wd-10p">Delivery Date</th>
                                <th class="wd-10p">Supplier</th>
                                <th class="wd-10p">Supplier Name</th>
                                <th class="wd-10p">Cycle</th>
                                <th class="wd-10p">Time Delivery</th>
                                <th class="wd-10p">View Detail</th>
                            </tr>
                        </thead>
                        <tbody id="bodynya">
                        {!! $tablenya !!}
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
