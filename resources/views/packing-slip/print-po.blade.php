@extends('templates.main')

@section('title', 'List Of Purchase Order Print')

@section('body')
        <!-- Row -->
        <div class="row row-sm my-md-2 mt-1">
            <div class="col-sm-12 col-md-12 col-lg-3 col-xl-4">
                <div class="card custom-card">
                    <div class="main-content-app pt-0">
                        <div class="main-content-left main-content-left-chat">
                            <div class="card-body">
                                <h2 class="main-content-title tx-24 mg-b-5">List Of Purchase Order Print</h2>
                                <br>
                                <h6 class="main-content-label mb-1">PO Order Date</h6>
                                <div class="input-group">
                                    <select name="bulan" id="bulan" class="form-control form-control-sm" style="color:black">
                                        <option value="">Choose Month</option>
                                            @for($i = 1; $i <= 12 ; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                    </select>
                                    <select name="tahun" id="tahun" class="form-control form-control-sm" style="color:black">
                                        <option value="">Choose Year</option>
                                        @for($t = date('Y')-4; $t <= date('Y') ; $t++)
                                        <option value="{{ $t }}">{{ $t }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <br>
                                <h6 class="main-content-label mb-1">PO Number</h6>
                                    <div class="input-group">
                                        <input type="text" style="color:black" class="form-control" name="po" id="po" placeholder="Search ...">
                                    </div>
                                    <br>
                                    <div class="input-group">
                                    <a href="#" class="btn btn-info" onclick="print_po()";>Search</a>
                                    <a href="#" class="btn btn-danger" onclick="reset()";>Reset</a>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-9 col-xl-8">
                <div class="card custom-card">
                    <div class="main-content-app pt-0">
                        <div class="main-content-body main-content-body-chat">
                            <!-- Row -->
						<div class="row row-sm">
							<div class="col-lg-12">
								<div class="card custom-card">
									<div class="card-body">
										<div class="table-responsive secondtable">
											<table class="table text-nowrap text-md-nowrap table-bordered text-center mg-b-0" id="tablenya">
												<thead>
													<tr>
														<th>No</th>
														<th>PO Number</th>
														<th>PO Date</th>
														<th>Print</th>
													</tr>
												</thead>
												<tbody id="result">
                                                    @foreach($results as $rs)
                                                    @php 
                                                        $no     = 1; 
                                                        $bln    = date('n',strtotime($rs->po_app2_effdate));
                                                        $tahun  = date('Y',strtotime($rs->po_app2_effdate));
                                                        $periode= $bln."".$tahun;
                                                        $url    = 'http://purchasing.akebono-astra.co.id/po/'.$periode.'/'.$rs->po_nbr.'-'.$rs->po_revisi.'.pdf';
                                                    @endphp
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ $rs->po_nbr }}</td>
                                                        <td>{{ date('d-F-Y',strtotime($rs->po_effdate)) }}</td>
                                                        <td><a href="<?= $url ?>" target="_blank" class="btn btn-info" title="Print"><i class="fas fa-print"></i></a></td>
                                                    </tr>
                                                    @endforeach
												</tbody>
                                
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- End Row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
@endsection