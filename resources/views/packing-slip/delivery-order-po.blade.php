@extends('templates.main')

@section('title', 'Delivery Order')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="row mb-4">
                    <table>
                        <tr>
                            <td class="wd-35p">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Periode Order Date : </span>
                            </td>
                            <td class="wd-32p">
                                <select class="form-control form-control" id="filter_moon" style="color:black">
                                    <option value="" selected>Choose Month</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </td>
                            <td class="wd-33p">
                                <select class="form-control form-control" id="filter_year" style="color:black">
                                    <option value="" selected>Choose Year</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                    <option value="2026">2026</option>
                                    <option value="2027">2027</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row my-1">
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
                        <button type="button" class="btn btn-info btn-block mb-2" id="search" onclick="search()"><i class="fas fa-search"></i> <b>Search</b></button>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
                        <button type="button" class="btn btn-danger btn-block mb-2" id="search" onclick="reset()"><i class="fas fa-undo"></i> <b>Reset</b></button>
                    </div>
                </div>
                <div class="table-responsive mb-4 mt-3 secondtable">
                        <table class="table table-bordered text-center" id="tablenya" width="100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">PO Number</th>
                                <th scope="col">PO Order Date</th>
                                <th scope="col">PO Status</th>
                                <th scope="col">PO Approve</th>
                                <th scope="col">Task</th>
                            </tr>
                        </thead>
                        <tbody id="tblsecond">
                            {!! $tablenya !!}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
