@extends('templates.main')

@section('title', 'Edit Packing Slip')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="row">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-lg-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">Transaction ID</span>
                            <p class="mt-3">{{ $atas->portal_tr_id }}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-lg-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">Delivery Date</span>
                            <!-- <p class="mt-3">{{ date('d-F-Y',strtotime($atas->portal_dlv_date)) }}</p> -->
                            <input type="date" style="color:black" class="form-control" id="tgl" value="{{ $atas->portal_dlv_date }}">
                        </div>
                    </div>
                </div>
                <div class="row mt-lg-n4">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">Purchase Order</span>
                            <p class="mt-3">{{ $atas->portal_po_nbr }}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3 mt-lg-2">
                            <span class="input-group-text spanbox exinput-custom mr-3">Packing Slip</span>
                            <input type="text" style="color:black" class="form-control" id="ps" value="{{ $atas->portal_ps_nbr }}">
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-3 col-12">
                        <p class="text-danger">Purchose Order List</p>
                    </div>
                </div>
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="tabelnya" width="100%">
                        <thead >
                            <tr>
                                <th>Line</th>
                                <th>Item Number</th>
                                <th>UM</th>
                                <th>Deskripsi</th>
                                <th>Type</th>
                                <th>Qty PO</th>
                                <th>Qty<br>Received</th>
                                <th>Qty<br>Outstanding<br>sj</th>
                                <th>Qty DN</th>
                                <th>Sisa DN</th>
                                <th>Qty To Ship</th>
                                <th>UM</th>
                                <th>Lot No</th>
                                <th>PO<br>Kurang</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {!! $tablenya !!}
                        </tbody>
                    </table>
                </div>
                <div class="row my-4">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-success btn-block mb-2" onclick="save()"><i class="fas fa-save"></i> Save</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <a href="/edit-delivery-order-dn" class="btn btn-danger btn-block mb-2"><i class="fas fa-chevron-circle-left"></i> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
