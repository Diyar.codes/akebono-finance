@extends('templates.main')

@section('title', 'Packing Slip Barcode')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    {{-- <h6 class="main-content-label mb-3">@yield('title')</h6> --}}
                    <h6 class="main-content-label mb-3">View DS - FEXQMS</h6>
                </div>
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center w-100" id="main-table">
                        <thead>
                            <tr>
                                <th scope="col">Transaction ID</th>
                                <th scope="col">PO Number</th>
                                <th scope="col">Packing Slip</th>
                                <th scope="col">Delivery Date</th>
                                <th scope="col">Supplier</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Status Cek</th>
                                <th scope="col">Print</th>
                                <th scope="col">Inspection AAIJ</th>
                            </tr>
                        </thead>
                        <tbody class="tbodymain">
                            @foreach ($packingslipbarcode as $row)
                            @php
                                if($row->app_dn_qc =="OK" or $row->app_dn_qc =="True"){
                                    $cek = "Sudah di Cek";
                                    $print = "Print";
                                    $view = "View";
                                    $clr = "#55efc4";
                                } 
                                if($row->printsj_status =="1"){
                                    $cek = "Sudah di Print";
                                    $print = "Sudah Print";
                                    $view = "View";
                                    $clr = "#0abde3";
                                }
                            @endphp
                                <tr>
                                    <td>{{$row->portal_tr_id}}</td>
                                    <td>{{$row->portal_po_nbr}}</td>
                                    <td>{{$row->portal_ps_nbr}}</td>
                                    <td>{{$row->portal_dlv_date}}</td>
                                    <td>{{$row->kode_supplier}}</td>
                                    @php
                                        $datasuppliername = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr',$row->kode_supplier)->first();
                                    @endphp
                                    <td>{{$datasuppliername->ct_ad_name}}</td>
                                    <td>{{$cek}}</td>
                                    <td>
                                        @if (auth()->user()->privilage == 'QC')
                                        <a href="{{route('view-detail-barcode',base64_encode($row->portal_tr_id))}}">View Detail</a>
                                        @else
                                        <a href="{{route('print-barcode',base64_encode($row->portal_tr_id))}}" target="_blank">{{$print}}</a>
                                        @endif
                                    </td>
                                    <td>
                                        @if (auth()->user()->privilage == 'QC')
                                        <a href="{{route('view-detail-barcode',base64_encode($row->portal_tr_id))}}">View Detail</a>
                                        @else
                                        <a href="{{route('packing-slip-barcode-view',base64_encode($row->portal_tr_id))}}">{{$view}}</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
