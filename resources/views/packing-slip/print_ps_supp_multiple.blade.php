<script language="javascript">
    window.print();
    </script>
<STYLE TYPE="text/css">
	body
	{
		margin:0px;
		padding:0px;
	}
	table
	{
		margin:5px;
		padding:0px;
		border-collapse:collapse;
	}
	td
	{
		padding:0px;
	}
	.tdHeadersmall
	{
		font-family:sans-serif;
		font-size:10px;
		color: #576574;
		text-align:left;
		vertical-align:middle;
		padding-left:5px;
	}
	.tdContentHeader
	{
		font-weight:bold;
		font-size:15px;
		text-align:left;
		vertical-align:middle;
		color:#222f3e;
		font-family:sans-serif;
		padding-left:5px;
		
	}
	.tdQR
	{
		border-bottom:thin #cccccc solid;
		text-align:right;
		vertical-align:middle;
	}
	.tdHeader
	{
		text-align:left;
		font-family:sans-serif;
		font-size:18px;
		background-color: black;
		color: white;
		padding:5px 5px 5px 5px;
	}
	.tdHeader2
	{
		text-align:center;
		font-family:sans-serif;
		font-size:12px;
		color: #576574;
		padding:5px 5px 5px 5px;
		border-bottom:thin #cccccc solid;
	}
	#divSupp
	{
		font-size:12px;
		font-weight:bold;
	}
	.tdHeader3
	{
		text-align:left;
		font-family:sans-serif;
		font-size:12px;
		color: #576574;
		width:13%;
	}
	.tdContent3
	{
		text-align:left;
		font-family:sans-serif;
		font-size:12px;
		color: #576574;
		font-weight:bold;
		
		
	}
	#divDate
	{
		font-size:10px;
		font-family:sans-serif;
		margin-right:6px;
		color: #576574;
	}
	.tdLine
	{
		font-size:25px;
		font-family:sans-serif;
		font-weight:bold;
		
		padding:0px 3px 0px 5px;
		border-bottom:thin #cccccc solid;
	}
	.tdType
	{
		font-size:11px;
		font-family:sans-serif;
		font-weight:bold;
		padding:0px;
		vertical-align:bottom;
		background-color:transparent;
	}
	.tdItemData
	{
		border-bottom:thin #cccccc solid;
	}
	.tdItem
	{
		font-size:8px;
		font-family:sans-serif;
		padding:0px;
		vertical-align:top;
		width:76px;
		background-color:transparent;
	}
	.tdQty
	{
		font-size:13px;
		font-family:sans-serif;
		text-align:right;
		border-bottom:thin #cccccc solid;
		border-right:thin #cccccc solid;
		padding-right:5px;
	}
   P.breakhere {page-break-before: always}
</STYLE> 
@foreach ($master as $item)
<?php $isi_master = DB::table('portald_det')->where('portald_tr_id',$item->portal_tr_id)->get(); ?>
<table>
	<tr>
		<td class='tdHeader' colspan='2' style='border-left:thin #576574 solid;border-right:thin #576574 solid;border-top:thin #576574 solid;border-bottom:thin #576574 solid;'>Delivery Slip</td>
		    <td rowspan='13' style='padding:0px;vertical-align:top;max-width:400px;border-bottom:thin #576574 solid;border-right:thin #576574 solid;border-top:thin #576574 solid;border-bottom:thin #576574 solid;'>
			    <table style='margin:0px;'>
                    @foreach($isi_master as $im)
				    <tr>
                        <td class='tdLine'>{{ $im->portald_line }}</td>
                        <td class='tdItemData'>
                            <div class='tdType'>R60</div>
                            <div class='tdItem'>{{ $im->portald_part }}</div>
                        </td>
                        <td class='tdQty'>{{ $im->portald_qty_ship }}</td>
                    </tr>
                    @endforeach
			    </table>
		    </td>
	</tr>
	<tr>
		<td colspan='2' style='height:5px;border-left:thin #576574 solid;border-right:thin #576574 solid;'></td>
	</tr>
    <?php $packingslipbarcode = DB::table('portal_mstr')->where('portal_tr_id' , $item->portal_tr_id)->first(); ?>
	<tr>
		<td class='tdHeadersmall' style='border-left:thin #576574 solid;'>Purchase Order</td>
		<td class='tdQR' rowspan='4'  style='border-right:thin #576574 solid;padding-right:5px;'>
			  <div id='divDate'>{{ date('d-m-Y',strtotime($packingslipbarcode->portal_dlv_date)) }}</div>{!! QrCode::size(85)->generate($packingslipbarcode->portal_tr_id); !!}</td>
	</tr>
	<tr>
		<td class='tdContentHeader' style='padding-bottom:5px;border-left:thin #576574 solid;'>{{$packingslipbarcode->portal_po_nbr}}</td>
	</tr>
	<tr>
		<td class='tdHeadersmall'  style='border-left:thin #576574 solid;'>Transaction No</td>
	</tr>
	<tr>
		<td class='tdContentHeader' style='padding-bottom:5px;border-bottom:thin #cccccc solid;border-left:thin #576574 solid;'>{{ $packingslipbarcode->portal_tr_id }}</td>
	</tr>
	<tr>
        @php
            $datasuppliername = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr',$packingslipbarcode->portal_vend)->first();
        @endphp
		<td class='tdHeader2' colspan='2' style='border-left:thin #576574 solid;border-right:thin #576574 solid; word-wrap: break-word;width:180px;' ><div id='divSupp'>{{$datasuppliername->ct_vd_addr}}</div>
            {{$datasuppliername->ct_ad_name}}
        </td>
	</tr>
	<tr>
		<td colspan='2' style='height:5px;border-left:thin #576574 solid;border-right:thin #576574 solid;'></td>
	</tr>
	<tr>
		<td class='tdHeadersmall'  style='border-left:thin #576574 solid;border-right:thin #576574 solid;' colspan='2'>Packing Slip</td>
	</tr>
	<tr>
		<td class='tdContentHeader' style='padding-bottom:5px;border-left:thin #576574 solid;border-right:thin #576574 solid;' colspan='2'>{{ $packingslipbarcode->portal_ps_nbr }}</td>
	</tr>
	<tr>
		<td class='tdHeadersmall'  style='border-left:thin #576574 solid;border-right:thin #576574 solid;' colspan='2'>Delivery Note</td>
	</tr>
	<tr>
		<td class='tdContentHeader' style='padding-bottom:5px;border-right:thin #576574 solid;border-left:thin #576574 solid;border-bottom:thin #576574 solid;' colspan='2'><span class="text-uppercase" style="text-transform: uppercase"></span></td>
	</tr>
	<tr>
		<td class='tdHeader' colspan='2' style='border-left:thin #576574 solid;border-right:thin #576574 solid;border-top:thin #576574 solid;border-bottom:thin #576574 solid;'>&nbsp;</td>
	</tr>
</table>
@endforeach
<br>
