@extends('templates.main')

@section('title', 'Detail View Packing Slip')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <table class="table table-striped table-bordered text-center text-nowrap w-100" id="uwu" width="100%">
                    <tr style="background-color:#0066CC">
                        <td><font color=white>Transaction ID</font></td>
                        <td><font color=white>Packing Slip</font></td>
                        <td><font color=white>Purchase Order</font></td>
                    </tr>
                    <tr>
                        <td>{{ $atas->portal_tr_id }}</td>
                        <td>{{ $atas->portal_ps_nbr }}</td>
                        <td>{{ $atas->portal_po_nbr }}</td>
                    </tr>
                </table>
                <br>
                <div class="table-responsive mb-2">
                        <table class="table table-bordered text-center" id="list_detail" width="100%">
                        <thead>
                            <tr>
                                <th class="wd-15p">Item Number</th>
                                <th class="wd-25p">Deskripsi</th>
                                <th class="wd-10p">Qty Packing Slip</th>
                                <th class="wd-10p">Qty Confirm</th>
                                <th class="wd-20p">Delivery Date</th>
                                <th class="wd-20p">Confirm Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        {!! $tablenya !!}
                        </tbody>
                    </table>
                </div>
                <div class="row my-4">
                    <div class="col-lg-2 col-sm-4">
                        <a href="/view-packing-slip" class="btn btn-danger btn-block mb-2"><i class="fas fa-chevron-circle-left"></i> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
