@extends('templates.main')

@section('title', 'View - Packing Slip Barcode')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    {{-- <h6 class="main-content-label mb-3">@yield('title')</h6> --}}
                    <h6 class="main-content-label mb-3">Delivery Note</h6>
                </div>
                <div class="row mb-2">
                    <div class="col-md-2">
                        Delivery Note
                    </div>
                    <div class="col-md-3 text-uppercase">
                        {{$data_dn->portald_no_dn}}
                    </div>
                    <div class="col-md-2">
                        Delivery Date
                    </div>
                    <div class="col-md-3">
                        {{$data_dn->portald_dlv_date}}
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-2">
                        Purchase Order
                    </div>
                    <div class="col-md-3">
                        {{$data_dn->portald_po_nbr}}
                    </div>
                    <div class="col-md-2">
                        Packing Slip
                    </div>
                    <div class="col-md-3">
                        {{$data_dn->portal_ps_nbr}}
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-2">
                        Date
                    </div>
                    <div class="col-md-3">
                        <input type="date" name="date" id="date" value="{{date('Y-m-d')}}" class="form-control">
                    </div>
                    <div class="col-md-2">
                        Time
                    </div>
                    <div class="col-md-3">
                        @php
                            date_default_timezone_set('Asia/Jakarta');
                        @endphp
                        <input type="time" name="time" id="time" value="{{date('H:i')}}" class="form-control">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-12">
                        <div class="table-responsive mb-2">
                            <table class="table table-striped table-bordered text-center w-100" id="main-table">
                                <thead>
                                    <tr>
                                        <th scope="col" rowspan="2" style="vertical-align: middle;">Line</th>
                                        <th scope="col" rowspan="2" style="vertical-align: middle;">Item Number</th>
                                        <th scope="col" rowspan="2" style="vertical-align: middle;">Description</th>
                                        <th scope="col" rowspan="2" style="vertical-align: middle;">Old Description</th>
                                        <th scope="col" rowspan="2" style="vertical-align: middle;">UM</th>
                                        <th scope="col" rowspan="2" style="vertical-align: middle;">Qty To Ship</th>
                                        <th scope="col" colspan="4">Supplier</th>
                                        <th scope="col" colspan="2">AAIJ</th>
                                    </tr>
                                    <tr>
                                        <th scope="col">Supplier Inspection</th>
                                        <th scope="col">Supplier judgment</th>
                                        <th scope="col">Supplier Indirect ? <input type='checkbox' name='checkSemua' id='checkSemua' onclick='checkA();'></th>
                                        <th scope="col">Supplier Note</th>
                                        <th scope="col">AAIJ Inspection</th>
                                        <th scope="col">AAIJ Judgment</th>
                                    </tr>
                                </thead>
                                <tbody class="tbodymain">
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($detail as $row)                                        
                                    @php
                                        //untuk cek data deskripsi Item master
                                        $detail1 = pt_mstr($row->portald_part);

                                        getPODetail($row->portald_po_nbr);
                                        $descPO = DB::table('SOAP_po_detail')
                                                    ->select('item_deskripsi')
                                                    ->where('po_domain','AAIJ')
                                                    ->where('no_po',$row->portald_po_nbr)
                                                    ->where('line',$row->portald_line)
                                                    ->where('item_number',$row->portald_part)
                                                    ->first();

                                        //cari deskripsi LAMA
                                        $deskripsi_q = DB::table('pub_xxitem_mapping')
                                                        ->select('xxold_part','xxold_desc1','xxold_desc2')
                                                        ->where('xxnew_part',$row->portald_part)
                                                        ->first();
                                        $insp_direct = "Direct";
                                        if ($row->portald_app_insp_note != "") {
                                            $insp_direct = "Indirect";
                                        }

                                        //untuk menampilkan data qty yang di barcode_qc
                                        // $tampil_data_receive = DB::table('elpb_dtl')
                                        //                         ->where('trans_id', $row->portald_tr_id)
                                        //                         ->where('item_number', $row->portald_part)
                                        //                         ->where('line', $row->portald_line)->first();
                                    @endphp

                                    <tr>
                                        <input type='hidden' name='txt_idsupp' size='20' value='{{$row->portal_vend}}' readonly>
                                        <input type='hidden' name='txt_trans{{$no}}' size='20' value='' readonly>
                                        <input type='hidden' name='txt_line{{$no}}' size='20' value='{{$row->portald_line}}' readonly>
                                        <input type='hidden' name='txt_item{{$no}}' size='20' value='{{$row->portald_part}}' readonly>
                                        <td>{{$row->portald_line}}</td>
                                        <td nowrap='nowrap' align='center'>{{$row->portald_part}}</td>
                                        @if (is_null($detail1['deskripsi1']))
                                        <input type='hidden' name='txt_desc{{$no}}' size='20' value='{{$descPO->item_deskripsi}}' readonly>
                                            <td nowrap='nowrap'>{{$descPO->item_deskripsi}}</td>
                                            <td nowrap='nowrap'>{{(empty($deskripsi_q->xxold_desc1) ? '-' : $deskripsi_q->xxold_desc1). " " .(empty($deskripsi_q->xxold_desc2) ? '-' : $deskripsi_q->xxold_desc2)}}</td>
                                        @else    
                                            <input type='hidden' name='txt_desc{{$no}}' size='20' value='{{$detail1['deskripsi1']}}' readonly>
                                            <td nowrap='nowrap'>{{$detail1['deskripsi1']}}</td>
                                            <td nowrap='nowrap'>{{(empty($deskripsi_q->xxold_desc1) ? '-' : $deskripsi_q->xxold_desc1). " " .(empty($deskripsi_q->xxold_desc2) ? '-' : $deskripsi_q->xxold_desc2)}}</td>
                                        @endif
                                        {{-- <input type='text' style='text-align:right;' name='txt_qty_confirm{{$no}}' id='txt_qty_confirm{{$no}}' size='20' value='{{$tampil_data_receive->qty_received}}' readonly> --}}
                                        <td nowrap='nowrap' align='center'>{{$row->portald_um}}</td>
                                        <td nowrap='nowrap' align='center'>
                                            <input  style='text-align:right;' type='text' name='txt_qty_qc" . $hitung . "' id='txt_qty_qc{{$no}}' value='{{number_format($row->portald_qty_ship)}}' size='10'  readonly/>
                                            </td>
                                        <td nowrap='nowrap' align='center'><a href="http://sqms.akebono-astra.co.id/QualityControl/DataEntry.aspx?inspid={{$row->portald_inpection_id}}">{{$row->portald_inpection_id}}</a></td>
                                        @if (auth()->user()->previllage == 'QC')
                                            <td nowrap="" align='center'>{{$row->portald_insp_judg}}</td>

                                            <td style='text-align:center; width:300px;'>
                                                <input type='checkbox' name='checkqc<?php echo $no ?>' id='checkqc{{$no}}' onclick='check({{$no}});' {{$insp_direct != "" ? 'checked' : ''}}>
                                            </td>
                                            <td nowrap='nowrap' align='center'>
                                                <select name="finalJudge{{$no}}" id="finalJudge{{$no}}" class='form-control'>
                                                    <option value=""></option>
                                                    <option value="Sampling" {{$row->portald_app_insp_note == "Sampling" ? 'selected' : ''}}>Sampling</option>
                                                    <option value="NG" {{$row->portald_app_insp_note == "NG" ? 'selected' : ''}}>NG</option>
                                                </select>
                                            </td>
                                            <td nowrap='nowrap' align='center' style='text-align:center;color:blue'><a href="">{{$row->portald_rcv_id}}</a></td>
                                            <td nowrap='nowrap' align='center'>{{ $row->portald_rcv_judge }}</td>
                                            {{-- <a href='http://sqms.akebono-astra.co.id/QualityControl/DataEntry.aspx?inspid=" . $portald_rcv_id . "&m=1' target='_new'><u>" . $portald_rcv_id . "</u></a> --}}
                                        @else
                                            <td nowrap='' align='center'>{{$row->portald_insp_judg}}</td>
                                            <td nowrap='nowrap' align='center'>{{$insp_direct}}</td>
                                            <td nowrap='nowrap' align='center'>{{$row->portald_app_insp_note}}</td>
                                            <td nowrap='nowrap' align='center' style='text-align:center;color:blue'>{{$row->portald_rcv_id}}</td>
                                            <td nowrap='nowrap' align='center'>{{$row->portald_rcv_judge}}</td>
                                        @endif
                                        
                                    </tr>
                                    @php
                                        $no++;
                                    @endphp
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- @if (auth()->user()->previllage == 'QC')
                    <div class="row mb-1">
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
                            <button type="button" class="btn btn-primary btn-block mb-2" id="search"><i
                                    class="fas fa-print"></i> Save</button>
                        </div>
                    </div>
                @else
                    <div class="row mb-1">
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
                            <button type="button" class="btn btn-success btn-block mb-2" id="search"><i
                                    class="fas fa-print"></i> Print</button>
                        </div>
                    </div>
                @endif --}}
            </div>
        </div>
    </div>
</div>
@endsection
