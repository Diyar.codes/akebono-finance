@extends('templates.main')

@section('title', 'Detail Transaksi')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <div class="row">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-lg-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">Transaction ID</span>
                            <p class="mt-3">{{ $tr_id }}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-lg-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">Purchase Order</span>
                            <p class="mt-3">{{ $po }}</p>
                        </div>
                    </div>
                </div>
                <div class="row mt-lg-n4">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">Item Number</span>
                            <p class="mt-3">{{ $item_number }}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3 mt-lg-2">
                            <span class="input-group-text spanbox exinput-custom mr-3">Line</span>
                            <p class="mt-3">{{ $line }}</p>
                        </div>
                    </div>
                </div>
                <div class="table-responsive mb-4">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100">
                        <thead>
                            <tr>
                                <th scope="col">Characteristics</th>
                                <th scope="col">Standard</th>
                                <th scope="col">Lower Std</th>
                                <th scope="col">Upper Std</th>
                                <th scope="col">Uom</th>
                                <th scope="col">Equipment</th>
                                <th scope="col">Data 1</th>
                                <th scope="col">Judg</th>
                                <th scope="col">Data 2</th>
                                <th scope="col">Judg</th>
                                <th scope="col">Data 3</th>
                                <th scope="col">Judg</th>
                                <th scope="col">Data 4</th>
                                <th scope="col">Judg</th>
                                <th scope="col">Data 5</th>
                                <th scope="col">Judg</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
				<td>Appearance
					<input type='hidden' class='txtCharacteristic' name='Appearance' id='Appearance' style='width:60px!important;height:30px;' value='1'>
				</td>
				<td>Tidak ada burry, cacat, karat</td>
				<td></td>
				<td></td>
				<td></td>
				<td>VS</td>
				<td><input type='text' class='form-control' name='Appearance_Data1' id='Appearance_Data1' style='width:60px!important;height:30px;'></td>
				<td><input type='text' class='form-control' name='Appearance_Judg1' id='Appearance_Judg1' style='width:60px!important;height:30px;'></td>
				<td><input type='text' class='form-control' name='Appearance_Data2' id='Appearance_Data2' style='width:60px!important;height:30px;'></td>
				<td><input type='text' class='form-control' name='Appearance_Judg2' id='Appearance_Judg2' style='width:60px!important;height:30px;'></td>
				<td><input type='text' class='form-control' name='Appearance_Data3' id='Appearance_Data3' style='width:60px!important;height:30px;'></td>
				<td><input type='text' class='form-control' name='Appearance_Judg3' id='Appearance_Judg3' style='width:60px!important;height:30px;'></td>
				<td><input type='text' class='form-control' name='Appearance_Data4' id='Appearance_Data4' style='width:60px!important;height:30px;'></td>
				<td><input type='text' class='form-control' name='Appearance_Judg4' id='Appearance_Judg5' style='width:60px!important;height:30px;'></td>
				<td><input type='text' class='form-control' name='Appearance_Data5' id='Appearance_Data5' style='width:60px!important;height:30px;'></td>
				<td><input type='text' class='form-control' name='Appearance_Judg5' id='Appearance_Judg5' style='width:60px!important;height:30px;'></td>
			</tr>
			<tr>
				<td>Dimension
					<input type='hidden' class='txtCharacteristic' name='Dimension1' id='Dimension1' style='width:60px!important;height:30px;' value='2'>
				</td>
				<td>10<sup>0</sup> <low>-0.2</low></td>
				<td>9.8</td>
				<td>10</td>
				<td>mm</td>
				<td>VS</td>
				<td><input type='text' class='form-control' name='Dimension1_Data1' id='Dimension1_Data1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension1_Judg1' id='Dimension1_Judg1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension1_Data2' id='Dimension1_Data2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension1_Judg2' id='Dimension1_Judg2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension1_Data3' id='Dimension1_Data3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension1_Judg3' id='Dimension1_Judg3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension1_Data4' id='Dimension1_Data4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension1_Judg4' id='Dimension1_Judg4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension1_Data5' id='Dimension1_Data5' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension1_Judg5' id='Dimension1_Judg5' style='width:60px!important;height:20px;'></td>
			</tr>
			<tr>
				<td>Dimension
					<input type='hidden' class='txtCharacteristic' name='Dimension2' id='Dimension2' style='width:60px!important;height:30px;' value='3'>
				</td>
				<td>4&#177;0.5</td>
				<td>3.85</td>
				<td>4.15</td>
				<td>mm</td>
				<td>VC</td>
				<td><input type='text' class='form-control' name='Dimension2_Data1' id='Dimension2_Data1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension2_Judg1' id='Dimension2_Judg1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension2_Data2' id='Dimension2_Data2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension2_Judg2' id='Dimension2_Judg2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension2_Data3' id='Dimension2_Data3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension2_Judg3' id='Dimension2_Judg3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension2_Data4' id='Dimension2_Data4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension2_Judg4' id='Dimension2_Judg4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension2_Data5' id='Dimension2_Data5' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension2_Judg5' id='Dimension2_Judg5' style='width:60px!important;height:20px;'></td>
				
			</tr>
			<tr>
				<td>Dimension
					<input type='hidden' class='txtCharacteristic' name='Dimension3' id='Dimension3' style='width:60px!important;height:30px;' value='4'>
				</td>
				<td>31&#177;0.5</td>
				<td>30.5</td>
				<td>31.5</td>
				<td>mm</td>
				<td>VC</td>
				<td><input type='text' class='form-control' name='Dimension3_Data1' id='Dimension3_Data1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension3_Judg1' id='Dimension3_Judg1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension3_Data2' id='Dimension3_Data2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension3_Judg2' id='Dimension3_Judg2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension3_Data3' id='Dimension3_Data3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension3_Judg3' id='Dimension3_Judg3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension3_Data4' id='Dimension3_Data4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension3_Judg4' id='Dimension3_Judg4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension3_Data5' id='Dimension3_Data5' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension3_Judg5' id='Dimension3_Judg5' style='width:60px!important;height:20px;'></td>
				
			</tr>
			<tr>
				<td>Dimension
					<input type='hidden' class='txtCharacteristic' name='Dimension4' id='Dimension4' style='width:60px!important;height:30px;' value='5'>
				</td>
				<td>17<sup>+0.5</sup> <low>+0.2</low></td>
				<td>17.2</td>
				<td>17.5</td>
				<td>mm</td>
				<td>VC</td>
				<td><input type='text' class='form-control' name='Dimension4_Data1' id='Dimension4_Data1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension4_Judg1' id='Dimension4_Judg1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension4_Data2' id='Dimension4_Data2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension4_Judg2' id='Dimension4_Judg2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension4_Data3' id='Dimension4_Data3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension4_Judg3' id='Dimension4_Judg3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension4_Data4' id='Dimension4_Data4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension4_Judg4' id='Dimension4_Judg4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension4_Data5' id='Dimension4_Data5' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Dimension4_Judg5' id='Dimension4_Judg5' style='width:60px!important;height:20px;'></td>
				
			</tr>
			<tr>
				<td>Diameter
					<input type='hidden' class='txtCharacteristic' name='Diameter' id='Diameter' style='width:60px!important;height:30px;' value='6'>
				</td>
				<td>&Oslash;8<sup>0</sup> <low>-0.1</low></td>
				<td>7.9</td>
				<td>8</td>
				<td>mm</td>
				<td>VC</td>
				<td><input type='text' class='form-control' name='Diameter_Data1' id='Diameter_Data1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Diameter_Judg1' id='Diameter_Judg1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Diameter_Data2' id='Diameter_Data2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Diameter_Judg2' id='Diameter_Judg2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Diameter_Data3' id='Diameter_Data3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Diameter_Judg3' id='Diameter_Judg3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Diameter_Data4' id='Diameter_Data4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Diameter_Judg4' id='Diameter_Judg4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Diameter_Data5' id='Diameter_Data5' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Diameter_Judg5' id='Diameter_Judg5' style='width:60px!important;height:20px;'></td>
			</tr>
			<tr>
				<td>Std Thread
					<input type='hidden' class='txtCharacteristic' name='Thread' id='Thread' style='width:60px!important;height:30px;' value='7'>
				</td>
				<td>M6XP1.0</td>
				<td>16.5</td>
				<td>31.5</td>
				<td>mm</td>
				<td>Ring Gauge</td>
				<td><input type='text' class='form-control' name='Thread_Data1' id='Thread_Data1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Thread_Judg1' id='Thread_Judg1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Thread_Data2' id='Thread_Data2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Thread_Judg2' id='Thread_Judg2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Thread_Data3' id='Thread_Data3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Thread_Judg3' id='Thread_Judg3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Thread_Data4' id='Thread_Data4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Thread_Judg4' id='Thread_Judg4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Thread_Data5' id='Thread_Data5' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Thread_Judg5' id='Thread_Judg5' style='width:60px!important;height:20px;'></td>
			</tr>
			<tr>
				<td>Hardness
					<input type='hidden' class='txtCharacteristic' name='Hardness' id='Hardness' style='width:60px!important;height:30px;' value='8'>
				</td>
				<td>HRC 20 - 28</td>
				<td>20</td>
				<td>28</td>
				<td>mm</td>
				<td>Hardness Test</td>
				<td><input type='text' class='form-control' name='Hardness_Data1' id='Hardness_Data1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Hardness_Judg1' id='Hardness_Judg1' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Hardness_Data2' id='Hardness_Data2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Hardness_Judg2' id='Hardness_Judg2' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Hardness_Data3' id='Hardness_Data3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Hardness_Judg3' id='Hardness_Judg3' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Hardness_Data4' id='Hardness_Data4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Hardness_Judg4' id='Hardness_Judg4' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Hardness_Data5' id='Hardness_Data5' style='width:60px!important;height:20px;'></td>
				<td><input type='text' class='form-control' name='Hardness_Judg5' id='Hardness_Judg5' style='width:60px!important;height:20px;'></td>
			</tr>
                        </tbody>
                    </table>
                </div>
                <div class="row my-4">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-success btn-block mb-2" onclick="save()"><i class="fas fa-save"></i> Save</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <a href="{{ url()->previous() }}" class="btn btn-danger btn-block mb-2"><i class="fas fa-chevron-circle-left"></i> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
