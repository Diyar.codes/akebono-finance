@extends('templates.main')

@section('title', 'View Receipt')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <h6 class="main-content-label mb-3">@yield('title')</h6>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-lg-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">Periode</span>
                                <select class="form-control form-control-sm" id="bulan" style="color:black">
                                    <option value="" selected>Choose Month</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                                <select class="form-control form-control-sm" id="tahun" style="color:black">
                                    <option value="" selected>Choose Year</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                    <option value="2026">2026</option>
                                    <option value="2027">2027</option>
                                </select>
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-lg-3">
                            <span class="input-group-text spanbox exinput-custom mr-3">PO Number</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" name="po" id="po">
                        </div>
                    </div>
                </div>
                <p></p> <p></p>
                <div class="row mt-lg-n4">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3 mt-lg-2"">
                            <span class="input-group-text spanbox exinput-custom mr-3">Item Number</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" name="item" id="item">
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3 mt-lg-2">
                            <span class="input-group-text spanbox exinput-custom mr-3">RC</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" name="rc" id="rc">
                        </div>
                    </div>
                </div>
                <div class="row my-4">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-success btn-block mb-2" onclick="search()"><i class="fas fa-save"></i> Search</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                    <button type="submit" class="btn btn-danger btn-block mb-2" onclick="reset()"><i class="fas fa-undo"></i> Reset</button>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive mb-2 secondtable">
                                <table class="table table-bordered text-center text-nowrap" id="tabelnya" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="wd-5p">Line</th>
                                            <th class="wd-10p">PO Number</th>
                                            <th class="wd-10p">Supplier ID</th>
                                            <th class="wd-15p">Item Number</th>
                                            <th class="wd-20p">Description</th>
                                            <th class="wd-7p">Receiver</th>
                                            <th class="wd-9p">Packing Slip</th>
                                            <th class="wd-7p">Qty Receipt</th>
                                            <th class="wd-10p">Tanggal Receipt</th>
                                            <th class="wd-7p">Retur RC</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tblsecond">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
