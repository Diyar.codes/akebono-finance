@extends('templates.main')

@section('title', 'Master Cycle')

@section('body')
{{-- row --}}
<div class="row row-sm my-md-2 mt-5">
    <div class="col-lg-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <hr class="mg-b-40">
                <div class="row mb-2">
                    {{-- <div class="col-xl-4 col-lg-6 col-md-8 col-sm-8 box">
                        <div class="input-group mb-3">
                            <span clas="input-group-text exinput-custom spanbox mr-3" id="basic-addon1">Nama Supplier</span>
                            <input type="text" class="form-control form-control-sm text-dark" id="input-search" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 mb-2 box ml-2">
                        <button type="button" class="btn btn-info  btn-sm rounded-left" id="search"><i class="fas fa-search"></i> Search</button>
                    </div> --}}
                    <div class="form-group col-12">
                        <div class="row">
                            <div class="col-lg-2 col-md-3 col-4 mt-n1 mr-xl-n5">
                                <label for="itemNumber" class="col-form-label text-wrap label-responsive">Nama Supplier</label>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                                <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" id="input-search" name="input-search" readonly autocomplete="off">
                            </div>
                            <div>
                                <button type="submit" class="btn btn-sm btn-sm-responsive btn-info" data-toggle="modal" data-target="#getSupplierTarget" id="seacrhSOAPBusinesRelation"><i class="fas fa-search"></i></button>
                            </div>
                            {{-- <div class="d-md-inline-block d-none col-md-5">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-4 col-md-5 p-0 mr-1">
                                        <button type="button" class="modal-effect btn btn-sm btn-sm-responsive btn-info btn-block" id="search"><i class="fas fa-search"></i><span class="d-none d-sm-inline-block ml-1"> Search</span></button>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-5 p-0">
                                        <button type="button" class="btn btn-danger btn-block btn-sm" id="reset"><i class="fas fa-undo"></i> Reset</button>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <div class="row mt-2">
                            <div class="col-xl-2 col-lg-3 col-sm-4 col-6">
                                <button type="button" class="btn btn-info btn-block btn-sm btn-sm-responsive" id="search"><i class="fas fa-search"></i> Search</button>
                            </div>
                            <div class="col-xl-2 col-lg-3 col-sm-4 col-6">
                                <button type="button" class="btn btn-danger btn-block btn-sm btn-sm-responsive" id="reset"><i class="fas fa-undo"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form>
                    <div class="secondtable mt-1">
                        <table id="dataSecondTable" class="display" style="width: 100%">
                            <thead>
                                <tr style="background-color: #0066CC;">
                                    <th scope="col" class="text-white">Kode Supplier</th>
                                    <th scope="col" class="text-white">Nama Supplier</th>
                                    <th scope="col" class="text-white">Cycle</th>
                                    <th scope="col" class="text-white">Start Time</th>
                                    <th scope="col" class="text-white">End Time</th>
                                </tr>
                            </thead>
                            <tbody class="tblsecondd text-center">
                            </tbody>
                        </table>
                        <div class="row my-4 t-automatical" style="display: none">
                            <div class="col-lg-2 col-sm-4">
                                <button type="submit" class="btn btn-sm btn-sm-responsive btn-success btn-block mb-2" id="updateMasterCycle"><i class="fas fa-save"></i> Save</button>
                            </div>
                            <div class="col-lg-2 col-sm-4">
                                <button type="reset" class="btn btn-sm btn-sm-responsive btn-danger btn-block mb-2"><i class="fas fa-undo"></i> Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Row -->

<!-- get supplier Modal effects -->
<div class="modal" id="getSupplierTarget">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="get-supplier">
                        <thead class="thead">
                            <tr class="backgroudrowblue">
                                <th scope="col" style="color: white">Supplier Id</th>
                                <th scope="col" style="color: white">Supplier Name</th>
                                <th scope="col" style="color: white">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection
