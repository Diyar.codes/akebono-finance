@extends('layouts.main')

@section('title', 'Create Master Cycle')

@section('body')
    <div class="row">
        <div class="col-12">
            <h4>Create Master Cycle</h4>
            <div class="separator mb-5"></div>
        </div>
    </div>

    <form action="" method="POST" name="form">
        <div class="row mb-4">
            <div class="col-12 mb-4">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-xl-4 col-lg-6 col-md-8 box">
                                <div class="input-group mb-3">
                                    <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Nama Supplier</span>
                                    <input type="text" class="form-control" placeholder="" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 mb-2 box">
                                <button type="button" class="btn btn-light btn-block rounded-left">Search</button>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="table-responsive">
                                <table class="table table-striped text-center">
                                    <thead class="exthead-custom">
                                        <tr>
                                            <th><a href="#" class="btn btn-primary" id="addRow"><i class="fas fa-plus"></i></a></th>
                                            <th scope="col">Kode Supplier</th>
                                            <th scope="col">Nama Supplier</th>
                                            <th scope="col">Cycle</th>
                                            <th scope="col">Start Time</th>
                                            <th scope="col">End Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><a href="#" class="btn btn-danger remove" id="remove"><i class="fas fa-trash"></i></a></td>
                                            <td><input type="text" class="form-control cek" name="kode_supplier[]" autocomplete="off" required></td>
                                            <td><input type="text" class="form-control cek" name="nama_supplier[]" autocomplete="off"></td>
                                            <td><input type="text" class="form-control cek" name="cycle[]" autocomplete="off"></td>
                                            <td><input type="time" class="form-control cek" name="start_time[]"></td>
                                            <td><input type="time" class="form-control cek" name="end_time[]"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-2">
                                <button type="submit" class="btn btn-light btn-block mb-2" id="createMasterCycle"><i class="fas fa-feather-alt"></i> Save</button>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-light btn-block mb-2">Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
