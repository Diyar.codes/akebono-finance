<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="content-type" content="text/plain; charset=UTF-8"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
    <table border="1">
        <thead>
            <tr class="backgroudrowblue">
                <th scope="col" colspan="26" class="text-wrap align-middle text-white" style="text-align: center; vertical-align: center">LIST PROBLEM SUPPLIER</th>
            </tr>
            <tr class="backgroudrowblue">
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">No</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">QPR No</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Supplier</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Part Number</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Part Desc</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Type</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">2W/4W</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">QTY NG</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">QTY Delivery</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Invoice</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Lot</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Informasi</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Problem</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Issue Date</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Found Date</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Delivery Date</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Receiving Date</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Invoice Date</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Rank</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Occurence</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Disposition</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Point Detection</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Invoice</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Paid</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Countermeasure</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">Verifikasi</th>
            </tr>
        </thead>
        <tbody>
        @foreach($data as $da)
            <tr class="tr1">
                <td>{{ $da['0'] }}</td>
                <td>{{ $da['1'] }}</td>
                <td>{{ $da['2'] }}</td>
                <td>{{ $da['3'] }}</td>
                <td>{{ $da['4'] }}</td>
                <td>{{ $da['5'] }}</td>
                <td>{{ $da['6'] }}</td>
                <td>{{ $da['7'] }}</td>
                <td>{{ $da['8'] }}</td>
                <td>{{ $da['9'] }}</td>
                <td>{{ $da['10'] }}</td>
                <td>{{ $da['11'] }}</td>
                <td>{{ $da['12'] }}</td>
                <td>{{ $da['13'] }}</td>
                <td>{{ $da['14'] }}</td>
                <td>{{ $da['15'] }}</td>
                <td>{{ $da['16'] }}</td>
                <td>{{ $da['17'] }}</td>
                <td>{{ $da['18'] }}</td>
                <td>{{ $da['19'] }}</td>
                <td>{{ $da['20'] }}</td>
                <td>{{ $da['21'] }}</td>
                <td>{{ $da['22'] }}</td>
                <td>{{ $da['23'] }}</td>
                <td>{{ $da['24'] }}</td>
                <td>{{ $da['25'] }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
</body>
</html>
