<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="content-type" content="text/plain; charset=UTF-8"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
    <table border="1">
        <thead>
            <tr class="backgroudrowblue">
                <th scope="col" rowspan="3" class="text-wrap align-middle text-white" style="text-align: center; vertical-align: center">TANGGAL</th>
                <th scope="col" rowspan="3" class="text-wrap align-middle text-white" style="text-align: center; vertical-align: center">Part Number</th>
                <th scope="col" colspan="3" class="text-wrap align-middle text-white" style="text-align: center">JENIS TRANSAKSI</th>
            </tr>
            <tr class="backgroudrowblue">
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">PORTAL</th>
                <th scope="col" colspan="2" class="text-wrap align-middle text-white" style="text-align: center">IN</th>
            </tr>
            <tr class="backgroudrowblue">
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">QAD</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">IN</th>
                <th scope="col" class="text-wrap align-middle text-white" style="text-align: center">OUT</th>
            </tr>
        </thead>
        <tbody>
        @foreach($data as $da)
            <tr class="tr1">
                <td>{{ $da['0'] }}</td>
                <td>{{ $da['1'] }}</td>
                <td>{{ $da['2'] }}</td>
                <td>{{ $da['3'] }}</td>
                <td>{{ $da['4'] }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
</body>
</html>
