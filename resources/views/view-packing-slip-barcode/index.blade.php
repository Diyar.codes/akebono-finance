@extends('templates.main')

@section('title', 'View Packing Slip Barcode')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <h6 class="main-content-label mb-3">@yield('title')</h6>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-2">Item Number</div>
                        <div class="col-md-2"><input type="text" name="itemNumber" id="dnNumber" class="form-control">
                        </div>
                        <div class="col-md-2">Transaction ID</div>
                        <div class="col-md-2"><input type="text" name="poNumber" id="poNumber" class="form-control">
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-2">Packing Slip</div>
                        <div class="col-md-2"><input type="text" name="itemNumber" id="packingSlip" class="form-control">
                        </div>
                        <div class="col-md-2">Delivery Date</div>
                        <div class="col-md-2"><input type="date" name="poNumber" id="deliveryDate" class="form-control">
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-1">
                            <button class="btn btn-success">Search</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive mb-2">
                                <table class="table table-striped table-bordered text-center w-100" id="main-table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Transaction ID</th>
                                            <th scope="col">PO Number</th>
                                            <th scope="col">Packing Slip</th>
                                            <th scope="col">Delivery Date</th>
                                            <th scope="col">Supplier</th>
                                            <th scope="col">Supplier Name</th>
                                            <th scope="col">Cycle</th>
                                            <th scope="col">Time Delivery</th>
                                            <th scope="col">View Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>92928299</td>
                                            <td>2</td>
                                            <td>90-R7929</td>
                                            <td>PIN, GUIDE</td>
                                            <td>PC</td>
                                            <td>0.27</td>
                                            <td>45,000.0000</td>
                                            <td><a href="#">View Detail</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
