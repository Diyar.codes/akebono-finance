<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {{-- css bootstrap --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
        @page { size: 210mm 297mm landscape; }
    </style>

    <title>Print Delivery Note</title>
</head>
<body>
    <div class="container">
        <div class="row text-center">
            <div class="col-12">
                <h3><b>DELIVERY NOTE</b></h3>
            </div>
        </div>
        <table class="table border-0">
            <tbody>
                <tr>
                    <td style="border-top: none !important">Delivery Note No.</td>
                    <td style="border-top: none !important"></td>
                    <td style="border-top: none !important">Supplier</td>
                    <td style="border-top: none !important"></td>
                </tr>
                <tr>
                    <td style="border-top: none !important">Purchase Order No.</td>
                    <td style="border-top: none !important"></td>
                </tr>
                <tr>
                    <td style="border-top: none !important">Order Date</td>
                    <td style="border-top: none !important"></td>
                    <td style="border-top: none !important">Cycle</td>
                    <td style="border-top: none !important"></td>
                </tr>
                <tr>
                    <td style="border-top: none !important">Arrival Date</td>
                    <td style="border-top: none !important"></td>
                    <td style="border-top: none !important">Cycle</td>
                    <td style="border-top: none !important"></td>
                </tr>
            </tbody>
        </table>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>NO</td>
                    <td>ITEM NUMBER</td>
                    <td>PART NAME</td>
                    <td>TYPE</td>
                    <td>UM</td>
                    <td>BACK NO</td>
                    <td>UM/KANBAN</td>
                    <td>PALLET/KANBAN</td>
                    <td>ORDER KANBAN</td>
                    <td>QTY UM</td>
                    <td>ACTUAL</td>
                    <td>STATUS</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>oke</td>
                    <td>oke</td>
                    <td>oke</td>
                    <td>oke</td>
                    <td>oke</td>
                    <td>oke</td>
                    <td>oke</td>
                    <td>oke</td>
                    <td>oke</td>
                    <td>oke</td>
                    <td>oke</td>
                    <td>oke</td>
                </tr>
            </tbody>
        </table>
    </div>

    {{-- js bootstrap --}}
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script> --}}
</body>
</html>
