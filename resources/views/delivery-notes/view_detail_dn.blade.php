@extends('templates.main')

@section('title', 'View Delivery Note')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body minstylebody">
                <div>
                    <h6 class="main-content-label mb-3">Detail Transaksi</h6>
                </div>
                <div>
                    <h4 class="font-weight-normal">Packing Slip No : <span><b>{{ $ps_no }}</b></span></h4>
                </div>
                <hr>
                <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Line</th>
                                <th scope="col">Part Number</th>
                                <th scope="col">Description</th>
                                <th scope="col">UM</th>
                                <th scope="col">Qty DN</th>
                                <th scope="col">Qty Ship</th>
                            </tr>
                        </thead>
                        <tbody>
                        {!! $tablenya !!}
                        </tbody>
                    </table>
                </div>
                <div class="row"> 
                    <div class="col-md-12">
                        <a href="/dn-vs-packingslip" class="btn btn-danger">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
