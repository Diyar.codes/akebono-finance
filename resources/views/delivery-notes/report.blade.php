@extends('templates.main')

@section('title', 'Print Delivery Note')

@section('body')
        <!-- Row -->
        <div class="row row-sm my-md-2 mt-1">
            <div class="col-sm-12 col-md-12 col-lg-3 col-xl-4">
                <div class="card custom-card">
                    <div class="main-content-app pt-0">
                        <div class="main-content-left main-content-left-chat">

                            <div class="card-body">
                            <h2 class="main-content-title tx-24 mg-b-5">Print Delivery Note</h2>
                            <br>
                            <h6 class="main-content-label mb-1">DN Number</h6>
                                <div class="input-group">
                                    
                                    <input type="text" class="form-control" name="search" id="search" placeholder="Search ...">
                                    <span class="input-group-append">
                                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                        <button class="btn ripple btn-info" type="button" onclick="search_dn()";>Search</button>
                                    </span>
                                </div>
                                <br>
                                <a href="#" class="btn btn-success" onclick="cetak_dn()";>Print</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-9 col-xl-8">
                <div class="card custom-card">
                    <div class="main-content-app pt-0">
                        <div class="main-content-body main-content-body-chat">
                            <!-- Row -->
						<div class="row row-sm">
							<div class="col-lg-12">
								<div class="card custom-card">
									<div class="card-body">
										<div>
											<h6 class="main-content-label mb-1">Arrival Date : </h6>
										</div>
                                        <input class="form-control" placeholder="DD-MM-YYYY" type="date" id="arrival_date" style="margin-bottom:20px" onchange="arrival()">
										<div class="table-responsive secondtable">
											<table class="table text-nowrap text-md-nowrap table-bordered mg-b-0" id="tablenya">
												<thead>
													<tr>
														<th>DN Number</th>
														<th>Supplier</th>
														<th>Purchase Order</th>
														<th>Order Date</th>
													</tr>
												</thead>
												<tbody id="result">
                                                @foreach($data as $d)
													<tr>
                                                        <?php $url = base64_encode($d->dn_tr_id); ?>
														<td><a href="{{ route('print-delivery-notes',$url) }}" target="_blank">{{ $d->dn_tr_id }}</a></td>
														<td>{{ $d->ct_ad_name }}</td>
														<td>{{ $d->dn_po_nbr }}</td>
														<td>{{ date('d-m-Y',strtotime($d->dn_order_date)) }}</td>
													</tr>
												</tbody>
                                                @endforeach
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- End Row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
@endsection