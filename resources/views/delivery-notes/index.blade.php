@extends('templates.main')

@section('title', 'Edit Delivery Note')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body mt-3">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <hr class="mg-b-40">
                <div class="row mb-1">
                    <div class="col-lg-4 col-md-6 box">
                        <div class="input-group">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">DN Number</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" id="filter_dn_number" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row mb-1 mt-2">
                    <div class="col-lg-3 box">
                        <div class="input-group">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Supplier</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" placeholder="" id="code" disabled>
                        </div>
                    </div>
                    <div class="col-lg-3 box">
                        <div class="input-group">
                            <span class="input-group-text spanbox exinput-custom d-sm-none" id="basic-addon1"></span>
                            <input type="text" style="color:black" class="form-control form-control-sm" placeholder="" id="name" disabled>
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-1 box ml-2">
                        <button class="btn btn-info btn-sm" id="seacrhSOAPBusinesRelation"><i class="fas fa-search"></i></button>
                    </div>
                </div>

                <div class="row mt-2 mb-2">
                    <div class="col-lg-4 box">
                        <div class="input-group mt-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Arrival Date</span>
                            <select class="form-control form-control-sm" id="filter_moon" style="color:black">
                                <option value="" selected>Choose Month</option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                            <select class="form-control form-control-sm ml-2" id="filter_year" style="color:black">
                                <option value="" selected>Choose Year</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-8 box"></div>
                    <div class="btn btn-list">
                        <button type="button" class="btn btn-info btn-md ml-2 mt-2" id="filter"><i class="fas fa-search"></i> Search</button>
                        <button type="button" class="btn btn-danger btn-md ml-2 mt-2" id="reset"><i class="fas fa-undo"></i> Reset</button>
                    </div>
                    <div class="col-lg-2 box mt-2">
                        <div class="input-group mb-1">
                        </div>
                    </div>

                </div>
                <div class="table-responsive mb-2 mt-2">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100 mt-2" id="main-table">
                        <thead>
                            <tr>
                                <th scope="col">DN Number</th>
                                <th scope="col">PO Number</th>
                                <th scope="col">Supplier ID</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Order Date</th>
                                <th scope="col">Order Time</th>
                                <th scope="col">Arrival Date</th>
                                <th scope="col">Arrival Time</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- get supplier Modal effects -->
<div class="modal" id="getSupplierTarget">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="get-supplier">
                        <thead class="thead">
                            <tr class="backgroudrowblue">
                                <th scope="col">Supplier Id</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->

<!-- Alert Modal -->
<div class="modal" id="checksupplier">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-body tx-center pd-y-20 pd-x-20">
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button> <i class="icon icon ion-ios-close-circle-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                <h4 id="nullsender" class="tx-danger mg-b-20"></h4>
                <button aria-label="Close" class="btn ripple btn-danger pd-x-25" data-dismiss="modal" type="button">Continue</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="checksupplier">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-body tx-center pd-y-20 pd-x-20">
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button> <i class="icon icon ion-ios-close-circle-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                <h4 id="nullsender" class="tx-danger mg-b-20"></h4>
            </div>
        </div>
    </div>
</div>
<!-- End Alert Modal -->

@endsection

@section('scripts')
<script type="text/javascript">
    function deleteDN(tr) {
        Swal.fire({
            title: 'Are you want to delete this data ?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "GET",
                    url: "/delete-delivery-notes",
                    data:{
                        tr : tr,
                    },
                    success: function (data) {
                        SW.success({
                            message: 'Delete Delivery Note Successfully',

                        });
                        setTimeout(function(){
                            location.reload();
                        }, 1000);
                    },
                    error: function(error) {
                        alert('Data is not Defined!!!');
                    }
                });
            }
        })
    }
</script>
@endsection
