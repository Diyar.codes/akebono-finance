<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {{-- css bootstrap --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
        @page { 
            size: 210mm 297mm landscape;
         }
        table {
            margin: 5px 0;
            width: 100%;
        }

    </style>

    <title>Print Delivery Note</title>
</head>
<body>
    <div class="container">

        <div class="d-lg-flex">
        <h2 class="main-content-label mb-1">Delivery Note</h2>
        </div>
        <hr class="mg-b-40">
                <table cellpadding='0' cellspacing='0' align='top' valign='top'>
                    <tr align='left' valign='middle' >
                        <td><font style='font-size:10pt'>Delivery Note No.</font> </td>
                        <td><font style='font-size:10pt'>: {{ $header['numberDN'] }}</font>  </td>
                        <td><font style='font-size:10pt'></font> </td>
                        <td><font style='font-size:10pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font> </td>
                        <td><font style='font-size:10pt'>Supplier </font> </td>
                        <td><font style='font-size:10pt'>: {{ $header['namasupplier'] }} {{ $header['alamat'] }} {{ $header['alamat2'] }}</font> </td>
                        <td rowspan='4'><font style='font-size:10pt'></font> </td>
                    </tr>
                    
                    <tr align='left' valign='middle' >
                        <td><font style='font-size:10pt'>Purchase Order No.</font> </td>
                        <td><font style='font-size:10pt'>: {{ $header['ponum'] }}</font>  </td>
                        <td colspan='2'><font style='font-size:10pt'></font> </td>
                    </tr>
                    
                    <tr align='left' valign='middle' >
                        <td><font style='font-size:10pt'>Order Date</font> </td>
                        <td><font style='font-size:10pt'>:<?= date('d-m-Y', strtotime($header['orderdate'])); ?></font>  </td>
                        <td colspan='2'><font style='font-size:10pt'>&nbsp;&nbsp; Cycle : {{ $header['ordercyle'] }}</font>  </td>
                        
                    </tr>
                    
                    <tr align='left' valign='middle' >
                        <td><font style='font-size:10pt'>Arrival Date</font>  </td>
                        <td><font style='font-size:10pt'>:<?= date('d-m-Y', strtotime($header['arrivaldate'])); ?></font>  </td>
                        <td colspan='2'><font style='font-size:10pt'>&nbsp;&nbsp; Cycle : {{ $header['arrivalecycle'] }}</font> </td>
                        
                    </tr>
                </table>
                <br>
        <table border='0.5' cellpadding='0' cellspacing='0' width="100%">
                <tr align='center' valign='middle'>                        
                    <td style="font-size: 9pt"> NO </td>
                    <td style="font-size: 9pt"> ITEM NUMBER</td>
                    <td style="font-size: 9pt"> PART NAME</td>
                    <td style="font-size: 9pt"> TYPE</td>
                    <td style="font-size: 9pt"> UM</td>
                    <td style="font-size: 9pt"> BACK NO</td>   
                    <td style="font-size: 9pt"> UM/<br>KANBAN</td>                     
                    <td style="font-size: 9pt"> PALLET/ <br>KANBAN</td>                     
                    <td style="font-size: 9pt"> ORDER <br> KANBAN</td>                      
                    <td style="font-size: 9pt"> QTY UM</td>                        
                    <td style="font-size: 9pt"> ACTUAL</td>                        
                    <td style="font-size: 9pt"> STATUS</td>                        
                </tr>
                    <?= $header['tablenya'] ?>
                
        </table>
        <table border='0' cellpadding='0' cellspacing='0' align='left'>
                <tr>
                    <td><font style='font-size:9pt'>AAIJ - Supplier</font></td>
                </tr>
                <tr>
                    <td>
                        <table border='0.5' cellpadding='0' cellspacing='0' align='left'>
                            <tr>
                                <td align='center'><font style='font-size:9pt'>Pengirim</font></td>
                                <td align='center'><font style='font-size:9pt'>Driver</font></td>
                                <td align='center'><font style='font-size:9pt'>Penerima</font></td>
                            </tr>
                            <tr>
                                <td height='40' width='150'>&nbsp;</td>
                                <td height='40' width='150'>&nbsp;</td>
                                <td height='40' width='150'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align='center'><font style='font-size:9pt'>PPC-AAIJ</font></td>
                                <td align='center'><font style='font-size:9pt'>Sycrum Logistic</font></td>
                                <td align='center'><font style='font-size:9pt'>PPC-Supplier</font></td>
                            </tr>
                        </table>
                        <br>
                        <table border='0' cellpadding='0' cellspacing='0' align='left'>
                        <tr>    
                            <td><font style='font-size:9pt'>Supplier - AAIJ</font></td>
                        </tr>
                        </table>
                        <table border='0.5' cellpadding='0' cellspacing='0' align='left'>
                            <tr>
                                <td align='center'><font style='font-size:9pt'>Pengirim</font></td>
                                <td align='center'><font style='font-size:9pt'>Driver</font></td>
                                <td align='center'><font style='font-size:9pt'>Penerima</font></td>
                            </tr>
                            <tr>
                                <td height='40' width='150'>&nbsp;</td>
                                <td height='40' width='150'>&nbsp;</td>
                                <td height='40' width='150'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align='center'><font style='font-size:9pt'>PPC-Supplier</font></td>
                                <td align='center'><font style='font-size:9pt'>Sycrum Logistic</font></td>
                                <td align='center'><font style='font-size:9pt'>PPC-AAIJ</font></td>
                            </tr>
                        </table>
                    </td>
                    
                    
                </tr>
            </table>
            <p>
            Perhatian : 
            a. Jika jumlah barang yang diorder belum terpenuhi, kolom tanda tangan <br>Supplier - AAIJ tidak boleh diisi.   
            <p></p>
            Keterangan : <br>
            Lembar 1 (Putih) untuk PPC<br>
            Lembar 2 (Merah) untuk Supplier<br>
            Lembar 3 (Kuning) untuk Syncrum Logistic
            </p>
    </div>

<!--     {{-- js bootstrap --}}
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script> --}} -->
</body>
</html>
