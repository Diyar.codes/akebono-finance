@extends('templates.main')

@section('title', 'View Delivery Note')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <hr class="mg-b-40">
                <div class="row">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">DN Number</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" placeholder="" aria-label="Username" aria-describedby="basic-addon1" id="dn" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-3 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">PO Number</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" placeholder="" aria-label="Username" aria-describedby="basic-addon1" id="po" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-3 box">
                        <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Item Number</span>
                            <input type="text" style="color:black" class="form-control form-control-sm" placeholder="" aria-label="Username" aria-describedby="basic-addon1" id="item" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 box">
                        <button type="button" class="btn btn-info btn-md ml-2" onclick="search_data()"><i class="fas fa-search"></i> Search </button>
                    </div>
                </div>
                <div class="table-responsive mt-3 secondtable">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="isi-table">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">DN Number</th>
                                <th scope="col">PO Number</th>
                                <th scope="col">Order Date</th>
                                <th scope="col">Arrival Date</th>
                                <th scope="col">Cycle Order</th>
                                <th scope="col">Cycle Arrival</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tblsecond">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal effects -->
<div class="modal" id="modaldemo8part2">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="main-table">
                        <thead class="thead">
                            <tr>
                                <th scope="col">Supplier Id</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection


@section('scripts')
<script type="text/javascript">
    $('#isi-table').DataTable({
        ordering: false
    });
</script>
@endsection