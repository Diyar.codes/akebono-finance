<title>Print Delivery Note</title>
  <style>
    table {
    border-collapse:separate; 
    border-spacing: 0;
  }
  </style>
  <table border ='0' align='center' cellpadding='0' cellspacing='0' width='80%' height='100px'>
        <tr>
          <td align='center'><font size='5'>PT. AKEBONO BRAKE ASTRA INDONESIA</font><br><font size='4'>Jl. Pegangsaan Dua,Blok A1, Km 1.6 Kelapa Gading, Jakarta Utara 14250 <br> Telp: 021 - 46830075 , Fax : 021 - 46830080</font></td>
        </tr>
        <?php $today = date("d F Y"); ?>
  </table>
  <br>
  <table border ='0' align='left'>
		  <tr>
				<td  nowrap><font size='5'><b>DELIVERY NOTE</font></b></td>
		  </tr>	
		  <tr>
        <td nowrap>DN Number</td>
        <td width='20'>:</td>
        <td width='400' colspan='4'>{{ $cari_no->dn_tr_id }}</td>
        <td width='20'>Supplier</td>
        <td rowspan='6' align='center' width='150' height='150'>{!! QrCode::size(85)->generate($cari_no->dn_tr_id); !!}</td>
      </tr>
      <tr>
        <td nowrap >PO Number</td>
        <td width='20'>:</td>
        <td width='300' colspan='4'>{{ $cari_no->dn_po_nbr }}</td>
        <td width='300' rowspan='4'>{{ $data_supplier->ct_ad_name }} <br> {{ $data_supplier->ct_ad_line1 }} <br> {{ $data_supplier->ct_ad_line2 }}<br> {{ $data_supplier->ct_ad_city }}</td>
      </tr>
      <tr>
        <td nowrap>Order Date</td>
        <td width='20'>:</td>
        <td>{{ date('d-m-Y',strtotime($cari_no->dn_order_date)) }}</td>
      </tr>
      <tr>	
        <td nowrap>Cycle Order</td>
        <td width='10'>:</td>
        <td>{{ $cari_no->dn_cycle_order }}</td>
      </tr>
      <tr>
        <td nowrap>Arrival Date</td>
        <td width='20'>:</td>
        <td>{{ date('d-m-Y',strtotime($cari_no->dn_arrival_date)) }}</td>
      </tr>
      <tr>	
        <td nowrap>Cycle Order</td>
        <td width='10'>:</td>
        <td>{{ $cari_no->dn_cycle_arrival }}</td>
      </tr>	
  </table>	

<br><br><br>
<table border ='0' align='left' cellpadding='0' cellspacing='0' style='margin-bottom:20px'>
<tr>
	<td>
	<table border ='1' align='left' cellpadding='0' cellspacing='0'> 
		<tr>
      <td align='center' ><font size='3' > Line </font></td>
			<td align='center' ><font size='3' > Item Number </font></td>
			<td align='center' ><font size='3' > Part Name </font> </td>
			<td align='center' ><font size='3' > Type </font> </td>
			<td align='center' ><font size='3' > UM </font> </td>
			<td align='center' ><font size='3' > Back No </font></td>
			<td align='center' ><font size='3' > UM/Kanban </font> </td>
			<td align='center' ><font size='3' > Pallet/Kanban </font> </td>
			<td align='center' ><font size='3' > Order Kanban </font> </td>
			<td align='center' ><font size='3' > Qty UM </font> </td>
			<td align='center' ><font size='3' > Actual </font> </td>
			<td align='center' ><font size='3' > Status </font> </td>
    </tr>	
   			
   			@foreach($cari_dn as $cd)
	   		<?php	
	   			getDataPtMstr($cd->dnd_part);
			    
			    $detail_item = DB::table('SOAP_pt_mstr')->select('*')
			    ->where('item_number',$cd->dnd_part)
			    ->where('pt_domain','AAIJ')
			    ->first();

			    // $cari_back = DB::table('dn_item_supp')->select('back_no','pcs_kanban','pallet_kanban')
			    // ->where('kd_item',$cd->dnd_part)
			    // ->where('kd_supp',$cd->dn_vend)
			    // ->first();
			    
			    $tampil_um = DB::table('SOAP_po_detail')->select('*')
			    ->where('no_po',$cd->dn_po_nbr)
			    ->where('item_number',$cd->dnd_part)
			    ->first();
  				
  				$back   = $cd->back_no;
				$kanban = $cd->pcs_kanban;
				$pallet = $cd->pallet_kanban;

				$order  = $cd->dnd_qty_order;
				$pcs    = ($kanban * $order);
      ?>
				<tr>
				<td align='center' width='55' nowrap>{{ $cd->dnd_line }}</td>
				<td align='center' width='150' nowrap>{{ $cd->dnd_part }}</td>
				<?php if ($detail_item->deskripsi1 == ''){ ?>
					<td align='center' width='230' nowrap><?= $tampil_um->item_deskripsi ?></td>
				<?php }else{ ?>
					<td align='center' width='230' nowrap><?= $detail_item->deskripsi1 ?></td>
				<?php }	?>
				
				<td align='center' width='75'><?= $detail_item->desc_type ?></td>
				<td align='center' width='45'><?= $tampil_um->po_um ?></td>
				
				<?php if($back == ""){ ?>
					<td width='80'>&nbsp;</td>
        <?php }else{ ?>
					<td align='center' width='80'><?= $back ?></td>
        <?php } ?>
				
				<?php if($kanban == ""){ ?>
					  <td width='85'>&nbsp;</td>
        <?php }else{ ?>
					  <td align='center' width='85'><?= $kanban ?></td>
        <?php } ?>
				
        <?php if($pallet == ""){ ?>
					  <td width='85'>&nbsp;</td>
        <?php }else{ ?>
					  <td align='center' width='85'><?= $pallet ?></td>
        <?php } ?>
				
				<td align='center' height='30'><?= number_format($order) ?></td>
				<td align='center' width='90'><?= number_format($pcs) ?></td>
				<td width='90'>&nbsp;</td>
				<td width='90'>&nbsp;</td>
				</tr>
			@endforeach
	  <!-- <tr>
        <td colspan='11'>&nbsp;</td>
    </tr>	 -->
		</td>
		</tr>
		</table>
		
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
		<td colspan='11'>
			<table border ='0' align='right' cellpadding='0' cellspacing='0'>
					<tr>	
						<td height='40' width='200' align='center'>Supplier</td>
						<td>
							<table border ='1' align='right' cellpadding='0' cellspacing='0' >
								<tr>
									<td height='40' width='200'>&nbsp;</td>
									<td height='40' width='200'>&nbsp;</td>
								</tr>
							</table>	
						</td>
						
					</tr>
					<tr>	
						<td height='40' width='200' align='center'>Driver</td>
						<td>
							<table border ='1' align='right' cellpadding='0' cellspacing='0' >
								<tr>
									<td height='40' width='200'>&nbsp;</td>
									<td height='40' width='200'>&nbsp;</td>
								</tr>
							</table>	
						</td>
					</tr>
			</table>
		</td>
	</tr>
		
	</td>
	</tr>
	
	<tr>
		<td>
			<table border ='0' align='left' cellpadding='0' cellspacing='0' >
				<tr>
					<td width='650'>AAIJ - SUPPLIER</td>
					<td width='350px' rowspan='3'>PERHATIAN : <br> - Jika jumlah barang yang diorder belum terpenuhi,	 	 
														kolom tanda tangan SUPPLIER - AAIJ tidak boleh diisi. <br>
														<BR>KETERANGAN : <BR>Lembar 1 (putih) untuk PPC<br>
														Lembar 2 (merah) untuk Supplier<br>
														Lembar 3 (kuning) untuk Syncrum Logistic</td>
				</tr>
				
				<tr>
					<td>
						<table border ='1' align='left' cellpadding='0' cellspacing='0' >
							<tr>
								<td align='center'>Pengirim</td>
								<td align='center'>Driver</td>
								<td align='center'>Penerima</td>
							</tr>
							<tr>
								<td width='150' height='50'>&nbsp;</td>
								<td width='150' height='50'>&nbsp;</td>
								<td width='150' height='50'>&nbsp;</td>
							</tr>	
							<tr>
								<td align='center'>PPC - AAIJ</td>
								<td align='center'>Syncrum Logistic</td>
								<td align='center'>PPCIC - Supplier</td>
							</tr>
						</table>
				   </td>
				</tr>
				
				<tr>
						<td>SUPPLIER - AAIJ</td>
				</tr>
				<tr>	
					<td>
						<table border ='1' align='left' cellpadding='0' cellspacing='0' >
							<tr>
								<td align='center'>Pengirim</td>
								<td align='center'>Driver</td>
								<td align='center'>Penerima</td>
							</tr>
							<tr>
								<td width='150' height='50'>&nbsp;</td>
								<td width='150' height='50'>&nbsp;</td>
								<td width='150' height='50'>&nbsp;</td>
							</tr>	
							<tr>
								<td align='center'>PPCI - Supplier</td>
								<td align='center'>Syncrum Logistic</td>
								<td align='center'>PPC - AAIJ</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>	
<script>
  window.print();
</script>