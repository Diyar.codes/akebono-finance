@extends('templates.main')

@section('title', 'Delivery Notes')

@section('body')
<div class="inner-body">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div>
                        <h6 class="main-content-label mb-3">@yield('title')</h6>
                    </div>
                    <hr class="mg-b-40">
                    <div class="row">
                        <div class="col-lg-3 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text exinput-custom spanbox mr-3" id="basic-addon1">Purchase Order</span>
                                <input type="text" style="color:black" class="form-control form-control-sm" name="ponumber" placeholder="" id="ponumber">
                                <button type="button" class="btn ripple btn-info btn-sm ml-2" id="modalkdsupplier"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-lg-n2">
                        <div class="col-lg-3 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom mr-3" id="basic-addon1">Supplier</span>
                                <input type="text" style="color:black" id="kodesupplier" name="kdsupplier" class="form-control form-control-sm" placeholder="" readonly aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-lg-3 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom mr-3 d-lg-none" id="basic-addon1"></span>
                                <input type="text" style="color:black" id="namesupplier" class="form-control form-control-sm" placeholder="" readonly aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-lg-n2">
                        <div class="col-lg-3 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom mr-3" id="basic-addon1">Order Date</span>
                                <input type="date" style="color:black" id="orderdate" class="form-control form-control-sm" placeholder="" name="orderdate" required>
                            </div>
                        </div>
                        <div class="col-lg-3 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom mr-3" id="basic-addon1">Cycle</span>
                                <select class="form-control form-control-sm" onchange="generateDN()" id="selectcycle" style="color:black">
                                    <option value="bak">Choose Cycle</option>
                                    @foreach($listcycle as $key)
                                        <option value="{{ $key->cycle_cycle}}">{{ $key->cycle_cycle}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom mr-3" id="basic-addon1">Time</span>
                                <input id="timeorder" type="text" style="color:black" class="form-control form-control-sm" name="timeorder">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-lg-n2">
                        <div class="col-lg-3 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom mr-3" id="basic-addon1">Arrival Date</span>
                                <input type="date" id="arrivaldate" style="color:black" class="form-control form-control-sm" placeholder="" name="arrivaldate">
                            </div>
                        </div>
                        <div class="col-lg-3 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom mr-3" id="basic-addon1">Cycle</span>
                                <input type="text" id="cyclegenerate" style="color:black" class="form-control form-control-sm" name="cycle">
                            </div>
                        </div>
                        <div class="col-lg-3 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom mr-3" id="basic-addon1">Time</span>
                                <input id="timestart" type="text" style="color:black" class="form-control form-control-sm" placeholder="" name="timestart">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-lg-n2">
                        <div class="col-lg-9 box">
                            <div class="input-group mb-3">
                                <span class="input-group-text spanbox exinput-custom mr-3" id="basic-addon1">DN Number</span>
                                <input type="text" id="numberDN" style="color:black" class="form-control form-control-sm" placeholder="" aria-label="Username" aria-describedby="basic-addon1" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="btn btn-list">
                        <div class="input-group mb-3">
                            <button id="searchptmstr" type="button" class="btn btn-info btn-md mb-2 mt-2"><i class="fas fa-search"></i> Search</button>
                            <button type="button" class="btn btn-danger btn-md btn-with-icon mb-2 mt-2 ml-2" onclick="window.location.reload();"><i class="fas fa-undo"></i> Reset</button>
                        </div>
                    </div>
                    <div class="secondtable mt-3">
                        <table id="dataSecondTable" class="display" style="width: 100%">
                            <thead>
                                <tr style="background-color: #0066CC;">
                                    <th>Line </th>
                                    <th>PP Number</th>
                                    <th>Item Number </th>
                                    <th>Part Name </th>
                                    <th>Type</th>
                                    <th>Pcs/ Kanban</th>
                                    <th>Kanban/ Pallet</th>
                                    <th>Qty PO</th>
                                    <th>Qty/LPB</th>
                                    <th>TOTAL DN</th>
                                    <th>TOTAL SJ Supplier</th>
                                    <th>OPEN DN</th>
                                    <th>Qty Outstanding sj</th>
                                    <th>QTY sj Today</th>
                                    <th>QTY Open</th>
                                    <th>Order (Kanban)</th>
                                    <th>Over PO</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody class="tblsecond"></tbody>
                        </table>
                        <div class="row mb-2">
                            <div class="col-lg-2 col-sm-4">
                                <button class="btn btn-success btn-block mb-2" onclick="act_dn()"><i class="fas fa-save"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- PO Modal -->
<div class="modal" id="mdkdsupplier">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header">
                <h6 class="modal-title">PO Lookup</h6>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button class="btn ripple btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End PO Modal -->

<!-- Alert Modal -->
<div class="modal" id="checksupplier">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-body tx-center pd-y-20 pd-x-20">
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button> <i class="icon icon ion-ios-close-circle-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                <h4 id="nullsender" class="tx-danger mg-b-20"></h4>
            </div>
        </div>
    </div>
</div>
<!-- End Alert Modal -->

@endsection

@section('scripts')
<script type="text/javascript">

    $(document).ready(function() {
        $('.secondtable').hide();

        $("#modalkdsupplier").click(function() {
            getLoader();
            $.ajax({
                type: "GET",
                url: "/modal-delivery-notes",
                success: function (data) {
                    $('#mdkdsupplier').modal('show')
                    $('#mdkdsupplier').find('.modal-body').html(data)
                }
            });
        });

        $("#searchptmstr").click(function() {
            event.preventDefault();

            const ponumber = document.getElementById("ponumber").value;
            const kdsupplier = document.getElementById("kodesupplier").value;

            getLoader();
            $.ajax({
                type: "GET",
                url: "/data-second-table-dn",
                data:{
                    ponum : ponumber,
                    kdsupp : kdsupplier
                },
                success: function (data) {
                    $('#dataSecondTable').DataTable().destroy();
                    $('.secondtable').show();
                    $('.secondtable').find('.tblsecond').html(data);
                    $('#dataSecondTable').DataTable({
                    responsive: false,
                    paging : false,
                    scrollY: "400px",
                    ordering : false,
                    language: {
                        searchPlaceholder: '',
                        sSearch: '',
                        lengthMenu: '_MENU_ items/page',
                        }
                    });
                },
                error: function(error) {
                    SW.error({
                        message: 'Data is not Defined!!',
                    });
                }
            });
        });

    });

    function rp(urut){
        var hitung = urut - 1;
        var qty = $('[id^="ordertb"]').map(function () { return $(this).val(); }).get();
        document.getElementById("ordertb["+parseInt(urut)+"]").value = formatRupiah(qty[hitung]);  
    }

    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split    = number_string.split(','),
            sisa     = split[0].length % 3,
            rupiah     = split[0].substr(0, sisa),
            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    function cekdata(urut) {
        var ordertb = $('[id^="ordertb"]').map(function () { return $(this).val(); }).get();
        var qty_open = $('[id^="qty_open"]').map(function () { return $(this).val(); }).get();
        var hitung = urut - 1;

        if(Number(ordertb[hitung].replace(/\D/g,'')) > Number(qty_open[hitung].replace(/\D/g,''))){
            SW.error({
                    message: 'Over Qty Order !!',
            });
            document.getElementById("ordertb["+Number(urut)+"]").value = '';
        }
    }


    function act_dn(){
        var ordertb = $('[id^="ordertb"]').map(function () { return $(this).val(); }).get();
        var ppnumber = $('[id^="ppnumber"]').map(function () { return $(this).val(); }).get();
        var partname = $('[id^="partname"]').map(function () { return $(this).val(); }).get();
        var type = $('[id^="type"]').map(function () { return $(this).val(); }).get();
        var pcskanban = $('[id^="pcskanban"]').map(function () { return $(this).val(); }).get();
        var palletkanban = $('[id^="palletkanban"]').map(function () { return $(this).val(); }).get();
        var line = $('[id^="line"]').map(function () { return $(this).val(); }).get();
        var qty_po = $('[id^="qty_po"]').map(function () { return $(this).val(); }).get();
        var qty_open = $('[id^="qty_open"]').map(function () { return $(this).val(); }).get();

        var ponumber        = document.getElementById("ponumber").value;
        var kodesupplier    = document.getElementById("kodesupplier").value;
        var orderdate       = document.getElementById("orderdate").value;
        var timeorder       = document.getElementById("timeorder").value;
        var arrivaldate     = document.getElementById("arrivaldate").value;
        var timestart       = document.getElementById("timestart").value;
        var numberDN        = document.getElementById("numberDN").value;
        var row             = document.getElementById("row").value;
        var ordercyle       = document.getElementById("selectcycle").value;
        var arrivalecycle   = document.getElementById("cyclegenerate").value;
        var namesupplier    = document.getElementById("namesupplier").value;

            getLoader();
            $.ajax({
                type: "POST",
                url: "/store-delivery-notes",
                data:{
                    _token: $('#token').val(),
                    ordertb : ordertb,
                    ppnumber : ppnumber,
                    partname : partname,
                    type : type,
                    pcskanban : pcskanban,
                    palletkanban : palletkanban,
                    qty_po : qty_po,
                    ponum : ponumber,
                    kdsupp : kodesupplier,
                    orderdate : orderdate,
                    timeorder : timeorder,
                    arrivaldate : arrivaldate,
                    timestart : timestart,
                    numberDN : numberDN,
                    row : row,
                    ordercyle : ordercyle,
                    arrivalecycle : arrivalecycle,
                    line:line,
                    namesupplier : namesupplier
                },
                success: function (data) {
                    if(data == "duplicated"){
                        SW.error({
                            message: 'Delivery Note has been recorded in database',
                        });
                    }else if (data == 'Email empty'){
                        SW.error({
                            message: 'Email Adress is not defined',
                        });
                    }else {
                        setTimeout(function(){
                               location.reload();
                        }, 1000);
                        SW.success({
                            message: 'Create Delivery Note Successfully',
                        });
                    }
                },
                error: function(error) {
                    SW.error({
                            message: 'Data is not Defined!!!',
                    });
                }
            });

    }

    function generateDN() {

        var supplier    = document.getElementById("kodesupplier").value;
        var orderdate   = document.getElementById("orderdate").value;

        if (supplier) {
            var cycle       = document.getElementById("selectcycle").value;
            var ponumber    = document.getElementById("ponumber").value;

            if (cycle == 'bak') {
                document.getElementById("cyclegenerate").value = null;
                document.getElementById("numberDN").value = null;
                document.getElementById("timeorder").value = null;
                document.getElementById("timestart").value = null;
                document.getElementById("arrivaldate").value = null;
            }else {
                getLoader();
                $.ajax({
                    type : "GET",
                    url : "/check-cycle",
                    data:{
                        supplier : supplier,
                        cycle : cycle,
                        orderdate:orderdate,
                        ponumber:ponumber,
                    },
                    success: function(response){
                        var split_response = response.split("+");
                        var split1 = split_response[0];
                        var split2 = split_response[1];
                        var split3 = split_response[2];
                        var split4 = split_response[3];
                        var split5 = split_response[4];
                        var split6 = split_response[5];
                        
                        $("#timeorder").val(split1);
                        $("#cyclegenerate").val(split2);
                        $("#timestart").val(split3);
                        $("#arrivaldate").val(split4);
                        $("#numberDN").val(split5);
                        
                    },
                    error: function(error){
                        SW.error({
                            message: 'Data is not Defined!!!',
                        });
                    }
                });
            }
        }else{
            document.getElementById("nullsender").innerHTML = "Supplier Tidak Boleh Kosong!";
            $('#checksupplier').modal('show');
        }

    }

</script>
@endsection
