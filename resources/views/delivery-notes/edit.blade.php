@extends('templates.main')

@section('title', 'Edit Delivery Note')

@section('body')
<div class="row mb-4">
    <div class="col-12 mb-4">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">DN Number</span>
                            <input type="text" class="form-control form-control-sm" style="color: black;" value="{{ $atas->dn_tr_id }}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Order Date</span>
                            <input type="text" class="form-control form-control-sm" style="color: black;" value="{{ date('d-m-Y',strtotime($atas->dn_order_date)) }}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Cycle</span>
                            <input type="text" class="form-control form-control-sm" style="color: black;" value="{{ $atas->dn_cycle_order }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row mt-lg-n2">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">PO Number</span>
                            <input type="text" class="form-control form-control-sm" style="color: black;" value="{{ $atas->dn_po_nbr }}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Arrival Date</span>
                            <input type="text" class="form-control form-control-sm" style="color: black;" value="{{ date('d-m-Y',strtotime($atas->dn_arrival_date)) }}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Cycle</span>
                            <input type="text" class="form-control form-control-sm" style="color: black;" value="{{ $atas->dn_cycle_arrival }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row mt-lg-n2">
                    <div class="col-lg-4 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Supplier ID</span>
                            <input type="text" class="form-control form-control-sm" style="color: black;" value="{{ $atas->dn_vend }}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-8 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Supplier Name</span>
                            <input type="text" class="form-control form-control-sm" style="color: black;" value="{{ $atas->ct_ad_name }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered text-center text-nowrap w-100">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Line</th>
                                    <th scope="col">Item Number</th>
                                    <th scope="col">Part Name</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Back No</th>
                                    <th scope="col">Pcs/Kanban</th>
                                    <th scope="col">Kanban Pallet</th>
                                    <th scope="col">Qty</th>
                                    <th scope="col">Order(Kanban)</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $q = 1; $jml_row = count($bawah); ?>
                            @if (count($bawah) >= 1)
                           
                            <input type="hidden" name="jml_row" id="jml_row" value="{{ $jml_row }}">
                            @foreach($bawah as $b)
                                <?php 
                                $item = $b->dnd_part;
                                // $item_supp = DB::table('dn_item_supp')
                                //     ->where('kd_item', $item)
                                //     ->first();
                                $pt_mstr = DB::table('SOAP_pt_mstr')
                                ->where('item_number', $item)
                                ->first();
                                ?>
                                    <tr>
                                        <td>{{ $q }}</td>
                                        <input type="hidden" name="dnd_tr_id" id="dnd_tr_id" value="{{ $b->dnd_tr_id }}">
                                        <input type="hidden" name="dnd_part[]" id="dnd_part[]" value="{{ $b->dnd_part }}">
                                        <input type="hidden" name="dnd_line[]" id="dnd_line[]" value="{{ $b->dnd_line }}">
                                        <input type="hidden" name="dnd_qty_order[]" id="dnd_qty_order[]" value="{{ $b->dnd_qty_order }}">
                                        <td>{{ $b->dnd_line }}</td>
                                        <td>{{ $b->dnd_part }}</td>
                                        <td><?= $pt_mstr->deskripsi1 ?></td>
                                        <td><?= $pt_mstr->desc_type ?></td>
                                        <td>{{ $b->back_no }}</td>
                                        <td>{{ $b->pcs_kanban }}</td>
                                        <td>{{ $b->pallet_kanban }}</td>
                                        <td>{{ number_format($b->dnd_qty_order) }}</td>
                                        <td><input type="text" class="form-control" name="order[]" id="order[]"></td>
                                    </tr>
                            <?php $q++ ?>
                            @endforeach
                            @else
                                <tr>
                                    <td colspan="9" align="center">Tidak ada data</td>
                                </tr>
                            @endif
                            
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-success btn-block mb-2" onclick="editDn()"><i class="fas fa-save"></i> Save</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <!-- <a href="" class="btn btn-danger btn-block mb-2"><i class="fas fa-chevron-circle-left"></i> Back</a> -->
                        <button type="button" class="btn btn-danger btn-block mb-2" onclick="back()"><i class="fas fa-chevron-circle-left"></i> Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

    $(document).ready(function(){
        $(".nextinput").hide();

        $("#nextBtn").click(function(){
            
            const pengirim = document.getElementById("pengirim").value;

            if(pengirim){
                $(".nextinput").toggleClass('nextinput newinput');
                $(".newinput").show();
                $(".next").attr("id","newId");
                $("#newId").hide();
            }else {
                document.getElementById("nullsender").innerHTML = "Pengirim Tidak Boleh Kosong!";
                $('#modaldemo5').modal('show');
            }

        });

    });

    function typeno() {
        const date = new Date();
        const years = date.getFullYear();
        const month = date.getMonth();
        const data = document.getElementById("selecttype").value;

        const nogenerate = data + years + month + static;
        
        document.getElementById("nosurat").value = nogenerate;
    }

</script>
@endsection
