<div class="row">
    <div class="col-lg-8 box">
        <div class="input-group mb-1">
            <span class="input-group-text spanbox exinput-custom mr-3" id="basic-addon1">Periode Due Date</span>
            <input type="date" id="min" class="form-control form-control-sm" name="startdate" placeholder="" aria-describedby="basic-addon1">
            <input type="date" id="max" class="form-control form-control-sm ml-2" name="enddate" placeholder="" aria-label="Username" aria-describedby="basic-addon1">
        </div>
    </div>
    <div class="col-lg-4 box"></div>
    <div class="col-lg-2 box mt-2">
        <div class="input-group mb-1">
                <button  class="btn btn-info btn-block btn-sm mb-2 ml-2" onclick="daterange()"><i class="fas fa-search"></i> Search</button>
        </div>
    </div>
    <div class="col-lg-2 box mt-2">
        <div class="input-group mb-1">
                <button type="button" class="btn btn-danger btn-block btn-sm mb-2 ml-2" onclick="refreshPage()"><i class="fas fa-undo"></i> Reset</button>
        </div>
    </div>
</div>

<hr class="mg-b-40">

<div class="table-responsive">
    <table id="tablepo" class="display" style="width: 100%;">
        <thead>
            <tr style="background-color: #0066CC;">
                <th scope="col"><font color="white">PO Number</font></th>
                <th scope="col"><font color="white">PO Due Date</font></th>
                <th scope="col"><font color="white">Supplier</font></th>
                <th scope="col"><font color="white">Supplier Name</font></th>
                <th scope="col"><font color="white">PO Status</font></th>
                <th scope="col"><font color="white">PO Approve</font></th>
                <th scope="col"><font color="white">Task</font></th>
            </tr>
        </thead>
        <tbody class="firsttable">
            @foreach($data as $key)
            <?php 
            set_time_limit(0);
            $GetPO = collect(\DB::select("SELECT COUNT(a.po_nbr) jumlahApprove from (select max(po_revisi) as revisi, po_nbr from po_trans_mstr where po_nbr ='".$key['po_number']."' group by po_nbr ) aux inner join po_trans_mstr a on a.po_nbr = aux.po_nbr where aux.revisi=a.po_revisi and a.po_app2_status='OK'"))->first();

            $jumlahApprove = $GetPO->jumlahApprove;

            if($jumlahApprove == 0){
                $keteranganApprove ='Not Yet Approved';
            }else{
                $keteranganApprove ='Approved';
            }
            if(empty($key['po_status'])){
                $st = '-';
            }else{
                $st = $key['po_status'];
            } ?>
            
            <tr>
                <td> {{ $key['po_number'] }}</td>
                <td> {{ date('d-m-Y',strtotime($key['POdue_date'])) }}</td>
                <td> {{ $key['kode_supplier'] }}</td>
                <td> {{ $key['nama_supplier'] }}</td>
                @if($st != 'c')
                    <td> Opened </td>
                @else
                    <td> Closed </td>
                @endif
                <td> {{ $keteranganApprove }}</td>
                <td> <button class="btn ripple btn-primary btn-sm selected">select</button> </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script type="text/javascript">

$(document).ready(function() {

    var table = $('#tablepo').DataTable( {
        ordering : false,
        fixedHeader: {
            header: false,
            footer: true
        }
    });

    $("#tablepo").on('click','.selected',function(){

        var currentRow=$(this).closest("tr");

        var col1=currentRow.find("td:eq(0)").text();
        var col2=currentRow.find("td:eq(2)").text();
        var col3=currentRow.find("td:eq(3)").text();
        // var data=col1+"\n"+col2+"\n"+col3;

        $("#ponumber").val(col1);
        $("#kodesupplier").val(col2);
        $("#namesupplier").val(col3);

        $("#mdkdsupplier").modal('hide');

    });

});

function daterange() {

    var minDate = document.getElementById('min').value;
    var maxDate = document.getElementById('max').value;

    $.ajax({
        type : "GET",
        url : "/search-data-dn",
        data : {
            minDate : minDate,
            maxDate : maxDate
        },
        success: function(data) {
            $('#mdkdsupplier').find('.modal-body').html(data)
        },
        error: function(error){
            SW.success({
                message: 'Data is not Defined!',
            });
        }
    })
}

function refreshPage(){
    $.ajax({
        type: "GET",
        url: "/modal-delivery-notes",
        success: function (data) {
            $('#mdkdsupplier').find('.modal-body').html(data)
        }
    });
}

</script>
