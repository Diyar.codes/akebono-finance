@extends('templates.main')

@section('title', 'List Delivery Notes')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <hr class="mg-b-40">
                <div class="row mt-lg-n2">
                    <div class="col-lg-3 box">
                        <div class="input-group mb-3">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Supplier</span>
                            <input type="text" style="color:black" id="supp_id" class="form-control form-control-sm" placeholder="" readonly aria-describedby="basic-addon1">
                        </div>
                    </div>
                    <div class="col-lg-3 box">
                        <div class="input-group mb-3">
                            <input type="text" style="color:black" id="supp_name" class="form-control form-control-sm" placeholder="" readonly aria-describedby="basic-addon1">
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-1 box ml-2">
                        <button class="btn btn-info btn-sm" id="seacrhSOAPBusinesRelation"><i class="fas fa-search"></i></button>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-lg-5 box">
                        <div class="input-group">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Range Date</span>
                            <input type="date" style="color:black" class="form-control" placeholder="" aria-label="Username" aria-describedby="basic-addon1" id="date_start" autocomplete="off">
                            <input type="date" style="color:black" class="form-control ml-2" placeholder="" aria-label="Username" aria-describedby="basic-addon1" id="date_end" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-3 box">
                        <div class="input-group">
                        </div>
                    </div>
                </div>
                <div class="btn btn-list">
                    <button type="button" class="btn btn-info btn-md mb-2 mt-2" onclick="search_dn()"><i class="fas fa-search"></i> Search</button>
                    <button type="button" class="btn btn-danger btn-md mb-2 mt-2" onclick="window.location.reload();"><i class="fas fa-undo"></i> Reset</button>  
                    <form action="{{ route('export-list-dn')}}" method="POST">
                        @csrf
                        <input type="hidden" style="color:black" id="supplier_id" class="form-control form-control-sm" aria-describedby="basic-addon1" name="supplierID" readonly>
                        <input type="hidden" style="color:black" id="datestart" class="form-control form-control-sm"  readonly aria-describedby="basic-addon1" name="datestart">
                        <input type="hidden" style="color:black" id="dateend" class="form-control form-control-sm"  readonly aria-describedby="basic-addon1" name="dateend">
                        <button type="submit" class="btn ripple btn-outline-success btn-sm btn-icon mb-2 btnlist" data-toggle="tooltip" data-original-title="Download Excel"><i class="si si-cloud-download"></i></button>
                    </form>
                </div>
                <div class="table-responsive mb-2 mt-2 secondtable">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="isi-table">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">No Delivery Note</th>
                                <th scope="col">Item</th>
                                <th scope="col">Description</th>
                                <th scope="col">Type</th>
                                <th scope="col">Arrival Date</th>
                                <th scope="col">Arrival Cycle</th>
                                <th scope="col">QTY</th>
                            </tr>
                        </thead>
                        <tbody class="tblsecond">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- get supplier Modal effects -->
<div class="modal" id="getSupplierTarget">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="get-supplier">
                        <thead class="thead">
                            <tr class="backgroudrowblue">
                                <th scope="col">Supplier Id</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection
