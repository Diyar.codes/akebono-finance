@extends('templates.main')

@section('title', 'View Delivery Note')

@section('body')
    <div class="row mb-4">
        <div class="col-12 mb-4">
            <div class="card mt-n3">
                <div class="card-header mt-4 mb-n4">
                    <div class="col-lg-6">
                        <h2 style="font-weight: bold;" class="page-title">@yield('title')</h2>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-lg-4 mr-n4">
                            <div class="input-group">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">DN Number</span>
                                <input type="text" class="form-control" id="item-number" aria-label="Username" aria-describedby="basic-addon1" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-lg-4 mr-n4">
                            <div class="input-group">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">PO Number</span>
                                <input type="text" class="form-control" id="transaction" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Item Number</span>
                                <input type="text" class="form-control" id="packing-slip" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-1">
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
                            <button type="button" class="btn btn-info btn-block mb-2" id="filter"><i class="fas fa-search"></i> Search</button>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
                            <button type="button" class="btn btn-danger btn-block mb-2" id="reset"><i class="fas fa-undo"></i> Reset</button>
                        </div>
                    </div>
                    <div class="row table-responsive">
                        <table class="data-table table table-striped text-center" id="main-table">
                            <thead class="exthead-custom">
                                <tr>
                                    <th scope="col" class="align-middle">DN Number</th>
                                    <th scope="col" class="align-middle">PO Number</th>
                                    <th scope="col" class="align-middle">Item Number</th>
                                    <th scope="col" class="align-middle">Deskripsi</th>
                                    <th scope="col" class="align-middle">Supplier ID</th>
                                    <th scope="col" class="align-middle">Type</th>
                                    <th scope="col" class="align-middle">Order Date</th>
                                    <th scope="col" class="align-middle">Arrival Date</th>
                                    <th scope="col" class="align-middle">Cycle Order</th>
                                    <th scope="col" class="align-middle">Cycle Arrival</th>
                                    <th scope="col" class="align-middle">Qty</th>
                                    <th scope="col" class="align-middle">Time</th>
                                    <th scope="col" class="align-middle">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
