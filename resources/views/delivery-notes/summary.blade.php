@extends('templates.main')

@section('title', 'Summary Delivery Note')

@section('body')
<div class="row row-sm my-md-2 mt-5">
    <div class="col-12">
        <div class="card custom-card overflow-hidden">
            <div class="card-body">
                <div>
                    <h6 class="main-content-label mb-3">@yield('title')</h6>
                </div>
                <hr>
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-4 p-0">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Supplier ID</span>
                        </div>
                    </div>
                    <div class="col-lg-1 col-2 p-0">
                        <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="code" disabled>
                    </div>
                    <div class="ml-1 mr-1">
                        <button type="submit" class="btn btn-sm btn-sm-responsive btn-info" data-toggle="modal" data-target="#getSupplierTarget" id="seacrhSOAPBusinesRelation"><i class="fas fa-search"></i></button>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-4 p-0">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-sm form-control-sm-responsive text-dark" autocomplete="off" id="name" disabled>
                        </div>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-4 p-0">
                        <div class="input-group mb-1">
                            <span class="exinput-custom-responsive">Created By</span>
                        </div>
                    </div>
                    <div class="col-lg-2 col-3 p-0">
                        <select class="form-control form-control-sm form-control-sm-responsive text-dark" id="app">
                            <option value="">Choose</option>
                            <option value="WHS" id="WHS">WHS</option>
                            <option value="latif" id="latif">latif</option>
                            <option value="yadi" id="yadi">yadi</option>
                            <option value="yudhi" id="yudhi">yudhi</option>
                            <option value="andi" id="andi">andi</option>
                            <option value="prahara" id="prahara">prahara</option>
                            <option value="wiyanto" id="wiyanto">wiyanto</option>
                            <option value="sapto" id="sapto">sapto</option>
                        </select>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-xl-2 col-md-3 col-4 p-0">
                        <div class="input-group">
                            <span class="exinput-custom-responsive">Periode Transaction</span>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-8 p-0">
                        <div class="row">
                            <div class="col-6 mr-n2">
                                <select class="form-control form-control-sm form-control-sm-responsive text-dark" id="moon">
                                    <option value="">Month</option>
                                    @for($i = 1; $i <= 12 ; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-6 ml-n2">
                                <select class="form-control form-control-sm form-control-sm-responsive text-dark" id="year">
                                    <option value="">Year</option>
                                    @for($t = date('Y')-5; $t <= date('Y') ; $t++)
                                        <option value="{{ $t }}">{{ $t }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-lg-2 col-sm-4">
                        <button type="submit" class="btn btn-sm btn-success btn-block mb-2" id="search"><i class="fas fa-search"></i> Search</button>
                    </div>
                    <div class="col-lg-2 col-sm-4">
                        <button type="button" class="btn btn-sm btn-danger btn-block btn-md" id="reset"><i class="fas fa-undo"></i> Reset</button>
                    </div>
                </div>
                <div class="row mt-2 t-automatical" style="display: none">
                    <form action="{{ route('export.summary-dn') }}" method="POST">
                        @csrf
                        <div class="col-1 mr-n5">
                            <input type="hidden" value="" name="moonExcel" id="moonExcel">
                            <input type="hidden" value="" name="yearExcel" id="yearExcel">
                            <input type="hidden" value="" name="appExcel" id="appExcel">
                            <input type="hidden" value="" name="codeExcel" id="codeExcel">
                            <button type="submit" class="btn btn-outline-success btn-sm btn-icon mb-2 counterexcel" data-toggle="tooltip" data-placement="top" title="Download Excel" id="downloadExcel"><i class="si si-cloud-download"></i></button>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="table-responsive mb-2">
                    <table class="table table-striped table-bordered text-center w-100" id="main-table">
                        <thead class="atas">
                        </thead>
                        <tbody class="bawah">
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- get supplier Modal effects -->
<div class="modal" id="getSupplierTarget">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100" id="get-supplier">
                        <thead class="thead">
                            <tr class="backgroudrowblue">
                                <th scope="col">Supplier Id</th>
                                <th scope="col">Supplier Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody class="tbodyselect">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects-->
@endsection
