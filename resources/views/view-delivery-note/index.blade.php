@extends('templates.main')

@section('title', 'View Delivery Note')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <h6 class="main-content-label mb-3">@yield('title')</h6>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-1">DN Number</div>
                        <div class="col-md-2"><input type="text" name="dnNumber" id="dnNumber" class="form-control">
                        </div>
                        <div class="col-md-1">PO Number</div>
                        <div class="col-md-2"><input type="text" name="poNumber" id="poNumber" class="form-control">
                        </div>
                        <div class="col-md-1">Item Number</div>
                        <div class="col-md-2"><input type="text" name="itemNumber" id="itemNumber" class="form-control">
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-1">
                            <button class="btn btn-success">Search</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive mb-2">
                                <table class="table table-striped table-bordered text-center w-100" id="main-table">
                                    <thead>
                                        <tr>
                                            <th scope="col">DN Number</th>
                                            <th scope="col">PO Number</th>
                                            <th scope="col">Item Number</th>
                                            <th scope="col">Deskripsi</th>
                                            <th scope="col">Type</th>
                                            <th scope="col">Order Date</th>
                                            <th scope="col">Arrival Date</th>
                                            <th scope="col">Cycle Order</th>
                                            <th scope="col">Cycle Arrival</th>
                                            <th scope="col">Qty</th>
                                            <th scope="col">Time</th>
                                            <th scope="col">Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>92928299</td>
                                            <td>2</td>
                                            <td>90-R7929</td>
                                            <td>PIN, GUIDE</td>
                                            <td>PC</td>
                                            <td>0.27</td>
                                            <td>45,000.0000</td>
                                            <td>RC335503</td>
                                            <td>0</td>
                                            <td>26.800.000</td>
                                            <td>7.187.76</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
