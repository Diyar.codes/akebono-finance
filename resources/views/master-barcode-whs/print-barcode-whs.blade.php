@extends('templates.main')

@section('title', 'Print Barcode WHS')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                            <form action="{{route('print-barcode-whs-show')}}" method="POST" target="_blank">
                                @csrf
                                <div class="row mb-2">
                                    <div class="col-md-1">
                                        Choose :
                                    </div>
                                    <div class="col-md-4">
                                        <select name="print" id="print" style="width:8rem" class="form-control-sm">
                                            <option value="kanban">Kanban</option>  
                                            <option value="chutter">Chutter</option>  
                                            <option value="box">Box</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2 col-sm-4 mb-2">
                                        <button type="submit" class="btn btn-info btn-block mb-2" width="30px" id="Seacrh"><i class="fas fa-eye"></i> View</button>
                                    </div>
                                    <div class="col-lg-2 col-sm-4 mb-2">
                                        <a href="{{route('master-barcode-whs')}}" class="btn btn-danger" style="width: 100%"><i class="fa fa-arrow-left"></i> Back</a>
                                    </div>
                                </div>

                                <input type="hidden" id="qtysupp" name="qtysupp" value="{{$data['qtysupp']}}">
                                <input type="hidden" id="hddpolibox_item" name="hddpolibox_item" value="{{$data['pi']}}">
                                <input type="hidden" id="hddpolibox_loc_from" name="hddpolibox_loc_from" value="{{$data['locfrom']}}">
                                <input type="hidden" id="hddpolibox_loc_to" name="hddpolibox_loc_to" value="{{$data['locto']}}">
                                <input type="hidden" id="hddpolibox_snp" name="hddpolibox_snp" value="{{$data['snp']}}">
                                <input type="hidden" id="hddpolibox_comon" name="hddpolibox_comon" value="{{$data['comon']}}">
                                <input type="hidden" id="hddpolibox_tipe" name="hddpolibox_tipe" value="{{$data['type']}}">	
                                <input type="hidden" id="hddpolibox_urutan" name="hddpolibox_urutan" value="{{$data['urutan']}}">	
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End Modal effects end-->
@endsection
