<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<style>
@media print {
.header{ display: none }
}
</style>

<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE> 

<style type="text/css" media="print">
	@page  
	{ 
		size: A4;   /* auto is the initial value */ 
		/* this affects the margin in the printer settings */ 
		margin: 3mm 3mm 3mm 3mm;  
	}
    P.breakhere {
		page-break-before: always;
	}
	.single_record{
		page-break-after: always;
    }
	table.fixed { 
		table-layout:fixed; 
	}
	table.fixed td { 
		overflow: hidden; 
		border: 0px solid black;
		border-width: thin;
	}
	body{
		table-layout:fixed;
		padding-top:3mm;
		height:auto;
	}
	table{
		margin-left:1.5px;
		margin-right:1.5px;
		margin-top:1.5px;
		margin-buttom:1.5px;
	}
</style> 

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Kanban WHS</title>
</head>

<body>
<div class="container body" style="zoom:78%">
	@php
		$hitung = 1;
		$a = 1;
	@endphp

	<center><table cellpadding='0' cellspacing='0' border='0' width='800px' align='center'><tr><td>
	@foreach ($select_q as $row)

	@php
		$tambah   = 0;
		$no_awal  = 1;
		$hal_awal = 1; 
		
		$polibox_nbr	= $row->polibox_nbr;
		$panjang = "150";
		if(strlen($polibox_nbr)==7){
			$panjang = "180";
		}
		if(strlen($polibox_nbr)==8){
			$panjang = "190";
		}
		if(strlen($polibox_nbr)==9){
			$panjang = "210";
		}
		if(strlen($polibox_nbr)==10){
			$panjang = "230";
		}
		if(strlen($polibox_nbr)==11){
			$panjang = "250";
		}
		if(strlen($polibox_nbr)==12){
			$panjang = "270";
		}
		if(strlen($polibox_nbr)==13){
			$panjang = "290";
		}
		if(strlen($polibox_nbr)==14){
			$panjang = "310";
		}
		if(strlen($polibox_nbr)==15){
			$panjang = "330";
		}
		if(strlen($polibox_nbr)==16){
			$panjang = "350";
		}
		if(strlen($polibox_nbr)==17){
			$panjang = "370";
		}
		if(strlen($polibox_nbr)==18){
			$panjang = "390";
		}
		if(strlen($polibox_nbr)==19){
			$panjang = "410";
		}
		if(strlen($polibox_nbr)==20){
			$panjang = "430";
		}

		$polibox_address_pc	= $row->polibox_address_pc; 
		if($polibox_address_pc==""){$polibox_address_pc="&nbsp;&nbsp;";}
		$polibox_address_sa	= $row->polibox_address_sa; 
		if($polibox_address_sa==""){$polibox_address_sa="&nbsp;&nbsp;";}
		$polibox_type = $row->polibox_type;

		if ($polibox_type == '')
		{
			$polibox_type = $row->polibox_type_det;
		}	

		//cek image extentions
		$img ="";
			$arrayExtension = array("JPG","jpg","JPEG","jpeg","PNG","png","GIF","gif", "JFIF", "RAW");
			for($i=0;$i<count($arrayExtension);$i++)
			{
				if(checkRemoteFile("http://tagbarcode.akebono-astra.co.id/TAG_BARCODE/$row->polibox_item".".".$arrayExtension[$i]))
				{
					$img = "http://tagbarcode.akebono-astra.co.id/TAG_BARCODE/$row->polibox_item".".".$arrayExtension[$i];
				}
			}
			// QR CODE START-->
				//set data
				$data = $polibox_nbr;
				//set it to writable location, a place for temp generated PNG files
				$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
				//html PNG location prefix
				$PNG_WEB_DIR = 'temp/';
				//ofcourse we need rights to create temp dir
				if (!file_exists($PNG_TEMP_DIR))
				mkdir($PNG_TEMP_DIR);
			
				$filename = $PNG_TEMP_DIR.'DELIVERY.png';
				//processing form input
				//remember to sanitize user input in real-life solution !!!
				$errorCorrectionLevel = 'H';
				if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
					$errorCorrectionLevel = $_REQUEST['level'];    

				$matrixPointSize = 2.8;
				if (isset($_REQUEST['size']))
					$matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);
						
					// user data
					$filename = $PNG_TEMP_DIR.'DELIVERY'.md5($data.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
					// QRcode::png($data, $filename, $errorCorrectionLevel, $matrixPointSize, 2);   
			// QR CODE END-->

	@endphp
	@php
		
		$data = "<table border='1' cellspacing='0' cellpadding='0' class='fixed' style='font-family:Calibri;width:135mm;height:50mm'>
			<tr>
				<td colspan='4' rowspan='2' style='text-align:left;height:12.5mm;width:50mm;font-family:Calibri;' valign='buttom' align='buttom'>
					<font style='font-size:12px;text-align:left;font-family:Calibri;'>&nbsp; Back No :</font>
					 <br>
					<font style='font-size:55px;text-align:center;'><b>&nbsp;".$row->polibox_backno."</b></font>
				</td>
				
				<td colspan='2' rowspan='2' style='text-align:left;width:35mm;' valign='top' align='left' >				
					<font style='font-size:12px;'>&nbsp; Box No:</font><br/><br/>
					
					<div align='center'><font style='font-size: 30px;'><b>".$hitung." / ".$select_r1."</b></font></div>
				</td>
				<td colspan='2' rowspan='3' style=height:17.5mm;width:;' align='center'>".QrCode::size(130)->generate($row->polibox_nbr)."</td>
			</tr>
			<tr></tr>
			<tr>
				<td colspan='4' rowspan='4' style='height:22.5mm;width:45mm;' valign='center' align='center'>
					<font style='font-size:12px;text-align:left;'>&nbsp;&nbsp; Part No:</font>
					
					<font style='font-size: 20px;'><b>&nbsp;&nbsp;&nbsp;".$row->polibox_item."</b></font>
					<div>
						
						<font style='font-size:18px;text-align:center;'/><b>&nbsp;".$row->polibox_deskripsi." ".$row->polibox_type." </b></font>
					</div>
				</td>
				<td colspan='2' rowspan='4' style='height:22.5mm;width:45mm;' valign='center' align='center'>
					<div>
						<font style='font-size: 20px;text-align:center;'><center>".QrCode::size(90)->generate($row->polibox_nbr)."</center></font>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan='2' rowspan='3' style='text-align:left;height:17.5mm;width:25mm;' valign='center'>
					<font style='font-size:12px;'>&nbsp;SNP:</font><br/>
					
					<font style='font-size:25px;text-align:center'><b>&nbsp;".$row->polibox_snp." PC</b></font>
					
					<div><font style='font-size:16px;'>&nbsp;".$row->polibox_loc_from." ~ ".$row->polibox_loc_to." </font></div>
				</td>
			</tr>
			<tr></tr><tr></tr>
			<tr>
				<td rowspan='2' style='text-align:center;width:10mm;' valign='top'><font style='font-size:14px;'>Frm</font></td>
				<td colspan='2' style='text-align:center;width:10mm;' valign='top'><font style='font-size:18px;'><b>".$row->polibox_address_pc."&nbsp</b></font></td>
				<td rowspan='2' style='text-align:center;width:15mm;' valign='center'><font style='font-size:18px;'><b>".$row->polibox_lok_asal."&nbsp</b></font></td>
				<td rowspan='2' style='text-align:center;width:10mm;' valign='top'><font style='font-size:14px;'>To</font></td>
				<td colspan='2' style='text-align:center;width:10mm;' valign='top'><font style='font-size:18px;'><b>".$row->polibox_address_sa."</b></font></td>
				<td rowspan='2' style='text-align:center;width:15mm;' valign='center'>
					<div><font style='font-size:18px;'><b>".$row->polibox_lok_now."</b></font></div>
				</td>
			</tr>
			<tr>
				<td colspan='2' style='text-align:center;height:7.5mm;' valign='top'><font style='font-size:20px;'><b>".$row->polibox_chutter."</b></font></td>
				<td colspan='2' style='text-align:center;height:7.5mm;' valign='top'><font style='font-size:20px;'><b>".$row->polibox_chutter1."</b></font></td>
			</tr>
		
		</table>";
		@endphp

		@php
			$tambah = $tambah+1;
			if(fmod($a,2) !=0)
			{
				echo "<table cellpadding='0' cellspacing='3' border='0' align='left' >
				<tr><td width='200px' height='50'>$data</td>";					
			}
			else
			{
				echo "<td width='200px' height='50'>$data</td>";
				echo "</tr></table>";
			}
			
			if(fmod($a,10)==0) { echo "<p class='single_record'></p>"; }
			$a +=1;
			$hitung +=1;
		@endphp
	@endforeach	
	<p class='breakhere'>;

		@php
		function checkRemoteFile($url)
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			// don't download content
			curl_setopt($ch, CURLOPT_NOBODY, 1);
			curl_setopt($ch, CURLOPT_FAILONERROR, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			if(curl_exec($ch)!==FALSE)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		@endphp
</div>
</body>
</html>

