<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<style>
	@media print {
	.header{ display: none }
	}
</style>

<style type="text/css">
    P.breakhere {page-break-before: always}
</style> 
<style type="text/css" media="print">
    .page
    {
     -webkit-transform: rotate(-90deg); 
     -moz-transform:rotate(-90deg);
     filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }
	@page { size: landscape; }
	
	.single_record{
    page-break-after: always;
    }
	@page  
	{ 
		size: auto;   /* auto is the initial value */ 

		/* this affects the margin in the printer settings */ 
		margin: 1mm 0mm 0mm 0mm;  
	} 

	body  
	{ 
		/* this affects the margin on the content before sending to printer */ 
		margin: 0px;  
	} 
</style>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>E-Kanban</title>
</head>

<body>

	{{-- SELECT MAX POLIBOX_MSTR WHERE POLIBOX ITEM --}}
	{{-- WHILE JUMLAH --}}
	{{-- polibox_mstr WHERE polibox_item,polibox_loc_from, --}}
	@php
		$no = 1;
	@endphp
	@foreach ($select_q as $row)

	@php
		$tambah 	= 0;
		$no_awal 	= 1;
		$hal_awal 	= 1;
		
		$polibox_chutter   = $row->polibox_chutter;
		if($polibox_chutter==""){ $enter="<br/><br/>"; } 
		$polibox_chutter1   = $row->polibox_chutter1;
		if($polibox_chutter1==""){ $enter="<br/><br/>"; }


	@endphp
	
		<table border='1' cellspacing='0' cellpadding='0' align='center'>
			<tr>
				<td rowspan='2'>
					<font style='text-align:center;font-size:12px;' face='Arial'>&nbsp;&nbsp; Alamat asal:</font>
					<br/><br/>
					<div align='center'><font style='font-size: 40px;' face='Arial'><b>{{$row->polibox_alamat_asal}}</b></font></div>
						
					<div align='center'><font align='center' face='Arial'>____________________</font></div>
						
					<font style='font-size:12px;' face='Arial'>&nbsp;&nbsp;No. chutter:</font>{!!$enter!!}
					<div align='center'><font style='font-size: 40px;' face='Arial'><b>{{$row->polibox_chutter}} </b></font></div>
						
					<div align='center'><font align='center' face='Arial'>____________________</font></div>
						
					<font  style='font-size: 12px;' face='Arial'>&nbsp;&nbsp;Lokasi: <br/> &nbsp;&nbsp; <b><center><font style='font-size:40px;'>{{$row->polibox_lok_asal}}</font></center></b></font>
						
					<div align='center'><font align='center' face='Arial'>____________________</font></div>
						
					<font style='font-size:12px;' face='Arial'>&nbsp;&nbsp;Back No. Asal:</font>
						
					<div align='center'><font style='font-size:40px;text-align:center;' face='Arial'><b>{{$row->polibox_backno}}</b></font></div>
						
					<div align='center'><font align='center' face='Arial'>____________________</font></div>
						
					<font style='font-size:12px;' face='Arial'>&nbsp;&nbsp;No. Urut Kanban:</font>
						
					<div align='center'><font style='font-size: 35px;' face='Arial'><b>{{$no .' / '. $subbox}}</b></font></div>
				</td>
				<td>
					<font style='font-size:12px;' face='Arial'>&nbsp;&nbsp; Back No:</font>
			
					<div align='center'><font style='font-family:Arial;font-size: 130px;'><b>{{$row->polibox_backno}}</b></font></div>
					
					<font style='font-size:12px;' face='Arial'>&nbsp;&nbsp; SNP:</font>							
					
					<div align='right'><font style='font-size: 22px;' face='Arial'>
						{{-- <IMG SRC='{{DNS1D::getBarcodePNGPath('4445645656', 'PHARMA2T')}}' width="545" height="80"> --}}
							<img src="data:image/png;base64,{{ DNS1D::getBarcodePNG('4445645656', 'C39+')}}" alt="barcode" width="545" height="80"/>
					</font></div>
					<br/>
					<font style='font-size:25px;' face='Arial'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$row->polibox_snp}} PCS</b></font><br/>
					
					<font style='font-size:12px;' face='Arial'>&nbsp;&nbsp; Part No:</font>
						
					<div align='center'><font style='font-size: 25px;' face='Arial'><b>{{$row->polibox_item}}</b></font></div>
						
					<font style='font-size:12px;' face='Arial'>&nbsp;&nbsp; Part Name:</font><br/>
						
					<div><font style='font-size:25px;' face='Arial'><b><center>&nbsp;{{$row->polibox_deskripsi}}  {{$row->polibox_type}}</center></b></font></div>
				</td>
				<td rowspan='2''>
					<font style='text-align:center;font-size:12px;' face='Arial'>&nbsp;&nbsp; Alamat saat ini:</font><br/><br/>
					
					<div align='center'><font style='font-size: 40px;' face='Arial'><b>{{$row->polibox_alamat_now}}</b></font></div>
						
					<div align='center'><font align='center' face='Arial'>______________________</font></div>
						
					<font style='font-size:12px;' face='Arial'>&nbsp;&nbsp;No. chutter:</font>{!!$enter!!}
					<div align='center'><font style='font-size: 40px;' face='Arial'><b>{{$row->polibox_chutter1}} </b></font></div>
					<div align='center'><font align='center' face='Arial'>______________________</font></div>
						
					<font style='font-size:12px;' face='Arial'>&nbsp;&nbsp;Lokasi:<br/> &nbsp;&nbsp; <b><center><font style='font-size:40px;'>{{$row->polibox_lok_now}}</font></center></b></font>
						
					<div align='center'><font align='center'>______________________</font></div>
						
					<font style='font-size:12px;' face='Arial'>&nbsp;&nbsp;Process:</font>
						
					<div align='center'><font style='font-size: 30px;' face='Arial'><b>{{$row->polibox_loc_from}} <br/>{{$row->polibox_loc_to}}</b></font></div>
						
					<div align='center'><font align='center' face='Arial'>______________________</font></div>
						
					<font style='font-size:12px;' face='Arial'>&nbsp;&nbsp;Item No:</font><br/>
						
					<div align='center'><font style='font-size:20px;' face='Arial'>{{$row->polibox_item}}</font></div>
				</td>
			</tr>
		</table>
		@php
			if (fmod($no,3) == 0) {
				echo '<p class="breakhere">';
			}
			$no += 1;
			$hal_awal = $hal_awal+1; 
			$tambah = $tambah+1;
		@endphp
	@endforeach

</body>
</html>