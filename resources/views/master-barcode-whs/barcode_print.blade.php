<style>
    table, td, th {
      border: 1px solid black;
    }

    table {
      width: 100%;
      border-collapse: collapse;
    }
</style>
<div id="printbarcode" style="width:50%;border: 1px solid black">
    <div style="text-align: center;border-bottom:1px solid black">
        <h3>BON PENGAMBILAN BARANG</h3>
    </div>
    <div style="margin-top:25px;align-content:center;">
        <center>
        <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($data->id, 'S25+')}}" alt="barcode" width="300" height="100" /><br>
        <p style="margin-top:0%;"><b>*{{$data->kode_transaksi}}*</b></p>
        </center>
    </div>
    <div style="margin-top:20px;">
        <table>
            <tr><td colspan='2'>Kode Transaksi</td><td colspan='3'>{{$data->kode_transaksi}}</td></tr>
            <tr><td colspan='2'>Pengambil</td><td colspan='3'>{{$data->nama_pengambil}}</td></tr>
            <tr><td colspan='2'>Tanggal Ambil</td><td colspan='3'>{{$data->tgl_ambil}}</td></tr>
            <tr><td colspan='2'>Waktu Ambil</td><td colspan='3'>{{$data->waktu}}</td></tr>
        </table>
    </div>
    <div style="margin-top:25px;">
        <p style="margin:0%">Detail</p>
        <table>
            <tr>
                <th>No</th>
                <th>Kode Barang</th>
                <th>Nama Barang</th>
                <th>Jumlah</th>
                <th>Keterangan</th>
            </tr>
            <tr>
                <td style="text-align: center">1</td>
                <td style="text-align: center">{{ $qry2->kd_brg}}</td>
                <td style="text-align: center">{{ $qry2->nm_brg}}</td>
                <td style="text-align: center">{{ $qry2->jml_brg}}</td>
                <td style="text-align: center">{{ $qry2->ket}}</td>
            </tr>
        </table>
    </div>
</div>
<script type="text/javascript">
        window.print();
</script>
