@php
    use Illuminate\Support\Facades\DB;
@endphp
@extends('templates.main')

@section('title', 'Summary Bon Pengambilan Barang')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="" method="POST">
                                @csrf
                                <div class="secondtable mt-3" id="">
                                    <table id="completeBON" class="display" style="width: 100%">
                                        <thead>
                                            <tr style="background-color: #0066CC;color:#fff;">
                                                <th class="text-white">No</th>
                                                <th class="text-white">Kode Transaksi</th>
                                                <th class="text-white">Produksi/Area/Line</th>
                                                <th class="text-white">Pengambil Barang</th>
                                                <th class="text-white">Tanggal Ambil</th>
                                                <th class="text-white">Item</th>
                                                <th class="text-white">Approve FM</th>
                                                <th class="text-white">Approve Admin</th>
                                                <th class="text-white">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tblcomplete">
                                            <?php $no = 1; ?>
                                            @foreach($data as $key)
                                            <tr>
                                                <td style="text-align:center"> {{ $no++ }} </td>
                                                <td style="text-align:center"> {{ $key->kode_transaksi }} </td>
                                                <td style="text-align:center"> {{ $key->plant_name }} / {{ $key->area_name }} / {{ $key->line_prefix }} </td>
                                                <td style="text-align:center"> {{ $key->nama_pengambil }} </td>
                                                <td style="text-align:center"> {{ $key->tgl_ambil }} </td>
                                                <td style="text-align:center">
                                                    @php
                                                        $kdtr = $key->kode_transaksi;
                                                        $jml = DB::select("SELECT COUNT(*) jml FROM whs_bon_brg WHERE id_bon='$kdtr'");
                                                    @endphp
                                                    {{$jml[0]->jml}}
                                                </td>
                                                <td style="text-align:center">
                                                    @if($key->approve_foreman == "1")
                                                        <b>Approve</b><br>{{$key->fm}}<br>{{$key->approve_frm_time}}
                                                    @elseif($key->approve_foreman == "2")
                                                        <b>Rejected</b><br>{{$key->fm}}<br>{{$key->approve_frm_time}}
                                                    @else
                                                        <b>Waiting for Approval</b>
                                                    @endif
                                                </td>
                                                <td style="text-align:center">
                                                    @if($key->approve_admin == "1")
                                                        <b>Approve</b><br>{{$key->fm}}<br>{{$key->approve_frm_time}}
                                                    @elseif($key->approve_admin == "2")
                                                        <b>Rejected</b><br>{{$key->fm}}<br>{{$key->approve_frm_time}}
                                                    @else
                                                        <b>Waiting for Approval</b>
                                                    @endif
                                                </td>
                                                <td style="text-align:center">
                                                <a href="{{route('print-approve-bon-whs',['kode_transaksi' => $key->kode_transaksi , 'id' => $key->id])}}" target="_blank" class="btn btn-sm btn-info mr-1 tablefontmini"><i class="fas fa-print" data-toggle="tooltip" title="" data-original-title="Print"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $('#completeBON').DataTable( {
        fixedHeader:{
            header:true,
            headerOffset: $('#fixed').height(),
        },
        ordering : false,
        paging: false
    } );
</script>
@endsection
