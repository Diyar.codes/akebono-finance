@extends('templates.main')

@section('title', 'Detail Barcode WHS')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                            <form action="{{route('update-detail-master-barcode-whs')}}" method="POST">
                                @csrf
                                <div class="secondtable mt-3" id="">
                                    <table id="datatables" class="display" style="width: 100%">
                                        <thead style="background-color: #0066CC;">
                                            <tr style="background-color: #0066CC;">
                                                <th>No</th>
                                                <th>Item Number</th>
                                                <th>Sub Box</th>
                                                <th>Description</th>
                                                <th>Type Umum</th>
                                                <th>Type Detail</th>
                                                <th>SNP</th>
                                                <th>Loc - From</th>
                                                <th>Loc - To</th>
                                                <th>Barcode</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($data as $row)
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>{{$row->polibox_item}}</td>
                                                    <td>{{$row->polibox_subbox}}</td>
                                                    <td>{{$row->polibox_deskripsi}}</td>
                                                    <td>{{$row->polibox_type}}</td>
                                                    <td><input type='text' name='type_det[{{$no}}]' class='form-control-sm' value='{{$row->polibox_type_det}}'></td>
                                                    <td>{{$row->polibox_snp}}</td>
                                                    <td>{{$row->polibox_loc_from}}</td>
                                                    <td>{{$row->polibox_loc_to}}</td>
                                                    <td>{{$row->polibox_nbr}}</td>
                                                    <input type='hidden' name='idbarcode[{{$no}}]' value='{{$row->polibox_nbr}}'>
                                                </tr>
                                                @php
                                                    $no += 1;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-lg-2 mt-2 mt-md-0">
                                        <button type="submit" class="btn btn-success" style="width: 100%"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-lg-2 mt-2 mt-md-0">
                                        <a href="{{route('master-barcode-whs')}}" class="btn btn-danger" style="width: 100%"><i class="fa fa-arrow-left"></i> Back</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
