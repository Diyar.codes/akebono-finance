@extends('templates.main')

@section('title', 'Master Barcode WHS')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    Item Number
                                </div>
                                <div class="col-10">
                                    <input type="text" name="itemnumber" id="itemnumber" class="form-control-sm" autocomplete="off">
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    Location From
                                </div>
                                <div class="col-md-4">
                                    <select name="locationfrom" id="locationfrom" style="width:8rem" class="form-control-sm">
                                        <option selected disabled></option>
                                        @php
                                            $locationfrom = DB::table('polibox_mstr')->select('polibox_loc_from')->groupBy('polibox_loc_from')->get();
                                        @endphp
                                        @foreach ($locationfrom as $row)
                                            <option value="{{$row->polibox_loc_from}}">{{$row->polibox_loc_from}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    Location To
                                </div>
                                <div class="col-md-4">
                                    <select name="locationto" id="locationto" style="width:8rem" class="form-control-sm">
                                        <option selected disabled></option>
                                        @php
                                            $locationto = DB::table('polibox_mstr')->select('polibox_loc_to')->groupBy('polibox_loc_to')->get();
                                        @endphp
                                        @foreach ($locationto as $row)
                                            <option value="{{$row->polibox_loc_to}}">{{$row->polibox_loc_to}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-lg-2 mt-2 mt-md-0">
                                    <button type="button" class="btn btn-info btn-block mb-2" id="search"><i class="fas fa-search"></i> Search</button>
                                </div>
                                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-lg-2 mt-2 mt-md-0">
                                    <button type="button" class="btn btn-danger btn-block btn-md" id="reset"><i class="fas fa-undo"></i> Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route('update-master-barcode-whs')}}" method="POST">
                                @csrf
                                <div class="secondtable mt-3" id="">
                                    <table id="dataSecondTable" class="display" style="width: 100%">
                                        <thead>
                                            <tr style="background-color: #0066CC;color:#fff;">
                                                <th>No</th>
                                                <th>Item Number</th>
                                                <th>Description</th>
                                                <th>Type Umum</th>
                                                <th>Part Common</th>
                                                <th>SNP</th>
                                                <th>Loc - From</th>
                                                <th>Loc - To</th>
                                                <th>Jml Box</th>
                                                <th>Back No</th>
                                                <th>Alamat From</th>
                                                <th>Chutter From</th>
                                                <th>Lokasi From</th>
                                                <th>Alamat To</th>
                                                <th>Chutter To</th>
                                                <th>Lokasi To</th>
                                                <th>Address PC</th>
                                                <th>Address SA</th>
                                                <th>Qty Supplier</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tblseconded">
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-lg-2 mt-2 mt-md-0">
                                        <button type="submit" class="btn btn-success" style="width:100%">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Modal effects -->
<div class="modal location" id="location">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header text-center">
                <h6 class="modal-title w-100"></h6><button aria-label="Close" class="close" data-dismiss="modal"
                    type="button"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="itemm" id="itemm" class="itemm">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center text-nowrap w-100 main-tablee" id="main-tablee">
                        <thead class="thead">
                            <tr style="background-color: #0066CC;color:#fff;">
                                <th scope="col">Location</th>
                                <th scope="col">Location Deskripsi</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                            <tbody id="body">

                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal effects end-->

@endsection
