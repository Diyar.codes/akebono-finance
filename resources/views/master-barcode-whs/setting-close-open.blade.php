@extends('templates.main')

@section('title', 'Setting CLose/Open Transaction')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                            <hr>
                            <div class="col-md-12">
                                <div class="secondtable mt-3" id="">
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                    <table id="tblsettingcls" class="display text-center" style="width: 100%;">
                                        <thead>
                                            <tr style="background-color: #0066CC;color:#fff;">
                                                <th class="text-white">No</th>
                                                <th class="text-white">Tahun</th>
                                                <th class="text-white">Bulan</th>
                                                <th class="text-white">Status</th>
                                                <th class="text-white">Action</th>
                                                <th class="text-white">Open Date</th>
                                                <th class="text-white">Close Date</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tblapprove">
                                            <?php $no = 1; ?>
                                            @foreach($data as $key)
                                            <tr>
                                                <td> <?= $no++; ?> </td>
                                                <td> {{ $key->set_thn }} </td>
                                                <td> {{ $key->set_bln }} </td>
                                                <td> {{ $key->set_status }} </td>
                                                @if($key->set_thn == $check['years'] && $key->set_bln == $check['month'])
                                                    @if($key->set_status == 'OPEN')
                                                        <td align="center"> Wait Next Month </td>
                                                    @else
                                                        <input type='hidden' id='thn' name='thn' value='{{ $key->set_thn}}'>
                                                        <input type='hidden' id='bln' name='bln' value='{{$key->set_bln}} '>
                                                        <td><button type="btn" class="btn btn-info btn-sm" onclick="storedata('set_currentmonth')">Open</button></td>
                                                    @endif
                                                @else
                                                    @if($key->set_status == 'OPEN')
                                                        <input type='hidden' id='thn' value='{{ $key->set_thn}}'>
                                                        <input type='hidden' id='bln' value='{{$key->set_bln}} '>
                                                        <td><button type="btn" class="btn btn-danger btn-sm" onclick="storedata('set_lastmonth')">Close</button></td>
                                                    @else
                                                        <td>-</td>
                                                    @endif
                                                @endif
                                                <td> {{ $key->set_open_date == null ? '-' : date('d-m-Y H:i:s', strtotime($key->set_open_date)) }} </td>
                                                <td> {{ $key->set_close_date == null ? '-' : date('d-m-Y H:i:s', strtotime($key->set_close_date)) }} </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End Modal effects end-->
@endsection

@section('scripts')
<script type="text/javascript">
    $('#tblsettingcls').DataTable( {
        fixedHeader:{
            header:false,
            headerOffset: $('#fixed').height(),
        },
        ordering : false,
        paging: true
    } );

    function storedata(check) {
        getLoader()
        
        if(check == "set_currentmonth"){
            var title = "Are you sure to activate transaction current month?";
        }else if(check == "set_lastmonth"){
            var title = "Are you sure to close transaction last month?";
        }
        Swal.fire({
            title: title,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!'
        }).then((result) => {
            if (result.isConfirmed) {
                var thn  = document.getElementById('thn').value;
                var bln  = document.getElementById('bln').value;
                $.ajax({
                    type : 'POST',
                    url : '/store-setting-closeopen',
                    data : {
                        _token: $('#token').val(),
                        check : check,
                        bln : bln,
                        thn : thn,
                    },
                    success : function(data){
                        if (data == 'closed') {
                            SW.success({
                                message: 'Data have been closed',
                            });
                        }else if(data == 'duplicate') {
                            SW.success({
                                message: 'Data is Exists!',
                            });
                        }else {
                            SW.success({
                                message: 'Data have been open',
                            });
                        }
                        setTimeout(function(){ 
                            window.location.reload();
                        }, 1000);
                    }, 
                    error : function(error){
                        SW.error({
                            message: 'Failed Data',
                        });
                    }
                })
            }
        });
        

    }
</script>
@endsection