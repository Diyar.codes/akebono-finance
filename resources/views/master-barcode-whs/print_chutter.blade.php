<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<style>
@media print {
.header{ display: none }
}
</style>

<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE> 

<style type="text/css" media="print">
	@page  
	{ 
		size: A4;   /* auto is the initial value */ 
		/* this affects the margin in the printer settings */ 
		margin: 3mm 3mm 3mm 3mm;  
	}
    P.breakhere {
		page-break-before: always;
	}
	.single_record{
		page-break-after: always;
    }
	table.fixed { 
		//table-layout:fixed; 
		 table-layout: auto;
	}
	table.fixed td { 
		overflow: hidden; 
		border: 0px solid black;
		border-width: thin;
	}
	
	.fixed th { 
		text-align 	: center;
		font-size	:50px!important;
	}
	body{
		padding-top:3mm;
		height:auto;
	}
	table{
		width :100%;
		margin-left:1.5px;
		margin-right:1.5px;
		margin-top:1.5px;
		margin-buttom:1.5px;
	}
</style> 

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Kanban WHS</title>
</head>

<body>
<div >

	@php
		
	@endphp
	<center><table style='width:80%!important;padding:0!important;' ><tr><td>

	@foreach ($select_q as $row)

	@php
		$tambah   	= 0;
		$no_awal  	= 1;
		$hal_awal 	= 1; 
		$jml		= $row->qty_supplier;
		if($jml==NULL || $jml==''){
			$qSupp		= mssql_fetch_array( mssql_query("SELECT * FROM polibox_chutter WHERE item_name='".$polibox_item."'"));
			$qSupp		= DB::table('polibox_chutter')->where('item_name',$row->polibox_item)->first();
			$jml		= $qSupp->qty_supplier;
		}

		$panjang = "150";
		if(strlen($row->polibox_nbr)==7){
			$panjang = "180";
		}
		if(strlen($row->polibox_nbr)==8){
			$panjang = "190";
		}
		if(strlen($row->polibox_nbr)==9){
			$panjang = "210";
		}
		if(strlen($row->polibox_nbr)==10){
			$panjang = "230";
		}
		if(strlen($row->polibox_nbr)==11){
			$panjang = "250";
		}
		if(strlen($row->polibox_nbr)==12){
			$panjang = "270";
		}
		if(strlen($row->polibox_nbr)==13){
			$panjang = "290";
		}
		if(strlen($row->polibox_nbr)==14){
			$panjang = "310";
		}
		if(strlen($row->polibox_nbr)==15){
			$panjang = "330";
		}
		if(strlen($row->polibox_nbr)==16){
			$panjang = "350";
		}
		if(strlen($row->polibox_nbr)==17){
			$panjang = "370";
		}
		if(strlen($row->polibox_nbr)==18){
			$panjang = "390";
		}
		if(strlen($row->polibox_nbr)==19){
			$panjang = "410";
		}
		if(strlen($row->polibox_nbr)==20){
			$panjang = "430";
		}

		$polibox_address_pc	= $row->polibox_address_pc; 
		if($polibox_address_pc==""){$polibox_address_pc="&nbsp;&nbsp;";}
		$polibox_address_sa	= $row->polibox_address_sa; 
		if($polibox_address_sa==""){$polibox_address_sa="&nbsp;&nbsp;";}
		$polibox_type = $row->polibox_type;
		if ($polibox_type == '')
		{
			$polibox_type = $row->polibox_type_det;
		}	

		$reset = date("ymdhist");
	@endphp
		
	<table border='1' cellspacing='0' class='fixed' cellpadding='0' style='font-family:Calibri;padding:0!important;'>
		<tr style='width:3.5cm!important'>
			<th style='text-align :center;font-size:16px!important;padding:0!important;width:3.5cm!important'>Qty Supplier</th>
			<th colspan='3' style='text-align :center;font-size:16px!important;padding:0!important;'>Part Identity</th>
			<th colspan='2' style='text-align :center;font-size:16px!important;padding:0!important;width:4cm!important'>Address</th>
			<th style='text-align :center;font-size:16px!important;padding:0!important;width:3.5cm!important'>Barcode</th>
		</tr>	
		<tr>
			<td rowspan='3' style='text-align:center;font-size:50px!important;padding:0!important;width:3.5cm!important'><b>{{$jml}}</b></td>
			<td style='font-size:16px!important;padding:0!important;'>&nbsp;Back No</td>
			<td colspan='2' style='text-align:center;font-size:50px!important;padding:0!important;width:5cm!important;white-space: nowrap!important;'><b>{{$row->polibox_backno}}</b></td>
			<td colspan='2' style='text-align:center;font-size:40px!important;padding:0!important;width:4cm!important'><b>{{$row->polibox_address_pc}}</b></td>
			<td rowspan='3' style='width:3.5cm!important'><center>{{QrCode::size(85)->generate($row->polibox_item)}}</center></td>
		</tr>	
		<tr>
			<td style='font-size:16px!important;padding:0!important;width:2.5cm!important'>&nbsp;Item Number</td>
			<td colspan='2' style='text-align:center;font-size:20px!important;padding:0!important;white-space: nowrap!important'><b>{{$row->polibox_item}}</b></td>
			<td rowspan='2' style='text-align:center;font-size:35px!important;padding:0!important;width:2cm!important'><b>{{$row->polibox_chutter}}</b></td>
			<td rowspan='2' style='text-align:center;font-size:35px!important;padding:0!important;width:2cm!important'><b>{{$row->polibox_lok_asal}}</b></td>
		</tr>		
		<tr>
			<td style='font-size:16px!important;width:2.5cm!important'>&nbsp;Description</td>
			<td colspan='2' style='text-align:center;font-size:20px!important;white-space: nowrap!important'><b>&nbsp;{{$row->polibox_deskripsi}} {{$row->polibox_type}}</b></td>
		</tr>					
	</table>
	<br><br>

	@endforeach

	</td></tr></table>
	<p class='breakhere'>

</div>
</body>
</html>

