@extends('templates.main')

@section('title', 'Summary Bon Pengambilan Barang')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="" class="table table-bordered" style="width: 30%">
                                    <tr>
                                        <td> Transaction ID </td>
                                        <td> {{ $data->kode_transaksi}} </td>
                                    </tr>
                                    <tr>
                                        <td> Produksi </td>
                                        <td> {{ $data->plant_name}} </td>
                                    </tr>
                                    <tr>
                                        <td> Area </td>
                                        <td> {{ $data->area_name}} </td>
                                    </tr>
                                    <tr>
                                        <td> Line </td>
                                        <td> {{ $data->line_prefix}} </td>
                                    </tr>
                                    <tr>
                                        <td> Pengambilan Barang </td>
                                        <td> <input type='text' value='{{$data->nama_pengambil}}' name='pengambil'> </td>
                                    </tr>
                                    <tr>
                                        <td> Tanggal Pengambilan </td>
                                        <td> {{ $data->tgl_ambil}} </td>
                                    </tr>
                                    <tr>
                                        <td> Waktu </td>
                                        <td> {{ $data->waktu}} </td>
                                    </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('update-approve-bon-whs')}}" method="POST">
                                @csrf
                                <input  name="idtrans" type="hidden" id="idtrans" size='25'  value="{{$data->id}}" readonly />
                                <div class="secondtable mt-3" id="">
                                    <table id="approveBON" class="display text-center" style="width: 100%;">
                                        <thead>
                                            <tr style="background-color: #0066CC;color:#fff;">
                                                <th class="text-white">Kode Barang</th>
                                                <th class="text-white">Nama Barang</th>
                                                <th class="text-white">Satuan</th>
                                                <th class="text-white">Jumlah</th>
                                                <th class="text-white">Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tblapprove">
                                            @foreach($qry2 as $key)
                                            <tr>
                                                <td> {{ $key->kd_brg }} </td>
                                                <td> {{ $key->nm_brg }} </td>
                                                <td> {{ $key->satuan }} </td>
                                                <td> <input type="text" value="{{$key->jml_brg}}" class="txtJumlah" name="txtJumlah[]" id="txtJumlah{{$key->id}}"> <input type="hidden" name="idbrg[]" value="{{$key->id}}"> <input type="hidden" name="changedata[]" value="{{$key->jml_brg}}"> <input type="text" style="text-align:center;" placeholder="Alasan" class="inputField txtAlasan" name="txtAlasan[]" id="txtAlasan{{$key->id}}" size="50px"></td>
                                                <td> {{ $key->ket }} </td>

                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div id="reason" class="col-md-4">
                                        <div class="form-group">
                                            <label for=""><b>Reason</b></label>
                                            <textarea class="form-control" name="txtReason" id="txtReason" rows="3"></textarea>
                                        </div>

                                        <button type="submit" name="action" id="Save" value="save" class="btn btn-success">Save</button>
                                        {{-- <button type="submit" name="action" id="Save" value="save" class="btn btn-primary">Save<button> --}}
                                        <input style="text-align:center;" name="Cancel" id="Cancel" value="Cancel" class="btn btn-danger"/>
                                    </div>

                                    {{-- punyaku --}}
                                    <button type="submit" class="btn btn-success" name="action" id="Approve" value="approve">Approve</button>
                                    {{-- <button type="submit" class="btn btn-danger" name="action" id="Reject" value="reject">Reject</button> --}}
                                    <input style="text-align:center;" name="Reject" id="Reject" value="Reject" class="btn btn-danger"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $('#approveBON').DataTable( {
        fixedHeader:{
            header:true,
            headerOffset: $('#fixed').height(),
        },
        ordering : false,
        paging: false
    } );
</script>
<script language="javascript">
    $(document).ready(function(){
        $('#reason').hide();
        $('.area').empty();
        $('.line').empty();
        $('.txtAlasan').hide();
        $("select.plant").change(function(){
            var plt = $(this).children("option:selected").val();
            getArea(plt);

        });

        $("select.area").change(function(){
            var area = $(this).children("option:selected").val();
            getLine(area);

        });
        $('#Reject').bind("click", function(){
            $('#Approve').hide();
            $('#Reject').hide();
            $('#reason').show();
        });
        $('#Cancel').bind("click", function(){
            $('#Approve').show();
            $('#Reject').show();
            $('#reason').hide();
        });

        $('.txtJumlah').keyup(function() {
                var id=$(this).attr("id").replace("txtJumlah","")
                $('#txtAlasan'+id).show();

        });
    });

</script>
@endsection
