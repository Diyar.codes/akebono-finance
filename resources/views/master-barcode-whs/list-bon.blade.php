@extends('templates.main')

@section('title', 'List BON')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-6 mb-3">
                    
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text" style="width: 5rem;">Date</div>
                        </div>  
                            <input class="form-control text-dark" type="date" id="startdate">
                            <input class="form-control text-dark" type="date" id="endDate">
                    </div>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text">Produksi</div>
                        </div>  
                        <select id="product" class="form-control text-dark product">
                            <option selected value="0">Pilih Produksi</option>
                            <option value="1">P1</option>
                            <option value="2">P2</option>
                            <option value="3">P3</option>
                            <option value="4">P4</option>
                        </select>
                    </div>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text" style="width: 5rem;">Area</div>
                        </div>  
                        <select id="area" class="form-control text-dark area">
                            <option value="0" selected>Pilih Area</option>
                            {{-- @foreach($sql2 as $key)
                            <option value="{{ $key['area'] }}">{{ $key['area'] }}</option>
                            @endforeach --}}
                        </select>
                    </div>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text" style="width: 5rem;">Line</div>
                        </div>  
                        <select id="line" class="form-control text-dark line">
                            <option value="0" selected>Pilih Line</option>
                            {{-- @foreach($sql2 as $key)
                            <option value="{{ $key['line'] }}">{{ $key['line'] }}</option>
                            @endforeach --}}
                        </select>
                    </div>

                    <div class="btn btn-list mt-2">
                        <button type="button" class="btn btn-info mb-2" id="saveProcess" onclick="searchData();"><small><i class="fas fa-search"></i></small> Search</i></button>
                        <button type="button" class="btn btn-danger mb-2" id="cancelProcess" onclick="window.location.reload();"><small><i class="fas fa-undo"></i></small> Reset</button>
                    </div>
                    
                </div>
                    <div class="row">
                        <div class="col-md-12">

                            <form action="{{ route('excel-list-bon')}}" method="POST">
                                @csrf
                                <input type="hidden" name="startdate" class="startdate">
                                <input type="hidden" name="enddate" class="enddate">
                                <input type="hidden" name="produksi" class="produksi">
                                <input type="hidden" name="area" class="area">
                                <input type="hidden" name="line" class="line">
                                <button type="submit" class="btn ripple btn-outline-success btn-sm btn-icon mb-2 counterexcel"data-toggle="tooltip" data-placement="top" title="Download Excel"><i class="si si-cloud-download"></i></button>
                            </form>

                            <form action="" method="POST">
                                @csrf
                                <div class="secondtable mt-3" id="">
                                    <table id="completeBON" class="display" style="width: 100%">
                                        <thead>
                                            <tr style="background-color: #0066CC;color:#fff;">
                                                <th class="text-white">No</th>
                                                <th class="text-white">Kode Transaksi</th>
                                                <th class="text-white">Produksi/Area/Line</th>
                                                <th class="text-white">Pengambil Barang</th>
                                                <th class="text-white">Tanggal Pesan</th>
                                                <th class="text-white">Tanggal Ambil</th>
                                                <th class="text-white">Item</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tblcomplete">
                                            <?php $no = 1; ?>
                                            @foreach($sql2 as $key)
                                            <tr>
                                                <td class="text-center"> {{ $no++ }} </td>
                                                <td class="text-center"> {{ $key['kode_transaksi'] }} </td>
                                                <td class="text-center"> {{ $key['plant_name'] }}/{{ $key['area_name'] }}/{{ $key['line_prefix'] }} </td>
                                                <td class="text-center"> {{ $key['nama_pengambil'] }} </td>
                                                <td class="text-center"> {{ date('d-m-Y',strtotime($key['create_time'])) }} </td>
                                                <td class="text-center"> {{ $key['tgl_ambil'] }} </td>
                                                <td class="text-center"> {{ $key['jml'] }} </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $('#completeBON').DataTable( {
        fixedHeader:{
            header:true,
            headerOffset: $('#fixed').height(),
        },
        ordering : false,
        paging: false
    } );

    function arealine(){
        var produksi   = document.getElementById('product').value;
       
        $.ajax({
            type : "GET",
            url : "/autocomplete-list",
            data : {

            },
            success: function(data){

            }, error : function(error){
                SW.error({
                    message: 'Data Not Defined!',
                });
            }
        })

    }

    function searchData(){
        getLoader()
        var startdate  = document.getElementById('startdate').value;
        var enddate    = document.getElementById('endDate').value;
        var produksi   = document.getElementById('product').value;
        var area       = document.getElementById('area').value;
        var line       = document.getElementById('line').value;

        $(".startdate").val(startdate);
        $(".enddate").val(enddate);
        $(".produksi").val(produksi);
        $(".area").val(area);
        $(".line").val(line);

        $.ajax({
            type : "GET",
            url: "/search-list-bon",
            data:{
                startdate : startdate,
                enddate : enddate,
                produksi : produksi,
                area : area,
                line : line,
            },
            success: function(data){
                    var tdata ='';
                    var no = 1;
                    $.each(data, function(key, value){
                            tdata += "<tr>";
                            tdata += "<td id='tLine'>"+ no++; +"</td>";
                            tdata += "<td id='tLine'>"+ value["kode_transaksi"] +"</td>";
                            tdata += `<td id='tItem'>${value["plant_name"]}/${value["area_name"]}/${value["line_prefix"]}`;
                            tdata += "<td id='tItem'>"+ value["nama_pengambil"] +"</td>";
                            tdata += "<td id='tItem'>"+ formatDate(value["create_time"]) +"</td>";
                            tdata += "<td id='tItem'>"+ value["tgl_ambil"] +"</td>";
                            tdata += "<td id='tItem'>"+ value["jml"] +"</td>";
                            tdata += "</tr>"
                    });

                    document.getElementById("tblcomplete").innerHTML = tdata;
            },
            error: function(error){
                SW.error({
                    message: 'Data Not Defined!',
                });
            }
        })
    }

    function formatDate(value) {
        let date = new Date(value);
        const day = date.toLocaleString('default', { day: '2-digit' });
        const month = date.toLocaleString('default', { month: '2-digit' });
        const year = date.toLocaleString('default', { year: 'numeric' });
        return day + '-' + month + '-' + year;
    }

    getArea = function (a) 
    {
            $.ajax({
                url : "get-data-bon",
                type: "POST",
                data : {
                    plant : a,
                    sts: 1
                },
                success: function(response)
                {
                    $('.area').html(response.option);
                    getLine(response.areaid[0]);
                    // getLine(id[0]);
                    
                }
            });
    }
    getLine = function (a) 
    {
            $.ajax({
                url : "get-data-bon",
                type: "POST",
                data : {
                    plant : a,
                    sts: 2
                },
                success: function(response)
                {
                    $('.line').html(response);                    
                }
            });
    }

    $(document).ready(function(){
        
        $("select.product").change(function(){
            var plt = $(this).children("option:selected").val();
            getArea(plt);
        });

        $("select.area").change(function(){
            var area = $(this).children("option:selected").val();
            getLine(area);
        });
    });
</script>
@endsection