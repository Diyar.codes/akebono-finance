@extends('templates.main')

@section('title', 'Detail - Transfer Intrans Pending')

@section('body')
    <style>
        .tableFixHead {
            overflow: auto;
            height: 400px;
            font-size: 12px;
        }

        .tableFixHead thead th {
            position: sticky;
            top: 0;
            z-index: 1;
        }

        /* Just common table stuff. Really. */
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            padding: 8px 16px;
        }

        th {
            background: #0066CC;
        }

    </style>
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h6 class="main-content-label mb-3">@yield('title')</h6>
                        </div>
                        <div class="col-md-12">
                            <form action="{{ route('pending-intrans-detail-proses') }}" method="post">
                                @csrf
                                <div class="tableFixHead mb-2">
                                    <table class="table table-bordered text-center w-100" id="main-table">
                                        <thead>
                                            <input type="hidden" name="no_invoice" value="{{ $no_invoice }}">
                                            <tr>
                                                <th>No</th>
                                                <th>Invoice No.</th>
                                                <th>Item Number</th>
                                                <th>Description</th>
                                                <th>Type</th>
                                                <th>UM</th>
                                                <th>Qty</th>
                                                <th>Loc From</th>
                                                <th>Loc To</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($data == 'false')
                                                <tr>
                                                    <td colspan="9" class="text-center">No data available in table</td>
                                                </tr>
                                            @else
                                                <?php $no = 1; ?>
                                                @foreach ($data as $row)
                                                    <?php
                                                    $kode_supplier = $row->int_supp;
                                                    $ship_name = $row->int_ship;
                                                    $part = $row->int_part;
                                                    $qty_lpb = $row->int_qty_rcp;
                                                    $item_desc = $row->int_part_deskripsi;
                                                    $item_um = $row->int_part_um;
                                                    $type = $row->int_part_type;

                                                    $lokasi_from = 'INTRANS';

                                                    $itemMaster = pt_mstr($part);
                                                    $lokasi_to = $itemMaster['lokasi_item'];
                                                    $deskripsiItem = $itemMaster['deskripsi1'];
                                                    ?>
                                                    <tr>
                                                        <td>{{ $no }}</td>
                                                        <td>{{ $no_invoice }}</td>
                                                        <td>{{ $part }}</td>
                                                        <td>{{ $deskripsiItem }}</td>
                                                        <td>{{ $type }}</td>
                                                        <td>{{ $item_um }}</td>
                                                        <td>{{ number_format($qty_lpb, 0) }}</td>
                                                        <td>{{ $lokasi_from }}</td>
                                                        <td>{{ $lokasi_to }}</td>
                                                    </tr>
                                                    <?php $no++; ?>
                                                @endforeach
                                                <input type="hidden" name="no" value="{{ $no - 1 }}">
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                <a href="/pending-intrans" class="btn btn-sm btn-danger"> <span class="fas fa-undo"></span>
                                    Back</a>
                                <button type="submit" class="btn btn-sm btn-success"> <span class="fas fa-check"></span>
                                    Proses</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
