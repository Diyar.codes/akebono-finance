@extends('templates.main')

@section('title', 'Transfer Intrans Pending')

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    Periode Transaction :
                                    <select name="month" id="month" class="form-control-sm">
                                        <option value="0" selected>Month</option>
                                        @for ($i = 1; $i <= 12; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                    <select name="year" id="year" class="form-control-sm">
                                        <option value="0" selected>Year</option>
                                        @for ($i = 2014; $i <= date('Y') + 2; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-sm-4 mb-2">
                                    <button type="submit" class="btn btn-sm btn-info mb-2" width="30px" id="searchBlDate"><i class="fas fa-search"></i> Search</button>
                                    <button type="button" class="btn btn-sm btn-danger mb-2" width="30px" onclick="window.location.reload();"><i class="fas fa-undo"></i> Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive mb-2">
                                <table class="table table-striped table-bordered text-center w-100" id="main-table">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Invoice No.</th>
                                            <th scope="col">BL No.</th>
                                            <th scope="col">BL Date</th>
                                            <th scope="col">Ship</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="dataBody">
                                        <?php $no = 1; ?>
                                        @foreach ($data as $row)
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->int_inv}}</td>
                                                <td>{{$row->int_no_bl}}</td>
                                                <td>{{$row->int_etd}}</td>
                                                <td>{{$row->int_ship}}</td>
                                                <td><a class="btn btn-sm btn-warning" data-toggle="tooltip" title="detail" href="/pending-intrans-detail/{{base64_encode($row->int_inv)}}/{{base64_encode($row->int_supp)}}/{{base64_encode($row->int_no_bl)}}"><span class="fas fa-eye"></span></a></td>
                                            </tr>
                                        <?php $no++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
