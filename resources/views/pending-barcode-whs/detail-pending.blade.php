@extends('templates.main')

@section('title', 'Detail Barcode WHS Pending (Schedule Transfer Otomatis Jam 02, 06, 08, 10, 12, 14, 16, 18, 20, 23)')

@section('styles')
    <style>
        table.dataTable thead th, table.dataTable thead td {
            border: 1px solid #fff;
        }   
    </style>
@endsection

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                            <div class="secondtable mt-3" id="">
                                <table id="dataSecondTable" class="display" style="width: 100%;">
                                    <thead>
                                        <tr style="background-color: #0066CC;color:#fff;">
                                            <th>No</th>
                                            <th>Trans ID</th>
                                            <th>Item Number</th>
                                            <th>Buyer</th>
                                            <th>SNP</th>
                                            <th>Tanggal</th>
                                            <th>Loc From</th>
                                            <th>Loc To</th>
                                            <th>Note</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tblseconded">
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($result as $row)
                                            @php
                                                $ptmstr = pt_mstr($row->polibox_item_nbr);
                                            @endphp
                                            <tr>
                                                <td align='center'>{{$no}}</td>
                                                <td align='center'>{{$row->trans_id}}</td>
                                                <td align='center'>{{$row->polibox_item_nbr}}</td>
                                                <td align='center'>{{$ptmstr['buyer_planner']}}</td>
                                                <td align='center'>{{number_format($row->snp,0)}}</td>
                                                <td align='center'>{{date("d F Y H:i",strtotime($row->creadate))}}</td>
                                                <td align='center'>{{$row->locfrom}}</td>
                                                <td align='center'>{{$row->locto}}</td>
                                                <td align='center'>{{$row->note}}</td>
                                            </tr>
                                        @php
                                            $no += 1;
                                        @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-lg-2 mt-2 mt-md-0">
                                        <a href="{{route('pending-barcode-whs')}}" class="btn btn-danger" style="width: 100%"><i class="fa fa-arrow-left"></i> Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
