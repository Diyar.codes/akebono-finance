@extends('templates.main')

@section('title', 'Barcode WHS Pending (Schedule Transfer Otomatis Jam 02, 06, 08, 10, 12, 14, 16, 18, 20, 23)')

@section('styles')
    <style>
        table.dataTable thead th, table.dataTable thead td {
            border: 1px solid #fff;
        }   
    </style>
@endsection

@section('body')
    <div class="row row-sm my-md-2 mt-5">
        <div class="col-12">
            <div class="card custom-card overflow-hidden">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <h6 class="main-content-label mb-3">@yield('title')</h6>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-2">
                                    Periode Transaction:
                                </div>
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" style="width:5rem" class="form-control-sm">
                                        <option value="" selected disabled>Month</option>
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                    <select name="tahun" id="tahun" style="width:8rem" class="form-control-sm">
                                        <option value="" selected disabled>Year</option>
                                        <?php
                                        $year=date("Y");
                                        $awal=$year-4;
                                        for($awal=$year-4;$awal<=$year;$awal++)
                                        {
                                            echo "<option value='".$awal."'>".$awal."</option> ";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-lg-2 mt-2 mt-md-0">
                                    <button type="button" class="btn btn-info btn-block mb-2" id="search"><i class="fas fa-search"></i> Search</button>
                                </div>
                                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 mt-lg-2 mt-2 mt-md-0">
                                    <button type="button" class="btn btn-danger btn-block btn-md" id="reset"><i class="fas fa-undo"></i> Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="secondtable mt-3" id="">
                                <table id="dataSecondTable" class="display" style="width: 100%;">
                                    <thead>
                                        <tr style="background-color: #0066CC;color:#fff;" align="center">
                                            <th id='row1' width='' colspan='11'><div class="periodeblnthn">Periode</div> </th>
                                        </tr>
                                        <tr style="background-color: #0066CC;color:#fff;">
                                            <th>No</th>
                                            <th>Item Number</th>
                                            <th>Deskripsi</th>
                                            <th>Type</th>
                                            <th>Buyer</th>
                                            <th>SNP</th>
                                            <th>Total PENDING</th>
                                            <th>QAD Stock</th>
                                            <th>Loc From</th>
                                            <th>Loc To</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tblseconded">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
