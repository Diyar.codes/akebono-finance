<?php 
	if (!function_exists('xmlView'))
		include "xml_parse.php";
		
	function getRCNo($db,$tempTable, $PO,$PS)
	{
		$xml =
		"<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wsas='http://ws.iris.co.id/wsatdw'>
		   <soapenv:Header/>
		   <soapenv:Body>
			  <wsas:supplier_getRCNo>
				<wsas:inpPO>".$PO."</wsas:inpPO>
				<wsas:inpPS>".$PS."</wsas:inpPS>
			  </wsas:supplier_getRCNo>
		   </soapenv:Body>
		</soapenv:Envelope>";

		return xmlView($db,$xml,$tempTable,null);
	}
	
	function getOpenPO($db,$tempTable, $PO,$POLine)
	{
		//kosongin nilai inpLinePO jika ingin dapetin semua qtyopen untuk setiap item di PO tersebut.
		//jangan pake item number karena 1 PO bisa ada 2item number (item return)
		$xml =
		"<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wsas='http://ws.iris.co.id/wsatdw'>
		   <soapenv:Header/>
		   <soapenv:Body>
			  <wsas:supplier_getOpenPO>
				<wsas:inpPO>".$PO."</wsas:inpPO>
				<wsas:inpLinePO>".$POLine."</wsas:inpLinePO>
			  </wsas:supplier_getOpenPO>
		   </soapenv:Body>
		</soapenv:Envelope>";

		return xmlView($db,$xml,$tempTable,null);
	}
?>