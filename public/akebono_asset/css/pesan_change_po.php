<?php
include "./class/connect_sql.class.php";
	
function sendemail($to,$subject,$message)
{	$nama="Supplier Portal";
	$from="helpdesk@akebono-astra.co.id";
	require_once("./phpmailer/class.phpmailer.php");
	$mail = new PHPMailer();
	$mail->IsSMTP();                                      // set mailer to use SMTP
	$mail->From = $from;
	$mail->FromName = $nama;
	$mail->AddAddress($to);
	
	//$mail->AddReplyTo("it@akebono-astra.co.id");
	$mail->WordWrap = 70;                                 // set word wrap to 50 characters
	$mail->IsHTML(true);                                  // set email format to HTML

	$mail->Subject = $subject;
	$mail->Body    = $message;

	if(!$mail->Send())
	{
	   echo "Message could not be sent. <p>";
	   echo "Mailer Error: " . $mail->ErrorInfo;
	}

}	
	$jml_data = $_POST['txt_hitung'];
	$po_number = $_POST['txtPo'];
	
	$bulan=date("m");
	$tahun = date("Y");
	//untuk mendapatkan penomeran transaksi
	$tampil_no_memo = mssql_fetch_array(mssql_query("select isnull(max(SUBSTRING(transaksi_id,1,3)),0)+1 as nomor_transaksi from PO_change_type 
													 where year(effdate)='".$tahun."'"));
					
	$memo_No = str_pad($tampil_no_memo['nomor_transaksi'], 3, "0", STR_PAD_LEFT);
	$transaksi_id = $memo_No."/AAI/".$bulan."/".$tahun; 
	
	for ($x = 0; $x<=$jml_data; $x++)
	{
		$no_po		 = $_POST["txt_po".$x];
		$pilih		 = $_POST["chkCheck".$x];
		$line		 = $_POST["line".$x];
		$item		 = $_POST["txt_item".$x];
		$desc		 = $_POST["txt_desc".$x];
		$type_po	 = $_POST["txt_type_Po".$x];
		$type_po_rev = $_POST["txt_type_Po_rev".$x];
		$approval_name = $_POST["txt_approval_name".$x];
		$approval_email = $_POST["txt_approval_email".$x];
		
		if ($pilih == 'OK')
		{
			/*echo "insert into PO_change_type (no_po,line_po,type_po,approval_po,po_deskripsi,effdate,type_po_rev,po_item) 
								  values('".$no_po."','".$line."','".$type_po."','Yani','".$desc."',getDate(),'".$type_po_rev."',
							      '".$item."')";*/
			//simpan data 
			$query = mssql_query("insert into PO_change_type (transaksi_id,no_po,line_po,type_po,approval_po,approval_email,po_deskripsi,effdate,type_po_rev,po_item) 
								  values('".$transaksi_id."','".$no_po."','".$line."','".$type_po."','".$approval_name."','".$approval_email."','".$desc."',getDate(),'".$type_po_rev."',
							      '".$item."')");
					
		}
	}
	sentEmail($transaksi_id,$approval_email,$approval_name);
	
?>	

<?php
function sentEmail($transaksi_id,$approval_email,$approval_name)
{	
		
		include "./class/connect_sql.class.php";
		//include "./class/connect_progress.class.php";
		include "function.php";
		
		//untuk menampilkan data perubahan
		$tampil_data_rubah = "select no_po,line_po,po_item,po_deskripsi,type_po,type_po_rev,effdate from PO_change_type  where transaksi_id ='".$transaksi_id."'";
		$rs_rubah_data = mssql_query($tampil_data_rubah);
						
		while ($row = mssql_fetch_assoc($rs_rubah_data))
		{
			$no_po = $row['no_po'];		
			$line_po = $row['line_po'];		
			$item = $row['po_item'];		
			$deskripsi = $row['po_deskripsi'];		
			$type_po = $row['type_po'];		
			$type_po_revisi = $row['type_po_rev'];		
			$effdate = $row['effdate'];			
			
			$header1 = "<table width='100%' border='1' cellspacing = '0' cellpadding='0'> <tr align='center'> <td width='10%'> <b> PO Number </b> </td><td width='10%'> <b> Line </b> </td> <td width='20%'> <b> Item Number </b> </td> <td width='50%'> <b> Description </b> </td> <td width='15%'> <b> Type PO</b> </td> <td width='5%'> Revisi Type PO </td> </tr>";
		
			$data = $data."<tr><td align='center' width='10%'> ".$no_po." </td> 
								<td align='left' width='20%'> ".$line_po." </td> 
								<td align='left' width='50%'> ".$desc." </td> 
								<td align='center' width='15%'> ".$type_po." </td>
								<td align='center' width='5%'> ".$type_po_revisi." </td>
								</tr>";
			$end = "</table>";
			$pesan = $header1.$data.$end;
		}
		
		$to = $approval_email;
		$Username = $approval_name;		
		
		$subject = "Change Type Approval ".$transID;
		$address = "http://supplier.akebono-astra.co.id/cek_verifikasichange.php?".paramEncrypt("username=".$Username."&transID=".$transID."")."";
		
		$message = "Dear ".ucfirst($Username)."-San, <br> <br>
					
					You have request from ".ucfirst($_SESSION["username"])."-San to approve the new change type of Purchase Order that have been created on ".Date('d-M-Y H:m')." with Transaction Number 
					<font size='4' color='blue'> <b> ".$transID." </b> </font> and following item(s) below : <br> <br>
					
					".$pesan." <br> <br>
					
					Please follow this link to respond as soon as possible : <br>
					<font size='5' color='blue'> <i> <u> <a href='".$address."'> Click Here For Approval </a> </u> </i> </font> <br> <br>
					
					If you need further information, please login to <a href='http://supplier.akebono-astra.co.id'> <font color='blue' size='3'> <b> <i> <u>Supplier Portal</u> </i> </b> </font> </a> or contact Information Technology Department of AAIJ on extension 525. <br>
					Thank you. <br> <br> <br>
					
					Regards, <br> <br>

					Admin AAIJ Supplier Portal";
			
		$header = "From:".$_SESSION["email"]." \r\n";
		//$header .= "Cc:Roberd.W@akebono-astra.co.id \r\n";
		$header .= "MIME-Version: 1.0\r\n";
		$header .= "Content-type: text/html\r\n";
		
		//mail($to,$subject,$message,$header);
		sendemail($to,$subject,$message);

}
?>