$(()=> {
    // gate data
    main_table()

    // view data
    function main_table(filter_id_supp = '', filter_dn_number = '', filter_moon = '', filter_year = '') {
        // getLoader
        getLoader()

        $('#main-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            searching: false,
            paging: true,
            ordering : false,
            // scrollY: "80vh",
            // scrollX: true,
            // scrollCollapse: true,
            // fixedColumns: {
            //     leftColumns: 0,
            //     rightColumns: 0
            // },
            ajax: {
                url: `${BASEURL}print-delivery-slip`,
                data: {
                    filter_dn_number: filter_dn_number,
                    filter_id_supp:filter_id_supp,
                    filter_moon: filter_moon,
                    filter_year: filter_year,
                }
            },
            columns:[
                {
                    data: "no",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'no',
                    name: 'no',
                },
                {
                    data: 'ct_ad_name',
                    name: 'ct_ad_name',
                },
                {
                    data: 'tanggal',
                    name: 'tanggal',
                },
                {
                    data: 'cycle',
                    name: 'cycle',
                },
                {
                    data: 'pengirim',
                    name: 'pengirim',
                },
                {
                    data: 'kendaraan',
                    name: 'kendaraan',
                },
                {
                    data: 'app_supplier',
                    name: 'app_supplier',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
            columnDefs: [
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 0
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 1
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 2
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 3
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 4
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 5
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 6
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 7
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 8
                },
            ],
            rowCallback: function(row, data, index){
                if (data['app_supplier'] == 'Confirmed') {
                    $(row).css('backgroundColor', '#FFFF99');
                }
            },
        })
    }

    // get supplier
    $(document).on('click', '#seacrhSOAPBusinesRelationModal', function(){
        $('#get-supplier').DataTable().clear().destroy()
        getSupplier();
    })

    // view data supplier
    function getSupplier() {
        $('#get-supplier').DataTable({
            // processing: true, koment
            responsive: true,
            // serverSide: true, koment
            paging: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'ct_vd_addr',
                },
                {
                    data: 'ct_ad_name',
                    name: 'ct_ad_name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // filter
    $('#filter').click(function() {
        var filter_id_supp = $('#supp_id').val()
        var filter_dn_number = $('#filter_dn_number').val()
        var filter_moon = $('#filter_moon').val()
        var filter_year = $('#filter_year').val()

        if(filter_id_supp != '' || filter_dn_number != '' || filter_moon != '' || filter_year != '') {
            $('#main-table').DataTable().clear().destroy()
            main_table(filter_id_supp, filter_dn_number, filter_moon, filter_year)
        } else {
            $('#main-table').DataTable().clear().destroy()
            main_table()
        }
    })

    // reset
    $('#reset').click(function() {
        $('#supp_id').val('')
        $('#supp_name').val('')
        $('#filter_dn_number').val('')
        $('#filter_moon').val('')
        $('#filter_year').val('')
        $('#main-table').DataTable().clear().destroy()
        main_table()
    })

    // select modal
    $(document).on('click', '#selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#supp_id').val(codebusiness)
        $('#supp_name').val(namebusiness)
        $('#getSupplierTarget').click()
    })

    // reject
    $(document).on('click', '#rejectDtlCoverLetter', function(e) {
        e.preventDefault()

        // getLoader
        getLoader()

        Swal.fire({
            title: 'Reject cover letter?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!'
        }).then((result) => {
            if (result.value) {
                var form = new FormData();
                form.append("_method", "PUT");

                var settings = {
                    "url": `${BASEURL}reject-delivery-slip/` + $(this).data('id'),
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Accept": "application/json"
                    },
                    "processData": false,
                    "mimeType": "multipart/form-data",
                    "contentType": false,
                    "data": form,
                };

                $.ajax(settings).done(function(response) {
                    let data = JSON.parse(response);

                    SW.success({
                        message: data.message
                    });

                    location.reload();
                });
            }
        })
    })
})
