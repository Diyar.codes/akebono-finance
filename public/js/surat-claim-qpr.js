$(()=> {
    // get data
    fill_datatable()

    // view_data {
    function fill_datatable(code = '', name = '', from_date = '', to_date = '') {
        // getLoader
        getLoader()

        $.ajax({
            type: "GET",
            url: "/search-surat-claim-qpr",
            data:{
                code : code,
                name : name,
                from_date : from_date,
                to_date : to_date,
            },
            success: function (data) {
                $('.secondtable').find('.tblseconded').html(data.tablenya);
                $('#dataSecondTable').DataTable({
                    processing: false,
                    paging: false,
                    scrollY: "80vh",
                    scrollX: true,
                    scrollCollapse: true,
                    fixedColumns: {
                        leftColumns: 0,
                        rightColumns: 0
                    },
                    searching: false,
                    ordering : false,
                } );
            },
            error: function(error) {
                SW.error({
                    message: 'Data is not Defined!!',
                })
            }
        })
    }

    $(document).on('click', '#seacrhSOAPBusinesRelationModal', function(){
        $('#get-supplier').DataTable().clear().destroy()
        getSupplier();
    })

    // get data supplier
    function getSupplier() {
        $('#get-supplier').DataTable({
            // processing: true,
            responsive: true,
            // serverSide: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'supplier id',
                },
                {
                    data: 'ct_ad_name',
                    name: 'supplier name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // select modal
    $(document).on('click', '#selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        $('#name').val(namebusiness)
        $('#modaldemo8part2').click()
    })

    // search
    $(document).on('click', '#search', function() {
        var code = $('#code').val()
        var name = $('#name').val()
        var fdate = $('#date-from').val()
        var tdate = $('#date-to').val()

        if(fdate) {
            from_date = fdate + 'T00:00:00'
        } else {
            from_date = fdate
        }

        if(tdate) {
            to_date = tdate + 'T23:59:59'
        } else {
            to_date = tdate
        }

        if(code == '') {
            SW.error({
                message: 'the supplier cannot be empty!',
            });
        } else if(from_date == '') {
            SW.error({
                message: 'the destination date must exist!',
            });
        } else if(to_date == '') {
            SW.error({
                message: 'date of must exist!',
            })
        } else if(from_date != '' && to_date == '') {
            SW.error({
                message: 'the destination date must exist!',
            });
        } else if (from_date == '' && to_date != '') {
            SW.error({
                message: 'date of must exist!',
            })
        } else if(code != '' && name != '' && from_date != '' && to_date != '') {
            $('#dataSecondTable').DataTable().clear().destroy()
            fill_datatable(code, name, from_date, to_date)
        } else {
            $('#dataSecondTable').DataTable().clear().destroy()
            fill_datatable()
        }
    })

     // reset
    $('#reset').click(function() {
        $('#code').val('')
        $('#name').val('')
        $('#date-from').val('')
        $('#date-to').val('')
        $('.t-automatical').css('display', 'none')
        $('#dataSecondTable').DataTable().clear().destroy()
        fill_datatable()
    })

    $('#search').bind('click', function(event) {
        var code = $('#code').val()
        var name = $('#name').val()
        var fdate = $('#date-from').val()
        var tdate = $('#date-to').val()

        if(fdate) {
            from_date = fdate + 'T00:00:00'
        } else {
            from_date = fdate
        }

        if(tdate) {
            to_date = tdate + 'T23:59:59'
        } else {
            to_date = tdate
        }

        if(!code || !name || !from_date || !to_date) {
            $('.t-automatical').css('display', 'none')
        } else {
            $('.t-automatical').css('display', 'flex')
        }
    })
})

// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
