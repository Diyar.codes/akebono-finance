// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

$(()=> {
    $(document).on('click', '#seacrhSOAPBusinesRelation', function(){
        $('#get-supplier').DataTable().clear().destroy()
        $('#getSupplierTarget').modal('show');
        getSupplier();
    });

    $(document).on('click', '#get-supplier #selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        // $('#code').val(codebusiness)
        $('#code').val(codebusiness)
        $('#name').val(namebusiness)
        $('#getSupplierTarget').click()
    });

    function getSupplier() {
        // getLoader
        getLoader()

        // view data supplier
        $('#get-supplier').DataTable({
            // processing: true,
            responsive: true,
            // serverSide: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'supplier id',
                },
                {
                    data: 'ct_ad_name',
                    name: 'supplier name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // call search report delivery slip
    searchSummaryDeliveryNote()

    // function search report delivery slip
    function searchSummaryDeliveryNote(code = '', moon = '', year = '', app = ''){
        // getLoader
        getLoader()

        $.ajax({
            url:`${BASEURL}search-summary-delivery_notes`,
            type:"GET",
            data:{
                code: code,
                moon: moon,
                year: year,
                app: app,
            },
            success: function (data) {
                $('.atas').html(data['atas']);
                $('.bawah').html(data['bawah']);
                $('#main-table').DataTable({
                    scrollY: "60vh",
                    scrollX: true,
                    scrollCollapse: true,
                    fixedColumns: {
                        leftColumns: 0,
                        rightColumns: 0
                    },
                    searching: false,
                    ordering : false,
                    paging: false
                });
            }
        });
    }

    // search data
    $(document).on('click', '#search', function() {
        var code = $('#code').val()
        var app = $('#app').val()
        var moon = $('#moon').val()
        var year = $('#year').val()

        if(code != '' && moon != '' && year != '' && app != '') {
            $('#codeExcel').val(code)
            $('#appExcel').val(app)
            $('#moonExcel').val(moon)
            $('#yearExcel').val(year)

            $('.t-automatical').css('display', 'flex')

            $('#main-table').DataTable().clear().destroy()
            searchSummaryDeliveryNote(code, moon, year, app)
        } else if(code != '' && moon != '' && year != '') {
            $('#codeExcel').val(code)
            $('#moonExcel').val(moon)
            $('#yearExcel').val(year)

            $('.t-automatical').css('display', 'flex')

            $('#main-table').DataTable().clear().destroy()
            searchSummaryDeliveryNote(code, moon, year)
        } else if(code == '') {
            SW.warning({
                message: 'Supplier Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else if(app == '') {
            SW.warning({
                message: 'Status Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else if(moon == '') {
            SW.warning({
                message: 'Month Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else if(year == '') {
            SW.warning({
                message: 'Year Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else {
            $('#main-table').DataTable().clear().destroy()
            searchSummaryDeliveryNote()
        }
    })

    // reset
    $('#reset').click(function() {
        $('.t-automatical').css('display', 'none')

        $('#moon').val('')
        $('#year').val('')
        $('#app').val('')
        $('#code').val('')
        $('#name').val('')
        $('#moonExcel').val('')
        $('#yearExcel').val('')
        $('#appExcel').val('')
        $('#codeExcel').val('')
        $('#main-table').DataTable().clear().destroy()
        searchSummaryDeliveryNote()
    })
})

