// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
$('#tabelnya').DataTable({
    scrollY: "400px",
    scrollX: true,
    scrollCollapse: true,
    paging: false,
    ordering:false,
    fixedColumns: true
});

function cekdata(urut) {

    var qty_ship = $('[id^="qty"]').map(function() { return $(this).val(); }).get();
    var qty_sisa = $('[id^="qty_sisa"]').map(function() { return $(this).val(); }).get();
    var qty_old  = $('[id^="qty_old"]').map(function() { return $(this).val(); }).get();
    var jml_row     = document.getElementById("jml_row").value;
    var hitung   = urut - 1;
    // alert(jml_row);
    // if (Number(ordertb[hitung]) > Number(qty_open[hitung])) {
    //     SW.error({
    //         message: 'Over Qty Order !!',
    //     });
    //     // document.getElementById("ordertb["+hitung+"]").focus();
    //     document.getElementById("ordertb[" + Number(urut) + "]").value = '';
    // }
}

function save(){
    var qty         = $('[id^="qty"]').map(function () { return $(this).val(); }).get();
    var lot_no      = $('[id^="lot_no"]').map(function () { return $(this).val(); }).get();
    var item_number = $('[id^="item_number"]').map(function () { return $(this).val(); }).get();
    var line        = $('[id^="line"]').map(function () { return $(this).val(); }).get();
    var tr_id       = document.getElementById("tr_id").value;
    var po          = document.getElementById("po").value;
    var dn          = document.getElementById("dn").value;
    var tgl         = document.getElementById("tgl").value;
    var packingslip = document.getElementById("ps").value;
    var jml_row     = document.getElementById("jml_row").value;

    $.ajax({
        type: "GET",
        url: "/edit-act-delivery-order-dn",
        data:{
            qty : qty,
            lot_no:lot_no,
            tr_id : tr_id,
            po : po,
            dn:dn,
            packingslip:packingslip,
            item_number:item_number,
            line:line,
            tgl:tgl,
            jml_row:jml_row,
        },
        success: function (data) {
            SW.success({
                message: 'Edit Delivery Order Successfully',
                
            });
            setTimeout(function(){ 
                location.reload(); 
            }, 1000);
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });

}

function delete_ship(hitung){
    var item_number  = $('[id^="item_number"]').map(function () { return $(this).val(); }).get();
    var line         = $('[id^="line"]').map(function () { return $(this).val(); }).get();
    var tr_id        = document.getElementById("tr_id").value;
    var po           = document.getElementById("po").value;
    var tampung_item = item_number[hitung];
    var tampung_line = line[hitung];

    Swal.fire({
        title: 'Are you want to Delete ? ',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "GET",
                url: "/delete-kosong-delivery-order-po",
                data:{
                    tampung_item : tampung_item,
                    tr_id : tr_id,
                    po : po,
                    tampung_line:tampung_line
                },
                success: function (data) {
                    SW.success({
                        message: 'Delete Item Delivery Order Successfully',
                        
                    });
                    setTimeout(function(){ 
                        location.reload(); 
                    }, 1000);
                },
                error: function(error) {
                    alert('Data is not Defined!!!');
                }
            });
        }
    })
}