$(()=> {
    fetch_customer_data()

    function fetch_customer_data(query = '') {
        getLoader()

        $.ajax({
            type: "GET",
            url: "/data-second-table-master-cycle",
            data:{
                query : query,
            },
            success: function (data) {
                $('.secondtable').show();
                $('.secondtable').find('.tblsecondd').html(data);
                $('#dataSecondTable').DataTable( {
                    responsive: false,
                    paging: false,
                    searching: false,
                    ordering : false,
                    scrollY: "70vh",
                    scrollX: true,
                    scrollCollapse: true,
                    fixedColumns: {
                        leftColumns: 0,
                        rightColumns: 0
                    },
                } );
            },
            error: function(error) {
                SW.error({
                    message: 'Data is not Defined!!',
                });
            }
        });
    }

    $(document).on('click', '#search', function() {
        var query = $('#input-search').val()

        if(query != '') {
            $('#dataSecondTable').DataTable().destroy()
            fetch_customer_data(query)
        } else {
            $('#dataSecondTable').DataTable().destroy()
            fetch_customer_data()
        }
    })

    $('#input-search').bind('click keyup', function(event) {
        if(!$('#input-search').val()) {
            $('.t-automatical').css('display', 'none')
        }
    })

    $('#search').bind('click', function(event) {
        if(!$('#input-search').val()) {
            $('.t-automatical').css('display', 'none')
        } else {
            $('.t-automatical').css('display', 'flex')
        }
    })

    // when click btn search to modal get supplier
    $(document).on('click', '#seacrhSOAPBusinesRelation', function(){
        $('#get-supplier').DataTable().clear().destroy()
        getSupplier();
    })

    // get supplier modal
    function getSupplier() {
        // getLoader
        getLoader()

        // view data supplier
        $('#get-supplier').DataTable({
            // processing: true,
            responsive: true,
            // serverSide: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'supplier id',
                },
                {
                    data: 'ct_ad_name',
                    name: 'supplier name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // select modal
    $(document).on('click', '#get-supplier #selectbusiness', function() {
        // var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        // $('#code').val(codebusiness)
        // $('#input-search-supplier_id').val(codebusiness)
        $('#input-search').val(namebusiness)
        $('#getSupplierTarget').click()
    })

    // reset
    $('#reset').click(function() {
        $('#input-search').val('')
        $('#dataSecondTable').DataTable().destroy()
        fetch_customer_data()
    })
})

// update
$(document).on('click', '#updateMasterCycle', function(e) {
    e.preventDefault()

    Swal.fire({
        title: 'update start time & end time ? ',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.value) {
            var form = new FormData();

            var start_time = []
            var end_time = []

            $('input[name="start_time[]"]').each(function(){
                start_time.push(this.value);
            });

            $('input[name="end_time[]"]').each(function(){
                end_time.push(this.value);
            });

            var lengthin = start_time.length - 1

            for (var i = 0; i <= lengthin; i++) {
                form.append("_method", "PUT");
                form.append("cycle_start" + "[" + i + "]", start_time[i])
                form.append("cycle_end" + "[" + i + "]", end_time[i])
            }

            var inputSearch = $('#input-search').val()

            if(inputSearch != '') {
                var settings = {
                    "url": `${BASEURL}master-cycles/` + inputSearch,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                      "Accept": "application/json"
                    },
                    "processData": false,
                    "mimeType": "multipart/form-data",
                    "contentType": false,
                    "data": form,
                    error: function(jqXHR, textStatus, errorThrown) {
                        var err = JSON.parse(jqXHR.status)
                        var errText = JSON.parse(jqXHR.responseText)
                        if (err == 404) {
                            SW.error({
                                message: errText.message
                            });
                        }

                        if (err == 405) {
                            SW.error({
                                message: 'search for keywords first'
                            });
                        }
                    }
                };
            } else {
                if(inputSearch == '') {
                    SW.error({
                        message: 'search data does not exist yet'
                    });
                    return
                }
            }

            $.ajax(settings).done(function(response) {
                let data = JSON.parse(response);

                SW.success({
                    message: data.message
                });

                location.reload();
            });
        }
    })
})

// function addRow() {
//     var tr = `
//             <tr class="rowAppend">
//             <td><a href="#" class="btn btn-danger remove" id="remove"><i class="fas fa-trash"></i></a></td>
//             <td><input type="text" class="form-control cek" name="kode_supplier[]" autocomplete="off"></td>
//             <td><input type="text" class="form-control cek" name="nama_supplier[]" autocomplete="off"></td>
//             <td><input type="text" class="form-control cek" name="cycle[]" autocomplete="off"></td>
//             <td><input type="time" class="form-control cek" name="start_time[]"></td>
//             <td><input type="time" class="form-control cek" name="end_time[]"></td>
//             </tr>
//             `
//     $('tbody').append(tr)
// }

// $('#addRow').on('click', function() {
//     addRow()
// })

// $(document).on('click', '.remove', function() {
//     var last = $('tbody tr').length
//     if(last == 1) {
//         SW.error({
//             message: 'the last line cannot be deleted!'
//         });
//     } else {
//         $(this).parent().parent().remove()
//     }
// })

// $(document).on('click', '#createMasterCycle', function(e) {
//     e.preventDefault()

//     var form = new FormData();

//     var kode_supplier = []
//     var nama_supplier = []
//     var cycle = []
//     var start_time = []
//     var end_time = []

//     $('input[name="kode_supplier[]"]').each(function(){
//         kode_supplier.push(this.value);
//     });

//     $('input[name="nama_supplier[]"]').each(function(){
//         nama_supplier.push(this.value);
//     });

//     $('input[name="cycle[]"]').each(function(){
//         cycle.push(this.value);
//     });

//     $('input[name="start_time[]"]').each(function(){
//         start_time.push(this.value);
//     });

//     $('input[name="end_time[]"]').each(function(){
//         end_time.push(this.value);
//     });

//     var lengthin = kode_supplier.length - 1

//     var emptyInput = $(".cek").filter(function(){
//         return $.trim($(this).val()) == '';
//     }).length

//     if(emptyInput) {
//         SW.error({
//             message: 'there is still an empty column!'
//         });
//     } else {
//         for (var i = 0; i <= lengthin; i++) {
//             form.append("kode_supplier" + "[" + i + "]", kode_supplier[i])
//             form.append("nama_supplier" + "[" + i + "]", nama_supplier[i])
//             form.append("cycle" + "[" + i + "]", cycle[i])
//             form.append("start_time" + "[" + i + "]", start_time[i])
//             form.append("end_time" + "[" + i + "]", end_time[i])
//         }
//     }

//     var settings = {
//         url: `${BASEURL}api/master-cycleapis`,
//         method: "POST",
//         timeout: 0,
//         headers: {
//             Accept: "application/json"
//         },
//         processData: false,
//         mimeType: "multipart/form-data",
//         contentType: false,
//         data: form,
//     };

//     $.ajax(settings).done(function(response) {
//         let data = JSON.parse(response);

//         $('input[name="kode_supplier[]"]').val('')
//         $('input[name="nama_supplier[]"]').val('')
//         $('input[name="cycle[]"]').val('')
//         $('input[name="start_time[]"]').val('')
//         $('input[name="end_time[]"]').val('')

//         $(".rowAppend").empty()

//         SW.success({
//             message: data.message
//         });
//     });
// })
