// change thead th datatable color
$('#tablenya').DataTable({
    responsive: true,
    language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
    }
});
$(".table-bordered thead th").css("background-color", "#0066CC");
$("#tablenya thead th").css("color", "#fff");
function search(){
    getLoader();
    var bulan = $('#filter_moon').val();
    var tahun = $('#filter_year').val();
    $.ajax({
        type: "GET",
        url: "/search-delivery-order-po",
        data:{
            bulan : bulan,
            tahun : tahun,
        },
        success: function (data) {
            $('#tablenya').DataTable().destroy();
            // $('#tablenya tbody tr').remove();
            $('.secondtable').show();
            $('.secondtable').find('#tblsecond').html(data);
            $('#tablenya').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}

function reset(){
    getLoader();
    var bulan = '0';
    var tahun = '0';
   $.ajax({
        type: "GET",
        url: "/search-delivery-order-po",
        data:{
            bulan : bulan,
            tahun : tahun,
        },
        success: function (data) {
            $('#tablenya').DataTable().destroy();
            // $('#tablenya tbody tr').remove();
            $('.secondtable').show();
            $('.secondtable').find('#tblsecond').html(data);
            $('#tablenya').DataTable({
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}