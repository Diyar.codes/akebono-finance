$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

function editDn(){
    var row = $('#jml_row').val();
    var dnd_tr_id = $('#dnd_tr_id').val();
    var dnd_part = $('[id^="dnd_part"]').map(function () { return $(this).val(); }).get();
    var order = $('[id^="order"]').map(function () { return $(this).val(); }).get();
    var dnd_line = $('[id^="dnd_line"]').map(function () { return $(this).val(); }).get();
    var dnd_qty_order = $('[id^="dnd_qty_order"]').map(function () { return $(this).val(); }).get();
    // // alert(act_qty);
    $.ajax({
        url:`${BASEURL}update-dn`,
        type:"GET",
        data:{
            // "_token": $('#token').val(),
            row: row,dnd_tr_id: dnd_tr_id,dnd_part: dnd_part,order:order,dnd_line:dnd_line,
            dnd_qty_order:dnd_qty_order
        },
        success:function(hasil){
            // var url = `${BASEURL}sp-supp-view`;
            console.log(hasil);
            var conv = JSON.parse(hasil);
            if(conv['message'] == 1){
                SW.success({
                    message: 'Update Successfully !',
                });
                var url = `${BASEURL}delivery-notes`;
                    
                setTimeout(function(){ 
                    window.location.href = url;
                }, 1200);
                // window.location.href = url;
            }
            
            
        }
    })
}

function back(){
    var url = `${BASEURL}delivery-notes`;
    window.location.href = url;
    // alert("oke");

}