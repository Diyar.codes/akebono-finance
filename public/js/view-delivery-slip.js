$(()=> {
    var no = window.location.pathname.split("/").pop()

    main_table()

    function main_table() {
        // getLoader
        getLoader()

        $.ajax({
            type: "GET",
            url: `/view-delivery-slip-main/` + no,
            success: function (data) {
                $('.main-table').find('.bodytable').html(data.main);
                $('#main-table').DataTable( {
                    responsive: true,
                    searching: false,
                    paging: false,
                    ordering: false, //komentar
                });
            },
        });
    }

})

