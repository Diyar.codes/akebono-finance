$(()=> {
    fill_datatable()

    function fill_datatable(filter_dn_number = '', filter_moon = '', filter_year = '') {
        $('#main-table').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            searching: false,
            ajax: {
                url: `${BASEURL}delivery-notes`,
                data: {
                    filter_dn_number: filter_dn_number,
                    filter_moon: filter_moon,
                    filter_year: filter_year,
                }
            },
            columns:[
                {
                    data: 'dn_tr_id',
                    name: 'dn_tr_id',
                },
                {
                    data: 'dn_po_nbr',
                    name: 'dn_po_nbr',
                },
                {
                    data: 'dn_vend',
                    name: 'dn_vend',
                },
                {
                    data: 'dn_user_id',
                    name: 'dn_user_id',
                },
                {
                    data: 'dn_order_date',
                    name: 'dn_order_date',
                },
                {
                    data: 'dn_ip',
                    name: 'dn_ip',
                },
                {
                    data: 'dn_arrival_date',
                    name: 'dn_arrival_date',
                },
                {
                    data: 'dn__char01',
                    name: 'dn__char01',
                },
                {
                    data: 'dn__date01',
                    name: 'dn__date01',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
            drawCallback: function( settings ) {
                $('#main-table_wrapper .row .col-sm-12').removeClass('col-md-6')
                $('#main-table_wrapper .row .col-sm-12').removeClass('col-md-7')
            }
        })
    }

    $('#filter').click(function() {
        var filter_dn_number = $('#filter_dn_number').val()
        var filter_moon = $('#filter_moon').val()
        var filter_year = $('#filter_year').val()

        if(filter_dn_number != '' || filter_moon != '' || filter_year != '') {
            $('#main-table').DataTable().destroy()
            fill_datatable(filter_dn_number, filter_moon, filter_year)
        } else {
            alert('Select Both filter Opinion')
        }
    })

    $('#reset').click(function() {
        $('#filter_dn_number').val('')
        $('#filter_moon').val('')
        $('#filter_year').val('')
        $('#main-table').DataTable().destroy()
        fill_datatable()
    })
})
