// change thead th datatable color
$(".display thead th").css("background-color", "#0066CC");
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

$(() => {
    $('#example').DataTable({
        "ordering": false,
    })
    // view data supplier
    $('#main-tablee').DataTable({
        // processing: true,
        responsive: true,
        // serverSide: true,
        ajax: {
            url: `${BASEURL}get-supplier`,
        },
        columns: [
            {
                data: 'ct_vd_addr',
                name: 'supplier id',
            },
            {
                data: 'ct_ad_name',
                name: 'supplier name',
            },
            {
                data: 'action',
                name: 'action',
            },
        ],
    })

    // select modal
    $(document).on('click', '#selectbusiness', function () {

        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        $('#supplierName').val(namebusiness)
        $('#searchSupplier').click()
    })
})

document.getElementById('searchIntransView').addEventListener('click', () => {
    getLoader()
    let month = document.getElementById('month').value;
    let year  = document.getElementById('year').value;
    let code  = document.getElementById('code').value;


    $.ajax({
        url: `${BASEURL}transfer-intrans-view`,
        method: "GET",
        data: {
            'code': code,
            'year': year,
            'month': month,
        },
        success: function (data) {
            console.log(data);
            $('#example').DataTable().destroy();
            $('#example tbody tr').remove();
            $('#example').find('#result').html(data);
            $('#example').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: '',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                },
                "ordering": false,
            });
        }
    })

})

