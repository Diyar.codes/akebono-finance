// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
$('#tablenya').DataTable({
    language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
    }
});
function search(){
    getLoader();
    var bulan = $('#bulan').val();
    var tahun = $('#tahun').val();
    $.ajax({
        type: "GET",
        url: "/search-outstanding-po",
        data:{
            bulan : bulan,
            tahun : tahun,
        },
        success: function (data) {
            $('#tablenya').DataTable().destroy();
            // $('#tablenya tbody tr').remove();
            $('.secondtable').find('#bodynya').html(data);
            $('#tablenya').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}

function reset(){
    getLoader();
    var bulan = '';
    var tahun = '';
    $.ajax({
        type: "GET",
        url: "/search-outstanding-po",
        data:{
            bulan : bulan,
            tahun : tahun,
        },
        success: function (data) {
            $('#tablenya').DataTable().destroy();
            // $('#tablenya tbody tr').remove();
            $('.secondtable').find('#bodynya').html(data);
            $('#tablenya').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}