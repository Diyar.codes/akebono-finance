$(()=> {
    $('#dataSecondTable').DataTable({
        responsive: false,
        paging: false,
        ordering : false,
    });
    $("table thead th").css("color", "#fff");
    
    function fetch_customer_data(bulan = '', tahun = '') {
        getLoader()
        $.ajax({
            type: "GET",
            url: "/data-pending-barcode-whs",
            data:{
                bulan : bulan,
                tahun : tahun,
            },
            success: function (data) {
                // $('.secondtable').show();
                $('.periodeblnthn').text(`Periode ${data.monthNow} - ${data.yearNow}`)
                $('#dataSecondTable').DataTable().destroy()
                $('.secondtable').find('.tblseconded').html(data.tablenya);
                $('#dataSecondTable').DataTable( {
                    responsive: false,
                    paging: false,
                    ordering : false,
                });
            },
            error: function(error) {
                SW.error({
                    message: 'Data is not Defined!!',
                });
            }
        });
    }

    // search
    $(document).on('click', '#search', function() {
        var bulan   = $('#bulan').val()
        var tahun   = $('#tahun').val()
        
        if(bulan != '' || tahun != '') {
            $('#dataSecondTable').DataTable().destroy()
            fetch_customer_data(bulan, tahun)
        } else {
            $('#dataSecondTable').DataTable().destroy()
            fetch_customer_data()
        }
    })

    $('#reset').click(function() {
        $('#bulan').val('');
        $('#tahun').val('');
        $('#dataSecondTable').DataTable().destroy()
        fetch_customer_data()
    });
});