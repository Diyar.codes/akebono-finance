// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
$('#tablenya').DataTable({
    responsive:true,
    language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
    }
});

function search(){
    getLoader();
    var from = $('#input-from').val();
    var to = $('#input-to').val();
    $.ajax({
        type: "GET",
        url: "/search-ps-barcode",
        data:{
            from : from,
            to : to,
        },
        success: function (data) {
            $('#tablenya').DataTable().destroy();
            $('#tablenya tbody tr').remove();
            $('.secondtable').show();
            $('.secondtable').find('#tbodymain').html(data);
            $('#tablenya').DataTable({
                responsive:true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}

function cetak(){
    getLoader();
    var from = $('#input-from').val();
    var to = $('#input-to').val();
    $.ajax({
        type: "GET",
        url: "/cetak-ps-direct",
        data:{
            from : from,
            to : to,
        },
        success: function (data) {
            if(data == 0){
                alert("NOt found");
            }else{
                window.open(data, '_blank');
            }
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}