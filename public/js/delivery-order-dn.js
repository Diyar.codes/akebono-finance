

    $(document).ready(function() {

        $('#tabelnya').DataTable({
            scrollY: "400px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            ordering:false,
            fixedColumns: true
        });

    });

    function cekdata(urut) {
        var sisa_dn = $('[id^="sisa_dn"]').map(function () { return $(this).val(); }).get();
        var qty = $('[id^="qty"]').map(function () { return $(this).val(); }).get();
        var hitung = urut - 1;
        if(parseInt(qty[hitung]) > parseInt(sisa_dn[hitung])){
            SW.error({
                    message: 'Over Qty Order !!',
            });
            document.getElementById("qty["+parseInt(urut)+"]").value = '';
        }
    }

    function save(){
        getLoader();
        var qty            = $('[id^="qty"]').map(function () { return $(this).val(); }).get();
        var lot            = $('[id^="lot"]').map(function () { return $(this).val(); }).get();
        var productdate    = $('[id^="productdate"]').map(function () { return $(this).val(); }).get();
        var item_number    = $('[id^="item_number"]').map(function () { return $(this).val(); }).get();
        var line           = $('[id^="line"]').map(function () { return $(this).val(); }).get();
        var po_um          = $('[id^="po_um"]').map(function () { return $(this).val(); }).get();
        var tr_id          = document.getElementById("tr_id").value;
        var dn             = document.getElementById("dn").value;
        var tgl            = document.getElementById("tgl").value;
        var po             = document.getElementById("po").value;
        var ps             = document.getElementById("ps").value;
        var jml_row        = document.getElementById("jml_row").value;
        if(tgl == ""){
            SW.error({
                message: 'Please Input Delivery Date',
            });
        }else if(ps == ""){
            SW.error({
                message: 'Please Input Packing Slip Number',
            });
        }else{
            $.ajax({
                type: "GET",
                url: "/insert-delivery-order-dn",
                data:{
                    qty : qty,
                    lot:lot,
                    productdate:productdate,
                    tr_id : tr_id,
                    dn:dn,
                    tgl : tgl,
                    po : po,
                    ps:ps,
                    item_number:item_number,
                    line:line,
                    po_um:po_um,
                    jml_row:jml_row
                },
                success: function (data) {
                    if(data != "berhasil"){
                        SW.error({
                            message: 'Delivery Order has been recorded in database',
                        });
                    }else{
                        SW.success({
                            message: 'Create Delivery Order Successfully',
                            
                        });
                        var url = `${BASEURL}create-delivery-order-dn`;
                        
                        setTimeout(function(){ 
                            window.location.href = url;
                        }, 1000);
                    }
                },
                error: function(error) {
                    alert('Data is not Defined!!!');
                }
            });
        }
    }

$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

//change model footer
$(".modal-footer").css("justify-content", "flex-start");
