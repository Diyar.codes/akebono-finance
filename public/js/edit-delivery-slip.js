$(()=> {
    var no = window.location.pathname.split("/").pop()

    // get detail delivery slip
    getDetailDeliverySlip()

    // view detail delivery slip
    function getDetailDeliverySlip() {
        // getLoader
        getLoader()

        $.ajax({
            type: "GET",
            url: `/edit-delivery-slip-update-detail-main/` + no,
            success: function (data) {
                $('.main-table').find('.bodytable').html(data.main);
                $('#main-table').DataTable({
                    // processing: true,
                    // serverSide: true,
                    // responsive: true,
                    searching: false,
                    paging: false,
                    ordering : false,
                    scrollY: "80vh",
                    scrollX: true,
                    scrollCollapse: true,
                    fixedColumns: {
                        leftColumns: 0,
                        rightColumns: 0
                    },
                });
            },
        });
    }

    // get data ptmstr
    $(document).on('click', '#seacrhSOAPPTMstrModal', function(){
        $('#get-ptmstr').DataTable().clear().destroy()
        fetch_pt_mstr();
    })

    // view data pt mstr
    function fetch_pt_mstr() {
        $('#get-ptmstr').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            paging: true,
            ajax: {
                url: `${BASEURL}get-pt-mstr-local`,
            },
            columns:[
                {
                    data: 'item_number',
                    name: 'item_number',
                },
                {
                    data: 'deskripsi1',
                    fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                        if(oData['deskripsi2'] == '-') {
                            oData['deskripsi2'] = ''
                        } else {
                            oData['deskripsi2'] = ' - ' + oData['deskripsi2']
                        }

                        $(nTd).append(oData['deskripsi2']);
                    }
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // select pt-mstr
    $(document).on('click', '#selectptmstr', function() {
        var itemnumber = $(this).data('itemnumber')
        var desc = $(this).data('desc')
        var um = $(this).data('um')

        $('[name="item[]"]').eq($('[name="item[]"]').length - 1).val(itemnumber)
        $('[name="desc[]"]').eq($('[name="item[]"]').length - 1).val(desc)
        $('[name="um[]"]').eq($('[name="item[]"]').length - 1).val(um)

        $('#getPtMstrTarget').click()
    })
})

function addRow() {
    var tr = ` <tr>
                    <td class="spacer p-3" style="padding-right: 3%">
                        <input type="text" id="item[]" name="item[]" class="form-control form-control-sm item text-dark" style="min-width: 80px" readonly>
                        <button type="button" class="btn btn-info btn-search pl-1 pr-1" data-effect="effect-rotate-left" data-toggle="modal" data-target="#getPtMstrTarget" id="seacrhSOAPPTMstrModal"><i class="fas fa-search"></i></button>
                    </td>
                    <td><input type="text" class="form-control form-control-sm text-dark desc" style="min-width:80px" name="desc[]" id="desc[]" readonly></td>
                    <td><input type="text" class="form-control form-control-sm text-dark um" style="min-width:80px" name="um[]" id="um[]" readonly></td>
                    <td><input type="number" class="form-control form-control-sm text-dark qty" style="min-width:80px" name="qty[]" id="qty[]"></td>
                    <td><a href="#" class="btn btn-danger remove"><i class="fas fa-trash"></i></a></td>
                </tr>
            `
    $('.tbody').append(tr)
}

// add row
$('#addRow').on('click', function() {
    addRow()
})

// remove row
$(document).on('click', '.remove', function() {
    var last = $('tbody tr').length
    if(last == 1) {
        SW.error({
            message: 'the last line cannot be deleted!'
        });
    } else {
        $(this).parent().parent().remove()
    }
})

$(document).on('click', '.remove-item', function() {
    Swal.fire({
        title: 'delete cover letter details?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.value) {
            var form = new FormData();
            form.append("_method", "DELETE");

            var settings = {
                "url": `${BASEURL}delete-dtl-delivery-slip/` + $(this).data('no') + `/` +  $(this).data('urutan'),
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Accept": "application/json",
                },
                "processData": false,
                "mimeType": "multipart/form-data",
                "contentType": false,
                "data": form
            };

            $.ajax(settings).done(function(response) {
                let data = JSON.parse(response);
                SW.success({
                    message: data.message
                });

                location.reload();
            });
        }
    })
})
