// change thead th datatable color
$("thead th").css("background-color", "#0066CC");

// change thead th datatable font colorrr
$("table thead th").css("color", "#fff");

$(() => {
    // view data supplier
    $('#main-tablee').DataTable({
        // processing: true,
        responsive: true,
        // serverSide: true,
        ajax: {
            url: `${BASEURL}get-supplier`,
        },
        columns: [
            {
                data: 'ct_vd_addr',
                name: 'supplier id',
            },
            {
                data: 'ct_ad_name',
                name: 'supplier name',
            },
            {
                data: 'action',
                name: 'action',
            },
        ],
    })

    // select modal
    $(document).on('click', '#selectbusiness', function () {

        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#supplierCode').val(codebusiness)
        $('#supplierName').val(namebusiness)
        $('#searchSupplier').click()
    })
})

function cek_data() {
    if (document.form1.cycle.value == 'none') {
        alert('Please input Cycle !');
        document.form1.cycle.focus();
        return false;
    }
    if (document.form1.txtSupplier.value == '') {
        alert('Please input Supplier !');
        document.form1.txtDescSupp.focus();
        return false;
    }
    if (document.form1.tanggal.value == '') {
        alert('Please complete this field !');
        document.form1.tanggal.focus();
        return false;
    }
    if (document.form1.pengirim.value == '') {
        alert('Please complete this field(Pengirim) !');
        document.form1.pengirim.focus();
        return false;
    }
}

function selectType(val) {
    let value = val
    const date = new Date();
    const years = date.getFullYear();
    const yearEnds = years.toString().substr(-2)
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const static = "WHS-PPC-AAIJ";
    getLoader();
    let kodeNo = ''
    if (value == 'item' || value == 'ng') {
        kodeNo = 'SC'
    } else if (value == 'box/palet' || value == 'kosong' || value == 'return') {
        kodeNo = 'SP'
    } else {
        // SW.error({
        //     message: 'Pilih Data Yang ada'
        // });
        document.getElementById('selecttype').children[0].setAttribute('selected', 'selected')
        document.getElementById('parsData').innerHTML = ''
    }

    $.ajax({
        url: `${BASEURL}cover-letter`,
        method: "GET",
        data: {
            'kodeNo': kodeNo
        },
        success: function (data) {
            let obj = JSON.parse(data)
            let no = obj.data

            const nogenerate = `${yearEnds}${month}${no}/${static}`;

            viewHtml = ''
            if (value == 'item' || value == 'box/palet' || value == 'ng' || value == 'return' || value == 'kosong') {
                let generate = '';
                if (value == 'item') {
                    generate = `SC${nogenerate}`
                }
                if (value == 'box/palet') {
                    generate = `SP${nogenerate}`
                }
                if (value == 'ng') {
                    generate = `SC${nogenerate}`
                }
                if (value == 'return') {
                    generate = `SP${nogenerate}`
                }
                if (value == 'kosong') {
                    generate = `SP${nogenerate}`
                }

                let parsHtml = document.getElementById('parsData');
                viewHtml += `
                <div class="row">
                    <div class="col-lg-4 col-md-8 col-sm-10 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">No</span>
                            <input type="text" name="no" id="no" class="form-control form-control-sm" value="${generate}" name="typenosrt" readonly aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-sm-6 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Cycle</span>
                            <select class="form-control form-control-sm cek" name="cycle" id="cycle" required>
                                <option value="">select</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="extra">extra</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-6 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Dikirim ke</span>
                            <input type="text" class="form-control form-control-sm cek" name="supplierCode" readonly id="supplierCode" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-lg-3 col-6 ml-n2">
                            <div class="input-group mb-2">
                            <button class="btn btn-info btn-sm mr-2" id="buttonTarget" data-toggle="modal" data-target="#searchSupplier"><i
                                                class="fas fa-search"></i></button>
                            <input type="text" class="form-control form-control-sm cek" name="supplierName" readonly id="supplierName"  autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-sm-6 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Tanggal</span>
                            <input type="date" class="form-control form-control-sm date cek" id="tanggal" name="tanggal" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-8 col-sm-10 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Kendaraan</span>
                            <input type="text" class="form-control form-control-sm cek" id="kendaraan" name="kendaraan" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-10 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Keterangan</span>
                            <input type="text" class="form-control form-control-sm cek" id="keterangan" name="keterangan" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-sm-6 box">
                        <div class="input-group mb-2">
                            <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Pengirim</span>
                            <input type="text" class="form-control form-control-sm cek" id="pengirim" name="pengirim" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div id="info">
                    <button class="btn btn-info" id="next">Next</button>
                </div>
                `;
                parsHtml.innerHTML = viewHtml;
                document.getElementById('buttonTarget').addEventListener('click', el => {
                    el.preventDefault();
                })
                document.getElementById('next').addEventListener('click', el => {
                    el.preventDefault();
                    var emptyInput = $(".cek").filter(function () {
                        return $.trim($(this).val()) == '';
                    }).length

                    if (emptyInput) {
                        viewHtmlDetail = `<button class="btn btn-info" id="next">Next</button>`
                        SW.error({
                            message: 'there is still an empty column!'
                        });
                        return false
                    } else {
                        let type = value;
                        console.log(type);
                        let supplierCode = document.getElementById('supplierCode').value;

                        $.ajax({
                            url: `${BASEURL}cover-letter`,
                            method: "GET",
                            data: {
                                'type': type,
                                'supplierCode': supplierCode
                            },
                            success: function (data) {
                                let obj = JSON.parse(data)
                                console.log(data)
                                if (obj.data == 'data kosong') {
                                    SW.error({
                                        message: 'data is empty'
                                    });
                                } else {
                                    let no_urut = 1;
                                    let viewHtmlDetail = '';

                                    if (type == 'ng' || type == 'item') {
                                        viewHtmlDetail += `
                                        <div class="table-responsive mt-2">
                                            <table class="display text-center" style="font-size:11px" id="data_table">
                                                <thead>
                                                    <tr>
                                                        <th width="12%" class="text-white">Kode</th>
                                                        <th width="20%" class="text-white">Nama Barang</th>
                                                        <th class="text-white">Type</th>
                                                        <th class="text-white">Satuan</th>
                                                        <th class="text-white">Jumlah</th>
                                                        <th class="text-white">Location From</th>
                                                        <th class="text-white">Location To</th>
                                                    </tr>
                                                </thead>
                                                <tbody>`
                                        obj.data.forEach(element => {
                                            viewHtmlDetail += `
                                            <tr>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="kode${no_urut}" name="kode${no_urut}" value="${element.item_no}" readonly> </td>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="namabarang${no_urut}" name="namabarang${no_urut}" value="${element.description}" readonly> </td>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="type${no_urut}" name="type${no_urut}" value="${element.type_item == null ? '' : element.type_item}" readonly> </td>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="satuan${no_urut}" name="satuan${no_urut}" value="${element.um}" readonly> </td>
                                                <td> <input type="number" class="form-control form-control-sm cek" id="jumlah${no_urut}" name="jumlah${no_urut}"> </td>
                                                <td> <input type="text" class="form-control form-control-sm cek" value="${element.location_from}" readonly> </td>
                                                <td> <input type="text" class="form-control form-control-sm cek" value="${element.location_to}" readonly> </td>
                                            </tr>`
                                            no_urut++;
                                        });
                                    } else if (type == 'box/palet') {
                                        viewHtmlDetail += `
                                        <div class="table-responsive mt-2">
                                            <table class="display text-center" style="font-size:11px" id="data_table">
                                                <thead>
                                                    <tr>
                                                        <th width="12%" class="text-white">Kode</th>
                                                        <th width="20%" class="text-white">Nama Barang</th>
                                                        <th class="text-white">Satuan</th>
                                                        <th class="text-white">Jumlah</th>
                                                    </tr>
                                                </thead>
                                                <tbody>`
                                        obj.data.forEach(element => {
                                            viewHtmlDetail += `
                                            <tr>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="kode${no_urut}" name="kode${no_urut}" value="${element.item_no}" readonly> </td>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="namabarang${no_urut}" name="namabarang${no_urut}" value="${element.description}" readonly> </td>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="satuan${no_urut}" name="satuan${no_urut}" value="${element.um}" readonly> </td>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="jumlah${no_urut}" name="jumlah${no_urut}"> </td>
                                            </tr>`
                                            no_urut++;
                                        });
                                    } else if (type == 'return') {
                                        viewHtmlDetail += `
                                        <div class="table-responsive mt-2">
                                            <table class="display text-center" style="font-size:11px" id="data_table">
                                                <thead>
                                                    <tr>
                                                        <th width="12%" class="text-white">Kode</th>
                                                        <th width="20%" class="text-white">Nama Barang</th>
                                                        <th class="text-white">Type</th>
                                                        <th class="text-white">Satuan</th>
                                                        <th class="text-white">Jumlah</th>
                                                    </tr>
                                                </thead>
                                                <tbody>`
                                        obj.data.forEach(element => {
                                            viewHtmlDetail += `
                                            <tr>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="kode${no_urut}" name="kode${no_urut}" value="${element.item_no}" readonly> </td>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="namabarang${no_urut}" name="namabarang${no_urut}" value="${element.description}" readonly> </td>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="type${no_urut}" name="type${no_urut}" value="${element.type_item == null ? '' : element.type_item}" readonly> </td>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="satuan${no_urut}" name="satuan${no_urut}" value="${element.um}" readonly> </td>
                                                <td> <input type="text" class="form-control form-control-sm cek" id="jumlah${no_urut}" name="jumlah${no_urut}"> </td>
                                            </tr>`
                                            no_urut++;
                                        });
                                    } else {
                                        viewHtmlDetail += `
                                        <div class="table-responsive mt-2">
                                            <table class="display text-center" style="font-size:11px" id="data_table">
                                                <thead>
                                                    <tr>
                                                        <th width="12%" class="text-white">Kode</th>
                                                        <th width="20%" class="text-white">Nama Barang</th>
                                                        <th class="text-white">Type</th>
                                                        <th class="text-white">Satuan</th>
                                                        <th class="text-white">Jumlah</th>
                                                        <th class="text-white">Location From</th>
                                                        <th class="text-white">Location To</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="number" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="number" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                </tr>
                                                <tr>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="number" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="number" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                </tr>
                                                <tr>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="number" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="number" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                    <td> <input type="text" class="form-control form-control-sm cek" readonly> </td>
                                                </tr>
                                                `
                                    }

                                    viewHtmlDetail += `</tbody></table><input type="hidden" name="jumlah_item" value="${no_urut}">`
                                    if (type == 'item' || type == 'box/palet' || type == 'ng' || type == 'return') {
                                        viewHtmlDetail += 'Note : <i>Untuk menambah item yang tidak ada, masuk menu Delivery Slip > Master Item</i>'
                                    }
                                    viewHtmlDetail += `
                                            <div class="row mt-2 mb-2">
                                                <div class="col-lg-12 col-sm-12">
                                                    <button type="submit" class="btn btn-sm btn-success mb-2" id="updateItemKanban"><i class="fas fa-save"></i> Save</button>
                                                    <button type="button" class="btn btn-sm btn-danger mb-2" id="cancel"><i class="fas fa-undo"></i> Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                        `;
                                    document.getElementById('info').innerHTML = viewHtmlDetail
                                    // change thead th datatable color
                                    $("thead th").css("background-color", "#0066CC");

                                    // change thead th datatable font color
                                    $("table thead th").css("color", "#fff");
                                    document.getElementById('cancel').addEventListener('click', el => {
                                        document.getElementById('selecttype').children[0].setAttribute('selected', 'selected')
                                        document.getElementById('parsData').innerHTML = ''
                                    })
                                    $('#data_table').DataTable({
                                        searching: false, paging: false, info: false, scrollY: 350,
                                        scrollX: true,
                                        scroller: true,
                                        ordering: false
                                    })
                                }
                            }
                        })
                    }
                })
            } else if (value == '') {
                document.getElementById('selecttype').children[0].setAttribute('selected', 'selected')
                document.getElementById('parsData').innerHTML = ''
            }
        }
    })
}
