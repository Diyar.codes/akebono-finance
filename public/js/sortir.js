// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

$(()=> {
    $(document).on('click', '#seacrhSOAPBusinesRelationModal', function(){
        $('#get-supplier').DataTable().clear().destroy()
        getSupplier();
    })

    function getSupplier() {
        // view data supplier
        $('#get-supplier').DataTable({
            // processing: true,
            responsive: false,
            // serverSide: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'supplier id',
                },
                {
                    data: 'ct_ad_name',
                    name: 'supplier name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    $(document).on('click', '#search-get-pt-mstr', function(){
        $('#get-pt-mstr').DataTable().clear().destroy()
        fetch_pt_mstr();
    })

     // get pt mstr model
    function fetch_pt_mstr() {
        // getLoader
        getLoader()

        // view data supplier
        $('#get-pt-mstr').DataTable({
            // processing: true,
            responsive: true,
            // serverSide: true,
            ajax: {
                url: `${BASEURL}get-pt-mstr-local`,
            },
            columns:[
                {
                    data: 'item_number',
                    name: 'Item Number',
                },
                {
                    data: 'deskripsi1',
                    name: 'Description',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

     // select pt-mstr
    $(document).on('click', '#selectptmstr', function() {
        var itemnumber = $(this).data('itemnumber')

        $('#item_number').val(itemnumber)
        $('#modalptmstrbydesc').click()
    })

    // select modal business relation
    $(document).on('click', '#selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        $('#modaldemo8part2').click()
    })

    $('#ok').keyup(function(e) {
        var ok = $('#ok').val()
        if (ok) {
            var oke = $('#ok').val()
        } else {
            var oke = 0
        }

        var ng = $('#ng').val()

        if(ng) {
            var ngerusak = $('#ng').val()
        } else {
            var ngerusak = 0
        }

        $('#total_sortir').val(parseInt(oke) + parseInt(ngerusak))
    })

    $('#ng').keyup(function(e) {
        var ok = $('#ok').val()
        if (ok) {
            var oke = $('#ok').val()
        } else {
            var oke = 0
        }

        var ng = $('#ng').val()

        if(ng) {
            var ngerusak = $('#ng').val()
        } else {
            var ngerusak = 0
        }

        $('#total_sortir').val(parseInt(oke) + parseInt(ngerusak))
    })
})

// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
