
function save_bast_form(){
    SW.success({
        message: 'Add Bast Successfully',
    });
}

$(document).ready(function() {

    $('#list_pengerjaan').DataTable({
        responsive: true,
        paging : false,

    });

    $('#list_lokasi').DataTable({
        responsive: true,
        language: {
            lengthMenu: '_MENU_ items/page',
        }
    });

    $('#list_po').DataTable({
        responsive: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
        }
    });
    $(".table-bordered thead th").css("background-color", "#0066CC");
    $("#list_pengerjaan thead th").css("color", "#fff");
    $("#jsa thead th").css("color", "#fff");
    $("#list_lokasi thead th").css("color", "#fff");
    $("#list_po thead th").css("color", "#fff");
    $("#sr thead th").css("color", "#fff");

    $('.sr').hide();
    $("#sr").click(function() {
        var checkedValue = $('#sr:checked').val();
        if(checkedValue == 'on'){
            $('.sr').show();
        }else{
            $('.sr').hide();
        }
    });

    $('.jsa').hide();
    $('#preview_bast').hide();
    $("#jsa").click(function() {
        var checkedValue = $('#jsa:checked').val();
        if(checkedValue == 'on'){
            $('.jsa').show();
        }else{
            $('.jsa').hide();
        }
    });

    $('#upload_form').on('submit', function(event){
        var no_po       = document.getElementById("no_po").value;
        var no_ref      = document.getElementById("no_ref").value;
        var garansi     = document.getElementById("garansi").value;
        //PIHAK KE 1
        var nama1       = document.getElementById("nama1").value;
        //PIHAK KE 2
        var nama2       = document.getElementById("nama2").value;
        if(no_po == ""){
            SW.error({
                message: "PO Number Can't Empty"
            });
            return false;
        }else if(no_ref == ""){
            SW.error({
                message: "No Referensi Can't Empty"
            });
            return false;
        }else if(garansi == ""){
            SW.error({
                message: "Garansi Can't Empty"
            });
            return false;
        }else if(nama1 == ""){
            SW.error({
                message: "Supplier Name Can't Empty"
            });
            return false;
        }else{
            event.preventDefault();
            getLoader();
            $.ajax({
                url:"/save-bast-data",
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success:function(data){
                    console.log(data);
                    if(data.result == 0){
                        SW.error({
                            message: 'No BAST has been recorded database',
                        });
                    }else if(data.result == 1){
                        SW.success({
                            message: 'Add Bast Successfully',
                        });
                        setTimeout(function(){ 
                            location.reload(); 
                        }, 1000);
                    }else{
                        SW.warning({
                            message: 'Please check your form',
                        });
                    }
                }
            })
        }
    });

    $('#preview_bast_form').on('click', function(event) {
        event.preventDefault();
        $.ajax({
            url:"/preview-bast-data",
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success:function(data){
                console.log(data);
            }
        })
    });

    // add row
    $('#addRow').on('click', function() {
        addRow()
    });

    // remove row
    $(document).on('click', '.remove', function() {
        var last = $('tbody tr').length
        if(last == 1) {
            SW.error({
                message: 'the last line cannot be deleted!'
            });
        } else {
            $(this).parent().parent().remove();
        }
    });

    // $("#bast_form").submit(function(e) {

    //     e.preventDefault(); // avoid to execute the actual submit of the form.
    
    //     var form = $(this);
    //     var url = form.attr('action');
        
    //     $.ajax({
    //            type: "POST",
    //            url: url,
    //            data: form.serialize(), // serializes the form's elements.
    //            success: function(data){
    //                 SW.success({
    //                     message: 'Add Bast Successfully',
    //                 });
    //                 setTimeout(function(){
    //                     location.reload();
    //                 }, 1000);
                    
    //            },error: function(error) {
    //               alert('Busy');
    //         }
    //     });
    // });

    $("#save_bast").click(function() {
        //ISI AWAL BERITA ACARA SERAH TERIMA
        var no_ba       = document.getElementById("no_ba").value;
        var no_po       = document.getElementById("no_po").value;
        var status      = document.getElementById("status").value;
        var no_ref      = document.getElementById("no_ref").value;
        var periode     = document.getElementById("periode").value;
        var to          = document.getElementById("to").value;
        var garansi     = document.getElementById("garansi").value;

        //PIHAK KE 1
        var nama1       = document.getElementById("nama1").value;
        var perusahaan1 = document.getElementById("perusahaan1").value;
        var alamat1     = document.getElementById("alamat1").value;
        var jabatan1    = document.getElementById("jabatan1").value;
        var pihak1      = document.getElementById("pihak1").value;
        //PIHAK KE 2
        var nama2       = document.getElementById("nama2").value;
        var perusahaan2 = document.getElementById("perusahaan2").value;
        var alamat2     = document.getElementById("alamat2").value;
        var jabatan2    = document.getElementById("jabatan2").value;
        var pihak2      = document.getElementById("pihak2").value;

        //ADDTIONAL DATAA
        var note        = document.getElementById("note").value;

        //isi table barang PO
        var line        = $('[id^="line"]').map(function () { return $(this).val(); }).get();
        var desc        = $('[id^="desc"]').map(function () { return $(this).val(); }).get();
        var satuan      = $('[id^="satuan"]').map(function () { return $(this).val(); }).get();
        var qty_open    = $('[id^="qty_open"]').map(function () { return $(this).val(); }).get();
        var qba         = $('[id^="qba"]').map(function () { return $(this).val(); }).get();
        var cks         = $('[id^="cks"]').map(function () { return $(this).val(); }).get();

        //isi table area lokasi pekerjaan
        var lokasi       = $('[id^="lokasi"]').map(function () { return $(this).val(); }).get();
        var uraian       = $('[id^="uraian"]').map(function () { return $(this).val(); }).get();
        var keterangan   = $('[id^="keterangan"]').map(function () { return $(this).val(); }).get();
        var status_1     = $('[id^="status"]').map(function () { return $(this).val(); }).get();
        var foto_sebelum = $('[id^="foto_sebelum"]').map(function () { return $(this).val(); }).get();
        var foto_sesudah = $('[id^="foto_sesudah"]').map(function () { return $(this).val(); }).get();
        var status2      = $('[id^="status2"]').map(function () { return $(this).val(); }).get();
        // alert(foto_sebelum);
        //TABLE SERVICE
        var service_img       = document.getElementById("service_img").value;
        var service_ket       = document.getElementById("service_ket").value;

        //TABLE jsa
        var jsa_img       = document.getElementById("jsa_img").value;
        var jsa_ket       = document.getElementById("jsa_ket").value;

        // alert(cks);
        var jml_data     = document.getElementById("jml_data").value;

        if(no_ref == ""){
            SW.error({
                message: "No Referensi Can't Empty",
            });
        }else if(garansi == ""){
            SW.error({
                message: "Guaranted Can't Empty",
            });
        }else if(nama1 == ""){
            SW.error({
                message: "Name Can't Empty",
            });
        }else if(jabatan1 == ""){
            SW.error({
                message: "Position Can't Empty",
            });
        }else{
            $.ajax({
                type: "POST",
                url: "/save-bast-data",
                data:{
                    _token: $('#token').val(),
                    no_ba : no_ba,
                    no_po : no_po,
                    status : status,
                    no_ref : no_ref,
                    periode : periode,
                    to : to,
                    note : note,
                    garansi : garansi,
                    nama1 : nama1,
                    nama2 : nama2,
                    perusahaan1 : perusahaan1,
                    perusahaan2 : perusahaan2,
                    alamat1 : alamat1,
                    alamat2 : alamat2,
                    jabatan1 : jabatan1,
                    jabatan2:jabatan2,
                    pihak1 : pihak1,
                    pihak2:pihak2,
                    line:line,
                    desc:desc,
                    satuan:satuan,
                    qty_open:qty_open,
                    qba:qba,
                    cks:cks,
                    jml_data:jml_data,
                },
                success: function (data) {
                    console.log(data);
                    // if(data == "duplicated"){
                    //     SW.error({
                    //         message: 'Delivery Note has been recorded in database',
                    //     });
                    // }else if (data == 'Email empty'){
                    //     SW.error({
                    //         message: 'Email Adress is not defined',
                    //     });
                    // }else {
                    //     setTimeout(function(){
                    //            location.reload();
                    //     }, 1000);
                    //     SW.success({
                    //         message: 'Create Delivery Note Successfully',
                    //     });
                    // }
                },
                error: function(error) {
                    SW.error({
                            message: 'Data is not Defined!!!',
                    });
                }
            });

        }
    });


    $(function() {
        var no_ba       = document.getElementById("no_ba").value;
        var no_po       = document.getElementById("no_po").value;
        var status      = document.getElementById("status").value;
        var no_ref      = document.getElementById("no_ref").value;
        var periode     = document.getElementById("periode").value;
        var to          = document.getElementById("to").value;
        var garansi     = document.getElementById("garansi").value;

        var nama1       = document.getElementById("nama1").value;
        var perusahaan1 = document.getElementById("perusahaan1").value;
        var alamat1     = document.getElementById("alamat1").value;
        var jabatan1    = document.getElementById("jabatan1").value;
        var pihak1      = document.getElementById("pihak1").value;
        //PIHAK KE 2
        var nama2       = document.getElementById("nama2").value;
        var perusahaan2 = document.getElementById("perusahaan2").value;
        var alamat2     = document.getElementById("alamat2").value;
        var jabatan2    = document.getElementById("jabatan2").value;
        var pihak2      = document.getElementById("pihak2").value;

        document.getElementById("preview_nbr").innerHTML = no_ba;
        document.getElementById("preview_status_bast").innerHTML = status;
        document.getElementById("preview_npr").innerHTML = perusahaan1;
        document.getElementById("preview_po").innerHTML = no_po;
        document.getElementById("preview_ref").innerHTML = no_ref;
        document.getElementById("preview_periode").innerHTML = periode+" - "+to;
        document.getElementById("preview_garansi").innerHTML = garansi;

        document.getElementById("preview_nama1").innerHTML = nama1;
        document.getElementById("preview_perusahaan1").innerHTML = perusahaan1;
        document.getElementById("preview_alamat1").innerHTML = alamat1;
        document.getElementById("preview_jabatan1").innerHTML = jabatan1;

        document.getElementById("preview_nama2").innerHTML = nama2;
        document.getElementById("preview_jabatan2").innerHTML = jabatan2;

        // document.getElementById("preview_status_supp").innerHTML = nama1;
        // document.getElementById("preview_app_supp").innerHTML = perusahaan1;
        document.getElementById("preview_nama_supp").innerHTML = nama1.toUpperCase();
        document.getElementById("preview_jabatan_supp").innerHTML = jabatan1.toUpperCase();

        // document.getElementById("preview_status_user").innerHTML = nama2.toUpperCase();
        // document.getElementById("preview_app_user").innerHTML = perusahaan2.toUpperCase();
        document.getElementById("preview_nama_user").innerHTML = nama2.toUpperCase();
        document.getElementById("preview_jabatan_user").innerHTML = jabatan2.toUpperCase();

        document.getElementById("preview_section").innerHTML = jabatan2;

        $("a#preview_bast_button").click(nWin);
    });

    $("#preview").click(function() {
        alert("preview");
        //ISI AWAL BERITA ACARA SERAH TERIMA
        var no_ba       = document.getElementById("no_ba").value;
        var no_po       = document.getElementById("no_po").value;
        var status      = document.getElementById("status").value;
        var no_ref      = document.getElementById("no_ref").value;
        var periode     = document.getElementById("periode").value;
        var to          = document.getElementById("to").value;
        var garansi     = document.getElementById("garansi").value;

        //PIHAK KE 1
        var nama1       = document.getElementById("nama1").value;
        var perusahaan1 = document.getElementById("perusahaan1").value;
        var alamat1     = document.getElementById("alamat1").value;
        var jabatan1    = document.getElementById("jabatan1").value;
        var pihak1      = document.getElementById("pihak1").value;
        //PIHAK KE 2
        var nama2       = document.getElementById("nama2").value;
        var perusahaan2 = document.getElementById("perusahaan2").value;
        var alamat2     = document.getElementById("alamat2").value;
        var jabatan2    = document.getElementById("jabatan2").value;
        var pihak2      = document.getElementById("pihak2").value;

        //ADDTIONAL DATA
        var note        = document.getElementById("note").value;

        //isi table barang PO
        var line        = $('[id^="line"]').map(function () { return $(this).val(); }).get();
        var desc        = $('[id^="desc"]').map(function () { return $(this).val(); }).get();
        var satuan      = $('[id^="satuan"]').map(function () { return $(this).val(); }).get();
        var qty_open    = $('[id^="qty_open"]').map(function () { return $(this).val(); }).get();
        var qba         = $('[id^="qba"]').map(function () { return $(this).val(); }).get();
        var cks         = $('[id^="cks"]').map(function () { return $(this).val(); }).get();

        //isi table area lokasi pekerjaan
        var lokasi       = $('[id^="lokasi"]').map(function () { return $(this).val(); }).get();
        var uraian       = $('[id^="uraian"]').map(function () { return $(this).val(); }).get();
        var keterangan   = $('[id^="keterangan"]').map(function () { return $(this).val(); }).get();
        var status_1     = $('[id^="status"]').map(function () { return $(this).val(); }).get();
        var foto_sebelum = $('[id^="foto_sebelum"]').map(function () { return $(this).val(); }).get();
        var foto_sesudah = $('[id^="foto_sesudah"]').map(function () { return $(this).val(); }).get();
        var status2      = $('[id^="status2"]').map(function () { return $(this).val(); }).get();
        // alert(foto_sebelum);
        //TABLE SERVICE
        var service_img       = document.getElementById("service_img").value;
        var service_ket       = document.getElementById("service_ket").value;

        //TABLE jsa
        var jsa_img       = document.getElementById("jsa_img").value;
        var jsa_ket       = document.getElementById("jsa_ket").value;

        // alert(cks);
        var jml_data     = document.getElementById("jml_data").value;

        if(no_ref == ""){
            SW.error({
                message: "No Referensi Can't Empty",
            });
        }else if(garansi == ""){
            SW.error({
                message: "Guaranted Can't Empty",
            });
        }else if(nama1 == ""){
            SW.error({
                message: "Name Can't Empty",
            });
        }else if(jabatan1 == ""){
            SW.error({
                message: "Position Can't Empty",
            });
        }else{
            // $.ajax({
            //     type: "POST",
            //     url: "/save-bast-data",
            //     data:{
            //         _token: $('#token').val(),
            //         no_ba : no_ba,
            //         no_po : no_po,
            //         status : status,
            //         no_ref : no_ref,
            //         periode : periode,
            //         to : to,
            //         note : note,
            //         garansi : garansi,
            //         nama1 : nama1,
            //         nama2 : nama2,
            //         perusahaan1 : perusahaan1,
            //         perusahaan2 : perusahaan2,
            //         alamat1 : alamat1,
            //         alamat2 : alamat2,
            //         jabatan1 : jabatan1,
            //         jabatan2:jabatan2,
            //         pihak1 : pihak1,
            //         pihak2:pihak2,
            //         line:line,
            //         desc:desc,
            //         satuan:satuan,
            //         qty_open:qty_open,
            //         qba:qba,
            //         cks:cks,
            //         jml_data:jml_data,
            //     },
            //     success: function (data) {
            //         console.log(data);
            //         // if(data == "duplicated"){
            //         //     SW.error({
            //         //         message: 'Delivery Note has been recorded in database',
            //         //     });
            //         // }else if (data == 'Email empty'){
            //         //     SW.error({
            //         //         message: 'Email Adress is not defined',
            //         //     });
            //         // }else {
            //         //     setTimeout(function(){
            //         //            location.reload();
            //         //     }, 1000);
            //         //     SW.success({
            //         //         message: 'Create Delivery Note Successfully',
            //         //     });
            //         // }
            //     },
            //     error: function(error) {
            //         SW.error({
            //                 message: 'Data is not Defined!!!',
            //         });
            //     }
            // });

        }
    });

});

function addRow() {
    // var hitung       = document.getElementById("hitung").value;
    // var calculate    = Number(hitung) + Number(1);
    var tr = `
            <tr>
                <td><input class="form-control" type="text" name="lokasi[]" id="lokasi[]" style="color:black"></td>
                <td><textarea class="form-control" type="text" name="uraian[]" id="uraian[]" style="color:black"></textarea></td>
                <td>
                    <select name="status1[]" id="status1[]" class="form-control" style="color:black">
                        <option value="Finished">Finished</option>
                        <option value="Unfinished">Unfinished</option>
                    </select>
                </td>
                <td><input class="form-control" type="file" name="foto_sebelum[]" id="foto_sebelum" required></td>
                <td><input class="form-control" type="file" name="foto_sesudah[]" id="foto_sesudah[]" required></td>
                <td><textarea class="form-control" type="text" name="keterangan[]" id="keterangan[]" style="color:black"></textarea></td>
                <td>
                    <select name="status2[]" id="status2[]" class="form-control" style="color:black">
                        <option value="Finished">Finished</option>
                        <option value="Unfinished">Unfinished</option>
                    </select>
                </td>
                <td><a class="btn btn-danger remove"><i class="fas fa-times"></i></a></td>
            </tr>
        `
    $('.tbody').append(tr);
}

function nWin() {
    var w = window.open();
    var html = $("#preview_bast").html();

      $(w.document.body).html(html);
}

function cek_qba(hitung){
    var txt_qba = $('[id^="qba"]').map(function () { return $(this).val(); }).get();
    var txt_qpo = $('[id^="qty_open"]').map(function () { return $(this).val(); }).get();

    if (parseInt(txt_qba[hitung]) > parseInt(txt_qpo[hitung])){
        SW.error({
                message: "Quantity BA Must be small",
            });
        document.getElementById('qba['+hitung+']').value="";

    }
}

function select_po(po){
    // alert(po);
    document.getElementById("no_po").value = po;
    rinci_data(po);
    $('#modaldemo8part2').modal('hide');

}
function rinci_data(po){
    var ponya = po;
        $.ajax({
            url: "/get-rincian-bast",
            method: "GET",
            data:{
                ponya:ponya
            },
            success:function(data){
                // console.log(data);
                var obj = JSON.parse(data);
                document.getElementById("nama2").value = obj.div_head;
                document.getElementById("no_pp").value = obj.no_pp;
                $('#list_pengerjaan').DataTable().destroy();
                $('#list_pengerjaan tbody tr').remove();
                $('.secondtable').find('#result').html(obj.table);
                $('#list_pengerjaan').DataTable({
                    responsive: true,
                    paging : false,
                    scrollY: "400px",
                    language: {
                        searchPlaceholder: 'Search...',
                        sSearch: '',
                        lengthMenu: '_MENU_ items/page',
                    }
                });
            }
    });
}
