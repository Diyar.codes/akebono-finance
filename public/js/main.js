const BASEURL = $('meta[name=base-url]').attr('content')

$(() => {
    $('#buttonLogout').click(() => {
        SW.confirm({
            message: 'do you want to leave the supplier portal?',
            onConfirm: () => {
                return window.location.href = '/logout'
            },
        });
    });

    $('#buttonLogoutt').click(() => {
        SW.confirm({
            message: 'do you want to leave the supplier portal?',
            onConfirm: () => {
                return window.location.href = '/logout'
            },
        });
    });
})

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function getLoader() {
    if($.ajax) {
        $(document).ajaxStart(function() {
            $(".preloader").removeClass('d-none');
            $(".preloader").fadeIn();
        });

        $(document).ajaxStop(function() {
            $(".preloader").addClass('d-none')
            $(".preloader").fadeOut();
        });
    }
}
