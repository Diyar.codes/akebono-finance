$(()=> {
    // seacrh soap business relation
    $(document).on('click', '#seacrhSOAPBusinesRelationModal', function(){
        $('#get-supplier').DataTable().clear().destroy()
        getSupplier();
    })

    // get supplier
    function getSupplier() {
        $('#get-supplier').DataTable({
            // processing: true
            responsive: true,
            // serverSide: true
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'supplier id',
                },
                {
                    data: 'ct_ad_name',
                    name: 'supplier name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // select modal
    $(document).on('click', '#selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        $('#name').val(namebusiness)
        $('#getSupplierTarget').click()
    })

    // gate list_packing
    getListData()

    // view list_packing
    function getListData(filter_id_supp = '', filter_no_surat = '', filter_moon = '', filter_year = '') {
        // getLoader make waiting
        getLoader()

        $('#main-table').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            searching: false,
            ordering : false,
            ajax: {
                url: `${BASEURL}list-miss-packing`,
                data: {
                    filter_id_supp:filter_id_supp,
                    filter_no_surat:filter_no_surat,
                    filter_moon:filter_moon,
                    filter_year:filter_year,
                }
            },
            columns:[
                {
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'no',
                    name: 'no',
                },
                {
                    data: 'ct_ad_name',
                    name: 'ct_ad_name',
                },
                {
                    data: 'tanggal',
                    name: 'tanggal',
                },
                {
                    data: 'cycle',
                    name: 'cycle',
                },
                {
                    data: 'pengirim',
                    name: 'pengirim',
                },
                {
                    data: 'kendaraan',
                    name: 'kendaraan',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
             columnDefs: [
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap'>" + data + "</div>";
                    },
                    targets: 0
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap'>" + data + "</div>";
                    },
                    targets: 1
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap'>" + data + "</div>";
                    },
                    targets: 2
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap'>" + data + "</div>";
                    },
                    targets: 3
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap'>" + data + "</div>";
                    },
                    targets: 4
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap'>" + data + "</div>";
                    },
                    targets: 5
                },
            ],
        })
    }

    // filter
    $('#searchUp').click(function() {
        var filter_id_supp = $('#code').val()
        var filter_no_surat = $('#no_surat').val()

        $('#moon').val('')
        $('#year').val('')

        if(filter_id_supp != '' || filter_no_surat != '') {
            $('#main-table').DataTable().clear().destroy()
            getListData(filter_id_supp, filter_no_surat)
        } else {
            $('#main-table').DataTable().clear().destroy()
            getListData()
        }
    })

    $('#searchDown').click(function() {
        var filter_id_supp = $('#code').val()
        var filter_no_surat = $('#no_surat').val()
        var filter_moon = $('#moon').val()
        var filter_year = $('#year').val()

        $('#code').val('')
        $('#name').val('')
        $('#no_surat').val('')

        if(filter_moon != '' || filter_year != '') {
            $('#main-table').DataTable().clear().destroy()
            getListData(filter_id_supp, filter_no_surat, filter_moon, filter_year)
        } else {
            $('#main-table').DataTable().clear().destroy()
            getListData()
        }
    })

    // reset
    $('#resetUp').click(function() {
        $('#moon').val('')
        $('#year').val('')
        $('#code').val('')
        $('#name').val('')
        $('#no_surat').val('')
        $('#main-table').DataTable().clear().destroy()
        getListData()
    })

    $('#resetDown').click(function() {
        $('#moon').val('')
        $('#year').val('')
        $('#code').val('')
        $('#name').val('')
        $('#no_surat').val('')
        $('#main-table').DataTable().clear().destroy()
        getListData()
    })
})
