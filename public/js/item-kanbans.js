
$(()=> {
    // get main table
    main_table()

    // main table
    function main_table(queryItemNumber = '', querySupplierId = '', querySupplierName = '') {
        // getLoader
        getLoader()

        $.ajax({
            type: "GET",
            url: "/search-item-kanban",
            data:{
                queryItemNumber : queryItemNumber,
                querySupplierId : querySupplierId,
                querySupplierName : querySupplierName,
            },
            success: function (data) {
                $('.main-table').find('.bodytable').html(data.main);
                $('#main-table').DataTable( {
                    responsive: false,
                    paging: false,
                    searching: false,
                    ordering : false,
                    scrollY: "70vh",
                    scrollX: true,
                    scrollCollapse: true,
                    fixedColumns: {
                        leftColumns: 0,
                        rightColumns: 0
                    },
                } );
            },
        });
    }

    // when click btn search to modal get supplier
    $(document).on('click', '#seacrhSOAPBusinesRelation', function(){
        $('#get-supplier').DataTable().clear().destroy()
        getSupplier();
    })

    $(document).on('click', '#seacrhSOAPBusinesRelationAdd', function(){
        $('#get-supplierAdd').DataTable().clear().destroy()
        getSupplierAdd();
    })

    // get supplier modal
    function getSupplier() {
        // getLoader
        getLoader()

        // view data supplier
        $('#get-supplier').DataTable({
            // processing: true,
            responsive: true,
            // serverSide: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'supplier id',
                },
                {
                    data: 'ct_ad_name',
                    name: 'supplier name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    function getSupplierAdd() {
        // getLoader
        getLoader()

        // view data supplier
        $('#get-supplierAdd').DataTable({
            // processing: true,
            responsive: true,
            // serverSide: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'supplier id',
                },
                {
                    data: 'ct_ad_name',
                    name: 'supplier name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // select modal
    $(document).on('click', '#get-supplier #selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        // $('#code').val(codebusiness)
        $('#input-search-supplier_id').val(codebusiness)
        $('#input-search-supplier_name').val(namebusiness)
        $('#getSupplierTarget').click()
    })

    $(document).on('click', '#get-supplierAdd #selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        // $('#input-search-supplier_id').val(codebusiness)
        // $('#input-search-supplier_name').val(namebusiness)
        $('#getSupplierTargetAdd').click()
    })

    // search
    $(document).on('click', '#search', function() {
        var queryItemNumber = $('#input-search-item_number').val()
        var querySupplierId = $('#input-search-supplier_id').val()
        var querySupplierName = $('#input-search-supplier_name').val()

        if(queryItemNumber != '' || querySupplierId != '' || querySupplierName != '') {
            $('#main-table').DataTable().clear().destroy()
            main_table(queryItemNumber, querySupplierId, querySupplierName)
        } else {
            $('#main-table').DataTable().clear().destroy()
            main_table()
        }
    })

    // get pt mstr model
    function getPtMstr() {
        // getLoader
        getLoader()

        // view data supplier
        $('#get-ptmstr').DataTable({
            // processing: true,
            responsive: true,
            // serverSide: true,
            ajax: {
                url: `${BASEURL}get-pt-mstr-local`,
            },
            columns:[
                {
                    data: 'item_number',
                    name: 'Item Number',
                },
                {
                    data: 'deskripsi1',
                    render: function ( data, type, row ) {
                        if(row.deskripsi2 == '-') {
                            return row.deskripsi1;
                        } else {
                            return row.deskripsi1 + ' - ' + row.deskripsi2;
                        }
                    }
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // search pt mstr
    $(document).on('click', '.searchPTMstr', function(){
        $('#get-ptmstr').DataTable().clear().destroy()
        getPtMstr();
    })

    // select ptmstr
    $(document).on('click', '#selectptmstr', function() {
        var itemnumber = $(this).data('itemnumber')
        var desc = $(this).data('desc')

        $('#item_number').val(itemnumber)
        $('#desc').val(desc)
        $('#modalptmstrbydesc').click()
    })

    $(document).on('click', '#seacrhSOAPBusinesRelationModal', function(){
        $('#get-supplier').DataTable().clear().destroy()
        getSupplier();
    })

    $('#search').bind('click', function(event) {
        if($('#input-search-item_number').val() || $('#input-search-supplier_id').val() || $('#input-search-supplier_name').val()) {
            $('.t-automatical').css('display', 'flex')
        } else {
            $('.t-automatical').css('display', 'none')
        }
    })

    // reset
    $('#reset').click(function() {
        $('#input-search-item_number').val('')
        $('#input-search-supplier_id').val('')
        $('#input-search-supplier_name').val('')
        $('.t-automatical').css('display', 'none')
        $('#main-table').DataTable().destroy()
        main_table()
    })
})

// close
$('.close').click(function(e) {
    $('#ukanban_pall_validation').text('')
    $('#usnp_kanban_validation').text('')
    $('#uback_no_validation').text('')
    $('#ucode_validation').text('')
    $('#udesc_validation').text('')
    $('#uitem_number_validation').text('')
    $('#desc').val('')
})

// change thead th datatable font color
$("table thead th").css("color", "#fff");

//change model footer
$(".modal-footer").css("justify-content", "flex-start");

// update
$(document).on('click', '#updateItemKanban', function(e) {
    e.preventDefault()

    // getLoader
    getLoader()

    Swal.fire({
        title: 'update back no, snp kanban & kanban pallet ? ',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.value) {
            var form = new FormData();

            var back_no = []
            var pcs_kanban = []
            var pallet_kanban = []

            $('input[name="back_no[]"]').each(function(){
                back_no.push(this.value);
            });

            $('input[name="pcs_kanban[]"]').each(function(){
                pcs_kanban.push(this.value);
            });

            $('input[name="pallet_kanban[]"]').each(function(){
                pallet_kanban.push(this.value);
            });

            var lengthin = back_no.length - 1

            for (var i = 0; i <= lengthin; i++) {
                form.append("_method", "PUT");
                form.append("back_no" + "[" + i + "]", back_no[i])
                form.append("pcs_kanban" + "[" + i + "]", pcs_kanban[i])
                form.append("pallet_kanban" + "[" + i + "]", pallet_kanban[i])
            }

            var queryItemNumber = $('#input-search-item_number').val()
            var querySupplierId = $('#input-search-supplier_id').val()
            var querySupplierName = $('#input-search-supplier_name').val()

            if(queryItemNumber != '') {
                var settings = {
                    "url": `${BASEURL}item-kanbans/` + queryItemNumber,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Accept": "application/json"
                    },
                    "processData": false,
                    "mimeType": "multipart/form-data",
                    "contentType": false,
                    "data": form,
                    error: function(jqXHR, textStatus, errorThrown) {
                        var err = JSON.parse(jqXHR.status)
                        var errText = JSON.parse(jqXHR.responseText)
                        if (err == 500) {
                            SW.error({
                                message: 'please enter the correct keywords'
                            });
                        }
                    }
                };
            } else if(querySupplierId != '' ) {
                var settings = {
                    "url": `${BASEURL}item-kanbans/` + querySupplierId,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Accept": "application/json"
                    },
                    "processData": false,
                    "mimeType": "multipart/form-data",
                    "contentType": false,
                    "data": form,
                    error: function(jqXHR, textStatus, errorThrown) {
                        var err = JSON.parse(jqXHR.status)
                        var errText = JSON.parse(jqXHR.responseText)
                        if (err == 500) {
                            SW.error({
                                message: 'please enter the correct keywords'
                            });
                        }
                    }
                };
            }else if(querySupplierName != '' ) {
                var settings = {
                    "url": `${BASEURL}item-kanbans/` + querySupplierName,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Accept": "application/json"
                    },
                    "processData": false,
                    "mimeType": "multipart/form-data",
                    "contentType": false,
                    "data": form,
                    error: function(jqXHR, textStatus, errorThrown) {
                        var err = JSON.parse(jqXHR.status)
                        var errText = JSON.parse(jqXHR.responseText)
                        if (err == 500) {
                            SW.error({
                                message: 'please enter the correct keywords'
                            });
                        }
                    }
                };
            } else {
                if(queryItemNumber == '' && querySupplierId == '' && querySupplierName == '') {
                    SW.error({
                        message: 'search data does not exist yet'
                    });
                    return
                }
            }

            $.ajax(settings).done(function(response) {
                let data = JSON.parse(response);

                SW.success({
                    message: data.message
                });

                location.reload();
            });
        }
    })
})

// insert
$(document).on('click', '#itemkanbaninsertbtn', function(e) {
    e.preventDefault()

    // getLoader
    getLoader()

    var form = new FormData();
    form.append("item_number", $('input[name = "item_number"]').val())
    form.append("desc", $('input[name = "desc"]').val())
    form.append("code", $('input[name = "code"]').val())
    form.append("back_no", $('input[name = "back_no"]').val())
    form.append("snp_kanban", $('input[name = "snp_kanban"]').val())
    form.append("kanban_pall", $('input[name = "kanban_pall"]').val())

    var settings = {
        "url": `${BASEURL}item-kanbans`,
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Accept": "application/json",
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        error: function(jqXHR, textStatus, errorThrown) {
            var err = JSON.parse(jqXHR.responseText)
            var err2 = err.errors
            if (err2.item_number) {
                $('#uitem_number_validation').text(err2.item_number)
            } else {
                $('#uitem_number_validation').text('')
            }
            if (err2.desc) {
                $('#udesc_validation').text(err2.desc)
            } else {
                $('#udesc_validation').text('')
            }
            if (err2.code) {
                $('#ucode_validation').text(err2.code)
            } else {
                $('#ucode_validation').text('')
            }
            if (err2.back_no) {
                $('#uback_no_validation').text(err2.back_no)
            } else {
                $('#uback_no_validation').text('')
            }
            if (err2.snp_kanban) {
                $('#usnp_kanban_validation').text(err2.snp_kanban)
            } else {
                $('#usnp_kanban_validation').text('')
            }
            if (err2.kanban_pall) {
                $('#ukanban_pall_validation').text(err2.kanban_pall)
            } else {
                $('#ukanban_pall_validation').text('')
            }
        },
    };

    $.ajax(settings).done(function(response) {
        var data1 = JSON.parse(response)
        $('#ukanban_pall_validation').text('')
        $('#usnp_kanban_validation').text('')
        $('#uback_no_validation').text('')
        $('#ucode_validation').text('')
        $('#udesc_validation').text('')
        $('#uitem_number_validation').text('')
        $('#modaldemo8').click();
        // var oTable = $('#main-table').DataTable()
        // oTable.draw(false)
        Swal.fire(
            data1.message,
            'silahkan klik tombol ok!',
            'success'
        )

        location.reload();
    });
})
