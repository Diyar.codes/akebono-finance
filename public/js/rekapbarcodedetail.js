$(()=> {
    $('#dataSecondTable').DataTable({
        responsive: false,
        paging: false,
        ordering : false,
    });
    $("table thead th").css("color", "#fff");

    fetch_customer_data()

    function fetch_customer_data(bulan = '', tahun = '', itemnumber = '') {
        getLoader()
        $.ajax({
            type: "GET",
            url: "/rekap-barcode-detail/table",
            data:{
                bulan : bulan,
                tahun : tahun,
                itemnumber : itemnumber,
            },
            success: function (data) {
                console.log(data);
                // $('.secondtable').show();
                $('#dataSecondTable').DataTable().destroy()
                $('.secondtable').find('.tblseconded').html(data.tablenya);
                $('#dataSecondTable').DataTable( {
                    responsive: false,
                    paging: false,
                    ordering : false,
                    scrollX: true,
                });
            },
            error: function(error) {
                SW.error({
                    message: 'Data is not Defined!!',
                });
            }
        });
    }

    // search
    $(document).on('click', '#search', function() {
        var bulan   = $('#bulan').val()
        var tahun   = $('#tahun').val()
        var itemnumber   = $('#itemnumber').val()

        if(bulan != '' || tahun != '' || itemnumber != '') {
            $('#dataSecondTable').DataTable().destroy()
            fetch_customer_data(bulan, tahun, itemnumber)
        } else {
            $('#dataSecondTable').DataTable().destroy()
            fetch_customer_data()
        }
    })
    $('#reset').click(function() {
        location.reload();
    })
});
