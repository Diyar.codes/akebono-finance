$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
$('#tablenya').DataTable({
    ordering:false,
    language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
    }
});
function print_po(){
    getLoader();
    var po = $('#po').val();
    var bulan = $('#bulan').val();
    var tahun = $('#tahun').val();
    // alert(tahun);

        $.ajax({
            url:`${BASEURL}act-print-po`,
            type:"GET",
            data:{
                po: po,
                bulan:bulan,
                tahun:tahun
            },
            success: function (data) {
                $('#tablenya').DataTable().destroy();
                $('#tablenya tbody tr').remove();
                $('.secondtable').show();
                $('.secondtable').find('#result').html(data);
                $('#tablenya').DataTable({
                    responsive: true,
                    ordering:false,
                    language: {
                        searchPlaceholder: 'Search...',
                        sSearch: '',
                        lengthMenu: '_MENU_ items/page',
                    }
                });
            },
            error: function(error) {
                alert('Data Not Found');
            }
        });
}

function reset(){
    getLoader();
    var po = 0;
    var bulan = 0;
    var tahun = 0;

    $.ajax({
        url:`${BASEURL}act-print-po`,
        type:"GET",
        data:{
            po: po,
            bulan:bulan,
            tahun:tahun
        },
        success: function (data) {
            $('#tablenya').DataTable().destroy();
            $('#tablenya tbody tr').remove();
            $('.secondtable').show();
            $('.secondtable').find('#result').html(data);
            $('#po').val('');
            $('#bulan').val('');
            $('#tahun').val('');
            $('#tablenya').DataTable({
                responsive: true,
                ordering:false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data Not Found');
        }
    })
}