
$(document).ready(function() {

    $("table thead th").css("background-color", "#0066CC");
    $("table thead th").css("color", "#fff");
    $('#tabelnya').DataTable({
        responsive: true,
        paging : false,
        ordering:false,
        scrollY: "400px",
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
        }
    });
    var po = document.getElementById("nomor_po").value;
    var tr = document.getElementById("tr_id_enc").value;
    getLoader();
    $.ajax({
        type: "GET",
        url: "/generate-ship-do-po",
        data:{
            po : po,
            tr:tr
        },
        success: function (data) {
            $('#tabelnya').DataTable().destroy();
            $('.secondtable').find('#result').html(data);
            $('#tabelnya').DataTable({
                // responsive: true,
                ordering:false,
                paging : false,
                scrollY: "400px",
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });

});



function cekdata(urut) {
    var qty_open = $('[id^="qty_open"]').map(function () { return $(this).val(); }).get();
    var qty_ship = $('[id^="qty_ship"]').map(function () { return $(this).val(); }).get();
    var hitung = urut - 1;
    if(parseInt(qty_ship[hitung]) > parseInt(qty_open[hitung])){
        SW.error({
                message: 'Over Qty Order !!',
        });
        document.getElementById("qty_ship["+parseInt(urut)+"]").value = '';
    }
}


function save(){
    getLoader();
    var qty_ship    = $('[id^="qty_ship"]').map(function () { return $(this).val(); }).get();
    var item_number = $('[id^="item_number"]').map(function () { return $(this).val(); }).get();
    var line        = $('[id^="line"]').map(function () { return $(this).val(); }).get();
    var po_um       = $('[id^="po_um"]').map(function () { return $(this).val(); }).get();
    var tr_id       = document.getElementById("tr_id").value;
    var tgl         = document.getElementById("tgl").value;
    var po          = document.getElementById("po").value;
    var cycle       = document.getElementById("cycle").value;
    var packingslip = document.getElementById("packingslip").value;
    var jml_row     = document.getElementById("jml_row").value;
    if(cycle == ""){
        SW.error({
            message: 'Please Choose Cycle',
        });
    }else if(packingslip == ""){
        SW.error({
            message: "Packing Slip Can't Empty",
        });
    }else{
        $.ajax({
            type: "GET",
            url: "/insert-delivery-order-po",
            data:{
                qty_ship : qty_ship,
                tr_id : tr_id,
                tgl : tgl,
                po : po,
                cycle : cycle,
                packingslip:packingslip,
                item_number:item_number,
                line:line,
                po_um:po_um,
                jml_row:jml_row
            },
            success: function (data) {
                if(data != "berhasil"){
                    SW.error({
                        message: 'Packing Slip has been recorded in database',
                    });
                }else{
                    SW.success({
                        message: 'Create Delivery Order Successfully',
                        
                    });
                    var url = `${BASEURL}delivery-order-po`;
                    
                    setTimeout(function(){ 
                        window.location.href = url;
                    }, 1000);
                    
                }
            },
            error: function(error) {
                alert('Data is not Defined!!!');
            }
        });
    }
}