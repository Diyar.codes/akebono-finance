
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

$('#main-table').DataTable({
    "ordering": false,
})

document.getElementById('searchBlDate').addEventListener('click', () => {
    getLoader()
    let month = document.getElementById('month').value
    let year = document.getElementById('year').value

    if (month == 0 && year == 0) {
        SW.error({
            message: 'Input Month and Year!'
        });
    } else {
        $.ajax({
            url: `${BASEURL}pending-intrans`,
            type: "GET",
            data: {
                month: month,
                year: year
            },
            success: function (hasil) {
                let obj = JSON.parse(hasil);
                document.getElementById('dataBody').innerHTML = obj.data
            }
        })
    }

})