$(()=> {
    $('#datatables').DataTable({
        searching: true,
        ordering : false,
        paging: false,
        processing: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
        }
    });

    $("table thead th").css("color", "#fff");
});