

$('#tabelnya').DataTable({
    responsive: true,
    ordering:false,
    language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
    }
});
$(".table-bordered thead th").css("background-color", "#0066CC");
$("#tabelnya thead th").css("color", "#fff");

function search(){
    getLoader();
    var po      = $('#po').val();
    var item    = $('#item').val();
    var rc      = $('#rc').val();
    var bulan   = $('#bulan').val();
    var tahun   = $('#tahun').val();
    if(po == ""){
        SW.error({
            message: "PO Can't Empty",
        });
    }else{

        $.ajax({
            type: "GET",
            url: "/search-view-receipt",
            data:{
                po : po,
                item : item,
                rc:rc,
                bulan : bulan,
                tahun : tahun,
            },
            success: function (data) {
                $('#tabelnya').DataTable().destroy();
                $('#tabelnya tbody tr').remove();
                $('#item').val("");
                $('#rc').val("");
                $('.secondtable').show();
                $('.secondtable').find('#tblsecond').html(data);
                $('#tabelnya').DataTable({
                    responsive: true,
                    ordering:false,
                    language: {
                        searchPlaceholder: 'Search...',
                        sSearch: '',
                        lengthMenu: '_MENU_ items/page',
                    }
                });
            },
            error: function(error) {
                alert('Data is not Defined!!!');
            }
        });
    }
}

function reset(){
    getLoader();
    var po      = "";
    var item    = "";
    var rc      = "";
    var bulan   = "";
    var tahun   = "";
    var mod     = "reset";
   $.ajax({
        type: "GET",
        url: "/search-view-receipt",
        data:{
            po : po,
            item : item,
            rc:rc,
            bulan : bulan,
            tahun : tahun,
            mod : mod,
        },
        success: function (data) {
            $('#tabelnya').DataTable().destroy();
            $('#tabelnya tbody tr').remove();
            $('#po').val("");
            $('#item').val("");
            $('#rc').val("");
            $('.secondtable').show();
            $('.secondtable').find('#tblsecond').html(data);
            $('#tabelnya').DataTable({
                ordering:false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}