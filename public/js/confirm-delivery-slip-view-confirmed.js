$(()=> {
    var no = window.location.pathname.split("/").pop()

    main_table()

    function main_table() {
        getLoader()

        $.ajax({
            type: "GET",
            url: `/confirm-delivery-slip-view-confirmed-main/` + no,
            success: function (data) {
                $('.main-table').find('.bodytable').html(data.main);
                $('#main-table').DataTable( {
                    responsive: true,
                    paging: true,
                    searching: false,
                    ordering : false,
                });
            },
        });
    }
})

