$(() => {
    // view data supplier
    $('#main-tablee').DataTable({
        // processing: true,
        responsive: true,
        // serverSide: true,
        ajax: {
            url: `${BASEURL}get-supplier`,
        },
        columns: [
            {
                data: 'ct_vd_addr',
                name: 'supplier id',
            },
            {
                data: 'ct_ad_name',
                name: 'supplier name',
            },
            {
                data: 'action',
                name: 'action',
            },
        ],
    })

    // select modal
    $(document).on('click', '#selectbusiness', function () {

        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        $('#name').val(namebusiness)
        $('#modaldemo8part2').click()
    })

})


function searchSupp() {
    getLoader();
    filter_id_supp = document.getElementById('code').value;
    filter_no_surat = document.getElementById('no_surat').value;
    filter_month = document.getElementById('month').value;
    filter_year = document.getElementById('year').value;

    if(filter_id_supp == '' && filter_no_surat == '' && filter_month == '' && filter_year == ''){
        SW.error({
            message: 'enter one of the forms'
        });
    }else{
        $.ajax({
            url: `${BASEURL}delivery-item-pending`,
            method: "GET",
            data: {
                filter_id_supp: filter_id_supp,
                filter_no_surat: filter_no_surat,
                filter_month: filter_month,
                filter_year: filter_year,
            },
            success: function (data) {
                let viewHtml = '';
                obj = JSON.parse(data)
                if (obj.status == false) {
                    
                } else {
                    let no = 0;
                    obj.data.forEach(el => {
                        viewHtml += `
                        <tr>
                            <td>${no+1} </td>
                            <td>${el.no} <input type="hidden" name="no${no+1}" value="${el.no}"></td>
                            <td>${el.kode} <input type="hidden" name="kode${no+1}" value="${el.kode}"></td>
                            <td>${el.act_qty} <input type="hidden" name="qty${no+1}" value="${el.act_qty}"></td>
                            <td>${el.ct_ad_name}</td>
                            <td>${el.tanggal} <input type="hidden" name="tgl${no+1}" value="${el.tanggal}"></td>
                            <td>${el.keterangan} <input type="hidden" name="tgl${no+1}" value="${el.keterangan}"></td>
                            <td>${el.remarks == null ? '-' : el.remarks}</td>
                            <td><input type="checkbox" name="check${no+1}" id="check${no+1}"></td>
                        </tr>
                        `;
                        no++
                    });
                }
                let setJumlah = obj.data.length;
                let setHtml = document.getElementById('jumlah');
                let setAttr = document.createAttribute('value');
                setAttr.value = setJumlah;
                setHtml.setAttributeNode(setAttr);
                document.getElementById('result').innerHTML = viewHtml;
            }
        })
    }


    
}

// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
