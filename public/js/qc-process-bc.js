// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

$(()=> {
    // gate data
    main_table()

    // view data
    function main_table(transaction_id = '', supplier_id = '', filter_moon = '', filter_year = '') {
        // getLoader
        getLoader()

        $('#main-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            searching: false,
            paging: true,
            ordering : false,
            // scrollY: "80vh",
            // scrollX: true,
            // scrollCollapse: true,
            // fixedColumns: {
            //     leftColumns: 0,
            //     rightColumns: 0
            // },
            ajax: {
                url: `${BASEURL}qc-process-bc`,
                data: {
                    transaction_id: transaction_id,
                    supplier_id: supplier_id,
                    filter_moon: filter_moon,
                    filter_year: filter_year,
                }
            },
            columns:[
                {
                    data: 'portald_tr_id',
                    name: 'portald_tr_id',
                },
                {
                    data: 'portal_dlv_date',
                    name: 'portal_dlv_date',
                },
                {
                    data: 'portald_po_nbr',
                    name: 'portald_po_nbr',
                },
                {
                    data: 'portal_vend',
                    name: 'portal_vend',
                },
                {
                    data: 'portal_ps_nbr',
                    name: 'portal_ps_nbr',
                },
                {
                    data: 'portal_user_id',
                    name: 'portal_user_id',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // filter
    $('#filter').click(function() {
        var transaction_id = $('#transaction_id').val()
        var supplier_id = $('#supplier_id').val()
        var filter_moon = $('#filter_moon').val()
        var filter_year = $('#filter_year').val()

        if(transaction_id != '' || supplier_id != '' || filter_moon != '' || filter_year != '') {
            $('#main-table').DataTable().clear().destroy()
            main_table(transaction_id, supplier_id, filter_moon, filter_year)
        } else {
            $('#main-table').DataTable().clear().destroy()
            main_table()
        }
    })

    // reset]
    $('#reset').click(function() {
        $('#transaction_id').val('')
        $('#supplier_id').val('')
        $('#filter_moon').val('')
        $('#filter_year').val('')
        $('#main-table').DataTable().clear().destroy()
        main_table()
    })

})
