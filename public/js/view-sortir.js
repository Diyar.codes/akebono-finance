// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

$(()=> {
    $('#main-table').DataTable({
        processing: true,
        paging: true,
        responsive: true,
        // scroller: {
        //     rowHeight: 1000
        // },
        // fixedHeader:{
        //     header:true,
        //     headerOffset: $('#fixed').height(),
        // },
        serverSide: true,
        searching: true,
        ordering : false,
        ajax: {
            url: `${BASEURL}view-sort`,
        },
        columns:[
            {
                data: 'tanggal',
                name: 'tanggal',
            },
            {
                data: 'supplier_id',
                name: 'supplier_id',
            },
            {
                data: 'problem_desc',
                name: 'problem_desc',
            },
            {
                data: 'item_nbr',
                name: 'item_nbr',
            },
            {
                data: 'qty_ok',
                name: 'qty_ok',
            },
            {
                data: 'qty_ng',
                name: 'qty_ng',
            },
            {
                data: 'qty_sortir',
                name: 'qty_sortir',
            },
            {
                data: 'jamstart',
                name: 'jamstart',
            },
            {
                data: 'jamend',
                name: 'jamend',
            },
            {
                data: 'pic_sortir',
                name: 'pic_sortir',
            },
            {
                data: 'pic_aaij',
                name: 'pic_aaij',
            },
        ],
    })
})
