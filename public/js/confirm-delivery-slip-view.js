$(()=> {
    var no = window.location.pathname.split("/").pop()

    main_table()

    function main_table() {
        getLoader()

        $.ajax({
            type: "GET",
            url: `/confirm-delivery-slip-view-main/` + no,
            success: function (data) {
                $('.main-table').find('.bodytable').html(data.main);
                $('#main-table').DataTable( {
                    responsive: false,
                    paging: true,
                    scrollY: "70vh",
                    scrollX: true,
                    scrollCollapse: true,
                    fixedColumns: {
                        leftColumns: 0,
                        rightColumns: 0
                    },
                    searching: false,
                    ordering : false,
                    paging: false,
                });
            },
            error: function(error) {
                SW.error({
                    message: 'Data is not Defined!!',
                });
            }
        });
    }

    $(document).on('click', '#save', function(e) {
        e.preventDefault()

        // getLoader
        getLoader()

        var item = []
        var act = []
        var remark = []

        $('input[name="aaij_qty[]"]').each(function(){
            item.push(this.value);
        });

        $('input[name="act_qty[]"]').each(function(){
            act.push(this.value);
        });

        $('input[name="remark[]"]').each(function(){
            remark.push(this.value);
        });

        var lengthin = item.length - 1

        if(lengthin >= 0) {
            var check = [];
            for (var i = 0; i <= lengthin; i++) {
                if(item[i] != act[i] && remark[i] == '') {
                    SW.error({
                        message: 'Enter the remark because the Qty sent by AAIJ is different from the Qty received'
                    });
                } else {
                    check.push('oke')
                }
            }

            if(check.length == item.length) {
                Swal.fire({
                    title: 'cover letter confirmation',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes!',
                    cancelButtonText: 'No!'
                }).then((result) => {
                    if (result.value) {
                        var form = new FormData();

                        for (var i = 0; i <= lengthin; i++) {
                            form.append("_method", "PUT");
                            form.append("aaij_qty" + "[" + i + "]", item[i])
                            form.append("act_qty" + "[" + i + "]", act[i])
                            form.append("remark" + "[" + i + "]", remark[i])
                        }

                        var file = $('input[name = "attachment"]')[0].files[0];

                        if(file) {
                            form.append("attachment", file);
                        }

                        var settings = {
                            "url": `${BASEURL}update-confirm-delivery-slip-view/` + no,
                            "method": "POST",
                            "timeout": 0,
                            "headers": {
                                "Accept": "application/json"
                            },
                            "processData": false,
                            "mimeType": "multipart/form-data",
                            "contentType": false,
                            "data": form,
                            error: function(jqXHR, textStatus, errorThrown) {
                                var err = JSON.parse(jqXHR.status)
                                var errText = JSON.parse(jqXHR.responseText)
                                if (err == 500) {
                                    SW.error({
                                        message: 'There is something wrong'
                                    });
                                }
                            }
                        };

                        $.ajax(settings).done(function(response) {
                            let data = JSON.parse(response);

                            SW.success({
                                message: data.message
                            });

                            window.location.href = `${BASEURL}confirm-delivery-slip`;
                        });
                    }
                })
            }
        }
    })
})

