$(()=> {
    // get data
    fill_datatable()

    // view data
    function fill_datatable(no_surat ='', moon = '', year = '') {
        // getLoader
        getLoader()

        $('#main-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            paging: true,
            searching: false,
            ordering : false,
            ajax: {
                url: `${BASEURL}confirm-delivery-slip`,
                data: {
                    no_surat: no_surat,
                    moon: moon,
                    year: year,
                }
            },
            rowCallback: function(row, data, index){
                if (data['app_supplier'] == 'Confirmed') {
                    $(row).css('backgroundColor', '#99FFFF');
                }
            },
            columns:[
                {
                    data: 'no',
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {
                    data: 'no',
                    name: 'no',
                },
                {
                    data: 'tanggal',
                    name: 'tanggal',
                },
                {
                    data: 'kendaraan',
                    name: 'kendaraan',
                },
                {
                    data: 'line',
                    name: 'line',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // get function SOAP Business relation
    $(document).on('click', '#search-businessrelation', function(){
        $('#main-businessrelation').DataTable().clear().destroy()
        getSupplier();
    })

    // view data supplier
    function getSupplier() {
        $('#main-businessrelation').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'supplier id',
                },
                {
                    data: 'ct_ad_name',
                    name: 'supplier name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // select modal business relation
    $(document).on('click', '#selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        $('#name').val(namebusiness)
        $('#modal-businesrelation').click()
    })

    // search data
    $(document).on('click', '#searchbusinessrelation', function() {
        var no_surat = $('#no_surat').val()

        $('#moon').val('')
        $('#year').val('')

        if(no_surat != '') {
            $('#main-table').DataTable().clear().destroy()
            fill_datatable(no_surat)
        } else if(no_surat == '') {
            SW.warning({
                message: 'letter number must be filled',
            })
            $('#main-table').DataTable().clear().destroy()
        } else {
            $('#main-table').DataTable().clear().destroy()
            searchReportMutasiBulanan()
        }
    })

    // search data
    $(document).on('click', '#searchperiode', function() {
        $('.t-automatical').css('display', 'flex')

        var code = $('#code').val()
        var moon = $('#moon').val()
        var year = $('#year').val()

        $('#moonExcel').val(moon)
        $('#yearExcel').val(year)

        $('#no_surat').val('')

        if(moon != '' && year != '') {
            $('#main-table').DataTable().clear().destroy()
            fill_datatable(code, moon, year)
        } else if(moon == '' && year != '') {
            SW.warning({
                message: 'Month Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else if(year == '' && moon != '') {
            SW.warning({
                message: 'Year Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else if(year == '' && moon == '') {
            SW.warning({
                message: 'period must be filled in completely',
            })
            $('#main-table').DataTable().clear().destroy()
        } else {
            $('#main-table').DataTable().clear().destroy()
            searchReportMutasiBulanan()
        }
    })

    // reset businessrelation
    $('#resetbusinessrelation').click(function() {
        $('#moonExcel').val('')
        $('#yearExcel').val('')
        $('#moon').val('')
        $('#year').val('')
        $('#main-table').DataTable().clear().destroy()
        fill_datatable()
    })

    // reset periode
    $('#resetperiode').click(function() {
        $('.t-automatical').css('display', 'none')

        $('#moonExcel').val('')
        $('#yearExcel').val('')
        $('#no_surat').val('')
        $('#moon').val('')
        $('#year').val('')
        $('#main-table').DataTable().clear().destroy()
        fill_datatable()
    })
})
