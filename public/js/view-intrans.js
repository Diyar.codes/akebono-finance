$(() => {
    // view data supplier
    $('#main-tablee').DataTable({
        // processing: true,
        responsive: true,
        // serverSide: true,
        ajax: {
            url: `${BASEURL}get-supplier`,
        },
        columns: [
            {
                data: 'ct_vd_addr',
                name: 'supplier id',
            },
            {
                data: 'ct_ad_name',
                name: 'supplier name',
            },
            {
                data: 'action',
                name: 'action',
            },
        ],
    })


    // select modal
    $(document).on('click', '#selectbusiness', function () {

        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        $('#name').val(namebusiness)
        $('#searchSupplier').click()
    })

    // change thead th datatable color
    $("thead th").css("background-color", "#0066CC");

    // change thead th datatable font color
    $("thead th").css("color", "#fff");

    $('#main-table').DataTable({
        ordering: false
    })


    /**
     * search container
     */
    document.getElementById('searchCountairModal').addEventListener('click', () => {
        $.ajax({
            url: `${BASEURL}view-intrans-container`,
            method: "GET",
            data: {
                'data': true,
            },
            success: function (data) {
                $('#main-tablee2').dataTable().fnClearTable();
                $('#main-tablee2').dataTable().fnDestroy();  
                let obj = JSON.parse(data)
                let bodyContainer = document.getElementById('bodyContainer');
                let viewHtml = '';
                if (obj.status == false) {
                    console.log('data kosong');
                } else {
                    let i = 1
                    obj.data.forEach(el => {
                        viewHtml += `
                    <tr>
                        <td>${el.int_inv}</td>
                        <td>${el.int_countainer_packaging == null ? '-' : el.int_countainer_packaging}</td>
                        <td><button class="btn btn-sm btn-warning closeContainer" aria-label="Close" data-dismiss="modal" value="${el.int_countainer_packaging == null ? '' : el.int_countainer_packaging}" aria-label="Close" data-dismiss="modal"><span class="fas fa-check ok"></span> Click</button></td>
                    </tr>`;
                        i++;
                    });
                }
                bodyContainer.innerHTML = viewHtml
                $('#main-tablee2').DataTable({
                    ordering: false,
                })

                bodyContainer.addEventListener('click', value => {
                    if (value.target.className == 'btn btn-sm btn-warning closeContainer') {
                        document.getElementById('countainer').setAttribute('value', value.target.getAttribute('value'))
                    }
                    if (value.target.className == 'fas fa-check ok') {
                        document.getElementById('countainer').setAttribute('value', value.target.parentElement.getAttribute('value'))
                    }            
                })
            }
        })
    })


    /**
     * end search container
     */
})


function SearchViewIntrans() {
    getLoader()
    bulan = document.getElementById('bulan').value;
    tahun = document.getElementById('tahun').value;
    code = document.getElementById('code').value;
    countainer = document.getElementById('countainer').value;

    console.log(countainer)

    $.ajax({
        url: `${BASEURL}view-intrans`,
        method: "GET",
        data: {
            'bulan': bulan,
            'tahun': tahun,
            'code': code,
            'countainer': countainer
        },
        success: function (data) {
            obj = JSON.parse(data)
            if (obj.status == false) {
                if(obj.data == false){
                    SW.warning({
                        message: 'Input Month and Year!',
                    });
                }else{
                    let viewHtml = `
                        <tr class="text-center">
                            <td colspan="11">No data available in table</td>
                        </tr>
                    `;
                    document.getElementById('debug').innerHTML = viewHtml
                }
            } else {

                no = 0;
                viewHtml = '';
                viewHtml += `
                <table class="display" id="main-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Supplier</th>
                            <th>Invoice Number</th>
                            <th>ETD Date</th>
                            <th>Lead Time Shipment</th>
                            <th>Name of Ship</th>
                            <th>Container</th>
                            <th>Shift</th>
                            <th>Penerima</th>
                            <th>Lokasi</th>
                            <th>Confirm Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                `;
                obj.data.forEach(el => {

                    viewHtml += `
                    <tr>
                        <td>${no + 1}</td>
                        <td>${el.int_supp} | ${el.ct_ad_name}</td>
                        <td>${el.int_inv != null ? el.int_inv : '-'}</td>
                        <td>${el.int_etd != null ? el.int_etd : '-'}</td>
                        <td style="background-color: #FFCC80">${el.estimasi_tgl_kedatangan != null ? el.estimasi_tgl_kedatangan : '-'}</td>
                        <td>${el.int_ship != null ? el.int_ship : '-'}</td>
                        <td>${el.int_countainer_packaging != null ? el.int_countainer_packaging : '-'}</td>
                        <td>${el.int_shift != null ? el.int_shift : '-'}</td>
                        <td>${el.int_penerima_barang != null ? el.int_penerima_barang : '-'}</td>
                        <td>${el.int_lokasi != null ? el.int_lokasi : '-'}</td>
                        <td>${el.confirm != null ? el.confirm : '-'}</td>
                        <td><a class="btn btn-sm btn-warning" href="/view-intrans-detail/${el.int_supp_encode}/${el.int_inv_encode}"><span class="fas fa-eye"></span></a></td>
                    </tr>
                    `;
                    no++;
                });
                viewHtml += `</tbody></table>`;

                body = document.getElementById('result');
                body.innerHTML = viewHtml
                // change thead th datatable color
                $("thead th").css("background-color", "#0066CC");

                // change thead th datatable font color
                $("thead th").css("color", "#fff");
                $('#main-table').DataTable({
                    ordering: false
                })
            }
        }
        // view-intrans
    })
}

