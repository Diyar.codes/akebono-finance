$(()=> {
    // function search report delivery slip
    function searchReportMutasiBulanan(moon = '', year = '', code = ''){
        // loader
        getLoader()

        $.ajax({
            url:`${BASEURL}search-monthly-ds-report`,
            type:"GET",
            data:{
                moon: moon,
                year: year,
                code: code
            },
            success: function (data) {
                getLoader()
                $('#bodytable').html(data['table']);
                // $('#main-table').DataTable({
                    // scrollY: "70vh",
                    // scrollX: true,
                    // scrollCollapse: true,
                    // fixedColumns: {
                    //     leftColumns: 0,
                    //     rightColumns: 0
                    // },
                    // searching: false,
                    // ordering : false,
                    // paging: false
                // });
            }
        });
    }

    // get function SOAP Business relation
    $(document).on('click', '#search-businessrelation', function(){
        $('#get-supplier').DataTable().clear().destroy()
        getSupplier();
    })

    // function SOAP Business relation
    function getSupplier() {
        // view data supplier
        $('#get-supplier').DataTable({
            // processing: true,
            responsive: true,
            // serverSide: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'supplier id',
                },
                {
                    data: 'ct_ad_name',
                    name: 'supplier name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // select modal business relation
    $(document).on('click', '#selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        $('#name').val(namebusiness)
        $('#modal-businesrelation').click()
    })

    // search data
    $(document).on('click', '#search', function() {
        var code = $('#code').val()
        var moon = $('#moon').val()
        var year = $('#year').val()

        if(moon != '' && year != '' && code != '') {
            $('#main-table').DataTable().clear().destroy()
            searchReportMutasiBulanan(moon, year, code)
        } else if(code == '') {
            SW.warning({
                message: 'Supplier Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else if(moon == '') {
            SW.warning({
                message: 'Month Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else if(year == '') {
            SW.warning({
                message: 'Year Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else {
            $('#main-table').DataTable().clear().destroy()
            searchReportMutasiBulanan()
        }
    })

     // reset
    $('#reset').click(function() {
        // $('#code').val('')
        // $('#moon').val('')
        // $('#year').val('')
        // $('#name').val('')
        // $('#main-table').DataTable().clear().destroy()
        // searchReportMutasiBulanan()
    })
})
