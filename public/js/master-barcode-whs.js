$(()=> {
    $('#dataSecondTable').DataTable({
        responsive: false,
        paging: false,
        ordering : false,
        scrollY: "70vh",
        scrollX: true,
        scrollCollapse: true,
        fixedColumns: {
            leftColumns: 0,
            rightColumns: 0
        },
    });

    fetch_customer_data()

    function fetch_customer_data(itemnumber = '', locationfrom = '', locationto = '') {
        // getLoader
        getLoader()
        $.ajax({
            type: "GET",
            url: "/data-master-barcode-whs",
            data:{
                itemnumber : itemnumber,
                locationfrom : locationfrom,
                locationto : locationto,
            },
            success: function (data) {
                // $('.secondtable').show();
                $('#dataSecondTable').DataTable().destroy()
                $('.secondtable').find('.tblseconded').html(data.tablenya);
                $('#dataSecondTable').DataTable( {
                    responsive: false,
                    paging: false,
                    ordering : false,
                    scrollY: "70vh",
                    scrollX: true,
                    scrollCollapse: true,
                    fixedColumns: {
                        leftColumns: 0,
                        rightColumns: 0
                    },
                });
            },
            error: function(error) {
                SW.error({
                    message: 'Data is not Defined!!',
                });
            }
        });
    }

    // change thead th datatable color
    $(".table-bordered thead th").css("background-color", "#0066CC");

    $("table thead th").css("color", "#fff");
    // search
    $(document).on('click', '#search', function() {
        var itemnumber      = $('#itemnumber').val()
        var locationfrom    = $('#locationfrom').val()
        var locationto      = $('#locationto').val()

        if(itemnumber != '' || locationfrom != '' || locationto != '') {
            $('#dataSecondTable').DataTable().destroy()
            fetch_customer_data(itemnumber, locationfrom, locationto)
        } else {
            $('#dataSecondTable').DataTable().destroy()
            fetch_customer_data()
        }
    })


    $(document).on('click', '.searchLocMaster', function(){
        var item = $(this).data('item');
        $('.itemm').val(item)
        $('.main-tablee').DataTable().clear().destroy()
        getLocMaster();
    })

    // $(document).on('click', '#searchLocMasterLocTo', function(){
    //     var item = $(this).data('item');
    //     $('#itemto').val(item)
    //     $('#main-tableto').DataTable().clear().destroy()
    //     getLocMasterTo();
    // })

    $(document).on('click', '#selectLocMstr', function(){
        var loc = $(this).data('loc');
        var id = $('.itemm').val();

    
        $(`#${id}`).val(loc);

        $('#location').modal('hide');
        
    })

    $('#reset').click(function(){
        $('#itemnumber').val('')
        $('#locationfrom').val('')
        $('#locationto').val('')
        $('#dataSecondTable').DataTable().destroy()
        fetch_customer_data()
    });

    // $(document).on('click','#selectLocMstr',function(){
    //     $(this).data('')
    // })

    function getLocMaster() {
        // view data supplier
        $('.main-tablee').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url: '/get-loc-master',
            },
            columns:[
                {
                    data: 'locLoc',
                    name: 'locLoc',
                },
                {
                    data: 'locDesc',
                    name: 'locDesc',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }
    // function getLocMasterTo() {
    //     // view data supplier
    //     $('#main-tableto').DataTable({
    //         processing: true,
    //         responsive: true,
    //         serverSide: true,
    //         ajax: {
    //             url: '/get-loc-master',
    //         },
    //         columns:[
    //             {
    //                 data: 'locLoc',
    //                 name: 'locLoc',
    //             },
    //             {
    //                 data: 'locDesc',
    //                 name: 'locDesc',
    //             },
    //             {
    //                 data: 'action',
    //                 name: 'action',
    //             },
    //         ],
    //     })
    // }

    
});