const SW = {
    success: ({ message, callback = () => { } }) => {
        return Swal.fire({
            title: 'Success!',
            text: message,
            icon: 'success',
            allowOutsideClick: false,
            allowEscapeKey: false,
        }).then(() => callback());
    },

    warning: ({ message, callback = () => { } }) => {
        return Swal.fire({
            title: 'Warning!',
            text: message,
            icon: 'warning',
            allowOutsideClick: false,
            allowEscapeKey: false,
        }).then(() => callback());
    },

    error: ({ message, callback = () => { } }) => {
        return Swal.fire({
            title: 'Failed!',
            text: message,
            icon: 'error',
            allowOutsideClick: false,
            allowEscapeKey: false,
        }).then(() => callback());
    },

    confirm: ({
        message,
        onConfirm = () => { },
        onReject = () => { },
    }) => {
        return Swal.fire({
            title: 'Warning!',
            text: message,
            icon: 'warning',
            allowOutsideClick: false,
            showDenyButton: true,
            confirmButtonText: 'Yes',
            allowEscapeKey: false,
        }).then(({ isConfirmed, isDenied }) => {
            if (isConfirmed) {
                return onConfirm()
            }
            if (isDenied) {
                return onReject()
            }
        });
    },
}

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

const Tst = {
    success: ({ message, callback = () => { } }) => {
        return Toast.fire({
            icon: 'success',
            title: message,
        }).then(() => callback());
    },

    error: ({ message, callback = () => { } }) => {
        return Toast.fire({
            icon: 'error',
            title: message,
        }).then(() => callback());
    },
}
