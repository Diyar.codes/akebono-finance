// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

function save(){
    var qty_ship    = $('[id^="qty_ship"]').map(function () { return $(this).val(); }).get();
    var item_number = $('[id^="item_number"]').map(function () { return $(this).val(); }).get();
    var line        = $('[id^="line"]').map(function () { return $(this).val(); }).get();
    var tr_id       = document.getElementById("tr_id").value;
    var po          = document.getElementById("po").value;
    var tgl         = document.getElementById("tgl").value;
    var packingslip = document.getElementById("packingslip").value;
    var jml_row     = document.getElementById("jml_row").value;

    $.ajax({
        type: "GET",
        url: "/edit-act-delivery-order-po",
        data:{
            qty_ship : qty_ship,
            tr_id : tr_id,
            po : po,
            packingslip:packingslip,
            item_number:item_number,
            line:line,
            tgl:tgl,
            jml_row:jml_row,
        },
        success: function (data) {
            SW.success({
                message: 'Edit Delivery Order Successfully',
                
            });
            setTimeout(function(){ 
                location.reload(); 
            }, 1000);
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });

}

function delete_ship(hitung){
    var item_number  = $('[id^="item_number"]').map(function () { return $(this).val(); }).get();
    var line         = $('[id^="line"]').map(function () { return $(this).val(); }).get();
    var tr_id        = document.getElementById("tr_id").value;
    var po           = document.getElementById("po").value;
    var tampung_item = item_number[hitung];
    var tampung_line = line[hitung];

    Swal.fire({
        title: 'Are you want to Delete ? ',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "GET",
                url: "/delete-kosong-delivery-order-po",
                data:{
                    tampung_item : tampung_item,
                    tr_id : tr_id,
                    po : po,
                    tampung_line:tampung_line
                },
                success: function (data) {
                    SW.success({
                        message: 'Delete Item Delivery Order Successfully',
                        
                    });
                    var url = `${BASEURL}edit-delivery-order-po`;
                    
                    setTimeout(function(){ 
                        window.location.href = url;
                    }, 1000);
                },
                error: function(error) {
                    alert('Data is not Defined!!!');
                }
            });
        }
    })
}