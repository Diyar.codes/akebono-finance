function confirmSp(){
    var row = $('#jml_row').val();
    var nosur = $('#nosur').val();
    var id_item = $('[id^="id_item"]').map(function () { return $(this).val(); }).get();
    var act_qty = $('[id^="act_qty"]').map(function () { return $(this).val(); }).get();
    var remark = $('[id^="remark"]').map(function () { return $(this).val(); }).get();
    // alert(act_qty);
    $.ajax({
        url:`${BASEURL}update-sp`,
        type:"GET",
        data:{
            // "_token": $('#token').val(),
            row: row,act_qty: act_qty,remark: remark,nosur:nosur,id_item:id_item
        },
        success:function(hasil){
            var url = `${BASEURL}sp-supp-view`;
            console.log(hasil);
            var conv = JSON.parse(hasil);
            if(conv['message'] == 1){
                SW.success({
                    message: 'Thank you for your confirmation!',
                });
                window.location.href = url;
            }


        }
    })
}

// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
