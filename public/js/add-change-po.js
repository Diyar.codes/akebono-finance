// change thead th datatable color
$("thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("thead th").css("color", "#fff");
$(() => {
    $('#main-tablee').DataTable({
        searching: false, paging: false, info: false, scrollY: 350,
        scrollX: true,
        scroller: true
    })

    $.ajax({
        url: `${BASEURL}add-change-po`,
        method: "GET",
        data: {
            'approval': true
        },
        success: function (data) {
            let obj = JSON.parse(data);
            let viewApproval = '';
            obj.data.forEach(el => {
                viewApproval += `
                    <tr>
                        <td>${el.approval_name}</td>
                        <td>${el.approval_email}</td>
                        <td><button class="btn btn-sm btn-warning closeApproval" getName="${el.approval_name}" getEmail="${el.approval_email}" aria-label="Close" class="close" data-dismiss="modal"><span class="fas fa-check"></span> Click</button></td>
                    </tr>
                `
            });
            document.getElementById('bodyApproval').innerHTML = viewApproval;
            $('#main-table').DataTable()
        }
    })
    document.getElementById('bodyApproval').addEventListener('click', value => {
        if (value.target.className == 'btn btn-sm btn-warning closeApproval') {
            document.getElementById('name').setAttribute('value', value.target.getAttribute('getName'))
            document.getElementById('email').setAttribute('value', value.target.getAttribute('getEmail'))
        }

        if(value.target.className == 'fas fa-check'){
            document.getElementById('name').setAttribute('value',value.target.parentElement.getAttribute('getName'));
            document.getElementById('email').setAttribute('value',value.target.parentElement.getAttribute('getEmail'));
        }
        value.preventDefault();
    })
})


function SeacchData() {
    let effdate = document.getElementById('effdate').value;
    let name = document.getElementById('name').value;
    let email = document.getElementById('email').value;
    let purchase_order = document.getElementById('purchase_order').value;

    if (purchase_order != '' && effdate != '' && name != '' && email != '') {
        getLoader();
        $.ajax({
            url: `${BASEURL}add-change-po`,
            method: "GET",
            data: {
                'pur_ord': purchase_order
            },
            success: function (data) {
                obj = JSON.parse(data);
                console.log((obj.data));

                if (obj.status == false) {
                    SW.error({
                        message: 'data kosong'
                    });
                } else {
                    let viewHtml = ''
                    let no = 1

                    viewHtml +=
                        `
                    <input type="hidden" name="txteffdate" id="txteffdate" size='5' value="${effdate}">
                    <input type="hidden" name="txt_approval_name" id="txt_approval_name" size='5' value="${name}">
                    <input type="hidden" name="txt_approval_email" id="txt_approval_email" size='5' value="${email}">
                    <input type="hidden" name="txt_po" id="txt_po" size='5' value="${purchase_order}">
                    <table class="display" id="main-tablee" width="100%">
                        <thead style="background-color:#0066CC">
                            <tr>
                                <th class="text-white">Line</th>
                                <th class="text-white">Item Number</th>
                                <th class="text-white">Part Name</th>
                                <th class="text-white">P/L/M</th>
                                <th class="text-white">Type</th>
                                <th class="text-white">Qty PO</th>
                                <th class="text-white">Qty LPB</th>
                                <th class="text-white">Qty Open</th>
                                <th class="text-white">Type</th>
                                <th class="text-white">Check</th>
                                <th class="text-white">Revisi Type</th>
                            </tr>
                        </thead>
                        <tbody>
                    `
                    obj.data.forEach(el => {

                        let qty_order = el.qty_po
                        let qty_lpb = el.qty_receive
                        let descPO = el.deskripsi1
                        let no_pp = el.no_po
                        let type_po = el.desc_type
                        let line_pp = el.line
                        let pp = no_pp.substr(0, 1)
                        let pm_code = el.ptPm_code
                        let pod_part = el.item_number
                        let qty_sisa = qty_order - qty_lpb

                        let type_pp = ''; //diambil dari PUB.rqd_det
                        // <input type="hidden" name="line${no}" id="line${no}" size='5' value="${pod_line}">
                        // <input type="hidden" name="txt_line_pp${no}" id="txt_line_pp${no}" size='5' value="${line_pp}">
                        // <td>${pod_line}</td>
                        viewHtml += `
                            <tr>
                                <input type="hidden" name="txt_no_pp${no}" id="txt_no_pp${no}" value="${no_pp}">
                                <input type="hidden" name="txt_line_pp${no}" id="txt_line_pp${no}" value="${line_pp}">
                                <input type="hidden" name="txt_type_pp${no}" id="txt_type_pp${no}" value="${type_pp}">

                                <input type="hidden" name="line${no}" id="line${no}" value="${line_pp}">
                                <input type="hidden" name="txt_item${no}" id="txt_item${no}" value="${pod_part}">
                                <input type="hidden" name="txt_desc${no}" id="txt_desc${no}" value="${descPO}">
                                <input type="hidden" name="txt_type${no}" id="txt_type${no}" value="${type_po}">
                                <td>${line_pp}</td>
                                <td>${pod_part}</td>
                                <td>${descPO}</td>
                                <td>${pm_code}</td>
                                <td>${type_po}</td>
                                <td>${(qty_order).toLocaleString('en-US', { style: 'currency', currency: 'USD', })}</td>
                                <td>${(qty_lpb).toLocaleString('en-US', { style: 'currency', currency: 'USD', })}</td>
                                <td><input type="text" name="txt_qty_sisa${no}" id="txt_qty_sisa${no}" value="${qty_sisa}"></td>
                                <td><input type="text" name="txt_type_Po${no}" id="txt_type_Po${no}" value="${type_po == null ? '' : type_po}"></td>
                                <td><input type="checkbox" name="chkCheck${no}" id="chkCheck${no}" value="OK"></td>
                                <td><input type="text" name="txt_type_Po_rev${no}" id="txt_type_Po_rev${no}" value="${type_po == null ? '' : type_po}"></td>
                            </tr>
                        `;
                        no++
                    });
                    viewHtml += `</tbody>`
                    viewHtml += `<input type="hidden" name="no_jumlah" value="${no - 1}">`;
                    document.getElementById('result').innerHTML = viewHtml;
                    document.getElementById('button').innerHTML = `<button type="submit" class="btn btn-sm btn-success bt-3 mt-2"><i class="fas fa-save"></i> Save</button> <button type="button" id="cancel" class="btn btn-sm btn-danger bt-3 mt-2"><i class="fas fa-undo"></i> Cancel</button>`;
                    $('#main-tablee').DataTable({
                        searching: false, paging: false, info: false, scrollY: 350,
                        scrollX: true,
                        scroller: true,
                        "ordering": false,
                    })

                    document.getElementById('cancel').addEventListener('click', (elCancel) => {
                        // console.log(document.getElementById('form_data'));
                        document.getElementById('form_data').reset();
                        elCancel.preventDefault();
                    })
                }
            }
        })
    } else {

        SW.error({
            message: 'Input all Data'
        });

    }

}
