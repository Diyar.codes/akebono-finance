
$('#tabelnya').DataTable({
    language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
    }
});

$(".table-bordered thead th").css("background-color", "#0066CC");
$("#tabelnya thead th").css("color", "#fff");

function search(){
    getLoader();
    var item     = $('#item').val();
    var tr_id    = $('#tr_id').val();
    var ps       = $('#packingslip').val();
    var dlv_date = $('#dlv_date').val();

    $.ajax({
        type: "GET",
        url: "/search-ps",
        data:{
            item : item,
            tr_id : tr_id,
            ps : ps,
            dlv_date : dlv_date,
        },
        success: function (data) {
            // alert(data);
            $('#item').val("");
            $('#tr_id').val("");
            $('#packingslip').val("");
            $('#dlv_date').val("");

            $('#tabelnya').DataTable().destroy();
            $('#tabelnya tbody tr').remove();
            $('.secondtable').show();
            $('.secondtable').find('#bodynya').html(data);
            $('#tabelnya').DataTable({
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}

function reset(){
    getLoader();
    var item     = '';
    var tr_id    = '';
    var ps       = '';
    var dlv_date = '';

    $.ajax({
        type: "GET",
        url: "/search-ps",
        data:{
            item : item,
            tr_id : tr_id,
            ps : ps,
            dlv_date : dlv_date,
        },
        success: function (data) {
            // alert(data);
            $('#item').val("");
            $('#tr_id').val("");
            $('#packingslip').val("");
            $('#dlv_date').val("");

            $('#tabelnya').DataTable().destroy();
            $('#tabelnya tbody tr').remove();
            $('.secondtable').show();
            $('.secondtable').find('#bodynya').html(data);
            $('#tabelnya').DataTable({
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}

$('#date').datepicker("setDate",new Date());

$('#reset').click(function(){
    $('#item-number').val('');
    $('#transaction').val('');
    $('#packing-slip').val('');
    $('#date').datepicker("setDate",new Date());
});


