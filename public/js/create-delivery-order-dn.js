
$(".table-bordered thead th").css("background-color", "#0066CC");
$("#tablenya thead th").css("color", "#fff");
$('#tablenya').DataTable({
    ordering:false,
    language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
    }
});


function search(){
    getLoader();
    var dn_no = $('#dn').val();
    var bulan = $('#bulan').val();
    var tahun = $('#tahun').val();
    $.ajax({
        url:`${BASEURL}create-delivery-order-dn`,
        type:"GET",
        data:{
            dn_no: dn_no,bulan : bulan,tahun : tahun
        },
        success: function (data) {
            $('#tablenya tbody tr').remove();
            $('#tablenya').DataTable().destroy();
            // $('.secondtable').show();
            $('.secondtable').find('#result').html(data);
            $('#tablenya').DataTable({
                responsive :true,
                ordering:false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            SW.warning({
                message: 'Data is not defined',
            });
        }
    })
}

function reset(){
    getLoader();
    var dn_no = '';
    var bulan = '';
    var tahun = '';
    $.ajax({
        url:`${BASEURL}create-delivery-order-dn`,
        type:"GET",
        data:{
            dn_no: dn_no,bulan : bulan,tahun : tahun
        },
        success: function (data) {
            $('#tablenya tbody tr').remove();
            $('#tablenya').DataTable().destroy();
            // $('.secondtable').show();
            $('.secondtable').find('#result').html(data);
            $('#tablenya').DataTable({
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            SW.warning({
                message: 'Data is not defined',
            });
        }
    })
}


//change model footer
$(".modal-footer").css("justify-content", "flex-start");
