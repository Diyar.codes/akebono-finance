

    $(document).ready(function() {
        $('#tablenya').DataTable({
            // responsive: true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });
        $(".table-bordered thead th").css("background-color", "#0066CC");
        $("#tablenya thead th").css("color", "#fff");
    });

function search(){
    getLoader();
    var rc = $('#rc').val();

    $.ajax({
        type: "GET",
        url: "/search-lpb",
        data:{
            rc : rc,
        },
        success: function (data) {
            $('#tablenya').DataTable().destroy();
            $('#tablenya tbody tr').remove();
            $('.secondtable').show();
            $('.secondtable').find('#tbodymain').html(data);
            $('#tablenya').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            SW.warning({
                message: 'Data Not Found',
                
            });
        }
    });
}

function direct(){
    getLoader();
    var rc = $('#rc').val();
    
    $.ajax({
        type: "GET",
        url: "/direct-print-lpb",
        data:{
            rc : rc,
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if(obj.message == 0){
                SW.error({
                    message: 'Data Not Found',
                    
                });
            }else{
                var url = obj.url;
                window.open(url, '_blank');
                // window.location.href = url;
            }
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}

function reset(){
    getLoader();
    var bulan = '0';
    var tahun = '0';
   $.ajax({
        type: "GET",
        url: "/search-delivery-order-po",
        data:{
            bulan : bulan,
            tahun : tahun,
        },
        success: function (data) {
            $('#tablenya').DataTable().destroy();
            // $('#tablenya tbody tr').remove();
            $('.secondtable').show();
            $('.secondtable').find('#tblsecond').html(data);
            $('#tablenya').DataTable({
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}