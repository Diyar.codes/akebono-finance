$(()=> {
    var url = window.location.href;

    // pemilahan url tertentu
    var urlSelection = url.split('/');

    var kode = urlSelection[4]
    var moon = urlSelection[5]
    var year = urlSelection[6]
    var locto = urlSelection[7]
    var app = urlSelection[8]

    $('#yearExcel').val(atob(year))
    $('#kodeExcel').val(atob(kode))
    $('#moonExcel').val(atob(moon))
    $('#loctoExcel').val(atob(locto))
    $('#appExcel').val(atob(app))

    main_table()

    // var url = `search-monthly-ds-report-detail-main/` + kode + `/` + moon + `/` + year + `/` + locto;
    // console.log(url)

    function main_table() {
        getLoader()

        $.ajax({
            type: "GET",
            url: `/search-monthly-ds-report-detail-main/` + kode + `/` + moon + `/` + year + `/` + locto + `/` + app,
            success: function (data) {
                $('#bodytable').html(data.main);
                // $('#bodytable').html(data['table']);
                // $('#main-table').DataTable( {
                //     responsive: false,
                //     paging: true,
                //     scrollY: "70vh",
                //     scrollX: true,
                //     scrollCollapse: true,
                //     fixedColumns: {
                //         leftColumns: 0,
                //         rightColumns: 0
                //     },
                //     searching: false,
                //     ordering : false,
                //     paging: false,
                // });
            },
            error: function(error) {
                SW.error({
                    message: 'Data is not Defined!!',
                });
            }
        });
    }
})
