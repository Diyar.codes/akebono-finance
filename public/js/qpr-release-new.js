// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

$(() => {
    // view data supplier
    $('#main-tablee').DataTable({
        // processing: true,
        responsive: true,
        // serverSide: true,
        ajax: {
            url: `${BASEURL}get-supplier`,
        },
        columns: [
            {
                data: 'ct_vd_addr',
                name: 'supplier id',
            },
            {
                data: 'ct_ad_name',
                name: 'supplier name',
            },
            {
                data: 'action',
                name: 'action',
            },
        ],
    })

    // select modal
    $(document).on('click', '#selectbusiness', function () {

        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        $('#name_supp').val(namebusiness)
        $('#searchSupplier').click()
    })

})


let found_date = document.getElementById('found_date');
found_date.addEventListener('change', () => {
    let code = document.getElementById('code').value;
    let name = document.getElementById('name_supp').value;


    if (code == '' || name == '') {
        SW.error({
            message: 'seach for supplier id and supplier name first'
        });
        found_date.value = new Date()
    } else {

        document.getElementById('content').removeAttribute('hidden');
        let supp = document.getElementById('code').value;
        let supp_name = document.getElementById('name_supp').value;

        found_date = document.getElementById('found_date').value;

        let viewHtml = '';
        viewHtml += `
            <input type="hidden" name="supp" value="${supp}">
            <input type="hidden" name="supp_name" value="${supp_name}">
            <input type="hidden" name="found_date" value="${found_date}">
            <input type="hidden" name="id" id="id">
            <!-- kiri -->
            <div class="col-md-6">
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">No QPR</span>
                        <input type="text" class="form-control form-control-sm" id="no" readonly name="no" autocomplete="off">
                    </div>
                </div>
            </div>
            <!-- kanan -->
            <div class="col-md-6">
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Issue
                            Date</span>
                        <input type="date" class="form-control form-control-sm" id="issu" name="issu" autocomplete="off">
                    </div>
                </div>
            </div>
            <!-- tengah -->
            <div class="col-md-12">
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Fax</span>
                        <input type="text" class="form-control form-control-sm" id="fax" name="fax" autocomplete="off">
                    </div>
                </div>
            </div>
            <!-- kiri -->
            <div class="col-md-6">
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Part Name</span>
                        <input type="text" class="form-control form-control-sm" id="name" name="name" readonly autocomplete="off">
                        <button class="btn btn-info btn-sm" id="partName" data-toggle="modal"
                            data-target="#modalPartName"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Part
                            Number</span>
                        <input type="text" class="form-control form-control-sm" id="item" name="item" readonly autocomplete="off">
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">2W/4W</span>
                        <select name="w" id="w" class="form-control-sm">
                            <option value="2w">2w</option>
                            <option value="4w">4w</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Material
                            Number</span>
                        <input type="text" class="form-control form-control-sm" id="number" name="number" autocomplete="off">
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Material
                            Type</span>
                        <select name="tipe" id="tipe" class="form-control-sm">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Qty NG</span>
                        <input type="text" class="form-control-sm" id="ng" name="ng" autocomplete="off">
                        <select name="ng_satuan" id="ng_satuan" class="form-control-sm">
                            <option value="pcs">pcs</option>
                            <option value="un">un</option>
                            <option value="kg">kg</option>
                            <option value="bag">bag</option>
                            <option value="pail">pail</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Invoice
                            NO</span>
                        <input type="text" class="form-control form-control-sm" id="invoice" id="invoice" name="invoice" required autocomplete="off">
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom"
                            id="basic-addon1">Disposition</span>
                        <select name="dispo" id="dispo" class="form-control-sm">
                            <option value="">Select One</option>
                            <option value="Sortir">Sortir</option>
                            <option value="Repair">Repair</option>
                            <option value="NG-Return">NG-Return</option>
                            <option value="Suspect-Return">Suspect-Return</option>
                            <option value="Scrap">Scrap</option>
                            <option value="Sort">Sort</option>
                            <option value="Rework">Rework</option>
                            <option value="Special Accept">Special Accept</option>
                            <option value="point">point</option>
                            <option value="Visual">Visual</option>
                            <option value="Dimensi">Dimensi</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom"
                            id="basic-addon1">Information</span>
                        <select name="info" id="info" class="form-control-sm">
                            <option value="information">Information</option>
                            <option value="Information For Investigation">Information For Investigation</option>
                        </select>
                    </div>
                </div>
            </div>
            <!-- kanan -->
            <div class="col-md-6">
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Delivery
                            Date</span>
                        <input type="date" class="form-control form-control-sm" id="deliv" name="deliv" autocomplete="off">
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Receiving
                            Date</span>
                        <input type="date" class="form-control form-control-sm" id="receive" name="receive" autocomplete="off">
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Invoice
                            Date</span>
                        <input type="date" class="form-control form-control-sm" id="date" name="date" autocomplete="off">
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Quality
                            Rank</span>
                        <select name="rank" id="rank" class="form-control-sm">
                            <option value="">Select One</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Occurence</span>
                        <select name="occu" id="occu" class="form-control-sm">
                            <option value="Select One">Select One</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Qty Del</span>
                        <input type="text" class="form-control form-control-sm" id="det" name="det" autocomplete="off">
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Lot No</span>
                        <input type="text" class="form-control form-control-sm" id="lot" name="lot" autocomplete="off">
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Point
                            Detection</span>
                        <select name="point" id="point" class="form-control-sm">
                            <option value="">Select One</option>
                            <option value="Visual">Visual</option>
                            <option value="Dimensi">Dimensi</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Revise</span>
                        <input type="text" class="form-control form-control-sm" id="revise" name="revise" value="1" autocomplete="off">
                    </div>
                </div>
            </div>
            <!-- tengah -->
            <div class="col-md-12">
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom"
                            id="basic-addon1">Description</span>
                        <textarea id="deskripsi" name="deskripsi" cols="70" rows='5' autocomplete="off"></textarea>
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Image</span>
                        <input type="file" class="file" id="file" name="file" autocomplete="off">
                    </div>
                </div>
            </div>
            <!-- kiri -->
            <div class="col-md-6">
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Found At</span>
                        <select name="at" id="at" class="form-control-sm">
                            <option value="line">Line</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Checked</span>
                        <select name="check" id="check" class="form-control-sm">
                        </select>
                    </div>
                </div>
            </div>
            <!-- kanan -->
            <div class="col-md-6">
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">In Charge</span>
                        <select name="charge" id="charge" class="form-control-sm">
                        </select>
                    </div>
                </div>
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <span class="input-group-text spanbox exinput-custom" id="basic-addon1">Approved</span>
                        <select name="approve" id="approve" class="form-control-sm">
                        </select>
                    </div>
                </div>
            </div>
            <!-- button -->
            <div class="col-md-12">
                <div class="col-lg-12 ml-xl-2 ml-lg-3 box">
                    <div class="input-group mb-3">
                        <button class="btn btn-sm btn-success"><span class="fas fa-save"></span> Save</button>
                    </div>
                </div>
            </div>
        `;
        document.getElementById('dataGet').innerHTML = viewHtml
        document.getElementById('partName').addEventListener('click', el => {
            $('#main-tablee-2').dataTable().fnClearTable();
            $('#main-tablee-2').dataTable().fnDestroy();

            el.preventDefault();
            $.ajax({
                url: `${BASEURL}qpr-receiving`,
                method: "GET",
                data: {
                    'partName': true,
                },
                success: function (data) {
                    let obj = JSON.parse(data)
                    let viewPartName = ''
                    let bodyPartName = document.getElementById('bodyPartName');
                    let i = 1;
                    obj.data.forEach(el => {
                        viewPartName += `
                        <tr>
                            <td>${el.item_number}</td>
                            <td>${el.deskripsi1}</td>
                            <td><button class="btn btn-sm btn-warning closePartName" desc="${el.deskripsi1}" item="${el.item_number}" aria-label="Close" class="close" data-dismiss="modal"><span class="fas fa-check"></span> Click</button></td>
                        </tr>`;
                        i++;
                    });
                    bodyPartName.innerHTML = viewPartName;
                    $('#main-tablee-2').DataTable()
                }

            })

            bodyPartName.addEventListener('click', value => {
                console.log(value.target);
                if (value.target.className == 'btn btn-sm btn-warning closePartName') {
                    document.getElementById('item').setAttribute('value', value.target.getAttribute('item'))
                    document.getElementById('name').setAttribute('value', value.target.getAttribute('desc'))
                }
                if (value.target.className == 'fas fa-check') {
                    document.getElementById('item').setAttribute('value', value.target.parentElement.getAttribute('item'))
                    document.getElementById('name').setAttribute('value', value.target.parentElement.getAttribute('desc'))
                }
                value.preventDefault();
            })
        })


        // generate no qpr
        $.ajax({
            url: `${BASEURL}qpr-receiving`,
            method: "GET",
            data: {
                'supp': supp,
            },
            success: function (data) {
                let obj = JSON.parse(data);
                let no = obj.data.no.nomer;
                let checked = obj.data.checked
                let approved = obj.data.approved
                let charge = obj.data.charge
                let id = obj.data.id

                // generate no id
                document.getElementById('id').setAttribute('value', id)

                // generate no qpr
                if (no.length === 1) {
                    no = "00" + no;
                }
                if (no.length === 2) {
                    no = "0" + no;
                }
                let year = new Date().getFullYear().toString().substr(-2);
                let month = (new Date().getMonth() + 1).toString();

                if (month.length === 1) {
                    month = "0" + month
                }

                let no_qpr_code = `${year}${month}-${no}/QC-AAIJ/L/${supp}`;
                document.getElementById('no').setAttribute('value', no_qpr_code);

                // checked
                viewCheked = '';
                checked.forEach(el => {
                    viewCheked += `<option value="${el.approval_name}">${el.approval_name}</option>`;
                });
                document.getElementById('check').innerHTML = viewCheked

                // approved
                let viewApproved = '';
                approved.forEach(el => {
                    viewApproved += `<option value="${el.approval_name}">${el.approval_name}</option>`;
                });
                document.getElementById('approve').innerHTML = viewApproved

                // charge
                let viewCharge = '';
                charge.forEach(el => {
                    viewCharge += `<option value="${el.username}">${el.username}</option>`;
                });
                document.getElementById('charge').innerHTML = viewCharge
            }
        })
    }
})
