$(()=> {
    // get data
    fill_datatable()

    // view data
    function fill_datatable(type = '', qprl_no = '', code ='', from_date = '', to_date = '') {
        // getLoader
        getLoader()

        $('#main-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            paging: true,
            // scrollY: "80vh",
            // scrollX: true,
            // scrollCollapse: true,
            // fixedColumns: {
            //     leftColumns: 0,
            //     rightColumns: 0
            // },
            searching: false,
            ordering : false,
            ajax: {
                url: `${BASEURL}all-list-qpr`,
                data: {
                    type: type,
                    qprl_no: qprl_no,
                    code: code,
                    from_date: from_date,
                    to_date: to_date,
                }
            },
            deferRender:    true,
            columns:[
                {
                    // "data": "id",
                    data: 'qprl_no',
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {
                    data: 'qprl_no',
                    name: 'qprl_no',
                },
                {
                    data: 'qprl_supplier_name',
                    name: 'qprl_supplier_name',
                },
                {
                    data: 'qprl_item',
                    name: 'qprl_item',
                },
                {
                    data: 'qprl_part_name',
                    name: 'qprl_part_name',
                },
                {
                    data: 'qprl_qty_ng',
                    name: 'qprl_qty_ng',
                },
                {   data: "qprl_checked",
                    fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                        if(oData['qprl_checked_status'] == 'OK') {
                            oData['qprl_checked_status'] = 'Approved'
                        } else if(oData['qprl_checked_status'] == 'NOK') {
                            oData['qprl_checked_status'] = 'Not Approved'
                        } else {
                            oData['qprl_checked_status'] = 'Waiting'
                        }

                        if(oData['qprl_checked_date'] == null) {
                            oData['qprl_checked_date'] = ''
                        }

                        $(nTd).append('<div class="text-wrap tablefontmini">'+oData['qprl_checked_status']+'</div>'+'<div class="text-wrap tablefontmini"> '+oData['qprl_checked_date'] + '</div>');
                    }
                },
                {   data: "qprl_approved",
                    fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                        if(oData['qprl_approved_status'] == 'OK') {
                            oData['qprl_approved_status'] = 'Approved'
                        } else if(oData['qprl_approved_status'] == 'NOK') {
                            oData['qprl_approved_status'] = 'Not Approved'
                        } else {
                            oData['qprl_approved_status'] = 'Waiting'
                        }

                        if(oData['qprl_approved_date'] == null) {
                            oData['qprl_approved_date'] = ''
                        }

                        $(nTd).append('<div class="tablefontmini"> '+oData['qprl_approved_status']+'</div>'+'<div class="tablefontmini"> '+oData['qprl_approved_date']+'</div>');
                    }
                },
                {
                    mData: "qprl_rp_date",
                    render: function(data, type, row) {
                        var nice =  data ? data.split("\n").join("<br/>") : "";
                        return "<div class='text-wrap tablefontmini'>" + nice + "</div>";
                    }
                },
                {
                    data: 'verification',
                    name: 'verification'
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
            columnDefs: [
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 0
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 1
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 2
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 3
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 4
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 5
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 6
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 7
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 8
                },
                {
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap tablefontmini'>" + data + "</div>";
                    },
                    targets: 9
                },
            ],
        })
    }

    // // button show
    // $('#input-search').bind('click keyup', function(event) {
    //     if(!$('#input-search').val()) {
    //         $('.t-automatical').css('display', 'none')
    //     }
    // })

    // search
    $(document).on('click', '#search', function() {
        // if(!$('#name').val()) {
        //     $('.t-automatical').css('display', 'none')
        // } else {
        //     console.log('not-oke')
        //     $('.t-automatical').css('display', 'flex')
        // }

        var type = $('#type').val()
        var qprl_no = $('#qprl_no').val()
        var code = $('#code').val()
        var fdate = $('#date-from').val()
        var tdate = $('#date-to').val()

        if(fdate) {
            from_date = fdate + 'T00:00:00'
        } else {
            from_date = fdate
        }

        if(tdate) {
            to_date = tdate + 'T23:59:59'
        } else {
            to_date = tdate
        }

        $('#typeExcel').val(type)
        $('#qprNoExcel').val(qprl_no)
        $('#supplierIdExcel').val(code)
        $('#fromExcel').val(from_date)
        $('#toExcel').val(to_date)

        if(from_date != '' && to_date == '') {
            SW.error({
                message: 'the destination date must exist!',
            });
        } else if (from_date == '' && to_date != '') {
            SW.error({
                message: 'date of must exist!',
            })
        } else if(type != '' || qprl_no != '' || code != '' || from_date != '' || to_date != '') {
            $('.t-automatical').css('display', 'flex')

            $('#main-table').DataTable().clear().destroy()
            fill_datatable(type, qprl_no, code, from_date, to_date)
        } else {
            $('#main-table').DataTable().clear().destroy()
            fill_datatable()
        }
    })

    // reset
    $('#reset').click(function() {
        $('.t-automatical').css('display', 'none')

        $('#typeExcel').val('')
        $('#qprNoExcel').val('')
        $('#supplierIdExcel').val('')
        $('#fromExcel').val('')
        $('#toExcel').val('')

        $('#type').val('')
        $('#qprl_no').val('')
        $('#code').val('')
        $('#name').val('')
        $('#date-from').val('')
        $('#date-to').val('')
        $('#main-table').DataTable().clear().destroy()
        fill_datatable()
    })

    // seacrh soap business relation
    $(document).on('click', '#seacrhSOAPBusinesRelationModal', function(){
        $('#main-supplier').DataTable().clear().destroy()
        getSupplier();
    })

    // get supplier
    function getSupplier() {
        $('#main-supplier').DataTable({
            // processing: true,
            responsive: true,
            // serverSide: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'supplier id',
                },
                {
                    data: 'ct_ad_name',
                    name: 'supplier name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })

        // getLoader
        getLoader()
    }

    // select modal
    $(document).on('click', '#selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        $('#name').val(namebusiness)
        $('#modaldemo8part2').click()
    })
})

// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
