$(()=> {
    var no = window.location.pathname.split("/").pop()

    main_table()

    function main_table() {
        // getLoader
        getLoader()

        $.ajax({
            type: "GET",
            url: `/view-list-miss-packing-main/` + no,
            success: function (data) {
                $('.main-table').find('.bodytable').html(data.main);
                $('#main-table').DataTable( {
                    responsive: true,
                    paging: true,
                    scrollY: "70vh",
                    scrollX: true,
                    scrollCollapse: true,
                    fixedColumns: {
                        leftColumns: 0,
                        rightColumns: 0
                    },
                    searching: false,
                    ordering : false,
                    // paging: false, add komen
                });
            },
        });
    }

    // update
    $(document).on('click', '#updateRemarks', function(e) {
        e.preventDefault()

        // getLoader
        getLoader()

        Swal.fire({
            title: 'update remarks cover letter ? ',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!'
        }).then((result) => {
            if (result.value) {
                var form = new FormData();
                form.append("_method", "PUT");
                form.append("remarks", $('textarea[name = "remarks"]').val());

                var settings = {
                    "url": `${BASEURL}update-list-miss-packing/` + no,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Accept": "application/json"
                    },
                    "processData": false,
                    "mimeType": "multipart/form-data",
                    "contentType": false,
                    "data": form,
                };

                $.ajax(settings).done(function(response) {
                    let data = JSON.parse(response);

                    SW.success({
                        message: data.message
                    });

                    location.reload();
                });
            }
        })
    })
})

