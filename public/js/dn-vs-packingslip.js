function search_data(){
    getLoader();
    var dn = $('#dn').val();
    var po = $('#po').val();
    var item = $('#item').val();
    
    // alert(item);
    $.ajax({
        url:`${BASEURL}get-packing`,
        type:"POST",
        data:{
            "_token": $('#token').val(),
            dn: dn, 
            po: po,
            item:item
        },
        success: function (data) {
            $('#isi-table').DataTable().destroy();
            $('.secondtable').show();
            $('.secondtable').find('.tblsecond').html(data);
            $('#isi-table').DataTable({
                ordering: false
            });
            
        },
        error: function(error) {
            alert('Error System !');
        }
    })
    }

// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
