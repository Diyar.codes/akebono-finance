// $.ajaxSetup({
//     headers: {
//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     }
// });
function search_nosur(){
    var nosur = $('#nosur').val();
    var modul = 1;
    if(nosur !== ""){
        $.ajax({
            url:`${BASEURL}sp-supp-view`,
            type:"GET",
            data:{
                // "_token": $('#token').val(),
                nosur: nosur,modul: modul
            },
            success:function(hasil){
                console.log(hasil);
                var conv = JSON.parse(hasil);
                if(conv['message'] == 0){
                    SW.warning({
                        message: 'No Surat Not Found',
                    });
                }else if(conv['message'] == 1){
                    $('#initable tbody tr').remove();
                    for (var i = 0; i < conv['hasil'].length; i++) {
                        var no = conv['hasil'][i].id;
                        var urut = 1;
                        var url = `${BASEURL}confirm-sp/`+no;
                        $('#result').append(
                        '<tr>'+
                            '<td>'+urut+++'</td>'+
                            '<td>'+conv['hasil'][i].no+'</td>'+
                            '<td>'+conv['hasil'][i].tanggal+'</td>'+
                            '<td>'+conv['hasil'][i].kendaraan+'</td>'+
                            '<td>'+(conv['hasil'][i].app_supplier == "Confirmed" ? '<a href="'+url+'">View (Confirmed)</a>' : '<a href="'+url+'">View</a>')+'</td>' +
                        '</tr>'
                        );
                    }
                }

            }
        })
    }else{
        SW.warning({
            message: 'No Surat Cannot Empty',
        })
    }

}

// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
