// $.ajaxSetup({
//     headers: {
//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     }
// });

$('.btnlist').hide();
function search_dn(){
    var supp_id = $('#supp_id').val();
    var ds = $('#date_start').val();
    var de = $('#date_end').val();

    document.getElementById("supplier_id").value = supp_id;
    document.getElementById("datestart").value = ds;
    document.getElementById("dateend").value = de;
    // var modul = 1;
    if(supp_id == ""){
        SW.warning({
            message: 'Supplier Cannot Empty',
        })
    }else if(ds == "" || de ==""){
        SW.warning({
            message: 'Range Date Cannot Empty',
        })
    }else{
        $.ajax({
            url:`${BASEURL}list-delivery-notes`,
            type:"GET",
            data:{
                // "_token": $('#token').val(),
                supp_id: supp_id,ds:ds, de:de
            },
            success: function (data) {
                if(data['message'] == 0){
                    SW.warning({
                        message: 'Data Not Found',
                    })
                }else{
                    console.log(data);
                }
                $('.secondtable').show();
                $('.secondtable').find('.tblsecond').html(data);
                $('.btnlist').show();

            },
            error: function(error) {
                alert('Error System !');
            }
        })
    }

}

$(()=> {
    // view data supplier
    $(document).on('click', '#seacrhSOAPBusinesRelation', function(){
        $('#get-supplier').DataTable().clear().destroy()
        $('#getSupplierTarget').modal('show');
        getSupplier();
    });

    $(document).on('click', '#get-supplier #selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        // $('#code').val(codebusiness)
        $('#supp_id').val(codebusiness)
        $('#supp_name').val(namebusiness)
        $('#getSupplierTarget').click()
    });

    function getSupplier() {
        // getLoader
        getLoader()

        // view data supplier
        $('#get-supplier').DataTable({
            // processing: true,
            responsive: true,
            // serverSide: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'supplier id',
                },
                {
                    data: 'ct_ad_name',
                    name: 'supplier name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }


    // search
    $(document).on('click', '#search', function() {
        var queryItemNumber = 1;
        var querySupplierId = $('#supp_id').val()
        var querySupplierName = $('#supp_name').val()

        if(queryItemNumber != '' || querySupplierId != '' || querySupplierName != '') {
            fetch_customer_data(queryItemNumber, querySupplierId, querySupplierName)
        } else {
            fetch_customer_data()
        }
    })

    // select modal
    $(document).on('click', '#selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#supp_id').val(codebusiness)
        $('#supp_name').val(namebusiness)
        $('#modaldemo8part2').click()
    })
})

// close
$('.close').click(function(e) {
    $('#finsertitemkanban').trigger('reset')
    $('#vc-item_number').text('')
    $('#vc-supplier').text('')
})

// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

//change model footer
$(".modal-footer").css("justify-content", "flex-start");

// update
$(document).on('click', '#updateItemKanban', function(e) {
    e.preventDefault()
    Swal.fire({
        title: 'update back no, snp kanban & kanban pallet ? ',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.value) {
            var form = new FormData();

            var back_no = []
            var pcs_kanban = []
            var pallet_kanban = []

            $('input[name="back_no[]"]').each(function(){
                back_no.push(this.value);
            });

            $('input[name="pcs_kanban[]"]').each(function(){
                pcs_kanban.push(this.value);
            });

            $('input[name="pallet_kanban[]"]').each(function(){
                pallet_kanban.push(this.value);
            });

            var lengthin = back_no.length - 1

            for (var i = 0; i <= lengthin; i++) {
                form.append("_method", "PUT");
                form.append("back_no" + "[" + i + "]", back_no[i])
                form.append("pcs_kanban" + "[" + i + "]", pcs_kanban[i])
                form.append("pallet_kanban" + "[" + i + "]", pallet_kanban[i])
            }

            var queryItemNumber = $('#input-search-item_number').val()
            var querySupplierId = $('#input-search-supplier_id').val()
            var querySupplierName = $('#input-search-supplier_name').val()

            if(queryItemNumber != '') {
                var settings = {
                    "url": `${BASEURL}api/item-kanbanapis/` + queryItemNumber,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Accept": "application/json"
                    },
                    "processData": false,
                    "mimeType": "multipart/form-data",
                    "contentType": false,
                    "data": form,
                    error: function(jqXHR, textStatus, errorThrown) {
                        var err = JSON.parse(jqXHR.status)
                        var errText = JSON.parse(jqXHR.responseText)
                        if (err == 500) {
                            SW.error({
                                message: 'please enter the correct keywords'
                            });
                        }
                    }
                };
            } else if(querySupplierId != '' ) {
                var settings = {
                    "url": `${BASEURL}api/item-kanbanapis/` + querySupplierId,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Accept": "application/json"
                    },
                    "processData": false,
                    "mimeType": "multipart/form-data",
                    "contentType": false,
                    "data": form,
                    error: function(jqXHR, textStatus, errorThrown) {
                        var err = JSON.parse(jqXHR.status)
                        var errText = JSON.parse(jqXHR.responseText)
                        if (err == 500) {
                            SW.error({
                                message: 'please enter the correct keywords'
                            });
                        }
                    }
                };
            }else if(querySupplierName != '' ) {
                var settings = {
                    "url": `${BASEURL}api/item-kanbanapis/` + querySupplierName,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Accept": "application/json"
                    },
                    "processData": false,
                    "mimeType": "multipart/form-data",
                    "contentType": false,
                    "data": form,
                    error: function(jqXHR, textStatus, errorThrown) {
                        var err = JSON.parse(jqXHR.status)
                        var errText = JSON.parse(jqXHR.responseText)
                        if (err == 500) {
                            SW.error({
                                message: 'please enter the correct keywords'
                            });
                        }
                    }
                };
            } else {
                if(queryItemNumber == '' && querySupplierId == '' && querySupplierName == '') {
                    SW.error({
                        message: 'search data does not exist yet'
                    });
                    return
                }
            }

            $.ajax(settings).done(function(response) {
                let data = JSON.parse(response);

                SW.success({
                    message: data.message
                });

                location.reload();
            });
        }
    })
})

// insert
$(document).on('click', '#itemkanbaninsertbtn', function(e) {
    e.preventDefault()
    var form = new FormData();
    form.append("item_number", $('#finsertitemkanban input[name = "item_number"]').val());
    form.append("supplier", $('#finsertitemkanban input[name = "code"]').val());
    form.append("ct_vd_addr", $('#finsertitemkanban input[name = "code"]').val());
    form.append("ct_vd_type", $('#finsertitemkanban input[name = "type"]').val());
    form.append("ct_ad_name", $('#finsertitemkanban input[name = "name"]').val());

    var settings = {
        "url": `${BASEURL}api/item-kanbanapis`,
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Accept": "application/json",
        },
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": form,
        error: function(jqXHR, textStatus, errorThrown) {
            var err = JSON.parse(jqXHR.responseText)
            var err2 = err.errors
            if (err2.item_number) {
                $('#vc-item_number').text(err2.item_number)
            } else {
                $('#vc-item_number').text('')
            }
            if (err2.supplier) {
                $('#vc-supplier').text(err2.supplier)
            } else {
                $('#vc-supplier').text('')
            }
            $('#fprocessinsertitemkanban').html("<button class='btn ripple btn-light btn-spinner' type='button' id='itemkanbaninsertbtn'>Save</button>");
        },
        "beforeSend": function() {
            $('#fprocessinsertitemkanban').html("<button class='btn btn-light btn-block btn-spinner' type='button' disabled><span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span></button>");
        },
        "success": function(data) {
            $('#fprocessinsertitemkanban').html("<button class='btn ripple btn-light btn-spinner' type='button' id='itemkanbaninsertbtn'>Save</button>");
        }
    };

    $.ajax(settings).done(function(response) {
        var data = JSON.parse(response)
        $('#finsertitemkanban').trigger('reset')
        $('#vc-item_number').text('')
        $('#vc-supplier').text('')
        $('#modaldemo8').click();
        location.reload();
        SW.success({
            message: data.message
        });
    });
})

