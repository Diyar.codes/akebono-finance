$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
// $('#tablenya').DataTable({
//     scrollY: "300px",
//     scrollX: true,
//     scrollCollapse: true,
//     paging: false,
//     fixedColumns: true
// });
$('#tablenya').DataTable({
    language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
    }
});