// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

$('#main-main').DataTable({
    responsive:true,
    language: {
        searchPlaceholder: 'Search...',
        sSearch: '',
        lengthMenu: '_MENU_ items/page',
    }
});

function search(){
    getLoader();
    var bulan = $('#filter_moon').val();
    var tahun = $('#filter_year').val();
    $.ajax({
        type: "GET",
        url: "/search-edit-delivery-order-po",
        data:{
            bulan : bulan,
            tahun : tahun,
        },
        success: function (data) {
            console.log(data);
            $('#main-main').DataTable().destroy();
            // $('#tablenya tbody tr').remove();
            $('#main-main').find('#bodynya').html(data);
            $('#main-main').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}

function reset(){
    getLoader();
    var bulan = 0;
    var tahun = 0;
    $.ajax({
        type: "GET",
        url: "/search-edit-delivery-order-po",
        data:{
            bulan : bulan,
            tahun : tahun,
        },
        success: function (data) {
            console.log(data);
            $('#main-main').DataTable().destroy();
            // $('#tablenya tbody tr').remove();
            $('#main-main').find('#bodynya').html(data);
            $('#main-main').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        },
        error: function(error) {
            alert('Data is not Defined!!!');
        }
    });
}

function hapus_transaksi(id){
    Swal.fire({
        title: 'Are you want to delete this Transaction ? ',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "GET",
                url: "/delete-transaction-delivery-order-po",
                data:{
                    id : id,
                },
                success: function (data) {
                    SW.success({
                        message: 'Delete Delivery Order Successfully',
                        
                    });
                    setTimeout(function(){ 
                        location.reload(); 
                    }, 1000);
                },
                error: function(error) {
                    alert('Data is not Defined!!!');
                }
            });
        }
    })
}