$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function search_dn(){
    getLoader();
    var cari = $('#search').val();
    $.ajax({
        url:`${BASEURL}get-dn`,
        type:"POST",
        data:{
            "_token": $('#token').val(),
            cari: cari
        },
        success: function (data) {
            $('#tablenya tbody tr').remove();
            $('.secondtable').show();
            $('.secondtable').find('#result').html(data);
        },
        error: function(error) {
            alert('Error System');
        }
    })
}

function arrival(){
    getLoader();
    var arr = $('#arrival_date').val();
    var modul = "arrival";
    $.ajax({
        url:`${BASEURL}get-dn`,
        type:"POST",
        data:{
            "_token": $('#token').val(),
            arr: arr,
            modul:modul
        },
        success: function (data) {
            $('#tablenya tbody tr').remove();
            $('.secondtable').show();
            $('.secondtable').find('#result').html(data);
        },
        error: function(error) {
            alert('Error System');
        }
    })
}

function cetak_dn(){
    getLoader();
    var cari = $('#search').val();
    // alert("_token": $('#token').val());
    $.ajax({
        url:`${BASEURL}cetak-dn`,
        type:"GET",
        data:{
            // "_token": $('#token').val(),
            cari: cari
        },
        success:function(hasil){
            var conv = JSON.parse(hasil);
            if(conv['message'] == 0){
                SW.warning({
                    message: 'Delivery note number not found',
                });
            }else if(conv['message'] == 1){
                var dn_num = conv['dn'];
                var url = `${BASEURL}print-delivery-notes/`+dn_num;
                window.open(url);
            }

        }
    })
}

// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");
