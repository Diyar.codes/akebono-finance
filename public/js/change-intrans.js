function searchChangeIntrans() {
    getLoader()
    bulan = document.getElementById('bulan').value;
    tahun = document.getElementById('tahun').value;

    if (bulan == 0 || tahun == 0) {
        SW.error({
            message: 'enter month and year first!'
        })
    } else {
        $.ajax({
            url: `${BASEURL}change-intrans`,
            method: "GET",
            data: {
                'bulan': bulan,
                'tahun': tahun,
            },
            success: function (data) {
                obj = JSON.parse(data);

                viewHtml = '';

                if (obj.status == false) {
                    viewHtml += `
                        <tr class="text-center">
                            <td colspan="9">No data available in table</td>
                        </tr>
                      `;
                    table = document.getElementById('result');
                    table.innerHTML = viewHtml
                } else {
                    obj.data.forEach(el => {

                        approval_status = el.approval_status;
                        status = '';
                        if (approval_status == 'OK') {
                            status = 'Approved';
                        } else {
                            if (approval_status == 'NOK') {
                                status = 'Rejected';
                            } else {
                                status = 'Not yet Approved';
                            }
                        }
                        viewHtml += `
                        <tr>
                            <td>${el.transaksi_id}</td>
                            <td>${el.effdate}</td>
                            <td>${el.no_po}</td>
                            <td>${el.line_po}</td>
                            <td>${el.po_item}</td>
                            <td>${el.po_deskripsi}</td>
                            <td>${el.type_po}</td>
                            <td>${el.type_po_rev}</td>
                            <td>${el.approval_po} <br> ${el.approval_date} <br> ${status}</td>
                        </tr>
                      `;
                    });
                    table = document.getElementById('result');
                    table.innerHTML = viewHtml
                }
            }
        })
    }

}

// change thead th datatable color
$(".table-bordered thead th").css("background-color", "#0066CC");

// change thead th datatable font color
$("table thead th").css("color", "#fff");

$('#main-table').DataTable({
    "ordering": false,
});
