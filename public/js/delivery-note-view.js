$(()=> {
    $('#main-table').DataTable({
        processing: true,
        responsive: true,
        serverSide: true,
        searching: false,
        ajax: `${BASEURL}view-delivery-notes`,
        columns:[
            {
                data: 'kd_item',
                name: 'kd_item',
            },
            {
                data: 'kd_item', //um
                name: 'kd_item',
            },
            {
                name: 'kd_supp',
                data: 'kd_supp',
            },
            {
                data: 'kd_item', //supp_name
                name: 'kd_item',
            },
            {
                data: 'kd_item', //desc1
                name: 'kd_item',
            },
            {
                data: 'kd_item', //desc2
                name: 'kd_item',
            },
            {
                data: 'back_no',
                name: 'back_no',
            },
            {
                data: 'pcs_kanban',
                name: 'pcs_kanban',
            },
            {
                data: 'pallet_kanban',
                name: 'pallet_kanban',
            },
        ],
        language: {
            oPaginate: {
                sNext: '<i class="simple-icon-arrow-right"></i>',
                sPrevious: '<i class="simple-icon-arrow-left"></i>',
                sFirst: '<i class="fa fa-step-backward"></i>',
                sLast: '<i class="fa fa-step-forward"></i>'
            }
        },
        drawCallback: function( settings ) {
            $('#main-table_wrapper .row .col-sm-12').removeClass('col-md-7')
            $('#main-table_wrapper .row .col-sm-12').removeClass('col-md-6')
            $('#main-table_wrapper .row .col-sm-12').removeClass('col-md-5')
            $('#main-table_wrapper .row:nth-child(3)').addClass('text-center')
        }
    })
})
