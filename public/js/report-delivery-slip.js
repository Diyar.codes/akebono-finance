$(()=> {
    // call search report delivery slip
    searchReportDeliverySlip()

    // function search report delivery slip
    function searchReportDeliverySlip(moon = '', year = '', app = '', code = ''){
        // getLoader
        getLoader()

        $.ajax({
            url:`${BASEURL}search-report-delivery-slip`,
            type:"GET",
            data:{
                moon: moon,
                year: year,
                app: app,
                code: code
            },
            success: function (data) {
                $('.atas').html(data['atas']);
                $('.bawah').html(data['bawah']);
                $('#main-table').DataTable({
                    scrollY: "60vh",
                    scrollX: true,
                    scrollCollapse: true,
                    fixedColumns: {
                        leftColumns: 0,
                        rightColumns: 0
                    },
                    searching: false,
                    ordering : false,
                    paging: false
                });
            }
        });
    }

    // search data
    $(document).on('click', '#search', function() {
        var moon = $('#moon').val()
        var year = $('#year').val()
        var app = $('#app').val()
        var code = $('#code').val()

        if(moon != '' && year != '' && app != '' && code != '') {
            $('#moonExcel').val(moon)
            $('#yearExcel').val(year)
            $('#appExcel').val(app)
            $('#codeExcel').val(code)

            $('.t-automatical').css('display', 'flex')

            $('#main-table').DataTable().clear().destroy()
            searchReportDeliverySlip(moon, year, app, code)
        } else if(code == '') {
            SW.warning({
                message: 'Supplier Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else if(app == '') {
            SW.warning({
                message: 'Status Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else if(moon == '') {
            SW.warning({
                message: 'Month Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else if(year == '') {
            SW.warning({
                message: 'Year Cannot Empty',
            })
            $('#main-table').DataTable().clear().destroy()
        } else {
            $('#main-table').DataTable().clear().destroy()
            searchReportDeliverySlip()
        }
    })

    // get function SOAP Business relation
    $(document).on('click', '#seacrhSOAPBusinesRelationModal', function(){
        $('#get-supplier').DataTable().clear().destroy()
        getSupplier();
    })

    // view data supplier
    function getSupplier() {
        $('#get-supplier').DataTable({
            // processing: true,
            responsive: true,
            // serverSide: true,
            paging: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'ct_vd_addr',
                },
                {
                    data: 'ct_ad_name',
                    name: 'ct_ad_name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // select modal business relation
    $(document).on('click', '#selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        $('#name').val(namebusiness)
        $('#getSupplierTarget').click()
    })

    // reset
    $('#reset').click(function() {
        $('.t-automatical').css('display', 'none')

        $('#moon').val('')
        $('#year').val('')
        $('#app').val('')
        $('#code').val('')
        $('#name').val('')
        $('#moonExcel').val('')
        $('#yearExcel').val('')
        $('#appExcel').val('')
        $('#codeExcel').val('')
        $('#main-table').DataTable().clear().destroy()
        searchReportDeliverySlip()
    })
})
