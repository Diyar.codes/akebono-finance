$(()=> {

    $(document).on('click', '#seacrhSOAPBusinesRelation', function(){
        $('#get-supplier').DataTable().clear().destroy()
        $('#getSupplierTarget').modal('show');
        getSupplier();
    });

    $(document).on('click', '#get-supplier #selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        // $('#code').val(codebusiness)
        $('#code').val(codebusiness)
        $('#name').val(namebusiness)
        $('#getSupplierTarget').click()
    });

    function getSupplier() {
        // getLoader
        getLoader()

        // view data supplier
        $('#get-supplier').DataTable({
            // processing: true,
            responsive: true,
            // serverSide: true,
            ajax: {
                url: `${BASEURL}get-supplier-local`,
            },
            columns:[
                {
                    data: 'ct_vd_addr',
                    name: 'supplier id',
                },
                {
                    data: 'ct_ad_name',
                    name: 'supplier name',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // gate date
    fill_datatable()

    // view date
    function fill_datatable(filter_dn_number = '', filter_moon = '', filter_year = '', filter_id_supp = '') {
        $('#main-table').DataTable({
            processing: true,
            paging: true,
            scroller: {
                rowHeight: 1000
            },
            fixedHeader:{
                header:true,
                headerOffset: $('#fixed').height(),
            },
            serverSide: true,
            searching: true,
            ordering : false,
            ajax: {
                url: `${BASEURL}delivery-notes`,
                data: {
                    filter_dn_number: filter_dn_number,
                    filter_moon: filter_moon,
                    filter_year: filter_year,
                    filter_id_supp:filter_id_supp,
                }
            },
            columns:[
                {
                    data: 'dn_tr_id',
                    name: 'dn_tr_id',
                },
                {
                    data: 'dn_po_nbr',
                    name: 'dn_po_nbr',
                },
                {
                    data: 'dn_vend',
                    name: 'dn_vend',
                },
                {
                    data: 'ct_ad_name',
                    name: 'ct_ad_name',
                },
                {
                    data: 'dn_order_date',
                    name: 'dn_order_date',
                },
                {
                    data: 'dn__char01',
                    name: 'dn__char01',
                },
                {
                    data: 'dn_order_date',
                    name: 'dn_order_date',
                },
                {
                    data: 'dn__char02',
                    name: 'dn__char02',
                },
                {
                    data: 'action',
                    name: 'action',
                },
            ],
        })
    }

    // change thead th datatable color
    $(".table-bordered thead th").css("background-color", "#0066CC");

    // change thead th datatable font color
    $("table thead th").css("color", "#fff");

    // filter
    $('#filter').click(function() {
        var filter_dn_number = $('#filter_dn_number').val()
        var filter_moon = $('#filter_moon').val()
        var filter_year = $('#filter_year').val()
        var filter_id_supp = $('#code').val()

        if(filter_dn_number != '' || filter_moon != '' || filter_year != '' || filter_id_supp != '') {
            $('#main-table').DataTable().destroy()
            fill_datatable(filter_dn_number, filter_moon, filter_year, filter_id_supp)
        } else {
            alert('Select Both filter Opinion')
        }
    })

    // modal select
    $(document).on('click', '#selectbusiness', function() {
        var codebusiness = $(this).data('code')
        var namebusiness = $(this).data('name')

        $('#code').val(codebusiness)
        $('#name').val(namebusiness)
        $('#modaldemo8part2').click()
    })

    // reset
    $('#reset').click(function() {
        $('#filter_dn_number').val('')
        $('#filter_moon').val('')
        $('#filter_year').val('')
        $('#code').val('')
        $('#name').val('')
        $('#main-table').DataTable().destroy()
        fill_datatable()
    })
})
