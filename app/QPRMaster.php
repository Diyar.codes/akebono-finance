<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QPRMaster extends Model
{
    protected $guarded = [];

    protected $table = 'qprl_master';

    public $timestamps = false;
}
