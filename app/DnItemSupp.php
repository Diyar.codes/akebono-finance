<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DnItemSupp extends Model
{
    protected $table = 'dnd_det';

    public $timestamps = false;

    protected $guarded = [];
}
