<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplieMasterSuratJalan extends Model
{
    protected $table = 'Supplier_MSTR_suratJalan';

    protected $fillable = [
		'no',
	    'tujuan',
	    'tanggal',
	    'kendaraan',
	    'keterangan',
	    'cycle',
	    'pengirim',
	    'creadate',
	    'type'         ];

    public $timestamps = false;
}
