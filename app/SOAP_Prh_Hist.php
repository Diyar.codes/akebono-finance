<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SOAP_Prh_Hist extends Model
{
    protected $table = 'SOAP_prh_hist';

    public $timestamps = false;

    protected $guarded = [];

}
