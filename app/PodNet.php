<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PodNet extends Model
{
    protected $table = 'pub_item_supp';

    public $timestamps = false;

    protected $guarded = [];
}
