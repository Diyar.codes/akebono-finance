<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier_pic_ppc extends Model
{
    protected $table = 'supplier_pic_ppc';

    public $timestamps = false;

    protected $guarded = [];
}
