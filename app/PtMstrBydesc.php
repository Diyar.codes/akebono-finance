<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PtMstrBydesc extends Model
{
    protected $table = 'SOAP_pt_mstr_bydesc';

    public $timestamps = false;

    protected $guarded = [];
}
