<?php 

    function inboundSJ($kode, $tanggal, $act_qty, $kd, $locFrom, $locTo){
		$xml_header ="".
		"<?xml version='1.0' encoding='UTF-8'?>
		<soapenv:Envelope xmlns='urn:schemas-qad-com:xml-services'
		  xmlns:qcom='urn:schemas-qad-com:xml-services:common'
		  xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wsa='http://www.w3.org/2005/08/addressing'>
		<soapenv:Header>
			<wsa:Action/>
			<wsa:To>urn:services-qad-com:qad2014ee</wsa:To>
			<wsa:MessageID>urn:services-qad-com::qad2014ee</wsa:MessageID>
			<wsa:ReferenceParameters>
			  <qcom:suppressResponseDetail>true</qcom:suppressResponseDetail>
			</wsa:ReferenceParameters>
			<wsa:ReplyTo>
			  <wsa:Address>urn:services-qad-com:</wsa:Address>
			</wsa:ReplyTo>
		 </soapenv:Header>
			<soapenv:Body>
				<transferInventory>
					<qcom:dsSessionContext>
						<qcom:ttContext>
						  <qcom:propertyQualifier>QAD</qcom:propertyQualifier>
						  <qcom:propertyName>domain</qcom:propertyName>
						  <qcom:propertyValue>AAIJ</qcom:propertyValue>
						  <qcom:propertyValue/>
						</qcom:ttContext>
						<qcom:ttContext>
						  <qcom:propertyQualifier>QAD</qcom:propertyQualifier>
						  <qcom:propertyName>scopeTransaction</qcom:propertyName>
						  <qcom:propertyValue>false</qcom:propertyValue>
						</qcom:ttContext>
						<qcom:ttContext>
						  <qcom:propertyQualifier>QAD</qcom:propertyQualifier>
						  <qcom:propertyName>version</qcom:propertyName>
						  <qcom:propertyValue>eB_2</qcom:propertyValue>
						</qcom:ttContext>
						<qcom:ttContext>
						  <qcom:propertyQualifier>QAD</qcom:propertyQualifier>
						  <qcom:propertyName>mnemonicsRaw</qcom:propertyName>
						  <qcom:propertyValue>false</qcom:propertyValue>
						</qcom:ttContext>
						<!--
						<qcom:ttContext>
						  <qcom:propertyQualifier>QAD</qcom:propertyQualifier>
						  <qcom:propertyName>username</qcom:propertyName>
						  <qcom:propertyValue/>
						</qcom:ttContext>
						<qcom:ttContext>
						  <qcom:propertyQualifier>QAD</qcom:propertyQualifier>
						  <qcom:propertyName>password</qcom:propertyName>
						  <qcom:propertyValue/>
						</qcom:ttContext>
						-->
						<qcom:ttContext>
						  <qcom:propertyQualifier>QAD</qcom:propertyQualifier>
						  <qcom:propertyName>action</qcom:propertyName>
						  <qcom:propertyValue/>
						</qcom:ttContext>
						<qcom:ttContext>
						  <qcom:propertyQualifier>QAD</qcom:propertyQualifier>
						  <qcom:propertyName>entity</qcom:propertyName>
						  <qcom:propertyValue/>
						</qcom:ttContext>
						<qcom:ttContext>
						  <qcom:propertyQualifier>QAD</qcom:propertyQualifier>
						  <qcom:propertyName>email</qcom:propertyName>
						  <qcom:propertyValue/>
						</qcom:ttContext>
						<qcom:ttContext>
						  <qcom:propertyQualifier>QAD</qcom:propertyQualifier>
						  <qcom:propertyName>emailLevel</qcom:propertyName>
						  <qcom:propertyValue/>
						</qcom:ttContext>
					  </qcom:dsSessionContext>";
						
				$xml_data = "".
					"<dsInventoryTransfer>
						<inventoryTransfer>
							<operation>A</operation>
							<part>".$kode."</part>
							<lotserialQty>".$act_qty."</lotserialQty>
							<inventoryDetail>
								<operation>A</operation>
								<lotserialQty>".$act_qty."</lotserialQty>
								<nbr>".$kd."</nbr>
								<siteFrom>AAIJ</siteFrom>
								<locFrom>".$locFrom."</locFrom>
								<locTo>".$locTo."</locTo>
								<siteTo>AAIJ</siteTo>
								<effDate>".$tanggal."</effDate>
								<yn>true</yn>
							</inventoryDetail>
						</inventoryTransfer>
					</dsInventoryTransfer>
				</transferInventory>
			</soapenv:Body>
		</soapenv:Envelope>";

		$xml = $xml_header." ".$xml_data;
		 echo "<pre>".htmlentities($xml)."</pre>";
		//$url = 'http://192.168.0.42:8080/qxilive/services/QdocWebService';
		$url = 'http://qadee2014.akebono-astra.co.id:8989/qxisim/services/QdocWebService';
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml","SOAPAction: \"/soap/action/query\"", "Content-length: ".strlen($xml))); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		// return $response;
        //for get elementTage
		$dom = new DOMDocument;
		$dom->loadXML($response);
        
		$result = $dom->getElementsByTagName("result");
		$result1 = $dom->getElementsByTagName("tt_msg_desc");
		
		//echo "<pre>".htmlentities($response)."</pre>";
		//echo "<pre>".htmlentities($result)."</pre>";
		
		echo "<pre>".htmlentities($xml)."</pre>"."<br><br>";
		echo "<pre>".htmlentities($response)."</pre>"."<br><br>";
		
        $error = '';
		foreach ($result as $results) 
		{
			$errorMessage=(string)$results->nodeValue;
			if($errorMessage=="error")
			{
				$error_desc = $dom->getElementsByTagName("tt_msg_desc");
				$hitung=1;
				foreach($error_desc as $desc)
				{
					$error_desc=(string)$desc->nodeValue;
					$error = $error.$hitung.".".$error_desc;
					$hitung +=1;
				}
			}
			else
			{
				$error_desc = $dom->getElementsByTagName("tt_msg_desc");
				$hitung=1;
				foreach($error_desc as $desc)
				{
					$error_desc=(string)$desc->nodeValue;
					$error = $error.$hitung.".".$error_desc;
					$hitung +=1;
				}
			}
		}
		$keterangan = $errorMessage.",Detail:".$error;
		//END 
		curl_close($ch);
		
		if (stristr($response, 'error') == TRUE) {
			$ret = false;
		}else{
			$ret = true;
		}
        $datanya = array(
            'keterangan' => $keterangan,
            'return'     => $ret,
        );
        return $datanya;
	}



?>