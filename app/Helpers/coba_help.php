<?php

use App\SOAPPoMstr;
use App\SOAPPoDetail;
use App\SOAPPtMstr;
use App\SOAPPubBusinessRelation;
use App\SOAP_Prh_Hist;
use Illuminate\Support\Facades\Storage;

    //ambil data PO Master -> Masuk Database
    function getDataPO($url){

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('po_mstr.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/po_mstr.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
        // echo json_encode($arrOutput);
		SOAPPoMstr::truncate();

		$total_row = count($arrOutput['ttPO']['ttPORow']);
		for($i = 0 ; $i < $total_row; $i++){

			$po_nbr = $arrOutput['ttPO']['ttPORow'][$i]['po_number'];
			$po_due_date = $arrOutput['ttPO']['ttPORow'][$i]['POdue_date'];
			$kode_supp = $arrOutput['ttPO']['ttPORow'][$i]['kode_supplier'];
			$po_status = $arrOutput['ttPO']['ttPORow'][$i]['po_status'];
			$nama_supplier = $arrOutput['ttPO']['ttPORow'][$i]['nama_supplier'];
			$tanggal = \Carbon\Carbon::parse($po_due_date)->format('Y-m-d');

			$data = new SOAPPoMstr;
			$data->po_number = $po_nbr;
			$data->POdue_date = $tanggal;
			$data->kode_supplier = $kode_supp;
			$data->nama_supplier = $nama_supplier;
			if($po_status){
				$data->po_status = $po_status;
			}
            $data->po_domain = 'AAIJ';
			$data->save();
		}

	}
    //ambil PO Master --> langsung , ga masuk database
    function po_mstr($url){

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('po_mstr.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/po_mstr.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
        $datanya = $arrOutput['ttPO']['ttPORow'];
        return $datanya;

	}
    //ambil supplier --> masuk database
    function get_supplier($url){
        $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $headers = array(
                "Accept: application/xml",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $resp = curl_exec($curl);
            curl_close($curl);

            $find = "&lt;/soapenv:Envelope&gt;";
            $arr = explode($find, $resp, 2);
            $first = $arr[1];

            $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
            $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
            $arrrep = ["&","\"",">","<", "", "","", "","", ""];
            $rep = str_replace($arrfind,$arrrep,$first);

            Storage::put('supplier.xml', $rep);

            libxml_use_internal_errors(TRUE);

            $objXmlDocument = simplexml_load_file(storage_path('app')."/supplier.xml");

            if ($objXmlDocument === FALSE) {
                echo "There were errors parsing the XML file.\n";
            foreach(libxml_get_errors() as $error) {
                echo $error->message;
            }
            exit;
            }

            $objJsonDocument = json_encode($objXmlDocument);
            $arrOutput = json_decode($objJsonDocument, TRUE);
            $total_row = count($arrOutput['ttSupplier']['ttSupplierRow']);
            // echo json_encode($arrOutput['ttSupplier']['ttSupplierRow']);
            SOAPPubBusinessRelation::truncate();
            for($i = 0 ; $i < $total_row; $i++){

                $ct_vd_addr = $arrOutput['ttSupplier']['ttSupplierRow'][$i]['ct_vd_addr'];
                $ct_vd_type = $arrOutput['ttSupplier']['ttSupplierRow'][$i]['ct_vd_type'];
                $ct_ad_name = $arrOutput['ttSupplier']['ttSupplierRow'][$i]['ct_ad_name'];
                $ct_ad_line1 = $arrOutput['ttSupplier']['ttSupplierRow'][$i]['ct_ad_line1'];
                $ct_ad_line2 = $arrOutput['ttSupplier']['ttSupplierRow'][$i]['ct_ad_line2'];
                $ct_ad_city = $arrOutput['ttSupplier']['ttSupplierRow'][$i]['ct_ad_city'];
                // $tanggal = \Carbon\Carbon::parse($po_due_date)->format('Y-m-d');

                $data = new SOAPPubBusinessRelation;
                $data->ct_vd_addr = $ct_vd_addr;
                if($ct_vd_type){
                    $data->ct_vd_type = $ct_vd_type;
                }
                $data->ct_ad_name = $ct_ad_name;

                if($ct_ad_line1){
                    $data->ct_ad_line1 = $ct_ad_line1;
                }
                if($ct_ad_line2){
                    $data->ct_ad_line2 = $ct_ad_line2;
                }
                if($ct_ad_city){
                    $data->ct_ad_city = $ct_ad_city;
                }

                $data->save();
        }
    }
    //ambil po detail --> masuk database
	function getPODetail($no_po){
		$url = "https://supplier.akebono-astra.co.id/wsatdw/po_detail.php?no_po=".$no_po;
        // dd($url);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('po_detail.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/po_detail.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
		$total_row = count($arrOutput['ttPO_detail']['ttPO_detailRow']);
		// echo $total_row;
		// echo json_encode($arrOutput['ttPO_detail']['ttPO_detailRow']);
		SOAPPoDetail::truncate();
		for($i = 0 ; $i < $total_row; $i++){

			$item_number = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_number'];
			$no_po = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['no_po'];
			$qty_po = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['qty_po'];
			$qty_receive = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['qty_receive'];
			$no_pp = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['no_pp'];
			$item_deskripsi = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_deskripsi'];
			$item_price = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_price'];
			$item_disc = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_disc'];
			$po_status = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['po_status'];
			$po_um = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['po_um'];
            $podRequest = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['podRequest'];
			$podLoc = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['podLoc'];

			$data = new SOAPPoDetail;
			$data->item_number = $item_number;
			$data->no_po = $no_po;
			$data->qty_po = $qty_po;
			$data->qty_receive = $qty_receive;
			$data->no_pp = $no_pp;
			if($item_deskripsi){
				$data->item_deskripsi = $item_deskripsi;
			}else{
                $data->item_deskripsi = '-';
            }
			$data->item_price = $item_price;
			$data->item_disc = $item_disc;
			if($po_status){
				$data->po_status = $po_status;
			}else{
                $data->po_status = '-';
            }
			$data->po_um = $po_um;
            $data->podRequest = $podRequest;
            if($po_status){
				$data->podLoc = $podLoc;
			}else{
                $data->podLoc = '-';
            }
            $data->line = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['line'];
            $data->po_domain = 'AAIJ';
			$data->save();
		}

	}
    //ambil PO Detail + item --> masuk database
    function getPODetailItem($no_po){
		$url = "https://supplier.akebono-astra.co.id/wsatdw/po_detail_item.php?no_po=".$no_po;
        // dd($url);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('po_detail.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/po_detail.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
		$total_row = count($arrOutput['ttPO_detail']['ttPO_detailRow']);
		// echo $total_row;
		// echo json_encode($arrOutput['ttPO_detail']['ttPO_detailRow']);
		SOAPPoDetail::truncate();
		for($i = 0 ; $i < $total_row; $i++){

			$item_number = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_number'];
			$no_po = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['no_po'];
			$qty_po = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['qty_po'];
			$qty_receive = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['qty_receive'];
			$no_pp = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['no_pp'];
			$item_deskripsi = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_deskripsi'];
			$item_price = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_price'];
			$item_disc = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_disc'];
			$po_status = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['po_status'];
			$po_um = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['po_um'];
            $podRequest = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['podRequest'];
			$podLoc = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['podLoc'];
            $line = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['line'];
            $item_type = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_type'];

			$data = new SOAPPoDetail;
			$data->item_number = $item_number;
			$data->no_po = $no_po;
			$data->qty_po = $qty_po;
			$data->qty_receive = $qty_receive;
			$data->no_pp = $no_pp;
			if($item_deskripsi){
				$data->item_deskripsi = $item_deskripsi;
			}
			$data->item_price = $item_price;
			$data->item_disc = $item_disc;
            $data->item_type = $item_type;
			if($po_status){
				$data->po_status = $po_status;
			}
			$data->po_um = $po_um;
            $data->podRequest = $podRequest;
            if($po_status){
				$data->podLoc = $podLoc;
			}
            $data->line = $line;

			$data->save();
		}

	}
    //untung ambil data barang yang sudah datang di warehouse
    function get_data_kedatangan($item,$d1,$d2,$supplier){
        $date1 = date('m-d-Y',strtotime($d1));
        $date2 = date('m-d-Y',strtotime($d2));
        $url = 'https://supplier.akebono-astra.co.id/wsatdw/get_data_kedatangan.php?part='.$item.'&periode1='.$date1.'&periode2='.$date2.'&supplier='.$supplier.'';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    
        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    
        $resp = curl_exec($curl);
        curl_close($curl);
    
        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];
    
        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);
    
        Storage::put('prh_hist.xml', $rep);
    
        libxml_use_internal_errors(TRUE);
    
        $objXmlDocument = simplexml_load_file(storage_path('app')."/prh_hist.xml");
        // return $objXmlDocument;
        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }
    
        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
    
        if(empty($arrOutput['tt_data_kedatangan']['tt_data_kedatanganRow'])){
            return false;
        }else{
            if(!empty($arrOutput['tt_data_kedatangan']['tt_data_kedatanganRow'][0])){
                $datanya = $arrOutput['tt_data_kedatangan']['tt_data_kedatanganRow'];
                $dataALl =  $datanya;
            }else{
                $datanya = $arrOutput['tt_data_kedatangan']['tt_data_kedatanganRow'];
                $data[0] = $datanya;
                $dataALl = $data;
            }
            return $dataALl;
        }
    }
    //ambil pt_mstr --> masuk database
	function getDataPtMstr($item){
        //example url, url+ lempar item
		$url = "https://supplier.akebono-astra.co.id/wsatdw/pt_mstr.php?item=".$item;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('pt_mstr.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/pt_mstr.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
		// echo json_encode($arrOutput);
		$total_row = count($arrOutput['ttItem']['ttItemRow']);
	// 	// echo $total_row;
		// SOAPPtMstr::truncate();
		// for($i = 0 ; $i < $total_row; $i++){

			$item_number = $arrOutput['ttItem']['ttItemRow']['item_number'];
			$deskripsi1 = $arrOutput['ttItem']['ttItemRow']['deskripsi1'];
            if($arrOutput['ttItem']['ttItemRow']['deskripsi2']){
                $deskripsi2 = $arrOutput['ttItem']['ttItemRow']['deskripsi2'];
            }else{
                $deskripsi2 = "-";
            }
			$um = $arrOutput['ttItem']['ttItemRow']['um'];
			$buyer_planner = $arrOutput['ttItem']['ttItemRow']['buyer_planner'];
			$desc_type = $arrOutput['ttItem']['ttItemRow']['desc_type'];

            $nama_supplier = "-";

            if($arrOutput['ttItem']['ttItemRow']['lokasi_item']){
                $lokasi_item = $arrOutput['ttItem']['ttItemRow']['lokasi_item'];
            }else{
                $lokasi_item = "-";
            }
		// 	// $tanggal = \Carbon\Carbon::parse($po_due_date)->format('Y-m-d');

            SOAPPtMstr::updateOrCreate(
                ['item_number' => $item_number],
                [
                    'deskripsi1'    => $deskripsi1,
                    'deskripsi2'    => $deskripsi2,
                    'um'            => $um,
                    'buyer_planner' => $buyer_planner,
                    'desc_type'     => $desc_type,
                    'nama_supplier' => $nama_supplier,
                    'lokasi_item'   => $lokasi_item,
                    'pt_domain'     => 'AAIJ',
                ]
                );
	}

    //ambil PO Detail --> langsung , ga masuk database
    function po_detail($no_po){
        $url = "https://supplier.akebono-astra.co.id/wsatdw/po_detail.php?no_po=".$no_po;
        // dd($url);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        // dd($resp);
        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('po_detail.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/po_detail.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        // $objJsonDocument = json_encode($objXmlDocument);
        // $arrOutput = json_decode($objJsonDocument, TRUE);
        // echo $total_row;
        // $datanya = $arrOutput['ttPO_detail']['ttPO_detailRow'];

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
        if(empty($arrOutput['ttPO_detail']['ttPO_detailRow'])){
            return false;
        }else{
            if(!empty($arrOutput['ttPO_detail']['ttPO_detailRow'][0])){
                $datanya = $arrOutput['ttPO_detail']['ttPO_detailRow'];
                return $datanya;
            }else{
                $datanya = $arrOutput['ttPO_detail']['ttPO_detailRow'];
                $data[0] = $datanya;
                return $data;
            }
        }

        // return $datanya;

    }
    //ambil code satuan UM --> Langsung pake ga masuk database
    function code_um($um){
        $url = "https://supplier.akebono-astra.co.id/wsatdw/pub_code_mstr.php?um=".$um;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('pt_mstr.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/pt_mstr.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
        $datanya = $arrOutput['tt_master_Satuan']['tt_master_SatuanRow'];
        return $datanya;
    }
    //ambil loc_mstr --> langsung, ga masuk database
    function loc_mstr($url){

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('pt_mstr.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/pt_mstr.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
        $datanya = $arrOutput['tt_master_Loc']['tt_master_LocRow'];
        return $datanya;
    }
    //ambil pt_mstr --> langsung pake, ga masuk database
    function pt_mstr($item){
        //example url, url+ lempar item
        $url = "https://supplier.akebono-astra.co.id/wsatdw/pt_mstr.php?item=".$item;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('pt_mstr.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/pt_mstr.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
        // $datanya = $arrOutput['ttItem']['ttItemRow'];


        if(empty($arrOutput['ttItem']['ttItemRow'])){
            return false;
        }else{
            if(!empty($arrOutput['ttItem']['ttItemRow'][0])){
                $datanya = $arrOutput['ttItem']['ttItemRow'];
                return $datanya;
            }else{
                $datanya = $arrOutput['ttItem']['ttItemRow'];
                $data= $datanya;
                return $data;
            }
        }
    }
    //ambil prh_hist -->langsung pke, ga masuk database
    function prh_hist($po){
        $url = "https://supplier.akebono-astra.co.id/wsatdw/prh_hist.php?no_po=".$po;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('prh_hist.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/prh_hist.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
        // $datanya = $arrOutput['tt_prh']['tt_prhRow'];


        if(empty($arrOutput['tt_prh']['tt_prhRow'])){
            return false;
        }else{
            if(!empty($arrOutput['tt_prh']['tt_prhRow'][0])){
                $datanya = $arrOutput['tt_prh']['tt_prhRow'];
                return $datanya;
            }else{
                $datanya = $arrOutput['tt_prh']['tt_prhRow'];
                $data[0] = $datanya;
                return $data;
            }
        }

        // return $datanya;

	}
    //ambil prh_hist --> masuk database
    function getPrhHist($po){
        $url = "https://supplier.akebono-astra.co.id/wsatdw/prh_hist.php?no_po=".$po;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('prh_hist.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/prh_hist.xml");
        // return $objXmlDocument;
        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);

        if(empty($arrOutput['tt_prh']['tt_prhRow'])){
            return false;
        }else{
            if(!empty($arrOutput['tt_prh']['tt_prhRow'][0])){
                $datanya = $arrOutput['tt_prh']['tt_prhRow'];
                $dataALl =  $datanya;
            }else{
                $datanya = $arrOutput['tt_prh']['tt_prhRow'];
                $data[0] = $datanya;
                $dataALl = $data;
            }
        }

        SOAP_Prh_Hist::truncate();
        foreach($dataALl as $row){
            $prhLine = $row['prhLine'];
            $prhNbr = $row['prhNbr'];
            $prhVend = $row['prhVend'];
            $prhPart = $row['prhPart'];
            $prhReceiver = $row['prhReceiver'];
            $prhPsNbr = $row['prhPsNbr'];
            $prhRcvd = $row['prhRcvd'];
            $prhRcp_date = \Carbon\Carbon::parse($row['prhRcp_date'])->format('Y-m-d');
            $prhReason = $row['prhReason'];
            // $tanggal = \Carbon\Carbon::parse($po_due_date)->format('Y-m-d');

            $data = new SOAP_Prh_Hist;
            $data->prhLine = $prhLine;
            $data->prhNbr = $prhNbr;
            $data->prhVend = $prhVend;
            $data->prhPart = $prhPart;

            $data->prhReceiver = $prhReceiver;

            $data->prhPsNbr = $prhPsNbr;
            $data->prhRcvd = $prhRcvd;
            $data->prhRcp_date = $prhRcp_date;
            if($prhReason){
                $data->prhReason = $prhReason;
            }
            $data->prhDomain = 'AAIJ';
            $data->save();
        }
    }

// line
function getDataPtMstrByD($item){
    $url = "https://supplier.akebono-astra.co.id/wsatdw/pt_mstr.php?item=".$item;

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
    $arrrep = ["&","\"",">","<", "", "","", "","", ""];
    $rep = str_replace($arrfind,$arrrep,$first);

    Storage::put('pt_mstr.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app')."/pt_mstr.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
    foreach(libxml_get_errors() as $error) {
        echo $error->message;
    }
    exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);
    $data = $arrOutput['ttItem'];
    if (key_exists('ttItemRow', $data))
    {
        return $data['ttItemRow'];
    }
}

function getQtyStockBegin($item, $loc, $bulan, $tahun) {
    $url = "https://supplier.akebono-astra.co.id/wsatdw/get_qtyStockBegin.php?item=".$item."&lokasi=".$loc."&bulan=".$bulan."&tahun=".$tahun."";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
    $arrrep = ["&","\"",">","<", "", "","", "","", ""];
    $rep = str_replace($arrfind,$arrrep,$first);

    Storage::put('get_qtyStockBegin.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app')."/get_qtyStockBegin.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
    foreach(libxml_get_errors() as $error) {
        echo $error->message;
    }
    exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);
    $data = $arrOutput['ttSupplier_stockBegin'];
    if (key_exists('ttSupplier_stockBeginRow', $data))
    {
        return $data['ttSupplier_stockBeginRow'];
    }
}

function getAdjust($item, $loc, $bulan, $tahun) {
    $url = "https://supplier.akebono-astra.co.id/wsatdw/get_Adjust.php?item=".$item."&lokasi=".$loc."&bulan=".$bulan."&tahun=".$tahun."";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
    $arrrep = ["&","\"",">","<", "", "","", "","", ""];
    $rep = str_replace($arrfind,$arrrep,$first);

    Storage::put('get_Adjust.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app')."/get_Adjust.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
    foreach(libxml_get_errors() as $error) {
        echo $error->message;
    }
    exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);
    $data = $arrOutput['tt_qtyAdjust'];
    if (key_exists('tt_qtyAdjustRow', $data))
    {
        return $data['tt_qtyAdjustRow'];
    }
}

function getStockIn($item, $loc, $bulan, $tahun) {
    $url = "https://supplier.akebono-astra.co.id/wsatdw/get_StockIn.php?item=".$item."&lokasi=".$loc."&bulan=".$bulan."&tahun=".$tahun."";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
    $arrrep = ["&","\"",">","<", "", "","", "","", ""];
    $rep = str_replace($arrfind,$arrrep,$first);

    Storage::put('get_StockIn.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app')."/get_StockIn.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
    foreach(libxml_get_errors() as $error) {
        echo $error->message;
    }
    exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);
    $data = $arrOutput['ttSupplier_stockIn'];
    if (key_exists('ttSupplier_stockInRow', $data))
    {
        return $data['ttSupplier_stockInRow'];
    }
}

function getStockOut($item, $loc, $bulan, $tahun) {
    $url = "https://supplier.akebono-astra.co.id/wsatdw/get_StockOut.php?item=".$item."&lokasi=".$loc."&bulan=".$bulan."&tahun=".$tahun."";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
    $arrrep = ["&","\"",">","<", "", "","", "","", ""];
    $rep = str_replace($arrfind,$arrrep,$first);

    Storage::put('get_StockOut.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app')."/get_StockOut.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
    foreach(libxml_get_errors() as $error) {
        echo $error->message;
    }
    exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);
    $data = $arrOutput['ttSupplier_stockOut'];
    if (key_exists('ttSupplier_stockOutRow', $data))
    {
        return $data['ttSupplier_stockOutRow'];
    }
}

function getStockInDate($item, $loc, $date) {
    $url = "https://supplier.akebono-astra.co.id/wsatdw/get_StockIn_date.php?item=".$item."&lokasi=".$loc."&periode=".$date."";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
    $arrrep = ["&","\"",">","<", "", "","", "","", ""];
    $rep = str_replace($arrfind,$arrrep,$first);

    Storage::put('get_StockIn_date.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app')."/get_StockIn_date.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
    foreach(libxml_get_errors() as $error) {
        echo $error->message;
    }
    exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);
    $data = $arrOutput['ttSupplier_stockInDate'];

    if (key_exists('ttSupplier_stockInDateRow', $data))
    {
        return $data['ttSupplier_stockInDateRow'];
    }
}

function getStockOutDate($item, $loc, $date) {
    $url = "https://supplier.akebono-astra.co.id/wsatdw/get_StockOut_date.php?item=".$item."&lokasi=".$loc."&periode=".$date."";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
    $arrrep = ["&","\"",">","<", "", "","", "","", ""];
    $rep = str_replace($arrfind,$arrrep,$first);

    Storage::put('get_StockOut_date.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app')."/get_StockOut_date.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
    foreach(libxml_get_errors() as $error) {
        echo $error->message;
    }
    exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);
    $data = $arrOutput['ttSupplier_stockOutDate'];
    if (key_exists('ttSupplier_stockOutDateRow', $data))
    {
        return $data['ttSupplier_stockOutDateRow'];
    }
}

function get_open_po($po,$line){
    $url = 'https://supplier.akebono-astra.co.id/wsatdw/transaction_view_dev.php?po='.$po.'&line='.$line.'';
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
    $arrrep = ["&","\"",">","<", "", "","", "","", ""];
    $rep = str_replace($arrfind,$arrrep,$first);

    Storage::put('open_po.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app')."/open_po.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
    foreach(libxml_get_errors() as $error) {
        echo $error->message;
    }
    exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);

    if(empty($arrOutput['ttOpenPO']['ttOpenPORow'])){
        return false;
    }else{
        if(!empty($arrOutput['ttOpenPO']['ttOpenPORow'][0])){
            $datanya = $arrOutput['ttOpenPO']['ttOpenPORow'];
            $dataALl =  $datanya;
        }else{
            $datanya = $arrOutput['ttOpenPO']['ttOpenPORow'];
            $data[0] = $datanya;
            $dataALl = $data;
        }
        return $dataALl;
    }



}

function prh_hist_rc($rc){
    $url = "https://supplier.akebono-astra.co.id/wsatdw/get_Prh_HistByReceiver.php?receiver=".$rc;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
    $arrrep = ["&","\"",">","<", "", "","", "","", ""];
    $rep = str_replace($arrfind,$arrrep,$first);

    Storage::put('prh_hist.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app')."/prh_hist.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
    foreach(libxml_get_errors() as $error) {
        echo $error->message;
    }
    exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);
    if(empty($arrOutput['tt_prh_receiver']['tt_prh_receiverRow'])){
        return false;
    }else{
        if(!empty($arrOutput['tt_prh_receiver']['tt_prh_receiverRow'][0])){
            $datanya = $arrOutput['tt_prh_receiver']['tt_prh_receiverRow'];
            return $datanya;
        }else{
            $datanya = $arrOutput['tt_prh_receiver']['tt_prh_receiverRow'];
            $data[0] = $datanya;
            return $data;
        }
    }
}

function get_Prh_HistByReceiver($receiver){
    $url = "https://supplier.akebono-astra.co.id/wsatdw/get_Prh_HistByReceiver.php?receiver=".$receiver;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
    $arrrep = ["&","\"",">","<", "", "","", "","", ""];
    $rep = str_replace($arrfind,$arrrep,$first);

    Storage::put('prh_hist.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app')."/prh_hist.xml");
    // return $objXmlDocument;
    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
    foreach(libxml_get_errors() as $error) {
        echo $error->message;
    }
    exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);

    if(empty($arrOutput['tt_prh_receiver']['tt_prh_receiverRow'])){
        return false;
    }else{
        if(!empty($arrOutput['tt_prh_receiver']['tt_prh_receiverRow'][0])){
            $datanya = $arrOutput['tt_prh_receiver']['tt_prh_receiverRow'];
            $dataALl =  $datanya;
        }else{
            $datanya = $arrOutput['tt_prh_receiver']['tt_prh_receiverRow'];
            $data[0] = $datanya;
            $dataALl = $data;
        }
    }

    return $dataALl;
}

function tanggal_TrHist($receiver){
    $url = 'https://supplier.akebono-astra.co.id/wsatdw/get_tanggal_TrHist.php?receiver='.$receiver;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
    $arrrep = ["&","\"",">","<", "", "","", "","", ""];
    $rep = str_replace($arrfind,$arrrep,$first);

    Storage::put('prh_hist.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app')."/prh_hist.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
    foreach(libxml_get_errors() as $error) {
        echo $error->message;
    }
    exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);
    if(empty($arrOutput['tt_tanggal_trHist']['tt_tanggal_trHistRow'])){
        return false;
    }else{
        if(!empty($arrOutput['tt_tanggal_trHist']['tt_tanggal_trHistRow'][0])){
            $datanya = $arrOutput['tt_tanggal_trHist']['tt_tanggal_trHistRow'];
            $dataALl =  $datanya['trDate'];
        }else{
            $datanya = $arrOutput['tt_tanggal_trHist']['tt_tanggal_trHistRow'];
            $data = $datanya;
            $dataALl = $data['trDate'];
        }
    }
    return $dataALl;
}

function q_used($item)
{
    $url = "https://supplier.akebono-astra.co.id/wsatdw/get_Used.php?item=" . $item;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;", "&quot;", "&gt;", "&lt;", "<pre>", "</pre>", "</SOAP-ENV:Body>", "</SOAP-ENV:Envelope>", "<SOAP-ENV:Body>", $env];
    $arrrep = ["&", "\"", ">", "<", "", "", "", "", "", ""];
    $rep = str_replace($arrfind, $arrrep, $first);

    Storage::put('q_used.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app') . "/q_used.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
        foreach (libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);
    $datanya = $arrOutput['tt_qtyUsed']['tt_qtyUsedRow'];

    return $datanya;
}

function tr_hits($periode1, $periode2, $item, $lokasi)
{
    $url = "https://supplier.akebono-astra.co.id/wsatdw/get_qtyMan.php?part=" . $item . "&periode1=" . $periode1 . "&periode2=" . $periode2 . "&lokasi=" . $lokasi;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;", "&quot;", "&gt;", "&lt;", "<pre>", "</pre>", "</SOAP-ENV:Body>", "</SOAP-ENV:Envelope>", "<SOAP-ENV:Body>", $env];
    $arrrep = ["&", "\"", ">", "<", "", "", "", "", "", ""];
    $rep = str_replace($arrfind, $arrrep, $first);

    Storage::put('tr_hist.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app') . "/tr_hist.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
        foreach (libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);
    $datanya = $arrOutput['tt_qtyMan'];

    if (!empty($datanya)) {
        $datanya = $arrOutput['tt_qtyMan']['tt_qtyManRow'];
    }

    return $datanya;
}

function pt_mstr_bydesc($like)
{
    //example url, url+ lempar item
    $url = "https://supplier.akebono-astra.co.id/wsatdw/pt_mstrByDesc.php?deskripsi=" . $like;

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
        "Accept: application/xml",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);

    $find = "&lt;/soapenv:Envelope&gt;";
    $arr = explode($find, $resp, 2);
    $first = $arr[1];

    $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
    $arrfind = ["&amp;", "&quot;", "&gt;", "&lt;", "<pre>", "</pre>", "</SOAP-ENV:Body>", "</SOAP-ENV:Envelope>", "<SOAP-ENV:Body>", $env];
    $arrrep = ["&", "\"", ">", "<", "", "", "", "", "", ""];
    $rep = str_replace($arrfind, $arrrep, $first);

    Storage::put('pt_mstr_bydesc.xml', $rep);

    libxml_use_internal_errors(TRUE);

    $objXmlDocument = simplexml_load_file(storage_path('app') . "/pt_mstr_bydesc.xml");

    if ($objXmlDocument === FALSE) {
        echo "There were errors parsing the XML file.\n";
        foreach (libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
    }

    $objJsonDocument = json_encode($objXmlDocument);
    $arrOutput = json_decode($objJsonDocument, TRUE);
    if(empty($arrOutput['ttItem_desc']['ttItem_descRow'])){
        return false;
    }else{
        if(!empty($arrOutput['ttItem_desc']['ttItem_descRow'][0])){
            $datanya = $arrOutput['ttItem_desc']['ttItem_descRow'];
            return $datanya;
        }else{
            $datanya = $arrOutput['ttItem_desc']['ttItem_descRow'];
            $data[0] = $datanya;
            return $data;
        }
    }
}