<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplieDTLSuratJalan extends Model
{
    protected $table = 'supplier_DTL_suratJalan';

    // protected $primaryKey = 'no';

    protected $guarded = [];

    public $timestamps = false;
}
