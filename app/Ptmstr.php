<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ptmstr extends Model
{
    protected $table = 'pub_pt_mstr';

    public $timestamps = false;

    protected $guarded = [];
}
