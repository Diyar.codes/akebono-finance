<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QPRMsSjPrc extends Model
{
    protected $table = 'qpr_ms_sj_prc';

    public $timestamps = false;

    protected $guarded = [];
}
