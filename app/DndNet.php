<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DndNet extends Model
{
    protected $table = 'dnd_det';

    public $timestamps = false;

    protected $guarded = [];
}
