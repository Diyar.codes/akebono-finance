<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QPRDtSjPrc extends Model
{
    protected $table = 'qpr_dt_sj_prc';

    public $timestamps = false;

    protected $guarded = [];
}
