<?php

// use Storage;
use App\SOAPPoMstr;
use App\SOAPPoDetail;
use App\SOAPPtMstr;
use App\SOAPPubBusinessRelation;

    function removeNamespaceFromXML( $xml )
    {
        // Because I know all of the the namespaces that will possibly appear in
        // in the XML string I can just hard code them and check for
        // them to remove them
        $toRemove = ['rap', 'turss', 'crim', 'cred', 'j', 'rap-code', 'evic'];
        // This is part of a regex I will use to remove the namespace declaration from string
        $nameSpaceDefRegEx = '(\S+)=["\']?((?:.(?!["\']?\s+(?:\S+)=|[>"\']))+.)["\']?';

        // Cycle through each namespace and remove it from the XML string
    foreach( $toRemove as $remove ) {
            // First remove the namespace from the opening of the tag
            $xml = str_replace('<' . $remove . ':', '<', $xml);
            // Now remove the namespace from the closing of the tag
            $xml = str_replace('</' . $remove . ':', '</', $xml);
            // This XML uses the name space with CommentText, so remove that too
            $xml = str_replace($remove . ':commentText', 'commentText', $xml);
            // Complete the pattern for RegEx to remove this namespace declaration
            $pattern = "/xmlns:{$remove}{$nameSpaceDefRegEx}/";
            // Remove the actual namespace declaration using the Pattern
            $xml = preg_replace($pattern, '', $xml, 1);
        }

        // Return sanitized and cleaned up XML with no namespaces
        return $xml;
    }

    function responSOAP($xml) {
        $array = xml_to_array($xml);
    }


    function test(){
        $sidemenu = DB::table('sidemenu')->get();

        $arr = [];
        foreach ($sidemenu as $key) {
          array_push($arr,[
            'name' => $key->name,
          ]);
        }

        $data = implode('',$arr);
        return $data;

    }

    function getDataPO($url){
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];
        dd($resp);
        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('po_mstr.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/po_mstr.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
		SOAPPoMstr::truncate();

		$total_row = count($arrOutput['ttPO']['ttPORow']);
		for($i = 0 ; $i < $total_row; $i++){

			$po_nbr = $arrOutput['ttPO']['ttPORow'][$i]['po_number'];
			$po_due_date = $arrOutput['ttPO']['ttPORow'][$i]['POdue_date'];
			$kode_supp = $arrOutput['ttPO']['ttPORow'][$i]['kode_supplier'];
			$po_status = 0;
			$nama_supplier = $arrOutput['ttPO']['ttPORow'][$i]['nama_supplier'];
			$tanggal = \Carbon\Carbon::parse($po_due_date)->format('Y-m-d');

			$data = new SOAPPoMstr;
			$data->po_number = $po_nbr;
			$data->POdue_date = $tanggal;
			$data->kode_supplier = $kode_supp;
			$data->nama_supplier = $nama_supplier;
			$data->po_status = $po_status;
            $data->po_domain = 'AAIJ';
			$data->save();
		}
	}

    function get_supplier($url){
        $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $headers = array(
                "Accept: application/xml",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $resp = curl_exec($curl);
            curl_close($curl);

            $find = "&lt;/soapenv:Envelope&gt;";
            $arr = explode($find, $resp, 2);
            $first = $arr[1];

            $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
            $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
            $arrrep = ["&","\"",">","<", "", "","", "","", ""];
            $rep = str_replace($arrfind,$arrrep,$first);

            Storage::put('supplier.xml', $rep);

            libxml_use_internal_errors(TRUE);

            $objXmlDocument = simplexml_load_file(storage_path('app')."/supplier.xml");

            if ($objXmlDocument === FALSE) {
                echo "There were errors parsing the XML file.\n";
            foreach(libxml_get_errors() as $error) {
                echo $error->message;
            }
            exit;
            }

            $objJsonDocument = json_encode($objXmlDocument);
            $arrOutput = json_decode($objJsonDocument, TRUE);
            $total_row = count($arrOutput['ttSupplier']['ttSupplierRow']);
            echo json_encode($arrOutput);
            SOAPPubBusinessRelation::truncate();
            for($i = 0 ; $i < $total_row; $i++){

                $ct_vd_addr = $arrOutput['ttSupplier']['ttSupplierRow'][$i]['ct_vd_addr'];
                $ct_vd_type = $arrOutput['ttSupplier']['ttSupplierRow'][$i]['ct_vd_type'];
                $ct_ad_name = $arrOutput['ttSupplier']['ttSupplierRow'][$i]['ct_ad_name'];
            //     // $tanggal = \Carbon\Carbon::parse($po_due_date)->format('Y-m-d');

                $data = new SOAPPubBusinessRelation;
                $data->ct_vd_addr = $ct_vd_addr;
                if($ct_vd_type){
                    $data->ct_vd_type = $ct_vd_type;
                }
                $data->ct_ad_name = $ct_ad_name;
                $data->save();
            }
	}

	function getPODetaill($url){


        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('po_detail.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/po_detail.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
		$total_row = count($arrOutput['ttPO_detail']['ttPO_detailRow']);
		// echo $total_row;
		// echo json_encode($arrOutput);
		SOAPPoDetail::truncate();
		for($i = 0 ; $i < $total_row; $i++){

			$item_number = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_number'];
			$no_po = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['no_po'];
			$qty_po = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['qty_po'];
			$qty_receive = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['qty_receive'];
			$no_pp = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['no_pp'];
			$item_deskripsi = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_deskripsi'];
			$item_price = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_price'];
			$item_disc = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_disc'];
			$po_status = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['po_status'];
			$po_um = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['po_um'];
			// $tanggal = \Carbon\Carbon::parse($po_due_date)->format('Y-m-d');

			$data = new SOAPPoDetail;
			$data->item_number = $item_number;
			$data->no_po = $no_po;
			$data->qty_po = $qty_po;
			$data->qty_receive = $qty_receive;
			$data->no_pp = $no_pp;
			if($item_deskripsi){
				$data->item_deskripsi = $item_deskripsi;
			}
			$data->item_price = $item_price;
			$data->item_disc = $item_disc;
			if($po_status){
				$data->po_status = $po_status;
			}
			$data->po_um = $po_um;

			$data->save();
		}

	}

	function getDataPtMstr($url,$item){
        //example url, url+ lempar item
		// $url = "https://supplier.akebono-astra.co.id/wsatdw/pt_mstr.php/04465-0D200-001";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('pt_mstr.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/pt_mstr.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
		echo json_encode($arrOutput);
		// $total_row = count($arrOutput['ttItem']['ttItemRow']);
	// 	// echo $total_row;
		// SOAPPtMstr::truncate();
		// for($i = 0 ; $i < $total_row; $i++){

			$item_number = $arrOutput['ttItem']['ttItemRow']['item_number'];
			$deskripsi1 = $arrOutput['ttItem']['ttItemRow']['deskripsi1'];
			$deskripsi2 = $arrOutput['ttItem']['ttItemRow']['deskripsi2'];
			$um = $arrOutput['ttItem']['ttItemRow']['um'];
			$buyer_planner = $arrOutput['ttItem']['ttItemRow']['buyer_planner'];
			$desc_type = $arrOutput['ttItem']['ttItemRow']['desc_type'];
			$nama_supplier = $arrOutput['ttItem']['ttItemRow']['nama_supplier'];
		// 	// $tanggal = \Carbon\Carbon::parse($po_due_date)->format('Y-m-d');

			$data = new SOAPPtMstr;
			$data->item_number = $item_number;
			$data->deskripsi1 = $deskripsi1;
			if($deskripsi2){
				$data->deskripsi2 = $deskripsi2;
			}
			$data->um = $um;
			$data->buyer_planner = $buyer_planner;
			$data->desc_type = $desc_type;
			if($nama_supplier){
				$data->nama_supplier = $nama_supplier;
			}
			$data->save();
		// }

	}
?>
