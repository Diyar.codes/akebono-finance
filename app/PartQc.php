<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartQc extends Model
{
    protected $table = 'part_qc';

    public $timestamps = false;

    protected $guarded = [];
}
