<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DNDHistory extends Model
{
    protected $table = 'dnd_history';

    public $timestamps = false;

    protected $guarded = [];
}
