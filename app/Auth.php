<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auth extends Model
{
    protected $table = 'pub_login';

    public $timestamps = false;

    protected $guarded = [];
}
