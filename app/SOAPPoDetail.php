<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SOAPPoDetail extends Model
{
    protected $table = 'SOAP_po_detail';

    public $timestamps = false;

    protected $guarded = [];
}
