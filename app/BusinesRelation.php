<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinesRelation extends Model
{
    protected $table = 'SOAP_Pub_Business_Relation';

    public $timestamps = false;

    protected $guarded = [];
}
