<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SOAPPoMstr extends Model
{
    protected $table = 'SOAP_po_mstr';

    public $timestamps = false;

    protected $guarded = [];
}
