<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportDeliveryNote extends Model
{
    protected $table = 'pub_dn_master';
    public function get_dn($s){
        $cari = DB::table('pub_dn_master')->select('dn_tr_id','dn_po_nbr','dn_order_date','dn_vend')
                ->where('dn_vend', $s)->get();
        return $cari;
    }
}
