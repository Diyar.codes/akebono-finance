<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterCycle extends Model
{
    protected $guarded = [];

    protected $table = 'whs_cycle_master';

    public $timestamps = false;
}
