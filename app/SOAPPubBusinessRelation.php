<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SOAPPubBusinessRelation extends Model
{
    protected $table = 'SOAP_Pub_Business_Relation';

    public $timestamps = false;

    protected $guarded = [];
}
