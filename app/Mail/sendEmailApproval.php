<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class sendEmailApproval extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($no, $accountEmail, $level)
    {
        $this->no = $no;
        $this->accountEmail = $accountEmail;
        $this->level = $level;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->level == 'check') {
            return $this->subject("From:QPR PT. AKEBONO BRAKE ASTRA INDONESIA<qpr@akebono-astra.co.id> \r\n")
                ->from('dev@majapahit.id', 'Akebono Supplier')
                ->subject("New Quality Problem Report No " . $this->no . "")
                ->view('email.sendApproval')
                ->with(
                    [
                        'no' => $this->no,
                        'accountEmail' => $this->accountEmail,
                        'url' => asset('qpr-approval/' . base64_encode($this->no) . '/' . base64_encode($this->level))
                    ]
                );
        }

        if ($this->level == 'approve') {
            return $this->subject("From:QPR PT. AKEBONO BRAKE ASTRA INDONESIA<qpr@akebono-astra.co.id> \r\n")
                ->from('dev@majapahit.id', 'Akebono Supplier')
                ->subject("New Quality Problem Report No " . $this->no . "")
                ->view('email.sendApproval')
                ->with(
                    [
                        'no' => $this->no,
                        'accountEmail' => $this->accountEmail,
                        'url' => asset('qpr-approval/' . base64_encode($this->no) . '/' . base64_encode($this->level))
                    ]
                );
        }

        if ($this->level == 'supplier') {
            return $this->subject("From:QPR PT. AKEBONO BRAKE ASTRA INDONESIA<qpr@akebono-astra.co.id> \r\n")
                ->from('dev@majapahit.id', 'Akebono Supplier')
                ->subject("New Quality Problem Report No " . $this->no . "")
                ->view('email.sendSupplier')
                ->with(
                    [
                        'no' => $this->no,
                        'accountEmail' => $this->accountEmail,
                        'url' => asset('qpr-approval/' . base64_encode($this->no) . '/' . base64_encode($this->level))
                    ]
                );
        }
    }
}
