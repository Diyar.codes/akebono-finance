<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailDeliveryNote extends Mailable
{
    use Queueable, SerializesModels;

    public $data_email;

    public function __construct($data_email){
        $this->data_email = $data_email;
    }

    public function build(){
        return $this->subject('New Delivery Note From AAIJ')
                        // ->from(env('MAIL_FROM_ADDRESS'))
                        ->cc('nico.sanmediatek@gmail.com')
                        ->view('email.maildeliverynotes');
    }
}
