<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class SentApprove extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct($no_bpb, $nama, $namasect)
    {
        $this->no_bpb = $no_bpb;
        $this->nama = $nama;
        $this->namasect = $namasect;
    }

    public function build()
    {
        $query = DB::table('whs_bon')->where('id', $this->no_bpb)->first();
        $header = '';
        $header = "<table width='70%' border='0' cellspacing = '0' cellpadding='0'>
				<tr align=''>
					<td width='30%'>BPB Number</td>
					<td width='3%'>:</td>
					<td >".$query->kode_transaksi." </td>
				</tr>
				<tr align=''>
					<td width=''>Requester</td>
					<td>:</td>
					<td width=''>".$query->nama_pengambil."</td>
				</tr>
				<tr align=''>
					<td width=''>Pick Time</td>
					<td>:</td>
					<td width=''>".$query->tgl_ambil." Pukul ".$query->waktu."</td>
				</tr>
			</table>";

	    $header .= "<br><br><center><b> List Change Part Quantity<b></center><br><br> ";

        $sqlchg = DB::table('whs_bon')
                                ->join('whs_bon_brg', 'whs_bon.kode_transaksi', '=', 'whs_bon_brg.id_bon')
                                ->where('whs_bon.id', $this->no_bpb)
                                ->whereNotNull('whs_bon_brg.change_reason')
                                ->select('whs_bon_brg.*')
                                ->get();
        $header .= "<table width='80%' style='background-color:#0066CC;font-color:#fff;text-align:center;' border='0' cellspacing = '0' cellpadding='0'>
        <tr align=''>
            <td>No</td>
            <td>Part Number</td>
            <td>Part Name</td>
            <td>Qty Request</td>
            <td>Qty Approve</td>
            <td>Change Reason</td>
            <td>Change By</td>
        </tr>";
        $no =1;
        foreach ($sqlchg as  $rdt) {
            $header .= "<tr>
                            <td style='text-align:center;color:#fff'>". $no++ ."</td>
                            <td style='text-align:center;color:#fff'>".$rdt->kd_brg."</td>
                            <td style='text-align:center;color:#fff'>".$rdt->nm_brg."</td>
                            <td style='text-align:center;color:#fff'>".$rdt->jml_brg."</td>
                            <td style='text-align:center;color:#fff'>".$rdt->change_jml."</td>
                            <td style='text-align:center;color:#fff'>".$rdt->change_reason."</td>
                            <td style='text-align:center;color:#fff'>".$rdt->changeby."</td>
				        </tr>";
        }
        $header .= "</table>";
        $pesan 	= $header;
        return $this->subject('BPB Warehouse Change Quantiry Alert')
                        ->from('dev@majapahit.id')
                        ->view('email.sendApprove')
                        ->with([
                            'data' => $pesan,
                            'name' => $this->namasect,
                        ]
                    );
    }
}
