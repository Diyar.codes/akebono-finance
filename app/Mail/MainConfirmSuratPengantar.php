<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\SupplieMasterSuratJalan;

class MainConfirmSuratPengantar extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dataemail, $idCon, $nameto)
    {
        $this->dataemail = $dataemail;
        $this->idCon = $idCon;
        $this->nameto = $nameto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dataemail = SupplieMasterSuratJalan::leftjoin('supplier_DTL_suratjalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratjalan.no')->where('supplier_DTL_suratjalan.no', '=', $this->idCon)->get();

        $html = '';
        $html .= "<table width='100%' border='1' cellspacing = '0' cellpadding='0'>
                    <tr align='center'>
                        <td width=''> <b> No </b> </td>
                        <td width=''> <b> Supplier </b> </td>
                        <td width=''> <b> Date </b> </td>
                        <td width=''> <b> Item Number </b> </td>
                        <td width=''> <b> Description </b> </td>
                        <td width=''> <b> Qty </b> </td>
                        <td width=''> <b> Act Qty </b> </td>
                        <td width=''> <b> Remark </b> </td>
                        <td width=''> <b> Status QAD </b> </td>
                    </tr>";
        $no =1;
        foreach($dataemail as $data) {
            $html .= "<tr>
                        <td align='center' > ".$no++." </td>
                        <td align='left' > ".$data->tujuan." </td>
                        <td align='left' > ".$data->tanggal." </td>
                        <td align='center' > ".$data->kode." </td>
                        <td align='center' > ".$data->namabarang." </td>
                        <td align='center' > ".$data->jumlah." </td>
                        <td align='center' > ".$data->act_qty." </td>
                        <td align='center' > ".$data->remark." </td>
                        <td align='center' > ".$data->qxInbound." </td>
                    </tr>";
        }

        $html .= "</table>";

        $header = "From:Online Confirm Delivery Slip<helpdesk@akebono-astra.co.id> \r\n";
        //$header .= "Cc:it-team@akebono-astra.co.id \r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-type: text/html\r\n";

        return $this->subject('Different Qty DS ' . $this->idCon)
                        // ->from('dev@majapahit.id')
                        ->cc('diyar.codes16@gmail.com')
                        ->view('email.mailconfirmsuratpengantar')
                        ->with([
                            'data' => $html,
                            'header' => $header,
                            'idCon' => $this->idCon,
                            'nameto' => $this->nameto,
                        ]
                    );
    }
}
