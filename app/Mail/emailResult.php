<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class emailResult extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("From:QPR PT. AKEBONO BRAKE ASTRA INDONESIA<qpr@akebono-astra.co.id> \r\n")
            ->from('dev@majapahit.id', 'Akebono Supplier')
            ->subject("QPR Confirmation " . $this->data['qpr_no'] . "")
            ->view('email.emailResult')
            ->with(
                [
                    'data' => $this->data,
                ]
            );
    }
}
