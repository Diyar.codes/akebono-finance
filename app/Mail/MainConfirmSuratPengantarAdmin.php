<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\SupplieMasterSuratJalan;


class MainConfirmSuratPengantarAdmin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dataemail, $idCon, $nameto, $attach_file)
    {
        $this->dataemail = $dataemail;
        $this->idCon = $idCon;
        $this->nameto = $nameto;
        $this->$attach_file = $attach_file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dataemail = SupplieMasterSuratJalan::leftjoin('supplier_DTL_suratjalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratjalan.no')->where('supplier_DTL_suratjalan.no', '=', $this->idCon)->get();
        $attachment = DB::select("SELECT no,attachment from Supplier_MSTR_suratJalan where no='$this->idCon'");
        $attach_file = $attachment[0]->attachment;

        $html = '';
        $html .= "<table width='100%' border='1' cellspacing = '0' cellpadding='0'>
                    <tr align='center'>
                        <td width='5%'> <b> No </b> </td>
                        <td width='10%'> <b> Tujuan </b> </td>
                        <td width='8%'> <b> Date </b> </td>
                        <td width='10%'> <b> Kode </b> </td>
                        <td width='15%'> <b> Nama Barang </b> </td>
                        <td width='15%'> <b> Qty </b> </td>
                        <td width='15%'> <b> Act Qty </b> </td>
                    </tr>";
        $no =1;
        foreach($dataemail as $data) {
            $html .= "<tr>
                        <td align='center' > ".$data->no." </td>
                        <td align='left' > ".$data->tujuan." </td>
                        <td align='left' > ".$data->tanggal." </td>
                        <td align='center' > ".$data->kode." </td>
                        <td align='center' > ".$data->namabarang." </td>
                        <td align='center' > ".$data->jumlah." </td>
                        <td align='center' > ".$data->act_qty." </td>
                    </tr>";
        }

        $html .= "</table>";

        if(strlen($attach_file) > 1) {
            $attach = "http://supplier.akebono-astra.co.id/attachment/".rawurlencode($attach_file)."";
			$see = "See Attachment";
        } else {
            $attach = "Tidak ada attachment";
			$see = "Tidak ada attachment";
        }

        return $this->subject('Surat Pengantar Supplier Indotech ' . $this->idCon)
                        // ->from('dev@majapahit.id')
                        ->cc('diyar.codes16@gmail.com')
                        ->view('email.mailconfirmsuratpengantaradmin')
                        ->with([
                            'data' => $html,
                            'idCon' => $this->idCon,
                            'nameto' => $this->nameto,
                            'attach' => $attach,
                            'see' => $see
                        ]
                    );
    }
}
