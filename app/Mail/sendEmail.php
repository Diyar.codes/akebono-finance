<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class sendEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($transaksi_id, $approval_po, $pengirim, $approval_email)
    {
        $this->transaksi_id = $transaksi_id;
        $this->approval_po = $approval_po;
        $this->pengirim = $pengirim;
        $this->approval_email = $approval_email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = DB::table('PO_change_type')
            ->where('transaksi_id', $this->transaksi_id)
            ->get();

        // dd($data);

        $html = '';
        $html .= '
                <table width="100%" border="1" cellspacing="0" cellpadding="0">
                    <tr align="center">
                        <td width="10%"> <b> PO Number </b> </td>
                        <td width="10%"> <b> Line </b> </td>
                        <td width="20%"> <b> Item Number </b> </td>
                        <td width="50%"> <b> Description </b> </td>
                        <td width="15%"> <b> Type PO</b> </td>
                        <td width="5%"> Revisi Type PO </td>
                    </tr>';
        foreach ($data as $row) {
            $no_po = $row->no_po;
            $line_po = $row->line_po;
            $item = $row->po_item;
            $deskripsi = $row->po_deskripsi;
            $type_po = $row->type_po;
            $type_po_revisi = $row->type_po_rev;
            $effdate = $row->effdate;

            $html .= '
                <tr>
                    <td align="center" width="10%"> ' . $no_po . ' </td>
                    <td align="left" width="20%"> '.$line_po.'  </td>
                    <td align="left" width="20%"> '.$item.'  </td>
                    <td align="left" width="50%"> ' . $deskripsi . ' </td>
                    <td align="center" width="15%"> ' . $type_po . ' </td>
                    <td align="center" width="5%"> ' . $type_po_revisi . ' </td>
                </tr>
            ';
        }
        $html .= '</table>';


        return $this->from('dev@majapahit.id', $this->pengirim)
            ->subject('Change Type Approval ' . $this->transaksi_id)
            ->view('email.index')
            ->with(
                [
                    'data' => $html,
                    'transaksi_id' => $this->transaksi_id,
                    'email_to' => $this->approval_email,
                    'pengirim' => $this->pengirim,
                    'username' => $this->approval_po,
                    'url' => asset('intrans-approve-change/'.base64_encode($this->approval_po).'/'.base64_encode($this->transaksi_id))
                ]
            );
    }
}
