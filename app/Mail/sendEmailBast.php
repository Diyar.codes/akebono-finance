<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class sendEmailBast extends Mailable
{
    use Queueable, SerializesModels;

    public $data_email;

    public function __construct($data_email)
    {
        $this->data_email = $data_email;
    }

    public function build()
    {
        if ($this->data_email['step'] == 'FINISH') {
            return $this->subject('BAST APPROVAL FINISH ' . date('Y') . ' ' . $this->data_email['nama_supp'])
                ->from(env('MAIL_FROM_ADDRESS'))
                ->view('email.completedBastApproval');
        } else if ($this->data_email['step'] == 'PROSES') {
            return $this->subject('BAST APPROVAL ' . date('Y') . ' ' . $this->data_email['nama_supp'])
                ->from(env('MAIL_FROM_ADDRESS'))
                ->view('email.sendApprovalBast');
        } else if ($this->data_email['step'] == 'REJECT') {
            return $this->subject('BAST REJECT Information ' . date('Y'))
                ->from(env('MAIL_FROM_ADDRESS'))
                ->view('email.sendRejectBast');
        } else if ($this->data_email['step'] == 'REJECT_TO_SUPP') {
            return $this->subject('BAST REJECT INFO ' . date('Y'))
                ->from(env('MAIL_FROM_ADDRESS'))
                ->view('email.sendRejectBastToSupp');
        }
    }
}
