<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SettingCloseEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $thn, $bln, $status;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($thn, $bln, $status)
    {
        $this->thn = $thn;
        $this->bln = $bln;
        $this->status = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $email = "helpdesk@akebono-astra.co.id";
        //$email = "qad-bounces@akebono-astra.co.id";
        //$email = "helpdesk@akebono-astra.co.id";
        return $this->subject('[Mfgpro] Open GL ' . $this->bln . ' ' . $this->thn)
            ->from('dev@majapahit.id', 'Reminder Open GL')
            ->view('email.mailsettingcloseopenwhs');
    }
}
