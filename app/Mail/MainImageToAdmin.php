<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\SupplieMasterSuratJalan;

class MainImageToAdmin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($idCon)
    {
        $this->idCon = $idCon;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $query = SupplieMasterSuratJalan::leftjoin('supplier_DTL_suratjalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratjalan.no')->where('supplier_DTL_suratjalan.no', '=', $this->idCon)->get();
        $attachment = SupplieMasterSuratJalan::where('no', '=', $this->idCon)->first();
        $attach_file = $attachment->attachment;

        $html = '';
        $html .= "<table width='100%' border='1' cellspacing = '0' cellpadding='0'>
                <tr align='center'>
                    <td width='5%'> <b> No </b> </td>
                    <td width='10%'> <b> Tujuan </b> </td>
                    <td width='8%'> <b> Date </b> </td>
                    <td width='10%'> <b> Kode </b> </td>
                    <td width='15%'> <b> Nama Barang </b> </td>
                    <td width='15%'> <b> Qty </b> </td>
                    <td width='15%'> <b> Act Qty </b> </td>
                </tr>";

        foreach($query as $data) {
            $html .= "<tr>
                        <td align='center' > ".$data->no." </td>
                        <td align='left' > ".$data->tujuan." </td>
                        <td align='left' > ".$data->tanggal." </td>
                        <td align='center' > ".$data->kode." </td>
                        <td align='center' > ".$data->namabarang." </td>
                        <td align='center' > ".$data->jumlah." </td>
                        <td align='center' > ".$data->act_qty." </td>
                    </tr>";
        }
        $html .= "</table>";

        if(strlen($attachment->attachment) > 1) {
            $attach = "http://supplier.akebono-astra.co.id/attachment/".rawurlencode($attach_file)."";
            $see = "See Attachment";
        } else {
            $attach = "Tidak ada attachment";
            $see = "Tidak ada attachment";
        }

        $header = "From:Admin Warehouse<admin.w@akebono-astra.co.id> \r\n";
        $header .= "Cc:djuhni.s@akebono-astra.co.id,m.rohmat.s@akebono-astra.co.id,suherman@akebono-astra.co.id,ardi.n@akebono-astra.co.id \r\n";
        //$header .= "Cc:imam.k@akebono-astra.co.id,rahma.d@akebono-astra.co.id \r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-type: text/html\r\n";

        return $this->subject('"Surat Pengantar Supplier Indotech ' . $this->idCon)
                        ->from('dev@majapahit.id')
                        ->view('email.mailimagetoadmin')
                        ->with([
                            'data' => $html,
                            'attach' => $attach,
                            'see' => $see,
                            'header' => $header,
                        ]
                    );
    }
}
