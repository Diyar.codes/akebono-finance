<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MainCounterMeasureSupplier extends Mailable
{
    use Queueable, SerializesModels;

    public $no_qpr, $supp_name, $revisi, $supp_kode,$address;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($no_qpr, $supp_name, $revisi, $supp_kode,$address)
    {
        $this->no_qpr = $no_qpr;
        $this->supp_name = $supp_name;
        $this->revisi = $revisi;
        $this->supp_kode = $supp_kode;
        $this->address = $address;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Respond Quality Problem Report From Supplier')
                // ->from(env('MAIL_FROM_NAME'))
                ->cc('mdiwafairusd@gmail.com')
                ->view('email.mailcountermeasureaction');
    }
}
