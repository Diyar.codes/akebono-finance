<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortalDet extends Model
{
    protected $table = 'portald_det';

    public $timestamps = false;

    protected $guarded = [];
    protected $primaryKey = null;
	public $incrementing = false;
}
