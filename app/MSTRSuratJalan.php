<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MSTRSuratJalan extends Model
{
    protected $guarded = [];

    protected $table = 'supplier_m_s_t_r_surat_jalan';

    public $timestamps = false;
}
