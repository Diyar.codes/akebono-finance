<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SOAPPtMstr extends Model
{
    protected $table = 'SOAP_pt_mstr';

    public $timestamps = false;

    protected $guarded = [];
}
