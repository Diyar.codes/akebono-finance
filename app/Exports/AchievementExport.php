<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use DB;

class AchievementExport implements FromCollection,WithHeadings,WithStyles
{

	protected $item, $po, $bulan,$tahun,$supp;

	function __construct($item, $po, $bulan,$tahun,$supp) {
        $this->item     = $item;
        $this->po       = $po;
        $this->bulan    = $bulan;
        $this->tahun    = $tahun;
        $this->supp     = $supp;
 	}

    public function collection(){

        $data = DB::table('dn_mstr')
            ->join('dnd_det','dn_mstr.dn_tr_id','=','dnd_det.dnd_tr_id')
            ->leftJoin('dn_item_supp', 'dn_item_supp.kd_item', '=', 'dnd_det.dnd_part')
            ->select('dn_mstr.dn_tr_id','dnd_det.dnd_part')
            ->where('dn_vend',$this->supp)
            ->whereMonth('dn_arrival_date',$this->bulan)
            ->whereYear('dn_arrival_date',$this->tahun)
            ->get();

        return $data;
    }

    public function headings(): array {
        return [
            'No Delivery Note',
            'Item',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }
}
