<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

use DB;

class SummaryDeliveryNote implements FromCollection,WithHeadings,ShouldAutoSize,WithStyles
{

	protected $code, $moon, $year, $app;

	function __construct($code, $moon, $year, $app) {
        $this->code = $code;
        $this->moon = $moon;
        $this->year = $year;
        $this->app = $app;
 	}

    public function collection()
    {
        if($this->app) {
            $query = DB::table('dn_mstr')
                    ->join('dnd_det', 'dnd_det.dnd_tr_id', '=', 'dn_mstr.dn_tr_id')
                    ->leftJoin('dn_item_supp', 'dn_item_supp.kd_item', '=', 'dnd_det.dnd_part')
                    ->select('dn_tr_id', 'dn_arrival_date', 'dnd_part', 'dn_vend', 'pcs_kanban')
                    ->where('dn_vend', '=', $this->code)
                    ->whereMonth('dn_arrival_date', $this->moon)
                    ->whereYear('dn_arrival_date', $this->year)
                    ->where('dn_user_id', '=', $this->app)
                    ->orderBy('dn_arrival_date', 'ASC')
                    ->get();
        } else {
            $query = DB::table('dn_mstr')
            ->join('dnd_det', 'dnd_det.dnd_tr_id', '=', 'dn_mstr.dn_tr_id')
            ->leftJoin('dn_item_supp', 'dn_item_supp.kd_item', '=', 'dnd_det.dnd_part')
            ->select('dn_tr_id', 'dn_arrival_date', 'dnd_part', 'dn_vend', 'pcs_kanban')
            ->where('dn_vend', '=', $this->code)
            ->whereMonth('dn_arrival_date', $this->moon)
            ->whereYear('dn_arrival_date', $this->year)
            ->orderBy('dn_arrival_date', 'ASC')
            ->get();
        }

            $j = 0;
            $data_group = [];
            $data_group_value = [];
            foreach($query as $da) {
                $part = $da->dnd_part;
                $kode = $da->dn_tr_id;
                $tanggal = $da->dn_arrival_date;
                $day = \Carbon\Carbon::parse($da->dn_arrival_date)->format('d');

                if($day < 10) {
                    $endDay = explode(0,$day);
                    $makeDay = $endDay[1];
                } else {
                    $endDay = $day;
                    $makeDay = $endDay;
                }

                if($da->pcs_kanban == "") {
                    $ambil_kanban = 1;
                } else {
                    $ambil_kanban = $da->pcs_kanban;
                }

                $get_data_qty = DB::table('dn_mstr')
                                ->join('dnd_det', 'dnd_det.dnd_tr_id', '=', 'dn_mstr.dn_tr_id')
                                ->select('dnd_qty_order')
                                ->where('dn_vend', '=', $da->dn_vend)
                                ->where('dn_tr_id', '=', $da->dn_tr_id)
                                ->where('dnd_part', '=', $da->dnd_part)
                                ->where('dn_arrival_date', '=', $tanggal)
                                ->orderBy('dn_arrival_date', 'ASC')
                                ->first();

                $qtyKanban = $ambil_kanban * $get_data_qty->dnd_qty_order;

                $cur_data = $part."||".$kode;

                if(!in_array($cur_data, $data_group)) {
                    $data_group[$j] = $cur_data;
                    $j++;
                }

                $data_group_value[$cur_data][$makeDay] = $qtyKanban;
            }

            $dataend = [];
            foreach($query as $da1) {
                $part = $da1->dnd_part;
                $kode = $da1->dn_tr_id;

                array_push($dataend, $da1->dn_tr_id);
                array_push($dataend, $da1->dnd_part);

                for ($z = 1; $z <= date('t', mktime(0, 0, 0, $this->moon, 1, $this->year)); $z++){
                    if (empty($data_group_value[$part."||".$kode][$z])) {
                        array_push($dataend, '0');
                    } else {
                        array_push($dataend, $data_group_value[$part."||".$kode][$z]);
                    }
                }
            }

            $tglend = [];
            $spec = date('t', mktime(0, 0, 0, $this->moon, 1, $this->year));
            $specplus = $spec + 2;

            foreach ($query as $qwerty => $dtnd) {
                $tglend[] = array_slice($dataend, $specplus * $qwerty, $specplus);
            }
            return collect($tglend);
    }

    public function headings(): array {
        $tgl = ['No Delivery Note', 'Item'];
        for ($z = 1; $z <= date('t', mktime(0, 0, 0, $this->moon, 1, $this->year)); $z++){
            $tgl[] = $z;
        }
        return $tgl;
    }

    public function styles(Worksheet $sheet) {
        $query = DB::table('dn_mstr')
                ->join('dnd_det', 'dnd_det.dnd_tr_id', '=', 'dn_mstr.dn_tr_id')
                ->leftJoin('dn_item_supp', 'dn_item_supp.kd_item', '=', 'dnd_det.dnd_part')
                ->select('dn_tr_id', 'dn_arrival_date', 'dnd_part', 'dn_vend', 'pcs_kanban')
                ->where('dn_vend', '=', $this->code)
                ->where('dn_user_id', '=', $this->app)
                ->whereMonth('dn_arrival_date', $this->moon)
                ->whereYear('dn_arrival_date', $this->year)
                ->orderBy('dn_arrival_date', 'ASC')
                ->get();

        $countBorder = count($query);

        for($x = 1; $x <= $countBorder+1; $x++ ) {
            $help[$x] = array(
                'font'      => array('bold' => true),
                'borders'   => array(
                    'allBorders'    => array(
                    'borderStyle'   => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color'         => array('rgb' => '000000'),
                    ),
                    ),
                );
        }

        return $help;
    }
}
