<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use DB;

class CounterMeasureExport implements FromView,WithStyles,ShouldAutoSize
{
    protected $type;
    protected $qprNo;
    protected $supplier;
    protected $from;
    protected $to;

	function __construct($type, $qprNo, $supplier, $from, $to) {
        $this->type = $type;
        $this->qprNo = $qprNo;
        $this->supplier = $supplier;
        $this->from = $from;
        $this->to = $to;
 	}

    public function view(): View {
        if($this->type && $this->qprNo && $this->supplier && $this->from && $this->to) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->where('qprl_supplier', '=', $this->supplier)
            ->whereBetween('qprl_date_issue', [$this->from, $this->to])
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->type && $this->qprNo && $this->from && $this->to) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->whereBetween('qprl_date_issue', [$this->from, $this->to])
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->type && $this->supplier && $this->from && $this->to) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->where('qprl_supplier', '=', $this->supplier)
            ->whereBetween('qprl_date_issue', [$this->from, $this->to])
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->qprNo && $this->supplier && $this->from && $this->to) {
            $data = DB::table('qprl_master')
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->where('qprl_supplier', '=', $this->supplier)
            ->whereBetween('qprl_date_issue', [$this->from, $this->to])
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->type && $this->qprNo && $this->supplier) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->where('qprl_supplier', '=', $this->supplier)
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->type && $this->qprNo) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->type && $this->supplier) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->where('qprl_supplier', '=', $this->supplier)
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->qprNo && $this->supplier) {
            $data = DB::table('qprl_master')
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->where('qprl_supplier', '=', $this->supplier)
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->from && $this->to) {
            $data = DB::table('qprl_master')
            ->whereBetween('qprl_date_issue', [$this->from, $this->to])
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->type) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->qprNo) {
            $data = DB::table('qprl_master')
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->supplier) {
            $data = DB::table('qprl_master')
            ->where('qprl_supplier', '=', $this->supplier)
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else {
            $data = DB::table('qprl_master')
            ->orderby('qprl_id', 'asc')
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->get();
        }


        $no = 1;
        $dataend = [];
        foreach($data as $da) {
            if(substr($da->qprl_date_issue,0,4) < 2014) {
                $qprl_date_issue ="";
            } else {
                $qprl_date_issue = date("d-M-Y",strtotime($da->qprl_date_issue));
            }

            if(substr($da->qprl_date_found,0,4) < 2014) {
			    $qprl_date_found ="";
            } else {
			    $qprl_date_found = date("d-M-Y",strtotime($da->qprl_date_found));
		    }

            if(substr($da->qprl_date_delivery,0,4) < 2014) {
			    $qprl_date_delivery ="";
		    } else {
			    $qprl_date_delivery = date("d-M-Y",strtotime($da->qprl_date_delivery));
		    }

            if(substr($da->qprl_date_receiving,0,4) < 2014) {
			    $qprl_date_receiving ="";
		    } else {
			    $qprl_date_receiving = date("d-M-Y",strtotime($da->qprl_date_receiving));
		    }

            if(substr($da->qprl_date_invoice,0,4) < 2014) {
			    $qprl_date_invoice ="";
            } else {
			    $qprl_date_invoice = date("d-M-Y",strtotime($da->qprl_date_invoice));
		    }

            array_push($dataend, $no++);
            array_push($dataend, $da->qprl_no);
            array_push($dataend, $da->qprl_supplier_name);
            array_push($dataend, $da->qprl_item);
            array_push($dataend, $da->qprl_part_name);
            array_push($dataend, $da->qprl_type);
            array_push($dataend, $da->qprl_type_w);
            array_push($dataend, $da->qprl_qty_ng);
            array_push($dataend, $da->qprl_qty_det);
            array_push($dataend, $da->qprl_inv_no);
            array_push($dataend, $da->qprl_lot);
            array_push($dataend, $da->qprl_informasi);
            array_push($dataend, $da->qprl_deskripsi);
            array_push($dataend, $qprl_date_issue);
            array_push($dataend, $qprl_date_found);
            array_push($dataend, $qprl_date_delivery);
            array_push($dataend, $qprl_date_receiving);
            array_push($dataend, $qprl_date_invoice);
            array_push($dataend, $da->qprl_rank);
            array_push($dataend, $da->qprl_occ);
            array_push($dataend, $da->qprl_disposition);
            array_push($dataend, $da->qprl_point);
            array_push($dataend, $da->qprl_invoice);
            array_push($dataend, $da->qprl_paid);

            if($da->qprl_rp_date !=""){
                array_push($dataend, date("d-M-Y",strtotime($da->qprl_rp_date)));
            } else {
                array_push($dataend, '-');
            }

            if($da->qprl_ver_date !=""){
                array_push($dataend, date("d-M-Y",strtotime($da->qprl_ver_date)));
            } else {
                array_push($dataend, '-');
            }
        }

        $datafinal = [];
        foreach ($data as $qwerty => $dtnd) {
            $datafinal[] = array_slice($dataend, 26 * $qwerty, 26);
        }

        $point = collect($datafinal);

        return view('excel.qpr', [
            'data' => $point
        ]);
    }

    public function styles(Worksheet $sheet) {
        if($this->type && $this->qprNo && $this->supplier && $this->from && $this->to) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->where('qprl_supplier', '=', $this->supplier)
            ->whereBetween('qprl_date_issue', [$this->from, $this->to])
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->type && $this->qprNo && $this->from && $this->to) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->whereBetween('qprl_date_issue', [$this->from, $this->to])
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->type && $this->supplier && $this->from && $this->to) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->where('qprl_supplier', '=', $this->supplier)
            ->whereBetween('qprl_date_issue', [$this->from, $this->to])
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->qprNo && $this->supplier && $this->from && $this->to) {
            $data = DB::table('qprl_master')
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->where('qprl_supplier', '=', $this->supplier)
            ->whereBetween('qprl_date_issue', [$this->from, $this->to])
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->type && $this->qprNo && $this->supplier) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->where('qprl_supplier', '=', $this->supplier)
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->type && $this->qprNo) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->type && $this->supplier) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->where('qprl_supplier', '=', $this->supplier)
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->qprNo && $this->supplier) {
            $data = DB::table('qprl_master')
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->where('qprl_supplier', '=', $this->supplier)
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->from && $this->to) {
            $data = DB::table('qprl_master')
            ->whereBetween('qprl_date_issue', [$this->from, $this->to])
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->type) {
            $data = DB::table('qprl_master')
            ->where('qprl_found_at', '=', $this->type)
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->qprNo) {
            $data = DB::table('qprl_master')
            ->where('qprl_no', 'like', '%' . $this->qprNo . '%')
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else if($this->supplier) {
            $data = DB::table('qprl_master')
            ->where('qprl_supplier', '=', $this->supplier)
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->orderby('qprl_id', 'asc')
            ->get();
        } else {
            $data = DB::table('qprl_master')
            ->orderby('qprl_id', 'asc')
            ->select('qprl_no','qprl_supplier_name','qprl_item','qprl_part_name','qprl_type','qprl_type_w','qprl_qty_ng','qprl_qty_det','qprl_inv_no','qprl_lot','qprl_informasi','qprl_deskripsi','qprl_date_issue','qprl_date_found','qprl_date_delivery','qprl_date_receiving','qprl_date_invoice','qprl_rank','qprl_occ','qprl_disposition','qprl_point','qprl_invoice','qprl_paid','qprl_rp_date','qprl_ver_date')
            ->get();
        }

        $countBorder = count($data);

        for($x = 1; $x <= $countBorder+2; $x++ ) {
            $help[$x] = array(
                'font'      => array('bold' => true),
                'borders'   => array(
                    'allBorders'    => array(
                    'borderStyle'   => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color'         => array('rgb' => '000000'),
                    ),
                    ),
                );
        }

        return $help;
    }
}
