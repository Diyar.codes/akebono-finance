<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use App\SupplieMasterSuratJalan;

use DB;

class ReportConfirmDeliverySlip implements FromCollection, WithHeadings, ShouldAutoSize, WithStyles {
    protected $moon;
    protected $year;
    protected $user;

    function __construct($moon, $year, $user) {
        $this->moon = $moon;
        $this->year = $year;
        $this->user = $user;
    }

    public function collection()
    {
        $data = DB::table('supplier_MSTR_suratJalan')
            ->join('supplier_DTL_suratJalan', 'supplier_DTL_suratJalan.no', '=', 'supplier_MSTR_suratJalan.no')
            ->select('supplier_DTL_suratJalan.no', 'tanggal', 'kendaraan', 'kode', 'namabarang', 'jumlah', 'act_qty')
            ->where('tujuan', '=', $this->user)
            ->whereMonth('tanggal', '=', $this->moon)
            ->whereYear('tanggal', '=', $this->year)
            ->get();

        $no=1;
		$dataend = [];
        foreach($data as $da) {
            array_push($dataend, $no++);

            if($da->no) {
                array_push($dataend, $da->no);
            } else {
                array_push($dataend, '-');
            }

            if($da->tanggal) {
                array_push($dataend, $da->tanggal);
            } else {
                array_push($dataend, '-');
            }

            if($da->kendaraan) {
                array_push($dataend, $da->kendaraan);
            } else {
                array_push($dataend, '-');
            }

            if($da->kode) {
                array_push($dataend, $da->kode);
            } else {
                array_push($dataend, '-');
            }

            if($da->namabarang) {
                array_push($dataend, $da->namabarang);
            } else {
                array_push($dataend, '-');
            }

            if($da->jumlah) {
                array_push($dataend, $da->jumlah);
            } else {
                array_push($dataend, "0");
            }

            if($da->act_qty) {
                array_push($dataend, $da->act_qty);
            } else {
                array_push($dataend, "0");
            }
        }

        $datafinal = [];
        foreach ($data as $qwerty => $dtnd) {
            $datafinal[] = array_slice($dataend, 8 * $qwerty, 8);
        }

        return collect($datafinal);
    }

    public function headings(): array {
        return [
            'No',
            'No Surat',
            'Tanggal',
            'kendaraan',
            'Item Number',
            'Description',
            'QTY Delivery',
            'QTY Actual',
        ];
    }

    public function styles(Worksheet $sheet) {
        $data = DB::table('supplier_MSTR_suratJalan')
        ->join('supplier_DTL_suratJalan', 'supplier_DTL_suratJalan.no', '=', 'supplier_MSTR_suratJalan.no')
        ->select('supplier_DTL_suratJalan.no', 'tanggal', 'kendaraan', 'kode', 'namabarang', 'jumlah', 'act_qty')
        ->where('tujuan', '=', $this->user)
        ->whereMonth('tanggal', '=', $this->moon)
        ->whereYear('tanggal', '=', $this->year)
        ->get();

        $countBorder = count($data);

        for($x = 1; $x <= $countBorder+1; $x++ ) {
            $help[$x] = array(
                'font'      => array('bold' => true),
                'borders'   => array(
                    'allBorders'    => array(
                    'borderStyle'   => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color'         => array('rgb' => '000000'),
                    ),
                    ),
                );
        }

        return $help;
    }
}
