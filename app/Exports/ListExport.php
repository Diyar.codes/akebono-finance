<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use DB;

class ListExport implements FromCollection,WithHeadings,WithStyles,ShouldAutoSize
{

	protected $supp_id, $ds, $de;

	function __construct($supp_id,$ds, $de) {
        $this->supp_id = $supp_id;
        $this->ds = $ds;
        $this->de = $de;
 	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection() {

        $data = DB::table('dn_mstr')
                ->join('dnd_det','dnd_det.dnd_tr_id','=','dn_mstr.dn_tr_id')
                ->leftJoin('dn_item_supp', 'dn_item_supp.kd_item', '=', 'dnd_det.dnd_part')
                ->select('dn_tr_id','dn_arrival_date','dnd_part','dnd_qty_order','pcs_kanban','dn_cycle_arrival')
                ->whereBetween('dn_mstr.dn_arrival_date',[$this->de,$this->ds])
                ->where('dn_mstr.dn_vend','=',$this->supp_id)
                ->orderBy('dn_arrival_date','ASC')
                ->get();

        $dataend = [];
        foreach($data as $dt){
            array_push($dataend, $dt->dn_tr_id);
            array_push($dataend, $dt->dn_arrival_date);
            array_push($dataend, $dt->dnd_part);
            array_push($dataend, $dt->dnd_qty_order);
            array_push($dataend, $dt->pcs_kanban);
            array_push($dataend, $dt->dn_cycle_arrival);

            getDataPtMstr($dt->dnd_part);
            $item_desc = DB::table('SOAP_pt_mstr')
            ->where('item_number',$dt->dnd_part)
            ->first();

            $Pcs_Kanban = $dt->pcs_kanban;
            if($Pcs_Kanban == ""){
                $tampung = 1;
            }else{
                $tampung = $dt->pcs_kanban;
            }

            $qty = $dt->dnd_qty_order;
            $qtyKanban = $tampung * $qty;

            if ($qty == ""){
                array_push($dataend, '0');
            }else{
                array_push($dataend, $qtyKanban);
            }
        }

        $datafinal = [];
        foreach ($data as $qwerty => $dtnd) {
            $datafinal[] = array_slice($dataend, 7 * $qwerty, 7);
        }

        return collect($datafinal);
    }

    public function headings(): array {
        return [
            'No Delivery Note',
            'Item',
            'Description',
            'Type',
            'Arrival Date',
            'Arrival Cycle',
            'QTY',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $data = DB::table('dn_mstr')
        ->join('dnd_det','dnd_det.dnd_tr_id','=','dn_mstr.dn_tr_id')
        ->leftJoin('dn_item_supp', 'dn_item_supp.kd_item', '=', 'dnd_det.dnd_part')
        ->select('dn_tr_id','dn_arrival_date','dnd_part','dnd_qty_order','pcs_kanban','dn_cycle_arrival')
        ->whereBetween('dn_mstr.dn_arrival_date',[$this->de,$this->ds])
        ->where('dn_mstr.dn_vend','=',$this->supp_id)
        ->orderBy('dn_arrival_date','ASC')
        ->get();

        $countBorder = count($data);

        for($x = 1; $x <= $countBorder+1; $x++ ) {
            $help[$x] = array(
                'font'      => array('bold' => true),
                'borders'   => array(
                    'allBorders'    => array(
                    'borderStyle'   => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color'         => array('rgb' => '000000'),
                    ),
                    ),
                );
        }

        return $help;
    }

}
