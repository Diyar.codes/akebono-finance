<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Events\AfterSheet;

use DB;

class ExcelListBon implements FromCollection, WithHeadings
// WithStyles
{

    protected $startdate, $enddate, $produksi, $area, $line;

    function __construct($startdate, $enddate, $produksi, $area, $line)
    {
        $this->startdate = $startdate;
        $this->enddate = $enddate;
        $this->produksi = $produksi;
        $this->area = $area;
        $this->line = $line;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        if ($this->startdate != '' && $this->area != 0 && $this->line != 0 && $this->enddate != '' && $this->produksi != '') {
            $data = DB::table('whs_bon as wb')->select('wb.*', 'la.area_name', 'll.line_prefix', 'lp.plant_name', 'wbb.kd_brg', 'wbb.nm_brg', 'wbb.jml_brg', 'wbb.satuan', 'wbb.ket')
                ->join('lph_plant as lp', 'wb.produksi', '=', 'lp.ID')
                ->join('lph_area as la', 'wb.area', '=', 'la.ID')
                ->join('lph_line as ll', 'wb.line', '=', 'll.ID')
                ->LeftJoin('whs_bon_brg as wbb', 'wb.kode_transaksi', '=', 'wbb.id_bon')
                ->whereBetween('wb.create_time', [$this->startdate, $this->enddate])
                ->where('wb.produksi', $this->produksi)
                ->orderBy('wb.create_time', 'DESC')
                ->get();

            $sql2 = [];
            $no = 1;
            foreach ($data as $key) {
                array_push($sql2, [
                    'no' => $no++,
                    'kode_transaksi' => $key->kode_transaksi,
                    'produksi' => $key->plant_name . ' / ' . $key->area_name . ' / ' . $key->line_prefix,
                    'nama_pengambil' => $key->nama_pengambil,
                    'tgl_ambil' => $key->tgl_ambil,
                    'kode_barang' => $key->kd_brg,
                    'nama_barang' => $key->nm_brg,
                    'satuan' => $key->satuan,
                    'jml_brg' => $key->jml_brg,
                    'ket' => $key->ket,

                ]);
            }
        } elseif ($this->area == 0 && $this->line == 0 && $this->startdate != '' && $this->produksi != '') {
            $data = DB::table('whs_bon as wb')->select('wb.*', 'la.area_name', 'll.line_prefix', 'lp.plant_name', 'wbb.kd_brg', 'wbb.nm_brg', 'wbb.jml_brg', 'wbb.satuan', 'wbb.ket')
                ->join('lph_plant as lp', 'wb.produksi', '=', 'lp.ID')
                ->join('lph_area as la', 'wb.area', '=', 'la.ID')
                ->join('lph_line as ll', 'wb.line', '=', 'll.ID')
                ->LeftJoin('whs_bon_brg as wbb', 'wb.kode_transaksi', '=', 'wbb.id_bon')
                ->whereBetween('wb.create_time', [$this->startdate, $this->enddate])
                ->where('wb.produksi', $this->produksi)
                ->where('wb.area', $this->area)
                ->where('wb.line', $this->line)
                ->orderBy('wb.create_time', 'DESC')
                ->get();

            $sql2 = [];
            $no = 1;
            foreach ($data as $key) {
                array_push($sql2, [
                    'no' => $no++,
                    'kode_transaksi' => $key->kode_transaksi,
                    'produksi' => $key->plant_name . ' / ' . $key->area_name . ' / ' . $key->line_prefix,
                    'nama_pengambil' => $key->nama_pengambil,
                    'tgl_ambil' => $key->tgl_ambil,
                    'kode_barang' => $key->kd_brg,
                    'nama_barang' => $key->nm_brg,
                    'satuan' => $key->satuan,
                    'jml_brg' => $key->jml_brg,
                    'ket' => $key->ket,

                ]);
            }
        } else {
            $data = DB::table('whs_bon as wb')->select('wb.*', 'la.area_name', 'll.line_prefix', 'lp.plant_name', 'wbb.kd_brg', 'wbb.nm_brg', 'wbb.jml_brg', 'wbb.satuan', 'wbb.ket')
                ->join('lph_plant as lp', 'wb.produksi', '=', 'lp.ID')
                ->join('lph_area as la', 'wb.area', '=', 'la.ID')
                ->join('lph_line as ll', 'wb.line', '=', 'll.ID')
                ->LeftJoin('whs_bon_brg as wbb', 'wb.kode_transaksi', '=', 'wbb.id_bon')
                ->orderBy('wb.create_time', 'DESC')
                ->get();

            $sql2 = [];
            $no = 1;
            foreach ($data as $key) {
                array_push($sql2, [
                    'no' => $no++,
                    'kode_transaksi' => $key->kode_transaksi,
                    'produksi' => $key->plant_name . ' / ' . $key->area_name . ' / ' . $key->line_prefix,
                    'nama_pengambil' => $key->nama_pengambil,
                    'tgl_ambil' => $key->tgl_ambil,
                    'kode_barang' => $key->kd_brg,
                    'nama_barang' => $key->nm_brg,
                    'satuan' => $key->satuan,
                    'jml_brg' => $key->jml_brg,
                    'ket' => $key->ket,

                ]);
            }
        }

        return collect($sql2);
    }

    public function headings(): array
    {
        return [
            'No',
            'Kode Transaksi',
            'Produksi/Area/Line',
            'Pengambil Barang',
            'Tanggal Ambil',
            'Kode Barang',
            'Nama Barang',
            'Satuan',
            'Jumlah Barang',
            'Keterangan',
        ];
    }


    // public function styles(Worksheet $sheet)
    // {

    //     $styleArray = [
    //         'borders' => [
    //             'allBorders' => [
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //                 'color' => array('rgb' => '000000')
    //             ],
    //         ],
    //     ];

    //     return [
    //         1  => ['font' => ['bold' => true]],
    //         $styleArray,
    //     ];
    // }
}
