<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use App\SupplieMasterSuratJalan;

use DB;

class ReportDeliverySlip implements FromCollection, WithHeadings, ShouldAutoSize, WithStyles
{
    protected $moon;
    protected $year;
    protected $app;
    protected $code;

    function __construct($moon, $year, $app, $code) {
        $this->moon = $moon;
        $this->year = $year;
        $this->app = $app;
        $this->code = $code;
    }

    public function collection()
    {
        $data = DB::select("SELECT tujuan, day(tanggal) as tanggal, kode, sum(act_qty) as act_qty, sum(jumlah) as jml from supplier_DTL_suratJalan a
			inner join Supplier_MSTR_suratJalan b on a.no = b.no
			where year(tanggal)='$this->year' and month(tanggal)='$this->moon' and A.no like 'SC%' and tujuan='$this->code'
			and app_supplier='$this->app'
			group by tujuan, day(tanggal), kode
			order by tujuan, kode");

        $j=0;
		$data_group = [];
		$data_group_value = [];
        foreach($data as $da) {
            $tujuan = strtoupper($da->tujuan);
            $tanggal = $da->tanggal;
            $kode = strtoupper($da->kode);
            $act_qty = $da->act_qty;
            $jml = $da->jml;

			$cur_data = $tujuan."||".$kode;

            if(!in_array($cur_data, $data_group)) {
                $data_group[$j] = $cur_data;
                $j++;
            }

            $data_group_value[$cur_data][$tanggal] = $act_qty;

            if($this->app=="Not Confirmed")
            {
                $data_group_value[$cur_data][$tanggal] = $jml;
            }
        }

        $data1 = DB::select("SELECT a.tujuan, b.kode, namabarang from Supplier_MSTR_suratJalan a
				inner join Supplier_dtl_suratJalan b on a.no = b.no
				where year(tanggal)='$this->year' and app_supplier='$this->app' and
				month(tanggal)='$this->moon' and A.no like 'SC%' and tujuan='$this->code' group by tujuan, kode, namabarang");

        // $total = 0;
		$nomer = 1;
        $dataend = [];
        foreach($data1 as $da1) {
            $supp = strtoupper($da1->tujuan);
            $kode = strtoupper($da1->kode);
            $total = 0;

            array_push($dataend, $nomer++);

            if($da1->tujuan) {
                array_push($dataend, $da1->tujuan);
            } else {
                array_push($dataend, '-');
            }

            if($da1->namabarang) {
                array_push($dataend, $da1->namabarang);
            } else {
                array_push($dataend, '-');
            }


            for ($z = 1; $z <= date('t', mktime(0, 0, 0, $this->moon, 1, $this->year)); $z++){
                if(empty($data_group_value[$supp."||".$kode][$z])) {
                    array_push($dataend, '0');
                } else {
                    array_push($dataend, $data_group_value[$supp."||".$kode][$z]);
                    $total += $data_group_value[$supp."||".$kode][$z];
                }
            }
            array_push($dataend, $total);
        }

        $tglend = [];
        $spec = date('t', mktime(0, 0, 0, $this->moon, 1, $this->year));
        $specplus = $spec + 4;


        foreach ($data1 as $qwerty => $dtnd) {
            $tglend[] = array_slice($dataend, $specplus * $qwerty, $specplus);
        }
        return collect($tglend);
    }

    public function headings(): array {
        $tgl = ['No', 'Item', 'Desc'];
        for ($z = 1; $z <= date('t', mktime(0, 0, 0, $this->moon, 1, $this->year)); $z++){
            $tgl[] = $z;
        }
        $tgl[] = 'total';
        return $tgl;
    }

    public function styles(Worksheet $sheet) {
        $data1 = DB::select("SELECT a.tujuan, b.kode, namabarang from Supplier_MSTR_suratJalan a
				inner join Supplier_dtl_suratJalan b on a.no = b.no
				where year(tanggal)='$this->year' and app_supplier='$this->app' and
				month(tanggal)='$this->moon' and A.no like 'SC%' and tujuan='$this->code' group by tujuan, kode, namabarang");

        $countBorder = count($data1);

        for($x = 1; $x <= $countBorder+1; $x++ ) {
            $help[$x] = array(
                'font'      => array('bold' => true),
                'borders'   => array(
                    'allBorders'    => array(
                    'borderStyle'   => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color'         => array('rgb' => '000000'),
                    ),
                    ),
                );
        }

        return $help;
    }
}
