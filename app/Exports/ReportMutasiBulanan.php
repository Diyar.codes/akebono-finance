<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithMapping;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use DB;
class ReportMutasiBulanan implements FromView, WithStyles, ShouldAutoSize
{
    protected $kode;
    protected $moon;
    protected $year;
    protected $locto;
    protected $app;

    function __construct($kode, $moon, $year, $locto, $app) {
        $this->kode = $kode;
        $this->moon = $moon;
        $this->year = $year;
        $this->locto = $locto;
        $this->app = $app;
    }

    public function view(): View
    {
        $number = date('t', mktime(0, 0, 0, $this->moon, 1, $this->year));
        $nomer = 1;
        $dataend = [];
        for($b=1;$b<=$number;$b++) {
            array_push($dataend, $b);

            $query = DB::select("SELECT sum(act_qty) as total_act from Supplier_MSTR_suratJalan a
                    left join supplier_DTL_suratJalan b on a.no = b.no
                    inner join supplier_item_master c on a.tujuan = c.supplier_id and b.kode=c.item_no
                    where month(tanggal)='$this->moon' and year(tanggal)='$this->year' and day(tanggal)='$b' and tujuan='$this->app' and kode ='$this->kode'");

            array_push($dataend, $this->kode);

            $tot_qty = $query[0]->total_act;

            if( $b < 10) {
                $b = 0 . $b;
            }

            if($tot_qty == null) {
                $tot_qtyy = array_push($dataend, strval(0));
            } else {
                $tot_qtyy = array_push($dataend, strval($tot_qty));
            }

            $twodigityearend = substr($this->year, -2);
            $dateend = $this->moon . '-' . $b . '-' . $twodigityearend;

            $stockin = getStockInDate($this->kode, $this->locto, $dateend);
            if($stockin == null) {
                $tot_qad = array_push($dataend, 0);
            } else {
                $tot_qad = array_push($dataend, $stockin['sumQty']);
            }

            $getQtyStockOut = getStockOutDate($this->kode, $this->locto, $dateend);
            if($getQtyStockOut == null) {
                $end = -1 * 0;
                $tot_qad_out = array_push($dataend, strval($end));
            } else {
                $end = -1 * $getQtyStockOut['sumQty'];
                $tot_qad_out = array_push($dataend, strval($end));
            }
        }

        $datafinal = [];
        $foreachN = $number - 1;
        for ($c=0;$c<=$foreachN;$c++) {
            $datafinal[] = array_slice($dataend, 5 * $c, 5);
        }

        $point = collect($datafinal);

        return view('excel.mutasi', [
            'data' => $point
        ]);
    }

    public function styles(Worksheet $sheet) {
        $number = date('t', mktime(0, 0, 0, $this->moon, 1, $this->year)) + 2;

        for($x = 1; $x <= $number+1; $x++ ) {
            $help[$x] = array(
                'font'      => array('bold' => true),
                'borders'   => array(
                    'allBorders'    => array(
                    'borderStyle'   => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color'         => array('rgb' => '000000'),
                    ),
                    ),
                );
        }

        return $help;
    }
}
