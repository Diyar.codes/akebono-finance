<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QPRSortir extends Model
{
    protected $guarded = [];

    protected $table = 'qpr_sortir';

    public $timestamps = false;
}
