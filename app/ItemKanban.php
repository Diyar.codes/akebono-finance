<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemKanban extends Model
{
    protected $table = 'dn_item_supp';

    public $timestamps = false;

    protected $guarded = [];
}
