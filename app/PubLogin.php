<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class PubLogin extends Authenticatable
{
    use Notifiable;

    protected $table = 'pub_login';

    public $timestamps = false;

    protected $guarded = [];
}
