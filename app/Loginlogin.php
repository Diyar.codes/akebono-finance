<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Loginlogin extends Authenticatable
{
    use Notifiable;

    protected $table = 'loginlogin';

    public $timestamps = false;

    protected $guarded = [];
}
