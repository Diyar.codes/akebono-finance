<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portal_MSTR extends Model
{
    protected $table = 'portal_mstr';

    public $timestamps = false;

    protected $guarded = [];
}
