<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index(){
        return view('message.create_message');
    }

    public function inboxMessage(){
        return view('message.inbox_message');

    }
}
