<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function notFound(){
        return view('status.not-found');
    }
}
