<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Collection;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use App\DeliveryNote;
use App\BusinesRelation;
use PDF;
use Illuminate\Support\Facades\Storage;
use App\Mail\MainCounterMeasureSupplier;
use App\Mail\MailDeliveryNote;
use App\SOAPPoMstr;
use App\PubDnMaster;
use App\SOAPPtMstr;
use App\DndNet;
use App\DNDHistory;
use Spipu\Html2Pdf\Html2Pdf;

class DeliveryNoteController extends Controller
{
    public function index(Request $request) {
        if ($request->ajax()) {
            if (!empty($request->filter_dn_number) && !empty($request->filter_id_supp) && !empty($request->filter_moon) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number) && !empty($request->filter_id_supp) && !empty($request->filter_moon)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number) && !empty($request->filter_id_supp) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number) && !empty($request->filter_moon) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_id_supp) && !empty($request->filter_moon) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number) && !empty($request->filter_id_supp)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number) && !empty($request->filter_moon)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_id_supp) && !empty($request->filter_moon)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_id_supp) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_moon) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if (!empty($request->filter_moon)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if (!empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_id_supp)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            }

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    $confirm = 'return confirm("are you sure?")';
                    $tampung = base64_encode($data->dn_tr_id);

                    return '
                        <td>
                            <a class="btn btn-xs btn-warning mb-1" data-toggle="tooltip" data-placement="top" title="Edit" href="/edit-delivery-notes/' . base64_encode($data->dn_tr_id) . '"><i class="fas fa-edit"></i></a>

                            <a class="btn btn-xs btn-danger mb-1" data-toggle="tooltip" data-placement="top" title="Delete" onclick="deleteDN(\''.$tampung.'\')"><font color="white"><i class="fas fa-trash"></i></font></a>
                        </td>
                    ';
                })
                ->make(true);
        }

        return view('delivery-notes.index');
    }

    public function create() {

        $listcycle = DB::table('whs_cycle_master')
                    ->select('cycle_cycle')
                    ->distinct()
                    ->orderBy('cycle_cycle', 'ASC')
                    ->get();

        return  view('delivery-notes.create', compact('listcycle'));
    }

    public function modaldeliverynotes() {
        $url  = 'https://supplier.akebono-astra.co.id/wsatdw/po_mstr.php';
        $data = po_mstr($url);

        return view('delivery-notes.modal_create_delivery_notes', compact('data'));
    }

    public function checkcycle(Request $request) {

        $cycle      = $request->get('cycle');
        $kdsupp     = $request->get('supplier');
        $orderdate  = $request->get('orderdate');
        $po         = $request->get('ponumber');
        if($orderdate == ""){
            $date_now   = date('Y-m-d');
            $date_dn    = date('Y-m-d', strtotime( $date_now . "+1 days"));
            $code_arr   = str_replace("-","",$date_dn);
        }else{
            $date_dn    = date('Y-m-d', strtotime( $orderdate . "+1 days"));
            $code_arr   = str_replace("-","",$date_dn);
        }

        $amountcycle = DB::table('whs_cycle_master')->select('cycle_kode_supplier')
                ->where('cycle_kode_supplier', $kdsupp)
                ->count();

        if ($amountcycle == 0){
            $dt     = date("m/d/Y");
            $today1 = date( "m/d/Y", strtotime( "$orderdate +1 day" ));

            $taon   = date("Y");
            $day    = date( "d", strtotime( "$today1" ));
            $bln    = date( "m", strtotime( "$today1" ));

            $time_start1 = date("H:i");

            $tgl    = date('Y-m-d',strtotime($today1));
            $dn = $kdsupp."-".$po."-".$taon.$bln.$day."-".$cycle;
            echo $time_start1."+".$cycle."+".$time_start1."+".$tgl."+".$dn."";
        }else{
            //untuk menampilkan jam
            $tampildata = collect(\DB::select("SELECT convert(char(8), cycle_start, 108) [cycle_start],cycle_batas_a,cycle_batas_b,cycle_batas_c from whs_cycle_master
            where cycle_kode_supplier ='$kdsupp' and cycle_cycle ='$cycle'"))->first();

            $time_start=$tampildata->cycle_start;

            $a=$tampildata->cycle_batas_a;
            $b=$tampildata->cycle_batas_b;
            $c=$tampildata->cycle_batas_c;

            $all = $a.":".$b.":".$c;

            $jumlah_data = $c + $cycle;


            if ($jumlah_data > $b){
                $bykdata = $jumlah_data % $b;
            }else{
                $bykdata = $jumlah_data;
            }

            if ($kdsupp == 'SI0014I' and $cycle=='2'){
                $bykdata = 2;
            }

            if ($kdsupp == 'SI0014I' and $cycle=='3'){
                $bykdata = 3;
            }
            if ($kdsupp == 'SI0014I' and $cycle=='4'){
                $bykdata = 4;
            }

            if ($kdsupp == 'SA0046I' and $cycle=='1/1'){
                $bykdata = '1/1';
            }

            if ($kdsupp == 'SA0046I' and $cycle=='2/1'){
                $bykdata = '2/1';
            }
            if ($kdsupp == 'SA0046I' and $cycle=='7/1'){
                $bykdata = '7/1';
            }

            if ($kdsupp == 'SA0046I' and $cycle=='8/1'){
                $bykdata = '8/1';
            }

            if ($kdsupp == 'SA0046I' and $cycle=='6/1'){
                $bykdata = '6/1';
            }

            //untuk menampilkan data arrival date
            $tampildata1 = collect(\DB::select("SELECT CONVERT(char(8), cycle_start, 108) [cycle_start],cycle_batas_a,cycle_batas_b,cycle_batas_c FROM whs_cycle_master WHERE cycle_kode_supplier ='".$kdsupp."' AND cycle_cycle ='".$bykdata."'"))->first();

            $time_start1 = $tampildata1->cycle_start;


            if ($time_start1 > $time_start){
                $today1     = date( "m/d/Y", strtotime( "$orderdate" ));
            }else{
                $dt         = date("m/d/Y");
                $today1     = date( "m/d/Y", strtotime( "$orderdate +1 day" ) );
            }

            $taon = date("Y");

            $day = date( "d", strtotime( "$today1" ));
            $bln = date( "m", strtotime( "$today1" ));
                //untuk penomeran DN
            $dn = $kdsupp."-".$po."-".$taon.$bln.$day."-".$bykdata;
            $tgl     = date( "Y-m-d", strtotime($today1));
            echo $time_start."+".$bykdata."+".$time_start1."+".$tgl."+".$dn."";

        }

    }

    public function seconddatadeliverynote(Request $request) {

        $poNo = $request->get('ponum');
        $kdsupp = $request->get('kdsupp');

        $data = po_detail($poNo);

        // $data =  DB::table('SOAP_po_detail')
        // ->select('dn_item_supp.*','SOAP_po_detail.*')
        // // ->leftJoin('posts', 'users.id', '=', 'posts.user_id')
        // ->leftJoin('dn_item_supp', 'dn_item_supp.kd_item', '=', 'SOAP_po_detail.item_number','AND','dn_item_supp.kd_supp','=','$kdsupp')
        // ->where('SOAP_po_detail.no_po',$poNo)
        // // ->orderBy('SOAP_po_detail.line','ASC')
        // ->get();
        $jml_row = count($data);
        $tablenya = '';
        $urut = 0;
        foreach($data as $dt){
            $urut   = $urut + 1;
            $itnum  = $dt['item_number'];
            $line   = $dt['line'];
            $item_supp = DB::table('dn_item_supp')->where('kd_item',$itnum)->where('kd_supp', $kdsupp)->first();
            if (empty($item_supp)) {
                $pcs_kanban =  0;
                $pallet_kanban = 0;
            }else{
                $pcs_kanban     =  $item_supp->pcs_kanban;
                $pallet_kanban  = $item_supp->pallet_kanban;
            }

            if ($pcs_kanban){
                $datakan = $pcs_kanban;
            }else{
                $datakan = 1;
            }

            $LPB = DB::table('elpb_dtl')
                ->where('po', $poNo)
                ->where('item_number', $itnum)
                ->where('line', $line)
                ->whereNull('rc')
                ->whereNull('qty_received')
                ->sum('qty_received');


            // Untuk Menampilkan total DN
            $totalDN    = DB::table('dnd_det')
                ->join('dn_mstr','dnd_det.dnd_tr_id', '=','dn_mstr.dn_tr_id')
                ->where('dnd_det.dnd_po_nbr', $poNo)
                ->where('dnd_part', $itnum)
                ->where('dnd_line', $line)
                ->sum('dnd_qty_order');

            // $qtySumDN = $totalDN->dnd_qty_order;

            // $sumDN = $totalDN*$datakan;

            // while ($podnbr) {
            $qty_sumDN = $totalDN*$datakan ;

        // untuk menampilkan Total SJ yang sudah dibuat oleh Supplier
            $totalSJ =  DB::table('portald_det')
                ->where('portald_po_nbr', $poNo)
                ->where('portald_part', $itnum)
                ->where('portald_line', $line)
                ->sum('portald_qty_ship');

            $qty_open_dn = $qty_sumDN - $totalSJ;
            //     // untuk menampilkan Open DN
            //     $openDN = $totalDN - $totalSJ;

            //untuk menampilkan OTY Outstanding SJ
            $qtylSJ =  DB::table('portald_det')
                ->where('portald_po_nbr', $poNo)
                ->whereNull('portald_confirm_date')
                ->where('portald_part', $itnum)
                ->where('portald_line', $line)
                ->whereDate('portald_dlv_date', '!=', Carbon::today())
                ->sum('portald_qty_ship');

            //untuk menampilkan OTY SJ TODAY
            $qtysjtoday =  DB::table('portald_det')
                ->where('portald_po_nbr', $poNo)
                ->where('portald_part', $itnum)
                ->where('portald_line', $line)
                ->whereNull('portald_confirm_date')
                ->whereDate('portald_dlv_date', Carbon::today())
                ->sum('portald_qty_ship');

            $over_po        =  ($dt['qty_po'])-($dt['qty_receive'])-($qtylSJ);
            $data_persen    = (($LPB  + $qtylSJ)/$dt['qty_po'])*100;

            if ($itnum == '51-23311-28510'){
                //$qtyOpen1 = (($qtyPO - $qtyLPB)-($qty_outstanding_sj + $qty_outstanding_Today));
                $qtyOpen1 = (($dt['qty_po'] - $LPB)-($qtylSJ + $qtysjtoday));
            }else{
                $qtyOpen1 = (($dt['qty_po'] - $LPB)-($qty_open_dn + $qtylSJ + $qtysjtoday));
            }

        $qtyOpen = $qtyOpen1/$datakan;

                $tablenya .= '<tr>
                    <td>'.$line.'</td>
                    <td>'.$dt['no_pp'].'</td>
                    <td>'.$dt['item_number'].'</td>
                    <td>'.$dt['item_deskripsi'].'</td>
                    <td>'.$dt['item_type'].'</td>
                    <td>'.$datakan.'</td>
                    <td>'.number_format($pallet_kanban).'</td>
                    <td>'.number_format($dt['qty_po']).'</td>
                    <td>'.number_format($dt['qty_receive']).'</td>
                    <td>'.number_format($totalDN).'</td>
                    <td>'.number_format($totalSJ).'</td>
                    <td>'.number_format($qty_open_dn).'</td>
                    <td>'.number_format($qtylSJ).'</td>
                    <td>'.number_format($qtysjtoday).'</td>
                    <td>'.number_format($over_po).'</td>
                    <td><input type="text" class="form-control" name="ordertb['.$urut.']" id="ordertb['.$urut.']" onchange="cekdata('.$urut.')" onkeyup="rp('.$urut.')"> </td>
                    <td>'.number_format($qtyOpen,2).'</td>
                    <td>'.number_format($data_persen,2).'</td>
                    <input type="hidden" id="qty_open['.$urut.']" name="qty_open['.$urut.']" value="'. $over_po .'">
                    <input type="hidden" id="orderasli[]" name="orderasli[]">
                    <input type="hidden" id="ppnumber[]" name="ppnumber[]" value="'. $dt['item_number'] .'">
                    <input type="hidden" id="nopp[]" name="nopp[]" value="'. $dt['no_pp'].'">
                    <input type="hidden" id="partname[]" name="partname[]" value="'.$dt['item_deskripsi'].'">
                    <input type="hidden" id="line[]" name="line[]" value="'.$line.'">
                    <input type="hidden" id="pcskanban[]" name="pcskanban[]" value="'.$datakan.'">
                    <input type="hidden" id="palletkanban[]" name="palletkanban[]" value="'.$pallet_kanban.'">
                    <input type="hidden" id="qty_po[]" name="qty_po[]" value="'.$dt['qty_po'].'">
                    <input type="hidden" id="row" name="row" value="'.$jml_row.'">

                </tr>';

                // endforeach;
        }
        return response($tablenya);
    }

    public function searchdataDN(Request $request) {

        $minDate = $request->get('minDate');
        $maxDate = $request->get('maxDate');

        $data = SOAPPoMstr::whereBetween('POdue_date',[$minDate,$maxDate])->get();

        return  view('delivery-notes.modal_create_delivery_notes', compact('data'));
    }

    public function storadeliverynote(Request $request) {

        $order          = $request->get('ordertb');
        $ppnumber       = $request->get('ppnumber');
        $partname       = $request->get('partname');
        $type           = $request->get('type');
        $pcskanban      = $request->get('pcskanban');
        $palletkanban   = $request->get('palletkanban');
        $qty_po         = $request->get('qty_po');
        $ponum          = $request->get('ponum');
        $kdsupp         = $request->get('kdsupp');
        $orderdate      = $request->get('orderdate');
        $timeorder      = $request->get('timeorder');
        $arrivaldate    = $request->get('arrivaldate');
        $timestart     = $request->get('timestart');
        $numberDN       = $request->get('numberDN');
        $ordercyle      = $request->get('ordercyle');
        $arrivalecycle  = $request->get('arrivalecycle');
        $line           = $request->get('line');
        $namesupplier   = $request->get('namesupplier');

        $row = $request->get('row');

        //untuk mencari DN agar tidak duplikat
        $cek_dn = DB::table('dn_mstr')->where('dn_tr_id',$numberDN)->get();
        if(count($cek_dn) > 0){
            $result = 'duplicated';

        }else{
            $email = DB::table('whs_email_supplier_edn')->where('supplier_code', $kdsupp)->first();

            if ($email != null) {

                $todnmstr = PubDnMaster::create([
                    'dn_tr_id'          => $numberDN,
                    'dn_po_nbr'         => $ponum,
                    'dn_user_id'        => 1,
                    'dn_arrival_date'   => $arrivaldate,
                    'dn_order_date'     => $orderdate,
                    'dn_vend'           => $kdsupp,
                    'dn__char01'        => $timeorder,
                    'dn__char02'        => $timestart
                ]);

                for ($i=0; $i < $row; $i++) {
                    if($order[$i] != ""){
                        $data = new DndNet;
                        $data->dnd_tr_id        = $numberDN;
                        $data->dnd_line         = $line[$i];
                        $data->dnd_po_nbr       = $ponum;
                        $data->dnd_part         = $ppnumber[$i];
                        $data->dnd_user_id      = 1;
                        $data->dnd_qty_order    = preg_replace("/[^0-9]/", "",$order[$i]);
                        $data->dn_cycle_arrival = $arrivalecycle;
                        $data->dn_cycle_order   = $ordercyle;
                        $data->save();

                        $data = new DNDHistory;
                        $data->h_dnd_no         = $numberDN;
                        $data->h_dnd_po         = $ponum;
                        $data->h_dnd_qty        = preg_replace("/[^0-9]/", "",$order[$i]);
                        $data->h_dnd_line       = $line[$i];
                        $data->h_dnd_item       = $ppnumber[$i];
                        $data->h_dnd_effdate    = Carbon::today();
                        $data->h_dnd_create     = 'COBA';
                        $data->h_dnd_action     = 'ADD';
                        $data->save();
                    }
                }

                $username_supplier  = $namesupplier;
                $dn                 = $numberDN;
                $address            = url("delivery-notes-cetak_pdf?username=".base64_encode($username_supplier)."&transID=".base64_encode($dn));

                $data_email = [
                    'namesupplier'  => $namesupplier,
                    'numberDN'      => $numberDN,
                    'date'          => Carbon::today(),
                    'address'       => $address
                ];


                Mail::to($email->supplier_a)->send(new MailDeliveryNote($data_email));

                $result = 'berhasil';
            }else{
                $result = 'Email empty';
            }

        }

        return response()->json($result);
    }

    public function store(Request $request)
    {
    }

    public function show(DeliveryNote $deliveryNote)
    {
    }

    public function edit($id)
    {
        $atas = DB::table('dn_mstr')
        ->join('dnd_det','dnd_det.dnd_tr_id','=','dn_mstr.dn_tr_id')
        ->join('SOAP_Pub_Business_Relation','SOAP_Pub_Business_Relation.ct_vd_addr','=','dn_mstr.dn_vend')
        ->where('dn_tr_id', base64_decode($id))
        ->first();

        $bawah = DB::table('dnd_det')
        ->leftJoin('dn_item_supp', 'dn_item_supp.kd_item', '=', 'dnd_det.dnd_part')
        ->where('dnd_det.dnd_po_nbr', $atas->dn_po_nbr)
        ->where('dnd_det.dnd_tr_id',$atas->dn_tr_id)
        ->get();
        foreach($bawah as $b){
            getDataPtMstr($b->dnd_part);
        }
        return view('delivery-notes.edit',compact('atas','bawah'));
    }

    public function delete(Request $request)
    {
        $id = $request->get('tr');
        DB::table('dn_mstr')->where('dn_tr_id', '=', base64_decode($id))->delete();
        DB::table('dnd_det')->where('dnd_tr_id', '=', base64_decode($id))->delete();
        return back();
    }

    public function update_dn(Request $request){
        $row            = $request->get('row');
        $dnd_tr_id      = $request->get('dnd_tr_id');
        $dnd_part       = $request->get('dnd_part');
        $dnd_line       = $request->get('dnd_line');
        $order          = $request->get('order');
        $dnd_qty_order  = $request->get('dnd_qty_order');

        date_default_timezone_set('Asia/Jakarta');
        $ambil_date = date('Y-m-d H:i:s');

        for($a = 0; $a < $row; $a++){
            if($order[$a] == ""){
                DB::table('dnd_det')
                ->where('dnd_tr_id', $dnd_tr_id)
                ->where('dnd_part', $dnd_part[$a])
                ->where('dnd_line', $dnd_line[$a])
                ->update(['dnd_qty_order' => $dnd_qty_order[$a]]);
            }else{
                DB::table('dnd_det')
                ->where('dnd_tr_id', $dnd_tr_id)
                ->where('dnd_part', $dnd_part[$a])
                ->where('dnd_line', $dnd_line[$a])
                ->update(['dnd_qty_order' => $order[$a]]);
            }
        }
        $data['message'] = 1;
        echo json_encode($data);
    }

    public function update(Request $request, DeliveryNote $deliveryNote)
    {
    }

    public function destroy(DeliveryNote $deliveryNote)
    {
    }

    public function summary() {
        return view('delivery-notes.summary');
    }

    public function viewforsupp() {
        return view('delivery-notes.view');
    }

    public function cetak_pdf(Request $request) {

    $username = base64_decode($request->get('username'));
    $numdn = base64_decode($request->get('transID'));

    $header = DB::table('dnd_det')
            ->join('dn_mstr', 'dnd_det.dnd_tr_id', '=', 'dn_mstr.dn_tr_id')
            ->where('dnd_tr_id', $numdn)
            ->first();

    

        if ($header) {
            $namasupplier = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr', $header->dn_vend)->first();

            if ($namasupplier) {

                $data = DB::table('dnd_det')
                        ->select('dnd_part','dnd_qty_order','dnd_po_nbr','dn_item_supp.*')
                        ->leftJoin('dn_item_supp','dn_item_supp.kd_item','=','dnd_det.dnd_part')
                        ->where('dnd_tr_id', $numdn)
                        ->get();
                    $ambil_po = po_detail($data[0]->dnd_po_nbr);
                    

                    if ($data) {
                        $tablenya = '';
                        $no =1;
                        $zz = 0;
                        foreach ($data as $key) {

                            $data1 = pt_mstr($key->dnd_part);
                            
                            foreach($ambil_po as $po){
                                if($po['item_number'] == $key->dnd_part){
                                    $tampil_um = $po['po_um'];
                                }
                            }

                                    
                            if(empty($data1['deskripsi1'])){
                                $desk1 = ' ';
                            }else{
                                $desk1 = $data1['deskripsi1'];
                            }
                            
                            if(empty($data1['deskripsi2'])){
                                $desk2 = ' ';
                            }else{
                                $desk2 = $data1['deskripsi2'];
                            }

                            
                            $tablenya .= '<tr>
                                            <td class="text-center" style="font-size:9pt"> '.$no++.'</td>
                                            <td class="text-center" style="font-size:9pt">'.$key->dnd_po_nbr.'</td>
                                            <td class="text-center" style="font-size:9pt">'.$desk1.''.$desk2.'</td>
                                            <td class="text-center" style="font-size:9pt">'.$data1['desc_type'].'</td>
                                            <td class="text-center" style="font-size:9pt">'.$tampil_um.'</td>
                                            <td class="text-center" style="font-size:9pt">'.$key->back_no.'</td>
                                            <td class="text-center" style="font-size:9pt"></td>
                                            <td class="text-center" style="font-size:9pt">'.$key->pallet_kanban.'</td>
                                            <td class="text-center" style="font-size:9pt">'.$key->pcs_kanban.'</td>
                                            <td class="text-center" style="font-size:9pt">'.$key->qty.'</td>
                                            <td class="text-center" style="font-size:9pt"></td>
                                            <td class="text-center" style="font-size:9pt"></td>
                                        </tr>';
                        }

                        // dd($ambil_po);
                        //             die;

                        $header = [
                            'orderdate'     => $header->dn_order_date,
                            'arrivaldate'   => $header->dn_arrival_date,
                            'ordercyle'     => $header->dn_cycle_order,
                            'arrivalecycle' => $header->dn_cycle_arrival,
                            'ponum'         => $header->dnd_po_nbr,
                            'numberDN'      => $header->dnd_tr_id,
                            'tablenya'      => $tablenya,
                            'namasupplier'  => $namasupplier->ct_ad_name,
                            'alamat'        => $namasupplier->ct_ad_line1,
                            'alamat2'       => $namasupplier->ct_ad_line2,
                        ];

                        // ob_end_clean();
                        $filename = "DN | ".$numdn.".pdf";
                        $tgl      = date('dMY');
                        $content  = ob_get_clean();
                        $content  = '<page>' . $content . '</page>';
                        try {
                            $html2pdf = new Html2Pdf('L', 'A4', 'en', false, 'ISO-8859-15', array(12, 15, 0, 0));
                            // $html2pdf->SetMargins(1,1,1);
                            $html2pdf->pdf->SetTitle($filename . ' - Akebono Supplier');
                            $html2pdf->setDefaultFont('Arial', 5);
                            //$html2pdf->Footer('This Document printed by '.$_SESSION['username'].' by supplier.akebono-astra.co.id at '.$tgl.'');
                            $html2pdf->writeHTML(view('delivery-notes.pdf_delivery_note', compact('header')));
                            $html2pdf->Output($filename);
                        } catch (HTML2PDF_exception $e) {
                            echo $e;
                        }

                        // $pdf = PDF::loadview('delivery-notes.pdf_delivery_note', $header);

                        // $filename = 'delivery-notes';
                        // Storage::put('public/pdf/' . $filename . '.pdf', $pdf->output());
                        // $getpdf = $pdf->download($filename . '.pdf');

                        // return $getpdf;
                    }

            }
        }else{
            $message = 'Tidak Ada Data!';
            return response($message,404);
        }

    }



    public function searchdatalookup(Request $request) {
        $url = "https://supplier.akebono-astra.co.id/wsatdw/po_mstr.php";
        getDataPO($url);

        return response('berhasil');
    }
}
