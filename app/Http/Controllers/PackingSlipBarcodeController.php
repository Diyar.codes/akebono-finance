<?php

namespace App\Http\Controllers;

use App\Portal_MSTR;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Yajra\DataTables\Facades\DataTables;

class PackingSlipBarcodeController extends Controller
{
    public function index(Request $request)
    {
        $hariini = date("Y-m-d");
        $portal_dlv_date = date("Y-m-d", strtotime($hariini . " + 1 days"));
        $kode_supplier = Auth::user()->kode_supplier;
        $previllage = Auth::user()->previllage;
        if ($previllage == 'Supplier') {
            $packingslipbarcode = Portal_MSTR::select('portal_tr_id', 'portal_po_nbr', 'portal_ps_nbr', 'portal_vend', 'portal_dlv_date', 'portal_cycle', 'portal_time_delivery', 'app_dn_qc', 'printsj_status', 'app_dn_qc_date', 'kode_supplier')
                ->join('pub_login', 'pub_login.kode_supplier', '=', 'portal_mstr.portal_vend')
                ->where('portal_tr_id', '!=', '')
                ->where('supp_fexqms', '=', 1)
                ->where('portal_vend', $kode_supplier)
                ->where('portal_dlv_date', '<=', $portal_dlv_date)
                ->orderBy('portal_dlv_date', 'DESC')
                ->get();
        } elseif ($previllage == 'Admin' || $previllage == 'QC') {
            $packingslipbarcode = Portal_MSTR::select('portal_tr_id', 'portal_po_nbr', 'portal_ps_nbr', 'portal_vend', 'portal_dlv_date', 'portal_cycle', 'portal_time_delivery', 'app_dn_qc', 'printsj_status', 'app_dn_qc_date', 'kode_supplier')
                ->join('pub_login', 'pub_login.kode_supplier', '=', 'portal_mstr.portal_vend')
                ->where('portal_tr_id', '!=', '')
                ->where('supp_fexqms', '=', 1)
                ->where('portal_dlv_date', '<=', $portal_dlv_date)
                ->orderBy('portal_dlv_date', 'DESC')
                ->get();
        }
        return view('packing-slip.barcode', compact('packingslipbarcode'));
    }

    public function printbarcode($transaction_id)
    {
        $decode = base64_decode($transaction_id);
        $packingslipbarcode = Portal_MSTR::select('portal_tr_id', 'portal_po_nbr', 'portal_ps_nbr', 'portal_vend', 'portal_dlv_date', 'portal_cycle', 'portal_time_delivery', 'app_dn_qc', 'printsj_status', 'app_dn_qc_date', 'kode_supplier')
            ->join('pub_login', 'pub_login.kode_supplier', '=', 'portal_mstr.portal_vend')
            ->where('portal_tr_id', '=', $decode)
            ->orderBy('portal_dlv_date', 'DESC')
            ->first();

        $data_dn = DB::table('portald_det')
            ->select('portald_no_dn')
            ->join('portal_mstr', 'portald_det.portald_tr_id', '=', 'portal_mstr.portal_tr_id')
            ->where('portald_tr_id', $decode)
            ->first();

        $datadelivery = DB::table('portald_det')
            ->select('portald_line', 'portald_part', 'portald_qty_ship')
            ->join('portal_mstr', 'portald_det.portald_tr_id', '=', 'portal_mstr.portal_tr_id')
            ->where('portald_tr_id', $decode)
            ->get();

        return view('packing-slip.barcode-generator', compact('packingslipbarcode', 'data_dn', 'datadelivery'));
    }

    public function barcodeview($transaction_id)
    {
        $decode = base64_decode($transaction_id);
        // GET DATA DN
        $data_dn = DB::table('portald_det')
            ->select('portald_tr_id', 'portald_no_dn', 'portald_po_nbr', 'portald_dlv_date', 'portal_ps_nbr')
            ->join('portal_mstr', 'portald_det.portald_tr_id', '=', 'portal_mstr.portal_tr_id')
            ->where('portald_tr_id', $decode)
            ->first();

        // DETAIL
        $detail = DB::table('portald_det')
            ->select('portald_tr_id', 'portald_part', 'portald_qty_ship', 'portald_po_nbr', 'portald_line', 'portald_um', 'portald__int07', 'portal_vend', 'portald_confirm_rcvd', 'portald__date01', 'portald_inpection_id', 'portald_insp_judg', 'portald_app_insp_note', 'portald_rcv_id', 'portald_rcv_judge', 'portald_app_insp')
            ->join('portal_mstr', 'portald_det.portald_tr_id', '=', 'portal_mstr.portal_tr_id')
            ->where('portald_tr_id', $decode)
            ->get();

        //untuk cek status item QC
        $kode_supplier = substr($decode, 0, 7);
        $cek_data_item = DB::table('supplier_check_qc')->where('supplier_kode_check', $kode_supplier)->count();
        if ($cek_data_item == 0) {
            $ket_check = 'Non Check';
        } else {
            $ket_check = 'Check';
        }
        return view('packing-slip.barcode-view', compact('data_dn', 'detail', 'ket_check'));
    }
}
