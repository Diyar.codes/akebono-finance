<?php

namespace App\Http\Controllers;

use App\Mail\sendEmail;
use App\Mail\TestEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendEmailController extends Controller
{
    public function index(){
        Mail::to('soffatur26@gmail.com')->send(new TestEmail());

        return 'Email Telah Dikirim';

    }
}
