<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login() {
        return view('auth.login');
    }

    public function doLogin(Request $request)
    {
        $request->validate([
            'username'  => 'required|string|max:255',
            'password'  => 'required|string'
        ]);

        $credentials = $request->only('username', 'password');

        $attemp = Auth::guard('pub_login')->attempt($credentials);

        $user = Auth::guard('pub_login')->user();

        if ($user) {
            $user->update([
                'tgl_akses' => date('Y-m-d H:i:s')
            ]);
        }

        if ($attemp) {
            return redirect()->route('dashboard');
        } else {
            return redirect()->route('login')->with('error', 'Email atau Password anda salah, silahkan coba lagi !');
        }
    }

    public function doLogout()
    {
        $user = Auth::guard('pub_login')->user();

        if ($user) {
            $user->update([
                'effdate' => date('Y-m-d H:i:s')
            ]);
        }


        Auth::guard('pub_login')->logout();

        return redirect()->route('login');
    }
}
