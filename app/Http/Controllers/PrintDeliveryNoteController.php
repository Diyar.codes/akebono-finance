<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Exports\ListExport;
use App\Exports\SummaryDeliveryNote;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;


class PrintDeliveryNoteController extends Controller
{
    public function index(Request $request) {
        if($request->ajax()){
            // $url = "https://supplier.akebono-astra.co.id/wsatdw/master_view_test.php";
            // get_supplier($url);
            $dn     = $request->cari;
            $mod    = $request->modul;
            $arr    = $request->arr;
            if($mod == "arrival"){
                $cari_dn = DB::table('dn_mstr')->select('dn_tr_id','dn_po_nbr','dn_order_date','dn_vend')
                ->join('SOAP_Pub_Business_Relation','dn_mstr.dn_vend','=','SOAP_Pub_Business_Relation.ct_vd_addr')
                ->select('*')
                ->where('dn_arrival_date',$arr)
                ->get();
            }else if($dn != ""){
                $cari_dn = DB::table('dn_mstr')->select('dn_tr_id','dn_po_nbr','dn_order_date','dn_vend')
                ->join('SOAP_Pub_Business_Relation','dn_mstr.dn_vend','=','SOAP_Pub_Business_Relation.ct_vd_addr')
                ->select('*')
                ->where('dn_tr_id',$dn)
                ->get();
            }else{
                $cari_dn = DB::table('dn_mstr')->select('dn_tr_id','dn_po_nbr','dn_order_date','dn_vend')
                ->join('SOAP_Pub_Business_Relation','dn_mstr.dn_vend','=','SOAP_Pub_Business_Relation.ct_vd_addr')
                ->select('*')
                ->orderBy('dn_order_date', 'desc')
                ->limit(10)
                ->get();
            }

            $tablenya = '';
            if(count($cari_dn) > 0){
                foreach($cari_dn as $dt){
                    $tablenya .= '
                    <tr>
                        <td><a href="/print-delivery-notes/'.base64_encode($dt->dn_tr_id).'" target="_blank">'.$dt->dn_tr_id.'</a></td>
                        <td>'.$dt->ct_vd_addr.'</td>
                        <td>'.$dt->dn_po_nbr.'</td>
                        <td>'.date('d-m-Y',strtotime($dt->dn_order_date)).'</td>
                    </tr>';

                } //end foreach
            }else{
                $tablenya .= '
                    <tr>
                        <td colspan="4" align="center">No data available in table</td>
                    </tr>';
            }
            return response($tablenya);
            // echo json_encode($data);
        }else{
            $s = '3';
            $data = DB::table('dn_mstr')->select('dn_tr_id','dn_po_nbr','dn_order_date','dn_vend')
            ->join('SOAP_Pub_Business_Relation','dn_mstr.dn_vend','=','SOAP_Pub_Business_Relation.ct_vd_addr')
            ->select('*')
            ->orderBy('dn_order_date', 'desc')
            ->limit(10)
            ->get();
            // $cari_supp = DB::table('pub_business_relation')->select('BusinessRelationName1')->where('BusinessRelationCode', $s)->first();

            return view('delivery-notes.report',compact('data'));
        }
    }

    public function cetak_langsung(Request $request){
        $dn         = $request->get('cari');
        $cari_dn    = DB::table('dn_mstr')->select('dn_tr_id','dn_po_nbr','dn_order_date','dn_vend')
                    ->join('SOAP_Pub_Business_Relation','dn_mstr.dn_vend','=','SOAP_Pub_Business_Relation.ct_vd_addr')
                    ->select('*')
                    ->where('dn_tr_id',$dn)
                    ->get();

        if(count($cari_dn) > 0){
            $data['message']    = 1;
            $data['dn']         = base64_encode($cari_dn[0]->dn_tr_id);
        }else{
            $data['message']    = 0;
        }
        echo json_encode($data);
    }

    public function report_dn_print($id){
        $cari_no = DB::table('dn_mstr')->select('*')
            ->join('SOAP_Pub_Business_Relation','dn_mstr.dn_vend','=','SOAP_Pub_Business_Relation.ct_vd_addr')
            ->join('dnd_det','dnd_det.dnd_tr_id','=','dn_mstr.dn_tr_id')
            ->select('*')
            ->where('dn_tr_id',base64_decode($id))
            ->first();
        getPODetail($cari_no->dn_po_nbr);
        $cari_dn = DB::table('dn_mstr')->select('dn_tr_id','dn_po_nbr','dn_order_date','dn_vend')
            ->join('SOAP_Pub_Business_Relation','dn_mstr.dn_vend','=','SOAP_Pub_Business_Relation.ct_vd_addr')
            ->join('dnd_det','dnd_det.dnd_tr_id','=','dn_mstr.dn_tr_id')
            ->leftJoin('dn_item_supp', 'dn_item_supp.kd_item', '=', 'dnd_det.dnd_part')
            ->select('*')
            ->where('dn_tr_id',base64_decode($id))
            ->get();
        $data_supplier = DB::table('SOAP_Pub_Business_Relation')
        ->select('*')
        ->where('ct_vd_addr','=',$cari_no->dn_vend)
        ->first();
        return view('delivery-notes.print',compact('cari_dn','data_supplier','cari_no'));
    }

    public function cari_dn_number(Request $request){

        // $data = DB::table('dn_mstr')->select('dn_tr_id','dn_po_nbr','dn_order_date','dn_vend')
        // ->join('pub__business_relation','dn_mstr.dn_vend','=','pub__business_relation.BusinessRelationCode')
        // ->select('*')
        // ->get();
        if($request->ajax()){
            $data = $request->cari;
            echo json_encode($data);
        }else{
            echo "Access Denied";
        }
    }

    public function list_dn(Request $request){
        if($request->ajax()){
            $supp_id = $request->get('supp_id');
            $ds = $request->get('ds');
            $de = $request->get('de');
            if($ds != "" && $de != ""){
                $data = DB::table('dn_mstr')
                ->join('dnd_det','dnd_det.dnd_tr_id','=','dn_mstr.dn_tr_id')
                ->leftJoin('dn_item_supp', 'dn_item_supp.kd_item', '=', 'dnd_det.dnd_part')
                ->select('dn_tr_id','dn_arrival_date','dnd_part','dnd_qty_order','pcs_kanban','dn_cycle_arrival')
                ->whereBetween('dn_mstr.dn_arrival_date',[$ds,$de])
                ->where('dn_mstr.dn_vend','=',$supp_id)
                ->orderBy('dn_arrival_date','ASC')
                ->get();
            }else if($ds == "" && $de =""){
                $data = DB::table('dn_mstr')
                ->join('dnd_det','dnd_det.dnd_tr_id','=','dn_mstr.dn_tr_id')
                ->select('dn_tr_id','dn_arrival_date','dnd_part','dnd_qty_order')
                ->where('dn_mstr.dn_vend','=',$supp_id)
                ->orderBy('dn_arrival_date','ASC')
                ->get();
            }else{
                $data = 0;
            }

            $tablenya = '';
            $urut = 1;
            foreach($data as $dt){

                getDataPtMstr($dt->dnd_part);
                $item_desc = DB::table('SOAP_pt_mstr')
                ->where('item_number',$dt->dnd_part)
                ->first();

                // $item_supp = DB::table('dn_item_supp')
                // ->select('pcs_kanban')
                // ->where('kd_item',$dt->dnd_part)
                // ->where('kd_supp',$supp_id)
                // ->first();

                $Pcs_Kanban = $dt->pcs_kanban;
                if($Pcs_Kanban == ""){
                    $tampung = 1;
                }else{
                    $tampung = $dt->pcs_kanban;
                }

                $qty = $dt->dnd_qty_order;
                $qtyKanban = $tampung * $qty;

                if ($qty == ""){
                    $isi = 0;
                }else{
                    $isi = number_format($qtyKanban);
                }

                $tablenya .= '
                <tr>
                    <td>'.$urut++.'</td>
                    <td>'.$dt->dn_tr_id.'</td>
                    <td>'.$dt->dnd_part.'</td>
                    <td>'.$item_desc->deskripsi1.'</td>
                    <td>'.$item_desc->desc_type.'</td>
                    <td>'.date('d-m-Y',strtotime($dt->dn_arrival_date)).'</td>
                    <td>'.$dt->dn_cycle_arrival.'</td>
                    <td style="width:100px;" align="center">'.$isi.'</td>
                </tr>';


            } //end foreach

            if(count($data) > 0){
                $data['message'] = 1;
            }else{
                $data['message'] = 0;
            }


            return response($tablenya);
        }else{
            $supplier = DB::table('pub_business_relation')->get();
            return view('delivery-notes.list',compact('supplier'));
        }
    }

    public function dn_vs_pack(Request $request){
        if($request->ajax()){
            $dn     = $request->dn;
            $po     = $request->po;
            $item   = $request->item;

            if($dn != ""){
                $ambil_dulu = DB::table('dn_mstr')->where('dn_tr_id',$dn)->first();
                $ambil_det = DB::table('dnd_det')->where('dnd_tr_id',$ambil_dulu->dn_tr_id)->get();
                // foreach($ambil_det as $ad){
                    $data[0] = (object) array(
                        'dn_tr_id'          => $ambil_dulu->dn_tr_id,
                        'dn_po_nbr'         => $ambil_det[0]->dnd_po_nbr,
                        'dn_order_date'     => $ambil_dulu->dn_order_date,
                        'dn_arrival_date'   => $ambil_dulu->dn_arrival_date,
                        'dn_cycle_order'    => $ambil_det[0]->dn_cycle_order,
                        'dn_cycle_arrival'  => $ambil_det[0]->dn_cycle_arrival,
                    );
                // }

            }else if($po != ""){
                $data = DB::table('dn_mstr')

                ->join('dnd_det','dnd_det.dnd_tr_id','=','dn_mstr.dn_tr_id')
                ->where('dn_mstr.dn_po_nbr','=',$po)
                // ->groupBy('dn_mstr.dn_tr_id')
                ->get();
            }else if($item != ""){
                $data = DB::table('dn_mstr')
                ->join('dnd_det','dnd_det.dnd_tr_id','=','dn_mstr.dn_tr_id')
                ->where('dnd_det.dnd_part','=',$item)
                // ->groupBy('dn_mstr.dn_tr_id')
                ->get();
            }else{
                $data = 0;
            }
            $tablenya = '';
            if(count($data) > 0){
                $urut = 1;
                foreach($data as $dt){

                    $tablenya .= '
                    <tr>
                        <td>'.$urut++.'</td>
                        <td>'.$dt->dn_tr_id.'</td>
                        <td>'.$dt->dn_po_nbr.'</td>
                        <td>'.date('d-m-Y',strtotime($dt->dn_order_date)).'</td>
                        <td>'.date('d-m-Y',strtotime($dt->dn_arrival_date)).'</td>
                        <td>'.$dt->dn_cycle_order.'</td>
                        <td>'.$dt->dn_cycle_arrival.'</td>
                        <td><a href="/detail-view-dn/'. base64_encode($dt->dn_tr_id) .'" class="btn btn-dark"><i class="far fa-eye"></i> Detail</a></td>
                    </tr>';

                } //end foreach
            }else{
                $tablenya .= '';
            }
            return response($tablenya);
        }else{
            return view('delivery-notes.dn_vs_pack');
        }
    }

    public function detail_view_dn($id){
        $cari_ps = DB::table('portald_det')
                ->select('portald_tr_id')
                ->where('portald_no_dn', base64_decode($id))
                ->first();

        $tablenya = '';
        if(empty($cari_ps)){
            $ps_no = "-";
            $tablenya .= '';
        }else{
            $ps_no = $cari_ps->portald_tr_id;

            $cari_supp = DB::table('portal_mstr')
            ->select('portal_vend')
            ->where('portal_tr_id', $ps_no)
            ->first();

            $data = DB::table('portald_det')
                ->select('portald_line', 'portald_part' , 'portald_qty_ship')
                ->where('portald_tr_id',$cari_ps->portald_tr_id)
                ->get();
            $urut = 1;
            foreach($data as $dt){
                $line       = $dt->portald_line;
                $part       = $dt->portald_part;
                $qty_ship   = $dt->portald_qty_ship;

                $tampil_qty_dn = DB::table('dnd_det')
                ->select('dnd_qty_order')
                ->where('dnd_tr_id',base64_decode($id))
                ->where('dnd_part',$part)
                ->where('dnd_line',$line)
                ->first();

                $qty_dn = $tampil_qty_dn->dnd_qty_order;

                $item_supp = DB::table('dn_item_supp')
                ->select('back_no','pcs_kanban','pallet_kanban')
                ->where('kd_item',$part)
                ->where('kd_supp',$cari_supp->portal_vend)
                ->first();
                $pt_mstr = pt_mstr($part);
                $qty_kanban = $item_supp->pcs_kanban;
                if ($qty_kanban == ''){
                    $qty_DN_pcs = ($qty_dn*1);
                }else{
                    $qty_DN_pcs=($qty_kanban*$qty_dn);
                }

                $tablenya .= '
                <tr>
                    <td>'.$urut++.'</td>
                    <td>'.$line.'</td>
                    <td>'.$part.'</td>
                    <td>'.$pt_mstr['deskripsi1'].' - '.$pt_mstr['deskripsi2'].'</td>
                    <td>'.$pt_mstr['um'].'</td>
                    <td>'.number_format($qty_DN_pcs).'</td>
                    <td>'.number_format($qty_ship).'</td>
                </tr>';

            } //end foreach
        }
        return view('delivery-notes.view_detail_dn',compact('tablenya','ps_no'));
    }

    public function summary_dn(Request $request){
        return view('delivery-notes.summary');
    }

    public function searchSummaryDn(Request $request) {
        $code = $request->get('code');
        $app = $request->get('app');
        $moon = $request->get('moon');
        $year = $request->get('year');

        if($code != '' && $moon != '' && $year != '' && $app != '') {
            $query = DB::table('dn_mstr')
                ->join('dnd_det', 'dnd_det.dnd_tr_id', '=', 'dn_mstr.dn_tr_id')
                ->leftJoin('dn_item_supp', 'dn_item_supp.kd_item', '=', 'dnd_det.dnd_part')
                ->select('dn_tr_id', 'dn_arrival_date', 'dnd_part', 'dn_vend', 'pcs_kanban')
                ->where('dn_vend', '=', $code)
                ->whereMonth('dn_arrival_date', $moon)
                ->whereYear('dn_arrival_date', $year)
                ->where('dn_user_id', '=', $app)
                ->orderBy('dn_arrival_date', 'ASC')
                ->get();

            $j = 0;
            $data_group = [];
            $data_group_value = [];
            foreach($query as $da) {
                $part = $da->dnd_part;
                $kode = $da->dn_tr_id;
                $tanggal = $da->dn_arrival_date;
                $day = \Carbon\Carbon::parse($da->dn_arrival_date)->format('d');

                if($day < 10) {
                    $endDay = explode(0,$day);
                    $makeDay = $endDay[1];
                } else {
                    $endDay = $day;
                    $makeDay = $endDay;
                }

                if($da->pcs_kanban == "") {
                    $ambil_kanban = 1;
                } else {
                    $ambil_kanban = $da->pcs_kanban;
                }

                $get_data_qty = DB::table('dn_mstr')
                                ->join('dnd_det', 'dnd_det.dnd_tr_id', '=', 'dn_mstr.dn_tr_id')
                                ->select('dnd_qty_order')
                                ->where('dn_vend', '=', $da->dn_vend)
                                ->where('dn_tr_id', '=', $da->dn_tr_id)
                                ->where('dnd_part', '=', $da->dnd_part)
                                ->where('dn_arrival_date', '=', $tanggal)
                                ->orderBy('dn_arrival_date', 'ASC')
                                ->first();

                $qtyKanban = $ambil_kanban * $get_data_qty->dnd_qty_order;

                $cur_data = $part."||".$kode;

                if(!in_array($cur_data, $data_group)) {
                    $data_group[$j] = $cur_data;
                    $j++;
                }

                $data_group_value[$cur_data][$makeDay] = $qtyKanban;
            }
            $table_atas = '';
            $table_bawah = '';

            $table_atas = '
                <tr class="backgroudrowblue">
                    <th class="text-white text-wrap tablefontmini2" style="font-size:9px">No Delivery Note</th>
                    <th class="text-white text-wrap tablefontmini2" style="font-size:9px">Item</th>';

                    for ($x = 1; $x <=  date('t', mktime(0, 0, 0, $moon, 1, $year)); $x++){
                        $table_atas .=  "<th align='center' class='text-white text-wrap tablefontmini2' style='font-size:9px'>".$x."</th>";
                    }
            $table_atas .= '</tr>';

            foreach($query as $da1) {
                $part = $da1->dnd_part;
                $kode = $da1->dn_tr_id;
                $table_bawah .=
                    '<tr>
                        <td class="text-wrap tablefontmini2" style="max-width:100px; padding: 3px">'.$da1->dn_tr_id.'</td>
                        <td class="text-wrap tablefontmini2" style="max-width:100px; padding: 3px">'.$da1->dnd_part.'</td>';


                    for ($z = 1; $z <= date('t', mktime(0, 0, 0, $moon, 1, $year)); $z++){
                        if (empty($data_group_value[$part."||".$kode][$z])) {
                            $table_bawah .= "<td align='center' style='max-width:100px; padding: 3px' class='text-wrap tablefontmini2'>0</td>";
                        } else {
                            $table_bawah .= "<td align='center' style='max-width:100px; padding: 3px' class='text-wrap tablefontmini2'>".$data_group_value[$part."||".$kode][$z]."</td>";
                        }
                    }

                $table_bawah .= '</tr>';
            }
        } else if($code != '' && $moon != '' && $year != '') {
            $query = DB::table('dn_mstr')
                ->join('dnd_det', 'dnd_det.dnd_tr_id', '=', 'dn_mstr.dn_tr_id')
                ->leftJoin('dn_item_supp', 'dn_item_supp.kd_item', '=', 'dnd_det.dnd_part')
                ->select('dn_tr_id', 'dn_arrival_date', 'dnd_part', 'dn_vend', 'pcs_kanban')
                ->where('dn_vend', '=', $code)
                ->whereMonth('dn_arrival_date', $moon)
                ->whereYear('dn_arrival_date', $year)
                ->orderBy('dn_arrival_date', 'ASC')
                ->get();

            $j = 0;
            $data_group = [];
            $data_group_value = [];
            foreach($query as $da) {
                $part = $da->dnd_part;
                $kode = $da->dn_tr_id;
                $tanggal = $da->dn_arrival_date;
                $day = \Carbon\Carbon::parse($da->dn_arrival_date)->format('d');

                if($day < 10) {
                    $endDay = explode(0,$day);
                    $makeDay = $endDay[1];
                } else {
                    $endDay = $day;
                    $makeDay = $endDay;
                }

                if($da->pcs_kanban == "") {
                    $ambil_kanban = 1;
                } else {
                    $ambil_kanban = $da->pcs_kanban;
                }

                $get_data_qty = DB::table('dn_mstr')
                                ->join('dnd_det', 'dnd_det.dnd_tr_id', '=', 'dn_mstr.dn_tr_id')
                                ->select('dnd_qty_order')
                                ->where('dn_vend', '=', $da->dn_vend)
                                ->where('dn_tr_id', '=', $da->dn_tr_id)
                                ->where('dnd_part', '=', $da->dnd_part)
                                ->where('dn_arrival_date', '=', $tanggal)
                                ->orderBy('dn_arrival_date', 'ASC')
                                ->first();

                $qtyKanban = $ambil_kanban * $get_data_qty->dnd_qty_order;

                $cur_data = $part."||".$kode;

                if(!in_array($cur_data, $data_group)) {
                    $data_group[$j] = $cur_data;
                    $j++;
                }

                $data_group_value[$cur_data][$makeDay] = $qtyKanban;
            }
            $table_atas = '';
            $table_bawah = '';

            $table_atas = '
                <tr class="backgroudrowblue">
                    <th class="text-white text-wrap tablefontmini2" style="font-size:9px">No Delivery Note</th>
                    <th class="text-white text-wrap tablefontmini2" style="font-size:9px">Item</th>';

                    for ($x = 1; $x <=  date('t', mktime(0, 0, 0, $moon, 1, $year)); $x++){
                        $table_atas .=  "<th align='center' class='text-white text-wrap tablefontmini2' style='font-size:9px'>".$x."</th>";
                    }
            $table_atas .= '</tr>';

            foreach($query as $da1) {
                $part = $da1->dnd_part;
                $kode = $da1->dn_tr_id;
                $table_bawah .=
                    '<tr>
                        <td class="text-wrap tablefontmini2" style="max-width:100px; padding: 3px">'.$da1->dn_tr_id.'</td>
                        <td class="text-wrap tablefontmini2" style="max-width:100px; padding: 3px">'.$da1->dnd_part.'</td>';


                    for ($z = 1; $z <= date('t', mktime(0, 0, 0, $moon, 1, $year)); $z++){
                        if (empty($data_group_value[$part."||".$kode][$z])) {
                            $table_bawah .= "<td align='center' style='max-width:100px; padding: 3px' class='text-wrap tablefontmini2'>0</td>";
                        } else {
                            $table_bawah .= "<td align='center' style='max-width:100px; padding: 3px' class='text-wrap tablefontmini2'>".$data_group_value[$part."||".$kode][$z]."</td>";
                        }
                    }

                $table_bawah .= '</tr>';
            }
        } else {
            $table_atas = '';
            $table_bawah = '';

            $table_atas = '
                <tr class="backgroudrowblue">
                    <th class="text-white text-wrap tablefontmini2" style="font-size:9px">No Delivery Note</th>
                    <th class="text-white text-wrap tablefontmini2" style="font-size:9px">Item</th>';

                    for ($x = 1; $x <=  date('t', mktime(0, 0, 0, '12', 1, '2021')); $x++){
                        $table_atas .=  "<th align='center' class='text-white text-wrap tablefontmini2' style='font-size:9px'>".$x."</th>";
                    }
            $table_atas .= '</tr>';
        }


        $data = [
            'atas' => $table_atas,
            'bawah' => $table_bawah,
        ];

        return response($data);
    }

    public function export_summary_dn(Request $request)
    {
        $code = $request->get('codeExcel');
        $moon = $request->get('moonExcel');
        $year = $request->get('yearExcel');
        $app = $request->get('appExcel');

        return Excel::download(new SummaryDeliveryNote($code, $moon, $year, $app), 'summary_delivery_note.xlsx');
    }

    public function export_list_dn(Request $request)
    {
        $supp_id = $request->supplierID;
        $ds = $request->dateend;
        $de = $request->datestart;

        return Excel::download(new ListExport($supp_id, $ds, $de), 'list-delivery.xlsx');

    }
}
