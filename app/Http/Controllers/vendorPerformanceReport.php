<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class vendorPerformanceReport extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $supp        = Auth::guard('pub_login')->user()->kode_supplier;         
        $bulan       = 06;
        $tahun       = 2021;
        $sql_cek_mfg = DB::select("SELECT distinct dnd_part from dn_mstr,dnd_det 
                    WHERE dn_vend = '$supp' AND month(dn_arrival_date)='$bulan' AND year(dn_arrival_date)='$tahun'
                    AND dn_tr_id = dnd_tr_id ORDER BY dnd_part ASC");
        $first_day   = date('Y-m-d',strtotime($tahun.'-'.$bulan.'-'.'01')); 
        $last_day    = date('Y-m-t',strtotime($first_day));
        $jumHari     = date('t',strtotime($first_day));
        $l= 0;
        foreach($sql_cek_mfg as $ambilpo){
            // $prh = prh_hist($ambilpo->dnd_po_nbr)
            $tampung_prh = get_data_kedatangan($ambilpo->dnd_part,$first_day,$last_day,$supp);
            
        }
        dd($tampung_prh);
        die;
        
        $phitung=0;
        foreach($prh as $p){
            if($p['prhRcp_date'] >= $first_day && $p['prhRcp_date'] <= $last_day){
                $data_prh[$phitung++] = $p['prhRcvd'];
            }
        }
                
        $tablenya = '';
        if(count($sql_cek_mfg) > 0){
            //untuk cek apakah data sudah pernah di save atau blom
            $cek_data_simpan =  DB::table('vpr_ppc')
                            ->where('vpr_bulan', $bulan)
                            ->where('vpr_tahun', $tahun)
                            ->where('vpr_kode_supplier', $supp)
                            ->COUNT('*');
            // dd($cek_data_simpan);
            $jumlahData 	= $cek_data_simpan;					
            
            $tablenya .= '<tr bg-color="blue">
                                <td align="center" bgcolor="FFCC66"><b>No</b></td>
                                <td align="center" bgcolor="FFCC66"><b>Item</b></td>
                                <td align="center" bgcolor="FFCC66"><b>&nbsp;</b></td>';
        
            for ($x = 1; $x<=$jumHari; $x++){
                $tablenya .= '<td width="100" align="center" bgcolor="FFCC66">'.$x.'</td>';	
            }
                $tablenya .= '<td align="center" bgcolor="FFCC66"><b>Total</b></td>
                        </tr>';
        
            $no                     = 1;
            $total_plan             = 0;	
            $total_actual           = 0;
            $jumlahBanyakData       = 0;	
            $jumlahBanyakData_acumm = 0;	
            $jumlah_data_plan       = 0;	
            $jumlahBanyakData_A     = 0;
            $jumlahBanyakData_persenAcc = 0;
    
            foreach($sql_cek_mfg as $sc){	
                $part = $sc->dnd_part;
        
                //untuk menampilkan deskripsi item
                $rs_tampil_deskripsi= pt_mstr($part);

                $deskripsi_item = $rs_tampil_deskripsi['deskripsi1'];
                if(empty($rs_tampil_deskripsi['deskripsi2'])){
                    $type_item      = ' ';
                }else{
                    $type_item      = $rs_tampil_deskripsi['deskripsi2'];
                }
                
                $item_deskripsi = $deskripsi_item." ".$type_item; 
                
                //untuk mendapatkan qty perkanban
                $rs_PerKanban = DB::table('dn_item_supp')->where('kd_item' ,$part)->where('kd_supp',$supp)->first(); 
                if(empty($rs_PerKanban)){
                    $Pcs_Kanban = 1;
                }else{
                    $Pcs_Kanban = $rs_PerKanban->pcs_kanban;
                }
                
                
                $tablenya .= '<tr>
                                <td style="width:200px;" align="center" rowspan="9">'.$no.'</td>
                                <td style="width:200px;" align="center" rowspan="9">'.$part.'<br> <br><b>'.$item_deskripsi.'</b></td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffe6cc"><b>Plan</b></td>';

                $hari_akhir_a    = date("t",strtotime($tahun."-".$bulan."-01"));
                $tanggal_awal_a  = $tahun."-".$bulan."-01";
                $tanggal_akhir_a = $tahun."-".$bulan."-".$hari_akhir_a;
                $data_a          = array();

                $zz = DB::table('dn_mstr')
                ->join('dnd_det','dnd_det.dnd_tr_id','=','dn_mstr.dn_tr_id')
                ->select('dn_arrival_date', DB::raw('SUM(dnd_qty_order) as qty_dn'))
                ->groupBy('dn_arrival_date')
                ->where('dn_vend',$supp)
                ->where('dnd_part',$part)
                ->whereBetween('dn_arrival_date', [$tanggal_awal_a, $tanggal_akhir_a])
                ->get();
                foreach($zz as $loop){
                    $data_a[$loop->dn_arrival_date] = $loop->qty_dn;
                }
        
                $total_qty_dn       = 0;
                $total_plan         = 0;
                $jumlah_data_plan   = 0;	
                for ($a = 1; $a<=$jumHari; $a++){
                    $a      =  str_pad($a, 2, "0", STR_PAD_LEFT);
                    $bulan  =  str_pad($bulan, 2, "0", STR_PAD_LEFT);
                
                    $tanggal_looping_a = $tahun."-".$bulan."-".$a;
                    if(empty($data_a[$tanggal_looping_a])){
                        $data_a[$tanggal_looping_a] = 0;   
                    }
                    $qty_dn[$a] = $data_a[$tanggal_looping_a] * $Pcs_Kanban;						
    
                    if($qty_dn[$a] <> '' or $qty_dn[$a] <> 0){
                        $jumlah_data_plan  = $jumlah_data_plan +1;
                    }		
                
                    $tablenya .= '<input type="hidden" name="txt_part'.$tanggal_looping_a.'-'.$no.'" id="txt_part'.$tanggal_looping_a.'-'.$no.'" size="35" value="'.$part.'" readonly>';
                        
                    $tablenya .= '<td bgcolor="#ffe6cc">'.number_format($qty_dn[$a]).'</td>';
                
                    $tablenya .= '<input type="hidden" name="txt_plan'.$tanggal_looping_a.'-'.$no.'"  id ="txt_plan'.$tanggal_looping_a.'-'.$no.'" size="5" value="".$qty_dn[$a]."" readonly>';
                        
                    $total_plan = $total_plan + $qty_dn[$a];
                }
                
                $tablenya .= '<td bgcolor="#ffe6cc">'.number_format($total_plan).'</td>
                </tr>
                <tr>
                    <td bgcolor="#ffffe6"><b>Actual</b></td>';
                    $hari_akhir = date("t",strtotime($tahun."-".$bulan."-01"));
                    $tanggal_awal = $tahun."-".$bulan."-01";
                    $tanggal_akhir = $tahun."-".$bulan."-".$hari_akhir;
                    $data = array();

                    $total_actual = 0; 
                    $get_data_kedatangan =  DB::table('SOAP_prh_hist')
                        ->select('prhRcp_date', DB::raw('SUM(prhRcvd) as rcvd'))
                        ->groupBy('prhRcp_date')
                        ->where('prhVend', $supp)
                        ->where('prhPart', $part)
                        ->where('prhDomain', 'AAIJ')
                        ->whereBetween('prhRcp_date', [$tanggal_awal_a, $tanggal_akhir_a])
                        ->get();
                    foreach($get_data_kedatangan as $teko){
                        $data[$teko->prhRcp_date] = $teko->rcvd;
                    }

                for ($a = 1; $a<=$jumHari; $a++){
                    $a =  str_pad($a, 2, "0", STR_PAD_LEFT);
                    $bulan =  str_pad($bulan, 2, "0", STR_PAD_LEFT);
                
                    $tanggal_looping = $tahun."-".$bulan."-".$a;
                    if(empty($data[$tanggal_looping])){
                        $data[$tanggal_looping] = 0;   
                    }

                    $tablenya .= '<td bgcolor ="#ffffe6">'.number_format($data[$tanggal_looping]).'</td>';	
                
                    if ($data[$tanggal_looping] == ''){
                        $data[$tanggal_looping] = 0;	
                        $tablenya .= '<input type="hidden" name="txt_actual'.$tanggal_looping.'-'.$no.'"  id ="txt_actual'.$tanggal_looping.'-'.$no.'" value="'.$data[$tanggal_looping].'" readonly>';
                    }else{		
                        $tablenya .= '<input type="hidden" name="txt_actual'.$tanggal_looping.'-'.$no.'" id="txt_actual'.$tanggal_looping.'-'.$no.'" value="'.$data[$tanggal_looping].'" readonly>';
                    }		
                    $total_actual = $total_actual + $data[$tanggal_looping];
                }          
            
                $tablenya .= '<td bgcolor ="#ffffe6">'.number_format($total_actual).'</td>
                </tr>
                <tr>
                    <td nowrap bgcolor ="#ffe6cc"><b>% DAILY</b></td>';
                    $list_total_in = array();
                    $total_persen_daily = 0;
                    $jumlahBanyakData   = 0;		
                    $jumlahBanyakDataAcc= 0;

                for ($a = 1; $a<=$jumHari; $a++){
                    $a =  str_pad($a, 2, "0", STR_PAD_LEFT);
                    $bulan =  str_pad($bulan, 2, "0", STR_PAD_LEFT);			
                    $tanggal_looping_a = $tahun."-".$bulan."-".$a;

                    if ($data[$tanggal_looping_a] == '' || $qty_dn[$a] == ''){
					    $list_total_in[$a] = 0;
                    }else{	
                        $jumlahBanyakData = $jumlahBanyakData + 1;
                        $list_total_in[$a] = ($data[$tanggal_looping_a] / $qty_dn[$a])*100;        
                    }
                    
                    $total_persen_daily = $total_persen_daily + $list_total_in[$a];
                        
                    if ($jumlah_data_plan == 0){	
                        $totalPersenDaily = 0;
                    }else{
                        $totalPersenDaily = $total_persen_daily/$jumlah_data_plan;
                    }	

                    $tablenya .= '<td bgcolor ="#ffe6cc">'.number_format($list_total_in[$a]).' %</td>';
                    
                    $tablenya .= '<input type="hidden" name="txt_persen_daily'.$tanggal_looping_a.'-'.$no.'" id ="txt_persen_daily'.$tanggal_looping_a.'-'.$no.'" size="5" value="'.$list_total_in[$a].'" readonly>';
                }	
            
                if ($total_actual == 0){	
                    $total_persenDaily = 0;
                }else{
                    $total_persenDaily = ($total_plan/$total_actual) *100;
                }	
            
                $tablenya .= '<td bgcolor ="#ffe6cc">'.number_format($totalPersenDaily).'</td>
                </tr>
                <tr>
                    <td nowrap bgcolor ="#ffffe6"><b>% ACCUM</b></td>';
                $list_persen_akumulasi = array();
                $total_persen_accumulasi = 0;
                $total_persenAccumulasi  = 0;
                $jumlahBanyakData_acumm  = 0;

                
                for ($a = 1; $a<=$jumHari; $a++){
                    $b =  str_pad($a - 1, 2, "0", STR_PAD_LEFT);
                    $a =  str_pad($a, 2, "0", STR_PAD_LEFT);
                  
                
                    $bulan =  str_pad($bulan, 2, "0", STR_PAD_LEFT);
                
                    $tanggal_looping_lpb = $tahun."-".$bulan."-".$a;   
                    // dd($data);
                    // $list_acc_actual[$a] = $data[$tanggal_looping_lpb] + $list_acc_actual[$b];
                    // $list_acc_planning[$a] = $qty_dn[$a] + $list_acc_planning[$b];

                    $list_acc_actual[$a] = 0;
                    $list_acc_planning[$a] = 0;
                
                    if ($list_acc_planning[$a] == '0'){
                        $list_persen_akumulasi[$a] = 0;
                        $tablenya .= '<input type="hidden" name="txt_persen_accum'.$tanggal_looping_lpb.'-'.$no.'" id ="txt_persen_accum'.$tanggal_looping_lpb.'-'.$no.'"  size="5" value="'.$list_persen_akumulasi[$a].'" readonly>';
                    }else{
                        $list_persen_akumulasi[$a] = ($list_acc_actual[$a] / $list_acc_planning[$a])*100;
                        $tablenya .= '<input type="hidden" name="txt_persen_accum'.$tanggal_looping_lpb.'-'.$no.'" id="txt_persen_accum'.$tanggal_looping_lpb.'-'.$no.'"  size="5" value="'.$list_persen_akumulasi[$a].'" readonly>';
                    }
                    
                    if ($qty_dn[$a] == 0 and $data[$tanggal_looping_lpb] == 0 ){
                        $list_persen_akumulasi[$a] = 0;
                        $tablenya .= '<td bgcolor ="#ffffe6">'.number_format($list_persen_akumulasi[$a]).'</td>';
                    }else{		
                        $tablenya .= '<td bgcolor ="#ffffe6">'.number_format($list_persen_akumulasi[$a]).'%</td>';
                        
                        $jumlahBanyakData_persenAcc = $jumlahBanyakData_persenAcc + 1;
                        $jumlahBanyakData_acumm     = $jumlahBanyakData_acumm + 1;
                    }
                    
                    $total_persen_accumulasi = $total_persen_accumulasi + $list_persen_akumulasi[$a];

                    if ($total_persen_accumulasi == 0){
                        //$total_persenAccumulasi = ($total_persen_accumulasi/$jumlahBanyakData_persenAcc);
                        $total_persenAccumulasi = 0;
                    }else{
                        $total_persenAccumulasi = ($total_persen_accumulasi/$jumlahBanyakData_persenAcc);
                    }	
                
                }	

                $tablenya .= '<td bgcolor ="#ffffe6">'.number_format($total_persenAccumulasi).'</td>';
                $tablenya .= '</tr>
                <tr>
                    <td nowrap bgcolor="#ffe6cc"><b>Nilai DAILY</b></td>';
            
                $total_nilai_daily = 0;
                $total_persenDaily = 0;
                for ($a = 1; $a<=$jumHari; $a++){
                    $b =  str_pad($a - 1, 2, "0", STR_PAD_LEFT);
                    $a =  str_pad($a, 2, "0", STR_PAD_LEFT);
                    
                    $bulan =  str_pad($bulan, 2, "0", STR_PAD_LEFT);
                    
                    $tanggal_looping_lpb = $tahun."-".$bulan."-".$a;
                
                    //untuk mendapatkan nilai Daily dari master achivement
                    $tampil_nilaiDaily = DB::table('vpr_master_achivement')->select('vpr_nilai')
                    ->where('vpr_nilai_bawah','<=',$list_total_in[$a])
                    ->where('vpr_nilai_atas','>=',$list_total_in[$a])->first();
                    // dd($tampil_nilaiDaily);	
                    if(empty($tampil_nilaiDaily)){
                        $nilaiDaily[$a] = 0;
                    }else{
                        $nilaiDaily[$a] = $row_day['vpr_nilai'];
                    }
                   								   
                    
                    if ($nilaiDaily[$a]  == ''){	
                        $nilaiDaily[$a] = 0;
                        
                        $tablenya .= '<input type="hidden" name="txt_nilai_daily'.$tanggal_looping_lpb.'-'.$no.'" id ="txt_nilai_daily'.$tanggal_looping_lpb.'-'.$no.'" size="5" value="'.$nilaiDaily[$a].'" readonly>';
                    }else{
                        $tablenya .= '<td bgcolor ="#ffe6cc'>''.$nilaiDaily[$a].'</td>';
                        
                        $tablenya .= '<input type="hidden" name="txt_nilai_daily'.$tanggal_looping_lpb.'-'.$no.'" id="txt_nilai_daily'.$tanggal_looping_lpb.'-'.$no.'"  size="5" value="'.$nilaiDaily[$a].'" readonly>';
                    }	

                    $total_nilai_daily = $total_nilai_daily + $nilaiDaily[$a];
                    
                    if ($jumlahBanyakData_acumm == 0){
                        $total_persenDaily = 0;	
                    }else{			
                        $total_persenDaily = ($total_nilai_daily/$jumlahBanyakData_acumm);	
                    }
                    
                }
            
                $tablenya .= '<td bgcolor ="#ffe6cc">'.number_format($total_persenDaily).'</td>
                </tr>
                <tr>    
                    <td nowrap bgcolor="#ffffe6"><b>Nilai ACC</b></td>';
                $total_nilai_akumulasi = 0;
                $total_nilaiAKumulasi  = 0;
                $jumlahBanyakDataAcc   = 0;

                for ($a = 1; $a<=$jumHari; $a++){
                    $b =  str_pad($a - 1, 2, "0", STR_PAD_LEFT);
                    $a =  str_pad($a, 2, "0", STR_PAD_LEFT);
                    
                    $bulan =  str_pad($bulan, 2, "0", STR_PAD_LEFT);
                    
                    $tanggal_looping_lpb = $tahun."-".$bulan."-".$a;
                    
                    //untuk menampilkan nilai Achivement
                    $tampil_nilaiAkumulasi ="select vpr_nilai from vpr_master_achivement
                                            where '".$list_persen_akumulasi[$a]."' >= vpr_nilai_bawah  
                                            and '".$list_persen_akumulasi[$a]."' <= vpr_nilai_atas ";

                    $tampil_nilaiAkumulasi = DB::table('vpr_master_achivement')->select('vpr_nilai')
                    ->where('vpr_nilai_bawah','<=',$list_persen_akumulasi[$a])
                    ->where('vpr_nilai_atas','>=',$list_persen_akumulasi[$a])->first();
                    // dd($tampil_nilaiDaily);	
                    if(empty($tampil_nilaiAkumulasi)){
                        $nilaiAkumulasi[$a] = 0;
                    }else{
                        $nilaiAkumulasi[$a] = $row_day['vpr_nilai'];
                    }							
                    
                    if ($nilaiAkumulasi[$a]  == ''){

                    }else{
                        if ($qty_dn[$a] == 0 and $data[$tanggal_looping_lpb] == 0 ){
                            $nilaiAkumulasi[$a] = 0;
                            $tablenya .= '<td bgcolor ="#ffffe6">'.$nilaiAkumulasi[$a].'</td>
                            <input type="hidden" name="txt_nilai_accum'.$tanggal_looping_lpb.'-'.$no.'" id="txt_nilai_accum'.$tanggal_looping_lpb.'-'.$no.'"  size="5" value="'.$nilaiAkumulasi[$a].'" readonly>';
                            
                        }else{	
                            $tablenya .= '<td bgcolor ="#ffffe6">'.$nilaiAkumulasi[$a].'</td>';
                        }
                        
                        $total_nilai_akumulasi = $total_nilai_akumulasi + $nilaiAkumulasi[$a];
                            
                        if ($jumlahBanyakData_acumm == 0){	
                            $total_nilaiAKumulasi = 0;
                        }else{
                            $total_nilaiAKumulasi = ($total_nilai_akumulasi/$jumlahBanyakData_acumm);
                        }		
                    }		
                }
                
                $tablenya .= '<td bgcolor ="#ffffe6">'.number_format($total_nilaiAKumulasi).'</td>
                </tr>
                <tr>
                    <td bgcolor ="#ffe6cc"><b>Nilai</b></td>';

                $total_nilai_semua = 0;
                $total_nilaiSemua = 0;

                for ($a = 1; $a<=$jumHari; $a++){
                    $b =  str_pad($a - 1, 2, "0", STR_PAD_LEFT);
                    $a =  str_pad($a, 2, "0", STR_PAD_LEFT);
                
                    $bulan =  str_pad($bulan, 2, "0", STR_PAD_LEFT);
                
                    $tanggal_looping_lpb = $tahun."-".$bulan."-".$a;
                
                    $Akumulasi_value[$a] 	= $nilaiAkumulasi[$a] * 0.2 ;
                    $Daily_value[$a] 		= $nilaiDaily[$a] * 0.8 ;
                
                    $nilai_semua[$a]		=  $Akumulasi_value[$a] + $Daily_value[$a] ;
                
                
                    if ($qty_dn[$a] == 0 and $data[$tanggal_looping_lpb] == 0 ){
                        $nilai_semua[$a] = '0';
                        $tablenya .= '<td bgcolor="#ffe6cc">'.$nilai_semua[$a].'</td><input type="hidden" name="txt_nilai'.$tanggal_looping_lpb.'-'.$no.'" id="txt_nilai'.$tanggal_looping_lpb.'-'.$no.'" size="5" value="'.$nilai_semua[$a].'" readonly>';
                    }else{	
                        $tablenya .= '<td bgcolor="#ffe6cc">'.$nilai_semua[$a].'</td><input type="hidden" name="txt_nilai'.$tanggal_looping_lpb.'-'.$no.'" id="txt_nilai'.$tanggal_looping_lpb.'-'.$no.'"  size="5" value="'.$nilai_semua[$a].'" readonly>';                                
                    }

                    $total_nilai_semua = $total_nilai_semua + $nilai_semua[$a];

                    if ($jumlahBanyakData_acumm == 0){	
                        $total_nilaiSemua = 0;
                    }else{
                        $total_nilaiSemua = ($total_nilai_semua/$jumlahBanyakData_acumm);
                    }		
                }


                $tablenya .= '<td bgcolor ="#ffe6cc">'.number_format($total_nilaiSemua).'</td>
                </tr>
                <tr>
                    <td bgcolor="#ffffe6"><b>Nilai on Time</b></td>';

                $jumlahOnTime = 0;
                for ($a = 1; $a<=$jumHari; $a++){
                    $a =  str_pad($a, 2, "0", STR_PAD_LEFT);
                    $bulan =  str_pad($bulan, 2, "0", STR_PAD_LEFT);								
                    $tanggal_looping_lpb = $tahun."-".$bulan."-".$a;
                    
                    if ($qty_dn[$a] == 0 and  $data[$tanggal_looping_lpb] == 0){
                        $nilai_on_time[$a] = 0;
                        $tablenya .= '<td bgcolor="#ffffe6">'.$nilai_on_time[$a].'</td>
                        <input type="hidden" name="txt_onTime'.$tanggal_looping_lpb.'-'.$no.'" id="txt_onTime'.$tanggal_looping_lpb.'-'.$no.'" size="5" value="'.$nilai_on_time[$a].'" readonly>';
                    }else{
                        if ($qty_dn[$a] == $data[$tanggal_looping_lpb] ){
                            $nilai_on_time[$a] = 1;
                            $tablenya .= '<td bgcolor="#ffffe6">'.$nilai_on_time[$a].'</td><input type="hidden" name="txt_onTime'.$tanggal_looping_lpb.'-'.$no.'" id="txt_onTime'.$tanggal_looping_lpb.'-'.$no.'"  size="5" value="'.$nilai_on_time[$a].'" readonly>';
                        }else{	
                            if ($data[$tanggal_looping_lpb] > $qty_dn[$a] and  $qty_dn[$a] <> 0){
                                $nilai_on_time[$a] = 1;
                                $tablenya .= '<td bgcolor="#ffffe6">'.$nilai_on_time[$a].'</td>
                                <input type="hidden" name="txt_onTime'.$tanggal_looping_lpb.'-'.$no.'"  id ="txt_onTime'.$tanggal_looping_lpb.'-'.$no.'"  "size="5" value="'.$nilai_on_time[$a].'" readonly>';
                            }else{
                                if ($data[$tanggal_looping_lpb] > $qty_dn[$a] and  $qty_dn[$a] == 0){
                                    $nilai_on_time[$a] = 0;
                                    $tablenya .= '<td bgcolor="#ffffe6">'.$nilai_on_time[$a].'</td>
                                    <input type="hidden" name="txt_onTime'.$tanggal_looping_lpb.'-'.$no.'" id="txt_onTime'.$tanggal_looping_lpb.'-'.$no.'" size="5" value="'.$nilai_on_time[$a].'" readonly>';
                                }else{	
                                    if ($data[$tanggal_looping_lpb] < $qty_dn[$a]){
                                        $nilai_on_time[$a] = 0;
                                        $tablenya .= '<td bgcolor="#ffffe6">'.$nilai_on_time[$a].'</td>
                                        <input type="hidden" name="txt_onTime'.$tanggal_looping_lpb.'-'.$no.'" id="txt_onTime'.$tanggal_looping_lpb.'-'.$no.'" size="5" value="'.$nilai_on_time[$a].'" readonly>';
                                    }	
                                }
                            }			
                        }	
                    }

                    if ($nilai_on_time[$a] == 1){
                        $jumlahOnTime = $jumlahOnTime + $nilai_on_time[$a] ;
                    }	
                    
                }

                if ($jumlahBanyakData_acumm == 0){
                    $persen_onTime = 0;
                }else{
                    $persen_onTime = ($jumlahOnTime/$jumlahBanyakData_acumm)*100;
                }		
        
                $tablenya .= '<td bgcolor="#ffffe6">'.number_format($persen_onTime,0).'</td>
                </tr>';
                $no = $no + 1;
        
                $jumlahBanyakData           = 0;	
                $jumlahBanyakData_acumm     = 0;	
                $jumlah_data_plan           = 0;	
                $jumlahBanyakData_persenAcc = 0;	
                $total_all_total_plan = 0;
                $total_all_total_actual = 0;
                $total_all_nilai = 0;
                $total_all_nilai_onTime = 0;
                $total_all_nilaiOnTime = 0;
                
                $total_all_total_plan   = $total_all_total_plan + $total_plan ;
                $total_all_total_actual = $total_all_total_actual + $total_actual ;
                $total_all_nilai        = $total_all_nilai + $total_nilaiSemua ;
                $total_all_nilaiOnTime  = $total_all_nilaiOnTime + $persen_onTime ;
                
                if ($total_all_total_plan == 0 and  $total_all_total_actual == 0){
                    $total_all_nilai_onTime = 0;
                }else{
                    if ($total_all_total_plan == $total_all_total_actual){
                        $total_all_nilai_onTime= 1;
                    }else{	
                        if ($total_all_total_actual > $total_all_total_plan and  $total_all_total_plan <> 0){
                        $total_all_nilai_onTime = 1;
                        }else{
                            if ($total_all_total_actual < $total_all_total_plan){
                            $total_all_nilai_onTime = 0;
                            }	
                        }	
                    }		
                }
            }	
        }else{
            "tidak ada data";
        }
        return view('vendor-performance-report.index',compact('tablenya'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
