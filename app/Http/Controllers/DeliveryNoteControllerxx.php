<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Collection;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use App\DeliveryNote;
use App\BusinesRelation;
use PDF;
use Illuminate\Support\Facades\Storage;
use App\Mail\sendEmail;
use App\Mail\MailDeliveryNote;
use App\SOAPPoMstr;
use App\PubDnMaster;
use App\SOAPPtMstr;
use App\DndNet;
use App\DNDHistory;


class DeliveryNoteController extends Controller
{
    public function index(Request $request) {
        if ($request->ajax()) {
            if (!empty($request->filter_dn_number) && !empty($request->filter_id_supp) && !empty($request->filter_moon) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number) && !empty($request->filter_id_supp) && !empty($request->filter_moon)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number) && !empty($request->filter_id_supp) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number) && !empty($request->filter_moon) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_id_supp) && !empty($request->filter_moon) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number) && !empty($request->filter_id_supp)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number) && !empty($request->filter_moon)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_id_supp) && !empty($request->filter_moon)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_id_supp) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_moon) && !empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_dn_number)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_tr_id', 'like', '%' . $request->filter_dn_number . '%')
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if (!empty($request->filter_moon)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->whereMonth('dn_mstr.dn_arrival_date', $request->filter_moon)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if (!empty($request->filter_year)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->whereYear('dn_mstr.dn_arrival_date',  $request->filter_year)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else if(!empty($request->filter_id_supp)) {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->where('dn_mstr.dn_vend', '=',  $request->filter_id_supp)
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            } else {
                $data = DB::table('dn_mstr')
                ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_mstr.dn_vend')
                ->select('dn_mstr.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                ->get();
            }

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    $confirm = 'return confirm("are you sure?")';
                    return "
                        <td>
                            <a class='btn btn-xs btn-warning mb-1' href='/edit-delivery-notes/" . base64_encode($data->dn_tr_id) . "'><i class='fas fa-edit'></i></a>

                            <a class='btn btn-xs btn-danger mb-1' href='/delete-delivery-notes/" . base64_encode($data->dn_tr_id) . "' onclick='".$confirm."'><i class='fas fa-trash'></i></a>
                        </td>
                    ";
                })
                ->make(true);
        }

        return view('delivery-notes.index');
    }

    public function create() {
        return  view('delivery-notes.create');
    }

    public function modaldeliverynotes() {

        $data = SOAPPoMstr::all();

        return  view('delivery-notes.modal_create_delivery_notes', compact('data'));
    }

    public function checkcycle(Request $request) {

        $cycle = $request->get('selectcycle');
        $kdsupp = $request->get('kdsupp');

        $amountcycle = DB::table('whs_cycle_master')->select('cycle_kode_supplier')
                ->where('cycle_kode_supplier', $kdsupp)
                ->count();

        if ($amountcycle == 0) {
            return 0;
        }else {

        }

    }

    public function seconddatadeliverynote(Request $request) {

        $poNo = $request->get('ponum');
        $kdsupp = $request->get('kdsupp');

        // getPODetail($poNo);
        // $poNo = '';
        // $kdsupp = '';

        //data line, pp number, item numberm, part name, type
        $data = SOAPPtMstr::join('SOAP_po_detail', 'SOAP_pt_mstr.item_number', '=', 'SOAP_po_detail.item_number')
                            ->where('SOAP_pt_mstr.pt_domain', 'AAIJ')
                            ->where('SOAP_po_detail.no_po', $poNo)
                            // ->orderBy('SOAP_po_detail.pod_line', 'ASC')
                            ->get(['SOAP_pt_mstr.*', 'SOAP_po_detail.*']);

        $jml_row = count($data);

            $tablenya = '';
            foreach($data as $dt){

                $itnum = $dt->item_number;
                $item_supp = DB::table('dn_item_supp')->where('kd_item',$itnum)->where('kd_supp', $kdsupp)->first();

                $pcs_kanban = $item_supp->pcs_kanban;
                $pallet_kanban = $item_supp->pallet_kanban;

                if ($pcs_kanban)
                {
                    $datakan = $pcs_kanban;
                }
                else
                {
                    $datakan = 1;
                } 

                $LPB = DB::table('elpb_dtl')
                    ->where('po', $poNo)
                    ->where('item_number', $itnum)
                    // ->where('line', $data[0]['pod_line'])
                    ->whereNull('rc')
                    ->whereNull('qty_received')
                    ->sum('qty_received');
                   

        // Untuk Menampilkan total DN
        $totalDN = DB::table('dnd_det')
                        ->join('dn_mstr','dnd_det.dnd_tr_id', '=','dn_mstr.dn_tr_id')
                        ->where('dnd_det.dnd_po_nbr', $poNo)
                        ->where('dnd_part', $itnum)
                        // ->where('dnd_line', $data[0]['pod_line'])
                        ->sum('dnd_qty_order');
                      
        // $qtySumDN = $totalDN->dnd_qty_order;

        // $sumDN = $totalDN*$datakan;
                    
        // while ($podnbr) {
        $qty_sumDN = $totalDN*$datakan ;
        
        // untuk menampilkan Total SJ yang sudah dibuat oleh Supplier
        $totalSJ =  DB::table('portald_det')
                        ->where('portald_po_nbr', $poNo)
                        ->where('portald_part', $itnum)
                        // ->where('portald_line', $data[0]['pod_line'])
                        ->sum('portald_qty_ship');
                        
        $qty_open_dn = $qty_sumDN - $totalSJ;
        //     // untuk menampilkan Open DN
        //     $openDN = $totalDN - $totalSJ;

        //untuk menampilkan OTY Outstanding SJ
        $qtylSJ =  DB::table('portald_det')
                        ->where('portald_po_nbr', $poNo)
                        ->whereNull('portald_confirm_date')
                        ->where('portald_part', $itnum)
                        // ->where('portald_line', $data[0]['pod_line'])
                        ->whereDate('portald_dlv_date', '!=', Carbon::today())
                        ->sum('portald_qty_ship');
                        
        //untuk menampilkan OTY SJ TODAY
        $qtysjtoday =  DB::table('portald_det')
                        ->where('portald_po_nbr', $poNo)
                        ->where('portald_part', $itnum)
                        // ->where('portald_line', $data[0]['pod_line'])
                        ->whereNull('portald_confirm_date')
                        ->whereDate('portald_dlv_date', Carbon::today())
                        ->sum('portald_qty_ship');

        $over_po =  ($dt->qty_po)-($dt->qty_receive)-($qtylSJ);
        $data_persen  = (($LPB  + $qtylSJ)/$dt->qty_po)*100;			
        if ($itnum == '51-23311-28510'){
			//$qtyOpen1 = (($qtyPO - $qtyLPB)-($qty_outstanding_sj + $qty_outstanding_Today));
			$qtyOpen1 = (($dt->qty_po - $LPB)-($qtylSJ + $qtysjtoday));
		}else{
			$qtyOpen1 = (($dt->qty_po - $LPB)-($qty_open_dn + $qtylSJ + $qtysjtoday));
		}	
					
		$qtyOpen = $qtyOpen1/$datakan;
            
                $tablenya .= '<tr>
                    <td>LINE</td>
                    <td>'.$dt->no_pp.'</td>
                    <td>'.$dt->item_number.'</td>
                    <td>'.$dt->deskripsi1.''.$dt->deskripsi2.'</td>
                    <td>'.$dt->desc_type.'</td>
                    <td>'.$datakan.'</td>
                    <td>'.$pallet_kanban.'</td>
                    <td>'.$dt->qty_po.'</td>
                    <td>'.$dt->qty_receive.'</td>
                    <td>'.$totalDN.'</td>
                    <td>'.$totalSJ.'</td>
                    <td>'.$qty_open_dn.'</td>
                    <td>'.$qtylSJ.'</td>
                    <td>'.$qtysjtoday.'</td>
                    <td>'.$over_po.'</td>
                    <td><input type="number" class="form-control" name="ordertb[]" id="ordertb[]"> </td>
                    <td>'.$qtyOpen.'</td>
                    <td>'.$data_persen.'</td>

                    <input type="hidden" id="ppnumber[]" name="ppnumber[]" value="'. $dt->item_number .'">
                    <input type="hidden" id="nopp[]" name="nopp[]" value="'. $dt->no_pp.'">
                    <input type="hidden" id="partname[]" name="partname[]" value="'.$dt->deskripsi1.''.$dt->deskripsi1.'">
                    <input type="hidden" id="type[]" name="type[]" value="'.$dt->desc_type.'">
                    <input type="hidden" id="pcskanban[]" name="pcskanban[]" value="'.$datakan.'">
                    <input type="hidden" id="palletkanban[]" name="palletkanban[]" value="'.$pallet_kanban.'">
                    <input type="hidden" id="qty_po[]" name="qty_po[]" value="'.$dt->qty_po.'">
                    <input type="hidden" id="row" name="row" value="'.$jml_row.'">

                </tr>';
                
                // endforeach;
        }
        return response($tablenya);
    }

    public function storadeliverynote(Request $request) {

        $order = $request->get('ordertb');
        $ppnumber = $request->get('ppnumber');
        $partname = $request->get('partname');
        $type = $request->get('type');
        $pcskanban = $request->get('pcskanban');
        $palletkanban = $request->get('palletkanban');
        $qty_po = $request->get('qty_po');
        $ponum = $request->get('ponum');
        $kdsupp = $request->get('kdsupp');
        $orderdate = $request->get('orderdate');
        $arrivaldate = $request->get('arrivaldate');
        $numberDN = $request->get('numberDN');
        $ordercyle = $request->get('ordercyle');
        $arrivalecycle = $request->get('arrivalecycle');

        $row = $request->get('row');

        $todnmstr = PubDnMaster::create([
            'dn_tr_id'  => $numberDN,
            'dn_po_nbr'    => $ponum,
            'dn_user_id'   => 1,
            'dn_arrival_date' => $arrivaldate,
            'dn_order_date'=> $orderdate,
            'dn_vend' => $kdsupp
            // 'dn__char02'  => $request->??,
            // 'dn_ip'      => $request->??,
            // 'dn__date01'      => $request->??
        ]);

        for ($i=0; $i < $row; $i++) {
            // dd($ppnumber[$i]);
            $data = new DndNet;
			$data->dnd_tr_id = $numberDN;
			$data->dnd_po_nbr = $ponum;
			$data->dnd_part = $ppnumber[$i];
			$data->dnd_user_id = 1;
			$data->dnd_qty_order = $order[$i];
			$data->dn_cycle_arrival = $arrivalecycle;
			$data->dn_cycle_order = $ordercyle;
			$data->save();

            $data = new DNDHistory;
			$data->h_dnd_no = $numberDN;
			$data->h_dnd_po = $ponum;
			$data->h_dnd_item = $ppnumber[$i];
			$data->h_dnd_effdate = Carbon::today();
			$data->h_dnd_create = 'COBA';
			$data->h_dnd_action = 'ADD';
			$data->save();
            
        }


        $data = [
            'name' => 'Diwa',
            'body' => 'Akebono Astra'
        ];

        $address = "http://supplier.akebono-astra.co.id/cek_verifikasi_dn.php?".paramEncrypt("username=".$username_supplier."&transID=".$dn."")."";

        Mail::to('diwafairus13@gmail.com')->send(new sendEmail($data));

        $result = 'berhasil';
        return response($result);
        // return redirect('delivery-notes/create', compact('data'));
    }

    public function sendemail() {

        $details = [
            'title' => 'Akebono',
            'body' => 'Akebono Astra'
        ];

        Mail::to('diwafairus13@gmail.com')->send(new MailDeliveryNote($details));

        return 'berhasil';
    }

    public function store(Request $request)
    {
    }

    public function show(DeliveryNote $deliveryNote)
    {
    }

    public function edit($id)
    {
        $atas = DB::table('dn_mstr')
        ->join('dnd_det','dnd_det.dnd_tr_id','=','dn_mstr.dn_tr_id')
        ->join('SOAP_Pub_Business_Relation','SOAP_Pub_Business_Relation.ct_vd_addr','=','dn_mstr.dn_vend')
        ->where('dn_tr_id', base64_decode($id))
        ->first();
        $bawah = DB::table('dnd_det')
        ->where('dnd_po_nbr', $atas->dn_po_nbr)
        ->where('dnd_tr_id',$atas->dn_tr_id)
        ->get();
        return view('delivery-notes.edit',compact('atas','bawah'));
    }

    public function delete($id)
    {
        DB::table('dn_mstr')->where('dn_tr_id', '=', base64_decode($id))->delete();
        DB::table('dnd_det')->where('dnd_tr_id', '=', base64_decode($id))->delete();
        return back();
    }

    public function update_dn(Request $request){
        $row        = $request->get('row');
        $dnd_tr_id      = $request->get('dnd_tr_id');
        $dnd_part    = $request->get('dnd_part');
        $dnd_line    = $request->get('dnd_line');
        $order    = $request->get('order');

        date_default_timezone_set('Asia/Jakarta');
        $ambil_date = date('Y-m-d H:i:s');
        for($a = 0; $a < $row; $a++){
            DB::table('dnd_det')
            ->where('dnd_tr_id', $dnd_tr_id)
            ->where('dnd_part', $dnd_part[$a])
            ->where('dnd_line', $dnd_line[$a])
            ->update(['dnd_qty_order' => $order[$a]]);
        }
        $data['message'] = 1;
        echo json_encode($data);
    }

    public function update(Request $request, DeliveryNote $deliveryNote)
    {
    }

    public function destroy(DeliveryNote $deliveryNote)
    {
    }

    public function summary() {
        return view('delivery-notes.summary');
    }

    public function viewforsupp() {
        return view('delivery-notes.view');
    }

    public function cetak_pdf() {
        $pdf = PDF::loadview('delivery-notes.cetak_pdf');
        // return $pdf->stream();

        Storage::put('public/pdf/' . time() . '.pdf', $pdf->output());

        return $pdf->download(time() . '.pdf');
    }
}
