<?php

namespace App\Http\Controllers;

use App\Auth;
use App\SOAP_Prh_Hist;
use Facade\FlareClient\Stacktrace\File;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class IntransTransfer extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {            
            $code = $request->code;
            $year = $request->year;
            $month = $request->month;

            if ($code != '') {
                if ($year != '' && $month != '') {
                    $data = DB::select("SELECT DISTINCT int_inv,int_etd,int_ship,int_confirm,int_supp,int_confirm_whs_status, int_inventory_ket FROM intrans_trans, intrans_trans_email WHERE int_inv = email_invoice AND YEAR(int_etd)= '$year' AND MONTH(int_etd) = '$month' AND int_supp = '$code' AND email_section_status ='OK' ORDER BY int_etd DESC");
                }else if ($month != '') {
                    $data = DB::select("SELECT DISTINCT int_inv,int_etd,int_ship,int_confirm,int_supp,int_confirm_whs_status, int_inventory_ket FROM intrans_trans, intrans_trans_email WHERE int_inv = email_invoice AND MONTH(int_etd) = '$month' AND int_supp = '$code' AND email_section_status ='OK' ORDER BY int_etd DESC");
                }else if ($year != '') {
                    $data = DB::select("SELECT DISTINCT int_inv,int_etd,int_ship,int_confirm,int_supp,int_confirm_whs_status, int_inventory_ket FROM intrans_trans, intrans_trans_email WHERE int_inv = email_invoice AND YEAR(int_etd)= '$year' AND int_supp = '$code' AND email_section_status ='OK' ORDER BY int_etd DESC");
                }else{
                    $data = 0;
                }
            } else {
                if ($year != '' && $month != '') {
                    $data = DB::select("SELECT DISTINCT int_inv,int_etd,int_ship,int_confirm,int_supp,int_confirm_whs_status, int_inventory_ket FROM intrans_trans, intrans_trans_email WHERE int_inv = email_invoice AND YEAR(int_etd)= '$year' AND MONTH(int_etd) = '$month' AND email_section_status ='OK' ORDER BY int_etd DESC");
                }else if ($month != '') {
                    $data = DB::select("SELECT DISTINCT int_inv,int_etd,int_ship,int_confirm,int_supp,int_confirm_whs_status, int_inventory_ket FROM intrans_trans, intrans_trans_email WHERE int_inv = email_invoice AND MONTH(int_etd) = '$month' AND email_section_status ='OK' ORDER BY int_etd DESC");
                }else if ($year != '') {
                    $data = DB::select("SELECT DISTINCT int_inv,int_etd,int_ship,int_confirm,int_supp,int_confirm_whs_status, int_inventory_ket FROM intrans_trans, intrans_trans_email WHERE int_inv = email_invoice AND YEAR(int_etd)= '$year' AND email_section_status ='OK' ORDER BY int_etd DESC");
                }else{
                    $data = 0;
                }
            }
            $no = 1;
            $html = '';
            if(count($data) > 0){
                foreach($data as $row){
                    $jmlInv = 0;

                    $int_inv = $row->int_inv; #0
                    $int_etd = $row->int_etd; #1
                    $int_ship = $row->int_ship; #2
                    $int_confirm = $row->int_confirm; #3
                    $int_supp = $row->int_supp; #4
                    $int_confirm_whs_status = $row->int_confirm_whs_status; #5
                    $int_inventory_ket = $row->int_inventory_ket; #6
                    $rInv = DB::select("SELECT distinct int_cek, int_cek from intrans_trans where
                    int_inv= '$int_inv' and int_supp='$int_supp'");
                    $jmlInv = count($rInv);
                    $cek = $rInv[0]->int_cek;

                    $jmlLot = DB::select("SELECT * FROM intrans_trans WHERE int_inv = '$int_inv' AND
                    int_lot != '0'");

                    if(count($jmlLot) > 0){
                        $jmlLot = $jmlLot;
                    }else{
                        $jmlLot = 0;
                    }

                    $status_whs = $int_confirm_whs_status;
                    $date_whs = $int_inventory_ket;
                    $inventoryStatus = $int_inventory_ket;

                    // untuk menambahkan angka + delivery
                    $getDataDelivery = DB::table('intrans_leadtime_master')->where('lead_kode_supplier', $row->int_supp)->first();
                    $getLeadDelivery = $getDataDelivery->lead_delivery;
                    $estimasi_tgl_kedatangan = date('Y-m-d', strtotime('+'.$getLeadDelivery.'days', strtotime($int_etd)));

                    if ($jmlInv > 1) {
                        $confirm = 'Edit';
                    } else {
                        if ($cek == 'T') {
                            $confirm = $int_confirm;
                        } else {
                            $confirm = '';
                        }
                    }


                    $tanggal_lpb = '';
                    if ($confirm == '') {
                        $tampil_tgl_lpb = DB::table('SOAP_prh_hist')
                        ->select("prhRcp_date")
                        ->where('prhDomain', 'AAIJ')
                        ->where('prhVend', $int_supp)
                        ->where('prhPsNbr', $int_inv)
                        ->first();
                        if($tampil_tgl_lpb == null){
                            $tanggal_lpb = '';
                        }else{
                            $tanggal_lpb = $tampil_tgl_lpb->prhRcp_date;
                        }
                    } else {
                        $tanggal_lpb = $confirm;
                    }

                    // get supp (di native ada pada object class method getSupp)
                    $nmSupp = DB::select("SELECT ct_vd_addr FROM SOAP_Pub_Business_Relation WHERE ct_vd_addr = '$int_supp'");

                    if($nmSupp == NULL){
                        $nmSupp = '';
                    }else{
                        $nmSupp = $nmSupp[0]->ct_vd_addr;
                    }

                    $inv = str_replace('&', '---', $int_inv);

                    // untuk mendapat approval
                    $dataApproval = DB::table('intrans_trans_email')
                    ->where('email_invoice', $int_inv)
                    ->first();

                    $nama_section = $dataApproval->email_nama_section;
                    $status_section = $dataApproval->email_section_status;
                    $date_section = $dataApproval->email_section_date;

                    if($status_whs == 'OK'){
                        $td_status_whs = '<td bgcolor="#12F390" align="center">'.$status_whs.'</td>';
                    }else{
                        $td_status_whs = '<td>'.$status_whs.'</td>';
                    }

                    if ($int_confirm_whs_status == "OK"){
                        $action_int = '<td>-</td>
                        <td><a class="btn btn-sm btn-info" data-toggle="tooltip" title="print" href="/transfer-intrans-print/'.base64_encode($int_supp).'/'.base64_encode($inv).'"><span class="fas fa-print"></span></a></td>';
                    }else{
                        if ($inventoryStatus == 'M'){

                            $action_int = '
                            <td><a class="btn btn-sm" style="background-color: #00af91; color:white" data-toggle="tooltip" title="terima barang" href="/transfer-intrans-barang/'.base64_encode($int_supp).'/'.base64_encode($inv).'"><span class="fas fa-long-arrow-alt-left"></span></a></td>
                            <td><a class="btn btn-sm btn-info" data-toggle="tooltip" title="print" href="/transfer-intrans-print/'.base64_encode($int_supp).'/'.base64_encode($inv).'"><span class="fas fa-print"></span></a></td>';
                        }else{
                            if ($jmlLot == 0){
                                $action_int = '
                                <td><a class="btn btn-sm btn-success" data-toggle="tooltip" title="transfer" href="/transfer-intrans/'.base64_encode($int_supp).'/'.base64_encode($inv).'"><span class="fas fa-exchange-alt"></span></a></td>
                                <td><a class="btn btn-sm btn-info" data-toggle="tooltip" title="print" href="/transfer-intrans-print/'.base64_encode($int_supp).'/'.base64_encode($inv).'"><span class="fas fa-print"></span></a></td>';
                            }else{
                                $action_int = '
                                <td><a class="btn btn-sm" style="background-color: #12F390; color:white" data-toggle="tooltip" title="transfer with lot" href="/transfer-intrans-whs/'.base64_encode($int_supp).'/'.base64_encode($inv).'"><span class="fas fa-exchange-alt"></span></a></td>
                                <td><a class="btn btn-sm btn-primary" data-toggle="tooltip" title="print with lot" href="/transfer-intrans-print/'.base64_encode($int_supp).'/'.base64_encode($inv).'"><span class="fas fa-print"></span></a></td>';
                            }
                        }
                    }

                    $html .= '
                <tr>
                    <td>'.$no++.'</td>
                    <td>'.$int_supp.' - '.$nmSupp.'</td>
                    <td>'.$int_inv.'</td>
                    <td>'.$int_etd.'</td>
                    <td style="background-color: #FFCC80">'.$estimasi_tgl_kedatangan.'</td>
                    <td>'.$int_ship.'</td>
                    <td>'.$tanggal_lpb.'</td>
                    <td>'.$nama_section.'</td>
                    <td>'.$status_section.'</td>
                    <td>'.$date_section.'</td>
                    '.$td_status_whs.'
                    <td><a class="btn btn-sm btn-warning" data-toggle="tooltip" title="detail" href="/transfer-intrans-detail/'.base64_encode($int_supp).'/'.base64_encode($inv).'"><span class="fas fa-eye"></span></a></td>
                    '.$action_int.'
                </tr>'; 
                }
            }
            return response($html);
        } else {
            $getYear = Date('Y');
            $data = DB::select("SELECT DISTINCT int_po, int_inv,int_etd,int_ship,int_confirm,int_supp,int_confirm_whs_status, int_inventory_ket FROM intrans_trans, intrans_trans_email WHERE int_inv = email_invoice AND year(int_etd)='$getYear' AND email_section_status ='OK' ORDER BY int_etd DESC");

            return view('intrans-transfer.index', compact('data'));
        }
    }

    public function detail(Request $request, $kodesup, $inv)
    {

        $getKodeSup = base64_decode($kodesup);
        $getInv = base64_decode($inv);

        $data = DB::select("SELECT distinct int_po,int_line,int_part,int_lot,int_qty_rcp,int_qxtend_stat
        FROM intrans_trans WHERE int_supp = '$getKodeSup' AND int_inv = '$getInv'");

        $nameSupplier = DB::select("SELECT ct_ad_name FROM SOAP_Pub_Business_Relation WHERE ct_vd_addr = '$getKodeSup'");
        if ($nameSupplier == NULL) {
            $nameSupplier = '';
        } else {
            $nameSupplier = $nameSupplier[0]->ct_ad_name;
        }

        return view('intrans-transfer.detail', [
            'data' => $data,
            'kode_supp' => $getKodeSup,
            'inv' => $getInv,
            'name_supp' => $nameSupplier
        ]);
    }

    public function transferIntrans(Request $request, $kodesup, $inv)
    {
        $kodesup = base64_decode($kodesup);
        $inv = base64_decode($inv);

        $dataID = DB::table('intrans_trans')->where('int_inv', $inv)->where('int_supp', $kodesup)->first();
        $containerID = DB::table('intrans_container')->where('con_inv_no', $inv)->get();
        $document = DB::table('intrans_doc_prc')->where('doc_no_invoice', $inv)->where('doc_file', '!=', '')->get();
        $dataAll = DB::table('intrans_trans')->where('int_inv', $inv)->where('int_supp', $kodesup)->get();

        $data = [
            'etd_date' => $dataID->int_etd,
            'ship_name' => $dataID->int_ship,
            'no_bl' => $dataID->int_no_bl,
            'kodesup' => $kodesup,
            'inv' => $inv,
            'int_qty_rcp' => $dataID->int_qty_rcp,
            // data full
            'container' => $containerID,
            'document' => $document,
            'intrans' => $dataAll
        ];

        return view('intrans-transfer.transfer', compact('data'));
    }

    public function transferIntransBarang(Request $request, $kodesup, $inv)
    {
        $kodesup = base64_decode($kodesup);
        $inv = base64_decode($inv);

        $dataID = DB::table('intrans_trans')->where('int_inv', $inv)->where('int_supp', $kodesup)->first();
        $containerID = DB::table('intrans_container')->where('con_inv_no', $inv)->get();
        $dataAll = DB::table('intrans_trans')->where('int_inv', $inv)->where('int_supp', $kodesup)->get();

        $data = [
            'etd_date' => $dataID->int_etd,
            'ship_name' => $dataID->int_ship,
            'no_bl' => $dataID->int_no_bl,
            'kodesup' => $kodesup,
            'inv' => $inv,
            'int_qty_rcp' => $dataID->int_qty_rcp,
            // data full
            'container' => $containerID,
            'intrans' => $dataAll
        ];

        return view('intrans-transfer.transfer-barang', compact('data'));
    }

    public function transferIntransWhs(Request $request, $kodesup, $inv)
    {
        $kodesup = base64_decode($kodesup);
        $inv = base64_decode($inv);

        $dataID = DB::table('intrans_trans')->where('int_inv', $inv)->where('int_supp', $kodesup)->first();
        $containerID = DB::table('intrans_container')->where('con_inv_no', $inv)->get();
        $document = DB::table('intrans_doc_prc')->where('doc_no_invoice', $inv)->where('doc_file', '!=', '')->get();
        $dataAll = DB::table('intrans_trans')->where('int_inv', $inv)->where('int_supp', $kodesup)->get();

        $data = [
            'etd_date' => $dataID->int_etd,
            'ship_name' => $dataID->int_ship,
            'no_bl' => $dataID->int_no_bl,
            'kodesup' => $kodesup,
            'inv' => $inv,
            'int_qty_rcp' => $dataID->int_qty_rcp,
            // data full
            'container' => $containerID,
            'document' => $document,
            'intrans' => $dataAll
        ];

        return view('intrans-transfer.transfer-whs', compact('data'));
    }

    public function transferIntransSave(Request $request)
    {
        if (isset($request->file)) {
            $jumlahFile = count($request->file);
            $request->validate([
                'file.*' => 'required|file|mimes:png,jpg,pdf,xlsx,doc,docx,ppt,pptx|max:4048',
            ]);
        }

        if ($request->submit == 'reject') {
            for ($i = 1; $i <= $request->txtJml; $i++) {
                $noPO = $request->input('hdPO' . $i);
                $inv = $request->input('hdInv' . $i);
                $part = $request->input('hdPart' . $i);

                DB::table('intrans_trans')
                    ->where('int_po', $noPO)
                    ->where('int_inv', $inv)
                    ->where('int_part', $part)
                    ->update([
                        'int_confirm_whs_date' => NOW(),
                        'int_confirm_whs_status' => 'NOK',
                        'int_confirm_whs_ket' => 'REJECT WHS'
                    ]);
            }
            
            alert()->success('Success', 'Rejected Transfer Intrans');
            return redirect()->route('transfer-intrans-view');
        } else {
            for ($i = 1; $i <= $request->txtJml; $i++) {
                $supp       = $request->input('hdSup' . $i);
                $lokasi_to  = $request->input('txtLokasi_to' . $i);
                $inv        = $request->input('hdInv' . $i);
                $noPO       = $request->input('hdPO' . $i);
                $noLine     = $request->input('hdLine' . $i);
                $part       = $request->input('hdPart' . $i);
                $lot        = $request->input('hdLot' . $i);
                // $cek 		= $request->input('txtCek'.$i);
                // $rmk 		= $request->input('txtRmk'.$i);
                $qty_actual     = $request->input('txtActual' . $i);
                $eff        = $request->input('txtEff');
                $shift      = $request->input('txtShift');
                $penerima   = $request->input('txtPenerima');
                $lokasi     = $request->input('txtLokasi');

                $stat         = "BENAR";
                $confirm     = date('m/d/Y');

                // loop update data
                DB::table('intrans_trans')
                    ->where('int_supp', $supp)
                    ->where('int_inv', $inv)
                    ->where('int_po', $noPO)
                    ->where('int_line', $noLine)
                    ->where('int_part', $part)
                    ->where('int_lot', $lot)
                    ->update([
                        // 'int_cek' => $cek,
                        // 'int_rmk' => $rmk,
                        'int_confirm' => $confirm,
                        'int_effdate' => $eff,
                        'int_date_confirm' => NOW(),
                        'int_shift' => $shift,
                        'int_penerima_barang' => $penerima,
                        'int_tanggal_Actual' => $eff,
                        'int_lokasi' => $lokasi,
                        'int_lokasi_to' => $lokasi_to,
                    ]);
            }

            $jumlahFile = count($request->file);
            for ($i = 0; $i < $jumlahFile; $i++) {
                $imageName = $inv . '-' . date('dmY') . $request->file[$i]->getClientOriginalName();
                $request->file[$i]->move(public_path('dok_intrans_whs'), $imageName);

                DB::table('intrans_doc_whs')->insert([
                    'doc_no_invoice' => $inv,
                    'doc_file' => $imageName,
                    'doc_effdate' => NOW(),
                ]);
            }

            $intransSuppAll = DB::select("SELECT int_po, int_inv, int_supp, int_effdate ,int_part, int_qty_rcp, int_line,int_tanggal_Actual,int_lokasi_to FROM intrans_trans WHERE int_supp = '$supp' AND int_inv = '$inv' GROUP BY int_po, int_inv, int_supp, int_effdate ,int_part, int_qty_rcp, int_line,int_tanggal_Actual,int_lokasi_to");

            if ($intransSuppAll != null) {
                foreach ($intransSuppAll as $intransSupp) {
                    $po                = $intransSupp->int_po;
                    $inv            = $intransSupp->int_inv;
                    //$effective	= $intransSupp->int_etd;
                    $effective        = $intransSupp->int_tanggal_Actual;
                    $supp            = $intransSupp->int_supp;
                    $eff            = $intransSupp->int_effdate;
                    $part            = $intransSupp->int_part;
                    $qty_transfer    = $intransSupp->int_qty_rcp;
                    $part_line        = $intransSupp->int_line;
                    $lokasi_to        = $intransSupp->int_lokasi_to;
                    $lokasi_from     = "INTRANS";
                    $no_poInv         = $po . "TI";

                    // send ke inboundJS (skip dulu) dan memakai default
                    $tampil_type_item = prh_hist($po);
                    if($tampil_type_item == false){
                        $typeRC_item = '';
                        $prh_um_conv = 1;
                    }else{
                        foreach($tampil_type_item as $row){
                            if($row['prhPsNbr'] == $inv){ #$inv
                                if($row['prhPart'] == $part){ #$part
                                    if($row['prhLine'] == $part_line){ #$part_line
                                        if(is_array($row['prhType'])){
                                            $typeRC_item = '';
                                        }else{
                                            $typeRC_item = $row['prhType'];
                                        }
                                        $prh_um_conv = $row['prhUmConv'];
                                    }else{
                                        $typeRC_item = '';
                                        $prh_um_conv = 1;
                                    }
                                }else{
                                    $typeRC_item = '';
                                    $prh_um_conv = 1;
                                }
                            }else{
                                $typeRC_item = '';
                                $prh_um_conv = 1;
                            }
                        }
                    }

                    $qty_transfer_whs = $qty_transfer * $prh_um_conv;

                    // dd($intransSupp);
                    $typeRC_item = '';
                    if ($typeRC_item != 'M') {
                        DB::table('intrans_trans')
                            ->where('int_line', $part_line)
                            ->where('int_po', $noPO)
                            ->where('int_inv', $inv)
                            ->where('int_part', $part)
                            ->update([
                                'int_confirm_whs_date' => NOW(),
                                'int_confirm_whs_status' => 'OK',
                                'int_qty_actual' => $qty_transfer_whs
                            ]);
                    } else {
                        DB::table('intrans_trans')
                            ->where('int_line', $part_line)
                            ->where('int_po', $noPO)
                            ->where('int_inv', $inv)
                            ->where('int_part', $part)
                            ->update([
                                'int_confirm_whs_date' => NOW(),
                                'int_confirm_whs_status' => 'NO TRANSFER'
                            ]);
                    }
                }
            }

            alert()->success('Success', 'Confirm Transfer Intrans');
            return redirect()->route('transfer-intrans-view');
        }
    }

    public function transferIntransBarangSave(Request $request)
    {
        if ($request->submit == 'reject') {
            for ($i = 1; $i <= $request->txtJml; $i++) {
                $noPO = $request->input('hdPO' . $i);
                $inv = $request->input('hdInv' . $i);
                $part = $request->input('hdPart' . $i);

                DB::table('intrans_trans')
                    ->where('int_po', $noPO)
                    ->where('int_inv', $inv)
                    ->where('int_part', $part)
                    ->update([
                        'int_confirm_whs_date' => NOW(),
                        'int_confirm_whs_status' => 'NOK',
                        'int_confirm_whs_ket' => 'REJECT WHS'
                    ]);
            }
            alert()->success('Success', 'Rejected Transfer Intrans Barang');
            return redirect()->route('transfer-intrans-view');
        } else {
            for ($i = 1; $i <= $request->txtJml; $i++) {
                $supp       = $request->input('hdSup' . $i);
                $inv        = $request->input('hdInv' . $i);
                $noPO       = $request->input('hdPO' . $i);
                $noLine     = $request->input('hdLine' . $i);
                $part       = $request->input('hdPart' . $i);
                $lot        = $request->input('hdLot' . $i);
                // $cek 		= $request->input('txtCek'.$i);
                // $rmk 		= $request->input('txtRmk'.$i);
                $qty_actual     = $request->input('txtActual' . $i);
                $lokasi_to     = $request->input('lokasi_to' . $i);
                $eff        = $request->input('txtEff');
                $shift      = $request->input('txtShift');
                $penerima   = $request->input('txtPenerima');
                $lokasi     = $request->input('txtLokasi');

                $stat         = "BENAR";
                $confirm     = date('m/d/Y');

                // loop update data
                DB::table('intrans_trans')
                    ->where('int_supp', $supp)
                    ->where('int_inv', $inv)
                    ->where('int_po', $noPO)
                    ->where('int_line', $noLine)
                    ->where('int_part', $part)
                    ->where('int_lot', $lot)
                    ->update([
                        // 'int_cek' => $cek,
                        // 'int_rmk' => $rmk,
                        'int_confirm' => $confirm,
                        'int_effdate' => $eff,
                        'int_date_confirm' => NOW(),
                        'int_shift' => $shift,
                        'int_penerima_barang' => $penerima,
                        'int_tanggal_Actual' => $eff,
                        'int_lokasi' => $lokasi,
                        'int_lokasi_to' => $lokasi_to,
                    ]);
            }

            $intransSuppAll = DB::select("SELECT int_po, int_inv, int_supp, int_effdate ,int_part, int_qty_rcp, int_line,int_tanggal_Actual,int_lokasi_to FROM intrans_trans WHERE int_supp = '$supp' AND int_inv = '$inv' GROUP BY int_po, int_inv, int_supp, int_effdate ,int_part, int_qty_rcp, int_line,int_tanggal_Actual,int_lokasi_to");

            if ($intransSuppAll != null) {
                foreach ($intransSuppAll as $intransSupp) {
                    $po                = $intransSupp->int_po;
                    $inv            = $intransSupp->int_inv;
                    //$effective	= $intransSupp->int_etd;
                    $effective        = $intransSupp->int_tanggal_Actual;
                    $supp            = $intransSupp->int_supp;
                    $eff            = $intransSupp->int_effdate;
                    $part            = $intransSupp->int_part;
                    $qty_transfer    = $intransSupp->int_qty_rcp;
                    $part_line        = $intransSupp->int_line;
                    $lokasi_to        = $intransSupp->int_lokasi_to;
                    $lokasi_from     = "INTRANS";
                    $no_poInv         = $po . "TI";

                    // $typeItem = prh_hist($po);
                    // send ke inboundJS (skip dulu) dan memakai default
                    $prh_um_conv = 1;

                    $qty_transfer_whs = $qty_transfer * $prh_um_conv;

                    DB::table('intrans_trans')
                        ->where('int_line', $part_line)
                        ->where('int_po', $noPO)
                        ->where('int_inv', $inv)
                        ->where('int_part', $part)
                        ->update([
                            'int_confirm_whs_date' => NOW(),
                            'int_confirm_whs_status' => 'NO TRANSFER'
                        ]);
                }
            }

            alert()->success('Success', 'Confirm Transfer Intrans Barang');
            return redirect()->route('transfer-intrans-view');
        }
    }

    public function transferIntransWhsSave(Request $request)
    {
        if (isset($request->file)) {
            $jumlahFile = count($request->file);
            $request->validate([
                'file.*' => 'required|file|mimes:png,jpg,pdf,xlsx,doc,docx,ppt,pptx|max:4048',
            ]);
        }

        if ($request->submit == 'reject') {
            for ($i = 1; $i <= $request->txtJml; $i++) {
                $noPO = $request->input('hdPO' . $i);
                $inv = $request->input('hdInv' . $i);
                $part = $request->input('hdPart' . $i);

                DB::table('intrans_trans')
                    ->where('int_po', $noPO)
                    ->where('int_inv', $inv)
                    ->where('int_part', $part)
                    ->update([
                        'int_confirm_whs_date' => NOW(),
                        'int_confirm_whs_status' => 'NOK',
                        'int_confirm_whs_ket' => 'REJECT WHS'
                    ]);
            }
            alert()->success('Success', 'Rejected Transfer Intrans');
            return redirect()->route('transfer-intrans-view');
        } else {
            for ($i = 1; $i <= $request->txtJml; $i++) {
                $supp       = $request->input('hdSup' . $i);
                $lokasi_to  = $request->input('txtLokasi_to' . $i);
                $inv        = $request->input('hdInv' . $i);
                $noPO       = $request->input('hdPO' . $i);
                $noLine     = $request->input('hdLine' . $i);
                $part       = $request->input('hdPart' . $i);
                $lot        = $request->input('hdLot' . $i);
                // $cek 		= $request->input('txtCek'.$i);
                // $rmk 		= $request->input('txtRmk'.$i);
                $qty_actual     = $request->input('txtActual' . $i);
                $eff        = $request->input('txtEff');
                $shift      = $request->input('txtShift');
                $penerima   = $request->input('txtPenerima');
                $lokasi     = $request->input('txtLokasi');

                $stat         = "BENAR";
                $confirm     = date('m/d/Y');

                // loop update data
                DB::table('intrans_trans')
                    ->where('int_supp', $supp)
                    ->where('int_inv', $inv)
                    ->where('int_po', $noPO)
                    ->where('int_line', $noLine)
                    ->where('int_part', $part)
                    ->where('int_lot', $lot)
                    ->update([
                        // 'int_cek' => $cek,
                        // 'int_rmk' => $rmk,
                        'int_confirm' => $confirm,
                        'int_effdate' => $eff,
                        'int_date_confirm' => NOW(),
                        'int_shift' => $shift,
                        'int_penerima_barang' => $penerima,
                        'int_tanggal_Actual' => $eff,
                        'int_lokasi' => $lokasi,
                        'int_lokasi_to' => $lokasi_to,
                    ]);
            }

            $jumlahFile = count($request->file);
            for ($i = 0; $i < $jumlahFile; $i++) {
                $imageName = $inv . '-' . date('dmY') . $request->file[$i]->getClientOriginalName();
                $request->file[$i]->move(public_path('dok_intrans_whs'), $imageName);

                DB::table('intrans_doc_whs')->insert([
                    'doc_no_invoice' => $inv,
                    'doc_file' => $imageName,
                    'doc_effdate' => NOW(),
                ]);
            }

            $intransSuppAll = DB::select("SELECT int_po, int_inv, int_supp, int_effdate ,int_part, int_qty_rcp, int_line,int_tanggal_Actual,int_lokasi_to FROM intrans_trans WHERE int_supp = '$supp' AND int_inv = '$inv' GROUP BY int_po, int_inv, int_supp, int_effdate ,int_part, int_qty_rcp, int_line,int_tanggal_Actual,int_lokasi_to");

            if ($intransSuppAll != null) {
                foreach ($intransSuppAll as $intransSupp) {
                    $po                = $intransSupp->int_po;
                    $inv            = $intransSupp->int_inv;
                    //$effective	= $intransSupp->int_etd;
                    $effective        = $intransSupp->int_tanggal_Actual;
                    $supp            = $intransSupp->int_supp;
                    $eff            = $intransSupp->int_effdate;
                    $part            = $intransSupp->int_part;
                    $qty_transfer    = $intransSupp->int_qty_rcp;
                    $no_lot         = $intransSupp->int_lot;
                    $part_line        = $intransSupp->int_line;
                    $lokasi_to        = $intransSupp->int_lokasi_to;
                    $lokasi_from     = "INTRANS";
                    $no_poInv         = $po . "TI";

                    // send ke inboundJS (skip dulu) dan memakai default
                    $tampil_type_item = prh_hist($po);
                    if($tampil_type_item == false){
                        $typeRC_item = '';
                        $prh_um_conv = 1;
                    }else{
                        foreach($tampil_type_item as $row){
                            if($row['prhPsNbr'] == $inv){ #$inv
                                if($row['prhPart'] == $part){ #$part
                                    if($row['prhLine'] == $part_line){ #$part_line
                                        if(is_array($row['prhType'])){
                                            $typeRC_item = '';
                                        }else{
                                            $typeRC_item = $row['prhType'];
                                        }
                                        $prh_um_conv = $row['prhUmConv'];
                                    }else{
                                        $typeRC_item = '';
                                        $prh_um_conv = 1;
                                    }
                                }else{
                                    $typeRC_item = '';
                                    $prh_um_conv = 1;
                                }
                            }else{
                                $typeRC_item = '';
                                $prh_um_conv = 1;
                            }
                        }
                    }

                    $qty_transfer_whs = $qty_transfer * $prh_um_conv;

                    // dd($intransSupp);
                    $typeRC_item = '';
                    if ($typeRC_item != 'M') {
                        DB::table('intrans_trans')
                            ->where('int_line', $part_line)
                            ->where('int_po', $noPO)
                            ->where('int_inv', $inv)
                            ->where('int_part', $part)
                            ->update([
                                'int_confirm_whs_date' => NOW(),
                                'int_confirm_whs_status' => 'OK',
                                'int_qty_actual' => $qty_transfer_whs
                            ]);
                    } else {
                        DB::table('intrans_trans')
                            ->where('int_line', $part_line)
                            ->where('int_po', $noPO)
                            ->where('int_inv', $inv)
                            ->where('int_part', $part)
                            ->update([
                                'int_confirm_whs_date' => NOW(),
                                'int_confirm_whs_status' => 'NO TRANSFER'
                            ]);
                    }
                }
            }
            alert()->success('Success', 'Confirm Transfer Intrans WHS');
            return redirect()->route('transfer-intrans-view');
        }
    }

    public function printelpb(Request $request, $kodesup, $inv)
    {
        $kodesup = base64_decode($kodesup);
        $inv = base64_decode($inv);
        $data = [];
        $ctAdName = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr', $kodesup)->first();
        if ($ctAdName != null) {
            $ctAdName = $ctAdName->ct_ad_name;
        } else {
            $ctAdName = '';
        }

        $getALl = DB::select("SELECT DISTINCT int_po FROM intrans_trans WHERE int_supp = '$kodesup' AND int_inv = '$inv'");
        $no = 0;
        foreach ($getALl as $row) {
            $data[$no]['int_po'] = $row->int_po;
            $noRcALL = prh_hist($row->int_po);
            if ($noRcALL == false) {
                $data[$no]['noRC'] = '';
                if ($data[$no]['noRC'] == '') {
                    $data[$no]['noRC'] = 'false';
                }
            } else {
                $data[$no]['noRC'] = '';
                foreach ($noRcALL as $nor) {
                    if ($nor['prhReceiver'] != '' && $nor['prhReceiver'] != '') {
                        if ($data[$no]['noRC'] == '') {
                            $data[$no]['noRC'] = $nor['prhReceiver'];
                        }
                    }
                }
                if ($data[$no]['noRC'] == '') {
                    $data[$no]['noRC'] = 'false';
                }
            }
            $no++;
        }

        return view('intrans-transfer.list-print', ['data' => $data, 'kode_supp' => $kodesup, 'inv' => $inv, 'ctAdName' => $ctAdName]);
    }

    public function preview(Request $request, $kodesup, $rc, $int_po)
    {
        $kodesup = base64_decode($kodesup);
        $rc = base64_decode($rc);
        $int_po = base64_decode($int_po);

        $tglReceiver = tanggal_TrHist($rc);
        $tglReceiver = strtotime($tglReceiver);
        $tglReceiver = date('d-m-Y', $tglReceiver);

        $rs_tampil_pemakai = [];
        $supp = auth()->user()->kode_supplier;
        // $supp = 'ST0003U';
        if ($supp == '') {
            $receiverAll = get_Prh_HistByReceiver($rc);
            $no = 0;
            foreach ($receiverAll as $receiver) {
                $po_detail = po_detail($int_po);
                foreach ($po_detail as $po_det) {
                    if ($receiver['prhNbr'] == $po_det['no_po']) {
                        if ($receiver['prhPart'] == $po_det['item_number']) {
                            if ($receiver['prhLine'] == $po_det['line']) {
                                $rs_tampil_pemakai['prhNbr'] = $receiver['prhNbr'];
                                $rs_tampil_pemakai['prhPart'] = $receiver['prhPart'];
                                $rs_tampil_pemakai['prhLine'] = $receiver['prhLine'];
                                $rs_tampil_pemakai['prhRcvd'] = $receiver['prhRcvd'];
                                $rs_tampil_pemakai['prhReceiver'] = $receiver['prhReceiver'];
                                $rs_tampil_pemakai['prhRcpDate'] = $receiver['prhRcpDate'];
                                $rs_tampil_pemakai['prhVend'] = $receiver['prhVend'];
                                $rs_tampil_pemakai['podRequest'] = $receiver['podRequest'];
                                $rs_tampil_pemakai['podLoc'] = $receiver['podLoc'];
                                $rs_tampil_pemakai['pod_ReqNbr'] = $receiver['pod_ReqNbr'];
                                $rs_tampil_pemakai['prhPsNbr'] = $receiver['prhPsNbr'];
                                $rs_tampil_pemakai['prhUm'] = $receiver['prhUm'];
                                $rs_tampil_pemakai['prhPrint'] = $receiver['prhPrint'];
                                $rs_tampil_pemakai['prhLog01'] = $receiver['prhLog01'];
                            }
                        }
                    }
                }
                $no++;
            }
        }else{
            $receiverAll = get_Prh_HistByReceiver($rc);
            $no = 0;
            foreach ($receiverAll as $receiver) {
                $po_detail = po_detail($int_po);
                foreach ($po_detail as $po_det) {
                    if ($receiver['prhNbr'] == $po_det['no_po']) {
                        if ($receiver['prhPart'] == $po_det['item_number']) {
                            if ($receiver['prhLine'] == $po_det['line']) {
                                $rs_tampil_pemakai['prhNbr'] = $receiver['prhNbr'];
                                $rs_tampil_pemakai['prhPart'] = $receiver['prhPart'];
                                $rs_tampil_pemakai['prhLine'] = $receiver['prhLine'];
                                $rs_tampil_pemakai['prhRcvd'] = $receiver['prhRcvd'];
                                $rs_tampil_pemakai['prhReceiver'] = $receiver['prhReceiver'];
                                $rs_tampil_pemakai['prhRcpDate'] = $receiver['prhRcpDate'];
                                $rs_tampil_pemakai['prhVend'] = $receiver['prhVend'];
                                $rs_tampil_pemakai['podRequest'] = $receiver['podRequest'];
                                $rs_tampil_pemakai['podLoc'] = $receiver['podLoc'];
                                $rs_tampil_pemakai['pod_ReqNbr'] = $receiver['pod_ReqNbr'];
                                $rs_tampil_pemakai['prhPsNbr'] = $receiver['prhPsNbr'];
                                $rs_tampil_pemakai['prhUm'] = $receiver['prhUm'];
                                $rs_tampil_pemakai['prhPrint'] = $receiver['prhPrint'];
                                $rs_tampil_pemakai['prhLog01'] = $receiver['prhLog01'];
                            }
                        }
                    }
                }
                $no++;
            }
        }

        $rs_tampil_supplier = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr', $kodesup)->first();

        // dd($rs_tampil_pemakai['prhPsNbr']);

        $rs_tampil_transaksi = collect(DB::select("SELECT distinct portald_tr_id FROM portald_det JOIN portal_mstr ON portal_mstr.portal_tr_id = portald_det.portald_tr_id WHERE portal_mstr.portal_ps_nbr = '$rs_tampil_pemakai[prhPsNbr]' AND portald_det.portald_po_nbr = '$rs_tampil_pemakai[prhNbr]'"))->first();

        if($rs_tampil_transaksi == null){
            $portald_tr_id = '-';
        }else{
            $portald_tr_id = $rs_tampil_transaksi->portald_tr_id;
        }

        $receiverTgl = strtotime($rs_tampil_pemakai['prhRcpDate']);        

        $data = [
            'transaksi'     => $portald_tr_id,
			'supp_id'       => $rs_tampil_supplier->ct_vd_addr,	
			'supp_name'     => $rs_tampil_supplier->ct_ad_name,
			'alamat1'       => $rs_tampil_supplier->ct_ad_line1,
			'alamat2'       => $rs_tampil_supplier->ct_ad_line2,
			'po_nbr'        => $rs_tampil_pemakai['prhNbr'], #1
			'pemakai'       => $rs_tampil_pemakai['podRequest'], #3
			'lokasi'        => $rs_tampil_pemakai['podLoc'], #4
			'pp'            => $rs_tampil_pemakai['pod_ReqNbr'], #5
			'receiver'      => $rs_tampil_pemakai['prhReceiver'], #6
			'receiver_tgl'  => date('d-m-Y', $receiverTgl), #7
			'print_status'  => $rs_tampil_pemakai['prhPrint'], #8
			'print_status1' => $rs_tampil_pemakai['prhLog01'], #9
            'tgl_input' => $tglReceiver
        ];

        // batas data header dan body

        if ($supp == '') {
            $receiverAll = get_Prh_HistByReceiver($rc);
            $no = 0;
            $rs_tampil_data_rc = [];
            foreach ($receiverAll as $receiver) {
                $po_detail = po_detail($int_po);
                foreach ($po_detail as $po_det) {
                    if ($receiver['prhNbr'] == $po_det['no_po']) {
                        if ($receiver['prhPart'] == $po_det['item_number']) {
                            if ($receiver['prhLine'] == $po_det['line']) {
                                $rs_tampil_data_rc[$no]['prh_nbr'] = $receiver['prhNbr'];
                                $rs_tampil_data_rc[$no]['prh_part'] = $receiver['prhPart'];
                                $rs_tampil_data_rc[$no]['prh_line'] = $receiver['prhLine'];
                                $rs_tampil_data_rc[$no]['prh_rcvd'] = $receiver['prhRcvd'];
                                $rs_tampil_data_rc[$no]['prh_receiver'] = $receiver['prhReceiver'];
                                $rs_tampil_data_rc[$no]['prh_rcp_date'] = $receiver['prhRcpDate'];
                                $rs_tampil_data_rc[$no]['prh_vend'] = $receiver['prhVend'];
                                $rs_tampil_data_rc[$no]['pod_request'] = $receiver['podRequest'];
                                $rs_tampil_data_rc[$no]['pod_loc'] = $receiver['podLoc'];
                                $rs_tampil_data_rc[$no]['pod_req_nbr'] = $receiver['pod_ReqNbr'];
                                $rs_tampil_data_rc[$no]['prh_ps_nbr'] = $receiver['prhPsNbr'];
                                $rs_tampil_data_rc[$no]['prh_um'] = $receiver['prhUm'];
                                $rs_tampil_data_rc[$no]['prhPrint'] = $receiver['prhPrint'];
                                $rs_tampil_data_rc[$no]['prhLog01'] = $receiver['prhLog01'];
                            }
                        }
                    }
                }
                $no++;
            }

            $rs_hitung_data_rc = count($rs_tampil_data_rc); #data1
        }else{
            $receiverAll = get_Prh_HistByReceiver($rc);
            $no = 0;
            $rs_tampil_data_rc = [];
            foreach ($receiverAll as $receiver) {
                $po_detail = po_detail($int_po);
                foreach ($po_detail as $po_det) {
                    if ($receiver['prhNbr'] == $po_det['no_po']) {
                        if ($receiver['prhPart'] == $po_det['item_number']) {
                            if ($receiver['prhLine'] == $po_det['line']) {
                                $rs_tampil_data_rc[$no]['prh_nbr'] = $receiver['prhNbr'];
                                $rs_tampil_data_rc[$no]['prh_part'] = $receiver['prhPart'];
                                $rs_tampil_data_rc[$no]['prh_line'] = $receiver['prhLine'];
                                $rs_tampil_data_rc[$no]['prh_rcvd'] = $receiver['prhRcvd'];
                                $rs_tampil_data_rc[$no]['prh_receiver'] = $receiver['prhReceiver'];
                                $rs_tampil_data_rc[$no]['prh_rcp_date'] = $receiver['prhRcpDate'];
                                $rs_tampil_data_rc[$no]['prh_vend'] = $receiver['prhVend'];
                                $rs_tampil_data_rc[$no]['pod_request'] = $receiver['podRequest'];
                                $rs_tampil_data_rc[$no]['pod_loc'] = $receiver['podLoc'];
                                $rs_tampil_data_rc[$no]['pod_req_nbr'] = $receiver['pod_ReqNbr'];
                                $rs_tampil_data_rc[$no]['prh_ps_nbr'] = $receiver['prhPsNbr'];
                                $rs_tampil_data_rc[$no]['prh_um'] = $receiver['prhUm'];
                                $rs_tampil_data_rc[$no]['prhPrint'] = $receiver['prhPrint'];
                                $rs_tampil_data_rc[$no]['prhLog01'] = $receiver['prhLog01'];
                            }
                        }
                    }
                }
                $no++;
            }

            $rs_hitung_data_rc = count($rs_tampil_data_rc); #data1
        }

        return view('intrans-transfer/preview', compact('data', 'rs_tampil_data_rc', 'rs_hitung_data_rc', 'int_po'));
    }

    public function print(Request $request, $kodesup, $rc, $int_po)
    {
        $kodesup = base64_decode($kodesup);
        $rc = base64_decode($rc);
        $int_po = base64_decode($int_po);

        $tglReceiver = tanggal_TrHist($rc);
        $tglReceiver = strtotime($tglReceiver);
        $tglReceiver = date('d-m-Y', $tglReceiver);

        $rs_tampil_pemakai = [];
        $supp = auth()->user()->kode_supplier;
        if ($supp == '') {
            $receiverAll = get_Prh_HistByReceiver($rc);
            $no = 0;
            foreach ($receiverAll as $receiver) {
                $po_detail = po_detail($int_po);
                foreach ($po_detail as $po_det) {
                    if ($receiver['prhNbr'] == $po_det['no_po']) {
                        if ($receiver['prhPart'] == $po_det['item_number']) {
                            if ($receiver['prhLine'] == $po_det['line']) {
                                $rs_tampil_pemakai['prhNbr'] = $receiver['prhNbr'];
                                $rs_tampil_pemakai['prhPart'] = $receiver['prhPart'];
                                $rs_tampil_pemakai['prhLine'] = $receiver['prhLine'];
                                $rs_tampil_pemakai['prhRcvd'] = $receiver['prhRcvd'];
                                $rs_tampil_pemakai['prhReceiver'] = $receiver['prhReceiver'];
                                $rs_tampil_pemakai['prhRcpDate'] = $receiver['prhRcpDate'];
                                $rs_tampil_pemakai['prhVend'] = $receiver['prhVend'];
                                $rs_tampil_pemakai['podRequest'] = $receiver['podRequest'];
                                $rs_tampil_pemakai['podLoc'] = $receiver['podLoc'];
                                $rs_tampil_pemakai['pod_ReqNbr'] = $receiver['pod_ReqNbr'];
                                $rs_tampil_pemakai['prhPsNbr'] = $receiver['prhPsNbr'];
                                $rs_tampil_pemakai['prhUm'] = $receiver['prhUm'];
                                $rs_tampil_pemakai['prhPrint'] = $receiver['prhPrint'];
                                $rs_tampil_pemakai['prhLog01'] = $receiver['prhLog01'];
                            }
                        }
                    }
                }
                $no++;
            }
        }else{
            $receiverAll = get_Prh_HistByReceiver($rc);
            $no = 0;
            foreach ($receiverAll as $receiver) {
                $po_detail = po_detail($int_po);
                foreach ($po_detail as $po_det) {
                    if ($receiver['prhNbr'] == $po_det['no_po']) {
                        if ($receiver['prhPart'] == $po_det['item_number']) {
                            if ($receiver['prhLine'] == $po_det['line']) {
                                $rs_tampil_pemakai['prhNbr'] = $receiver['prhNbr'];
                                $rs_tampil_pemakai['prhPart'] = $receiver['prhPart'];
                                $rs_tampil_pemakai['prhLine'] = $receiver['prhLine'];
                                $rs_tampil_pemakai['prhRcvd'] = $receiver['prhRcvd'];
                                $rs_tampil_pemakai['prhReceiver'] = $receiver['prhReceiver'];
                                $rs_tampil_pemakai['prhRcpDate'] = $receiver['prhRcpDate'];
                                $rs_tampil_pemakai['prhVend'] = $receiver['prhVend'];
                                $rs_tampil_pemakai['podRequest'] = $receiver['podRequest'];
                                $rs_tampil_pemakai['podLoc'] = $receiver['podLoc'];
                                $rs_tampil_pemakai['pod_ReqNbr'] = $receiver['pod_ReqNbr'];
                                $rs_tampil_pemakai['prhPsNbr'] = $receiver['prhPsNbr'];
                                $rs_tampil_pemakai['prhUm'] = $receiver['prhUm'];
                                $rs_tampil_pemakai['prhPrint'] = $receiver['prhPrint'];
                                $rs_tampil_pemakai['prhLog01'] = $receiver['prhLog01'];
                            }
                        }
                    }
                }
                $no++;
            }
        }

        $rs_tampil_supplier = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr', $kodesup)->first();

        $rs_tampil_transaksi = collect(DB::select("SELECT distinct portald_tr_id FROM portald_det JOIN portal_mstr ON portal_mstr.portal_tr_id = portald_det.portald_tr_id WHERE portal_mstr.portal_ps_nbr = '$rs_tampil_pemakai[prhPsNbr]' AND portald_det.portald_po_nbr = '$rs_tampil_pemakai[prhNbr]'"))->first();

        if($rs_tampil_transaksi == null){
            $portald_tr_id = '-';
        }else{
            $portald_tr_id = $rs_tampil_transaksi->portald_tr_id;
        }

        $receiverTgl = strtotime($rs_tampil_pemakai['prhRcpDate']);        

        $data = [
            'transaksi'     => $portald_tr_id,
			'supp_id'       => $rs_tampil_supplier->ct_vd_addr,	
			'supp_name'     => $rs_tampil_supplier->ct_ad_name,
			'alamat1'       => $rs_tampil_supplier->ct_ad_line1,
			'alamat2'       => $rs_tampil_supplier->ct_ad_line2,
			'po_nbr'        => $rs_tampil_pemakai['prhNbr'], #1
			'pemakai'       => $rs_tampil_pemakai['podRequest'], #3
			'lokasi'        => $rs_tampil_pemakai['podLoc'], #4
			'pp'            => $rs_tampil_pemakai['pod_ReqNbr'], #5
			'receiver'      => $rs_tampil_pemakai['prhReceiver'], #6
			'receiver_tgl'  => date('d-m-Y', $receiverTgl), #7
			'print_status'  => $rs_tampil_pemakai['prhPrint'], #8
			'print_status1' => $rs_tampil_pemakai['prhLog01'], #9
            'tgl_input' => $tglReceiver
        ];

        // batas data header dan body

        if ($supp == '') {
            $receiverAll = get_Prh_HistByReceiver($rc);
            $no = 0;
            $rs_tampil_data_rc = [];
            foreach ($receiverAll as $receiver) {
                $po_detail = po_detail($int_po);
                foreach ($po_detail as $po_det) {
                    if ($receiver['prhNbr'] == $po_det['no_po']) {
                        if ($receiver['prhPart'] == $po_det['item_number']) {
                            if ($receiver['prhLine'] == $po_det['line']) {
                                $rs_tampil_data_rc[$no]['prh_nbr'] = $receiver['prhNbr'];
                                $rs_tampil_data_rc[$no]['prh_part'] = $receiver['prhPart'];
                                $rs_tampil_data_rc[$no]['prh_line'] = $receiver['prhLine'];
                                $rs_tampil_data_rc[$no]['prh_rcvd'] = $receiver['prhRcvd'];
                                $rs_tampil_data_rc[$no]['prh_receiver'] = $receiver['prhReceiver'];
                                $rs_tampil_data_rc[$no]['prh_rcp_date'] = $receiver['prhRcpDate'];
                                $rs_tampil_data_rc[$no]['prh_vend'] = $receiver['prhVend'];
                                $rs_tampil_data_rc[$no]['pod_request'] = $receiver['podRequest'];
                                $rs_tampil_data_rc[$no]['pod_loc'] = $receiver['podLoc'];
                                $rs_tampil_data_rc[$no]['pod_req_nbr'] = $receiver['pod_ReqNbr'];
                                $rs_tampil_data_rc[$no]['prh_ps_nbr'] = $receiver['prhPsNbr'];
                                $rs_tampil_data_rc[$no]['prh_um'] = $receiver['prhUm'];
                                $rs_tampil_data_rc[$no]['prhPrint'] = $receiver['prhPrint'];
                                $rs_tampil_data_rc[$no]['prhLog01'] = $receiver['prhLog01'];
                            }
                        }
                    }
                }
                $no++;
            }

            $rs_hitung_data_rc = count($rs_tampil_data_rc); #data1
        }else{
            $receiverAll = get_Prh_HistByReceiver($rc);
            $no = 0;
            $rs_tampil_data_rc = [];
            foreach ($receiverAll as $receiver) {
                $po_detail = po_detail($int_po);
                foreach ($po_detail as $po_det) {
                    if ($receiver['prhNbr'] == $po_det['no_po']) {
                        if ($receiver['prhPart'] == $po_det['item_number']) {
                            if ($receiver['prhLine'] == $po_det['line']) {
                                $rs_tampil_data_rc[$no]['prh_nbr'] = $receiver['prhNbr'];
                                $rs_tampil_data_rc[$no]['prh_part'] = $receiver['prhPart'];
                                $rs_tampil_data_rc[$no]['prh_line'] = $receiver['prhLine'];
                                $rs_tampil_data_rc[$no]['prh_rcvd'] = $receiver['prhRcvd'];
                                $rs_tampil_data_rc[$no]['prh_receiver'] = $receiver['prhReceiver'];
                                $rs_tampil_data_rc[$no]['prh_rcp_date'] = $receiver['prhRcpDate'];
                                $rs_tampil_data_rc[$no]['prh_vend'] = $receiver['prhVend'];
                                $rs_tampil_data_rc[$no]['pod_request'] = $receiver['podRequest'];
                                $rs_tampil_data_rc[$no]['pod_loc'] = $receiver['podLoc'];
                                $rs_tampil_data_rc[$no]['pod_req_nbr'] = $receiver['pod_ReqNbr'];
                                $rs_tampil_data_rc[$no]['prh_ps_nbr'] = $receiver['prhPsNbr'];
                                $rs_tampil_data_rc[$no]['prh_um'] = $receiver['prhUm'];
                                $rs_tampil_data_rc[$no]['prhPrint'] = $receiver['prhPrint'];
                                $rs_tampil_data_rc[$no]['prhLog01'] = $receiver['prhLog01'];
                            }
                        }
                    }
                }
                $no++;
            }

            $rs_hitung_data_rc = count($rs_tampil_data_rc); #data1
        }

        return view('intrans-transfer/print', compact('data', 'rs_tampil_data_rc', 'rs_hitung_data_rc', 'int_po'));
    }
}
