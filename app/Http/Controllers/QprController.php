<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Exports\CounterMeasureExport;
use Maatwebsite\Excel\Facades\Excel;
use App\sideMenu;
use App\PortalDet;
use App\SOAPPoDetail;
use App\BusinesRelation;
use App\SOAPPubBusinessRelation;
use App\Mail\emailResult;
use App\Mail\sendEmailApproval;
use App\Ptmstr;
use App\SOAPPtMstr;
use Illuminate\Support\Facades\Mail;
use App\Mail\MainCounterMeasureSupplier;
use App\Mail\sentEmailVendorReminder;
use App\Mail\sentEmailVendorReanswer;
use App\PartQc;
use App\QPRMaster;
use App\QPRMsSjPrc;
use App\QPRDtSjPrc;
use App\QPRSortir;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class QprController extends Controller
{
    public function index()
    {

        return view('QPR.index');
    }

    public function showqrbarcode(Request $request)
    {

        $transid = $request->get('idtrans');

        $tableLine = DB::table('portald_det as pd')
                    ->select('pd.portald_part','pd.portald_qty_ship','pd.portald_po_nbr','pd.portald_line',
                            'pd.portald_um','pd.portald__int07','pm.portal_vend','pd.portald_confirm_rcvd','pd.portald__date01','pd.portald_no_dn','pm.portal_dlv_date','pm.portal_tr_id')
                    ->leftJoin('portal_mstr as pm', 'pd.portald_tr_id','=','pm.portal_tr_id')
                    ->where('pd.portald_tr_id', $transid)
                    ->get();

        $dNnumber = $tableLine[0]->portald_no_dn;
        $packingSlip = $tableLine[0]->portal_tr_id;
        $po = $tableLine[0]->portald_po_nbr;
        $line = $tableLine[0]->portald_line;
        $deliveryDate = $tableLine[0]->portal_dlv_date;

        $data_po = po_detail($po);
        $html = '';
        $desc = [];
        $urut = 1;
        foreach ($tableLine as $key) {

            $ptmstr = pt_mstr($key->portald_part);
            //cari desk lama
            $deskripsi_q = DB::table('pub_xxitem_mapping')->where('xxnew_part',$key->portald_part)->first();
            if(empty($deskripsi_q->xxold_desc1)){
                $desk_old = '-';
            }else{
                $desk_old = $deskripsi_q->xxold_desc1;
            }

            $html .= '
            <tr>
                <td id="tLine">'.$key->portald_line.'</td>
                <td id="tItem">'.$key->portald_part.'</td>';
            if (empty($ptmstr['deskripsi1'])) {
                $html .= '<td> - </td>';
            }else{
                $html .= '<td>'.$ptmstr['deskripsi1'].'</td>';
            }
            $html .= '<td>'.$desk_old.'</td>';
            if (empty($key->portald_um)) {
                $html .= '<td> - </td>';
            }else{
                $html .= '<td>'.$key->portald_um.'</td>';
            }
            if (empty($key->portald_qty_ship)) {
                $html .= '<td> 0 </td>';
            }else{
                $html .= '<td id="qtyreceived">'.number_format($key->portald_qty_ship).'</td>';
            }
            $html .= '<td width="5%"><input type="hidden" class="form-control text-dark" id="dataConfirm" value="'.$key->portald__int07.'">OK</td>';
            if (empty($key->portald_confirm_rcvd)) {
                $html .='<td width="8%"><input type="text" class="form-control text-dark" id="listConfirm['.$urut.']" value="'.$key->portald_qty_ship.'" readonly></td>';
            }else{
                $html .= '<td id="qtyConfirm['.$urut.']" width="8%"><input type="text" class="form-control text-dark" id="listConfirm['.$urut.']" value="'.$key->portald_confirm_rcvd.'" readonly></td>';
            }
            if (empty($key->portald__int07)) {
                $html .= '<td width="10%"><input type="text" class="form-control form-control-sm text-dark" onchange="changeQtyConfirm('.$urut.')" id="listNG['.$urut.']"></td>';
            }else{
                $html .= '<td width="10%"><input type="text" class="form-control form-control-sm text-dark" onchange="changeQtyConfirm('.$urut.')" id="listNG['.$urut.']"></td>';
            }
            $html .='
            <td> </td>
            <td> </td>
            <td> </td>
            <input type="hidden" name="line[]" id="line[]" value="'.$key->portald_line.'">
            <input type="hidden" name="part[]" id="part[]" value="'.$key->portald_part.'">
            <td><a href="#">Click</a></td>
            <td><input type="text" class="form-control form-control-sm text-dark" name="tremark" id="remark"></td>
            </tr>';
            $urut++;
        }

        $data = [
            'tableLine'     => $tableLine,
            'dNnumber'      => $dNnumber,
            'po'            => $po,
            'html'          => $html,
            'desc'          => $desc,
            'deliveryDate'  => $deliveryDate,
            'packingslip'   => $packingSlip,
            'jumlahData'    => $tableLine->count()
        ];

        return response()->json($data);
    }

    public function storeItemCheckQc(Request $request) {

        $part       = $request->get('part');
        $supp       = $request->get('supp');
        $check      = $request->get('check');
        $frek       = $request->get('frek');
        $jumlah     = $request->get('Dtotal');

        DB::table('part_qc')
        ->update([
            'qc_check'      => '',
            // 'qc_supplier'   => $supp[0];
        ]);

        for ($i=0; $i < $jumlah ; $i++) {
            if($check[$i] == 'on'){
                DB::table('part_qc')->where('qc_part',$part[$i])->where('qc_supplier',$supp[$i])
                ->update([
                    'qc_check'      => 'C',
                    'qc_check_frek' => $frek[$i]
                ]);
            }
        }

        $data = 'Success';

        return response()->json($data);
    }

    public function storeqcbarcode(Request $request)
    {

        $transid = $request->get('transid');

        if ($transid != '') {

                $line       = $request->get('tline');
                $item       = $request->get('titem');
                $qty        = $request->get('qty');
                $part       = $request->get('part');
                $qty_qc     = $request->get('qtyConfirm');
                $remark     = $request->get('description');
                $dataTotal  = $request->get('dataTotal');
                // dd($part);
                // die;
            for ($i=0; $i < $dataTotal ; $i++) {

                // $totalItem = DB::table('portald_det')
                //     ->select('portald_tr_id')
                //     ->where('portald_tr_id', $transid)
                //     ->where('portald_part', $part[$i])
                //     // ->where('portald_line', $line)
                //     ->count();

                // if ($totalItem != 0) {

                    $updateBarcode = DB::table('portald_det')
                        ->where('portald_tr_id', $transid)
                        ->where('portald_part', $part[$i])
                        ->where('portald_line', $line[$i])
                        ->update(array(
                            // 'portald_qty_ship' => $qty_qc,
                            'portald_confirm_rcvd' => $qty[$i],
                            'portald__dec01' => 0,
                            'portald_remark' => $remark,
                            'portald__date01' => Carbon::today()
                        ));
                // }
            }

            $data = 'Success';
        } else {
            $data = 'Failed';
        }

        return response()->json($data);
    }

    public function itemcheckqc()
    {
        $data = DB::select("SELECT DISTINCT qc_part,qc_supplier,qc_check,qc_check_frek FROM part_qc ORDER BY qc_part ASC");
        $html = '';
        if(count($data) == 0){
            $html .= '';
        }else{
            $urut = 1;
            foreach($data as $d){
                $part_m = pt_mstr($d->qc_part);
                $xx = DB::table('pub_xxitem_mapping')->where('xxnew_part',$d->qc_part)->first();
                $s = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr',$d->qc_supplier)->first();
                if(empty($xx->xxold_desc1)){
                    $deskk1 = '-';
                }else{
                    $deskk1 = $xx->xxold_desc1;
                }

                $check = $d->qc_check;
                $frek = $d->qc_check_frek;
                if($frek == 'H'){
                    $gl1 = 'selected="selected"';
                }else{
                    $gl1 = '';
                }
                if($frek == 'M'){
                    $gl2 = 'selected="selected"';
                }else{
                    $gl2 = '';
                }
                if($frek == 'B'){
                    $gl3 = 'selected="selected"';
                }else{
                    $gl3 = '';
                }
                if($check=="C") { $ch = 'checked'; $val_check = 'on'; }else{ $ch = '';  $val_check = 'off';}
                $part = $d->qc_part;
                if(empty($part_m['deskripsi2'])){
                    $dsk2 = '-';
                }else{
                    $dsk2 = $part_m['deskripsi2'];
                }
                $html .= '
                <tr>
                    <td>'.$part.'</td>
                    <td>'.$part_m['deskripsi1'].'</td>
                    <td>'.$dsk2.'</td>
                    <td>'.$deskk1.'</td>
                    <td>'.$part_m['um'].'</td>
                    <td>'.$d->qc_supplier.'</td>
                    <td>'.$s->ct_ad_name.'</td>
                    <input type="hidden" name="jml_data" id="jml_data" value="'.count($data).'">
                    <input type="hidden" name="part[]" id="part[]" value="'.$d->qc_part.'">
                    <input type="hidden" name="supp[]" id="supp[]" value="'.$d->qc_supplier.'">
                    <td><input type="checkbox" name="checkqc['.$urut.']" id="checkqc['.$urut.']" onclick="checkcok('.$urut.')" value="'.$val_check.'" '.$ch.'></td>
                    <td>
                        <select name="frek[]" id="frek[]" class="form-control" style="color:black">
                            <option value=""></option>
                            <option value="H" '.$gl1.'>Harian</option>
                            <option value="M" '.$gl2.'>Mingguan</option>
                            <option value="B" '.$gl3.'>Bulanan</option>
                        </select>
                    </td>
                    <td><a class="btn btn-danger"id="btn_hps" onclick="hapus_part_qc(\''.$part.'\',\''.$d->qc_supplier.'\')" title="Delete"><i class="fas fa-trash-alt" style="color:white"></i></a></td>
                </tr>
                ';
                $urut ++;
            }
        }

        return view('QPR.item_check',compact('html'));
    }

    public function modaladditemcheck()
    {

        $data = SOAPPubBusinessRelation::all();

        return  view('QPR.modal_add_item_check', compact('data'));
    }

    public function cari_item_master(Request $request){
        $item = $request->get('numItem');
        if($item != ""){
            $pt = pt_mstr_bydesc($item);
        }else{
            $pt = pt_mstr_bydesc('');
        }
        $html = '';
        if($pt == false){

        }else{
            $hitung = count($pt);
            if($hitung == 0){
                $html .= '';
            }else{
                $urut =1;
                foreach($pt as $p){
                    if(empty($p['deskripsi2'])){
                        $des2 = '-';
                    }else{
                        $des2 = $p['deskripsi2'];
                    }
                    $html .= '
                    <tr>
                        <td>'.$p['item_number'].'</td>
                        <td>'.$p['deskripsi1'].'</td>
                        <td>'.$des2.'</td>
                        <td>'.$p['um'].'</td>
                        <td></td>
                        <td></td>
                        <td><input type="checkbox" name="checkqc['.$urut.']" id="checkqc['.$urut.']" onclick="checkedf('.$urut.')" value="off"></td>
                        <input type="hidden" name="part[]" id="part[]" value="'.$p['item_number'].'">
                        <input type="hidden" name="jml" id="jml" value="'.$hitung.'">
                    </tr>
                    ';
                    $urut++;
                }
            }

        }
        return response($html);

    }

    public function searchDataItemCheck(Request $request){

        $itemNum = $request->get('numItem');
        $idsupp = $request->get('suppID');
        if($itemNum != "" && $idsupp != ""){
            $data = DB::select("SELECT DISTINCT qc_part,qc_supplier,qc_check,qc_check_frek FROM part_qc WHERE qc_part = '$itemNum' AND qc_supplier = '$idsupp' ORDER BY qc_part");
        }else if($itemNum == "" && $idsupp != ""){
            $data = DB::select("SELECT DISTINCT qc_part,qc_supplier,qc_check,qc_check_frek FROM part_qc WHERE qc_supplier = '$idsupp' ORDER BY qc_part");
        }else if($itemNum != "" && $idsupp ==""){
            $data = DB::select("SELECT DISTINCT qc_part,qc_supplier,qc_check,qc_check_frek FROM part_qc WHERE qc_part = '$itemNum' ORDER BY qc_part");
        }else{
            $data = DB::select("SELECT DISTINCT qc_part,qc_supplier,qc_check,qc_check_frek FROM part_qc ORDER BY qc_part ASC");
        }

        $html = '';
        if(count($data) == 0){
            $html .= '';
        }else{
            $urut = 1;
            foreach($data as $d){
                $part_m = pt_mstr($d->qc_part);
                $xx = DB::table('pub_xxitem_mapping')->where('xxnew_part',$d->qc_part)->first();
                $s = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr',$d->qc_supplier)->first();
                if(empty($xx->xxold_desc1)){
                    $deskk1 = '-';
                }else{
                    $deskk1 = $xx->xxold_desc1;
                }

                $check = $d->qc_check;
                $frek = $d->qc_check_frek;
                if($frek == 'H'){
                    $gl1 = 'selected="selected"';
                }else{
                    $gl1 = '';
                }
                if($frek == 'M'){
                    $gl2 = 'selected="selected"';
                }else{
                    $gl2 = '';
                }
                if($frek == 'B'){
                    $gl3 = 'selected="selected"';
                }else{
                    $gl3 = '';
                }
                if($check=="C") { $ch = 'checked'; $val_check = 'on'; }else{ $ch = '';  $val_check = 'off';}
                if(empty($part_m['deskripsi2'])){
                    $des2 = '-';
                }else{
                    $des2 = $part_m['deskripsi2'];
                }
                // $params = $d->qc_part.'/'.$d->qc_supplier;
                $html .= '
                <tr>
                    <td>'.$d->qc_part.'</td>
                    <td>'.$part_m['deskripsi1'].'</td>
                    <td>'.$des2.'</td>
                    <td>'.$deskk1.'</td>
                    <td>'.$part_m['um'].'</td>
                    <td>'.$d->qc_supplier.'</td>
                    <td>'.$s->ct_ad_name.'</td>
                    <input type="hidden" name="jml_data" id="jml_data" value="'.count($data).'">
                    <input type="hidden" name="part[]" id="part[]" value="'.$d->qc_part.'">
                    <input type="hidden" name="supp[]" id="supp[]" value="'.$d->qc_supplier.'">
                    <td><input type="checkbox" name="checkqc['.$urut.']" id="checkqc['.$urut.']" '.$ch.' onclick="checkcok('.$urut.')" value="'.$val_check.'"></td>
                    <td>
                        <select name="frek[]" id="frek[]" class="form-control" style="color:black">
                            <option value=""></option>
                            <option value="H" '.$gl1.'>Harian</option>
                            <option value="M" '.$gl2.'>Mingguan</option>
                            <option value="B" '.$gl3.'>Bulanan</option>
                        </select>
                    </td>
                    <td><a class="btn btn-danger" id="btn_hps" onclick="hapus_part_qc(\''.$d->qc_part.'\',\''.$d->qc_supplier.'\')" title="Delete"><i class="fas fa-trash-alt" style="color:white"></i></a></td>
                </tr>
                ';
                $urut++;
            }
        }

        return response($html);
    }

    public function hapus_part(Request $request){
        $part       = $request->get('part');
        $supplier   = $request->get('supplier');
        DB::table('part_qc')->where('qc_part', $part)->where('qc_supplier', $supplier)->delete();
        $message = 'oke';
        return response($message);
    }

    public function storeAddItemQc(Request $request){

        $jumlah = $request->get('total');
        $check = $request->get('checkqc');
        for ($i=0; $i < $jumlah ; $i++) {

            if ($check[$i] == 'on') {
                $part = $request->get('part');
                DB::table('part_qc')->insert([
                    'qc_part'      => $part[$i],
                    'qc_supplier'  => 'SA0046I',
                    'qc_check'     => 'C',
                ]);
                // $supplierName  = $request->get('nameSupplier');
                // dd($itemNum, $supplierName);
            }

        }
        $data = 'Success';
        return response($data);
    }

    public function addItemCheck(){

        return view('QPR.add_item_check_qc');
    }

    public function searchadditemcheck(){


        $data = SOAPPtMstr::where('item_number', 'LIKE', '%'.$itemNum.'%')
            ->orWhere('deskripsi1', 'LIKE', '%'.$itemNum.'%')
            ->get();

        return response($data);
    }

    public function counterMeasureAction(Request $request) {

        $no_qpr = $request->noQpr;
        $revisi = $request->revisi;

        $data = DB::table('qprl_master')->where('qprl_no', $no_qpr)->first();

        $duedate = $data->qprl_rp_approriate_date;
        $duedate1 = $data->qprl_rp_approriate_date1;
        $duedate2 = $data->qprl_rp_approriate_date2;
        $duedate3 = $data->qprl_rp_approriate_date3;
        $duedate4 = $data->qprl_rp_approriate_date4;
        $duedate5 = $data->qprl_rp_approriate_date5;

        if(substr($duedate,0,3)=="1900")
        {
            $duedate ="";
        }
        if(substr($duedate1,0,4)=="1900")
        {
            $duedate1 ="";
        }
        if(substr($duedate2,0,4)=="1900")
        {
            $duedate2 ="";
        }
        if(substr($duedate3,0,4)=="1900")
        {
            $duedate3 ="";
        }
        if(substr($duedate4,0,4)=="1900")
        {
            $duedate4 ="";
        }
        if(substr($duedate5,0,5)=="1900")
        {
            $duedate5 ="";
        }

        $result = [
            'qpr' => $no_qpr,
            'revisi' => $revisi,
            'qprl_rp_approved' => $data->qprl_rp_approved,
            'qprl_rp_prepared' => $data->qprl_rp_prepared,
            'qprl_rp_desc_problem' => $data->qprl_rp_desc_problem,
            'qprl_rp_rec_problem' => $data->qprl_rp_rec_problem,
            'qprl_rp_analysis' => $data->qprl_rp_analysis,
            'qprl_rp_approriate' => $data->qprl_rp_approriate,
            'qprl_rp_approriate_pic' => $data->qprl_rp_approriate_pic,
            'duedate' => $duedate,
            'qprl_rp_approriate1' => $data->qprl_rp_approriate1,
            'qprl_rp_approriate_pic1' => $data->qprl_rp_approriate_pic1,
            'duedate1' => $duedate1,
            'qprl_rp_approriate2' => $data->qprl_rp_approriate2,
            'qprl_rp_approriate_pic2' => $data->qprl_rp_approriate_pic2,
            'duedate2' => $duedate2,
            'qprl_rp_approriate3' => $data->qprl_rp_approriate3,
            'qprl_rp_approriate_pic3' => $data->qprl_rp_approriate_pic3,
            'duedate3' => $duedate3,
            'qprl_rp_approriate4' => $data->qprl_rp_approriate4,
            'qprl_rp_approriate_pic4' => $data->qprl_rp_approriate_pic4,
            'duedate4' => $duedate4,
            'qprl_rp_confirm' => $data->qprl_rp_confirm,
            'qprl_rp_why1' => $data->qprl_rp_why1,
            'qprl_rp_why2' => $data->qprl_rp_why2,
            'qprl_rp_why3' => $data->qprl_rp_why3,
            'qprl_rp_why4' => $data->qprl_rp_why4,
            'qprl_rp_why5' => $data->qprl_rp_why5,
            'qprl_rp_feed' => $data->qprl_rp_feed,
            'qprl_rp_attachment' => $data->qprl_rp_attachment,
        ];

        return view('QPR.countermeasure_action',compact('result'));
    }

    public function storeCounterMeasureAction(Request $request){

        $no_qpr = $request->qpr_no;
        $revisi = $request->revisi;

        $tampil_data = DB::table('qprl_master')->where('qprl_no',$no_qpr)->first();

        $supp_name = $tampil_data->qprl_supplier_name;
        $supp_kode = $tampil_data->qprl_supplier;
        $revisi = $tampil_data->qprl_revise;

        $app = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->approved) );
        $prep = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->prepared) );
        $prob = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->problem_desc) );
        $rec_prob = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->problem_rec) );
        $analysis = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->analysis) );
        $approp = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->approriate) );
        $confirm = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->confirmation) );
        $step1 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->step1) );
        $step2 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->step2) );
        $step3 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->step3) );
        $step4 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->step4) );
        $step5 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->step5) );
        $feed = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->fedback) );
        $desc = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->desc) );
        $pic = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->pic) );
        $duedate = $request->duedate;
        $desc1 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->desc1) );
        $pic1 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->pic1) );
        $duedate1 = $request->duedate1;
        $desc2 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->desc2) );
        $pic2 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->pic2) );
        $duedate2 = $request->duedate2;
        $desc3 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->desc3) );
        $pic3 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->pic3) );
        $duedate3 = $request->duedate3;
        $desc4 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->desc4) );
        $pic4 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->pic4) );
        $duedate4 = $request->duedate4;
        $desc5 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->desc5) );
        $pic5 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->pic5) );
        $duedate5 = $request->duedate5;

        $readQPR = DB::table('qprl_master')
        ->where('qprl_no', $no_qpr)
        ->update(array(
            'qprl_status' => 'OK',
            'qprl_date_status' => Carbon::today(),
            'qprl_rp_date' => Carbon::today(),
            'qprl_rp_approved' => $app,
            'qprl_rp_prepared' => $prep,
            'qprl_rp_desc_problem' => $prob,
            'qprl_rp_rec_problem' => $prob,
            'qprl_rp_rec_problem' => $analysis,
            'qprl_rp_confirm' => $confirm,
            'qprl_rp_why1' => $step1,
            'qprl_rp_why2' => $step2,
            'qprl_rp_why3' => $step3,
            'qprl_rp_why4' => $step4,
            'qprl_rp_why5' => $step5,
            'qprl_rp_feed' => $feed,
            // 'qprl_rp_attachment' => $im_attc_dok, //noted
            'qprl_rp_approriate' => $desc,
            'qprl_rp_approriate_pic' => $pic,
            'qprl_rp_approriate_date' => $duedate,
            'qprl_rp_approriate_date2' => $duedate2,
            'qprl_rp_approriate_date3' => $duedate3,
            'qprl_rp_approriate_date4' => $duedate4,
            'qprl_rp_approriate_date5' => $duedate5,
            'qprl_rp_approriate1' => $desc1,
            'qprl_rp_approriate2' => $desc2,
            'qprl_rp_approriate3' => $desc3,
            'qprl_rp_approriate4' => $desc4,
            'qprl_rp_approriate5' => $desc5,
            'qprl_rp_approriate_pic1' => $pic1,
            'qprl_rp_approriate_pic2' => $pic2,
            'qprl_rp_approriate_pic3' => $pic3,
            'qprl_rp_approriate_pic4' => $pic4,
            'qprl_rp_approriate_pic5' => $pic5
        ));

        $readQPR_dtl = DB::table('qprl_dtl')
        ->where('qprl_no', $no_qpr)
        ->where('revisi', $revisi)
        ->update(array(
            'qprl_status' => 'OK',
            'qprl_date_status' => Carbon::today()
        ));

        $pic_email = DB::table('qpr_email_aaij')->select('email')->get();
        $address = "http://127.0.0.1:8000/qpr_report_l?id=".base64_encode($no_qpr);
        Mail::to($pic_email)->send(new MainCounterMeasureSupplier($no_qpr, $supp_name, $revisi, $supp_kode,$address));

        return redirect()->route('counter-measure');

    }

    public function printListQpr(Request $request){

        $no_qpr = $request->noQpr;
        if ($no_qpr != null) {

            $showdata = DB::table('qprl_master')->where('qprl_no', $no_qpr)->first();
            $qpr_dtl = DB::table('qprl_dtl')->select('qprl_qty_sortir_vd','qprl_ng_sortir_vd','qprl_mhr_vd','qprl_qty_sortir_aaij','qprl_ng_sortir_aaij','qprl_mhr_aaij', DB::raw('MAX(revisi) as revisi'))
                            ->where('qprl_no', $no_qpr)
                            ->groupBy('qprl_qty_sortir_vd','qprl_ng_sortir_vd','qprl_mhr_vd','qprl_qty_sortir_aaij','qprl_ng_sortir_aaij','qprl_mhr_aaij')
                            ->first();

            if ($qpr_dtl != null) {
                $qpr_qty_sortir = $qpr_dtl->qprl_qty_sortir_vd + $qpr_dtl->qprl_qty_sortir_aaij;
                $qpr_ng_sortir = $qpr_dtl->qprl_ng_sortir_vd + $qpr_dtl->qprl_ng_sortir_aaij;
                $qpr_mh_sortir = floatval($qpr_dtl->qprl_mhr_vd) + floatval($qpr_dtl->qprl_mhr_aaij);
            }else{
                $qpr_qty_sortir = "";
                $qpr_ng_sortir = "";
                $qpr_mh_sortir = "";
            }

            $qpr_date_found = $showdata->qprl_date_found;
            $qpr_date_delivery = $showdata->qprl_date_delivery;
            $qpr_date_receiving = $showdata->qprl_date_receiving;
            $qpr_date_invoice = $showdata->qprl_date_invoice;
            if(substr($qpr_date_receiving,0,4) < 2014)
            {
                $qpr_date_receiving="";
            }
            if(substr($qpr_date_delivery,0,4) < 2014)
            {
                $qpr_date_delivery="";
            }
            if(substr($qpr_date_invoice,0,4) < 2014)
            {
                $qpr_date_invoice="";
            }
            if(substr($qpr_date_found,0,4) < 2014)
            {
                $qpr_date_found="";
            }

             // $qpr_picture = $showdata->qprl_picture;
            // if(strlen($qpr_picture) <= 51)
            // {
            //     $pict = DB::table('QPR_Customer')->select('customer_image')->where('customer_no_claim', $claim_no)->first();
            //     // $qpr_picture = "http://coe.akebono-astra.co.id/imageQPR/".$pict['customer_image'];
            // }
            // $qpr_charge = $showdata->qprl_charge;
            // $qpr_checked = $showdata->qprl_checked;
            // $qpr_approved = $showdata->qprl_approved;
            // $qpr_status_charge = $showdata->qprl_charge_status;
            // $qpr_status_checked = $showdata->qprl_checked_status;
            // $qpr_status_approved = $showdata->qprl_approved_status;
            // $qpr_date_charge = $showdata->qprl_charge_date;
            // $qpr_date_checked = $showdata->qprl_checked_date;
            // $qpr_date_approved = $showdata->qprl_approved_date;

            // //perlu ditanyakan
            // // $charge = '../app/'.$qpr_charge;
            // // $app_qpr_charge = $charge.".png";

            // $rp_date = $showdata->qprl_rp_date;
            // $rp_approved = $showdata->qprl_rp_approved;
            // $rp_prepared = $showdata->qprl_rp_prepared;
            // $rp_desc_problem = $showdata->qprl_rp_desc_problem;
            // $rp_rec_problem = $showdata->qprl_rp_rec_problem;
            // $rp_analisis = $showdata->qprl_rp_analysis;
            // $rp_approriate = $showdata->qprl_rp_approriate;
            // $rp_confirm = $showdata->qprl_rp_confirm;
            // $rp_why1 = $showdata->qprl_rp_why1;
            // $rp_why2 = $showdata->qprl_rp_why2;
            // $rp_why3 = $showdata->qprl_rp_why3;
            // $rp_why4 = $showdata->qprl_rp_why4;
            // $rp_why5 = $showdata->qprl_rp_why5;
            // $rp_feed = $showdata->qprl_rp_feed;

            $data = [
                'data' => $showdata,
                'id' => $showdata->qprl_id,
                'qpr_no' => $showdata->qprl_no,
                'qpr_supplier' => $showdata->qprl_supplier_name,
                'supp_kode' => $showdata->qprl_supplier,
                'qpr_part_name' => $showdata->qprl_part_name,
                'qpr_item' => $showdata->qprl_item,
                'qpr_material' => $showdata->qprl_material,
                'qpr_part' => $showdata->qprl_part,
                'qpr_type' => $showdata->qprl_type,
                'qpr_qty_ng' => $showdata->qprl_qty_ng,
                'qpr_qty_dev' => $showdata->qprl_qty_det,
                'qpr_qty_det' => $showdata->qprl_qty_det,
                'qpr_inv_no' => $showdata->qprl_inv_no,
                'qpr_lot' => $showdata->qprl_lot,
                'qpr_informasi' => $showdata->qprl_informasi,
                'qpr_deskripsi' => nl2br($showdata->qprl_deskripsi),
                'qpr_date_issue' => $showdata->qprl_date_issue,
                'qpr_date_found' => $qpr_date_found,
                'qpr_date_delivery' => $qpr_date_delivery,
                'qpr_date_receiving' => $qpr_date_receiving,
                'qpr_date_invoice' => $qpr_date_invoice,
                'qpr_rank' => $showdata->qprl_rank,
                'qpr_occ' => $showdata->qprl_occ,
                'qpr_disposition' => $showdata->qprl_disposition,
                'qpr_point' => $showdata->qprl_point,
                'qpr_revise' => $showdata->qprl_revise,
                'qpr_creadate' => $showdata->creadate,
                'found_at' => $showdata->qprl_found_at,
                'found_at_line' => $showdata->qprl_found_at_line,
                'claim_no' => $showdata->qprl_claim_no,
                'qpr_qty_sortir' => $qpr_qty_sortir,
                'qpr_ng_sortir' => $qpr_ng_sortir,
                'qpr_mh_sortir' => $qpr_mh_sortir
            ];
            return view('QPR.print_list_supplier', compact('data'));
        }else{
            return Redirect::back()->withErrors(['msg', 'No QPR No Found!']);
        }


    }

   	public function qprreceiving(Request $request) {
         if ($request->ajax()) {
            if ($request->get('supp')) {
                $no = DB::select("SELECT getdate() as datenow, isnull(max(substring(qprl_no,6,3)),0)+1 as nomer from qprl_master where year(creadate)=year(getdate()) and qprl_supplier = '$request->supp'");
                $no = $no[0];

                $checked = DB::table('qpr_mstr_approval')->where('approval_type', 'Checked')->get();
                $approved = DB::table('qpr_mstr_approval')->where('approval_type', 'Approved')->get();
                // $charge = DB::table('qpr_mstr_approval')->where('approval_type', 'Charge')->get();
                $charge = [['username' => Auth::guard('pub_login')->user()->username]];

                // generate no_id
                $oke = DB::select("SELECT getdate() as datenow, isnull(max(substring(qprl_id,7,3)),0)+1 as id from qprl_master where year(creadate)=year(getdate()) and month(creadate)=month(getdate())");
                $id2_id = str_pad($oke[0]->id, 3, "0", STR_PAD_LEFT);
                $id = date("ymd", strtotime($oke[0]->datenow)) . $id2_id;

                $data = [];
                $data['no'] = $no;
                $data['checked'] = $checked;
                $data['approved'] = $approved;
                $data['charge'] = $charge;
                $data['id'] = $id;

                return json_encode(['status' => true, 'data' => $data]);
            }
            if($request->partName){
                $data = DB::table('SOAP_pt_mstr_bydesc')->distinct()->get();
                // dd($data);
                return json_encode(['status' => true, 'data' => $data]);
            }
        } else {
            $data = ['at' => 'receiving'];
            return view('QPR.qr_receiving_input', compact('data'));
        }
    }

    public function qprreceivingsave(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'file' => 'required|mimes:png,jpg|max:2048'
        ]);

        $imageName = time() . '' . $request->file->getClientOriginalName();
        $request->file->move(public_path('img_inv'), $imageName);
        $imageName = 'img_inv/'.$imageName;

        DB::table('qprl_master')->insert([
            'qprl_id' => $request->id,
            'qprl_no' => $request->no,
            'qprl_supplier' => $request->supp,
            'qprl_supplier_name' => $request->supp_name,
            'qprl_fax' => $request->fax,
            'qprl_part_name' => $request->name,
            'qprl_item' => $request->item,
            'qprl_material' => '',
            'qprl_part' => $request->number,
            'qprl_type' => $request->tipe,
            'qprl_qty_ng' => $request->ng,
            'qprl_qty_det' => $request->det,
            'qprl_inv_no' => $request->invoice,
            'qprl_lot' => $request->lot,
            'qprl_informasi' => $request->info,
            'qprl_deskripsi' => $request->deskripsi,
            'qprl_date_issue' => $request->issu,
            'qprl_date_found' => $request->found_date,
            'qprl_date_delivery' => $request->deliv,
            'qprl_date_receiving' => $request->receive,
            'qprl_date_invoice' => $request->date,
            'qprl_rank' => $request->rank,
            'qprl_occ' => $request->occu,
            'qprl_disposition' => $request->dispo,
            'qprl_point' => $request->point,
            'qprl_revise' => $request->revise,
            'qprl_picture' => $imageName,
            'qprl_charge' => $request->charge,
            'qprl_checked' => $request->check,
            'qprl_approved' => $request->approve,
            'qprl_found_at' => $request->at,
            'creadate' => NOW(),
            'qprl_qty_ng_satuan' => $request->ng_satuan,
            'qprl_claim_no' => '',
            'qprl_charge_date' => NOW(),
            'qprl_charge_status' => 'OK',
            'qprl_found_at_line' => '',
        ]);

        DB::table('qprl_dtl')->insert([
            'qprl_no' => $request->no,
            'qprl_qty_sortir_vd' => 0,
            'qprl_ng_sortir_vd' => 0,
            'qprl_mhr_vd' => '',
            'qprl_qty_sortir_aaij' => 0,
            'qprl_ng_sortir_aaij' => 0,
            'qprl_mhr_aaij' => '',
            'qprl_start_date' => NOW(),
            'qprl_end_date' => NOW(),
            'revisi' => $request->revise,
            'modif_date' => NULL,
            'qprl_status' => NULL,
            'qprl_date_status' => NULL
        ]);

        $emailAddress = DB::table('qpr_mstr_approval')->where('approval_name', $request->check)->first();

        // send email pemberitahuan ke supplier

        Mail::to($emailAddress->approval_email)
            ->send(new sendEmailApproval($request->no, $emailAddress, 'check'));

        if(!empty($request->page)){
            if($request->page == 'release'){
                alert()->success('Success', 'Insert QPR Release New Success');
                return redirect()->route('qpr-release-new');
            }else{
                alert()->success('Success', 'Insert QPR Receiving Success');
                return redirect()->route('qpr-receiving');
            }
        }else{
            alert()->success('Success', 'Insert QPR Receiving Success');
            return redirect()->route('qpr-receiving');
        }
    }

    public function approval($no, $level)
    {
        $no = base64_decode($no);
        $level = base64_decode($level);

        $result = DB::table('qprl_master')->where('qprl_no', $no)->first();

        $data = [
            'id' => $result->qprl_id,
            'qpr_no' => $result->qprl_no,
            'qpr_supplier' => $result->qprl_supplier_name,
            'supp_kode' => $result->qprl_supplier,
            'qpr_part_name' => $result->qprl_part_name,
            'qpr_item' => $result->qprl_item,
            'qpr_material' => $result->qprl_material,
            'qpr_part' => $result->qprl_part,
            'qpr_type' => $result->qprl_type,
            'qpr_qty_ng' => $result->qprl_qty_ng,
            'qpr_qty_dev' => $result->qprl_qty_det,
            'qpr_qty_det' => $result->qprl_qty_det,
            'qpr_inv_no' => $result->qprl_inv_no,
            'qpr_lot' => $result->qprl_lot,
            'qpr_informasi' => $result->qprl_informasi,
            // $qpr_qty_sortir' => $qpr_dtl['qprl_qty_sortir_vd'] + $qpr_dtl['qprl_qty_sortir_aaij,
            // $qpr_ng_sortir' => $qpr_dtl['qprl_ng_sortir_vd'] + $qpr_dtl['qprl_ng_sortir_aaij,
            // $qpr_mh_sortir' => $qpr_dtl['qprl_mhr_vd'] + $qpr_dtl['qprl_mhr_aaij,
            // if ($qpr_qty_sortir' ='>=> 0) {
            //     $qpr_qty_sortir' => "-",
            //     $qpr_ng_sortir' => "-",
            //     $qpr_mh_sortir' => "-",
            // }
            'qpr_qty_sortir' => "-",
            'qpr_ng_sortir' => "-",
            'qpr_mh_sortir' => "-",
            'qpr_deskripsi' => nl2br($result->qprl_deskripsi),
            'qpr_date_issue' => $result->qprl_date_issue,
            'qpr_date_found' => $result->qprl_date_found,
            'qpr_date_delivery' => $result->qprl_date_delivery,
            'qpr_date_receiving' => $result->qprl_date_receiving,
            'qpr_date_invoice' => $result->qprl_date_invoice,

            // validasi
            'qpr_rank' => $result->qprl_rank,
            'qpr_occ' => $result->qprl_occ,
            'qpr_disposition' => $result->qprl_disposition,
            'qpr_point' => $result->qprl_point,
            'qpr_revise' => $result->qprl_revise,
            'qpr_picture' => $result->qprl_picture,
            'qpr_creadate' => $result->creadate,
            'found_at' => $result->qprl_found_at,
            'found_at_line' => $result->qprl_found_at_line,
            'claim_no' => $result->qprl_claim_no,

            // validasi gambar ke detail
            'gambar_qpr' => $result->qprl_picture,
            'qpr_charge' => $result->qprl_charge,
            'qpr_checked' => $result->qprl_checked,
            'qpr_approved' => $result->qprl_approved,

            'qpr_status_charge' => $result->qprl_charge_status,

            'qpr_status_checked' => $result->qprl_checked_status,

            'qpr_status_approved' => $result->qprl_approved_status,

            'qpr_date_charge' => $result->qprl_charge_date,
            'qpr_date_checked' => $result->qprl_checked_date,
            'qpr_date_approved' => $result->qprl_approved_date,

            // validasi tak beraturan
            'rp_date' => $result->qprl_rp_date,
            'rp_approved' => $result->qprl_rp_approved,
            'rp_prepared' => $result->qprl_rp_prepared,
            'rp_desc_problem' => $result->qprl_rp_desc_problem,
            'rp_rec_problem' => $result->qprl_rp_rec_problem,
            'rp_analisis' => $result->qprl_rp_analysis,
            'rp_approriate' => $result->qprl_rp_approriate,
            'rp_confirm' => $result->qprl_rp_confirm,
            'rp_why1' => $result->qprl_rp_why1,
            'rp_why2' => $result->qprl_rp_why2,
            'rp_why3' => $result->qprl_rp_why3,
            'rp_why4' => $result->qprl_rp_why4,
            'rp_why5' => $result->qprl_rp_why5,
            'rp_feed' => $result->qprl_rp_feed,
        ];

        return view('QPR.approval', ['data' => $data, 'level' => $level]);
    }

	public function allListQpr(Request $request) {
        if ($request->ajax()) {
            if($request->get('type') && $request->get('qprl_no') && $request->get('code') && $request->get('from_date') && $request->get('to_date')) {
                $data = DB::table('qprl_master')
                ->where('qprl_found_at', '=', $request->get('type'))
                ->where('qprl_no', 'like', '%' . $request->get('qprl_no') . '%')
                ->where('qprl_supplier', '=', $request->get('code'))
                ->whereBetween('qprl_date_issue', [$request->get('from_date'), $request->get('to_date')])
                ->orderby('qprl_id', 'asc')
                ->get();
            } else if($request->get('type') && $request->get('qprl_no') && $request->get('from_date') && $request->get('to_date')) {
                $data = DB::table('qprl_master')
                ->where('qprl_found_at', '=', $request->get('type'))
                ->where('qprl_no', 'like', '%' . $request->get('qprl_no') . '%')
                ->whereBetween('qprl_date_issue', [$request->get('from_date'), $request->get('to_date')])
                ->orderby('qprl_id', 'asc')
                ->get();
            } else if($request->get('type') && $request->get('code') && $request->get('from_date') && $request->get('to_date')) {
                $data = DB::table('qprl_master')
                ->where('qprl_found_at', '=', $request->get('type'))
                ->where('qprl_supplier', '=', $request->get('code'))
                ->whereBetween('qprl_date_issue', [$request->get('from_date'), $request->get('to_date')])
                ->orderby('qprl_id', 'asc')
                ->get();
            } else if($request->get('qprl_no') && $request->get('code') && $request->get('from_date') && $request->get('to_date')) {
                $data = DB::table('qprl_master')
                ->where('qprl_no', 'like', '%' . $request->get('qprl_no') . '%')
                ->where('qprl_supplier', '=', $request->get('code'))
                ->whereBetween('qprl_date_issue', [$request->get('from_date'), $request->get('to_date')])
                ->orderby('qprl_id', 'asc')
                ->get();
            } else if($request->get('type') && $request->get('qprl_no') && $request->get('code')) {
                $data = DB::table('qprl_master')
                ->where('qprl_found_at', '=', $request->get('type'))
                ->where('qprl_no', 'like', '%' . $request->get('qprl_no') . '%')
                ->where('qprl_supplier', '=', $request->get('code'))
                ->orderby('qprl_id', 'asc')
                ->get();
            } else if($request->get('type') && $request->get('qprl_no')) {
                $data = DB::table('qprl_master')
                ->where('qprl_found_at', '=', $request->get('type'))
                ->where('qprl_no', 'like', '%' . $request->get('qprl_no') . '%')
                ->orderby('qprl_id', 'asc')
                ->get();
            } else if($request->get('type') && $request->get('code')) {
                $data = DB::table('qprl_master')
                ->where('qprl_found_at', '=', $request->get('type'))
                ->where('qprl_supplier', '=', $request->get('code'))
                ->orderby('qprl_id', 'asc')
                ->get();
            } else if($request->get('qprl_no') && $request->get('code')) {
                $data = DB::table('qprl_master')
                ->where('qprl_no', 'like', '%' . $request->get('qprl_no') . '%')
                ->where('qprl_supplier', '=', $request->get('code'))
                ->orderby('qprl_id', 'asc')
                ->get();
            } else if($request->get('from_date') && $request->get('to_date')) {
                $data = DB::table('qprl_master')
                ->whereBetween('qprl_date_issue', [$request->get('from_date'), $request->get('to_date')])
                ->orderby('qprl_id', 'asc')
                ->get();
            } else if($request->get('type')) {
                $data = DB::table('qprl_master')
                ->where('qprl_found_at', '=', $request->get('type'))
                ->orderby('qprl_id', 'asc')
                ->get();
            } else if($request->get('qprl_no')) {
                $data = DB::table('qprl_master')
                ->where('qprl_no', 'like', '%' . $request->get('qprl_no') . '%')
                ->orderby('qprl_id', 'asc')
                ->get();
            } else if($request->get('code')) {
                $data = DB::table('qprl_master')
                ->where('qprl_supplier', '=', $request->get('code'))
                ->orderby('qprl_id', 'asc')
                ->get();
            } else {
                $data = DB::table('qprl_master')
                ->orderby('qprl_id', 'asc')
                ->get();
            }

            return DataTables::of($data)
                ->addColumn('verification', function ($data) {
                    if($data->qprl_ver_date == '') {
                        $button = "<a href='/all-list-qpr-verifikasi/" . base64_encode($data->qprl_no) . "' class='btn btn-sm btn-info tablefontmini' title='verifikasi'><i class='fas fa-check'></i></a>";
                    } else {
                        $button = "<a href='/all-list-qpr-verifikasi/" . base64_encode($data->qprl_no) . "' class='btn btn-sm btn-info tablefontmini' title='verifikasi'><i class='fas fa-check'></i></a>";
                        $button .= '<br>';
                        $button .= \Carbon\Carbon::parse($data->qprl_ver_date)->format('d/m/Y');
                    }
                    return $button;
                })
                ->addColumn('action', function ($data) {
                    if(Auth::guard('pub_login')->user()->previllage == 'QC') {
                        $button = "<a class='btn btn-sm btn-secondary mr-1 tablefontmini' href='/sortir-all-list-qpr/" . base64_encode($data->qprl_no) . "' data-toggle='tooltip' data-placement='top' title='Sortir'><i class='fas fa-filter'></i></a>";
                        // if(Auth::guard('pub_login')->user()->username == 'viki' || Auth::guard('pub_login')->user()->username == 'puji') {
                        //     $button .= "<a class='btn btn-sm btn-secondary mr-1 tablefontmini' href='' data-toggle='tooltip' data-placement='top' title='Revision'><i class='fas fa-tasks'></i></a>";
                        //     $button .= "<a class='btn btn-sm btn-secondary mr-1 tablefontmini' href='' data-toggle='tooltip' data-placement='top' title='Delete'><i class='fas fa-trash'></i>/a>";
                        // }
                        $button .= "<a class='btn btn-sm btn-info mr-1 tablefontmini' href='/print-all-list-qpr/" . base64_encode($data->qprl_no) . "' data-toggle='tooltip' data-placement='top' title='Print' target='_blank'><i class='fas fa-print'></i></a>";
                        $button .= "<a class='btn btn-sm btn-success mr-1 tablefontmini' href='/report-counter-measure/" . base64_encode($data->qprl_no) . "' data-toggle='tooltip' data-placement='top' title='counter measure' target='_blank'><i class='fas fa-recycle'></i></a>";
                        $button .= "<a class='btn btn-sm btn-dark mr-1 tablefontmini' href='/attach-document/" . base64_encode($data->qprl_no) . "' data-toggle='tooltip' data-placement='top' title='Attach Document'><i class='fas fa-thumbtack'></i></a>";
                        if($data->qprl_rp_date == '' && $data->qprl_rp_attachment == '') {
                            $button .= "<a class='btn btn-sm btn-warning' href='/mail-reminder-vendor?&no_qpr=".$data->qprl_no."&supp_code=".$data->qprl_supplier."&revisi=".$data->qprl_revise."&email=resend' data-toggle='tooltip' data-placement='top' title='Resend Email to Supplier'><i class='fas fa-envelope-open-text'></i></a>";
                        } else {
                            $button .= "<a class='btn btn-sm btn-danger' href='/mail-reminder-vendor?&no_qpr=".$data->qprl_no."&supp_code=".$data->qprl_supplier."&revisi=".$data->qprl_revise."&email=reanswer' data-toggle='tooltip' data-placement='top' title='Reject Countermeasure'><i class='fas fa-dot-circle'></i></a>";
                        }
                        // $check_status = $data->qprl_checked_status;
                        // if($check_status=="OK") {
                        //     $check_status="Approved";
                        // } elseif ($check_status=="NOK") {
                        //     $check_status="Not Approved";
                        // }else {
                        //     $check_status="Waiting";
                        // }
                        // if($check_status == 'Waiting' || $check_status == 'Not Approved' && Auth::guard('pub_login')->user()->username == 'viki') {
                        //     $button .= "<a class='btn btn-sm btn-danger tablefontmini' href='#' data-toggle='tooltip' data-placement='top' title='Approval'><i class='fas fa-thumbs-up'></i></a>";
                        // } elseif (($check_status == 'Waiting' || $check_status == 'Not Approved') && $check_status == 'Approved' && Auth::guard('pub_login')->user()->username = 'sucipto') {
                        //     $button .= "<a class='btn btn-sm btn-danger tablefontmini' href='#' data-toggle='tooltip' data-placement='top' title='Approval'><i class='fas fa-thumbs-up'></i></a>";
                        // }
                    }
                    return $button;
                })
                ->rawColumns(['verification', 'action'])
                ->make(true);
        }

    	return view('QPR.all_list_qpr');
    }

    public function storeAlllistqpr(Request $request) {
        $no = DB::table('qprl_master')->where('qprl_no', $request->var)->count();
        if ($no == 0) {
        }else{
            DB::table('qprl_master')->where('qprl_no', $request->qpr_no)
                ->update([
                'qprl_checked_date' => NOW(),
                'qprl_checked_status' => 'OK',
                'qprl_inbound_status' => '-',
                'qprl_inbound_date' => NOW(),
                'qprl_inbound_ktr' => ''
            ]);
        }

        return redirect()->route('all-list-qpr');

    }

    public function allListQprVerifikasi($id) {
        $data = QPRMaster::select('qprl_no', 'qprl_ver_file', 'qprl_ver_date')->where('qprl_no', '=', base64_decode($id))->first();

    	return view('QPR.all_list_qpr_verifikasi', compact('data'));
    }

    public function allListQPRVerifikasiUpdate($id, Request $request) {
        $request->validate([
            'vimagename'    => 'required',
        ]);

        if($request->file('vimagename')) {
            $file = $request->file('vimagename');
            $nameImage = $file->getClientOriginalName();
            $folder = 'attachment_qpr/' . date('Ymdi');
            $nameImageEnd = $folder . $nameImage;
            $file->move(public_path('images/attachment_qpr'), $nameImageEnd);
        } else {
            $nameImageEnd = '';
        }

        $data = QPRMaster::where('qprl_no', '=', base64_decode($id))->update([
            'qprl_ver_file' => $nameImageEnd,
            'qprl_ver_date' => $request->vtanggal
        ]);

        alert()->success('Update Successfully','Verification successful!');
        return redirect()->route('all-list-qpr');
    }

    public function sortirAllListQpr(Request $request) {
        return view('QPR.sortir-all-list-qpr');
    }

    public function printAllListQpr(Request $request, $id) {
        $data = DB::table('qprl_master')->where('qprl_no', base64_decode($id))->first();
        $qprl_det = DB::table('qprl_dtl')->where('qprl_no', base64_decode($id))->first();
        if(!$qprl_det) {
            return view('status.not-found');
        }
        $qpr_sortir = $qprl_det->qprl_qty_sortir_vd + $qprl_det->qprl_qty_sortir_aaij;
        $qpr_ng_sortir = $qprl_det->qprl_ng_sortir_vd + $qprl_det->qprl_ng_sortir_aaij;

        if($qprl_det->qprl_mhr_aaij == null) {
            $qprl_mhr_aaij_det = 0;
        } else {
            $qprl_mhr_aaij_det = $qprl_det->qprl_mhr_aaij;
        }

        if($qprl_det->qprl_mhr_vd == null) {
            $qprl_mhr_vd_det = 0;
        } else {
            $qprl_mhr_vd_det = $qprl_det->qprl_mhr_vd;
        }

		$qpr_mh_sortir = $qprl_mhr_vd_det + $qprl_mhr_aaij_det;

        return view('QPR.print-all-list-qpr', compact('data', 'qprl_det', 'qpr_sortir', 'qpr_ng_sortir', 'qpr_mh_sortir'));
    }

    public function reportCounterMeasure(Request $request, $id) {
        $data = DB::table('qprl_master')->where('qprl_no', base64_decode($id))->first();
        $qprl_det = DB::table('qprl_dtl')->where('qprl_no', base64_decode($id))->first();
        if(!$qprl_det) {
            return view('status.not-found');
        }
        $qpr_sortir = $qprl_det->qprl_qty_sortir_vd + $qprl_det->qprl_qty_sortir_aaij;
        $qpr_ng_sortir = $qprl_det->qprl_ng_sortir_vd + $qprl_det->qprl_ng_sortir_aaij;
		if($qprl_det->qprl_mhr_aaij == null) {
            $qprl_mhr_aaij_det = 0;
        } else {
            $qprl_mhr_aaij_det = $qprl_det->qprl_mhr_aaij;
        }

        if($qprl_det->qprl_mhr_vd == null) {
            $qprl_mhr_vd_det = 0;
        } else {
            $qprl_mhr_vd_det = $qprl_det->qprl_mhr_vd;
        }

		$qpr_mh_sortir = $qprl_mhr_vd_det + $qprl_mhr_aaij_det;

        return view('QPR.report-counter-measure', compact('data', 'qprl_det', 'qpr_sortir', 'qpr_ng_sortir', 'qpr_mh_sortir'));
    }

    public function attachDocument(Request $request, $id) {
        $data = DB::table('qprl_master')->where('qprl_no', base64_decode($id))->first();

        return view('QPR.attach-document', compact('data'));
    }

    public function suratclaimqpr() {
    	return view('QPR.surat_claim');
    }

    public function searchSuratClaimQpr(Request $request) {
        if($request->get('code') != '' && $request->get('from_date') && $request->get('to_date')) {
            $from_date = \Carbon\Carbon::parse($request->from_date)->format('Y-m-d');
            $to_date = \Carbon\Carbon::parse($request->to_date)->format('Y-m-d');

            $data = DB::select("SELECT * from qprl_master where qprl_supplier='$request->code' and qprl_date_issue >= '$from_date' and qprl_date_issue <= '$to_date' and (substring(qprl_no,18,1)='L' or substring(qprl_no,18,1)='C')");
        } else {
            $tablenya = '';

            // $tablenya .= '<tr>
            //     <td></td>
            //     <td></td>
            //     <td></td>
            //     <td></td>
            //     <td></td>
            //     <td></td>
            //     <td></td>
            //     <td></td>
            // </tr>';

            $data = [
                'tablenya' => $tablenya,
            ];

            return response($data);
        }

        $no = 1;
        $tablenya = '';

        foreach($data as $i => $row){
            if($row->qprl_rp_date == "") {
			    $ounter = "Not yet";
			} else {
			    $ounter = "OK";
			}

            $tablenya .= '<tr>
                <td>'.$no++.'</td>
                <td>'.$row->qprl_no.'</td>
                <td>'.$row->qprl_supplier.'</td>
                <td>'.$row->qprl_supplier_name.'</td>
                <td>'.$row->qprl_item.'</td>
                <td>'.$row->qprl_part_name.'</td>
                <td>'.$ounter .' <br> ' .$row->qprl_rp_date.'</td>
                <td style="height: 2rem" class="d-flex justify-content-center align-self-center align-items-center ml-3"><input type="checkbox" name="check['.$row->qprl_no.']" class="form-check-input"></td>
            </tr>';
        }

        $data = [
            'tablenya' => $tablenya,
        ];

        return response($data);
    }

    public function storeSuratClaimQpr(Request $request) {
        // $data = DB::select("SELECT getdate() as datenow, isnull(max(substring(sj_no,6,3)),0)+1 as nomer from qpr_ms_sj_prc where year(creadate)=year(getdate()) and month(creadate)=month(getdate())");
        // dd($data);
        // $no = str_pad($data[0]->nomer, 3, "0", STR_PAD_LEFT);
        $no = date("ymd") . '001';
        $cek =  QPRDtSjPrc::where('sj_no', 'like', '%' . date("ymd") . '%')->first();
        if($cek) {
            $getdate = QPRDtSjPrc::where('sj_no', 'like', '%' . date("ymd") . '%')->orderBy('sj_no', 'desc')->first();
            $getdateno = intval($getdate->sj_no);
            $i = $getdateno + 1;
        } else {
            $i = $no;
        }
        try {
            DB::beginTransaction();

            if(count($request->check) > 0) {
                foreach($request->check as $item=>$v) {
                    $ms = QPRMaster::where('qprl_no', $item)->first();

                    if($item == array_key_first($request->check)) {
                        QPRMsSjPrc::insert([
                            'sj_no' => $i,
                            'supp_id' => $ms->qprl_supplier,
                            'supp_name' => $ms->qprl_supplier_name,
                            'creaby' => Auth::guard('pub_login')->user()->username,
                            'creadate' => now()->format('Y-m-d')
                        ]);
                    }
                }

                foreach($request->check as $item=>$v) {
                    $data = QPRMaster::where('qprl_no', $item)->first();

                    $dataend = [
                        'sj_no'  => $i,
                        'qpr_no' => $data->qprl_no,
                        'pn'     => $data->qprl_item,
                        'pdesc'  => $data->qprl_part_name,
                    ];

                    QPRDtSjPrc::insert($dataend);
                }
            }

            DB::commit();

            alert()->success('Added Successfully','claim letter has been added successfully');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollBack();

            alert()->error('error',$e->getMessage());
            return redirect()->back();
        }
    }

    public function viewSortir(Request $request) {
        if ($request->ajax()) {
            $data = DB::table('qpr_sortir')->orderBy('creadate', 'DESC')->get();

            return DataTables::of($data)
            ->make(true);
        }

    	return view('QPR.view-sortir');
    }


    public function sortir() {
        $data = 'ada';
    	return view('QPR.sortir', compact('data'));
    }

    public function storeSortir(Request $request) {
        $request->validate([
            'tgl'          => 'required',
            'supplier'     => 'required',
            'problem'      => 'required',
            'item_number'  => 'required',
            'ok'           => 'required',
            'ng'           => 'required',
            'total_sortir' => 'required',
            'jam_start'    => 'required',
            'jam_end'      => 'required',
            'pic_sortir'   => 'required'
        ]);

        try {
            $data = QPRSortir::create([
                'tanggal'   => $request->input('tgl'),
                'supplier_id' => $request->input('supplier'),
                'problem_desc'   => $request->input('problem'),
                'item_nbr'   => $request->input('item_number'),
                'qty_ok'=> $request->input('ok'),
                'qty_ng'   => $request->input('ng'),
                'qty_sortir'   => $request->input('total_sortir'),
                'jamstart' => $request->input('jam_start'),
                'jamend' => $request->input('jam_end'),
                'pic_sortir' => $request->input('pic_sortir'),
                'pic_aaij' => Auth::guard('pub_login')->user()->username,
                'creadate' => Carbon::now(),
                'creaby' => Auth::guard('pub_login')->user()->username
            ]);

            DB::commit();

             alert()->success('Added Successfully','sortir has been added successfully')->animation('tada faster','fadeInUp faster');
            return redirect()->back()->with(['sweet' => 'sweet']);
        } catch (\Exception $e) {
            DB::rollBack();

            toast($e->getMessage(),'error')->position('top-end')->autoClose(3000)->timerProgressBar();
            return redirect()->back();
        }
    }

    public function qprprintr() {
    	return view('QPR.print_qpr_r');
    }

    public function approvalSend(Request $request)
    {
        // dd($request->all());
        if (isset($request->check) || isset($request->reject_check)) {
            if ($request->check) {
                $data = [
                    'kode' => $request->pn,
                    'act_qty' => $request->qty,
                    'qpr_no' => $request->qpr_no,
                    'issued_date' => $request->effdate,
                    'locFrom' => $request->loc_from,
                    'locTo' => $request->loc_to,
                    'supp_name' => $request->supp_name,
                ];


                if (date("m") != date("m", strtotime($data['issued_date']))) {
                    $data['tanggal'] = date("Y-m") . "-1";
                } else {
                    $data['tanggal'] = $data['issued_date'];
                }

                // send email qc
                Mail::to('rahma.d@akebono-astra.co.id')->send(new emailResult($data));

                DB::table('qprl_master')->where('qprl_no', $request->qpr_no)
                    ->update([
                        'qprl_checked_date' => NOW(),
                        'qprl_checked_status' => 'OK',
                        'qprl_inbound_status' => '-',
                        'qprl_inbound_date' => NOW(),
                        'qprl_inbound_ktr' => ''
                    ]);

                $emailAddress = DB::table('qpr_mstr_approval')->where('approval_name', $request->approved)->first();

                Mail::to($emailAddress->approval_email)->send(new sendEmailApproval($request->qpr_no, $emailAddress, 'approve'));
                return view('QPR.redirect', ['status' => 'approval']);
            } else {
                DB::table('qprl_master')->where('qprl_no', $request->qpr_no)
                    ->update(['qprl_checked_date' => NOW(), 'qprl_checked_status' => 'NOK']);
                    return view('QPR.redirect', ['status' => 'rejected']);
            }
        }

        if (isset($request->approve) || isset($request->reject_approve)) {
            if ($request->approve) {

                $qprMstr = DB::table('qprl_master')->where('qprl_no', $request->qpr_no)->first();

                $emailAddress = DB::table('qpr_email_supp')->where('supp_kode', $qprMstr->qprl_supplier)->first();
                $data = [];
                $data['supp_kode'] = $emailAddress->supp_kode;
                $data['supp_name'] = $emailAddress->supp_name;
                $data['supp_pic_name'] = $emailAddress->supp_pic_name;
                $data['supp_pic_email'] = $emailAddress->supp_pic_email;
                $data['revisi'] = $qprMstr->qprl_revise;

                Mail::to($emailAddress->supp_pic_email)->send(new sendEmailApproval($request->qpr_no, $data, 'supplier'));

                DB::table('qprl_master')->where('qprl_no', $request->qpr_no)
                    ->update(['qprl_approved_date' => NOW(), 'qprl_approved_status' => 'OK']);
                return view('QPR.redirect', ['status' => 'approval']);
            } else {
                DB::table('qprl_master')->where('qprl_no', $request->qpr_no)
                    ->update(['qprl_approved_date' => NOW(), 'qprl_approved_status' => 'NOK']);
                return view('QPR.redirect', ['status' => 'rejected']);
            }
        }

        if ($request->qpr_data) {
            // dd($request->all());
            $cek = DB::table('qprl_master')->where('qprl_no', $request->qpr_no)->first();
            if ($cek->qprl_status == 'Read') {
                return view('QPR.redirect', ['status' => 'read']);
            } else {
                DB::table('qprl_master')->where('qprl_no', $request->qpr_no)
                ->update([
                    'qprl_date_status' => NOW(),
                    'qprl_status' => 'Read',
                ]);
                return view('QPR.redirect', ['status' => 'unread']);
            }
        }
    }

    public function qprcustomer()
    {
        $data = DB::select("SELECT * FROM QPR_Customer WHERE customer_area='QPR SUPPLIER' and customer_release IS NULL ORDER BY no DESC");
        return view('QPR.qpr_customer_view', compact('data'));
    }

    public function qprreleasenew()
    {
        return view('QPR.qpr_release_new');
    }

    public function qprreleasenewsave(Request $request)
    {
        DB::table('qprl_master')->insert([
            'qprl_id' => NULL,
            'qprl_no' => $request->no,
            'qprl_supplier' => $request->supp,
            'qprl_supplier_name' => $request->supp_name,
            'qprl_fax' => $request->fax,
            'qprl_part_name' => $request->name,
            'qprl_item' => $request->item,
            'qprl_material' => '',
            'qprl_part' => $request->number,
            'qprl_type' => $request->tipe,
            'qprl_qty_ng' => $request->ng,
            'qprl_qty_det' => $request->det,
            'qprl_inv_no' => $request->qprl_inv_no,
            'qprl_lot' => $request->lot,
            'qprl_informasi' => $request->info,
            'qprl_deskripsi' => $request->deskripsi,
            'qprl_date_issue' => $request->issu,
            'qprl_date_found' => $request->found_date,
            'qprl_date_delivery' => $request->deliv,
            'qprl_date_receiving' => $request->receive,
            'qprl_date_invoice' => $request->date,
            'qprl_rank' => $request->rank,
            'qprl_occ' => $request->occu,
            'qprl_disposition' => $request->dispo,
            'qprl_point' => $request->point,
            'qprl_revise' => $request->revise,
            'qprl_picture' => $request->file,
            'qprl_charge' => $request->charge,
            'qprl_checked' => $request->check,
            'qprl_approved' => $request->approve,
            'qprl_found_at' => $request->at,
            'creadate' => NOW(),
            'qprl_qty_ng_satuan' => $request->ng_satuan,
            'qprl_claim_no' => '',
            'qprl_charge_date' => NOW(),
            'qprl_charge_status' => 'OK',
            'qprl_found_at_line' => '',
        ]);
        alert()->success('Success', 'Insert QPR Release New Success');
        return redirect()->route('qpr-release-new');
    }

    public function allistqpr()
    {
        return view('QPR.all_list_qpr');
    }

    public function qprprintl()
    {
        return view('QPR.print_qpr_l');
    }

    public function qcprocessbc(Request $request)
    {
        // $dataall = DB::table('portald_det as pd')
        //             ->distinct()
        //             ->select('pd.portald_tr_id','pd.portald_po_nbr','pm.portal_vend','pm.portal_ps_nbr','pm.portal_dlv_date')
        //             ->leftJoin('portal_mstr as pm', 'pd.portald_tr_id','=','pm.portal_tr_id')
        //             ->whereNotNull('portald_status')
        //             ->orderBy('portal_dlv_date', 'asc')
        //             ->get();

        // $data = [];
        // foreach ($dataall as $key ){
        //     $name = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr', $key->portal_vend)->first();
        //     array_push($data, [
        //         'portald_tr_id' => $key->portald_tr_id,
        //         'portald_po_nbr' => $key->portald_po_nbr,
        //         'portal_vend' => $key->portal_vend,
        //         'portal_ps_nbr' => $key->portal_ps_nbr,
        //         'portal_dlv_date' => $key->portal_dlv_date,
        //         'supp_name' => $name->ct_ad_name
        //     ]);
        // }

    	// return view('QPR.qc_process_bc', compact('data'));
        if ($request->ajax()) {
            $now_month = date('m');
            $transaksi_id = $request->get('transaction_id');
            $supplier_id = $request->get('supplier_id');
            $filter_moon = $request->get('filter_moon');
            $filter_year = $request->get('filter_year');
            if($transaksi_id && $supplier_id && $filter_year && $filter_moon) {
                $data = DB::table('portal_mstr')
                        ->join('portald_det', 'portald_det.portald_tr_id', '=', 'portal_mstr.portal_tr_id')
                        ->select('portald_tr_id', 'portald_po_nbr', 'portal_vend', 'portal_ps_nbr', 'portal_dlv_date', 'portal_user_id')
                        ->whereNotNull('portald_status')
                        ->where('portald_tr_id', $transaksi_id)
                        ->where('portal_vend', $supplier_id)
                        ->whereMonth('portal_dlv_date', $filter_moon)
                        ->whereYear('portal_dlv_date', $filter_year)
                        ->orderBy('portal_dlv_date', 'ASC')
                        ->distinct()
                        ->get();
            } else if($transaksi_id && $filter_year && $filter_moon) {
                $data = DB::table('portal_mstr')
                        ->join('portald_det', 'portald_det.portald_tr_id', '=', 'portal_mstr.portal_tr_id')
                        ->select('portald_tr_id', 'portald_po_nbr', 'portal_vend', 'portal_ps_nbr', 'portal_dlv_date', 'portal_user_id')
                        ->whereNotNull('portald_status')
                        ->where('portald_tr_id', $transaksi_id)
                        ->whereMonth('portal_dlv_date', $filter_moon)
                        ->whereYear('portal_dlv_date', $filter_year)
                        ->orderBy('portal_dlv_date', 'ASC')
                        ->distinct()
                        ->get();
            } else if($supplier_id && $filter_year && $filter_moon) {
                $data = DB::table('portal_mstr')
                        ->join('portald_det', 'portald_det.portald_tr_id', '=', 'portal_mstr.portal_tr_id')
                        ->select('portald_tr_id', 'portald_po_nbr', 'portal_vend', 'portal_ps_nbr', 'portal_dlv_date', 'portal_user_id')
                        ->whereNotNull('portald_status')
                        ->where('portal_vend', $supplier_id)
                        ->whereMonth('portal_dlv_date', $filter_moon)
                        ->whereYear('portal_dlv_date', $filter_year)
                        ->orderBy('portal_dlv_date', 'ASC')
                        ->distinct()
                        ->get();
            } else if($transaksi_id) {
                $data = DB::table('portal_mstr')
                        ->join('portald_det', 'portald_det.portald_tr_id', '=', 'portal_mstr.portal_tr_id')
                        ->select('portald_tr_id', 'portald_po_nbr', 'portal_vend', 'portal_ps_nbr', 'portal_dlv_date', 'portal_user_id')
                        ->whereNotNull('portald_status')
                        ->where('portald_tr_id', $transaksi_id)
                        ->orderBy('portal_dlv_date', 'ASC')
                        ->distinct()
                        ->get();
            } else if($supplier_id) {
                $data = DB::table('portal_mstr')
                        ->join('portald_det', 'portald_det.portald_tr_id', '=', 'portal_mstr.portal_tr_id')
                        ->select('portald_tr_id', 'portald_po_nbr', 'portal_vend', 'portal_ps_nbr', 'portal_dlv_date', 'portal_user_id')
                        ->whereNotNull('portald_status')
                        ->where('portal_vend', $supplier_id)
                        ->orderBy('portal_dlv_date', 'ASC')
                        ->distinct()
                        ->get();
            } else if($filter_year && $filter_moon) {
                $data = DB::table('portal_mstr')
                        ->join('portald_det', 'portald_det.portald_tr_id', '=', 'portal_mstr.portal_tr_id')
                        ->select('portald_tr_id', 'portald_po_nbr', 'portal_vend', 'portal_ps_nbr', 'portal_dlv_date', 'portal_user_id')
                        ->whereNotNull('portald_status')
                        ->whereMonth('portal_dlv_date', $filter_moon)
                        ->whereYear('portal_dlv_date', $filter_year)
                        ->orderBy('portal_dlv_date', 'ASC')
                        ->distinct()
                        ->get();
            } else {
                $data = DB::table('portal_mstr')
                        ->join('portald_det', 'portald_det.portald_tr_id', '=', 'portal_mstr.portal_tr_id')
                        ->select('portald_tr_id', 'portald_po_nbr', 'portal_vend', 'portal_ps_nbr', 'portal_dlv_date', 'portal_user_id')
                        ->whereNotNull('portald_status')
                        ->whereMonth('portal_dlv_date', $now_month)
                        ->orderBy('portal_dlv_date', 'ASC')
                        ->distinct()
                        ->get();
            }

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    $button = "<a class='mr-1' href='/detail-proccess-bc/" . base64_encode($data->portal_ps_nbr) . "' data-toggle='tooltip' data-placement='top' title='Sortir'>Show</a>";
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    	return view('QPR.qc_process_bc');
    }

    public function detailprocessBc($sj)
    {

        $data = DB::table('portald_det as pd')
            ->select('pd.portald_tr_id', 'pd.portald_po_nbr', 'pd.portald_part', 'pd.portald_line', 'pd.portald_status', 'pd.portald__int07', 'pm.portal_vend', 'pm.portal_ps_nbr', 'pd.portald__dec01')
            ->leftJoin('portal_mstr as pm', 'pd.portald_tr_id', '=', 'pm.portal_tr_id')
            ->whereNotNull('portald_status')
            ->where('portal_ps_nbr', base64_decode($sj))
            ->get();

        $result = [];
        foreach ($data as $key) {
            $desc = DB::table('SOAP_pt_mstr')->where('item_number', $key->portald_part)->first();
            array_push($result, [
                'line' => $key->portald_line,
                'item_number' => $key->portald_part,
                'deskripsi' => $desc->deskripsi1,
                'qty_ng' => $key->portald__dec01,
                'status' => $key->portald_status,
            ]);
        }

        return view('QPR.detail_proccess_barcode', compact('result'));
    }

    public function searchProcessBc(Request $request)
    {

        $month  = $request->get('mondate');
        $year   = $request->get('years');

        $data = DB::table('portald_det as pd')
            ->distinct()
            ->select('pd.portald_tr_id', 'pd.portald_po_nbr', 'pm.portal_vend', 'pm.portal_ps_nbr', 'pm.portal_dlv_date')
            ->leftJoin('portal_mstr as pm', 'pd.portald_tr_id', '=', 'pm.portal_tr_id')
            ->whereNotNull('portald_status')
            ->whereMonth('portal_dlv_date', $month)
            ->whereYear('portal_dlv_date', $year)
            ->orderBy('portal_dlv_date', 'asc')
            ->get();

        return response($data);
    }

    public function detailpackingslip()
    {
        return view('QPR.detail_packing_slip');
    }

    public function countermeasure() {

        $user = Auth::user();
        $kdsupp = $user->kode_supplier;

        $data = DB::table('qprl_master')->where('qprl_supplier',$kdsupp)->orderBy('qprl_id','DESC')->get();
        // dd($data);

        return view('QPR.counter_measure', compact('data'));
    }

    public function searchCounterMeasure(Request $request){

        $qprnomor  = $request->get('noqpr');

        $data = DB::table('qprl_master')->where('qprl_no',$qprnomor)->get();

        return response($data);
    }

    public function excelCounterMeasure(Request $request){
        $type = $request->get('typeExcel');
        $qprNo = $request->get('qprNoExcel');
        $supplier = $request->get('supplierIdExcel');
        $from = $request->get('fromExcel');
        $to = $request->get('toExcel');
        // ob_end_clean();
        // ob_start();

        return Excel::download(new CounterMeasureExport($type, $qprNo, $supplier, $from, $to), 'Counter-Measure-Supplier.xlsx');
    }

    public function storeAttachDocument(Request $request){

        $no_qpr = $request->qpr_no;
        $revisi = $request->revisi;


        $tampil_data = DB::table('qprl_master')->where('qprl_no',$no_qpr)->first();

        $supp_name = $tampil_data->qprl_supplier_name;
        $supp_kode = $tampil_data->qprl_supplier;
        $revisi = $tampil_data->qprl_revise;

        $app = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->approved) );
        $prep = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->prepared) );
        $prob = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->problem_desc) );
        $rec_prob = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->problem_rec) );
        $analysis = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->analysis) );
        $approp = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->approriate) );
        $confirm = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->confirmation) );
        $step1 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->step1) );
        $step2 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->step2) );
        $step3 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->step3) );
        $step4 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->step4) );
        $step5 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->step5) );
        $feed = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->fedback) );
        $desc = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->desc) );
        $pic = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->pic) );
        $duedate = $request->duedate;
        $desc1 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->desc1) );
        $pic1 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->pic1) );
        $duedate1 = $request->duedate1;
        $desc2 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->desc2) );
        $pic2 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->pic2) );
        $duedate2 = $request->duedate2;
        $desc3 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->desc3) );
        $pic3 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->pic3) );
        $duedate3 = $request->duedate3;
        $desc4 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->desc4) );
        $pic4 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->pic4) );
        $duedate4 = $request->duedate4;
        $desc5 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->desc5) );
        $pic5 = str_replace(array("'", '"', "&quot;"), "", htmlspecialchars($request->pic5) );
        $duedate5 = $request->duedate5;

        $readQPR = DB::table('qprl_master')
        ->where('qprl_no', $no_qpr)
        ->update(array(
            'qprl_status' => 'OK',
            'qprl_date_status' => Carbon::today(),
            'qprl_rp_date' => Carbon::today(),
            'qprl_rp_approved' => $app,
            'qprl_rp_prepared' => $prep,
            'qprl_rp_desc_problem' => $prob,
            'qprl_rp_rec_problem' => $prob,
            'qprl_rp_rec_problem' => $analysis,
            'qprl_rp_confirm' => $confirm,
            'qprl_rp_why1' => $step1,
            'qprl_rp_why2' => $step2,
            'qprl_rp_why3' => $step3,
            'qprl_rp_why4' => $step4,
            'qprl_rp_why5' => $step5,
            'qprl_rp_feed' => $feed,
            // 'qprl_rp_attachment' => $im_attc_dok, //noted
            'qprl_rp_approriate' => $desc,
            'qprl_rp_approriate_pic' => $pic,
            'qprl_rp_approriate_date' => $duedate,
            'qprl_rp_approriate_date2' => $duedate2,
            'qprl_rp_approriate_date3' => $duedate3,
            'qprl_rp_approriate_date4' => $duedate4,
            'qprl_rp_approriate_date5' => $duedate5,
            'qprl_rp_approriate1' => $desc1,
            'qprl_rp_approriate2' => $desc2,
            'qprl_rp_approriate3' => $desc3,
            'qprl_rp_approriate4' => $desc4,
            'qprl_rp_approriate5' => $desc5,
            'qprl_rp_approriate_pic1' => $pic1,
            'qprl_rp_approriate_pic2' => $pic2,
            'qprl_rp_approriate_pic3' => $pic3,
            'qprl_rp_approriate_pic4' => $pic4,
            'qprl_rp_approriate_pic5' => $pic5
        ));

        $readQPR_dtl = DB::table('qprl_dtl')
        ->where('qprl_no', $no_qpr)
        ->where('revisi', $revisi)
        ->update(array(
            'qprl_status' => 'OK',
            'qprl_date_status' => Carbon::today()
        ));

        $pic_email = DB::table('qpr_email_aaij')->select('email')->get();
        $address = "http://127.0.0.1:8000/qpr_report_l?id=".base64_encode($no_qpr);
        Mail::to($pic_email)->send(new MainCounterMeasureSupplier($no_qpr, $supp_name, $revisi, $supp_kode,$address));

        return redirect()->route('all-list-qpr');

    }

    public function sentmailvendorreminder(Request $request) {

        $user = Auth::user()->previllage;

        $no_qpr = $request->get('no_qpr');
        $supp_code = $request->get('supp_code');
        $revisi = $request->get('revisi');

        if ($request->get('email') == 'resend') {
            $supp_email = DB::table('qpr_email_supp')->where('supp_kode', $supp_code)->first();
            $supp_name = $supp_email->supp_name;
            $pic_email = DB::table('qpr_email_aaij')->pluck('email');
            $address = "http://supplier.akebono-astra.co.id/qpr_approval.php?".base64_encode("id=".$no_qpr."&level=supplier");

            Mail::to($supp_email->supp_pic_email)->send(new sentEmailVendorReminder($no_qpr, $supp_name, $revisi, $supp_code,$address));
        }else{
            $supp_email = DB::table('qpr_email_supp')->where('supp_kode', $supp_code)->first();
            $supp_name = $supp_email->supp_name;
            $pic_email = DB::table('qpr_email_aaij')->pluck('email');
            $address = "http://supplier.akebono-astra.co.id/qpr_approval.php?".base64_encode("id=".$no_qpr."&level=supplier");

            Mail::to($supp_email->supp_pic_email)->send(new sentEmailVendorReanswer($no_qpr, $supp_name, $revisi, $supp_code,$address));
        }

        toast('Send Email Successfully','success')->position('top-end')->autoClose(5000)->timerProgressBar();
        return redirect()->route('all-list-qpr');
    }
}
