<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Exports\AchievementExport;
class AchivementDeliverySupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('achivement-delivery-supplier.index');
       
    }


    public function generate(Request $request){
        $item       = $request->get('item');
        $po         = $request->get('po');
        $bulan      = $request->get('bulan');
        $tahun      = $request->get('tahun');
        $supp       = Auth::guard('pub_login')->user()->kode_supplier; 

        if($item != "" && $po != ""){
            $sql_cek_mfg = DB::select("SELECT distinct dnd_part from dn_mstr,dnd_det 
                    WHERE dn_vend='$supp' AND dn_po_nbr = '$po' AND dnd_part = '$item' AND month(dn_arrival_date)='$bulan' AND year(dn_arrival_date)='$tahun' AND dn_tr_id = dnd_tr_id ORDER BY dnd_part ASC");
        }else if($item != ""){
            $sql_cek_mfg = DB::select("SELECT distinct dnd_part from dn_mstr,dnd_det 
                    WHERE dn_vend='$supp' AND dnd_part = '$item' AND month(dn_arrival_date)='$bulan' AND year(dn_arrival_date)='$tahun' AND dn_tr_id = dnd_tr_id ORDER BY dnd_part ASC");
        }else if($po != ""){
            $sql_cek_mfg = DB::select("SELECT distinct dnd_part from dn_mstr,dnd_det 
                    WHERE dn_vend='$supp' AND dn_po_nbr = '$po' AND month(dn_arrival_date)='$bulan' AND year(dn_arrival_date)='$tahun' AND dn_tr_id = dnd_tr_id ORDER BY dnd_part ASC");
        }else{
            $sql_cek_mfg = DB::select("SELECT distinct dnd_part from dn_mstr,dnd_det 
                    WHERE dn_vend='$supp' AND dnd_part = 'ZZ232323' AND month(dn_arrival_date)='$bulan' AND year(dn_arrival_date)='$tahun' AND dn_tr_id = dnd_tr_id ORDER BY dnd_part ASC");
        }
        $first_day  = date('Y-m-d',strtotime($tahun.'-'.$bulan.'-'.'1')); 
        $last_day   = date('Y-m-t',strtotime($first_day));
        $jumHari    = date('t',strtotime($first_day));

        $tablenya = '';
        if(count($sql_cek_mfg) > 0){
            $tablenya .= '
                    <tr bg-color="blue">
                        <td align="center" bgcolor="FFCC66"><b>No</b></td>
                        <td align="center" bgcolor="FFCC66"><b>Item</b></td>
                        <td align="center" bgcolor="FFCC66"><b></b></td>';
                        
            for ($x = 1; $x <= $jumHari; $x++){
                $tablenya .= '<td width="100" align="center" bgcolor="FFCC66"><b>'.$x.'</b></td>';	
            }

            $tablenya .= '</tr>';
            $z = 1;
            $ambil_lpb = prh_hist($po);
            $value_array = array();
            foreach($ambil_lpb as $al){
                if($al['prhRcp_date'] >= $first_day && $al['prhRcp_date'] <= $last_day){
                    if(array_key_exists($al['prhRcp_date'], $value_array)){ 
                    
                        $value_array[$al['prhRcp_date']] += $al['prhRcvd'];
                    }else{
                        //else creat a new element in the array with current value
                        $value_array[$al['prhRcp_date']] = $al['prhRcvd'];
                    }
                }
            }

            $cek = array();
            $sum = 0;
            for ($a = 1; $a <= $jumHari; $a++){
                
                $loop_tanggal = $tahun."-".$bulan."-".$a;    
                $convert_date = date('Y-m-d',strtotime($loop_tanggal));
                if(!array_key_exists($convert_date,$value_array)){
                    array_push($cek,array(
                        $convert_date => 0
                    ));
                }else{
                    array_push($cek,array(
                        $convert_date => $value_array[$convert_date]
                    ));
                }
            }

            // for ($a = 1; $a <= $jumHari; $a++){
            //     $loop_tanggal = $tahun."-".$bulan."-".$a;    
            //     $b = $a-1;
            //     $convert_date = date('Y-m-d',strtotime($loop_tanggal));
            //     echo $convert_date.' - '.$cek[$b][$convert_date].'<br>';
            // }
            
                        
            $no = 0 ; 
            foreach($sql_cek_mfg as $s){	
                $no = $no + 1;
                $part = $s->dnd_part;

                //untuk menampilkan deskripsi item
                $tampil_deskripsi = DB::table('SOAP_pt_mstr')
                    ->where('item_number', $part)
                    ->where('pt_domain', 'AAIJ')
                    ->first(); 

                $deskripsi_item      = $tampil_deskripsi->deskripsi1;
                $type_item           = $tampil_deskripsi->desc_type;
                $item_deskripsi      = $deskripsi_item." ".$type_item; 

                $get_Perkanban = DB::table('dn_item_supp')
                ->where('kd_item', $part)
                ->where('kd_supp', 'AAIJ')
                ->first(); 
                if(empty($get_Perkanban)){
                    $Pcs_Kanban = 1;
                }else{
                    $Pcs_Kanban = $get_Perkanban->pcs_kanban;
                }

                $value_array = array();
                foreach($ambil_lpb as $al){
                    if($al['prhRcp_date'] >= $first_day && $al['prhRcp_date'] <= $last_day && $al['prhPart'] == $part){
                        if(array_key_exists($al['prhRcp_date'], $value_array)){ 
                        
                            $value_array[$al['prhRcp_date']] += $al['prhRcvd'];
                        }else{
                            //else creat a new element in the array with current value
                            $value_array[$al['prhRcp_date']] = $al['prhRcvd'];
                        }
                    }
                }

                $cek = array();
                $sum = 0;
                for ($a = 1; $a <= $jumHari; $a++){
                
                    $loop_tanggal = $tahun."-".$bulan."-".$a;    
                    $convert_date = date('Y-m-d',strtotime($loop_tanggal));
                    if(!array_key_exists($convert_date,$value_array)){
                        array_push($cek,array(
                            $convert_date => 0
                        ));
                    }else{
                        array_push($cek,array(
                            $convert_date => $value_array[$convert_date]
                        ));
                    }

                    $get_data_qty =  DB::table('dn_mstr')
                            ->join('dnd_det','dnd_det.dnd_tr_id','=','dn_mstr.dn_tr_id')
                            ->where('dn_vend', $supp)
                            ->where('dnd_part', $part)
                            ->where('dn_arrival_date', $convert_date)
                            ->sum('dnd_qty_order');

                    $tampung_qty[$convert_date] = $get_data_qty;

                    $Actual =  DB::table('portald_det')
                            // ->where('dn_vend', $supp)
                            ->where('portald_part', $part)
                            ->where('portald_dlv_date', $convert_date)
                            ->sum('portald_qty_ship');
                    $tampung_actual[$convert_date] = $Actual;

                    
                }
                
                $tablenya .= '
                <tr style="font-size: 10.8px;">
                    <td style="width:200px;" align="center" rowspan="8">'.$no.'</td>
                    <td style="width:200px;" align="center" rowspan="8">'.$part.' <br> <br><b>'.$item_deskripsi.'</b></td>
                </tr>
                <tr style="font-size: 10.8px;">
                    <td align="center">DN</td>';
                    $hari_akhir_a = date("t",strtotime($tahun."-".$bulan."-01"));
                    $tanggal_awal_a = $tahun."-".$bulan."-01";
                    $tanggal_akhir_a = $tahun."-".$bulan."-".$hari_akhir_a;
                    $data_a = array();

                    $total_qty_dn = 0;	

                    for ($a = 1; $a<=$jumHari; $a++){
                        $a      =  str_pad($a, 2, "0", STR_PAD_LEFT);
                        $bulan  =  str_pad($bulan, 2, "0", STR_PAD_LEFT);
                        
                        $tanggal_looping_a = $tahun."-".$bulan."-".$a;    

                        $get_data_qty = $tampung_qty[$tanggal_looping_a];
                        $tablenya .= '<td align="center">'.number_format($get_data_qty*$Pcs_Kanban).'</td>';
                    }

                    $tablenya .= '</tr>
                    <tr style="font-size: 10.8px;">
                        <td nowrap align="center">Actual By DN</td>';


                    for ($a = 1; $a<=$jumHari; $a++){
                        $a     =  str_pad($a, 2, "0", STR_PAD_LEFT);
                        $bulan =  str_pad($bulan, 2, "0", STR_PAD_LEFT);
                        
                        $tanggal_looping = $tahun."-".$bulan."-".$a;
                        $Actual =  $tampung_actual[$tanggal_looping];
                        $tablenya .= '<td align="center">'.number_format($Actual).'</td>';	
                    }
                $tablenya .= '</tr>';
                
                $tablenya .= '
                <tr style="font-size: 10.8px;">
                    <td align="center" nowrap>Minus By DN</td>';

                    for ($a = 1; $a<=$jumHari; $a++){
                        $a =  str_pad($a, 2, "0", STR_PAD_LEFT);
                        $bulan =  str_pad($bulan, 2, "0", STR_PAD_LEFT);
                        
                        $tanggal_looping = $tahun."-".$bulan."-".$a;
                        $get_data_qty_lagi = $tampung_qty[$tanggal_looping];
                        // dd($get_data_qty_lagi);
                        $Actual_lagi = $tampung_actual[$tanggal_looping];
                            
                        $tablenya .= '<td align="center">'.number_format(($Actual_lagi - $get_data_qty_lagi)).'</td>';
                    }
            
                $tablenya.='</tr>
                <tr style="font-size: 10.8px;">
                    <td align="center" nowrap>Actual By LPB</td>';
                    for ($a = 1; $a <= $jumHari; $a++){
                        $loop_tanggal = $tahun."-".$bulan."-".$a;    
                        $b = $a-1;
                        $convert_date = date('Y-m-d',strtotime($loop_tanggal));
                        $tablenya .= '<td align="center">'.$cek[$b][$convert_date].'</td>';
                    }
                    
                    $tablenya .= '</tr>';
                    
                    $tablenya .= '<tr style="font-size: 10.8px;">
                    <td align="center" nowrap>Minus By LPB</td>
                    ';
    
                    for ($a = 1; $a<= $jumHari; $a++){
                                                
                        $tanggal_looping_lpb = $tahun."-".$bulan."-".$a;
                        $b = $a-1;
                        $convert_date = date('Y-m-d',strtotime($tanggal_looping_lpb));
                        $get_dn_lpb = $tampung_qty[$convert_date];
                        
                        $list_minus_lpb = number_format(($cek[$b][$convert_date] - $get_dn_lpb));
                            
                        $tablenya .= '<td align="center">'.$list_minus_lpb.'</td>';
                    }
                    $tablenya .= '</tr>';
                    
                    $tablenya .= '<tr style="font-size: 10.8px;">
                    <td align="center" nowrap>Acc Minus BY DN</td>';

                    for ($a = 1; $a<=$jumHari; $a++){
                        $b =  str_pad($a - 1, 2, "0", STR_PAD_LEFT);
                        $a =  str_pad($a, 2, "0", STR_PAD_LEFT);
                        
                        $bulan =  str_pad($bulan, 2, "0", STR_PAD_LEFT);
                        
                        $tanggal_looping = $tahun."-".$bulan."-".$a;
                        $dn_acc =  $tampung_qty[$tanggal_looping];
                
                        $list_acc_minus = $dn_acc + 0;
                            
                        $tablenya .= '<td align="center">'.number_format($list_acc_minus).'</td>';
                    }
                    $tablenya .= '</tr>';

                    $tablenya .= '<tr style="font-size: 10.8px;">
                    <td align="center" nowrap>Acc Minus BY LPB</td>';

                    for ($a = 1; $a<=$jumHari; $a++){
                        
                        $tanggal_looping_lpb = $tahun."-".$bulan."-".$a;
                        $b = $a-1;
                        $convert_date = date('Y-m-d',strtotime($tanggal_looping_lpb));
                        $acc_lpb_min =  $cek[$b][$convert_date];
                
                        $list_acc_minus_lpb = $acc_lpb_min + 0;
                            
                        $tablenya .= '<td align="center">'.number_format($list_acc_minus_lpb).'</td>';
                    }
                    $tablenya .= '</tr>';
            
                }
        }else{
            $tablenya .= '<tr></tr>';
        }    
                return response($tablenya);
            
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function export_data(){
        $item       = $request->get('item');
        $po         = $request->get('po');
        $bulan      = $request->get('bulan');
        $tahun      = $request->get('tahun');
        $supp       = Auth::guard('pub_login')->user()->kode_supplier; 
        return Excel::download(new AchievementExport($item, $po, $bulan,$tahun,$supp), 'Achievement-Delivery-Supplier.xlsx');
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
