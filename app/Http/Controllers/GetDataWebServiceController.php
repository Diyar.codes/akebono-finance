<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Storage;
use App\SOAPPubBusinessRelation;
use App\SOAPPoMstr;
use App\SOAPPoDetail;
use App\SOAPPtMstr;
use App\PtMstrBydesc;

class GetDataWebServiceController extends Controller
{
	public function getDataPO(){
        $url = "https://supplier.akebono-astra.co.id/wsatdw/po_mstr.php";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('po_mstr.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/po_mstr.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
        echo json_encode($arrOutput);
		SOAPPoMstr::truncate();

		$total_row = count($arrOutput['ttPO']['ttPORow']);
		for($i = 0 ; $i < $total_row; $i++){

			$po_nbr = $arrOutput['ttPO']['ttPORow'][$i]['po_number'];
			$po_due_date = $arrOutput['ttPO']['ttPORow'][$i]['POdue_date'];
			$kode_supp = $arrOutput['ttPO']['ttPORow'][$i]['kode_supplier'];
			$po_status = $arrOutput['ttPO']['ttPORow'][$i]['po_status'];
			$nama_supplier = $arrOutput['ttPO']['ttPORow'][$i]['nama_supplier'];
			$tanggal = \Carbon\Carbon::parse($po_due_date)->format('Y-m-d');
            $pocurr = $arrOutput['ttPO']['ttPORow'][$i]['pocurr'];

			$data = new SOAPPoMstr;
			$data->po_number = $po_nbr;
			$data->POdue_date = $tanggal;
			$data->kode_supplier = $kode_supp;
			$data->nama_supplier = $nama_supplier;
			if($po_status){
				$data->po_status = $po_status;
			}else{
                $data->po_status = '-';
            }
            $data->po_domain = 'AAIJ';
            $data->po_currency = $pocurr;
			$data->save();
		}

	}

    public function get_supplier(){
        $url = "https://supplier.akebono-astra.co.id/wsatdw/master_view_test.php";
        $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $headers = array(
                "Accept: application/xml",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $resp = curl_exec($curl);
            curl_close($curl);

            $find = "&lt;/soapenv:Envelope&gt;";
            $arr = explode($find, $resp, 2);
            $first = $arr[1];

            $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
            $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
            $arrrep = ["&","\"",">","<", "", "","", "","", ""];
            $rep = str_replace($arrfind,$arrrep,$first);

            Storage::put('supplier.xml', $rep);

            libxml_use_internal_errors(TRUE);

            $objXmlDocument = simplexml_load_file(storage_path('app')."/supplier.xml");

            if ($objXmlDocument === FALSE) {
                echo "There were errors parsing the XML file.\n";
            foreach(libxml_get_errors() as $error) {
                echo $error->message;
            }
            exit;
            }

            $objJsonDocument = json_encode($objXmlDocument);
            $arrOutput = json_decode($objJsonDocument, TRUE);
            $total_row = count($arrOutput['ttSupplier']['ttSupplierRow']);
            echo json_encode($arrOutput['ttSupplier']['ttSupplierRow']);
            SOAPPubBusinessRelation::truncate();	
            for($i = 0 ; $i < $total_row; $i++){

                $ct_vd_addr = $arrOutput['ttSupplier']['ttSupplierRow'][$i]['ct_vd_addr'];
                $ct_vd_type = $arrOutput['ttSupplier']['ttSupplierRow'][$i]['ct_vd_type'];
                $ct_ad_name = $arrOutput['ttSupplier']['ttSupplierRow'][$i]['ct_ad_name'];
            //     // $tanggal = \Carbon\Carbon::parse($po_due_date)->format('Y-m-d');
    
                $data = new SOAPPubBusinessRelation;
                $data->ct_vd_addr = $ct_vd_addr;
                if($ct_vd_type){
                    $data->ct_vd_type = $ct_vd_type;
                }
                $data->ct_ad_name = $ct_ad_name;
                $data->save();
            }
	}

    // public function getDetail_PO('sim','ttPO_detail','00578A21'){

    // }

	public function getPODetail(){
        $no_po = '00937A21';
		$url = "https://supplier.akebono-astra.co.id/wsatdw/po_detail_item.php?no_po=".$no_po;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('po_detail.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/po_detail.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
        echo json_encode($arrOutput);
		$total_row = count($arrOutput['ttPO_detail']['ttPO_detailRow']);
		SOAPPoDetail::truncate();
		for($i = 0 ; $i < $total_row; $i++){

			$item_number = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_number'];
			$no_po = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['no_po'];
			$qty_po = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['qty_po'];
			$qty_receive = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['qty_receive'];
			$no_pp = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['no_pp'];
			$item_deskripsi = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_deskripsi'];
			$item_price = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_price'];
			$item_disc = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_disc'];
			$po_status = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['po_status'];
			$po_um = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['po_um'];
            $podRequest = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['podRequest'];
			$podLoc = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['podLoc'];
            $line = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['line'];
            $item_type = $arrOutput['ttPO_detail']['ttPO_detailRow'][$i]['item_type'];

			$data = new SOAPPoDetail;
			$data->item_number = $item_number;
			$data->no_po = $no_po;
			$data->qty_po = $qty_po;
			$data->qty_receive = $qty_receive;
			$data->no_pp = $no_pp;
			if($item_deskripsi){
				$data->item_deskripsi = $item_deskripsi;
			}
			$data->item_price = $item_price;
			$data->item_disc = $item_disc;
            $data->item_type = $item_type;
			if($po_status){
				$data->po_status = $po_status;
			}
			$data->po_um = $po_um;
            $data->podRequest = $podRequest;
            if($po_status){
				$data->podLoc = $podLoc;
			}
            $data->line = $line;
            
			$data->save();
		}

	}

    public function getPtMstrByDesc(){
        $no_po = '00937A21';
		$url = "https://supplier.akebono-astra.co.id/wsatdw/pt_mstrByDesc.php";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('po_detail.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/po_detail.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
        echo json_encode($arrOutput);
		$total_row = count($arrOutput['ttItem_desc']['ttItem_descRow']);
		PtMstrBydesc::truncate();
        for($i = 0 ; $i < $total_row; $i++){

            $item_number = $arrOutput['ttItem_desc']['ttItem_descRow'][$i]['item_number'];
            if($arrOutput['ttItem_desc']['ttItem_descRow'][$i]['deskripsi1']){
                $deskripsi1 = $arrOutput['ttItem_desc']['ttItem_descRow'][$i]['deskripsi1'];
            }else{
                $deskripsi1 = '-';
            }
			
            if($arrOutput['ttItem_desc']['ttItem_descRow'][$i]['deskripsi2']){
                $deskripsi2 = $arrOutput['ttItem_desc']['ttItem_descRow'][$i]['deskripsi2'];
            }else{
                $deskripsi2 = "-";
            }
            if($arrOutput['ttItem_desc']['ttItem_descRow'][$i]['um']){
                $um = $arrOutput['ttItem_desc']['ttItem_descRow'][$i]['um'];
            }else{
                $um = "-";
            }
            if($arrOutput['ttItem_desc']['ttItem_descRow'][$i]['deskripsi1']){
                $buyer_planner = $arrOutput['ttItem_desc']['ttItem_descRow'][$i]['buyer_planner'];
            }else{
                $buyer_planner = '-';
            }
            if($arrOutput['ttItem_desc']['ttItem_descRow'][$i]['desc_type']){
                $desc_type = $arrOutput['ttItem_desc']['ttItem_descRow'][$i]['desc_type'];
            }else{
                $desc_type = '-';
            }			
            if($arrOutput['ttItem_desc']['ttItem_descRow'][$i]['nama_supplier']){
                $nama_supplier = $arrOutput['ttItem_desc']['ttItem_descRow'][$i]['nama_supplier'];
            }else{
                $nama_supplier = "-";
            }

            $data = new PtMstrBydesc;
            $data->item_number = $item_number;
			$data->deskripsi1 = $deskripsi1;
		    $data->deskripsi2 = $deskripsi2;
			$data->um = $um;
			$data->buyer_planner = $buyer_planner;
			$data->desc_type = $desc_type;
			$data->nama_supplier = $nama_supplier;
            $data->pt_domain = 'AAIJ';
			$data->save();
		}
		

	}

	public function getDataPtMstr(){
        $item = 'SPHC-181185C';
		$url = "https://supplier.akebono-astra.co.id/wsatdw/pt_mstr.php?item=".$item;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "Accept: application/xml",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $find = "&lt;/soapenv:Envelope&gt;";
        $arr = explode($find, $resp, 2);
        $first = $arr[1];

        $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
        $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
        $arrrep = ["&","\"",">","<", "", "","", "","", ""];
        $rep = str_replace($arrfind,$arrrep,$first);

        Storage::put('pt_mstr.xml', $rep);

        libxml_use_internal_errors(TRUE);

        $objXmlDocument = simplexml_load_file(storage_path('app')."/pt_mstr.xml");

        if ($objXmlDocument === FALSE) {
            echo "There were errors parsing the XML file.\n";
        foreach(libxml_get_errors() as $error) {
            echo $error->message;
        }
        exit;
        }

        $objJsonDocument = json_encode($objXmlDocument);
        $arrOutput = json_decode($objJsonDocument, TRUE);
		echo json_encode($arrOutput);
		$total_row = count($arrOutput['ttItem']['ttItemRow']);
      
	// 	// echo $total_row;
		// SOAPPtMstr::truncate();
		// for($i = 0 ; $i < $total_row; $i++){

			$item_number = $arrOutput['ttItem']['ttItemRow']['item_number'];  dd($item_number);
			$deskripsi1 = $arrOutput['ttItem']['ttItemRow']['deskripsi1'];
            if($arrOutput['ttItem']['ttItemRow']['deskripsi2']){
                $deskripsi2 = $arrOutput['ttItem']['ttItemRow']['deskripsi2'];
            }else{
                $deskripsi2 = "-";
            }
			$um = $arrOutput['ttItem']['ttItemRow']['um'];
			$buyer_planner = $arrOutput['ttItem']['ttItemRow']['buyer_planner'];
			$desc_type = $arrOutput['ttItem']['ttItemRow']['desc_type'];
            if($arrOutput['ttItem']['ttItemRow']['nama_supplier']){
                $nama_supplier = $arrOutput['ttItem']['ttItemRow']['nama_supplier'];
            }else{
                $nama_supplier = "-";
            }
		// 	// $tanggal = \Carbon\Carbon::parse($po_due_date)->format('Y-m-d');

            SOAPPtMstr::updateOrCreate(
                ['item_number' => $item_number],
                [
                    'deskripsi1'    => $deskripsi1, 
                    'deskripsi2'    => $deskripsi2,
                    'um'            => $um, 
                    'buyer_planner' => $buyer_planner, 
                    'desc_type'     => $desc_type, 
                    'nama_supplier' => $nama_supplier, 
                    'pt_domain'     => 'AAIJ', 
                ]
            );
			// $data->item_number = $item_number;
			// $data->deskripsi1 = $deskripsi1;
			// if($deskripsi2){
			// 	$data->deskripsi2 = $deskripsi2;
			// }
			// $data->um = $um;
			// $data->buyer_planner = $buyer_planner;
			// $data->desc_type = $desc_type;
			// if($nama_supplier){
			// 	$data->nama_supplier = $nama_supplier;
			// }
            // $data->pt_domain = 'AAIJ';
			// $data->save();
		// }

	}
}

