<?php

namespace App\Http\Controllers;

use SimpleXMLElement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\ItemKanban;
use App\BusinesRelation;
use Illuminate\Support\Facades\Storage;
use Mtownsend\XmlToArray\XmlToArray;
use App\SOAPPubBusinessRelation;
use App\Ptmstr;

class ItemKanbanController extends Controller
{
    public function index()
    {
        return view('item-kanbans.index');
    }

    public function searchItemKanban(Request $request) {
        $queryItemNumber = $request->get('queryItemNumber');
        $querySupplierId = $request->get('querySupplierId');
        $querySupplierName = $request->get('querySupplierName');
        if($queryItemNumber != '' && $querySupplierId != '' && $querySupplierName != '') {
            $data = DB::table('dn_item_supp')
            // ->join('SOAP_pt_mstr', 'dn_item_supp.kd_item', '=', 'SOAP_pt_mstr.item_number')
            ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_item_supp.kd_supp')
            ->select('dn_item_supp.id_item', 'dn_item_supp.kd_item', 'dn_item_supp.kd_supp', 'dn_item_supp.back_no', 'dn_item_supp.pcs_kanban', 'dn_item_supp.pallet_kanban','SOAP_Pub_Business_Relation.ct_ad_name')
            // ->select('dn_item_supp.*', 'SOAP_pt_mstr.um', 'SOAP_pt_mstr.deskripsi1', 'SOAP_pt_mstr.deskripsi2' ,'SOAP_Pub_Business_Relation.ct_ad_name')
            ->where('kd_item', 'like', '%' . $queryItemNumber . '%')
            ->where('kd_supp', 'like', '%' . $querySupplierId . '%')
            ->where('SOAP_Pub_Business_Relation.ct_ad_name', 'like', '%' . $querySupplierName . '%')
            ->where('dn_item_supp.kd_item', '<>', '')
            ->where('dn_item_supp.kd_supp', '<>', '')
            ->orderBy('kd_supp', 'ASC')
            ->orderBy('id_item', 'ASC')
            ->distinct()
            ->get();
        } elseif($queryItemNumber != '' && $querySupplierId != '') {
            $data = DB::table('dn_item_supp')
            // ->join('SOAP_pt_mstr', 'dn_item_supp.kd_item', '=', 'SOAP_pt_mstr.item_number')
            ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_item_supp.kd_supp')
            ->select('dn_item_supp.id_item', 'dn_item_supp.kd_item', 'dn_item_supp.kd_supp', 'dn_item_supp.back_no', 'dn_item_supp.pcs_kanban', 'dn_item_supp.pallet_kanban','SOAP_Pub_Business_Relation.ct_ad_name')
            // ->select('dn_item_supp.*', 'SOAP_pt_mstr.um', 'SOAP_pt_mstr.deskripsi1', 'SOAP_pt_mstr.deskripsi2' ,'SOAP_Pub_Business_Relation.ct_ad_name')
            ->where('kd_item', 'like', '%' . $queryItemNumber . '%')
            ->where('kd_supp', 'like', '%' . $querySupplierId . '%')
            ->where('dn_item_supp.kd_item', '<>', '')
            ->where('dn_item_supp.kd_supp', '<>', '')
            ->orderBy('kd_supp', 'ASC')
            ->orderBy('id_item', 'ASC')
            ->distinct()
            ->get();
        } elseif($queryItemNumber != '' && $querySupplierName != '') {
            $data = DB::table('dn_item_supp')
            // ->join('SOAP_pt_mstr', 'dn_item_supp.kd_item', '=', 'SOAP_pt_mstr.item_number')
            ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_item_supp.kd_supp')
            ->select('dn_item_supp.id_item', 'dn_item_supp.kd_item', 'dn_item_supp.kd_supp', 'dn_item_supp.back_no', 'dn_item_supp.pcs_kanban', 'dn_item_supp.pallet_kanban','SOAP_Pub_Business_Relation.ct_ad_name')
            // ->select('dn_item_supp.*', 'SOAP_pt_mstr.um', 'SOAP_pt_mstr.deskripsi1', 'SOAP_pt_mstr.deskripsi2' ,'SOAP_Pub_Business_Relation.ct_ad_name')
            ->where('kd_item', 'like', '%' . $queryItemNumber . '%')
            ->where('SOAP_Pub_Business_Relation.ct_ad_name', 'like', '%' . $querySupplierName . '%')
            ->where('dn_item_supp.kd_item', '<>', '')
            ->where('dn_item_supp.kd_supp', '<>', '')
            ->orderBy('kd_supp', 'ASC')
            ->orderBy('id_item', 'ASC')
            ->distinct()
            ->get();
        } elseif($querySupplierId != '' && $querySupplierName != '') {
            $data = DB::table('dn_item_supp')
            // ->join('SOAP_pt_mstr', 'dn_item_supp.kd_item', '=', 'SOAP_pt_mstr.item_number')
            ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_item_supp.kd_supp')
            ->select('dn_item_supp.id_item', 'dn_item_supp.kd_item', 'dn_item_supp.kd_supp', 'dn_item_supp.back_no', 'dn_item_supp.pcs_kanban', 'dn_item_supp.pallet_kanban','SOAP_Pub_Business_Relation.ct_ad_name')
            // ->select('dn_item_supp.*', 'SOAP_pt_mstr.um', 'SOAP_pt_mstr.deskripsi1', 'SOAP_pt_mstr.deskripsi2' ,'SOAP_Pub_Business_Relation.ct_ad_name')
            ->where('kd_supp', 'like', '%' . $querySupplierId . '%')
            ->where('SOAP_Pub_Business_Relation.ct_ad_name', 'like', '%' . $querySupplierName . '%')
            ->where('dn_item_supp.kd_item', '<>', '')
            ->where('dn_item_supp.kd_supp', '<>', '')
            ->orderBy('kd_supp', 'ASC')
            ->orderBy('id_item', 'ASC')
            ->distinct()
            ->get();
        } elseif($queryItemNumber != '') {
            $data = DB::table('dn_item_supp')
            // ->join('SOAP_pt_mstr', 'dn_item_supp.kd_item', '=', 'SOAP_pt_mstr.item_number')
            ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_item_supp.kd_supp')
            ->select('dn_item_supp.id_item', 'dn_item_supp.kd_item', 'dn_item_supp.kd_supp', 'dn_item_supp.back_no', 'dn_item_supp.pcs_kanban', 'dn_item_supp.pallet_kanban','SOAP_Pub_Business_Relation.ct_ad_name')
            // ->select('dn_item_supp.*', 'SOAP_pt_mstr.um', 'SOAP_pt_mstr.deskripsi1', 'SOAP_pt_mstr.deskripsi2' ,'SOAP_Pub_Business_Relation.ct_ad_name')
            ->where('kd_item', 'like', '%' . $queryItemNumber . '%')
            ->where('dn_item_supp.kd_item', '<>', '')
            ->where('dn_item_supp.kd_supp', '<>', '')
            ->orderBy('kd_supp', 'ASC')
            ->orderBy('id_item', 'ASC')
            ->distinct()
            ->get();
        } elseif($querySupplierId != '') {
            $data = DB::table('dn_item_supp')
            // ->join('SOAP_pt_mstr', 'dn_item_supp.kd_item', '=', 'SOAP_pt_mstr.item_number')
            ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_item_supp.kd_supp')
            // ->select('dn_item_supp.*', 'SOAP_pt_mstr.um', 'SOAP_pt_mstr.deskripsi1', 'SOAP_pt_mstr.deskripsi2' ,'SOAP_Pub_Business_Relation.ct_ad_name')
            ->select('dn_item_supp.id_item', 'dn_item_supp.kd_item', 'dn_item_supp.kd_supp', 'dn_item_supp.back_no', 'dn_item_supp.pcs_kanban', 'dn_item_supp.pallet_kanban','SOAP_Pub_Business_Relation.ct_ad_name')
            ->where('kd_supp', 'like', '%' . $querySupplierId . '%')
            ->where('dn_item_supp.kd_item', '<>', '')
            ->where('dn_item_supp.kd_supp', '<>', '')
            ->orderBy('kd_supp', 'ASC')
            ->orderBy('id_item', 'ASC')
            ->distinct()
            ->get();
        } elseif($querySupplierName != '') {
            $data = DB::table('dn_item_supp')
            // ->join('SOAP_pt_mstr', 'dn_item_supp.kd_item', '=', 'SOAP_pt_mstr.item_number')
            ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_item_supp.kd_supp')
            // ->select('dn_item_supp.*', 'SOAP_pt_mstr.um', 'SOAP_pt_mstr.deskripsi1', 'SOAP_pt_mstr.deskripsi2' ,'SOAP_Pub_Business_Relation.ct_ad_name')
            ->select('dn_item_supp.id_item', 'dn_item_supp.kd_item', 'dn_item_supp.kd_supp', 'dn_item_supp.back_no', 'dn_item_supp.pcs_kanban', 'dn_item_supp.pallet_kanban','SOAP_Pub_Business_Relation.ct_ad_name')
            ->where('SOAP_Pub_Business_Relation.ct_ad_name', 'like', '%' . $querySupplierName . '%')
            ->where('dn_item_supp.kd_item', '<>', '')
            ->where('dn_item_supp.kd_supp', '<>', '')
            ->orderBy('kd_supp', 'ASC')
            ->orderBy('id_item', 'ASC')
            ->distinct()
            ->get();
        } else {
            $data = DB::table('dn_item_supp')
            // ->join('SOAP_pt_mstr', 'dn_item_supp.kd_item', '=', 'SOAP_pt_mstr.item_number')
            ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_item_supp.kd_supp')
            // ->select('dn_item_supp.kd_item', 'dn_item_supp.kd_supp', 'dn_item_supp.back_no', 'dn_item_supp.pcs_kanban', 'dn_item_supp.pallet_kanban','SOAP_pt_mstr.um', 'SOAP_pt_mstr.deskripsi1', 'SOAP_pt_mstr.deskripsi2' ,'SOAP_Pub_Business_Relation.ct_ad_name')
            ->select('dn_item_supp.id_item', 'dn_item_supp.kd_item', 'dn_item_supp.kd_supp', 'dn_item_supp.back_no', 'dn_item_supp.pcs_kanban', 'dn_item_supp.pallet_kanban','SOAP_Pub_Business_Relation.ct_ad_name')
            ->where('dn_item_supp.kd_item', '<>', '')
            ->where('dn_item_supp.kd_supp', '<>', '')
            ->orderBy('kd_supp', 'ASC')
            ->orderBy('id_item', 'ASC')
            ->distinct()
            ->limit(10)
            // ->limit(10)
            ->get();
        }

        $main = '';

        foreach($data as $row){
            $soapPtMstr = getDataPtMstrByD($row->kd_item);
            if($soapPtMstr == null) {
                $desc2 = '-';
            } else {
                if($soapPtMstr['deskripsi2'] == null) {
                    $desc2 = '-';
                } else {
                    $desc2 = $soapPtMstr['deskripsi2'];
                }
            }

            // <td class="text-wrap text-dark-manually">'.$row->um.'</td>
            // <td class="text-wrap text-dark-manually">'.$row->deskripsi1.'</td>
            // <td class="text-wrap text-dark-manually">'.$row->deskripsi2.'</td>
            if(!empty($soapPtMstr)) {
                $main .= '<tr class="text-center">
                    <td class="text-wrap text-dark-manually">'.$row->kd_item.'</td>
                    <td class="text-wrap text-dark-manually">'.$soapPtMstr['um'].'</td>
                    <td class="text-wrap text-dark-manually">'.$row->kd_supp.'</td>
                    <td class="text-wrap text-dark-manually">'.$row->ct_ad_name.'</td>
                    <td class="text-wrap text-dark-manually">'.$soapPtMstr['deskripsi1'].'</td>
                    <td class="text-wrap text-dark-manually">'.$desc2.'</td>
                    <td class="text-wrap text-dark-manually"><input type="number" value="'.$row->back_no.'" style="min-width:80px; max-width: 81px" name="back_no[]"></td>
                    <td class="text-wrap text-dark-manually"><input type="number" value="'.$row->pcs_kanban.'" style="min-width:80px; max-width: 81px" name="pcs_kanban[]"></td>
                    <td class="text-wrap text-dark-manually"><input type="number" value="'.$row->pallet_kanban.'" style="min-width:80px; max-width: 81px" name="pallet_kanban[]"></td>
                </tr>';
            }
        }

        $data = [
            'main' => $main,
        ];

        return response($data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'item_number'    => 'required',
            'desc'           => 'required',
            'code'           => 'required',
            'back_no'        => 'required',
            'snp_kanban'     => 'required',
            'kanban_pall'    => 'required'
        ]);

        try {
            // DB::beginTransaction();
            // $url = "https://supplier.akebono-astra.co.id/wsatdw/master_view_test.php";
            // get_supplier($url);

            ItemKanban::create([
                'kd_item'       => $request->input('item_number'),
                'item_desc'     => $request->input('desc'),
                'kd_supp'       => $request->input('code'),
                'qty'           => 0,
                'back_no'       => $request->input('back_no'),
                'pcs_kanban'    => $request->input('snp_kanban'),
                'pallet_kanban' => $request->input('kanban_pall'),
            ]);

            DB::commit();

            $response = [
                'status'     => 'success',
                'message'    => 'Stored data',
            ];

            return response()->json($response, 201);
        } catch (\Exception $e) {
            DB::rollBack();

            // alert()->error('error',$e->getMessage());
            // return redirect()->back();

            $response = [
                'status'     => 'success',
                'message'    => $e->getMessage(),
            ];

            return response()->json($response, 500);
        }
    }

    public function update(Request $request, $id)
    {
        $data = DB::table('dn_item_supp')
            // ->join('SOAP_pt_mstr', 'dn_item_supp.kd_item', '=', 'SOAP_pt_mstr.item_number')
            ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_item_supp.kd_supp')
            ->select('dn_item_supp.id_item','dn_item_supp.kd_item', 'dn_item_supp.kd_supp', 'dn_item_supp.back_no', 'dn_item_supp.pcs_kanban', 'dn_item_supp.pallet_kanban','SOAP_Pub_Business_Relation.ct_ad_name')
            // ->select('dn_item_supp.*', 'SOAP_pt_mstr.um', 'SOAP_pt_mstr.deskripsi1', 'SOAP_pt_mstr.deskripsi2' ,'SOAP_Pub_Business_Relation.ct_ad_name')
            ->where('kd_item', 'like', '%' . $id . '%')
            ->where('dn_item_supp.kd_item', '<>', '')
            ->where('dn_item_supp.kd_supp', '<>', '')
            ->orderBy('kd_supp', 'ASC')
            ->orderBy('id_item', 'ASC')
            ->distinct()
            ->get();

        $end = [];
        foreach($data as $co) {
            $soapPtMstr = getDataPtMstrByD($co->kd_item);

            if(!empty($soapPtMstr)) {
                $end[] = $co;
            }
        }

        if(sizeof($data) == 0) {
            $data1 = DB::table('dn_item_supp')
            // ->join('SOAP_pt_mstr', 'dn_item_supp.kd_item', '=', 'SOAP_pt_mstr.item_number')
            ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'dn_item_supp.kd_supp')
            // ->select('dn_item_supp.*', 'SOAP_pt_mstr.um', 'SOAP_pt_mstr.deskripsi1', 'SOAP_pt_mstr.deskripsi2' ,'SOAP_Pub_Business_Relation.ct_ad_name')
            ->select('dn_item_supp.id_item', 'dn_item_supp.kd_item', 'dn_item_supp.kd_supp', 'dn_item_supp.back_no', 'dn_item_supp.pcs_kanban', 'dn_item_supp.pallet_kanban','SOAP_Pub_Business_Relation.ct_ad_name')
            ->where('kd_supp', 'like', '%' . $id . '%')
            ->where('dn_item_supp.kd_item', '<>', '')
            ->where('dn_item_supp.kd_supp', '<>', '')
            ->orderBy('kd_supp', 'ASC')
            ->orderBy('id_item', 'ASC')
            ->distinct()
            ->get();

            $end1 = [];
            foreach($data1 as $cod) {
                $soapPtMstr = getDataPtMstrByD($cod->kd_item);

                if(!empty($soapPtMstr)) {
                    $end1[] = $cod;
                }
            }

            try {
                if(count($end1) > 0) {
                    foreach($end1 as $item=>$v) {
                        ItemKanban::where('id_item', $v->id_item)->update([
                            'back_no'         => $request->back_no[$item],
                            'pcs_kanban'      => $request->pcs_kanban[$item],
                            'pallet_kanban'   => $request->pallet_kanban[$item],
                        ]);
                    }
                }

                $response = [
                    'status'     => 'success',
                    'message'    => 'Stored data',
                ];

                return response()->json($response, 201);
            } catch (\Exception $e) {
                DB::rollBack();

                $response = [
                    'status' => false,
                    'message' => $e->getMessage()
                ];

                return response()->json($response, 500);
            }
        }

        try {
            if(count($end) > 0) {
                foreach($end as $item=>$v) {
                    ItemKanban::where('id_item', $v->id_item)->update([
                        'back_no'         => $request->back_no[$item],
                        'pcs_kanban'      => $request->pcs_kanban[$item],
                        'pallet_kanban'   => $request->pallet_kanban[$item],
                    ]);
                }
            }

            $response = [
                'status'     => 'success',
                'message'    => 'Stored data',
            ];

            return response()->json($response, 201);
        } catch (\Exception $e) {
            DB::rollBack();

            alert()->error('error',$e->getMessage());
            return redirect()->back();
        }
    }

    public function destroy(ItemKanban $itemKanban)
    {
    }
}
