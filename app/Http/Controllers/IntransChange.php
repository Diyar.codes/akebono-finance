<?php

namespace App\Http\Controllers;

use App\Mail\sendEmail;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class IntransChange extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $bulan = $request->get('bulan');
            $tahun = $request->get('tahun');
            $data = DB::table('PO_change_type')
                ->whereMonth('effdate', $bulan)
                ->whereYear('effdate', $tahun)
                ->get();
            if (count($data) < 1) {
                echo json_encode([
                    'status' => false,
                    'data' => 'data kosong'
                ]);
            } else {
                echo json_encode([
                    'status' => true,
                    'data' => $data
                ]);
            }
        } else {
            $data = DB::table('PO_change_type')
                ->whereMonth('effdate', date('m'))
                ->whereYear('effdate', date('Y'))
                ->get();

            return view('intrans-change.index', ['data' => $data]);
        }

    }

    public function addChangePo(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->pur_ord)) {
                $data = [];
                $poDetail = po_detail($request->pur_ord);

                $no = 0;
                foreach ($poDetail as $row) {
                    $data[$no]['item_number'] = $row['item_number'];
                    $data[$no]['line'] = $row['line'];
                    $data[$no]['no_po'] = $row['no_po'];
                    $data[$no]['qty_po'] = $row['qty_po'];
                    $data[$no]['qty_receive'] = $row['qty_receive'];
                    $data[$no]['qty_rtnd'] = $row['qty_rtnd'];
                    $data[$no]['no_pp'] = $row['no_pp'];
                    $data[$no]['item_deskripsi'] = $row['item_deskripsi'];
                    $data[$no]['item_price'] = $row['item_price'];
                    $data[$no]['item_disc'] = $row['item_disc'];
                    $data[$no]['po_status'] = $row['po_status'];
                    $data[$no]['podRequest'] = $row['podRequest'];
                    $data[$no]['podLoc'] = $row['podLoc'];
                    $data[$no]['po_um'] = $row['po_um'];
                    $data[$no]['ptPm_code'] = $row['ptPm_code'];

                    $ptMstr = pt_mstr($row['item_number']);
                    $data[$no]['deskripsi1'] = $ptMstr['deskripsi1'];
                    $data[$no]['deskripsi2'] = $ptMstr['deskripsi2'];
                    $data[$no]['um'] = $ptMstr['um'];
                    $data[$no]['buyer_planner'] = $ptMstr['buyer_planner'];
                    $data[$no]['desc_type'] = $ptMstr['desc_type'];
                    $data[$no]['lokasi_item'] = $ptMstr['lokasi_item'];

                    $no++;
                }

                if (count($data) < 1) {
                    echo json_encode([
                        'status' => false,
                        'data' => 'data kosong'
                    ]);
                } else {
                    echo json_encode([
                        'status' => true,
                        'data' => $data
                    ]);
                }
            } else if (!empty($request->effdate)) {
                echo json_encode(['status' => true, 'data' => $request->effdate]);
            } else if (!empty($request->approval)) {
                $data = DB::table('PO_change_approval')
                    ->where('approval_active', 'T')->get();
                echo json_encode(['status' => true, 'data' => $data]);
            }
        } else {
            return view('intrans-change.add_change_po');
        }
    }

    public function saveChangePo(Request $request)
    {
        $data = $request->all();

        $jumlahData = $data['no_jumlah'];

        $check = 0;

        for ($i = 1; $i <= $jumlahData; $i++) {
            if (isset($_POST["chkCheck" . $i])) {
                $pilih          = $_POST["chkCheck" . $i];
                $type_po        = $_POST["txt_type_Po" . $i];
                $type_po_rev    = $_POST["txt_type_Po_rev" . $i];

                if ($pilih == "OK") {
                    $check++;
                    if(strlen($type_po) > 5 || strlen($type_po) > 5){
                        alert()->error('Error', 'Type and Revisi Type cannot be longer than 5 letters');
                        return redirect()->back();
                    }
                }
            }
        }
        if($check < 1){
            alert()->error('Error', 'Line cannot empty');
            return redirect()->back();
        }

        $poNumber = $data['txt_po'];

        $bulan = date('m');
        $tahun = date('Y');

        
        $no_memo = DB::select("SELECT isnull(max(SUBSTRING(transaksi_id,1,3)),0)+1 as nomor_transaksi FROM PO_change_type
        WHERE year(effdate) = $tahun");

        if (count($no_memo) < 1) {
            $memo_no = $poNumber;
        }

        $memo_no = str_pad($no_memo[0]->nomor_transaksi, 3, "0", STR_PAD_LEFT);
        $transaksi_id = $memo_no . "/AAI/" . $bulan . "/" . $tahun;
        $approval_po    = $_POST["txt_approval_name"];
        $approval_email = $_POST["txt_approval_email"];
        
        for ($i = 1; $i <= $jumlahData; $i++) {
            if (isset($_POST["chkCheck" . $i])) {
                $no_po          = $_POST["txt_po"];
                $pilih          = $_POST["chkCheck" . $i];
                $line           = $_POST["line" . $i];
                $item           = $_POST["txt_item" . $i];
                $desc           = $_POST["txt_desc" . $i];
                $type_po        = $_POST["txt_type_Po" . $i];
                $type_po_rev    = $_POST["txt_type_Po_rev" . $i];

                if ($pilih == "OK") {
                    DB::table('PO_change_type')->insert([
                        'transaksi_id' => $transaksi_id,
                        'no_po' => $no_po,
                        'line_po' => $line,
                        'type_po' => $type_po,
                        'approval_po' => $approval_po,
                        'approval_email' => $approval_email,
                        'po_deskripsi' => $desc,
                        'effdate' => NOW(),
                        'type_po_rev' => $type_po_rev,
                        'po_item' => $item
                    ]);
                }
            }
        }
        Mail::to($approval_email)->send(new sendEmail($transaksi_id, $approval_po, Auth::guard('pub_login')->user()->username, $approval_email));

        alert()->success('Success', 'Created Change PO successfully');
        return redirect()->back();
    }

    public function approveAddChange(Request $request, $username, $transaksi_id){
        $username = base64_decode($username);
        $transaksi_id = base64_decode($transaksi_id);

        return view('intrans-change.approve', ['username' => $username, 'transaksi_id' => $transaksi_id]);
    }

    public function approveAddChangeSave(Request $request){
        // dd($request->all());
        $transaksi_id = $request['transaksi_id'];
        $save_data = $request['save_data'];
        $txtReason2 = $request['txtReason2'];
        $jam = date("H");
        $menit = date("i");
        $waktu = (($jam*60)+$menit);
        $today = date("m/d/Y");

        if($transaksi_id != ''){
            DB::table('po_change_type')
            ->where('transaksi_id', $transaksi_id)
            ->update([
                'approval_status' => 'OK',
                'approval_date' => NOW()
                ]);

                alert()->success('Success', 'Approve Change PO successfully');
                return view('intrans-change.approve_update', compact('transaksi_id'));
        }else{
            abort(404);
        }
    }
    public function rejectAddChangeSave(Request $request){
        $transaksi_id = $request['transaksi_id'];
        $reject_data = $request['reject_data'];
        $txtRemark2 = $request['txtRemark2'];
        $dataAll = collect(DB::select("SELECT DISTINCT transaksi_id, approval_po FROM PO_change_type WHERE transaksi_id = '$transaksi_id'"))->first();
        if($dataAll != null){
            $approval_po = $dataAll->approval_po;
        }else{
            $approval_po = '-';
        }
        if($transaksi_id != ''){
            DB::table('po_change_type')
            ->where('transaksi_id', $transaksi_id)
            ->update([
                'approval_status' => 'NOK',
                'approval_date' => NOW(),
                'approval_remark' => $txtRemark2
                ]);

                alert()->success('Success', 'Rejected Change PO successfully');

                return view('intrans-change.rejected_update', compact('transaksi_id', 'approval_po'));
        }else{
            abort(404);
        }
    }
}