<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ViewReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        // $url_prh    = "https://supplier.akebono-astra.co.id/wsatdw/prh_hist.php";  
        $hak    = Auth::guard('pub_login')->user()->previllage;
        $supp   = Auth::guard('pub_login')->user()->kode_supplier;              
        // getPrhHist($url_prh);
        // $month = date('m');
        $month = 1;
        $year = date('Y');

        $data = DB::table('SOAP_prh_hist')
                        ->where('prhVend', $supp)
                        ->whereMonth('prhRcp_date', $month)
                        ->whereYear('prhRcp_date', $year)
                        ->where('prhDomain' , 'AAIJ')
                        ->orderBy('prhRcp_date','DESC')->get();
        // dd($data);
        $tablenya = '';
        if(count($data) > 0){
            foreach($data as $sql){
                // getDataPtMstr($sql->prhPart);
                DB::table('SOAP_pt_mstr')
                        ->where('item_number', $sql->prhPart)
                        ->where('pt_domain' , 'AAIJ')->first();

                $tablenya .= '
                <tr>
                    <td>'.$sql->prhLine.'</td>
                    <td>'.$sql->prhNbr.'</td>
                    <td>'.$sql->prhVend.'</td>
                    <td>'.$sql->prhPart.'</td>
                    <td>'.$sql->prhVend.'</td>
                    <td>'.$sql->prhReceiver.'</td>
                    <td>'.$sql->prhPsNbr.'</td>
                    <td>'.number_format($sql->prhRcvd).'</td>
                    <td>'.date('d-F-Y',strtotime($sql->prhRcp_date)).'</td>
                    <td>'.$sql->prhReason.'</td>
                </tr>'; 
            }
        }else{
            $tablenya .= '
            <tr>
                <td colspan="10">No data available in table</td>
            </tr>'; 
        }
        return view('view-receipt.index');
    }

    public function search(Request $request){
        $session    = Auth::guard('pub_login')->user()->kode_supplier;   
        $po      = $request->get('po');
        $item    = $request->get('item');
        $rc      = $request->get('rc');
        $bulan   = $request->get('bulan');
        $tahun   = $request->get('tahun');
        $mod     = $request->get('mod');
        $get_prh = prh_hist($po);
        $tablenya= '';
   
        if($get_prh == false){
            $tablenya .= '';
        }else{
            $i = 0;
            if($mod == "reset"){
                $data = [];
            }else{
                if($bulan == "" && $tahun == ""){
                    
                    if($item != ""){
                        foreach($get_prh as $c){
                            if($c['prhVend'] == $session && $c['prhNbr'] == $po && $c['prhPart'] == $item){
                                $data[$i++] = array(
                                    'prhLine'       => $c['prhLine'],
                                    'prhNbr'        => $c['prhNbr'],
                                    'prhVend'       => $c['prhVend'],
                                    'prhPart'       => $c['prhPart'],
                                    'prhReceiver'   => $c['prhReceiver'],
                                    'prhPsNbr'      => $c['prhPsNbr'],
                                    'prhRcvd'       => $c['prhRcvd'],
                                    'prhRcp_date'   => $c['prhRcp_date'],
                                    'prhReason'     => $c['prhReason'],
                                    'prhUm'         => $c['prhUm'],
                                );
                            }
                        }
                    }else if($rc != ""){
                        foreach($get_prh as $c){
                            if($c['prhVend'] == $session && $c['prhNbr'] == $po && $c['prhReceiver'] == $rc){
                                $data[$i++] = array(
                                    'prhLine'       => $c['prhLine'],
                                    'prhNbr'        => $c['prhNbr'],
                                    'prhVend'       => $c['prhVend'],
                                    'prhPart'       => $c['prhPart'],
                                    'prhReceiver'   => $c['prhReceiver'],
                                    'prhPsNbr'      => $c['prhPsNbr'],
                                    'prhRcvd'       => $c['prhRcvd'],
                                    'prhRcp_date'   => $c['prhRcp_date'],
                                    'prhReason'     => $c['prhReason'],
                                    'prhUm'         => $c['prhUm'],
                                );
                            }
                        }
                    }else{
                        foreach($get_prh as $c){
                            if($c['prhVend'] == $session && $c['prhNbr'] == $po){
                                $data[$i++] = array(
                                    'prhLine'       => $c['prhLine'],
                                    'prhNbr'        => $c['prhNbr'],
                                    'prhVend'       => $c['prhVend'],
                                    'prhPart'       => $c['prhPart'],
                                    'prhReceiver'   => $c['prhReceiver'],
                                    'prhPsNbr'      => $c['prhPsNbr'],
                                    'prhRcvd'       => $c['prhRcvd'],
                                    'prhRcp_date'   => $c['prhRcp_date'],
                                    'prhReason'     => $c['prhReason'],
                                    'prhUm'         => $c['prhUm'],
                                );
                            }
                        }
                    }
                }else if($bulan != "" && $tahun != ""){

                    if($item != ""){
                        foreach($get_prh as $c){
                            if($c['prhVend'] == $session && $c['prhNbr'] == $po && $c['prhPart'] == $item && date('m',strtotime($c['prhRcp_date'])) == $bulan && date('Y',strtotime($c['prhRcp_date'])) == $tahun){
                                $data[$i++] = array(
                                    'prhLine'       => $c['prhLine'],
                                    'prhNbr'        => $c['prhNbr'],
                                    'prhVend'       => $c['prhVend'],
                                    'prhPart'       => $c['prhPart'],
                                    'prhReceiver'   => $c['prhReceiver'],
                                    'prhPsNbr'      => $c['prhPsNbr'],
                                    'prhRcvd'       => $c['prhRcvd'],
                                    'prhRcp_date'   => $c['prhRcp_date'],
                                    'prhReason'     => $c['prhReason'],
                                    'prhUm'         => $c['prhUm'],
                                );
                            }
                        }
                    }else if($rc != ""){
                        foreach($get_prh as $c){
                            if($c['prhVend'] == $session && $c['prhReceiver'] == $rc && date('m',strtotime($c['prhRcp_date'])) == $bulan && date('Y',strtotime($c['prhRcp_date'])) == $tahun){
                                $data[$i++] = array(
                                    'prhLine'       => $c['prhLine'],
                                    'prhNbr'        => $c['prhNbr'],
                                    'prhVend'       => $c['prhVend'],
                                    'prhPart'       => $c['prhPart'],
                                    'prhReceiver'   => $c['prhReceiver'],
                                    'prhPsNbr'      => $c['prhPsNbr'],
                                    'prhRcvd'       => $c['prhRcvd'],
                                    'prhRcp_date'   => $c['prhRcp_date'],
                                    'prhReason'     => $c['prhReason'],
                                    'prhUm'         => $c['prhUm'],
                                );
                            }
                        }
                    }else{
                        foreach($get_prh as $c){
                            if($c['prhVend'] == $session && $c['prhNbr'] == $po){
                                $data[$i++] = array(
                                    'prhLine'       => $c['prhLine'],
                                    'prhNbr'        => $c['prhNbr'],
                                    'prhVend'       => $c['prhVend'],
                                    'prhPart'       => $c['prhPart'],
                                    'prhReceiver'   => $c['prhReceiver'],
                                    'prhPsNbr'      => $c['prhPsNbr'],
                                    'prhRcvd'       => $c['prhRcvd'],
                                    'prhRcp_date'   => $c['prhRcp_date'],
                                    'prhReason'     => $c['prhReason'],
                                    'prhUm'         => $c['prhUm'],
                                );
                            }
                        }
                    }
                }else if($bulan != "" && $tahun ==""){
                    if($item != ""){
                        foreach($get_prh as $c){
                            if($c['prhVend'] == $session && $c['prhNbr'] == $po && $c['prhPart'] == $item && date('m',strtotime($c['prhRcp_date'])) == $bulan){
                                $data[$i++] = array(
                                    'prhLine'       => $c['prhLine'],
                                    'prhNbr'        => $c['prhNbr'],
                                    'prhVend'       => $c['prhVend'],
                                    'prhPart'       => $c['prhPart'],
                                    'prhReceiver'   => $c['prhReceiver'],
                                    'prhPsNbr'      => $c['prhPsNbr'],
                                    'prhRcvd'       => $c['prhRcvd'],
                                    'prhRcp_date'   => $c['prhRcp_date'],
                                    'prhReason'     => $c['prhReason'],
                                    'prhUm'         => $c['prhUm'],
                                );
                            }
                        }
                    }else if($rc != ""){
                        foreach($get_prh as $c){
                            if($c['prhVend'] == $session && $c['prhNbr'] == $po && $c['prhReceiver'] == $rc && date('m',strtotime($c['prhRcp_date'])) == $bulan){
                                $data[$i++] = array(
                                    'prhLine'       => $c['prhLine'],
                                    'prhNbr'        => $c['prhNbr'],
                                    'prhVend'       => $c['prhVend'],
                                    'prhPart'       => $c['prhPart'],
                                    'prhReceiver'   => $c['prhReceiver'],
                                    'prhPsNbr'      => $c['prhPsNbr'],
                                    'prhRcvd'       => $c['prhRcvd'],
                                    'prhRcp_date'   => $c['prhRcp_date'],
                                    'prhReason'     => $c['prhReason'],
                                    'prhUm'         => $c['prhUm'],
                                );
                            }
                        }
                    }else{
                        foreach($get_prh as $c){
                            if($c['prhVend'] == $session && $c['prhNbr'] == $po){
                                $data[$i++] = array(
                                    'prhLine'       => $c['prhLine'],
                                    'prhNbr'        => $c['prhNbr'],
                                    'prhVend'       => $c['prhVend'],
                                    'prhPart'       => $c['prhPart'],
                                    'prhReceiver'   => $c['prhReceiver'],
                                    'prhPsNbr'      => $c['prhPsNbr'],
                                    'prhRcvd'       => $c['prhRcvd'],
                                    'prhRcp_date'   => $c['prhRcp_date'],
                                    'prhReason'     => $c['prhReason'],
                                    'prhUm'         => $c['prhUm'],
                                );
                            }
                        }
                    }
                }else{

                    if($item != ""){
                        foreach($get_prh as $c){
                            if($c['prhVend'] == $session && $c['prhNbr'] == $po && $c['prhPart'] == $item && date('Y',strtotime($c['prhRcp_date'])) == $bulan){
                                $data[$i++] = array(
                                    'prhLine'       => $c['prhLine'],
                                    'prhNbr'        => $c['prhNbr'],
                                    'prhVend'       => $c['prhVend'],
                                    'prhPart'       => $c['prhPart'],
                                    'prhReceiver'   => $c['prhReceiver'],
                                    'prhPsNbr'      => $c['prhPsNbr'],
                                    'prhRcvd'       => $c['prhRcvd'],
                                    'prhRcp_date'   => $c['prhRcp_date'],
                                    'prhReason'     => $c['prhReason'],
                                    'prhUm'         => $c['prhUm'],
                                );
                            }
                        }
                    }else if($rc != ""){
                        foreach($get_prh as $c){
                            if($c['prhVend'] == $session && $c['prhNbr'] == $po && $c['prhReceiver'] == $rc && date('Y',strtotime($c['prhRcp_date'])) == $bulan){
                                $data[$i++] = array(
                                    'prhLine'       => $c['prhLine'],
                                    'prhNbr'        => $c['prhNbr'],
                                    'prhVend'       => $c['prhVend'],
                                    'prhPart'       => $c['prhPart'],
                                    'prhReceiver'   => $c['prhReceiver'],
                                    'prhPsNbr'      => $c['prhPsNbr'],
                                    'prhRcvd'       => $c['prhRcvd'],
                                    'prhRcp_date'   => $c['prhRcp_date'],
                                    'prhReason'     => $c['prhReason'],
                                    'prhUm'         => $c['prhUm'],
                                );
                            }
                        }
                    }else{
                        foreach($get_prh as $c){
                            if($c['prhVend'] == $session && $c['prhNbr'] == $po){
                                $data[$i++] = array(
                                    'prhLine'       => $c['prhLine'],
                                    'prhNbr'        => $c['prhNbr'],
                                    'prhVend'       => $c['prhVend'],
                                    'prhPart'       => $c['prhPart'],
                                    'prhReceiver'   => $c['prhReceiver'],
                                    'prhPsNbr'      => $c['prhPsNbr'],
                                    'prhRcvd'       => $c['prhRcvd'],
                                    'prhRcp_date'   => $c['prhRcp_date'],
                                    'prhReason'     => $c['prhReason'],
                                    'prhUm'         => $c['prhUm'],
                                );
                            }
                        }
                    }
                }
                
            }
            
            if(count($data) > 0){
                foreach($data as $sql){
                // getDataPtMstr($sql->prhPart);
                $itemnya = pt_mstr($sql['prhPart']);
                // DB::table('SOAP_pt_mstr')
                //         ->where('item_number', $sql->prhPart)
                //         ->where('pt_domain' , 'AAIJ')->first();
                if(empty($sql['prhReason'])){
                    $res = '-';
                }else{
                    $res = $sql['prhReason'];
                }
                 
                $tablenya .= '
                <tr>
                    <td>'.$sql['prhLine'].'</td>
                    <td>'.$sql['prhNbr'].'</td>
                    <td>'.$sql['prhVend'].'</td>
                    <td>'.$sql['prhPart'].'</td>
                    <td>'.$itemnya['deskripsi1'].'</td>
                    <td>'.$sql['prhReceiver'].'</td>
                    <td>'.$sql['prhPsNbr'].'</td>
                    <td>'.number_format($sql['prhRcvd']).'</td>
                    <td>'.date('d-F-Y',strtotime($sql['prhRcp_date'])).'</td>
                    <td>'.$res.'</td>
                </tr>'; 
                }
            }else{
                $tablenya .= '';
            }

        } //end pengkondisian 

        return response($tablenya);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
