<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExcelListBon;
use App\Mail\SentApprove;
use Illuminate\Support\Facades\Mail;
use App\Mail\SettingCloseEmail;
use Carbon\Carbon;

class MasterBarcodeWhsController extends Controller
{
    public function index(Request $request)
    {
        return view('master-barcode-whs.index');
    }

    public function datamasterbarcodewhs(Request $request)
    {

        $itemnumber = $request->itemnumber;
        $locationfrom = $request->locationfrom;
        $locationto = $request->locationto;

        $data = DB::table('polibox_mstr')
            ->select('polibox_item', DB::raw('count(polibox_item) as jml_box'), 'polibox_deskripsi', 'polibox_snp', 'polibox_loc_from', 'polibox_loc_to', 'polibox_comon', 'polibox_type', 'polibox_backno', 'polibox_address_pc', 'polibox_address_sa', 'qty_supplier', 'polibox_address_current', 'polibox_chutter', 'polibox_chutter1', 'polibox_alamat_asal', 'polibox_lok_asal', 'polibox_alamat_now', 'polibox_lok_now');

        if ($itemnumber != '') {
            $data->where('polibox_item', $itemnumber);
        }
        if ($locationfrom != '') {
            $data->where('polibox_loc_from', $locationfrom);
        }
        if ($locationto != '') {
            $data->where('polibox_loc_to', $locationto);
        }

        $data->groupBy('polibox_item', 'polibox_deskripsi', 'polibox_snp', 'polibox_loc_from', 'polibox_loc_to', 'polibox_comon', 'polibox_type', 'polibox_backno', 'polibox_address_pc', 'polibox_address_sa', 'qty_supplier', 'polibox_address_current', 'polibox_chutter', 'polibox_chutter1', 'polibox_alamat_asal', 'polibox_lok_asal', 'polibox_alamat_now', 'polibox_lok_now')
            ->orderBy('polibox_item', 'ASC')
            ->limit(30);

        $query = $data->get();

        $tablenya = '';
        $no = 1;
        foreach ($query as $row) {

            if ($row->polibox_comon == 0) {
                $polibox_comon = "False";
            } elseif ($row->polibox_comon == 1) {
                $polibox_comon = "True";
            }
            $qtySupp = $row->qty_supplier;
            if ($row->qty_supplier == NULL || $row->qty_supplier == '') {
                $qSupp = DB::table('polibox_chutter')->select('qty_supplier')->where('item_name', $row->polibox_item)->first();
                if ($qSupp == null) {
                    $qtySupp = 0;
                } else {
                    $qtySupp  = $qSupp->qty_supplier;
                }
            }

            if (Auth::user()->username == 'harsono') {
                $delete = '<a href="">Delete</a>';
            } else {
                $delete = '';
            }

            $tablenya .= '<tr>
                <td>' . $no . '</td>
                <td>' . $row->polibox_item . '</td>
                <td>' . $row->polibox_deskripsi . '</td>
                <td>
                <input type="hidden" name="txt_urutan[' . $no . ']" value="' . $no . '">
                <input type="hidden" name="type_hide[' . $no . ']" value="' . $row->polibox_type . '">
                <input type="hidden" name="item_desc[' . $no . ']" value="' . $row->polibox_deskripsi . '">
                <input type="text" name="type_umum[' . $no . ']" class="form-control-sm" value="' . $row->polibox_type . '" size="15"></td>
                <td><input type="hidden" name="polibox_comon[' . $no . ']" value="' . $polibox_comon . '">' . $polibox_comon . '</td>
                <td><input type="hidden" name="snp[' . $no . ']" value="' . $row->polibox_snp . '">' . $row->polibox_snp . '</td>
                <input type="hidden" name="locfromold[' . $no . ']" id="locfromold' . $no . '" value="' . $row->polibox_loc_from . '">

                <td>
                <div class="d-flex align-items-center">
                <input type="text" name="locfrom[' . $no . ']" id="from' . $row->polibox_item . $no .  '" class="form-control-sm" value="' . $row->polibox_loc_from . '" size="10" readonly>
                <a href="javascript:;" class="loc_from searchLocMaster" data-toggle="modal" data-target=".location" data-item="from' . $row->polibox_item . $no . '" id="searchLocMaster"><i class="fa fa-search ml-1"></i></a>
                </div>
                </td>

                <input type="hidden" name="loctoold[' . $no . ']" id="loctoold' . $no . '" value="' . $row->polibox_loc_to . '">

                <td>
                <div class="d-flex align-items-center">
                <input type="text" name="pol_locto[' . $no . ']" id="to' . $row->polibox_item . $no .  '" class="form-control-sm" value="' . $row->polibox_loc_to . '" size="10" readonly>
                <a href="javascript:;" class="loc_to searchLocMaster" data-toggle="modal" data-target=".location" data-item="to' . $row->polibox_item . $no .  '" id="searchLocMasterLocTo"><i class="fa fa-search ml-1"></i></a>
                </div>
                </td>

                <td><input type="text" name="box[' . $no . ']" class="form-control-sm" value="' . $row->jml_box . '" size="3">
                <input type="hidden" name="boxOld[' . $no . ']" value="' . $row->jml_box . '"></td>
                <td><input type="text" name="backno[' . $no . ']" class="form-control-sm" value="' . $row->polibox_backno . '" size="6"></td>
                <td><input type="text" name="polibox_alamat_asal[' . $no . ']" class="form-control-sm" value="' . $row->polibox_alamat_asal . '" size="6"></td>
                <td><input type="text" name="address_chutter[' . $no . ']" class="form-control-sm" value="' . $row->polibox_chutter . '" size="6"></td>
                <td><input type="text" name="polibox_lok_asal[' . $no . ']" class="form-control-sm" value="' . $row->polibox_lok_asal . '" size="6"></td>
                <td><input type="text" name="polibox_alamat_now[' . $no . ']" class="form-control-sm" value="' . $row->polibox_alamat_now . '" size="6"></td>
                <td><input type="text" name="address_chutter1[' . $no . ']" class="form-control-sm" value="' . $row->polibox_chutter1 . '" size="6"></td>
                <td><input type="text" name="polibox_lok_now[' . $no . ']" class="form-control-sm" value="' . $row->polibox_lok_now . '" size="6"></td>
                <td><input type="text" name="address_pc[' . $no . ']" class="form-control-sm" value="' . $row->polibox_address_pc . '" size="6"></td>
                <td><input type="text" name="address_sa[' . $no . ']" class="form-control-sm" value="' . $row->polibox_address_sa . '" size="6"></td>
                <td><input type="text" name="qty_supplier[' . $no . ']" class="form-control-sm" value="' . $qtySupp . '" size="6"></td>
                <td align="center">
                <a href="' . route("detail-barcode-whs", [base64_encode($row->polibox_item), base64_encode($row->polibox_loc_from), base64_encode($row->polibox_loc_to), base64_encode($row->polibox_snp), base64_encode($row->polibox_comon)]) . '" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> Subbox</a>
                <a href="' . route('print-barcode-whs', [base64_encode($row->polibox_item), base64_encode($row->polibox_loc_from), base64_encode($row->polibox_loc_to), base64_encode($row->polibox_snp), base64_encode($row->polibox_comon), base64_encode($row->polibox_type), base64_encode($no), base64_encode($qtySupp)]) . '" class="btn btn-sm btn-info"><i class="fa fa-print"></i> Print</a>
                ' . $delete . '
                </td>
                <input type="hidden" name="polibox_item[' . $no . ']" value="' . $row->polibox_item . '">
            </tr>';
            $no += 1;
        }

        $data = [
            'tablenya' => $tablenya,
        ];

        return response()->json($data);
    }

    public function update(Request $request)
    {
        $type_hide = $request->type_hide;
        $item_desc = $request->item_desc;
        $type_umum = $request->type_umum;
        $polibox_comon = $request->polibox_comon;
        $snp = $request->snp;
        $locfromold = $request->locfromold;
        $locfrom = $request->locfrom;
        $loctoold = $request->loctoold;
        $pol_locto = $request->pol_locto;
        $box = $request->box;
        $boxOld = $request->boxOld;
        $backno = $request->backno;
        $polibox_alamat_asal = $request->polibox_alamat_asal;
        $address_chutter = $request->address_chutter;
        $polibox_lok_asal = $request->polibox_lok_asal;
        $polibox_alamat_now = $request->polibox_alamat_now;
        $address_chutter1 = $request->address_chutter1;
        $polibox_lok_now = $request->polibox_lok_now;
        $address_pc = $request->address_pc;
        $address_sa = $request->address_sa;
        $qty_supplier = $request->qty_supplier;
        $polibox_item = $request->polibox_item;

        for ($i = 1; $i <= count($request->type_hide); $i++) {

            if ($type_hide[$i] == "") {
                $tipequery = "IS NULL";
            } else {
                $tipequery = $type_hide[$i];
            }

            if ($box[$i] == $boxOld[$i]) {
                DB::table('polibox_mstr')
                    ->where('polibox_item', $polibox_item[$i])
                    ->where('polibox_loc_from', $locfromold[$i])
                    ->where('polibox_loc_to', $loctoold[$i])
                    ->where('polibox_snp', $snp[$i])
                    ->where('polibox_comon', $polibox_comon[$i])
                    ->where('polibox_deskripsi', $item_desc[$i])
                    ->where('polibox_type', $tipequery)
                    ->update([
                        'polibox_type' => $type_umum[$i],
                        'polibox_backno' => $backno[$i],
                        'polibox_address_pc' => $address_pc[$i],
                        'polibox_address_sa' => $address_sa[$i],
                        'polibox_loc_to' => $pol_locto[$i],
                        'polibox_chutter' => $address_chutter[$i],
                        'polibox_chutter1' => $address_chutter1[$i],
                        'polibox_loc_from' => $locfrom[$i],
                        'polibox_alamat_asal' => $polibox_alamat_asal[$i],
                        'polibox_lok_asal' => $polibox_lok_asal[$i],
                        'polibox_alamat_now' => $polibox_alamat_now[$i],
                        'polibox_lok_now' => $polibox_lok_now[$i],
                        'qty_supplier' => $qty_supplier[$i],
                    ]);
            } else {

                $datacekid = DB::table('polibox_idbox')
                    ->where('partno', $polibox_item[$i])
                    ->where('locto', $pol_locto[$i])
                    ->where('tipe', $type_hide[$i])
                    ->where('snp', $snp[$i])
                    ->first();
                dd($datacekid);
                $cekid = $datacekid->idbox;
                $cek_mstr = DB::table('polibox_mstr')
                    ->where('polibox_item', $polibox_item[$i])
                    ->where('polibox_loc_to', $pol_locto[$i])
                    ->where('polibox_type', $type_hide[$i])
                    ->where('polibox_snp', $snp[$i])
                    ->count();

                $del_data = DB::table('polibox_mstr')
                    ->where('polibox_item', $polibox_item[$i])
                    ->where('polibox_loc_to', $pol_locto[$i])
                    ->where('polibox_type', $type_hide[$i])
                    ->where('polibox_snp', $snp[$i])
                    ->delete();

                for ($j = 1; $j <= $box[$i]; $j++) {
                    if ($cekid != "" and $cek_mstr > 0) {
                        $nbr = $cekid . "-" . $j;
                    } else {
                        $nbr = $polibox_item[$j] . $j . $snp[$j];
                        // if ($common == "True") {
                        //     $nbr = $nbr . $pol_locto;
                        // }
                    }
                    $insert = DB::table('polibox_mstr')->insert([
                        'polibox_item' => $polibox_item[$i],
                        'polibox_subbox' => $j,
                        'polibox_deskripsi' => $item_desc[$i],
                        'polibox_snp' => $snp[$i],
                        'polibox_loc_from' => $locfrom[$i],
                        'polibox_loc_to' => $pol_locto[$i],
                        'polibox_nbr' => $nbr[$i],
                        'polibox_type' => $type_umum[$i],
                        'polibox_comon' => $polibox_comon[$i],
                        'polibox_creadate' => date('Y-m-d'),
                        'polibox_backno' => $backno[$i],
                        'polibox_address_pc' => $address_pc[$i],
                        'polibox_address_sa' => $address_sa[$i],
                        // 'polibox_address_current' => $address_current[$i],
                        'polibox_chutter' => $address_chutter[$i],
                        'polibox_chutter1' => $address_chutter1[$i],
                        'polibox_alamat_asal' => $polibox_alamat_asal[$i],
                        'polibox_lok_asal' => $polibox_lok_asal[$i],
                        'polibox_alamat_now' => $polibox_alamat_now[$i],
                        'polibox_lok_now' => $polibox_lok_now[$i],
                        'qty_supplier' => $qty_supplier[$i],
                    ]);
                }
            }
        }
        alert()->success('success', 'Updated Master Barcode WHS successfully');
        return redirect()->back();
    }

    public function detailbarcodewhs($pi, $locfrom, $locto, $snp, $comon)
    {
        $pi = base64_decode($pi);
        $locfrom = base64_decode($locfrom);
        $locto = base64_decode($locto);
        $snp = base64_decode($snp);
        $comon = base64_decode($comon);
        // $select_q = "select * from polibox_mstr where (polibox_item = '".$_GET['pi']."') and polibox_loc_from='".$_GET['locfrom']."' and polibox_loc_to='".$_GET['locto']."' and polibox_snp='".$_GET['snp']."' and polibox_comon='".$_GET['comon']."' order by polibox_item,polibox_type,polibox_subbox asc";

        $data = DB::table('polibox_mstr')
            ->where('polibox_item', $pi)
            ->where('polibox_loc_from', $locfrom)
            ->where('polibox_loc_to', $locto)
            ->where('polibox_snp', $snp)
            ->where('polibox_comon', $comon)
            ->orderBy('polibox_item', 'asc')
            ->orderBy('polibox_type', 'asc')
            ->orderBy('polibox_subbox', 'asc')
            ->get();

        return view('master-barcode-whs.detail-barcode-whs', compact('data'));
    }

    public function updatedetail(Request $request)
    {
        $type = $request->type_det;
        $barcode = $request->idbarcode;
        for ($i = 1; $i <= count($type); $i++) {
            $update = DB::table('polibox_mstr')->where('polibox_nbr', $barcode[$i])->update(['polibox_type_det' => $type[$i]]);
        }
        alert()->success('success', 'Updated Detail Master Barcode WHS successfully');
        return redirect()->back();
    }

    public function printbarcodewhs($pi, $locfrom, $locto, $snp, $comon, $type, $urutan, $qtysupp)
    {
        $data = [
            'pi' => base64_decode($pi),
            'locfrom' => base64_decode($locfrom),
            'locto' => base64_decode($locto),
            'snp' => base64_decode($snp),
            'comon' => base64_decode($comon),
            'type' => base64_decode($type),
            'urutan' => base64_decode($urutan),
            'qtysupp' => base64_decode($qtysupp),
        ];
        return view('master-barcode-whs.print-barcode-whs', compact('data'));
    }

    public function printbarcode(Request $request)
    {
        // dd($request->all());
        if ($request->print == 'kanban') {

            $subbox = DB::table('polibox_mstr')->where('polibox_item', $request->hddpolibox_item)->max('polibox_subbox');

            $select_q = DB::table('polibox_mstr')
                ->where('polibox_item', $request->hddpolibox_item)
                ->where('polibox_loc_from', $request->hddpolibox_loc_from)
                ->where('polibox_loc_to', $request->hddpolibox_loc_to)
                ->where('polibox_snp', $request->hddpolibox_snp)
                ->where('polibox_comon', $request->hddpolibox_comon)
                ->where('polibox_type', $request->hddpolibox_tipe)
                ->orderBy('polibox_subbox', 'asc')
                ->get();


            return view('master-barcode-whs.print_kanban', compact('select_q', 'subbox'));
        } elseif ($request->print == 'chutter') {
            //untuk menampilkan jumlah
            $subbox = DB::table('polibox_mstr')->where('polibox_item', $request->hddpolibox_item)->max('polibox_subbox');

            //untuk menampilkan jumlah dalam satu tipe
            $select_r1 = DB::table('polibox_mstr')
                ->where('polibox_item', $request->hddpolibox_item)
                ->where('polibox_loc_from', $request->hddpolibox_loc_from)
                ->where('polibox_loc_to', $request->hddpolibox_loc_to)
                ->where('polibox_snp', $request->hddpolibox_snp)
                ->where('polibox_comon', $request->hddpolibox_comon)
                ->where('polibox_type', $request->hddpolibox_tipe)
                ->groupBy('polibox_item', 'polibox_deskripsi', 'polibox_snp', 'polibox_loc_from', 'polibox_loc_to', 'polibox_type', 'polibox_comon', 'polibox_backno', 'polibox_address_pc', 'polibox_address_sa', 'polibox_address_current', 'polibox_chutter', 'polibox_chutter1', 'polibox_alamat_asal', 'polibox_lok_asal', 'polibox_alamat_now', 'polibox_lok_now')
                ->count('polibox_subbox');


            $select_q = DB::table('polibox_mstr')
                ->select(
                    'polibox_item',
                    'polibox_subbox',
                    'polibox_backno',
                    'polibox_deskripsi',
                    'polibox_supplier',
                    'polibox_snp',
                    'polibox_loc_from',
                    'polibox_loc_to',
                    'polibox_nbr',
                    'polibox_type_det',
                    'polibox_type',
                    'polibox_address_pc',
                    'polibox_address_sa',
                    'polibox_chutter',
                    'polibox_chutter1',
                    'polibox_lok_asal',
                    'polibox_lok_now',
                    'qty_supplier'
                )
                ->where('polibox_item', $request->hddpolibox_item)
                ->where('polibox_loc_from', $request->hddpolibox_loc_from)
                ->where('polibox_loc_to', $request->hddpolibox_loc_to)
                ->where('polibox_snp', $request->hddpolibox_snp)
                ->where('polibox_comon', $request->hddpolibox_comon)
                ->where('polibox_type', $request->hddpolibox_tipe)
                ->groupBy('polibox_item', 'polibox_subbox', 'polibox_backno', 'polibox_deskripsi', 'polibox_supplier', 'polibox_snp', 'polibox_loc_from', 'polibox_loc_to', 'polibox_nbr', 'polibox_type_det', 'polibox_type', 'polibox_address_pc', 'polibox_address_sa', 'polibox_chutter', 'polibox_chutter1', 'polibox_lok_asal', 'polibox_lok_now', 'qty_supplier')
                ->orderBy('polibox_subbox', 'asc')
                ->limit(2)
                ->get();

            return view('master-barcode-whs.print_chutter', compact('select_q', 'select_r1', 'subbox'));
        } elseif ($request->print == 'box') {

            //untuk menampilkan jumlah
            $subbox = DB::table('polibox_mstr')->where('polibox_item', $request->hddpolibox_item)->max('polibox_subbox');

            //untuk menampilkan jumlah dalam satu tipe
            $select_r1 = DB::table('polibox_mstr')
                ->where('polibox_item', $request->hddpolibox_item)
                ->where('polibox_loc_from', $request->hddpolibox_loc_from)
                ->where('polibox_loc_to', $request->hddpolibox_loc_to)
                ->where('polibox_snp', $request->hddpolibox_snp)
                ->where('polibox_comon', $request->hddpolibox_comon)
                ->where('polibox_type', $request->hddpolibox_tipe)
                ->groupBy('polibox_item', 'polibox_deskripsi', 'polibox_snp', 'polibox_loc_from', 'polibox_loc_to', 'polibox_type', 'polibox_comon', 'polibox_backno', 'polibox_address_pc', 'polibox_address_sa', 'polibox_address_current', 'polibox_chutter', 'polibox_chutter1', 'polibox_alamat_asal', 'polibox_lok_asal', 'polibox_alamat_now', 'polibox_lok_now')
                ->count('polibox_subbox');

            $select_q = DB::table('polibox_mstr')
                ->select('polibox_item', 'polibox_subbox', 'polibox_backno', 'polibox_deskripsi', 'polibox_supplier', 'polibox_snp', 'polibox_loc_from', 'polibox_loc_to', 'polibox_nbr', 'polibox_type_det', 'polibox_type', 'polibox_address_pc', 'polibox_address_sa', 'polibox_chutter', 'polibox_chutter1', 'polibox_lok_asal', 'polibox_lok_now')
                ->where('polibox_item', $request->hddpolibox_item)
                ->where('polibox_loc_from', $request->hddpolibox_loc_from)
                ->where('polibox_loc_to', $request->hddpolibox_loc_to)
                ->where('polibox_snp', $request->hddpolibox_snp)
                ->where('polibox_comon', $request->hddpolibox_comon)
                ->where('polibox_type', $request->hddpolibox_tipe)
                ->groupBy('polibox_item', 'polibox_subbox', 'polibox_backno', 'polibox_deskripsi', 'polibox_supplier', 'polibox_snp', 'polibox_loc_from', 'polibox_loc_to', 'polibox_nbr', 'polibox_type_det', 'polibox_type', 'polibox_address_pc', 'polibox_address_sa', 'polibox_chutter', 'polibox_chutter1', 'polibox_lok_asal', 'polibox_lok_now')
                ->orderBy('polibox_subbox', 'asc')
                ->get();


            return view('master-barcode-whs.print_box', compact('select_q', 'select_r1', 'subbox'));
        }
    }

    public function getlocmaster()
    {
        $url = "https://supplier.akebono-astra.co.id/wsatdw/loc_mstr.php";
        $lok = loc_mstr($url);
        return DataTables::of($lok)
            ->addColumn('action', function ($data) {
                $locLoc = $data['locLoc'];
                if (!empty($locLoc)) {
                    $locLoc = $data['locLoc'];
                } else {
                    $locLoc = '-';
                }
                $button = '<a href="javascript:void(0)" data-type="' . $locLoc . '" data-loc="' . $locLoc . '" class="btn btn-sm btn-warning" id="selectLocMstr"><i class="fa fa-check"></i> Click</a>';
                return $button;
            })
            ->editColumn('locDesc', function ($data) {
                $b = $data['locDesc'];
                if (!empty($b)) {
                    $a = $data['locDesc'];
                } else {
                    $a = '-';
                }
                return $a;
            })
            ->editColumn('locLoc', function ($data) {
                $b = $data['locLoc'];
                if (!empty($b)) {
                    $a = $data['locLoc'];
                } else {
                    $a = '-';
                }
                return $a;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function listbon(Request $request)
    {

        $data = DB::table('whs_bon as wb')->select('wb.*', 'la.area_name', 'll.line_prefix', 'lp.plant_name')
            ->join('lph_plant as lp', 'wb.produksi', '=', 'lp.ID')
            ->join('lph_area as la', 'wb.area', '=', 'la.ID')
            ->join('lph_line as ll', 'wb.line', '=', 'll.ID')
            ->orderBy('wb.create_time', 'DESC')
            ->get();

        $sql2 = [];
        foreach ($data as $key) {
            array_push($sql2, [
                'id' => $key->id,
                'kode_transaksi' => $key->kode_transaksi,
                'produksi' => $key->produksi,
                'area' => $key->area,
                'line' => $key->line,
                'nama_pengambil' => $key->nama_pengambil,
                'tgl_ambil' => $key->tgl_ambil,
                'waktu' => $key->waktu,
                'create_time' => $key->create_time,
                'create_by' => $key->create_by,
                'approve_foreman' => $key->approve_foreman,
                'approve_admin' => $key->approve_admin,
                'reason' => $key->reason,
                'approve_frm_time' => $key->approve_frm_time,
                'approve_admin_time' => $key->approve_admin_time,
                'fm' => $key->fm,
                'admin' => $key->admin,
                'area_name' => $key->area_name,
                'plant_name' => $key->plant_name,
                'line_prefix' => $key->line_prefix,
                'jml' => DB::table('whs_bon_brg')->where('id_bon', $key->kode_transaksi)->count()
            ]);
        }

        return view('master-barcode-whs.list-bon', compact('sql2'));
    }

    public function searchlistbon(Request $request)
    {

        $startdate  = $request->get('startdate');
        $enddate    = $request->get('enddate');
        $produksi   = $request->get('produksi');
        $area       = $request->get('area');
        $line       = $request->get('line');

        if ($area == 0 && $line == 0) {

            $data = DB::table('whs_bon as wb')->select('wb.*', 'la.area_name', 'll.line_prefix', 'lp.plant_name')
                ->join('lph_plant as lp', 'wb.produksi', '=', 'lp.ID')
                ->join('lph_area as la', 'wb.area', '=', 'la.ID')
                ->join('lph_line as ll', 'wb.line', '=', 'll.ID')
                ->whereBetween('wb.create_time', [$startdate, $enddate])
                ->where('wb.produksi', $produksi)
                ->orderBy('wb.create_time', 'DESC')
                ->get();

            $sql2 = [];
            foreach ($data as $key) {
                array_push($sql2, [
                    'id' => $key->id,
                    'kode_transaksi' => $key->kode_transaksi,
                    'produksi' => $key->produksi,
                    'area' => $key->area,
                    'line' => $key->line,
                    'nama_pengambil' => $key->nama_pengambil,
                    'tgl_ambil' => $key->tgl_ambil,
                    'waktu' => $key->waktu,
                    'create_time' => $key->create_time,
                    'create_by' => $key->create_by,
                    'approve_foreman' => $key->approve_foreman,
                    'approve_admin' => $key->approve_admin,
                    'reason' => $key->reason,
                    'approve_frm_time' => $key->approve_frm_time,
                    'approve_admin_time' => $key->approve_admin_time,
                    'fm' => $key->fm,
                    'admin' => $key->admin,
                    'area_name' => $key->area_name,
                    'plant_name' => $key->plant_name,
                    'line_prefix' => $key->line_prefix,
                    'jml' => DB::table('whs_bon_brg')->where('id_bon', $key->kode_transaksi)->count()
                ]);
            }
        } else {

            $data = DB::table('whs_bon as wb')->select('wb.*', 'la.area_name', 'll.line_prefix', 'lp.plant_name')
                ->join('lph_plant as lp', 'wb.produksi', '=', 'lp.ID')
                ->join('lph_area as la', 'wb.area', '=', 'la.ID')
                ->join('lph_line as ll', 'wb.line', '=', 'll.ID')
                ->whereBetween('wb.create_time', [$startdate, $enddate])
                ->where('wb.produksi', $produksi)
                ->where('wb.area', $area)
                ->where('wb.line', $line)
                ->orderBy('wb.create_time', 'DESC')
                ->get();

            $sql2 = [];
            foreach ($data as $key) {
                array_push($sql2, [
                    'id' => $key->id,
                    'kode_transaksi' => $key->kode_transaksi,
                    'produksi' => $key->produksi,
                    'area' => $key->area,
                    'line' => $key->line,
                    'nama_pengambil' => $key->nama_pengambil,
                    'tgl_ambil' => $key->tgl_ambil,
                    'waktu' => $key->waktu,
                    'create_time' => $key->create_time,
                    'create_by' => $key->create_by,
                    'approve_foreman' => $key->approve_foreman,
                    'approve_admin' => $key->approve_admin,
                    'reason' => $key->reason,
                    'approve_frm_time' => $key->approve_frm_time,
                    'approve_admin_time' => $key->approve_admin_time,
                    'fm' => $key->fm,
                    'admin' => $key->admin,
                    'area_name' => $key->area_name,
                    'plant_name' => $key->plant_name,
                    'line_prefix' => $key->line_prefix,
                    'jml' => DB::table('whs_bon_brg')->where('id_bon', $key->kode_transaksi)->count()
                ]);
            }
        }

        return response($sql2);
    }

    public function approvalbon()
    {

        $user = Auth::user()->username;
        // $status = $request->get('status'); //dynamic sementara hardcore
        $status = 'approval';

        if ($status == 'approval') {
            $data = DB::table('whs_bon as wb')->select('wb.*', 'lp.plant_name', 'la.area_name', 'll.line_prefix')
                ->join('lph_plant as lp', 'wb.produksi', '=', 'lp.ID')
                ->join('lph_area as la', 'wb.area', '=', 'la.ID')
                ->join('lph_line as ll', 'wb.line', '=', 'll.ID')
                ->whereNotNull('wb.approve_frm_time')
                ->whereNull('wb.approve_admin_time')
                ->orderBy('wb.create_time', 'DESC')
                ->get();
            // $kdtr = $data->kode_transaksi;
            // $sql2 = DB::table('whs_bon_brg')->pluck('id_bon')->where('id_bon', $kdtr)->count();

        } else {
            $data = DB::table('whs_bon as wb')->select('wb.*', 'lp.plant_name', 'la.area_name', 'll.line_prefix')
                ->join('lph_plant as lp', 'wb.produksi', '=', 'lp.ID')
                ->join('lph_area as la', 'wb.area', '=', 'la.ID')
                ->join('lph_line as ll', 'wb.line', '=', 'll.ID')
                ->whereNotNull('wb.approve_frm_time')
                ->whereNotNull('wb.approve_admin_time')
                ->orderBy('wb.create_time', 'DESC')
                ->get();

            // $kdtr = $data->kode_transaksi;
            // $sql2 = DB::table('whs_bon_brg')->pluck('id_bon')->where('id_bon', $kdtr)->count();
        }
        // dd($data);
        return view('master-barcode-whs.approval-bon', compact('data', 'user'));
    }

    public function completebon()
    {

        $user = Auth::user()->username;
        // $status = $request->get('status'); //dynamic sementara hardcore
        $status = 'approval';

        if ($status == 'approval') {
            $data = DB::table('whs_bon as wb')->select('wb.*', 'lp.plant_name', 'la.area_name', 'll.line_prefix')
                ->join('lph_plant as lp', 'wb.produksi', '=', 'lp.ID')
                ->join('lph_area as la', 'wb.area', '=', 'la.ID')
                ->join('lph_line as ll', 'wb.line', '=', 'll.ID')
                ->whereNotNull('wb.approve_frm_time')
                ->whereNull('wb.approve_admin_time')
                ->orderBy('wb.create_time', 'DESC')
                ->get();
            // $kdtr = $data->kode_transaksi;
            // $sql2 = DB::table('whs_bon_brg')->pluck('id_bon')->where('id_bon', $kdtr)->count();

        } else {
            $data = DB::table('whs_bon as wb')->select('wb.*', 'lp.plant_name', 'la.area_name', 'll.line_prefix')
                ->join('lph_plant as lp', 'wb.produksi', '=', 'lp.ID')
                ->join('lph_area as la', 'wb.area', '=', 'la.ID')
                ->join('lph_line as ll', 'wb.line', '=', 'll.ID')
                ->whereNotNull('wb.approve_frm_time')
                ->whereNotNull('wb.approve_admin_time')
                ->orderBy('wb.create_time', 'DESC')
                ->get();

            // $kdtr = $data->kode_transaksi;
            // $sql2 = DB::table('whs_bon_brg')->pluck('id_bon')->where('id_bon', $kdtr)->count();

        }

        return view('master-barcode-whs.complete-bon', compact('data', 'user'));
    }

    public function printapprovebonwhs(Request $request)
    {

        $id = $request->get('id');
        $kode_transaksi = $request->get('kode_transaksi');

        $data = DB::table('whs_bon as wb')->select('wb.*', 'lp.plant_name', 'la.area_name', 'll.line_prefix')
            ->join('lph_plant as lp', 'wb.produksi', '=', 'lp.ID')
            ->join('lph_area as la', 'wb.area', '=', 'la.ID')
            ->join('lph_line as ll', 'wb.line', '=', 'll.ID')
            ->where('wb.kode_transaksi', $kode_transaksi)
            ->orderBy('wb.create_time', 'DESC')
            ->first();

        $qry2 = DB::table('whs_bon_brg')->where('id_bon', $kode_transaksi)->first();
        return view('master-barcode-whs.barcode_print', compact('data', 'qry2'));
    }

    public function editaprrovalbonwhs(Request $request)
    {
        $name = $request->get('name');
        $id = $request->get('id');

        $data = DB::table('whs_bon as wb')->select('wb.*', 'lp.plant_name', 'la.area_name', 'll.line_prefix')
            ->join('lph_plant as lp', 'wb.produksi', '=', 'lp.ID')
            ->join('lph_area as la', 'wb.area', '=', 'la.ID')
            ->join('lph_line as ll', 'wb.line', '=', 'll.ID')
            ->where('wb.id', $id)
            ->orderBy('wb.create_time', 'DESC')
            ->first();

        $kd = $data->kode_transaksi;

        $qry2 = DB::table('whs_bon_brg')->where('id_bon', $kd)->get();

        return view('master-barcode-whs.edit_app_bon', compact('data', 'qry2'));
    }

        public function updateapprovebonwhs(Request $request) {
        // dd($request);
        $sectname = Auth::user()->username;
        $step = "app2";
        if($request->action == "approve"){
            $appr="1";
        }else{
            $appr="2";
        }
        // dd($request);
        $idbrg 		= str_replace("'","",$request->idbrg);
        $idtrans 	= str_replace("'","",$request->idtrans);
		$reason 	= str_replace("'","",$request->txtReason);
        $jmlpart 	= str_replace("'","",$request->txtJumlah);
        $changedata	= str_replace("'","",$request->changedata);
        $jmlreas 	= str_replace("'","",$request->txtAlasan);
        $nm 		= str_replace("'","",$request->pengambil);
        $size 		= count($idbrg);

        for($i = 0 ; $i < $size ; $i++){
            $id = $idbrg[$i];
            $jml = $jmlpart[$i];
            $als = $jmlreas[$i];
            $chg = $changedata[$i];
            $sql = DB::table('whs_bon_brg')
                ->where('id', $id)
                ->update(['change_jml' => $jml,'change_reason' => $als, 'changeby' => $sectname]);
        }
            if ($sql) {
                //echo "UPDATE whs_bon SET approve_foreman=$appr, approve_frm_time=GETDATE(),fm='$sectname', reason='$reason' WHERE id=$idtrans";
                if($step=="app1"){
                    DB::table('whs_bon')
                    ->where('id', $idtrans)
                    ->update(['approve_foreman' => $appr, 'approve_frm_time' => Carbon::now(), 'fm' => $sectname, 'reason' => $reason]);

                }else{
                    DB::table('whs_bon')
                    ->where('id', $idtrans)
                    ->update(['approve_admin' => $appr, 'approve_admin_time' => Carbon::now(), 'admin' => $sectname, 'reason' => $reason]);
					if($chg != $jml ){
                        Mail::to('fiandichus3@gmail.com')->send(new SentApprove($idtrans, $nm, 'Arjuna'));
                    }
                }
                alert()->success('Success','Update Successfully!');
                return redirect()->route('list-bon');
            } else {
                alert()->danger('Danger','Update Failed!');
                return redirect()->route('approval-bon-whs');
            }
    }

    public function excellistbon(Request $request)
    {

        $startdate  = $request->startdate;
        $enddate    = $request->enddate;
        $produksi   = $request->produksi;
        $area       = $request->area;
        $line       = $request->line;

        return Excel::download(new ExcelListBon($startdate, $enddate, $produksi, $area, $line), 'List-BON-Excel.xlsx');
    }

    public function settingclose()
    {

        $data = DB::table('polibox_set_GLopen')->orderBy('set_thn', 'DESC')->orderBy('set_bln', 'DESC')->get();
        $years = Carbon::now()->format('Y');
        $month = Carbon::now()->month;

        $check = [
            'years' => $years,
            'month' => $month
        ];

        return view('master-barcode-whs.setting-close-open', compact('data', 'check'));
    }

    public function storecloseopen(Request $request)
    {
        $check  = $request->get('check');
        $bln    = $request->get('bln');
        $thn    = $request->get('thn');
        if ($check == 'set_lastmonth') {

            $update_close = DB::table('polibox_set_GLOpen')
                ->where('set_thn', $thn)
                ->where('set_bln', $bln)
                ->update(['set_status' => 'CLOSE', 'set_close_date' => date('Y-m-d H:i:s')]);

            $polibox = DB::table('polibox_set_GLopen')->where('set_thn', '=', Carbon::now()->format('Y'))->where('set_bln', '=', Carbon::now()->month)->first();

            if ($polibox === null) {
                $insert_open = DB::table('polibox_set_GLopen')->insert([
                    'set_thn' => Carbon::now()->format('Y'),
                    'set_bln' => Carbon::now()->month,
                    'creadate' => Carbon::now()
                ]);

                $data = 'closed';
            } else {
                $data = 'duplicate';
            }

            return response($data);
        }

        if ($check == 'set_currentmonth') {

            $update_close = DB::table('polibox_set_GLOpen')->where('set_thn', $thn)
                ->where('set_bln', $bln)
                ->update(['set_status' => 'OPEN', 'set_open_date' => Carbon::now()]);
            $email = 'rahma.d@akebono-astra.co.id';
            $status = 'bisa dilakukan';

            $bln = date("F", strtotime($thn - $bln - 01));
            Mail::to($email)->send(new SettingCloseEmail($thn, $bln, $status));

            $data = 'open';
            return response($data);
        }
    }

    public function autocompleteList(Request $request)
    {

        $today1 = Carbon::now();

        if ($request->sts == "1") {
            $plancode = $request->plant;

            $qry = mssql_query("SELECT * FROM lph_area WHERE plant_code='$plancode'");

            while ($row = mssql_fetch_array($qry)) {
                $row[] = $row;
                $areaid[] = $row['ID'];
                $area[] = $row['area_name'];
            }

            $a  = json_encode($areaid);
            $b  = json_encode($area);
            echo json_encode(array("areaid" => $a, "area" => $b));
        }
        if ($request->sts == "2") {
            $plancode = $request->plant;

            $qry = mssql_query("SELECT * FROM lph_line WHERE area_code='$plancode'");

            while ($row = mssql_fetch_array($qry)) {
                $row[] = $row;
                $lineid[] = $row['ID'];
                $linepref[] = $row['line_prefix'];
            }

            $a  = json_encode($lineid);
            $b  = json_encode($linepref);
            echo json_encode(array("lineid" => $a, "lineprefix" => $b));
        }
    }

    public function getdatabon(Request $request)
    {
        if ($request->sts == "1") {
            $plancode = $request->plant;

            $qry = DB::table('lph_area')->where('plant_code', $plancode)->get();

            $areaid = [];
            $area = [];
            $option = "<option value='0'>Pilih Area</option>";
            foreach ($qry as $row) {
                $areaid[] = $row->ID;
                $option .= "<option value=" . $row->ID . ">" . $row->area_name . "</option>";
            }

            $data = [
                'option' => $option,
                'areaid' => $areaid
            ];

            return response()->json($data);
        } else if ($request->sts == "2") {
            $plancode = $request->plant;

            $qry = DB::table('lph_line')->where('area_code', $plancode)->get();

            $option = "<option value='0'>Pilih Line</option>";
            foreach ($qry as $row) {
                $option .= "<option value=" . $row->ID . ">" . $row->line_prefix . "</option>";
            }
            return response()->json($option);
        }
        // return response()->json($request->plant);
    }
}
