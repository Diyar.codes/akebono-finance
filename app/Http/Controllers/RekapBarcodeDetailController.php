<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RekapBarcodeDetailController extends Controller
{
    public function index()
    {
        // $bulan= "03";
        // $tahun= "2021";
        // $month_search = $tahun . "-" . $bulan . "-01 07:30";
        // $month_search_next = date("Y-m-d", strtotime($month_search . " +1 month")) . " 07:30";
        // $all = "select a.polibox_item_nbr, b.polibox_snp,locfrom,locto , sum(snp) as jml, effdate_qad
		// 			from polibox_trans a, polibox_mstr b where a.polibox_nbr=b.polibox_nbr and
		// 			a.creadate > '$month_search' and a.creadate < '$month_search_next' and
		// 			b.polibox_item NOT LIKE '32130109' and status='OK'
		// 			group by a.polibox_item_nbr, b.polibox_snp,locfrom,locto, effdate_qad
		// 			order by polibox_item_nbr";
        //             $is = DB::select($all);
        //             dd($is);
        // return view('rekap-barcode-detail.index');
                // $tlg_per = "2021-03-4";
                // $q_itung = DB::table('polibox_trans')
                // ->select('polibox_trans.polibox_item_nbr', 'polibox_mstr.polibox_snp', 'locfrom', 'locto', DB::raw('sum(snp) as jml'))
                // ->join('polibox_mstr', 'polibox_trans.polibox_nbr', '=', 'polibox_mstr.polibox_nbr')
                // ->whereBetween('polibox_trans.creadate', [$month_search, $month_search_next])
                // ->where('effdate_qad', $tlg_per)
                // ->where('polibox_mstr.polibox_item', '!=', '32130109')
                // ->where('locfrom', '<>', 'WHS-P3')
                // ->groupBy('polibox_trans.polibox_item_nbr', 'polibox_mstr.polibox_snp', 'locfrom', 'locto')
                // ->first();

                // dd($q_itung);
        return view('rekap-barcode-detail.detail');
    }

    public function table(Request $request)
    {
        // return $request->bulan;
        // $bln = date("m");
		// $thn = date("Y");
        if ($request->bulan != '' || $request->tahun != '' || $request->itemnumber != '') {
            $bulan = $request->bulan;
            $tahun = $request->tahun;
        } else {
            $bulan = date('m');
            $tahun = date('Y');
        }

        $month_date = date("Y-m-01");
        $month_search = $tahun . "-" . $bulan . "-01 07:30";
        $month_search_next = date("Y-m-d", strtotime($month_search . " +1 month")) . " 07:30";
        $lastMonth = date("m", strtotime($month_date . " -1 days"));
        $hitung = 1;
        $month = date("m");
        $month_st = $tahun . "-" . $bulan . "-01";
        $month_fin = date("Y-m-t", strtotime($month_st));

        //WEB SERVICES PUB.tr_hist //qty used //row
        // $q_used = q_used();
        // $jml_used = array();
        // foreach ($q_used as $row) {
        //     $tr_part_used = $row['item_number'];
        //     $jml_q_used = $row['sumQty'];
        //     $jml_used[$tr_part_used] = $jml_q_used;
        // }
        // // -------------------------


        // $query_used = DB::table('proj_qty_qad')->select('parent_item', 'child_item')->get();
        // $qty_used = array();
        // foreach ($query_used as $row) {
        //     $parent_item = $row->parent_item;
        //     $child_item_used = $row->child_item;
        //     $qty_used[$child_item_used] += $jml_used[$parent_item];
        // }

        $result = DB::table('polibox_trans')
            ->select('polibox_trans.polibox_item_nbr', 'polibox_mstr.polibox_snp', 'locfrom', 'locto')
            ->join('polibox_mstr', 'polibox_trans.polibox_nbr', '=', 'polibox_mstr.polibox_nbr')
            ->whereBetween('polibox_trans.creadate', [$month_search, $month_search_next])
            ->where('polibox_mstr.polibox_item', '!=', '32130109')
            ->where('locfrom', '<>', 'WHS-P3');

        if ($request->itemnumber != '') {
            $result->where('polibox_item_nbr', $request->itemnumber);
        }
        $result->groupBy('polibox_trans.polibox_item_nbr', 'polibox_mstr.polibox_snp', 'locfrom', 'locto');
        $result->orderBy('polibox_trans.polibox_item_nbr', 'desc');
        $data = $result->get();
        $tablenya = '';
        $no = 1;
		$jml_perdate = array();
        foreach ($data as $row) {

            // jmlh ok
            $q_ok = DB::table('polibox_trans')
                ->select('polibox_trans.polibox_item_nbr', 'polibox_mstr.polibox_snp', 'locfrom', 'locto', DB::raw('sum(snp) as jml'))
                ->join('polibox_mstr', 'polibox_trans.polibox_nbr', '=', 'polibox_mstr.polibox_nbr')
                ->whereBetween('polibox_trans.creadate', [$month_search, $month_search_next])
                ->where('polibox_item_nbr', $row->polibox_item_nbr)
                ->where('polibox_snp', $row->polibox_snp)
                ->where('locfrom', $row->locfrom)
                ->where('locto', $row->locto)
                ->where('polibox_mstr.polibox_item', '!=', '32130109')
                ->where('status', '=', 'OK')
                ->groupBy('polibox_trans.polibox_item_nbr', 'polibox_mstr.polibox_snp', 'locfrom', 'locto')
                ->orderBy('polibox_trans.polibox_item_nbr', 'desc')
                ->first();

            //not oke
            $q_pending = DB::table('polibox_trans')
                ->select('polibox_trans.polibox_item_nbr', 'polibox_mstr.polibox_snp', 'locfrom', 'locto', DB::raw('sum(snp) as jml'))
                ->join('polibox_mstr', 'polibox_trans.polibox_nbr', '=', 'polibox_mstr.polibox_nbr')
                ->whereBetween('polibox_trans.creadate', [$month_search, $month_search_next])
                ->where('polibox_item_nbr', $row->polibox_item_nbr)
                ->where('polibox_snp', $row->polibox_snp)
                ->where('locfrom', $row->locfrom)
                ->where('locto', $row->locto)
                ->where('polibox_mstr.polibox_item', '!=', '32130109')
                ->where('status', '<>', 'OK')
                ->groupBy('polibox_trans.polibox_item_nbr', 'polibox_mstr.polibox_snp', 'locfrom', 'locto')
                ->orderBy('polibox_trans.polibox_item_nbr', 'desc')
                ->first();

                $p_i_n = $q_ok->polibox_item_nbr;
                $p_s = $q_ok->polibox_snp;
                $lf = $q_ok->locfrom;
                $lt = $q_ok->locto;
                $jml = $q_ok->jml;
                // $eq = $q_ok->effdate;
			$jml_perdate[$p_i_n][$p_s][$lf][$lt] = $jml;

            $q_des = pt_mstr($row->polibox_item_nbr);

            $q_man = tr_hits($month_st, $month_fin, $row->polibox_item_nbr, $row->locfrom);
            if (!empty($q_man)) {
                $jml_man = $q_man['sumQty'] * -1;
            } else {
                $jml_man = 0;
            }
            $total = $q_ok->jml + $jml_man;

            $query = DB::table('proj_qty_qad')
                ->select('child_item', 'desc1', 'desc2', 'type', 'buyer', 'location', 'pic', DB::raw('SUM(qty_child) as qty_child', 'MAX(qty_on_hand) as qty_on_hand'))
                ->where('child_item', $row->polibox_item_nbr)
                ->where('location', $row->locto)
                ->groupBy('child_item', 'desc1', 'desc2', 'type', 'buyer', 'location', 'pic')
                ->orderBy('pic', 'asc')
                ->orderBy('buyer', 'asc')
                ->orderBy('location', 'asc')
                ->first();

            $q_used = q_used($row->polibox_item_nbr);

            $jml_q_used = $q_used['sumQty'];

            $query_used = DB::table('proj_qty_qad')->select('parent_item', 'child_item')->where('child_item', $row->polibox_item_nbr)->first();
            // $qty_used = array();
            // foreach ($query_used as $row) {
            //     $parent_item = $row->parent_item;
            //     $child_item_used = $row->child_item;
            //     $qty_used[$child_item_used] += $jml_q_used;
            // }
            // $total_need = $query->qty_child + $qty_used[$row->polibox_item_nbr];
            if ($query == null) {
                $qty_child = 0;
            } else {
                $qty_child = $query->qty_child;
            }
            $total_need = $qty_child + $jml_q_used;
            $tablenya .= '<tr>
            <td>' . $no . '</td>
            <td>' . $row->polibox_item_nbr . '</td>
            <td>' . $q_des['deskripsi1'] . '</td>
            <td>' . $q_des['desc_type'] . '</td>
            <td>' . $row->locfrom . '</td>
            <td>' . $row->locto . '</td>

            <td align="right">' . number_format($q_ok->jml, 0) . '</td>
            <td align="right">' . number_format($q_pending->jml, 0) . '</td>
            <td align="right">' . $jml_man . '</td>
            <td align="right">' . number_format($total, 0) . '</td>
            <td align="right">' . number_format($qty_child, 0) . '</td>
            <td align="right">' . number_format($jml_q_used, 0) . '</td>
            <td align="right">' . number_format($total_need, 0) . '</td>';
            for($a=1;$a<=31;$a++){
                $a = str_pad($a, 2, '0', STR_PAD_LEFT);
                $tlg_per = $tahun."-".$bulan."-".$a;
                $q_itung = DB::table('polibox_trans')
                ->select('polibox_trans.polibox_item_nbr', 'polibox_mstr.polibox_snp', 'locfrom', 'locto', DB::raw('sum(snp) as jml'))
                ->join('polibox_mstr', 'polibox_trans.polibox_nbr', '=', 'polibox_mstr.polibox_nbr')
                // ->whereBetween('polibox_trans.creadate', [$month_search, $month_search_next])
                // ->where('polibox_item_nbr', $row->polibox_item_nbr)
                // ->where('polibox_snp', $row->polibox_snp)
                // ->where('locfrom', $row->locfrom)
                // ->where('locto', $row->locto)
                ->where('effdate_qad', $tlg_per)
                ->where('polibox_mstr.polibox_item', '!=', '32130109')
                ->where('locfrom', '<>', 'WHS-P3')
                ->groupBy('polibox_trans.polibox_item_nbr', 'polibox_mstr.polibox_snp', 'locfrom', 'locto')
                ->first();
                if (!empty($q_itung)){
                    $jmll = $q_itung->jml;
                }else{
                    $jmll = "0";
                }
                // $tablenya .= '<td align="right">'.number_format($jml_perdate[$p_i_n][$p_s][$lf][$lt][$tlg_per],0).'</td>';
                // $tablenya .= '<td align="right">'.number_format($jml_perdate[$p_i_n][$tlg_per],0).'</td>';
                $tablenya .= '<td align="right">'.number_format($jmll,0).'</td>';
            }
            $tablenya .= '</tr>';
            $no += 1;
        }

        $data1 = [
            'tablenya' => $tablenya,
        ];

        return response()->json($data1);
    }
}
