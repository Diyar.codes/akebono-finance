<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IntransPending extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->ajax()){
            $month = $request->month;
            $year = $request->year;
            $html = '';

            if($month == 0 && $year != 0){
                // eksekusi tahun
                $data = DB::select("SELECT DISTINCT int_supp, int_inv , int_etd, int_ship, int_no_bl, int_confirm_whs_status FROM intrans_trans where int_confirm_whs_status ='NOK' AND YEAR(int_etd) = '$year'");
            }

            if($month != 0 && $year == 0){
                // eksekusi bulan
                $data = DB::select("SELECT DISTINCT int_supp, int_inv , int_etd, int_ship, int_no_bl, int_confirm_whs_status FROM intrans_trans where int_confirm_whs_status ='NOK' AND MONTH(int_etd) = '$month'");
            }

            if($month != 0 && $year != 0){
                $data = DB::select("SELECT DISTINCT int_supp, int_inv , int_etd, int_ship, int_no_bl, int_confirm_whs_status FROM intrans_trans where int_confirm_whs_status ='NOK' AND YEAR(int_etd) = '$year' AND MONTH(int_etd) = '$month'");
            }

            if($data != null){
                $no = 1;
                foreach($data as $row){
                    $html = '
                    <tr>
                        <td>1</td>
                        <td>'.$row->int_inv.'</td>
                        <td>'.$row->int_no_bl.'</td>
                        <td>'.$row->int_etd.'</td>
                        <td>'.$row->int_ship.'</td>
                        <td><a class="btn btn-sm btn-warning" href="/pending-intrans-detail/'.base64_encode($row->int_inv).'/'.base64_encode($row->int_supp).'/'.base64_encode($row->int_no_bl).'"><span class="fas fa-eye"></span></a></td>
                    </tr>
                    ';
                $no++;
                }
            }else{
                $html = '<tr><td colspan="6">No data available in table</td></tr>';
            }

            return json_encode(['status' => true, 'data' => $html]);
        }


        $data = DB::select("SELECT DISTINCT int_supp, int_inv , int_etd, int_ship, int_no_bl, int_confirm_whs_status
        from intrans_trans where int_confirm_whs_status ='NOK'");

        // dd($data);

        return view('intrans-pending.index', compact('data'));
    }

    public function detail(Request $request, $no_invoice, $kode_supp, $no_bl){
        $no_invoice = base64_decode($no_invoice);
        $kode_supp = base64_decode($kode_supp);
        $no_bl = base64_decode($no_bl);
        $data = DB::table('intrans_trans')
        ->where('int_confirm_whs_ket', 'LIKE', '%ERROR%')
        ->where('int_inv', $no_invoice)->get();
        // ->where('int_inv', '')->get();
        
        if(count($data) == 0){
            $data = 'false';
        }
        // $data = DB::select("SELECT int_supp, int_inv, int_ship, int_part,int_qty_rcp , int_part_deskripsi, int_part_um, int_part_type, int_confirm_whs_ket FROM intrans_trans WHERE int_confirm_whs_ket LIKE '%ERROR%' AND int_inv = '$no_invoice'");
        return view('intrans-pending.detail', ['data' => $data, 'no_invoice' => $no_invoice]);
    }

    public function detailproses(Request $request){
        $no_invoice = $request->no_invoice;
        $confirm = date('m/d/Y');

        $data = DB::select("SELECT DISTINCT * FROM intrans_trans WHERE int_confirm_whs_status ='NOK' AND int_inv = '$no_invoice'");

        foreach($data as $row){
            $kodeSupplier	= $row->int_supp;
            $shipName 		= $row->int_ship;
            $part_item 		= $row->int_part;
            $qty_transfer 	= $row->int_qty_rcp;
            $item_desc 		= $row->int_part_deskripsi;
            $item_um 		= $row->int_part_um;
            $type 			= $row->int_part_type;
            $po 			= $row->int_po;
            //$effective 		= $row->int_etd;
            $effective 		= $row->int_tanggal_actual;

            $itemMaster = pt_mstr($part_item);
            $lokasi_to = $itemMaster['lokasi_item'];
            $lokasi_from = 'INTRANS';

            // cek ke web service (default dulu karena web service belum)
            $cek = true;
            if($cek == true){
                DB::table('intrans_trans')
                ->where('int_inv', $no_invoice)
                ->where('int_part', $part_item)
                ->where('int_po', $po)
                ->update(['int_confirm_whs_date' => NOW(), 'int_confirm_whs_status' => 'OK']);
            }
        }
        alert()->success('Success', 'Intrans Item Pending have been changed successfully');
        return redirect()->route('pending-intrans');
    }
}
