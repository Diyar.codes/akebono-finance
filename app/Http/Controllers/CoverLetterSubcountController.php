<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Exports\ReportDeliverySlip;
use App\Exports\ReportConfirmDeliverySlip;
use App\Exports\ReportMutasiBulanan;
use App\Mail\MainConfirmSuratPengantar;
use App\Mail\MainConfirmSuratPengantarAdmin;
use App\Mail\MainImageToAdmin;
use Yajra\DataTables\Facades\DataTables;
use Vinkla\Hashids\Facades\Hashids;
use RealRashid\SweetAlert\Facades\Alert;
use Maatwebsite\Excel\Facades\Excel;
use App\BusinesRelation;
use App\SupplieMasterSuratJalan;
use App\SupplieDTLSuratJalan;
use App\Supplier_pic_ppc;
use Carbon\Carbon;
use Session;
use Redirect;

class CoverLetterSubcountController extends Controller
{
    public function suratPengantar(Request $request)
    {
        if ($request->ajax()) {
            if ($request->kodeNo) {
                $kodeNo = $request->kodeNo;
                $data = DB::select("SELECT ISNULL(max(substring(no,7,4)),0)+1 as nomor_SP FROM Supplier_MSTR_suratJalan
                WHERE SUBSTRING(no,1,2) = '$kodeNo' AND YEAR(GETDATE()) = YEAR(tanggal)
                AND MONTH(GETDATE()) = MONTH(tanggal)");
                
                if($data[0]->nomor_SP != ''){
                    $data = str_pad($data[0]->nomor_SP, 4, "0", STR_PAD_LEFT);
                }

                return json_encode(['status' => true, 'data' => $data]);
            } else if ($request->type || $request->supplierCode) {
                $data = DB::table('supplier_item_master')
                    ->where('supplier_id', $request->supplierCode)
                    ->where('type', $request->type)
                    ->get();
                if (count($data) > 0) {
                    return json_encode([
                        'status' => true,
                        'data' => $data
                    ]);
                } else {
                    return json_encode([
                        'status' => true,
                        'data' => 'data kosong'
                    ]);
                }
            } else {
                return json_encode(['status' => false, 'data' => 'kosong']);
            }
        } else {
            return view('cover-letter-subcount.cover-letter');
        }
    }

    public function storecoverletter(Request $request)
    {
        // dd($request->all());
        $cek = 0;
        $jumlah_item = $request->jumlah_item - 1;
        for($i = 1; $i <= $jumlah_item; $i++){
            if($request->input('jumlah'.$i) != null){
                $cek++;
            }
        }
        if($cek == 0){
            alert()->error('Error', 'Enter the data in the Amount column');
            return redirect()->back();
        }

        if ($request->typesurat == 'kosong') {
            dd($request->all());
            $suplliermstr = SupplieMasterSuratJalan::create([
                'no'         => $request->no,
                'tujuan'     => $request->supplierCode,
                'tanggal'    => $request->tanggal,
                'kendaraan'  => $request->kendaraan,
                'keterangan' => $request->keterangan,
                'cycle'      => $request->cycle,
                'pengirim'   => $request->pengirim,
                'type'       => $request->typesurat
            ]);
            if ($suplliermstr) {
                alert()->success('Success', 'Add Change successfully');
                return redirect()->back();
            } else {
                alert()->error('Error', 'Add Change error');
                return redirect()->back();
            }
        } else {
            $suplliermstr = SupplieMasterSuratJalan::create([
                'no'         => $request->no,
                'tujuan'     => $request->supplierCode,
                'tanggal'    => $request->tanggal,
                'kendaraan'  => $request->kendaraan,
                'keterangan' => $request->keterangan,
                'cycle'      => $request->cycle,
                'pengirim'   => $request->pengirim,
                'type'       => $request->typesurat
            ]);

            for ($i = 1; $i < $request->jumlah_item; $i++) {
                if ($request->input('jumlah' . $i) != '' && $request->input('namabarang' . $i) != '') {
                    $supplierdtl = SupplieDTLSuratJalan::create([
                        'no'            => $request->no,
                        'kode'          => $request->input('kode' . $i),
                        'namabarang'    => $request->input('namabarang' . $i),
                        'jumlah'        => $request->input('jumlah' . $i),
                        'satuan'        => $request->input('satuan' . $i),
                        'urutan'        => $i,
                    ]);
                }
            }
            if ($suplliermstr && $supplierdtl) {
                alert()->success('Success', 'Add Change successfully');
                return redirect()->back();
            } else {
                alert()->error('Error', 'Add Change error');
                return redirect()->back();
            }
        }
    }

    public function printDeliverySlip(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->filter_dn_number) && !empty($request->filter_year) && !empty($request->filter_moon)) {
                $data = DB::table('Supplier_MSTR_suratJalan')
                    ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'Supplier_MSTR_suratJalan.tujuan')
                    ->where('app_supplier', '!=', 'Reject')
                    ->where('no', '=', $request->filter_dn_number)
                    ->whereMonth('tanggal', $request->filter_moon)
                    ->whereYear('tanggal', $request->filter_year)
                    ->select('Supplier_MSTR_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('tanggal', 'DESC')
                    ->get();
            } else if (!empty($request->filter_id_supp) && !empty($request->filter_year) && !empty($request->filter_moon)) {
                $data = DB::table('Supplier_MSTR_suratJalan')
                    ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'Supplier_MSTR_suratJalan.tujuan')
                    ->where('app_supplier', '!=', 'Reject')
                    ->where('tujuan', '=', $request->filter_id_supp)
                    ->whereMonth('tanggal', $request->filter_moon)
                    ->whereYear('tanggal', $request->filter_year)
                    ->select('Supplier_MSTR_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('tanggal', 'DESC')
                    ->get();
            } else if (!empty($request->filter_moon) && !empty($request->filter_year)) {
                $data = DB::table('Supplier_MSTR_suratJalan')
                    ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'Supplier_MSTR_suratJalan.tujuan')
                    ->where('app_supplier', '!=', 'Reject')
                    ->whereMonth('tanggal', $request->filter_moon)
                    ->whereYear('tanggal', $request->filter_year)
                    ->select('Supplier_MSTR_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('tanggal', 'DESC')
                    ->get();
            } else if (!empty($request->filter_id_supp) && !empty($request->filter_moon)) {
                $data = DB::table('Supplier_MSTR_suratJalan')
                    ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'Supplier_MSTR_suratJalan.tujuan')
                    ->where('app_supplier', '!=', 'Reject')
                    ->where('ct_vd_addr', '=', $request->filter_id_supp)
                    ->whereMonth('tanggal', $request->filter_moon)
                    ->select('Supplier_MSTR_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('tanggal', 'DESC')
                    ->get();
            } else if (!empty($request->filter_id_supp) && !empty($request->filter_year)) {
                $data = DB::table('Supplier_MSTR_suratJalan')
                    ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'Supplier_MSTR_suratJalan.tujuan')
                    ->where('app_supplier', '!=', 'Reject')
                    ->where('ct_vd_addr', '=', $request->filter_id_supp)
                    ->whereYear('tanggal', $request->filter_year)
                    ->select('Supplier_MSTR_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('tanggal', 'DESC')
                    ->get();
            } else if (!empty($request->filter_dn_number) && !empty($request->filter_moon)) {
                $data = DB::table('Supplier_MSTR_suratJalan')
                    ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'Supplier_MSTR_suratJalan.tujuan')
                    ->where('app_supplier', '!=', 'Reject')
                    ->where('no', '=', $request->filter_dn_number)
                    ->whereMonth('tanggal', $request->filter_moon)
                    ->select('Supplier_MSTR_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('tanggal', 'DESC')
                    ->get();
            } else if (!empty($request->filter_dn_number) && !empty($request->filter_year)) {
                $data = DB::table('Supplier_MSTR_suratJalan')
                    ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'Supplier_MSTR_suratJalan.tujuan')
                    ->where('app_supplier', '!=', 'Reject')
                    ->where('no', '=', $request->filter_dn_number)
                    ->whereYear('tanggal', $request->filter_year)
                    ->select('Supplier_MSTR_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('tanggal', 'DESC')
                    ->get();
            } else if (!empty($request->filter_dn_number)) {
                $data = DB::table('Supplier_MSTR_suratJalan')
                    ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'Supplier_MSTR_suratJalan.tujuan')
                    ->where('app_supplier', '!=', 'Reject')
                    ->where('no', '=', $request->filter_dn_number)
                    ->select('Supplier_MSTR_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('tanggal', 'DESC')
                    ->get();
            } else if (!empty($request->filter_id_supp)) {
                $data = DB::table('Supplier_MSTR_suratJalan')
                    ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'Supplier_MSTR_suratJalan.tujuan')
                    ->where('app_supplier', '!=', 'Reject')
                    ->where('tujuan', '=', $request->filter_id_supp)
                    ->select('Supplier_MSTR_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('tanggal', 'DESC')
                    ->get();
            } else if (!empty($request->filter_moon)) {
                $data = DB::table('Supplier_MSTR_suratJalan')
                    ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'Supplier_MSTR_suratJalan.tujuan')
                    ->where('app_supplier', '!=', 'Reject')
                    ->whereMonth('tanggal', $request->filter_moon)
                    ->select('Supplier_MSTR_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('tanggal', 'DESC')
                    ->get();
            } else if (!empty($request->filter_year)) {
                $data = DB::table('Supplier_MSTR_suratJalan')
                    ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'Supplier_MSTR_suratJalan.tujuan')
                    ->where('app_supplier', '!=', 'Reject')
                    ->whereYear('tanggal', $request->filter_year)
                    ->select('Supplier_MSTR_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('tanggal', 'DESC')
                    ->get();
            } else {
                $data = DB::table('Supplier_MSTR_suratJalan')
                    ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'Supplier_MSTR_suratJalan.tujuan')
                    ->where('app_supplier', '!=', 'Reject')
                    ->whereMonth('tanggal', date('m'))
                    ->whereYear('tanggal', date('Y'))
                    ->select('Supplier_MSTR_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_vd_addr', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('tanggal', 'DESC')
                    ->get();
            }

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    if($data->app_supplier == 'Confirmed') {
                        return '
                            <td>
                                <a class="badge badge-warning text-white" href="/view-delivery-slip/' . base64_encode($data->no) . '" title="View"><i class="fas fa-eye"></i></a>
                                <a class="badge badge-info text-white" href="/sp-print/' . base64_encode($data->no) . '" title="Print" target="_blank"><i class="fas fa-print"></i></a>
                            </td>
                        ';
                    } else {
                        return '
                            <td>
                                <a class="badge badge-warning text-white" href="/view-delivery-slip/' . base64_encode($data->no) . '" title="View"><i class="fas fa-eye"></i></a>
                                <a class="badge badge-info text-white" href="/sp-print/' . base64_encode($data->no) . '" title="Print" target="_blank"><i class="fas fa-print"></i></a>
                                <a class="badge badge-dark text-white" href="/edit-delivery-slip/' . base64_encode($data->no) . '" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                                <a class="badge badge-danger text-white" href="#" data-id="' . base64_encode($data->no) . '" id="rejectDtlCoverLetter" title="Reject"><i class="fas fa-trash-alt"></i></a>
                            </td>
                        ';
                    }
                })
                ->make(true);
        }

        return view('cover-letter-subcount.print-delivery-slip');
    }

    public function viewDs($id)
    {
        $ambil_no = SupplieMasterSuratJalan::where('no', base64_decode($id))->first();

        return view('cover-letter-subcount.view-delivery-slip', compact('ambil_no'));
    }

    public function viewDsmain(Request $request, $id) {
        $data = SupplieDTLSuratJalan::where('no', base64_decode($id))->orderBy('namabarang', 'asc')->get();

        $main = '';
        $no = 1;
        foreach($data as $row){
            if($row->jumlah != $row->act_qty) {
                $main .= '<tr style="background-color: #FFFF99">';
            } else {
                $main .= '<tr>';
            }

            $main .= '<td>'.$no++.'</td>
                        <td>'.$row->kode.'</td>
                        <td>'.$row->namabarang.'</td>
                        <td>'.number_format($row->jumlah).'</td>
                        <td>'.number_format($row->act_qty).'</td>
                    </tr>';
        }

        $data = [
            'main' => $main,
        ];

        return response($data);
    }

    public function sp_print($id)
    {
        $data_supp = SupplieMasterSuratJalan::where('no', base64_decode($id))
        ->join('SOAP_Pub_Business_Relation', 'SOAP_Pub_Business_Relation.ct_vd_addr', '=', 'Supplier_MSTR_suratJalan.tujuan')
        ->select('Supplier_MSTR_suratJalan.*','SOAP_Pub_Business_Relation.ct_ad_name')
        ->first();

        $data_item = SupplieMasterSuratJalan::join('supplier_DTL_suratJalan', 'supplier_DTL_suratJalan.no', 'Supplier_MSTR_suratJalan.no')
                    ->where('supplier_DTL_suratJalan.no', '=', $data_supp->no)
                    ->select('Supplier_MSTR_suratJalan.*', 'supplier_DTL_suratJalan.*')
                    ->orderBy('urutan', 'asc')
                    ->get();
        $data_item_count = SupplieMasterSuratJalan::join('supplier_DTL_suratJalan', 'supplier_DTL_suratJalan.no', 'Supplier_MSTR_suratJalan.no')
                    ->where('supplier_DTL_suratJalan.no', '=', $data_supp->no)
                    ->select('Supplier_MSTR_suratJalan.*', 'supplier_DTL_suratJalan.*')
                    ->orderBy('urutan', 'asc')
                    ->count();
        $id = base64_decode($id);

        return view('cover-letter-subcount.sp_print', compact('id', 'data_supp', 'data_item', 'data_item_count'));
    }

    public function editDs($id)
    {
        $ambil_no = SupplieMasterSuratJalan::where('no', base64_decode($id))->first();
        $id = $id;

        return view('cover-letter-subcount.edit-delivery-slip', compact('ambil_no', 'id'));
    }

    public function editDsUpdtDtlMain($id) {
        $data = SupplieDTLSuratJalan::where('no', base64_decode($id))->orderBy(DB::raw("supplier_DTL_suratJalan.urutan+0"))->get();

        $main = '';
        $no = 1;
        foreach($data as $row){
            $main .= '<tr>
                        <td><input type="hidden" class="form-control form-control-sm text-dark" style="min-width:80px" name="urutan[]" value="'.$row->urutan.'">'.$no++.'</td>
                        <td><input type="text" class="form-control form-control-sm text-dark" style="min-width:80px" name="kode[]" value="'.$row->kode.'"></td>
                        <td><input type="text" class="form-control form-control-sm text-dark" style="min-width:80px" name="namabarang[]" value="'.$row->namabarang.'"></td>
                        <td><input type="text" class="form-control form-control-sm text-dark" style="min-width:80px" name="jumlah[]" value="'.number_format($row->jumlah).'"></td>
                        <td><a href="#" class="btn btn-danger remove-item" data-no="'.base64_encode($row->no).'" data-urutan="'.$row->urutan.'"><i class="fas fa-trash"></i></a></td>
                    </tr>';
        }

        $data = [
            'main' => $main,
        ];

        return response($data);
    }

    public function deleteDtlDs($no, $urutan) {
        try {
            $data = SupplieDTLSuratJalan::where('no', base64_decode($no))->where('urutan', $urutan)->delete();

            $response = [
                'status' => true,
                'message' => 'Cover letter details successfully deleted!'
            ];

            return response()->json($response, 200);
        } catch (\Exception $e) {
            DB::rollBack();

            alert()->error('error',$e->getMessage());
            return redirect()->back();
        }
    }

    public function updateDs(Request $request, $id)
    {
        $misspackings = SupplieMasterSuratJalan::where('no', base64_decode($id))->first();
        $misspackingdtls = SupplieDTLSuratJalan::where('no', $misspackings->no)->orderByRaw("supplier_DTL_suratJalan.urutan+0 desc")->first();

        if($misspackingdtls == null) {
            $urutandtl = 0;
        } else {
            $urutandtl = $misspackingdtls->urutan;
        }

        try {
            DB::beginTransaction();

            if($request->urutan != null) {
                foreach($request->urutan as $i=>$v) {
                    SupplieDTLSuratJalan::where('no', $misspackings->no)->where('urutan', $v)->update([
                        // 'kode'         =>> $request->kode[$i],
                        // 'namabarang'   => $request->namabarang[$i],
                        'jumlah'       => $request->jumlah[$i],
                    ]);
                }

                if($request->item != null) {
                    if(count($request->item) > 0) {
                        foreach($request->item as $i=>$v) {
                            if($request->item[$i] != null) {
                                $data = [
                                    'no'         => $misspackings->no,
                                    'urutan'     => $urutandtl + $i + 1,
                                    'kode'       => $request->item[$i],
                                    'namabarang' => $request->desc[$i],
                                    'satuan'     => $request->um[$i],
                                    'jumlah'     => $request->qty[$i],
                                ];

                                SupplieDTLSuratJalan::insert($data);
                            }
                        }
                    }
                }
            } else {
                if($request->item != null) {
                    if(count($request->item) > 0) {
                        foreach($request->item as $i=>$v) {
                            if($request->item[$i] != null) {
                                $data = [
                                    'no'         => $misspackings->no,
                                    'urutan'     => $urutandtl + $i + 1,
                                    'kode'       => $request->item[$i],
                                    'namabarang' => $request->desc[$i],
                                    'satuan'     => $request->um[$i],
                                    'jumlah'     => $request->qty[$i],
                                ];

                                SupplieDTLSuratJalan::insert($data);
                            }
                        }
                    }
                }
            }

            DB::commit();

            alert()->success('success','cover letter details have been changed successfully');
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollBack();

            alert()->error('error',$e->getMessage());
            return redirect()->back();
        }
    }

    public function rejectDs($id)
    {
        try {
            $ambil_no = SupplieMasterSuratJalan::where('no', base64_decode($id))->update([
                'app_supplier' => 'Reject',
            ]);

            $response = [
                'status'     => 'success',
                'message'    => 'Cover letter successfully reject',
            ];

            return response()->json($response, 201);
        } catch (\Exception $e) {
            DB::rollBack();

            alert()->error('error','$e->getMessage()');
            return redirect()->back();
        }
    }

    public function listMissPacking(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->filter_id_supp && !empty($request->filter_no_surat))) {
                $data = DB::select("SELECT * FROM Supplier_MSTR_suratJalan
                    JOIN SOAP_Pub_Business_Relation ON Supplier_MSTR_suratJalan.tujuan = SOAP_Pub_Business_Relation.ct_vd_addr
                    JOIN Supplier_DTL_suratJalan ON Supplier_DTL_suratJalan.no = Supplier_MSTR_suratJalan.no
                    WHERE Supplier_MSTR_suratJalan.app_supplier = 'Confirmed'
                    AND Supplier_DTL_suratJalan.jumlah != Supplier_DTL_suratJalan.act_qty
                    AND Supplier_MSTR_suratJalan.tujuan = '$request->filter_id_supp'
                    AND Supplier_DTL_suratJalan.no = '$request->filter_no_surat'");
            } else if (!empty($request->filter_id_supp)) {
                $data = DB::select("SELECT * FROM Supplier_MSTR_suratJalan
                    JOIN SOAP_Pub_Business_Relation ON Supplier_MSTR_suratJalan.tujuan = SOAP_Pub_Business_Relation.ct_vd_addr
                    JOIN Supplier_DTL_suratJalan ON Supplier_DTL_suratJalan.no = Supplier_MSTR_suratJalan.no
                    WHERE Supplier_MSTR_suratJalan.app_supplier = 'Confirmed'
                    AND Supplier_DTL_suratJalan.jumlah != Supplier_DTL_suratJalan.act_qty
                    AND Supplier_MSTR_suratJalan.tujuan = '$request->filter_id_supp'");
            } else if (!empty($request->filter_no_surat)) {
                $data = DB::select("SELECT * FROM Supplier_MSTR_suratJalan
                    JOIN SOAP_Pub_Business_Relation ON Supplier_MSTR_suratJalan.tujuan = SOAP_Pub_Business_Relation.ct_vd_addr
                    JOIN Supplier_DTL_suratJalan ON Supplier_DTL_suratJalan.no = Supplier_MSTR_suratJalan.no
                    WHERE Supplier_MSTR_suratJalan.app_supplier = 'Confirmed'
                    AND Supplier_DTL_suratJalan.jumlah != Supplier_DTL_suratJalan.act_qty
                    AND Supplier_DTL_suratJalan.no = '$request->filter_no_surat'");
            } else if (!empty($request->filter_moon && !empty($request->filter_year))) {
                $data = DB::select("SELECT * FROM Supplier_MSTR_suratJalan
                    JOIN SOAP_Pub_Business_Relation ON Supplier_MSTR_suratJalan.tujuan = SOAP_Pub_Business_Relation.ct_vd_addr
                    JOIN Supplier_DTL_suratJalan ON Supplier_DTL_suratJalan.no = Supplier_MSTR_suratJalan.no
                    WHERE Supplier_MSTR_suratJalan.app_supplier = 'Confirmed'
                    AND Supplier_DTL_suratJalan.jumlah != Supplier_DTL_suratJalan.act_qty
                    AND YEAR(Supplier_MSTR_suratJalan.tanggal) = $request->filter_year
                    AND MONTH(Supplier_MSTR_suratJalan.tanggal) = $request->filter_moon");
            } else if (!empty($request->filter_moon)) {
                $data = DB::select("SELECT * FROM Supplier_MSTR_suratJalan
                    JOIN SOAP_Pub_Business_Relation ON Supplier_MSTR_suratJalan.tujuan = SOAP_Pub_Business_Relation.ct_vd_addr
                    JOIN Supplier_DTL_suratJalan ON Supplier_DTL_suratJalan.no = Supplier_MSTR_suratJalan.no
                    WHERE Supplier_MSTR_suratJalan.app_supplier = 'Confirmed'
                    AND Supplier_DTL_suratJalan.jumlah != Supplier_DTL_suratJalan.act_qty
                    AND MONTH(Supplier_MSTR_suratJalan.tanggal) = $request->filter_moon");
            } else if (!empty($request->filter_year)) {
                $data = DB::select("SELECT * FROM Supplier_MSTR_suratJalan
                    JOIN SOAP_Pub_Business_Relation ON Supplier_MSTR_suratJalan.tujuan = SOAP_Pub_Business_Relation.ct_vd_addr
                    JOIN Supplier_DTL_suratJalan ON Supplier_DTL_suratJalan.no = Supplier_MSTR_suratJalan.no
                    WHERE Supplier_MSTR_suratJalan.app_supplier = 'Confirmed'
                    AND Supplier_DTL_suratJalan.jumlah != Supplier_DTL_suratJalan.act_qty
                    AND YEAR(Supplier_MSTR_suratJalan.tanggal) = $request->filter_year");
            } else {
                $data = DB::select("SELECT * FROM Supplier_MSTR_suratJalan
                    JOIN SOAP_Pub_Business_Relation ON Supplier_MSTR_suratJalan.tujuan = SOAP_Pub_Business_Relation.ct_vd_addr
                    JOIN Supplier_DTL_suratJalan ON Supplier_DTL_suratJalan.no = Supplier_MSTR_suratJalan.no
                    WHERE Supplier_MSTR_suratJalan.app_supplier = 'Confirmed'
                    AND Supplier_DTL_suratJalan.jumlah != Supplier_DTL_suratJalan.act_qty");
            }

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    return "
                        <td>
                            <a class='mb-1' href='/view-list-miss-packing/" . base64_encode($data->no) . "'>view</a>
                        </td>
                    ";
                })
                ->make(true);
        };

        return view('cover-letter-subcount.list-miss-packing');
    }

    public function showListMissPacking(Request $request, $id)
    {
        $misspackings = DB::table('supplier_MSTR_suratJalan')->where('no', base64_decode($id))->first();

        return view('cover-letter-subcount.view-list-miss-packing', compact('misspackings'));
    }

    public function showListMissPackingMain($id) {
        $data = SupplieDTLSuratJalan::where('no', base64_decode($id))->orderBy('no', 'desc')->get();

        $main = '';
        $no = 1;
        foreach($data as $row){
            if($row->jumlah != $row->act_qty) {
                $main .= '<tr style="background-color: #FFFF99">';
            } else {
                $main .= '<tr>';
            }

            $main .= '<td class="text-wrap">'.$no++.'</td>
                        <td class="text-wrap">'.$row->kode.'</td>
                        <td class="text-wrap">'.$row->namabarang.'</td>
                        <td class="text-wrap">'.number_format($row->jumlah).'</td>
                        <td class="text-wrap">'.number_format($row->act_qty).'</td>
                    </tr>';
        }

        $data = [
            'main' => $main,
        ];

        return response($data);
    }

    public function updateListMissPacking(Request $request, $id)
    {
        $misspackings = DB::table('supplier_MSTR_suratJalan')->where('no', base64_decode($id))->update([
            'remarks' => $request->remarks,
            'remarks_by' => Auth::guard('pub_login')->user()->username,
            'remarks_date' => now(),
        ]);

        $response = [
            'status'     => 'success',
            'message'    => 'Remarks update complete',
        ];

        return response()->json($response, 201);
    }

    public function reportDs() {
        return view('cover-letter-subcount.report-delivery-slip');
    }

    public function searchReportDeliverySlip(Request $request)
    {
        $moon = $request->get('moon');
        $year = $request->get('year');
        $app = $request->get('app');
        $code = $request->get('code');

        if($moon != '' && $year != '' && $app != '' && $code != '') {
            $data = DB::select("SELECT tujuan, day(tanggal) as tanggal, kode, sum(act_qty) as act_qty, sum(jumlah) as jml from supplier_DTL_suratJalan a
						inner join Supplier_MSTR_suratJalan b on a.no = b.no
						where year(tanggal)='$year' and month(tanggal)='$moon' and A.no like 'SC%' and tujuan='$code'
						and app_supplier='$app'
						group by tujuan, day(tanggal), kode
						order by tujuan, kode");

            $j=0;
            $data_group = [];
            $data_group_value = [];
            foreach($data as $da) {
                $tujuan = strtoupper($da->tujuan);
                $tanggal = $da->tanggal;
                $kode = strtoupper($da->kode);
                $act_qty = $da->act_qty;
                $jml = $da->jml;

                $cur_data = $tujuan."||".$kode;

                if(!in_array($cur_data, $data_group)) {
                    $data_group[$j] = $cur_data;
                    $j++;
                }

                $data_group_value[$cur_data][$tanggal] = $act_qty;

                if($app=="Not Confirmed")
                {
                    $data_group_value[$cur_data][$tanggal] = $jml;
                }
            }

            $data1 = DB::select("SELECT a.tujuan, b.kode, namabarang from Supplier_MSTR_suratJalan a
                    inner join Supplier_dtl_suratJalan b on a.no = b.no
                    where year(tanggal)='$year' and app_supplier='$app' and
                    month(tanggal)='$moon' and A.no like 'SC%' and tujuan='$code' group by tujuan, kode, namabarang");

            $total = 0;
            $nomer = 1;
            $table_atas = '';
            $tablenya = '';

            $table_atas = '
                <tr class="backgroudrowblue">
                    <th class="text-white text-wrap tablefontmini2" style="font-size:9px">No</th>
                    <th class="text-white text-wrap tablefontmini2" style="font-size:9px">Item</th>
                    <th class="text-white text-wrap tablefontmini2" style="font-size:9px">Desc</th>';

                    for ($x = 1; $x <=  date('t', mktime(0, 0, 0, $moon, 1, $year)); $x++){
                        $table_atas .=  "<th align='center' class='text-white text-wrap tablefontmini2' style='font-size:9px'>".$x."</th>";
                    }
            $table_atas .= "<th align='center' class='text-white text-wrap tablefontmini2' style='font-size:9px'>Total</th>";
            $table_atas .= '</tr>';

            foreach($data1 as $da1) {
                $supp = strtoupper($da1->tujuan);
                $kode = strtoupper($da1->kode);
                $desc = $da1->namabarang;
                $total = 0;

                $tablenya .=
                '<tr>
                        <td class="text-wrap tablefontmini2" style="max-width:100px; padding: 3px">'.$nomer++.'</td>
                        <td class="text-wrap tablefontmini2" style="max-width:100px; padding: 3px">'.$kode.'</td>
                        <td class="text-wrap tablefontmini2" style="max-width:100px; padding: 3px">'.$desc.'</td>';

                    for ($z = 1; $z <= date('t', mktime(0, 0, 0, $moon, 1, $year)); $z++){
                        if(empty($data_group_value[$supp."||".$kode][$z])) {
                            $tablenya .= "<td align='center' style='max-width:100px; padding: 3px' class='text-wrap tablefontmini2'>0</td>";
                        } else {
                            $tablenya .= "<td align='center' style='max-width:100px; padding: 3px' class='text-wrap tablefontmini2'>".number_format($data_group_value[$supp."||".$kode][$z])."</td>";
                            $total += $data_group_value[$supp."||".$kode][$z];
                        }
                    }
                    $tablenya .= "<td align='center' style='max-width:100px; padding: 3px' class='text-wrap tablefontmini2'>".number_format($total)."</td>";
                    $tablenya .= '</tr>';
            }

            $data = [
                'atas' => $table_atas,
                'bawah' => $tablenya,
            ];

            return response($data);
        } else {
            $table_atas = '';
            $tablenya = '';

            $table_atas .= '<tr style="background-color: #0066CC;">
                <td class="text-white text-wrap tablefontmini2" style="font-size:9px;">No</td>
                <td class="text-white text-wrap tablefontmini2" style="font-size:9px">Item</td>
                <td class="text-white text-wrap tablefontmini2" style="font-size:9px">Desc</td>';
                for ($x = 1; $x <= 31; $x++){
                    $table_atas .= '<td class="text-white text-wrap tablefontmini2" style="font-size:9px">'.$x.'</td>';
                }
            $table_atas .= '<td class="text-white text-wrap tablefontmini2" style="font-size:9px">Total</td>';
            $table_atas .= '</tr>';

            $data = [
                'atas' => $table_atas,
                'bawah' => $tablenya,
            ];

            return response($data);
        }
    }

    public function excelReportDeliverySlip(Request $request) {
        $moon = $request->get('moonExcel');
        $year = $request->get('yearExcel');
        $app = $request->get('appExcel');
        $code = $request->get('codeExcel');

        // ob_end_clean();
        // ob_start();
        return Excel::download(new ReportDeliverySlip($moon, $year, $app, $code), 'SP_report.xlsx');
    }

    public function monthlyDsReport(Request $request)
    {
        return view('cover-letter-subcount.monthly-ds-report');
    }

    public function searchMonthlyDsReport(Request $request) {
        $moon = $request->get('moon');
        $year = $request->get('year');
        $code = $request->get('code');

        if($moon != '' && $year != '' && $code != '') {
            $data = DB::select("SELECT kode, namabarang, sum(jumlah) as total, satuan, sum(act_qty) as total_act,location_to from Supplier_MSTR_suratJalan a
                        left join supplier_DTL_suratJalan b on a.no = b.no
                        inner join supplier_item_master c on a.tujuan = c.supplier_id and b.kode=c.item_no
                        where month(tanggal)='$moon' and year(tanggal)='$year' and tujuan='$code' and kode is not NULL
                        group by kode, namabarang, satuan, location_to");
        }

		$nomer = 1;
        $table = '';

        if($moon=="01") {
            $bln_bef = "12";
            $thn_bef = $year - 1;
        } else {
            $bln_bef = $moon - 1;
            $thn_bef = $year;
        }

        $moonBefore = $bln_bef;
        $yearBefore = $thn_bef;

        foreach($data as $i => $da) {
            // if($i < 2) {
            $buyer = getDataPtMstrByD($da->kode);
            if($buyer == null) {
                $ptmstr = '-';
            } else {
                $ptmstr = $buyer;
            }

            // $ptmstr = 0;

            $getQtyStockBegin = getQtyStockBegin($da->kode, $da->location_to, $moonBefore, $yearBefore);
            if($getQtyStockBegin == null) {
                $stock_begin = 0;
            } else {
                $stock_begin = $getQtyStockBegin->stock_begin;
            }

            // $stock_begin = 0;

            // $getQtyStockIn = getStockIn($da->kode, $da->total_act, $moon, $year);
            // if($getQtyStockIn == null) {
                // $tot_qad = 0;
            // } else {
                // $tot_qad = $getQtyStockIn->sumQty;
            // }

            $tot_qad = 0;

            $selisih = $tot_qad - $da->total_act;
            if($selisih != 0) {
                $warna="red";
            } else {
                $warna="";
            }

            // $getQtyStockOut = getStockOut($da->kode, $da->location_to, $moon, $year);
            // if($getQtyStockOut == null) {
            //     $tot_qad_out = 0;
            // } else {
            //     $tot_qad_out = $getQtyStockOut->tr_qty_loc;
            // }

            $tot_qad_out = 0;

            // $adjust = getAdjust($da->kode,$da->location_to, $moon, $year);
            // if($adjust == null) {
            //     $adjust_print = 0;
            // } else {
            //     $adjust_print = $adjust->tr_qty_loc;
            // }

            $adjust_print = 0;

            $end_stock = ($stock_begin + $tot_qad) - ($tot_qad_out + $adjust_print);

            // <td class="text-wrap">'.$ptmstr['desc_type'].'</td>
            // <td class="text-wrap">'.$ptmstr['buyer_planner'].'</td>


            $table .=
                '<tr>
                    <td class="text-wrap">'.$nomer++.'</td>
                    <td class="text-wrap">'.$da->kode.'</td>
                    <td class="text-wrap">'.$da->namabarang.'</td>
                    <td class="text-wrap">'.$ptmstr['desc_type'].'</td>
                    <td class="text-wrap">'.$da->satuan.'</td>
                    <td class="text-wrap">'.$ptmstr['buyer_planner'].'</td>
                    <td class="text-wrap">'.number_format($stock_begin).'</td>
                    <td class="text-wrap">'.number_format($tot_qad).'</td>
                    <td class="text-wrap">'.number_format($da->total_act).'</td>
                    <td class="text-wrap" style="background-color:'.$warna.'">'.number_format($selisih).'</td>
                    <td class="text-wrap">'.number_format($tot_qad_out).'</td>
                    <td class="text-wrap">'.number_format($adjust_print).'</td>
                    <td class="text-wrap">'.number_format($end_stock).'</td>
                    <td class="text-wrap"><a href="/search-monthly-ds-report-detail/'.base64_encode($da->kode).'/'.base64_encode($moon).'/'.base64_encode($year).'/'.base64_encode($da->location_to).'/'.base64_encode($code).'">detail</a></td>
                    ';
                $table .= '</tr>';
            // }
        }
        $data = [
            'table' => $table,
        ];

        return response($data);
    }

    public function searchMonthlyDsReportDetail($code, $moon, $year, $locto, $app) {
        return view('cover-letter-subcount.monthly-ds-report-detail');
    }

    public function searchMonthlyDsReportDetailMain($code, $moon, $year, $locto, $app) {
        $codeend = base64_decode($code);
        $moonend = base64_decode($moon);
        $yearend = base64_decode($year);
        $loctoend = base64_decode($locto);
        $append = base64_decode($app);

        $number = date('t', mktime(0, 0, 0, base64_decode($moon), 1, base64_decode($year)));

        $nomer = 1;
        $table = '';
        for($b=1;$b<=$number;$b++) {
            if( $b < 10) {
                $b = 0 . $b;
            }

            $query = DB::select("SELECT sum(act_qty) as total_act from Supplier_MSTR_suratJalan a
						left join supplier_DTL_suratJalan b on a.no = b.no
						inner join supplier_item_master c on a.tujuan = c.supplier_id and b.kode=c.item_no
						where month(tanggal)='$moonend' and year(tanggal)='$yearend' and day(tanggal)='$b' and tujuan='$append' and kode ='$codeend'");

            $tot_qty = $query[0]->total_act;

            if($tot_qty == null) {
                $tot_qtyy = 0;
            } else {
                $tot_qtyy = $tot_qty;
            }

            $twodigityearend = substr($yearend, -2);
            $dateend = $moonend . '-' . $b . '-' . $twodigityearend;

            $stockin = getStockInDate($codeend, $loctoend, $dateend);
            if($stockin == null) {
                $tot_qad = 0;
            } else {
                $tot_qad = $stockin['sumQty'];
            }

            // $tot_qad = 0;

            $getQtyStockOut = getStockOutDate($codeend, $loctoend, $dateend);
            if($getQtyStockOut == null) {
                $tot_qad_out = -1 * 0;
            } else {
                $tot_qad_out = -1 * $getQtyStockOut['sumQty'];
            }

            // $tot_qad_out = 0;

            $table .=
                '<tr>
                    <td class="text-wrap">'.$nomer++.'</td>
                    <td class="text-wrap">'.$codeend.'</td>
                    <td class="text-wrap">'.number_format($tot_qtyy).'</td>
                    <td class="text-wrap">'.number_format($tot_qad).'</td>
                    <td class="text-wrap">'.number_format($tot_qad_out).'</td>
                    ';
            $table .= '</tr>';
        }

        $data = [
            'main' => $table,
        ];

        return response($data);
    }

    public function excelMonthlyDsReportDetail(Request $request) {
        $kode = $request->get('kodeExcel');
        $moon = $request->get('moonExcel');
        $year = $request->get('yearExcel');
        $locto = $request->get('loctoExcel');
        $app = $request->get('appExcel');

        // ob_end_clean();
        // ob_start();
        return Excel::download(new ReportMutasiBulanan($kode, $moon, $year, $locto, $app), 'mutasi.xlsx');
    }

    public function confirmDeliverySlip(Request $request) {
        if ($request->ajax()) {
			$month = date("m");
            $year = date("Y");
            if(Auth::guard('pub_login')->user()->previllage == 'Admin') {
                $data = DB::select("SELECT * from supplier_MSTR_suratJalan  where supplier_MSTR_suratJalan.app_supplier != 'Reject' and
                year(tanggal)='$year' and month(tanggal)='$month' and substring(no,1,2)='SC' order by supplier_MSTR_suratJalan.tanggal desc");
            } else if(!empty($request->no_surat)) {
                $data = SupplieMasterSuratJalan::where('tujuan', '=', Auth::guard('pub_login')->user()->kode_supplier)->where('no', 'like', '%' . $request->no_surat . '%')->where('app_supplier', '!=', 'Reject')->orderBy('tanggal', 'desc')->get();
            } else if(!empty($request->moon) && !empty($request->year)) {
                $data = SupplieMasterSuratJalan::where('tujuan', '=', Auth::guard('pub_login')->user()->kode_supplier)->whereMonth('tanggal', '=', $request->moon)->whereYear('tanggal', '=', $request->year)->where('app_supplier', '!=', 'Reject')->orderBy('tanggal', 'desc')->get();
            } else {
                $data = SupplieMasterSuratJalan::where('tujuan', '=', Auth::guard('pub_login')->user()->kode_supplier)->where('app_supplier', '!=', 'Reject')->whereMonth('tanggal', $month)->whereYear('tanggal', $year)->orderBy('tanggal', 'desc')->get();
            }

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    if($data->app_supplier == 'Confirmed') {
                        return "
                            <td>
                                <a class='mb-1' href='/confirm-delivery-slip-view-confirmed/" . base64_encode($data->no) . "'>view(Confirmed)</a>
                            </td>
                        ";
                    } else if($data->type == 'NG') {
                        return "
                            <td>
                                <a class='mb-1' href='/confirm-delivery-slip-view-confirmed/" . base64_encode($data->no) . "'>view(NG)</a>
                            </td>
                        ";
                    } else {
                        return "
                            <td>
                                <a class='mb-1' href='/confirm-delivery-slip-view/" . base64_encode($data->no) . "'>view</a>
                            </td>
                        ";
                    }
                })
                ->make(true);
        };

        return view('cover-letter-subcount.confirm-delivery-slip');
    }

    public function excelConfirmDeliverySlip(Request $request) {
        $user = Auth::guard('pub_login')->user()->kode_supplier;
        $moon = $request->get('moonExcel');
        $year = $request->get('yearExcel');

        // ob_end_clean();
        // ob_start();
        return Excel::download(new ReportConfirmDeliverySlip($moon, $year, $user), 'sptoexcel.xlsx');
    }

    public function confirmDeliverySlipView($id) {
        $id = $id;
        $no = base64_decode($id);

        return view('cover-letter-subcount.confirm-delivery-slip-view', compact('id', 'no'));
    }

    public function confirmDeliverySlipViewMain($id) {
        $data = SupplieMasterSuratJalan::leftjoin('supplier_DTL_suratjalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratjalan.no')->where('supplier_DTL_suratjalan.no', '=', base64_decode($id))->orderBy('namabarang', 'asc')->get();

        $main = '';
        $no = 1;
        foreach($data as $row){
            $main .= '<tr>
                <td class="text-wrap">'.$no++.'</td>
                <td class="text-wrap">'.$row->kode.'</td>
                <td class="text-wrap">'.$row->namabarang.'</td>
                <td class="text-wrap">'.number_format($row->jumlah).'<input type="hidden" name="aaij_qty[]" value="'.number_format($row->jumlah).'" style="width:80px"</td>
                <td class="text-wrap"><input type="number" name="act_qty[]" value="'.number_format($row->jumlah).'" style="width:80px"></td>
                <td class="text-wrap"><input type="text" name="remark[]" value="'.$row->remark.'" style="width:80px"></td>
            </tr>';
        }

        $data = [
            'main' => $main,
        ];

        return response($data);
    }

    public function updateConfirmDeliverySlipView(Request $request, $id) {
        $cekid = base64_decode($id);
        $data = SupplieMasterSuratJalan::where('no', base64_decode($id))->first();
        if($data->app_supplier !="Confirmed") {
            if(Auth::guard('pub_login')->user()->kode_supplier == 106) {
                $request->validate([
                    'file' => 'mimes:gif,GIF,jpeg,JPEG,jpg,png,JPG,PNG,pdf,PDF|max:5000|unique:Supplier_MSTR_suratJalan'
                ]);

                if($request->file('attachment')) {
                    $file = $request->file('attachment');
                    $nameImage = $file->getClientOriginalName();
                    $nameImageEnd = date('mdY') . $nameImage;
                    $file->move(public_path('images/Attachment/'), $nameImageEnd);
                    $imageName = $nameImageEnd;
                } else {
                    $imageName = '';
                }
            } else {
                $imageName = '';
            }

            SupplieMasterSuratJalan::where('no', base64_decode($id))->update([
                'app_supplier' => 'Confirmed',
                'confirm_supplier_date' => now(),
                'attachment' => $imageName
            ]);

            $data1 = SupplieDTLSuratJalan::where('no', base64_decode($id))->get();
            if(count($data1) > 0) {
                $qty_tot_aaij = 0;
                $qty_tot_act = 0;
                foreach($data1 as $item=>$v) {
					//for effdate Inbound
                    // $queryTgl=mssql_query("select CONVERT(varchar(10), tanggal, 120) AS tanggal,tujuan from Supplier_MSTR_suratJalan where no='$kd'");
                    // $getTgl=mssql_fetch_array($queryTgl);
                    // $tanggal=$getTgl['tanggal'];
                    // $tujuan=$getTgl['tujuan'];

                    $queryTgl= collect(\DB::select("select CONVERT(varchar(10), tanggal, 120) AS tanggal,tujuan from Supplier_MSTR_suratJalan where no='$cekid'"))->first();
				    $tanggalend=$queryTgl->tanggal;

                    $queryLoc = collect(\DB::select("select item_no,location_from,location_to from supplier_item_master where item_no='$v->kode' and supplier_id='$v->tujuan' and location_from <>'-'"))->first();
                    if($queryLoc) {
                        $locFrom = $queryLoc->location_from;
					    $locTo = $queryLoc->location_to;
                    } else {
                        $locFrom = '-';
					    $locTo = '-';
                    }
					
                    //end

                    $queryLoc = DB::select("SELECT item_no,location_from,location_to from supplier_item_master where item_no='$v->kode' and supplier_id='$data->tujuan' and location_from <>'-'");
                    if($queryLoc) {
                        if($queryLoc[0]->item_no) {
                            $item_no = $queryLoc[0]->item_no;
                        } else {
                            $item_no = '';
                        }
                    } else {
                        $item_no = '';
                    }

                    if($item_no != '') {
                        // Masih ada satu pengecekan qxinbounds
                        $cek = inboundSJ($v->kode, $tanggalend, $v->qty, $cekid, $locFrom, $locTo);
                        
                        if($cek == true) {
                            // kurang session keterangan dari qxinbound
                            $qxtend = SupplieDTLSuratJalan::where('no', $v->no)->where('kode', $v->kode)->update([
                            'act_qty'    => $request->act_qty[$item],
                            'qxInbound'  => 'OK',
                            'keterangan' => $cek['keterangan'],
                            'remark'     => $request->remark[$item],
                            ]);
                        } else {
                            // kurang session keterangan dari qxinbound
                            $qxtend = SupplieDTLSuratJalan::where('no', $v->no)->where('kode', $v->kode)->update([
                            'act_qty'    => $request->act_qty[$item],
                            'qxInbound'  => 'NOK',
                            'keterangan' => $cek['keterangan'],
                            'remark'     => $request->remark[$item],
                            ]);
                        }

                        
                    } else {
                        $no = SupplieDTLSuratJalan::where('no', $v->no)->where('kode', $v->kode)->update([
                            'act_qty'    => $request->act_qty[$item],
                            'qxInbound'  => 'NOK',
                            'keterangan' => 'Local Item'
                        ]);
                    }

                    $qty_tot_aaij += $request->aaij_qty[$item];
                    $qty_tot_act += $request->act_qty[$item];
                }

                $dataemail = SupplieMasterSuratJalan::leftjoin('supplier_DTL_suratjalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratjalan.no')->where('supplier_DTL_suratjalan.no', '=', base64_decode($id))->get();


                $idCon = base64_decode($id);

                $emailto = Supplier_pic_ppc::where('kd_supplier', '=', $dataemail[0]->tujuan)->first();
                $nameto = $emailto->pic_aaij;

                if($qty_tot_aaij != $qty_tot_act) {
                    if($emailto->pic_email) {
                        Mail::to($emailto->pic_email)->send(new MainConfirmSuratPengantar($dataemail, $idCon, $nameto));
                    }
                }

                if(Auth::guard('pub_login')->user()->kode_supplier == 106) {
                    $attach_file = $data->attachment;
                    if($emailto->pic_email) {
                        // admin.warehouse@akebono-astra.co.id
                        // Mail::to('admin.warehouse@akebono-astra.co.id')->send(new MainConfirmSuratPengantarAdmin($dataemail, $idCon, $nameto, $attach_file));
                        Mail::to($emailto->pic_email)->send(new MainConfirmSuratPengantarAdmin($dataemail, $idCon, $nameto, $attach_file));
                    }
                }

            }

            $response = [
                'status'     => 'success',
                'message'    => 'Thank you for your confirmation',
            ];

            return response()->json($response, 201);
        }
    }

    // public function updateConfirmDeliverySlipView(Request $request, $id) {
    //     $data = SupplieMasterSuratJalan::where('no', base64_decode($id))->first();

    //     if($data->app_supplier !="Confirmed") {
    //         if(Auth::guard('pub_login')->user()->kode_supplier == 106) {
    //             $file = $request->file('file');
    //             if($file) {
    //                 $imageName = time() . '' . $file->getClientOriginalName();
    //                 $file->move(public_path('images/approvaldeliveryslip'), $imageName);
    //             } else {
    //                 $imageName = '';
    //             }
    //         }

    //         SupplieMasterSuratJalan::where('no', base64_decode($id))->update([
    //             'app_supplier' => 'Confirmed',
    //             'confirm_supplier_date' => now(),
    //             'attachment' => $imageName
    //         ]);

    //         $data1 = SupplieDTLSuratJalan::where('no', base64_decode($id))->get();
    //         if(count($data1) > 0) {
    //             $qty_tot_aaij = 0;
    //             $qty_tot_act = 0;
    //             foreach($data1 as $item=>$v) {
    //                 $queryLoc = DB::select("SELECT item_no,location_from,location_to from supplier_item_master where item_no='$v->kode' and supplier_id='$data->tujuan' and location_from <>'-'");
    //                 if($queryLoc) {
    //                     // Masih ada satu pengecekan qxinbounds
    //                     $qxtend = SupplieDTLSuratJalan::where('no', $v->no)->where('kode', $v->kode)->update([
    //                         'act_qty'    => $request->act_qty[$item],
    //                         'qxInbound'  => 'OK',
    //                         'keterangan' => 'keterangan',
    //                         'remark'     => $request->remark[$item],
    //                     ]);
    //                 } else {
    //                     $no = SupplieDTLSuratJalan::where('no', $v->no)->where('kode', $v->kode)->update([
    //                         'act_qty'    => $request->act_qty[$item],
    //                         'qxInbound'  => 'NOK',
    //                         'keterangan' => 'Local Item'
    //                     ]);
    //                 }

    //                 $qty_tot_aaij += $request->aaij_qty[$item];
    //                 $qty_tot_act += $request->act_qty[$item];
    //             }

    //             dd($qty_tot_aaij, $qty_tot_act);

    //             $dataemail = SupplieMasterSuratJalan::leftjoin('supplier_DTL_suratjalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratjalan.no')->where('supplier_DTL_suratjalan.no', '=', base64_decode($id))->get();
    //             $idCon = base64_decode($id);

    //             $emailto = Supplier_pic_ppc::where('kd_supplier', '=', $dataemail[0]->tujuan)->first();
    //             $nameto = $emailto->pic_aaij;

    //             if($qty_tot_aaij != $qty_tot_act) {
    //                 Mail::to($emailto->pic_email)->send(new MainConfirmSuratPengantar($dataemail, $idCon, $nameto));
    //             }

    //             if(Auth::guard('pub_login')->user()->kode_supplier == 106) {
	// 				$attach_file = $data['attachment'];

    //             }

    //             alert()->success('Success','successful cover letter on confirm');
    //             return redirect()->route('confirm-delivery-slip');
    //         }
    //     }
    // }

    public function confirmDeliverySlipViewConfirmed(Request $request, $id) {
        $no = base64_decode($id);

        return view('cover-letter-subcount.confirm-delivery-slip-confirmed', compact('no'));
    }

    public function confirmDeliverySlipViewConfirmedMain(Request $request, $id) {
		$data = SupplieDTLSuratJalan::where('no', base64_decode($id))->orderBy('namabarang', 'asc')->get();

        $main = '';
        $no = 1;
        foreach($data as $row){
            if($row->jumlah != $row->act_qty) {
                $main .= '<tr style="background-color: #FFFF99">';
            } else {
                $main .= '<tr>';
            }

            $main .= '<td class="text-wrap">'.$no++.'</td>
                        <td class="text-wrap">'.$row->kode.'</td>
                        <td class="text-wrap">'.$row->namabarang.'</td>
                        <td class="text-wrap">'.number_format($row->jumlah).'</td>
                        <td class="text-wrap">'.number_format($row->act_qty).'</td>
                    </tr>';
        }

        $data = [
            'main' => $main,
        ];

        return response($data);
    }
}
