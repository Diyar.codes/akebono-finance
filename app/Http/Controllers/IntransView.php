<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IntransView extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $bulan = $request->get('bulan');
            $tahun = $request->get('tahun');
            $code = $request->get('code');
            $countainer = $request->get('countainer');

            if($code != ''){
                if($countainer != ''){
                    if($bulan != 0 && $tahun != 0){
                        // eksekusi kode, bulan & tahun, container');
                        $data = DB::select("SELECT * FROM intrans_trans JOIN SOAP_Pub_Business_Relation ON SOAP_Pub_Business_Relation.ct_vd_addr = intrans_trans.int_supp WHERE YEAR(int_etd) = '$tahun' AND MONTH(int_etd) = '$bulan' AND int_supp = '$code' AND int_countainer_packaging = '$countainer' ORDER BY int_etd DESC");
                    }else{
                        // eksekusi kode, container
                        $data = DB::select("SELECT * FROM intrans_trans JOIN SOAP_Pub_Business_Relation ON SOAP_Pub_Business_Relation.ct_vd_addr = intrans_trans.int_supp WHERE int_supp = '$code' AND int_countainer_packaging = '$countainer' ORDER BY int_etd DESC");
                    }
                }else{
                    if($bulan != 0 && $tahun != 0){
                        // eksekusi code, bulan & tahun
                        $data = DB::select("SELECT * FROM intrans_trans JOIN SOAP_Pub_Business_Relation ON SOAP_Pub_Business_Relation.ct_vd_addr = intrans_trans.int_supp WHERE YEAR(int_etd) = '$tahun' AND MONTH(int_etd) = '$bulan' AND int_supp = '$code' ORDER BY int_etd DESC");
                    }else{
                        // eksekusi code
                        $data = DB::select("SELECT * FROM intrans_trans JOIN SOAP_Pub_Business_Relation ON SOAP_Pub_Business_Relation.ct_vd_addr = intrans_trans.int_supp WHERE int_supp = '$code' ORDER BY int_etd DESC");
                    }
                }
            }else{
                if($countainer != ''){
                    if($bulan != 0 && $tahun != 0){
                        // eksekusi container, bulan & tahun
                        $data = DB::select("SELECT * FROM intrans_trans JOIN SOAP_Pub_Business_Relation ON SOAP_Pub_Business_Relation.ct_vd_addr = intrans_trans.int_supp WHERE YEAR(int_etd) = '$tahun' AND MONTH(int_etd) = '$bulan' AND int_countainer_packaging = '$countainer' ORDER BY int_etd DESC");
                    }else{
                        // eksekusi container
                        $data = DB::select("SELECT * FROM intrans_trans JOIN SOAP_Pub_Business_Relation ON SOAP_Pub_Business_Relation.ct_vd_addr = intrans_trans.int_supp WHERE int_countainer_packaging = '$countainer' ORDER BY int_etd DESC");
                    }
                }else{
                    if($bulan != 0 && $tahun != 0){
                        // eksekusi bulan & tahun
                        $data = DB::select("SELECT * FROM intrans_trans JOIN SOAP_Pub_Business_Relation ON SOAP_Pub_Business_Relation.ct_vd_addr = intrans_trans.int_supp WHERE YEAR(int_etd) = '$tahun' AND MONTH(int_etd) = '$bulan' ORDER BY int_etd DESC");
                    }else{
                        $data = false;
                    }
                }
            }

            if($data == false){
                return json_encode(['status' => false, 'data' => false]);
            }

            $arrData = [];
            $no = 0;
            foreach($data as $row){
                $getDataDelivery = DB::table('intrans_leadtime_master')->where('lead_kode_supplier', $row->int_supp)->first();
                $getLeadDelivery = $getDataDelivery->lead_delivery;
                $estimasi_tgl_kedatangan = date('Y-m-d', strtotime('+'.$getLeadDelivery.'days', strtotime($row->int_etd)));

                $rInv = DB::select("SELECT distinct int_cek, int_cek from intrans_trans where int_inv= '$row->int_inv' and int_supp='$row->int_supp'");
                $jmlInv = count($rInv);
                $cek = $rInv[0]->int_cek;

                if ($jmlInv > 1) {
                    $confirm = 'Edit';
                } else {
                    if ($cek == 'T') {
                        $confirm = $row->int_confirm;
                    } else {
                        $confirm = '';
                    }
                }

                $arrData[$no]['int_supp'] = $row->int_supp;
                $arrData[$no]['ct_ad_name'] = $row->ct_ad_name;
                $arrData[$no]['int_inv'] = $row->int_inv;
                $arrData[$no]['int_etd'] = $row->int_etd;
                $arrData[$no]['int_ship'] = $row->int_ship;
                $arrData[$no]['int_countainer_packaging'] = $row->int_countainer_packaging;
                $arrData[$no]['int_shift'] = $row->int_shift;
                $arrData[$no]['int_penerima_barang'] = $row->int_penerima_barang;
                $arrData[$no]['int_shift'] = $row->int_shift;
                $arrData[$no]['int_lokasi'] = $row->int_lokasi;
                $arrData[$no]['estimasi_tgl_kedatangan'] = $estimasi_tgl_kedatangan;
                $arrData[$no]['confirm'] = $confirm;
                $arrData[$no]['int_supp_encode'] = base64_encode($row->int_supp);
                $arrData[$no]['int_inv_encode'] = base64_encode($row->int_inv);
                $no++;
            }

            if(count($data) > 0){
                echo json_encode(['status' => true, 'data' => $arrData]);
            }else{
                echo json_encode(['status' => false, 'data' => 'data kosong']);
            }

        } else {
            $data = DB::select("SELECT distinct * FROM intrans_trans LEFT OUTER JOIN SOAP_Pub_Business_Relation ON SOAP_Pub_Business_Relation.ct_vd_addr = intrans_trans.int_supp WHERE YEAR(int_etd) = YEAR(GETDATE()) AND MONTH(int_etd) = MONTH(GETDATE()) ORDER BY int_etd DESC");
            return view('intrans-view.index', compact('data'));
        }
    }

    public function searchContainer(Request $request){
        $data = DB::select("SELECT DISTINCT int_inv, int_countainer_packaging FROM intrans_trans --WHERE int_lpc = '1'");
        if(count($data) == 0){
            return json_encode(['status' => false, 'data' => 'kosong']);
        }
        return json_encode(['status' => true, 'data' => $data]);
        
    }

    public function detail(Request $request, $kodesup, $inv)
    {
        $getKodeSup = base64_decode($kodesup);
        $getInv = base64_decode($inv);

        $data = DB::select("SELECT distinct int_po,int_line,int_part,int_lot,int_qty_rcp,int_qxtend_stat
        FROM intrans_trans WHERE int_supp = '$getKodeSup' AND int_inv = '$getInv'");

        $nameSupplier = DB::select("SELECT ct_ad_name FROM SOAP_Pub_Business_Relation WHERE ct_vd_addr = '$getKodeSup'");
        if ($nameSupplier == NULL) {
            $nameSupplier = '';
        } else {
            $nameSupplier = $nameSupplier[0]->ct_ad_name;
        }

        $document = DB::table('intrans_doc_prc')->where('doc_no_invoice', $getInv)->where('doc_file', '!=', '')->get();

        return view('intrans-view.detail', [
            'data' => $data,
            'kode_supp' => $getKodeSup,
            'inv' => $getInv,
            'name_supp' => $nameSupplier,
            'document' => $document
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
