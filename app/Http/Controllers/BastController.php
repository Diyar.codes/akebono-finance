<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;
use Spipu\Html2Pdf\Html2Pdf;
use Validator;
use App\Mail\sendEmailBast;

class BastController extends Controller
{
    public function index()
    {

        $supp       = Auth::guard('pub_login')->user()->kode_supplier;
        $url        = "https://supplier.akebono-astra.co.id/wsatdw/po_mstr.php";
        $getpo      = po_mstr($url);
        $data_supp  = DB::table('SOAP_Pub_business_relation')->where('ct_vd_addr', $supp)->first();

        $hitung_po  = 1;
        $bulan = date("m");
        $tahun = date("Y");
        // format nomor berita acara
        $tampil_no_berita = DB::select("SELECT isnull(max(SUBSTRING(no_nbr_acr,1,3)),0)+1 as nomor_ba
                            from bast_mstr2 where month(effdate)='" . $bulan . "' AND year(effdate)='" . $tahun . "'");

        $nomor_ba = str_pad($tampil_no_berita[0]->nomor_ba, 3, "0", STR_PAD_LEFT);

        $Ba_nomor = $nomor_ba . "/BAST/" . $bulan . "/" . $tahun;

        return view('bast.index', compact('getpo', 'hitung_po', 'supp', 'data_supp', 'Ba_nomor'));
    }

    public function rincian(Request $request)
    {
        $po                     = $request->get('ponya');
        $getpodetail            = po_detail($po);
        // dd($getpodetail[0]['no_pp']);
        $trans                  = DB::table('PP_Trans_Mstr')->where('ppm_no_pp', $getpodetail[0]['no_pp'])->first();
        $data['div_head']       = ucwords($trans->ppm_app2);
        $data['nama_section']   = $trans->ppm_app0;
        $data['no_pp']          = $getpodetail[0]['no_pp'];
        $tablenya               = '';
        $itungdata              = count($getpodetail);
        if (count($getpodetail) > 0) {
            $hitung = 0;
            foreach ($getpodetail as $pod) {

                $line      = $pod['line'];
                $desc       = $pod['item_deskripsi'];
                $satuan  = $pod['po_um'];
                $qty_po  = $pod['qty_po'];
                $qty_lpb = $pod['qty_receive'];
                $item      = $pod['item_number'];

                $qty_open = $qty_po - $qty_lpb;
                if (empty($desc)) {
                    $rs_cek_part = pt_mstr($item);
                    $deskripsi     = $rs_cek_part['deskripsi1'];
                    $deskripsi2     = $rs_cek_part['deskripsi2'];

                    $desc1 = $deskripsi . " " . $deskripsi2;
                } else {
                    $desc1 = $desc;
                }
                $tablenya .= '
                    <tr>
                        <td>' . $line . '</td>
                        <td>' . $desc1 . '</td>
                        <td>' . $satuan . '</td>
                        <td><input type="text" name="qty_open[' . $hitung . ']" id="qty_open[' . $hitung . ']" value="' . $qty_open . '" readonly></td>
                        <td><input type="text" name="qba[' . $hitung . ']" id="qba[' . $hitung . ']" onchange="cek_qba(' . $hitung . ')"><font color="red"> *</font></td>
                        <td><label class="ckbox cksinput"><input type="checkbox" id="cks[' . $hitung . ']"><span class="tx-13"><font color="red">*</font></span></label></td>
                        <input type="hidden" name="line[]" id="line[]" value="' . $line . '">
                        <input type="hidden" name="desc[]" id="desc[]" value="' . $desc1 . '">
                        <input type="hidden" name="satuan[]" id="satuan[]" value="' . $satuan . '">
                        <input type="hidden" name="jml_data" id="jml_data" value="' . $itungdata . '">
                    </tr>';
                $hitung = $hitung + 1;
            }
        } else {
            $tablenya .= '';
        }
        $data['table'] = $tablenya;
        echo json_encode($data);
    }

    public function report()
    {

        $supp   = Auth::guard('pub_login')->user()->kode_supplier;
        $bast_master = DB::select("SELECT no_nbr_acr,kode_sup, no_po,nama_pr,app_sup, app_she, app_user, app_staff_prc,app_mgr_user,
        app_div_user, app_sh_prc, app_mgr_prc, status_sup, status_she, status_user, status_staff_prc,
        status_mgr_user, status_div_user, status_sh_prc,status_mgr_prc, p_nama1, nama_she2,note_she2,
        status,	p_nama,app_she2, status_she2,
        ISNULL(DATEDIFF(DAY, app_sup,GETDATE()),0) day_she2,
        ISNULL(DATEDIFF(DAY, app_she2,app_sup),0) day_she,
        ISNULL(DATEDIFF(DAY, app_she,app_she2),0) day_user,
        ISNULL(DATEDIFF(DAY, app_user,app_she),0) day_mgr_user,
        ISNULL(DATEDIFF(DAY, app_mgr_user,app_user),0) day_div_user,
        ISNULL(DATEDIFF(DAY, app_div_user,app_mgr_user),0) day_prc,
        ISNULL(DATEDIFF(DAY, app_sh_prc,app_div_user),0) day_mgr_prc
        FROM bast_mstr2 WHERE kode_sup='$supp' --AND MONTH(app_sup) > 9
        ORDER BY no_nbr_acr DESC");

        $sqlout = DB::select("SELECT SUM(CASE WHEN app_she IS NULL THEN 1 ELSE 0 END) as 'she'
        ,  SUM(CASE WHEN app_she2 IS NULL THEN 1 ELSE 0 END) as 'she2'
        ,  SUM(CASE WHEN app_user IS NULL THEN 1 ELSE 0 END) as 'user'
        ,  SUM(CASE WHEN app_mgr_user IS NULL THEN 1 ELSE 0 END) as 'mgr_user'
        ,  SUM(CASE WHEN app_div_user IS NULL THEN 1 ELSE 0 END) as 'div_user'
        ,  SUM(CASE WHEN app_sh_prc IS NULL THEN 1 ELSE 0 END) as 'sh_prc'
        ,  SUM(CASE WHEN app_mgr_prc IS NULL THEN 1 ELSE 0 END) as 'mgr_prc'
        ,  SUM(CASE WHEN status='CLOSE' THEN 1 ELSE 0 END)
        as 'finish' FROM bast_mstr2 WHERE kode_sup='$supp'");

        return view('bast.report', compact('bast_master', 'sqlout'));
    }

    public function list()
    {
        $supp   = Auth::guard('pub_login')->user()->kode_supplier;
        $bast_master = DB::select("SELECT no_nbr_acr,kode_sup, no_po,nama_pr,app_sup, app_she, app_user, app_staff_prc,app_mgr_user,
        app_div_user, app_sh_prc, app_mgr_prc, status_sup, status_she, status_user, status_staff_prc,
        status_mgr_user, status_div_user, status_sh_prc,status_mgr_prc, p_nama1, nama_she2,note_she2,
        status,	p_nama,app_she2, status_she2,
        ISNULL(DATEDIFF(DAY, app_sup,GETDATE()),0) day_she2,
        ISNULL(DATEDIFF(DAY, app_she2,app_sup),0) day_she,
        ISNULL(DATEDIFF(DAY, app_she,app_she2),0) day_user,
        ISNULL(DATEDIFF(DAY, app_user,app_she),0) day_mgr_user,
        ISNULL(DATEDIFF(DAY, app_mgr_user,app_user),0) day_div_user,
        ISNULL(DATEDIFF(DAY, app_div_user,app_mgr_user),0) day_prc,
        ISNULL(DATEDIFF(DAY, app_sh_prc,app_div_user),0) day_mgr_prc
        FROM bast_mstr2 WHERE kode_sup='$supp' --AND MONTH(app_sup) > 9
        ORDER BY no_nbr_acr DESC");

        $sqlout = DB::select("SELECT SUM(CASE WHEN app_she IS NULL THEN 1 ELSE 0 END) as 'she'
        ,  SUM(CASE WHEN app_she2 IS NULL THEN 1 ELSE 0 END) as 'she2'
        ,  SUM(CASE WHEN app_user IS NULL THEN 1 ELSE 0 END) as 'user'
        ,  SUM(CASE WHEN app_mgr_user IS NULL THEN 1 ELSE 0 END) as 'mgr_user'
        ,  SUM(CASE WHEN app_div_user IS NULL THEN 1 ELSE 0 END) as 'div_user'
        ,  SUM(CASE WHEN app_sh_prc IS NULL THEN 1 ELSE 0 END) as 'sh_prc'
        ,  SUM(CASE WHEN app_mgr_prc IS NULL THEN 1 ELSE 0 END) as 'mgr_prc'
        ,  SUM(CASE WHEN status='CLOSE' THEN 1 ELSE 0 END)
        as 'finish' FROM bast_mstr2 WHERE kode_sup='$supp'");

        return view('bast.list', compact('bast_master', 'sqlout'));
    }
    public function print($id, $lpb)
    {
        $ba          = base64_decode($id);
        $no_lpb      = base64_decode($lpb);
        $bast_master = DB::table('bast_mstr2')->where('no_nbr_acr', $ba)->first();

        // ob_end_clean();
        $filename = "BAST_" . $ba . ".pdf";
        $tgl      = date('dMY');
        $content  = ob_get_clean();
        $content  = '<page style="font-family: freeserif">' . $content . '</page>';
        try {
            $html2pdf = new Html2Pdf('P', 'A4', 'en', false, 'ISO-8859-15', array(12, 15, 0, 0));
            // $html2pdf->SetMargins(1,1,1);
            $html2pdf->pdf->SetTitle($filename . ' - Akebono Supplier');
            $html2pdf->setDefaultFont('Arial', 5);
            //$html2pdf->Footer('This Document printed by '.$_SESSION['username'].' by supplier.akebono-astra.co.id at '.$tgl.'');
            $html2pdf->writeHTML(view('bast.print', compact('ba', 'no_lpb', 'bast_master')));
            $html2pdf->Output($filename);
        } catch (HTML2PDF_exception $e) {
            echo $e;
        }
    }

    public function delete(Request $request)
    {
        $nobast = base64_decode($request->get('nobast'));
        $mstr   = DB::table('bast_mstr2')->where('no_nbr_acr', $nobast);
        $mstr->delete();

        $det   = DB::table('bast_det2')->where('no_nbr_acr', $nobast);
        $det->delete();

        $ambil_pekerjaan = DB::table('checklist_pekerjaan')->where('no_bast', $nobast)->first();
        $idpk = $ambil_pekerjaan->id;

        $pekerjaan   = DB::table('checklist_pekerjaan')->where('no_bast', $nobast);
        $pekerjaan->delete();

        $detail   = DB::table('detail_pekerjaan')->where('id_pekerjaan', $idpk);
        $detail->delete();

        $result = "Delete Success";
        echo json_encode($result);
    }

    public function lampiran($id, $lpb)
    {
        $ba          = base64_decode($id);
        $no_lpb      = base64_decode($lpb);
        $bast_master = DB::table('bast_mstr2')->where('no_nbr_acr', $ba)->first();
        $checklist   = DB::table('checklist_pekerjaan')
            ->where('no_bast', $ba)
            ->orderBy('create_time', 'DESC')
            ->get();
        $isi = '';
        $isi .= '<table border="1" style="width:100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="330" style align="center" bgcolor="#3498db" style="color:white;">Jenis Pekerjaan</td>
						<td width="330" style align="center" bgcolor="#3498db" style="color:white;">Area</td>
					</tr>';
        $no = 1;
        foreach ($checklist as $ck) {
            $idpkj = $ck->id;
            $isi .= '<tr>
								<td width="330" style align="center" bgcolor="#FFFFFF" style="color:black;padding:5px;">' . $ck->jenis_pekerjaan . '</td>
								<td width="330" style align="center" bgcolor="#FFFFFF" style="color:black;padding:5px;">' . $ck->area . '</td>
							</tr>';
            $no++;
        }
        $isi .=    '</table>
			<br>
			<div style="text-align:center;!important;font-size:14px">Foto sebelum dan sesudah pekerjaan</div>
            <br>

			<table  border="1" style="width:100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="20" style align="center" bgcolor="#3498db" style="color:white;">No</td>
                    <td width="335" style align="center" bgcolor="#3498db" style="color:white;" >Foto Sebelum</td>
                    <td width="335" style align="center" bgcolor="#3498db" style="color:white;" >Foto Sesudah</td>
                </tr>';

        $detail_pekerjaan   = DB::table("detail_pekerjaan")
            ->where("id_pekerjaan", $idpkj)
            ->orderBy("id", "DESC")
            ->get();

        $n = 1;
        foreach ($detail_pekerjaan as $r) {
            // dd(asset("images/bast/sesudah/".str_replace(" ","",$r->foto2)));
            // // die;
            // dd($r->foto1);
            // die;
            $isi .= '<tr>

                        <td style="text-align:center;padding:5px;">' . $n . '</td>
                        <td style="text-align:center;padding:5px;">';

            if (substr($r->foto1, -4) == ".pdf") {
                // dd("pdf");
                $isi .= '<b>Foto Pekerjaan Ada dalam bentuk file PDF <br>dicetak terpisah</b>';
            } else {
                // dd("foto");
                $tampung_foto1 = public_path('images/bast/' . str_replace(' ', '', $r->foto1));
                // dd($tampung_foto1);
                $isi .= '<img src="' . $tampung_foto1 . '" style="filter: brightness(120%);width:300px;"  />
                    <br><i>' . $r->status . '</i><br><br><b>Lokasi : </b><br>' . $r->lokasi . '<br><br><b>Deskripsi : </b><br>' . wordwrap($r->uraian, 46, "<br>\n");
            }
            $isi .= '</td>
                <td style="text-align:center;padding:5px;">';

            if (substr($r->foto2, -4) == ".pdf") {
                $isi .= '<b>Foto Pekerjaan Ada dalam bentuk file PDF <br>dicetak terpisah</b>';
            } else {
                $tampung_foto2 = public_path('images/bast/sesudah/' . str_replace(' ', '', $r->foto2));
                $isi .= '<img src="' . $tampung_foto2 . '" style="filter: brightness(120%);width:300px;"  />
                        <br><i>' . $r->status2 . '</i><br><br><b>Keterangan : </b><br>' . wordwrap($r->ket, 46, "<br>\n");
            }
            $isi .= '</td>';
            $isi .= '</tr>';

            $n++;
        }
        $isi .= '</table>';

        // ob_end_clean();
        $filename = "BASTLampiran_" . $ba . ".pdf";
        $tgl      = date('dMY');
        $content  = ob_get_clean();
        $content  = '<page style="font-family: freeserif">' . $content . '</page>';
        try {
            $html2pdf = new Html2Pdf('P', 'A4', 'en', false, 'ISO-8859-15', array(12, 15, 0, 0));
            // $html2pdf->SetMargins(1,1,1);
            $html2pdf->pdf->SetTitle($filename . ' - Akebono Supplier');
            $html2pdf->setDefaultFont('Arial', 5);
            //$html2pdf->Footer('This Document printed by '.$_SESSION['username'].' by supplier.akebono-astra.co.id at '.$tgl.'');
            $html2pdf->writeHTML(view('bast.lampiran', compact('ba', 'no_lpb', 'bast_master', 'checklist', 'isi')));
            $html2pdf->Output($filename);
        } catch (HTML2PDF_exception $e) {
            echo $e;
        }
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $supp   = Auth::guard('pub_login')->user()->kode_supplier;
        $uname   = Auth::guard('pub_login')->user()->username;

        $validator = Validator::make($request->all(), [
            'foto_sebelum'      => 'required|array',
            'foto_sesudah'      => 'required|array',
            'foto_sebelum.*'    => 'required|image|mimes:jpeg,png,jpg|max:4056',
            'foto_sesudah.*'    => 'required|image|mimes:jpeg,png,jpg|max:4056',
            'jsa_img'           => 'required|image|mimes:jpeg,png,jpg|max:4056',
            'service_img'       => 'required|image|mimes:jpeg,png,jpg|max:4056',
        ]);
        if ($validator->passes()) {
            //master BAST
            $no_ba          = $request->no_ba;
            $no_po          = $request->no_po;
            $status         = $request->status;
            $no_ref         = $request->no_ref;
            $periode        = $request->periode;
            $to             = $request->to;
            $garansi        = $request->garansi;
            //pihak ke 1 dan 2
            $nama1          = $request->nama1;
            $nama2          = $request->nama2;
            $perusahaan1    = $request->perusahaan1;
            $perusahaan2    = $request->perusahaan2;
            $alamat1        = $request->alamat1;
            $alamat2        = $request->alamat2;
            $jabatan1       = $request->jabatan1;
            $jabatan2       = $request->jabatan2;
            $pihak1         = $request->pihak1;
            $pihak2         = $request->pihak2;
            //Additional DATA
            $note           = $request->note;
            $jenis          = $request->jenis;
            $area           = $request->area;
            //DATA Produk
            $line           = $request->line;
            $desc           = $request->desc;
            $satuan         = $request->satuan;
            $qty_open       = $request->qty_open;
            $qba            = $request->qba;
            $cks            = $request->cks;
            $jml_data       = $request->jml_data;

            //DATA lokasi pekerjaan
            $lokasi          = $request->lokasi;
            $uraian          = $request->uraian;
            $status1         = $request->status1;
            $keterangan      = $request->keterangan;
            $status2         = $request->status2;
            //service yang dilakukan
            $service_ket    = $request->service_ket;
            //job service
            $jsa_ket        = $request->jsa_ket;
            $no_pp          = $request->no_pp;

            $bas			= str_replace("/","",$no_ba);

            $cek_dn = DB::table('bast_mstr2')->where('no_nbr_acr',$no_ba)->get();

            if(count($cek_dn) > 0){
                $result = 0;
            }else{
                DB::table('bast_mstr2')->insert([
                    'no_nbr_acr'    => $no_ba,
                    'no_po'         => $no_po,
                    'ref_penawaran' => $no_ref,
                    'nama_pr'       => $perusahaan1,
                    'alamat'        => $alamat1,
                    'p_nama1'       => $nama2,
                    'p_nama'        => $nama1,

                    'jabatan1'          => $jabatan2,
                    'garansi_pekerjaan' => $garansi,
                    'period_kerja'      => $periode,
                    'period_kerja1'     => $to,
                    'jabatan'           => $jabatan1,
                    'copy_po'           => 'OK',
                    'check_list'        => 'OK',

                    'check_list1' => $supp,
                    'check_list2' => $supp,
                    'drawing'     => $supp,
                    'risk'        => $supp,
                    'kesimpulan'  => $supp,
                    'manual_book' => $supp,
                    'note'        => $note,

                    'training'      => $supp,
                    'effdate'       => date('Y-m-d'),
                    'no_pp'         => $no_pp,
                    'kode_sup'      => $supp,
                    'pjb'           => $supp,
                    'rev'           => 0,
                    'status_bast'   => $status,
                ]);

                for ($i = 0; $i < $jml_data; $i++) {
                    if ($qba[$i] != "") {
                        DB::table('bast_det2')->insert([
                            'no_nbr_acr'    => $no_ba,
                            'line'          => $line[$i],
                            'deskripsi'     => $desc[$i],
                            'satuan'        => $satuan[$i],
                            'quantity_ba'   => $qba[$i],
                            'quantity_po'   => $qty_open[$i],
                            'creadate'      => date('Y-m-d H:i:s'),
                            'creaby'        => $supp
                        ]);
                    }
                }

                $doc3origin	= "Service_".$bas."_".str_replace(' ','',basename($request->service_img->getClientOriginalName()));
                $doc3 = str_replace(" ","_",$doc3origin);
                $request->service_img->move(public_path('images/bast/service'), $doc3);

                $doc2origin   = "JSA_".$bas."_".str_replace(' ','',basename($request->jsa_img->getClientOriginalName()));
                $doc2 = str_replace(" ","_",$doc2origin);
                $request->jsa_img->move(public_path('images/bast/jsa'), $doc2);

                foreach($request->file('foto_sebelum') as $fotseb){
                    $name_fsb1 = $fotseb->getClientOriginalName();
                    $name_fsb = str_replace(" ","_",$name_fsb1);
                    $fotseb->move(public_path('images/bast'), $name_fsb);
                    $foto_sebelum[] = $name_fsb;

                }

                foreach($request->file('foto_sesudah') as $fotses){
                    $name_fss1 = $fotses->getClientOriginalName();
                    $name_fss = str_replace(" ","_",$name_fss1);
                    $fotses->move(public_path('images/bast/sesudah'), $name_fss);
                    $foto_sesudah[] = $name_fss;
                }

                $cari_supp = DB::table('SOAP_Pub_business_relation')->where('ct_vd_addr', $supp)->first();

                $id_checklist = DB::table('checklist_pekerjaan')->insertGetId([
                    'no_bast'           => $no_ba,
                    'supplier_name'     => $cari_supp->ct_ad_name,
                    'supp_kode'         => $supp,
                    'periode_start'     => $periode,
                    'periode_end'       => $to,
                    'jenis_pekerjaan'   => $jenis,
                    'area'              => $area,
                    'doc2'              => $doc2,
                    'doc3'              => $doc3,
                    'doc2_ket'          => $service_ket,
                    'doc3_ket'          => $jsa_ket,
                    'user_aaij'         => $nama2,
                    'create_time'       => date('Y-m-d H:i:s'),
                ]);


                $hitung_data = count($foto_sebelum);

                for ($i = 0; $i < $hitung_data; $i++) {
                    DB::table('detail_pekerjaan')->insert([
                        'id_pekerjaan'  => $id_checklist,
                        'no'            => $i + 1,
                        'uraian'        => $uraian[$i],
                        'status'        => $status1[$i],
                        'lokasi'        => $lokasi[$i],
                        'ket'           => $keterangan[$i],
                        'status2'       => $status2[$i],
                        'foto1'         => $foto_sebelum[$i],
                        'foto2'         => $foto_sesudah[$i],
                        'create_time'   => date('Y-m-d H:i:s'),
                        'create_by'     => $uname,
                    ]);
                }
                $email = "nico.sanmediatek@gmail.com";
                $data_email = [
                    'no_bast'   => $no_ba,
                    'no_po'     => $no_po,
                    'nama'      => $nama1,
                    'name'      => $nama1,
                    'from'      => 'BAST PT. AKEBONO BRAKE ASTRA INDONESIA <helpdesk@akebono-astra.co.id>',
                    'subject'   => 'BAST APPROVAL ' . date('Y') . ' - ' . $perusahaan1,
                    'nama_supp' => $perusahaan1,
                    'url'       => url('/bast-approval/' . base64_encode($no_ba) . '/' . base64_encode('supplier')),
                    'step'      => 'PROSES',
                ];
                Mail::to($email)->send(new sendEmailBast($data_email));
                $result = 1;
            }
            return response()->json([
                'success'   => 'Image Upload Successfully',
                'result'    => $result
            ]);
        }
        return response()->json(['error'=>$validator->errors()->all()]);


    }

    public function preview_bast(Request $request){
        //master BAST
        $data['no_ba']          = $request->no_ba;
        $data['no_po']          = $request->no_po;
        $data['status']         = $request->status;
        $data['no_ref']         = $request->no_ref;
        $data['periode']        = $request->periode;
        $data['to']             = $request->to;
        $data['garansi']        = $request->garansi;
        //pihak ke 1 dan 2
        $data['nama1']          = $request->nama1;
        $data['nama2']          = $request->nama2;
        $data['perusahaan1']    = $request->perusahaan1;
        $data['perusahaan2']    = $request->perusahaan2;
        $data['alamat1']        = $request->alamat1;
        $data['alamat2']        = $request->alamat2;
        $data['jabatan1']       = $request->jabatan1;
        $data['jabatan2']       = $request->jabatan2;
        $data['pihak1']         = $request->pihak1;
        $data['pihak2']         = $request->pihak2;
        //Additional DATA
        $data['note']           = $request->note;
        $data['jenis']          = $request->jenis;
        $data['area']           = $request->area;

        //DATA Produk
        $line                   = $request->line;
        $desc                   = $request->desc;
        $satuan                 = $request->satuan;
        $qty_open               = $request->qty_open;
        $qba                    = $request->qba;
        $cks                    = $request->cks;
        $jml_data               = $request->jml_data;
        $z                      = 0;

        for ($i=0; $i < $jml_data; $i++) {
            if($qba[$i] != ""){
                $isi_po[$z++] = array(
                    'line' => $line[$i],
                    'desc' => $desc[$i],
                    'satuan' => $satuan[$i],
                    'qty_open' => $qty_open[$i],
                    'qba' => $qba[$i],
                ) ;
            }
        }

        return view('bast.preview_page',compact('data','isi_po'));
    }

    public function bast_approval($nobast,$step){
    //    echo base64_decode('c2hlX21ncg==');s
        $no         = base64_decode($nobast);
        $get_bast   = DB::table('bast_mstr2')->where('no_nbr_acr', $no)->first();
        $bast_det   = DB::table('bast_det2')->where('no_nbr_acr', $no)->get();
        $checklist  = DB::table('checklist_pekerjaan')->where('no_bast', $no)->get();
        $step       = base64_decode($step);
        return view('bast.approval_page', compact('get_bast', 'step', 'bast_det', 'checklist'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function approval(Request $request)
    {
        $nobast    = base64_decode($request->get('id'));
        $step      = base64_decode($request->get('step'));
        $note      = $request->get('shenote');
        $data_bast = DB::table('bast_mstr2')->where('no_nbr_acr', $nobast)->first();
        if ($step == "supplier") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_she2'   => 'OK',
                    'nama_she2'     => 'Ahmad Munir',
                    'app_she2'      => date('Y-m-d H:i:s'),
                    'note_she2'     => $note
                ]);
            $email = 'nico.sanmediatek@gmail.com';
            $data_email = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'Ahmad Munir',
                'name'      => $data_bast->p_nama,
                'from'      => 'Supplier PT. AKEBONO BRAKE ASTRA INDONESIA <helpdesk@akebono-astra.co.id>',
                'subject'   => 'BAST APPROVAL ' . date('Y') . ' - ' . $data_bast->nama_pr,
                'nama_supp' => $data_bast->nama_pr,
                'url'       => url('/bast-approval/' . base64_encode($nobast) . '/' . base64_encode('she_mgr')),
                'step'      => 'PROSES',
            ];
            Mail::to($email)->send(new sendEmailBast($data_email));
        } else if ($step == "she_mgr") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_she'   => 'OK',
                    'nama_she'     => 'Thomas Budiman Lahay',
                    'app_she'      => date('Y-m-d H:i:s'),
                    'note_she'     => $note
                ]);
            $email = 'nico.sanmediatek@gmail.com';
            $data_email = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'Thomas Budiman Lahay',
                'name'      => $data_bast->p_nama,
                'from'      => 'Ahmad Munir PT. AKEBONO BRAKE ASTRA INDONESIA <helpdesk@akebono-astra.co.id>',
                'subject'   => 'BAST APPROVAL ' . date('Y') . ' - ' . $data_bast->nama_pr,
                'nama_supp' => $data_bast->nama_pr,
                'url'       => url('/bast-approval/' . base64_encode($nobast) . '/' . base64_encode('sh_user')),
                'step'      => 'PROSES',
            ];
            Mail::to($email)->send(new sendEmailBast($data_email));
        } else if ($step == "sh_user" || $step == "she") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_user'   => 'OK',
                    'nama_user'     => 'Ade Samsoni Budiman',
                    'app_user'      => date('Y-m-d H:i:s'),
                ]);
            $email = 'nico.sanmediatek@gmail.com';
            $data_email = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'Ade Samsoni Budiman',
                'name'      => $data_bast->p_nama,
                'from'      => 'Thomas Budiman Lahay PT. AKEBONO BRAKE ASTRA INDONESIA <helpdesk@akebono-astra.co.id>',
                'subject'   => 'BAST APPROVAL ' . date('Y') . ' - ' . $data_bast->nama_pr,
                'nama_supp' => $data_bast->nama_pr,
                'url'       => url('/bast-approval/' . base64_encode($nobast) . '/' . base64_encode('mgr_user')),
                'step'      => 'PROSES',
            ];
            Mail::to($email)->send(new sendEmailBast($data_email));
        } else if ($step == "mgr_user") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_mgr_user'   => 'OK',
                    'nama_mgr_user'     => 'Joko S',
                    'app_mgr_user'      => date('Y-m-d H:i:s'),
                ]);
            $email = 'nico.sanmediatek@gmail.com';
            $data_email = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'Joko S',
                'name'      => $data_bast->p_nama,
                'from'      => 'Ade Samsoni Budiman PT. AKEBONO BRAKE ASTRA INDONESIA <helpdesk@akebono-astra.co.id>',
                'subject'   => 'BAST APPROVAL ' . date('Y') . ' - ' . $data_bast->nama_pr,
                'nama_supp' => $data_bast->nama_pr,
                'url'       => url('/bast-approval/' . base64_encode($nobast) . '/' . base64_encode('div_user')),
                'step'      => 'PROSES',
            ];
            Mail::to($email)->send(new sendEmailBast($data_email));
        } else if ($step == "div_user") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_div_user'   => 'OK',
                    'nama_div_user'     => 'Thomas Budiman Lahay',
                    'app_div_user'      => date('Y-m-d H:i:s'),
                ]);
            $email = 'nico.sanmediatek@gmail.com';
            $data_email = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'James RP Marbun',
                'name'      => $data_bast->p_nama,
                'from'      => 'Joko S PT. AKEBONO BRAKE ASTRA INDONESIA <helpdesk@akebono-astra.co.id>',
                'subject'   => 'BAST APPROVAL ' . date('Y') . ' - ' . $data_bast->nama_pr,
                'nama_supp' => $data_bast->nama_pr,
                'url'       => url('/bast-approval/' . base64_encode($nobast) . '/' . base64_encode('sh_prc')),
                'step'      => 'PROSES',
            ];
            Mail::to($email)->send(new sendEmailBast($data_email));
        } else if ($step == "sh_prc") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_sh_prc'   => 'OK',
                    'nama_sh_prc'     => 'Hartono',
                    'app_sh_prc'      => date('Y-m-d H:i:s'),
                ]);
            $email = 'nico.sanmediatek@gmail.com';
            $data_email = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'Hartono',
                'name'      => $data_bast->p_nama,
                'from'      => 'DIV USER PT. AKEBONO BRAKE ASTRA INDONESIA <helpdesk@akebono-astra.co.id>',
                'subject'   => 'BAST APPROVAL ' . date('Y') . ' - ' . $data_bast->nama_pr,
                'nama_supp' => $data_bast->nama_pr,
                'url'       => url('/bast-approval/' . base64_encode($nobast) . '/' . base64_encode('mgr_prc')),
                'step'      => 'PROSES',
            ];
            Mail::to($email)->send(new sendEmailBast($data_email));
        } else if ($step == "mgr_prc") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_mgr_prc'   => 'OK',
                    'nama_mgr_prc'     => 'MGR PRC',
                    'app_mgr_prc'      => date('Y-m-d H:i:s'),
                ]);
            $email = 'nico.sanmediatek@gmail.com';
            $data_email = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'ADMIN',
                'nama_from' => 'Hartono',
                'name'      => $data_bast->p_nama,
                'from'      => 'MGR PRC PT. AKEBONO BRAKE ASTRA INDONESIA <helpdesk@akebono-astra.co.id>',
                'subject'   => 'BAST APPROVAL FINISH - ' . date('Y') . ' - ' . $data_bast->nama_pr,
                'nama_supp' => $data_bast->nama_pr,
                'url'       => url('/bast-approval/' . base64_encode($nobast) . '/' . base64_encode('FINISH')),
                'print_url' => url('/print-completed-bast/' . base64_encode($nobast) . '/' . base64_encode('123')),
                'step'      => 'FINISH',
            ];
            Mail::to($email)->send(new sendEmailBast($data_email));

            $supplier_email = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'Hartono',
                'name'      => $data_bast->p_nama,
                'from'      => 'MGR PRC PT. AKEBONO BRAKE ASTRA INDONESIA <helpdesk@akebono-astra.co.id>',
                'subject'   => 'BAST APPROVAL FINISH - ' . date('Y') . ' - ' . $data_bast->nama_pr,
                'nama_supp' => $data_bast->nama_pr,
                'url'       => url('/bast-approval/' . base64_encode($nobast) . '/' . base64_encode('mgr_prc')),
                'step'      => 'PROSES',
            ];
            Mail::to($email)->send(new sendEmailBast($supplier_email));
        }
        echo json_encode($step);
    }

    public function reject(Request $request)
    {
        $nobast    = base64_decode($request->get('id'));
        $step      = base64_decode($request->get('step'));
        $note      = $request->get('reject_note');
        $data_bast = DB::table('bast_mstr2')->where('no_nbr_acr', $nobast)->first();

        $emailto_supp = 'nico.sanmediatek@gmail.com';
        $send_emaill_to_supp = [
            'no_bast'   => $nobast,
            'no_po'     => $data_bast->no_po,
            'nama'      => 'SHE MGR',
            'dear'      => $data_bast->p_nama,
            'reason'    => $note,
            'nama_supp' => $data_bast->nama_pr,
            'url'       => url('/bast-approval/' . base64_encode($nobast) . '/' . base64_encode('she_mgr')),
            'step'      => 'REJECT_TO_SUPP',
        ];
        Mail::to($emailto_supp)->send(new sendEmailBast($send_emaill_to_supp));


        if ($step == "supplier") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_she2'   => 'NOK',
                    'nama_she2'     => 'Ahmad Munir',
                    'app_she2'      => date('Y-m-d H:i:s'),
                    'note_she2'     => $note
                ]);

            $get_email = DB::table('portal_login')->select('login_email', 'login_name')
                ->where('login_username', 'like', '%munir%')->first();

            $email = $get_email->login_email;
            $data_email = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email)->send(new sendEmailBast($data_email));

            $get_email2 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email2 = $get_email2->login_email;
            $data_email2 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email2->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email2)->send(new sendEmailBast($data_email2));

            $get_email3 = DB::table('portal_login')->select('login_email', 'login_name')
                ->where('login_username', 'like', '%munir%')->first();

            $email3 = $get_email3->login_email;
            if ($email3 == "bambang.n@akebono-astra.co.id") {
                $to            = "joko.s@akebono-astra.co.id";
                $nama3        = "Joko S";
            } else {
                $to            = $get_email3->login_email;
                $nama3        = $get_email3->login_name;
            }

            $data_email3 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $nama3,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($to)->send(new sendEmailBast($data_email3));

            $get_email4 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email4 = $get_email4->login_email;
            $data_email4 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email4->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email4)->send(new sendEmailBast($data_email4));
        } else if ($step == "she_mgr") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_she'   => 'NOK',
                    'nama_she'     => 'Thomas Budiman Lahay',
                    'app_she'      => date('Y-m-d H:i:s'),
                    'note_she'     => $note
                ]);

            $get_email2 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email2 = $get_email2->login_email;
            $data_email2 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email2->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email2)->send(new sendEmailBast($data_email2));

            $get_email3 = DB::table('portal_login')->select('login_email', 'login_name')
                ->where('login_username', 'like', '%munir%')->first();

            $email3 = $get_email3->login_email;
            if ($email3 == "bambang.n@akebono-astra.co.id") {
                $to            = "joko.s@akebono-astra.co.id";
                $nama3        = "Joko S";
            } else {
                $to            = $get_email3->login_email;
                $nama3        = $get_email3->login_name;
            }

            $data_email3 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $nama3,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($to)->send(new sendEmailBast($data_email3));

            $get_email4 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email4 = $get_email4->login_email;
            $data_email4 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email4->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email4)->send(new sendEmailBast($data_email4));
        } else if ($step == "sh_user" || $step == "she") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_user'   => 'NOK',
                    'nama_user'     => 'Ade Samsoni Budiman',
                    'app_user'      => date('Y-m-d H:i:s'),
                ]);

            $get_email2 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email2 = $get_email2->login_email;
            $data_email2 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email2->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email2)->send(new sendEmailBast($data_email2));

            $get_email3 = DB::table('portal_login')->select('login_email', 'login_name')
                ->where('login_username', 'like', '%munir%')->first();

            $email3 = $get_email3->login_email;
            if ($email3 == "bambang.n@akebono-astra.co.id") {
                $to            = "joko.s@akebono-astra.co.id";
                $nama3        = "Joko S";
            } else {
                $to            = $get_email3->login_email;
                $nama3        = $get_email3->login_name;
            }

            $data_email3 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $nama3,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($to)->send(new sendEmailBast($data_email3));

            $get_email4 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email4 = $get_email4->login_email;
            $data_email4 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email4->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email4)->send(new sendEmailBast($data_email4));
        } else if ($step == "mgr_user") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_mgr_user'   => 'NOK',
                    'nama_mgr_user'     => 'Joko S',
                    'app_mgr_user'      => date('Y-m-d H:i:s'),
                ]);

            $get_email2 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email2 = $get_email2->login_email;
            $data_email2 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email2->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email2)->send(new sendEmailBast($data_email2));

            $get_email3 = DB::table('portal_login')->select('login_email', 'login_name')
                ->where('login_username', 'like', '%munir%')->first();

            $email3 = $get_email3->login_email;
            if ($email3 == "bambang.n@akebono-astra.co.id") {
                $to            = "joko.s@akebono-astra.co.id";
                $nama3        = "Joko S";
            } else {
                $to            = $get_email3->login_email;
                $nama3        = $get_email3->login_name;
            }

            $data_email3 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $nama3,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($to)->send(new sendEmailBast($data_email3));

            $get_email4 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email4 = $get_email4->login_email;
            $data_email4 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email4->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email4)->send(new sendEmailBast($data_email4));
        } else if ($step == "div_user") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_div_user'   => 'NOK',
                    'nama_div_user'     => 'Thomas Budiman Lahay',
                    'app_div_user'      => date('Y-m-d H:i:s'),
                ]);

            $get_email2 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email2 = $get_email2->login_email;
            $data_email2 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email2->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email2)->send(new sendEmailBast($data_email2));

            $get_email3 = DB::table('portal_login')->select('login_email', 'login_name')
                ->where('login_username', 'like', '%munir%')->first();

            $email3 = $get_email3->login_email;
            if ($email3 == "bambang.n@akebono-astra.co.id") {
                $to            = "joko.s@akebono-astra.co.id";
                $nama3        = "Joko S";
            } else {
                $to            = $get_email3->login_email;
                $nama3        = $get_email3->login_name;
            }

            $data_email3 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $nama3,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($to)->send(new sendEmailBast($data_email3));
        } else if ($step == "sh_prc") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_sh_prc'   => 'NOK',
                    'nama_sh_prc'     => 'Hartono',
                    'app_sh_prc'      => date('Y-m-d H:i:s'),
                ]);

            $get_email2 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email2 = $get_email2->login_email;
            $data_email2 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email2->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email2)->send(new sendEmailBast($data_email2));

            $get_email3 = DB::table('portal_login')->select('login_email', 'login_name')
                ->where('login_username', 'like', '%munir%')->first();

            $email3 = $get_email3->login_email;
            if ($email3 == "bambang.n@akebono-astra.co.id") {
                $to            = "joko.s@akebono-astra.co.id";
                $nama3        = "Joko S";
            } else {
                $to            = $get_email3->login_email;
                $nama3        = $get_email3->login_name;
            }

            $data_email3 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $nama3,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($to)->send(new sendEmailBast($data_email3));

            $get_email4 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email4 = $get_email4->login_email;
            $data_email4 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email4->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email4)->send(new sendEmailBast($data_email4));
        } else if ($step == "mgr_prc") {
            $query1 = DB::table('bast_mstr2')
                ->where('no_nbr_acr', $nobast)
                ->update([
                    'status_mgr_prc'   => 'NOK',
                    'nama_mgr_prc'     => 'MGR PRC',
                    'app_mgr_prc'      => date('Y-m-d H:i:s'),
                ]);
            $get_email2 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email2 = $get_email2->login_email;
            $data_email2 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email2->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email2)->send(new sendEmailBast($data_email2));

            $get_email3 = DB::table('portal_login')->select('login_email', 'login_name')
                ->where('login_username', 'like', '%munir%')->first();

            $email3 = $get_email3->login_email;
            if ($email3 == "bambang.n@akebono-astra.co.id") {
                $to            = "joko.s@akebono-astra.co.id";
                $nama3        = "Joko S";
            } else {
                $to            = $get_email3->login_email;
                $nama3        = $get_email3->login_name;
            }

            $data_email3 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $nama3,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($to)->send(new sendEmailBast($data_email3));

            $get_email4 = DB::table('portal_login')->select('login_email', 'login_name')
                ->join('PP_Trans_Mstr', 'login_username', 'ppm_app0')
                ->where('ppm_no_pp', $data_bast->no_pp)->first();

            $email4 = $get_email4->login_email;
            $data_email4 = [
                'no_bast'   => $nobast,
                'no_po'     => $data_bast->no_po,
                'nama'      => 'SHE MGR',
                'name'      => $data_bast->p_nama,
                'dear'      => $get_email4->login_name,
                'reason'    => $note,
                'nama_supp' => $data_bast->nama_pr,
                'step'      => 'REJECT',
            ];
            Mail::to($email4)->send(new sendEmailBast($data_email4));
        }
        echo json_encode($step);
    }

    public function destroy($id)
    {
        //
    }
}
