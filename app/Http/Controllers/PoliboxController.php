<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class PoliboxController extends Controller
{
    public function index(Request $request)
    {
		// $data =DB::select("select polibox_item, polibox_deskripsi from polibox_mstr");
		// $data = DB::table('whs_counter_mstr')
        // ->join('polibox_mstr', 'polibox_mstr.polibox_item', 'whs_counter_mstr.part_num')
        // ->where('rak_num', '<>','')
        // ->select('whs_counter_mstr.*', 'polibox_mstr.polibox_item', 'polibox_mstr.polibox_deskripsi')->get();
        // dd($data);
        $data = DB::table('whs_counter_mstr')
        ->where('rak_num', '<>','')
        ->orderBy('rak_num','asc')->get();
        // if ($request->ajax()) {
        //     // $data = DB::select("select * from whs_counter_mstr where rak_num <>'' order by rak_num asc");
        //     $data = DB::table('whs_counter_mstr')
        //     ->where('rak_num', '<>','')
        //     ->orderBy('rak_num','asc')->limit(10)->get();
        //     return DataTables::of($data)
        //     ->addColumn('action', function ($data) {
        //         $desc = DB::table('polibox_mstr')->where('polibox_item',$data->part_num)->value('polibox_deskripsi');
        //         // $desc = DB::select('select polibox_deskripsi from polibox_mstr where polibox_item = '.$data->part_num.'');
        //         return '
        //             <div color="white" id="desc' . $data->id . '">
        //             <input type="text" name="desc'.$data->id.'" id="desc'.$data->id.'" readonly size="50"  value="'.$desc.'">
        //             </div>
        //         ';
        //     })
        //     ->addColumn('part_nume', function ($data) {
        //         return '
        //             <div color="white" id="part_num' . $data->id . '">
        //             <input type="text" name="part_num'.$data->id.'" id="part_num'.$data->id.'"  value="'.$data->part_num.'">
        //             <a  name="edit"  class="edit btn btn-info btn-sm"><i class="fas fa-search-plus"></i></a>
        //             </div>
        //         ';
        //     })
        //     ->addColumn('loctoe', function ($data) {
        //         return '
        //             <div color="white" id="locto' . $data->id . '">
        //             <input type="text" name="locto'.$data->id.'" id="locto'.$data->id.'" readonly value="'.$data->locto.'">
        //             </div>
        //         ';
        //     })
        //     ->rawColumns(['action','part_nume','loctoe'])
        //     ->make(true);
        // }
        return view('polibox.index',compact('data'));
        // return view('polibox.index');
    }
    public function get_polibox(Request $request)
    {
        if ($request->ajax()) {
            $get_part = "select distinct polibox_item, polibox_deskripsi, polibox_loc_to FROM polibox_mstr where polibox_item<>''";
            $data = DB::select($get_part);
            return DataTables::of($data)
            ->addColumn('actione', function ($data) {
                return '
                    <td color="white">
                    <a name="add" id="added" data-itema="'.$data->polibox_item.'" data-itemb="'.$data->polibox_deskripsi.'" data-itemc="'.$data->polibox_loc_to.'" class="btn btn-warning btn-sm text-white">Click <i class="fas fa-check"></i></a>
                    </td>
                ';
            })
            ->rawColumns(['actione'])
            ->make(true);
        }
    }
    public function post_polibox(Request $request)
    {
        for($i = 1; $i <= count($request->id);$i++)
					{
                        $part = $request->part_num[$i];
                        $locto = $request->locto[$i];
						$id=$request->id[$i];
                        if($part != null) {
                            $affected = DB::table('whs_counter_mstr')
                                            ->where('id', $id)
                                            ->update(['part_num' => $part,'locto' => $locto ]);
                        }
                    }
        alert()->success('success', 'Updated successfully');
        // toast('Updated successfully','success')->position('top-end')->autoClose(3000)->timerProgressBar();
        return redirect()->back();
    }
}
