<?php

namespace App\Http\Controllers;

use App\SOAPPubBusinessRelation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\UrlGenerator;

class LpbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }

    public function index(Request $request)
    {
        // $isi = DB::table('elpb_mstr')->get();
        //     dd($isi);
        $dateTo = $request->dateTo;
        $dateFrom = $request->dateFrom;
        $supp = null;
        // $data = SOAPPubBusinessRelation::all();
        // $cek = list($day,$month,$year) = split ('/',$dateTo);
        // dd($cek);
        // $isi = array();
        $data = DB::table('SOAP_Pub_Business_Relation');
        if ($request->supp != null) {
            $data = $data->where('ct_vd_addr', $request->supp);
            $supp = $request->supp;
        }
        $data = $data->paginate(25);

        // foreach ($data as $key => $d) {
        //     $isi[] .= $d->ct_vd_type;
        // }
        // dd($isi);
        return view('lpb.approval_elpb',compact('data','dateTo','dateFrom','supp'));
    }

    public function getSupplier(Request $request){

        $search = $request->search;

        if($search == ''){
           $supplier = DB::table('SOAP_Pub_Business_Relation')->orderby('ct_ad_name','asc')->select('ct_vd_addr','ct_ad_name')->limit(5)->get();
        }else{
           $supplier = DB::table('SOAP_Pub_Business_Relation')->orderby('ct_ad_name','asc')->select('ct_vd_addr','ct_ad_name')->where('ct_ad_name', 'like', '%' .$search . '%')->limit(5)->get();
        }

        $response = array();
        foreach($supplier as $supp){
           $response[] = array(
                "id"=>$supp->ct_vd_addr,
                "text"=>$supp->ct_ad_name
           );
        }

        echo json_encode($response);
        exit;
     }

    public function table(Request $request)
    {
        $Username = Auth::user()->username;

        if($request->Status == "1"){
            $DateInput          = $request->DateFrom;
            $p1                 = date('Y-m-d',strtotime($DateInput));
            $tampung_supplier   = $request->Supplier;
            $Supplier           = substr($request->Supplier,0,6);
            $Month              = date("m",strtotime($DateInput));
            $Year               = date("Y",strtotime($DateInput));

            $DateInput2 = $request->DateTo;
            $p2 = date('Y-m-d',strtotime($DateInput2));

            if($tampung_supplier == 'All'){
                $qq = DB::select("SELECT DISTINCT(trans_id),* FROM elpb_mstr INNER JOIN mrp_supplier ON substring(supplier_code,0,6) = substring(supp_code,0,6)");
            }else{
                $qq = DB::select("SELECT DISTINCT(trans_id),* FROM elpb_mstr INNER JOIN mrp_supplier ON substring(supplier_code,0,6) = substring(supp_code,0,6) WHERE supp_code = '$tampung_supplier'");
            }
            // dd($Supplier);

            // $q_new = "EXEC USP_GET_ELPB @Username = '$Username', @Date1 = '$p1', @Date2 = '$p2', @Supplier = '$Supplier'";
            // $exec_query  = DB::statement(DB::select($q_new));
            // // dd($exec_query);
            // $cek = DB::connection("sqlsrv")->statement($q_new);
            // dd($cek);

            // foreach($query as $q){
            //     dd($q);
            // }
		//HOURLY

            echo "<table  id='searchTable' style='border-collapse:collapse' width='100%'>";
                echo "<tr>";
                    echo "<td  style='text-align:center;background-color:#0066CC; font-weight:bold; color:white;' >Supplier Name</td>";
                    echo "<td  style='text-align:center;background-color:#0066CC; font-weight:bold; color:white;' >Category</td>";
                    $startTime = strtotime($DateInput);
                    $endTime = strtotime($DateInput2);
                    // Loop between timestamps, 24 hours at a time
                    for ( $i = $startTime; $i <= $endTime; $i = $i + 86400 ){
                        $Day = date( 'd', $i );

                        echo "<td  style='text-align:center;background-color:#0066CC; font-weight:bold; color:white;' >".$Day."</td>";
                    }
                    echo "<td  style='text-align:center;background-color:#0066CC; font-weight:bold; color:white;' >Total</td>";
                echo "</tr>";

                $actual_total=0;
                $suspect_total=0;
                $category = "1";

                foreach($qq as $row){
                	$CatName  = "QC Check";
                	$BackgroundColor = "background-color:#fcf4dc;";
                    //cari data LPB

                	echo "<tr class='trData'>";
                		echo "<td rowspan='4' style='text-align:center; padding:3px 6px;color:#2f3542;background-color:#f0ecec; font-weight:bold;font-size:13px;'>".$row->supp_code."</br>".$row->supp_name."</td>";
                    echo "</tr>";
                        $CatName2 = "Not Approved"; $BackgroundColor2 = "background-color:#fde8e8;";
                        $CatName3 = "Outstanding RC"; $BackgroundColor3 = "background-color:#eeeeee;";
                        $Total = 0;
                		$Total1 = 0;
                		$Total2 = 0;
                		$Total3 = 0;
                		$Background = "";
                        echo "<tr>";
                            echo "<td style='text-align:left; width:120px;padding:3px 6px;color:#2f3542; $BackgroundColor '>".$CatName."</td>";
                        for ( $i = $startTime; $i <= $endTime; $i = $i + 86400 ){
                            $Day_lpb = date('Y-m-d', $i );
                            $qc = DB::table('elpb_mstr')
                                ->select('eff_date','status_qc')
                                ->where('status_qc','Need Check')
                                ->where('supp_code',$row->supp_code)
                                ->where('eff_date',$Day_lpb)
                                ->count();


                            echo "<td  style='text-align:Center;$BackgroundColor' >".$qc."</td>";
                        }
                        echo "<td  style='text-align:Center;$BackgroundColor' >".$Total."</td>";
                        echo '</tr>';

                        echo "<tr>";
                            echo "<td style='text-align:left; width:120px;padding:3px 6px;color:#2f3542; $BackgroundColor2 '>".$CatName2."</td>";
                        for ( $i = $startTime; $i <= $endTime; $i = $i + 86400 ){
                            $Day_lpb = date('Y-m-d', $i );

                            $lpb = DB::table('elpb_mstr')
                            ->select('eff_date','status_approval')
                            ->where('status_approval','Not Approved')
                            ->where('supp_code',$row->supp_code)
                            ->where('eff_date',$Day_lpb)
                            ->count();
                            if($lpb > 0){
                                $url = '/app-lpb/'.base64_encode($row->supp_code).'/'.base64_encode($Day_lpb);
                                echo "<td  style='text-align:Center;background-color:#ff7979; color:black;' ><a href='".$url."' target='_blank'>".$lpb."</a></td>";
                            }else if($lpb == 0){
                                echo "<td  style='text-align:Center;background-color:#fde8e8;' >".$lpb."</td>";
                            }
                        }
                        echo "<td  style='text-align:Center;$BackgroundColor' >".$Total."</td>";
                        echo '</tr>';
                        echo "<tr>";
                            echo "<td style='text-align:left; width:120px;padding:3px 6px;color:#2f3542; $BackgroundColor3 '>".$CatName3."</td>";
                        for ( $i = $startTime; $i <= $endTime; $i = $i + 86400 ){
                            $Day_lpb = date('Y-m-d', $i );

                            $outstanding = DB::table('elpb_mstr')
                            ->select('eff_date','status_approval','section')
                            ->where('status_approval','Approved')
                            ->where('section','LIKE',$Username.'%')
                            ->where('supp_code',$row->supp_code)
                            ->where('eff_date',$Day_lpb)
                            ->count();
                            echo "<td  style='text-align:Center;$BackgroundColor3' >".$outstanding  ."</td>";
                        }
                        echo "<td  style='text-align:Center;$BackgroundColor' >".$Total."</td>";
                        echo '</tr>';

                	echo "</tr>";
                }
            echo "</table>";
	}
    }

    public function app_lpb($supp,$date){
        $supplier = base64_decode($supp);
        $date     = base64_decode($date);


        return view('lpb.app_lpb',compact('supplier','date'));
    }
    public function detail_img_lpb($name){
        $Image =  base64_decode($name);
        return view('lpb.detail_img_lpb',compact('Image'));
    }

    public function pc_app_lpb(Request $request){
        $Status = $request->get('Status');

        if($Status == "1"){
            $LPBCode        = $request->get('LPBCode');
            // $PO             = $request->get('PO');
            // $PackingSlip    = $request->get('PackingSlip');
            // $EffDate        = $request->get('EffDate');
             //UPDATE STATUS PROSES SEDANG BF & PO RECEIPT
            DB::table('elpb_mstr')
              ->where('lpb_code', $LPBCode)
              ->update(['status_approval' => 'Approved']);

            DB::table('elpb_dtl')
              ->where('lpb_code', $LPBCode)
              ->update([
                  'outstanding_status' => 'PROCESS',
                  'outstanding_start'  => date('Y-m-d H:i:s'),
              ]);
        
            //END UPDATE STATUS PROSES SEDANG BF & PO RECEIPT
            $RC         = "";
            $TransID    = "";
            $query      = collect(\DB::select("SELECT TOP 1 trans_id FROM elpb_mstr WHERE lpb_code='".$LPBCode."' AND ISNULL(trans_id,'') != '' "))->first();

            $TransID = $query->trans_id;

            $LocSupplier    = "";
            $FinalStatus    = "Success";
            $ListRC         = "";
            $MessageError   = "";
            $query2         = DB::select("SELECT po, packing_slip, MAX(A.eff_date) eff_date,MAX(A.eff_time) eff_time FROM elpb_mstr A INNER JOIN elpb_dtl B ON A.lpb_code=B.lpb_code WHERE A.lpb_code='".$LPBCode."' AND ISNULL(B.status_rc_qxtend,'') !='Success'  GROUP BY po, packing_slip");
            dd($query2);
            foreach($query2 as $row2){

                $StatusQXtend_RC = "Error";
                $StatusQXtend_BF = "Error";
                $BF_Success = 0;
                $BF_Error   = 0;
            
                $PO             = $row2['po'];
                $PackingSlip    = $row2['packing_slip'];
                $EffDate        = $row2['eff_date'];
                $EffTime        = $row2['eff_time'];

                list($result, $message,$xml_request,$xml_return) = POReceipt($LPBCode,$PO,$PackingSlip,$EffDate);
                
                if($result){
                    $rs_rc  = collect(\DB::select("SELECT TOP 1 H.prh_receiver FROM prh_hist H WHERE prh_domain='AAIJ' AND prh_nbr='".$PO."' AND prh_ps_nbr='".substr($PackingSlip,0,20)."' WITH(NOLOCK)"))->first();  
                    $RC     = $rs_rc->prhReceiver;

                    $ListRC .= $RC .",";
                    $StatusQXtend_RC = "Success";
                    
                    if($TransID != ""){
                        DB::table('portal_mstr')
                        ->where('portal_tr_id', $TransID)
                        ->update([
                            'portal__date01' => $EffDate,
                            'portal__char01' => $EffTime,
                        ]);

                        $queryDetail = DB::select("SELECT * FROM elpb_mstr A INNER JOIN elpb_dtl B ON A.lpb_code=B.lpb_code WHERE A.lpb_code='".$LPBCode."'");
                        
                        foreach($queryDetail as $rowD){
                            /* JIKA QC AKTIF - HARUS CEK ULANG LAGI nilai QTY-nya!! LIHAT PHP MBAK YANI YANG INPUT RC BARCODE & QC BARCODE*/
                            DB::table('portald_det')
                            ->where('portald_tr_id', $TransID)
                            ->where('portald_part',$rowD->item_number)
                            ->where('portald_line',$rowD->line)
                            ->update([
                                'portald__int07'        => $rowD->qty_received,
                                'portald__date02'       => $rowD->eff_date,
                                'portald__char01'       => $rowD->eff_time,
                                'portald_qty_ship'      => $rowD->qty_after_qc,
                                'portald_confirm_rcvd'  => $rowD->qty_after_qc,
                                'portald_confirm_date'  => $EffDate,
                            ]);
                        }		
                    }
                }

                $MessageError = "Detail Error RC : \r" . ErrorFilterRC2($message);
                DB::table('elpb_dtl')
                    ->where('lpb_code', $LPBCode)
                    ->where('po', $PO)
                    ->where('packing_slip', $PackingSlip)
                    ->update([
                        'status_rc_qxtend'  => $StatusQXtend_RC,
                        'rc'                => $RC,
                    ]);

                DB::table('portal_rc_qxtend_history')->insert([
                    'lpb_code'      => $LPBCode,
                    'po'            => $PO,
                    'packing_slip'  => $PackingSlip,
                    'status_qxtend' => $StatusQXtend_RC,
                    'message'       => $message,
                    'xml_request'   => str_replace("'","\"", $xml_request),
                    'xml_return'    => str_replace("'","\"", $xml_return),
                ]);
                
                if($StatusQXtend_RC == "Success"){
                    $MessageError = "";
                    list($BF_Success, $BF_Error, $MessageBackflush) = backflush_qxtend($LPBCode,$PO,$PackingSlip,$EffDate,$LocSupplier);
                    $MessageError = "Detail Error Backflush : \r" . ErrorFilterBF2($MessageBackflush);
                }

                if($BF_Error == "0"){
                    $StatusQXtend_BF = "Success";
                }
                
                if($StatusQXtend_RC == "Error" || $StatusQXtend_BF == "Error"){
                    /*FINAL STATUS*/
                    $FinalStatus = "Error";
                    sendWA($StatusQXtend_RC,$StatusQXtend_BF,$LPBCode);
                }

                $ListRC = rtrim($ListRC,",");
            }
            DB::table('elpb_dtl')
                    ->where('lpb_code', $LPBCode)
                    ->update([
                        'outstanding_status'  => 'FINISH',
                        'outstanding_start'   => null,
                    ]);
            //SELESAI PROSES BF & PO RECEIPT
            
            // echo $FinalStatus . " " . $ListRC;
            echo json_encode(array("status" => $FinalStatus, "rc" => $ListRC, "message" => $MessageError));
        }

        if($Status == "1b"){

            $LPBCode    = $_POST['LPBCode'];
            $i          = $_POST['i'];
            $row      = collect(\DB::select("SELECT TOP 1 po,packing_slip FROM elpb_dtl WHERE lpb_code='".$LPBCode."'"));
            
            $query_rc = collect(\DB::select("SELECT TOP 1 H.prh_receiver FROM prh_hist H WHERE prh_domain='AAIJ' AND prh_nbr='".$row->po."' AND prh_ps_nbr='".substr($row->packing_slip,0,20)."' WITH(NOLOCK)"));
            
            $RC = $query_rc->prhReceiver;
            
            DB::select("UPDATE elpb_dtl SET rc='".$RC."',last_modified_by='". $i ."' WHERE lpb_code='".$LPBCode."' AND  po='".$row->po."' AND packing_slip='".$row->packing_slip."'");
                
            echo $RC;
        }

        if($Status == "2"){

            $LPBCode    = $_POST['LPBCode'];
            $PO         = $_POST['PO'];
            $PS         = $_POST['PS'];
            $ItemNumber = $_POST['ItemNumber'];
            $Line       = $_POST['Line'];
            $OldQty     = $_POST['OldQty'];
            $NewQty     = $_POST['NewQty'];
            $Username   = $_SESSION["username"];
            $Diff       = $OldQty - $NewQty;
            
            DB::select("UPDATE elpb_dtl SET qty_after_qc='".$NewQty."' WHERE lpb_code='".$LPBCode."' AND  po='".$PO."' AND  line='".$Line."' ");

            DB::select("INSERT INTO elpb_change_qty_approval(lpb_code,po,line,old_qty,new_qty,created_by) 
            VALUES('".$LPBCode."','".$PO."','".$Line."','".$OldQty."','".$NewQty."','".$Username."')");

            DB::select("IF EXISTS(SELECT cst_surat_jalan FROM cst_part_in  WHERE cst_in_po='".$PO."' AND cst_line_po='".$Line."' AND cst_surat_jalan='".$PS."' )
            UPDATE cst_master_item SET part_qty=part_qty-(".$Diff.") WHERE part_no='".$ItemNumber."'");
            
            if($NewQty == "0" || $NewQty == ""){
                DB::select("UPDATE cst_part_in SET cst_alasan_cancel='Data input tablet salah' , cst_status = 'CANCEL' WHERE cst_in_po='".$PO."' AND cst_line_po='".$Line."' AND cst_surat_jalan='".$PS."' ");
            }else{
                DB::select("UPDATE cst_part_in SET cst_in_qty='".$NewQty."', cst_alasan_cancel=NULL , cst_status = NULL WHERE cst_in_po='".$PO."' AND cst_line_po='".$Line."' AND cst_surat_jalan='".$PS."' ");
            }
        }

        if($Status == "3"){

            $PO         = $_POST['PO'];
            $ListOpenPO = get_open_po($PO,0);
            
            $ListLine   = $_POST['ListLine'];
            $Cond       = "";
            $Cond2      = "";
            $Cond3      = "";
            if($ListLine != ""){
                $Cond   = " and prh_line NOT IN (".$ListLine.") ";
                $Cond2  = " and portald_line NOT IN (".$ListLine.") ";
                $Cond3  = "  AND pod_line NOT IN (".$ListLine.") ";
            }

            $listRC         = array();
            $listProcessing = array();
            $listRC         = array();
            
            /*GET RECEIVED*/
            // $rs_cek_rcvd = DB::selc"select prh_line,sum(prh_rcvd) from prh_hist 
            //                 where prh_nbr='".$PO."' $Cond and prh_domain='AAIJ' GROUP BY prh_line order by prh_line WITH(NOLOCK)";
            $rs_cek_rcvd = odbc_exec($conn_mfg, $sql_cek_rcvd);
            while (odbc_fetch_row($rs_cek_rcvd)){
                $PO_Line = odbc_result($rs_cek_rcvd,1);
                $listRC[$PO ."_" . $PO_Line] = odbc_result($rs_cek_rcvd,2);
                
            }
            /*GET PROCESSING (QC)*/
            $item_sj = "select portald_line,sum(portald_qty_ship) from portald_det where portald_po_nbr='".$PO."'
                        $Cond2
                        and portald__date02 IS NOT NULL AND portald_confirm_date is null 
                        GROUP BY portald_line WITH(NOLOCK)";
                        
            $rs_cek_sj = odbc_exec($conn_portal, $item_sj);	
            while (odbc_fetch_row($rs_cek_sj))
            {
                $PO_Line = odbc_result($rs_cek_sj,1);
                $listProcessing[$PO ."_" . $PO_Line] = odbc_result($rs_cek_sj,2);
            }
            
            
            
            $sql_cek_mfg = "SELECT  pod_nbr,pod_line,pod_part,pod_qty_ord,pod_qty_rcvd,pod_um,pod_desc,pod_type,pod_loc,po_req_id,
                                            pt_desc1,pt_desc2,pt_drwg_loc, pt_pm_code,pt_memo_type,A.rqd_nbr, R.rqm_reason 
                                            FROM po_mstr M 
                                            INNER JOIN pod_det D ON M.po_nbr=D.pod_nbr AND M.po_domain=D.pod_domain
                                            INNER JOIN rqd_det A ON D.pod_req_nbr=A.rqd_nbr AND D.pod_req_line=A.rqd_line AND D.pod_domain=A.rqd_domain 
                                            INNER JOIN rqm_mstr R ON A.rqd_nbr=R.rqm_nbr AND A.rqd_domain=R.rqm_domain 
                                            LEFT JOIN pt_mstr PM ON D.pod_part=PM.pt_part AND D.pod_domain=PM.pt_domain
                                            WHERE pod_nbr='".$PO."' $Cond3  AND pod_domain='AAIJ' AND pod__chr03='' order by pod_nbr,pod_line WITH(NOLOCK)";
            $rs_cek = odbc_exec($conn_mfg, $sql_cek_mfg);	  
            while (odbc_fetch_row($rs_cek))
            {	
                
                $PO = odbc_result($rs_cek,1);
                $PO_Line = odbc_result($rs_cek,2);
                $item = odbc_result($rs_cek,3);
                $qty_ord = odbc_result($rs_cek,4);
                $um = odbc_result($rs_cek,6);
                $po_desc = odbc_result($rs_cek,7);
                $po_type = odbc_result($rs_cek,8);
                $loc = odbc_result($rs_cek,9);
                $po_pic = odbc_result($rs_cek,10);
                $desc = odbc_result($rs_cek,11);
                $desc2 = odbc_result($rs_cek,12);
                $type = odbc_result($rs_cek,13);
                $pm_code = odbc_result($rs_cek,14);
                $memo = strtoupper(odbc_result($rs_cek,8));
                $pp = strtoupper(odbc_result($rs_cek,16));
                $pp_desc = strtoupper(odbc_result($rs_cek,17));
            
                
                $currPOList = $PO ."_" . $PO_Line;
                $qty_process = $listProcessing[$currPOList];
                $qty_rc = $listRC[$currPOList];
                $openPO = $qty_ord - ($qty_rc + $qty_process);
                
                if($po_desc == "")
                {
                    $po_desc = $desc." ".$desc2;
                }
                $openPOQAD = $ListOpenPO[$PO][$PO_Line];
                echo "<tr class='trDataPopUP'>";
                        echo "<td class='tdPopUp_Line' style='text-align:center;width:15%;font-size:16px;'>".$PO_Line."</td>";
                        echo "<td class='tdPopUp_Desc' style='width:85%;' loc='".$loc."' qty_open='".$openPO."'  open_po_qad='".$openPOQAD."' ps='".$ps."' po='".$PO."' qty_po='".$qty_rc."' qc='".$cek."' qc='".$cek."' memo='".strtoupper($memo)."' pm_code='".$pm_code."' desc='".$po_desc."' um='".$um."' pp='".$pp."' pp_desc='".$pp_desc."' part_type='".$desc2."'><div class='divPopUP_PO'></div><div class='divPopUP_Item'>".strtoupper($item)." : " . strtoupper($type)."</div>".$po_desc."
                        <div class='divPopUP_Location'><img src='image/WHS.png' class='imgWHS'><div class='divPopUP_LocDetail'>".$loc."</div></div></td>";
                echo "</tr>";
                        
            }
                                
        
        }
        if($Status == "4"){
            dd($_POST);
            $ListPartQC = array();
            $ListPartQC = partQC();
            
            $LPBCode = $_POST['LPBCode'];
            $PO = $_POST['PO'];
            $Line = $_POST['Line'];
            $ItemNumber = $_POST['ItemNumber'];
            $Desc = $_POST['Desc'];
            $Type = $_POST['Type'];
            $Loc = $_POST['Loc'];
            $UM = $_POST['UM'];
            $Memo = strtoupper($_POST['Memo']);
            $PMCode = $_POST['PMCode'];
            $Qty = $_POST['Qty'];
            $PP = $_POST['PP'];
            $PPDesc = $_POST['PPDesc'];
            $PartType = $_POST['PartType'];
            $Username = $_SESSION["username"];
            $BFQxtend = "No Backflush";
            
            if($Memo == "M" && $PMCode == "L")
                $BFQxtend = "Backflush";
            if($ListPartQC[$ItemNumber] == '')
            {
                $Cek = 'Non Check';
            }
            else
            {
                $Cek = 'Check';
            }
            
            DB::select("INSERT INTO elpb_dtl(lpb_code,trans_id,invoice,po,packing_slip,line,
            item_number,item_desc,loc,type,qty_po,qty_shipping,qty_open_po,qty_received,qty_after_qc,um,
            memo,pm_code,bf_qxtend,status_item,created_by,created_date,pp,pp_desc)
            SELECT TOP 1 
                lpb_code,trans_id,invoice,'".$PO."',packing_slip,'".$Line."',
                '".$ItemNumber."','".$Desc."','".$Loc."','".$PartType."',0,'".$Qty."',0,'".$Qty."','".$Qty."','".$UM."',
                '".strtoupper($Memo)."','".$PMCode."','".$BFQxtend."','".$Cek."','".$Username."',GETDATE(),'".$PP."','".$PPDesc."'
            FROM elpb_dtl WHERE lpb_code='".$LPBCode."'");
            DB::select("EXEC [USP_InsertCSTPartIn_AddItem] '".$LPBCode."','".$Line."'");
        }
        if($Status == "5"){
            include "./class/connect_progress_replication.class.php";
            $LPBCode = $_POST['LPBCode'];
            $RC = "";
            $ListRC = "";
            $query = DB::select("SELECT po, packing_slip, MAX(A.eff_date) eff_date,MAX(A.eff_time) eff_time FROM elpb_mstr A
                INNER JOIN elpb_dtl B ON A.lpb_code=B.lpb_code 
                WHERE A.lpb_code='".$LPBCode."' GROUP BY po, packing_slip");
            while($row=mssql_fetch_array($query))
            {
                $PO = $row['po'];
                $PackingSlip = $row['packing_slip'];
                $query_rc = "SELECT TOP 1 H.prh_receiver FROM prh_hist H WHERE prh_domain='AAIJ' AND prh_nbr='".$PO."' AND prh_ps_nbr='".$PackingSlip."' WITH(NOLOCK)";
                $rs_rc = odbc_exec($conn_mfg, $query_rc);	  
                $RC = odbc_result($rs_rc,1);
                $ListRC .= $RC .",";
            }
            echo $ListRC;
            
        }

        if($Status == "6"){
            $EffDate = $_POST['EffDate'];
            $LPBCode = $_POST['LPBCode'];
            
            $row=mssql_fetch_array(DB::select("SELECT eff_date FROM elpb_mstr WHERE lpb_code='".$LPBCode."'"));
            
            $EffDate_Old = $row['eff_date'];
            
            DB::select("INSERT INTO elpb_change_effdate(lpb_code, eff_date_before,eff_date_after,created_date,created_by) VALUES('".$LPBCode."','".$EffDate_Old."','".$EffDate."',GETDATE(),'".$_SESSION["username"]."')");
            
            DB::select("UPDATE elpb_mstr SET eff_date='".$EffDate."' WHERE lpb_code='".$LPBCode."'");
        }

        if($Status == "7"){
            $PS = $_POST['PS'];
            $PS_Old = $_POST['PS_Old'];
            $LPBCode = $_POST['LPBCode'];
            
            DB::select("INSERT INTO elpb_change_packingslip(lpb_code, ps_before,ps_after,created_date,created_by) VALUES('".$LPBCode."','".$PS_Old."','".$PS."',GETDATE(),'".$_SESSION["username"]."')");
            
            DB::select("UPDATE elpb_dtl SET packing_slip='".$PS."' WHERE lpb_code='".$LPBCode."' AND packing_slip='".$PS_Old."'");
        }

        if($Status == "8"){
            $LPBCode = $_POST['LPBCode'];
            $PS = $_POST['PS'];
            $SuppCode = $_POST['SuppCode'];
            $SuppName = $_POST['SuppName'];
            $Reason = $_POST['Reason'];
            $Username = $_SESSION["username"];
            
            DB::select("INSERT INTO elpb_change_status_approval(lpb_code,packing_slip,reason,created_by,created_date) VALUES('".$LPBCode."','".$PS."','".$Reason."','".$Username."',GETDATE())");
            DB::select("UPDATE elpb_mstr SET status_approval = 'ApprovalX' WHERE lpb_code='".$LPBCode."'");
            
            sendWA_Hide($LPBCode,$Reason,$PS,$SuppCode,$SuppName, $Username);
        }
        if($Status == "9"){
            $PO = $request->get('PO');
            $Total = DB::select("SELECT DISTINCT TOP 1  a.po_nbr FROM (select max(po_revisi) as revisi, po_nbr from po_trans_mstr where po_nbr ='".$PO."' group by po_nbr ) aux inner join po_trans_mstr a on a.po_nbr = aux.po_nbr where aux.revisi=a.po_revisi and a.po_app2_status='OK'");
            $count = count($Total);
            echo $count;
        }
    }

    public function print_lpb()
    {
        return view('lpb.index');
    }
    public function search_lpb(Request $request){
        $rc         = $request->get('rc');
        $supplier   = Auth::guard('pub_login')->user()->kode_supplier;
        // $url = "https://supplier.akebono-astra.co.id/wsatdw/prh_hist.php";
        // getPrhHist($url);
        $supplier_data = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr',$supplier)->first();
        $tampil_pemakai = prh_hist_rc($rc);
        // dd($tampil_pemakai);
        if($tampil_pemakai == false){
            $table .= '';
        }else{
            $itung = count($tampil_pemakai);
            $table = '';
            if($itung > 0){
                foreach($tampil_pemakai as $tmp)
                    $url = $this->url->to('/print-elpb/'.base64_encode($tmp['prhNbr']).'/'.base64_encode($tmp['prhPsNbr']).'');
                    $preview = $this->url->to('/preview-elpb/'.base64_encode($tmp['prhNbr']).'/'.base64_encode($tmp['prhPsNbr']).'');
                    $table .= '
                        <tr>
                            <td>'.$tmp['prhReceiver'].'</td>
                            <td>'.$tmp['prhNbr'].'</td>
                            <td>'.$supplier_data->ct_ad_name.'</td>
                            <td>'.$tmp['prhPsNbr'].'</td>
                            <td>Status Print</td>
                            <td><a href="'.$url.'" target="_blank" class="btn btn-info"><i class="fas fa-print"></i> Print</a>

                            <a href="'.$preview.'" target="_blank" class="btn btn-success"><i class="fas fa-search"></i> Preview</a></td>
                        </tr>';
            }else{
                $table .= '';
            }
        }
        
        return response($table);

    }
    public function direct(Request $request){
        $rc     = $request->get('rc');
        $hak    = Auth::guard('pub_login')->user()->previllage;
        $supp   = Auth::guard('pub_login')->user()->kode_supplier;

        $cek = DB::select("SELECT prhNbr,prhVend,prhReceiver,prhRcp_date,prhPsNbr
        FROM SOAP_prh_hist WHERE prhDomain='AAIJ' AND prhReceiver='".$rc."' AND prhVend = '$supp'");

        $itung = count($cek);
        if($itung > 0){
            $data['url'] = $this->url->to('/print-elpb/'.base64_encode($cek[0]->prhNbr).'/'.base64_encode($cek[0]->prhPsNbr).'');
            $data['message'] = 1;
        }else{
            $data['message'] = 0;
        }

        echo json_encode($data);
    }

    public function print_lpb2($po,$ps)
    {
        $ponya  = base64_decode($po);
        $psnya  = base64_decode($ps);
        $supp   = Auth::guard('pub_login')->user()->kode_supplier;
        $cek    = prh_hist($ponya);
        $kode   = Auth::guard('pub_login')->user()->kode_supplier;
        $i = 0;
        foreach($cek as $c){
            if($c['prhPsNbr'] == $psnya){
                $tangkap[$i++] = array(
                    'prhNbr' => $c['prhNbr'],
                    'prhVend' => $c['prhVend'],
                    'prhReceiver' => $c['prhReceiver'],
                    'prhRcp_date' => $c['prhRcp_date'],
                    'prhPsNbr' => $c['prhPsNbr'],
                );
            }
        }
        $a = 0;
        $b = 0;
        if ($kode ==''){
            $tampil_data_po_detail = po_detail($ponya);
            foreach($cek as $c){
                if($c['prhReceiver'] == $tangkap[0]['prhReceiver']){
                    $tampil_data_rc[$a++] = array(
                        'prhLine' => $c['prhLine'],
                        'prhNbr' => $c['prhNbr'],
                        'prhVend' => $c['prhVend'],
                        'prhPart' => $c['prhPart'],
                        'prhReceiver' => $c['prhReceiver'],
                        'prhPsNbr' => $c['prhPsNbr'],
                        'prhRcvd' => $c['prhRcvd'],
                        'prhRcp_date' => $c['prhRcp_date'],
                        'prhReason' => $c['prhReason'],
                        'prhUm' => $c['prhUm'],
                        'prhPrint' => $c['prhPrint'],
                        'prhLog01' => $c['prhLog01'],
                        'prhType' => $c['prhType'],
                        'prhUmConv' => $c['prhUmConv'],
                    );
                }
            }

            foreach($cek as $c){
                if($c['prhReceiver'] == $tangkap[0]['prhReceiver']){
                    $cek_rc[$b++] = array(
                        'countline' => $c['prhLine']
                    );
                }
            }
            $hitung_rc = count($cek_rc);

        }else{

            $tampil_data_po_detail = po_detail($ponya);
            foreach($cek as $c){
                if($c['prhReceiver'] == $tangkap[0]['prhReceiver'] && $c['prhVend'] == $kode){
                    $tampil_data_rc[$a++] = array(
                        'prhLine' => $c['prhLine'],
                        'prhNbr' => $c['prhNbr'],
                        'prhVend' => $c['prhVend'],
                        'prhPart' => $c['prhPart'],
                        'prhReceiver' => $c['prhReceiver'],
                        'prhPsNbr' => $c['prhPsNbr'],
                        'prhRcvd' => $c['prhRcvd'],
                        'prhRcp_date' => $c['prhRcp_date'],
                        'prhReason' => $c['prhReason'],
                        'prhUm' => $c['prhUm'],
                        'prhPrint' => $c['prhPrint'],
                        'prhLog01' => $c['prhLog01'],
                        'prhType' => $c['prhType'],
                        'prhUmConv' => $c['prhUmConv'],
                    );
                }
            }

            foreach($cek as $c){
                if($c['prhReceiver'] == $tangkap[0]['prhReceiver'] && $c['prhVend'] == $kode){
                    $cek_rc[$b++] = array(
                        'countline' => $c['prhLine']
                    );
                }
            }

            $hitung_rc = count($cek_rc);


        }

        $tampil_tr = DB::table('portal_mstr')
        ->join('portald_det','portald_det.portald_tr_id' ,'=','portal_mstr.portal_tr_id')
        ->where('portal_po_nbr',$ponya)
        ->where('portal_ps_nbr',$psnya)->first();
        $tampil_supplier = DB::table('SOAP_pub_business_relation')
        ->where('ct_vd_addr',$supp)->first();

        return view('lpb.print',compact('tampil_tr','tangkap','tampil_supplier','tampil_data_rc','hitung_rc','tangkap','tampil_data_po_detail'));
    }

    public function preview_lpb2($po,$ps)
    {
        $ponya  = base64_decode($po);
        $psnya  = base64_decode($ps);
        $supp   = Auth::guard('pub_login')->user()->kode_supplier;
        $cek    = prh_hist($ponya);
        $kode   = Auth::guard('pub_login')->user()->kode_supplier;
        $i = 0;
        foreach($cek as $c){
            if($c['prhPsNbr'] == $psnya){
                $tangkap[$i++] = array(
                    'prhNbr' => $c['prhNbr'],
                    'prhVend' => $c['prhVend'],
                    'prhReceiver' => $c['prhReceiver'],
                    'prhRcp_date' => $c['prhRcp_date'],
                    'prhPsNbr' => $c['prhPsNbr'],
                );
            }
        }
        $a = 0;
        $b = 0;
        if ($kode ==''){
            $tampil_data_po_detail = po_detail($ponya);
            foreach($cek as $c){
                if($c['prhReceiver'] == $tangkap[0]['prhReceiver']){
                    $tampil_data_rc[$a++] = array(
                        'prhLine' => $c['prhLine'],
                        'prhNbr' => $c['prhNbr'],
                        'prhVend' => $c['prhVend'],
                        'prhPart' => $c['prhPart'],
                        'prhReceiver' => $c['prhReceiver'],
                        'prhPsNbr' => $c['prhPsNbr'],
                        'prhRcvd' => $c['prhRcvd'],
                        'prhRcp_date' => $c['prhRcp_date'],
                        'prhReason' => $c['prhReason'],
                        'prhUm' => $c['prhUm'],
                        'prhPrint' => $c['prhPrint'],
                        'prhLog01' => $c['prhLog01'],
                        'prhType' => $c['prhType'],
                        'prhUmConv' => $c['prhUmConv'],
                    );
                }
            }

            foreach($cek as $c){
                if($c['prhReceiver'] == $tangkap[0]['prhReceiver']){
                    $cek_rc[$b++] = array(
                        'countline' => $c['prhLine']
                    );
                }
            }
            $hitung_rc = count($cek_rc);

        }else{

            $tampil_data_po_detail = po_detail($ponya);
            foreach($cek as $c){
                if($c['prhReceiver'] == $tangkap[0]['prhReceiver'] && $c['prhVend'] == $kode){
                    $tampil_data_rc[$a++] = array(
                        'prhLine' => $c['prhLine'],
                        'prhNbr' => $c['prhNbr'],
                        'prhVend' => $c['prhVend'],
                        'prhPart' => $c['prhPart'],
                        'prhReceiver' => $c['prhReceiver'],
                        'prhPsNbr' => $c['prhPsNbr'],
                        'prhRcvd' => $c['prhRcvd'],
                        'prhRcp_date' => $c['prhRcp_date'],
                        'prhReason' => $c['prhReason'],
                        'prhUm' => $c['prhUm'],
                        'prhPrint' => $c['prhPrint'],
                        'prhLog01' => $c['prhLog01'],
                        'prhType' => $c['prhType'],
                        'prhUmConv' => $c['prhUmConv'],
                    );
                }
            }

            foreach($cek as $c){
                if($c['prhReceiver'] == $tangkap[0]['prhReceiver'] && $c['prhVend'] == $kode){
                    $cek_rc[$b++] = array(
                        'countline' => $c['prhLine']
                    );
                }
            }

            $hitung_rc = count($cek_rc);


        }

        $tampil_tr = DB::table('portal_mstr')
        ->join('portald_det','portald_det.portald_tr_id' ,'=','portal_mstr.portal_tr_id')
        ->where('portal_po_nbr',$ponya)
        ->where('portal_ps_nbr',$psnya)->first();
        $tampil_supplier = DB::table('SOAP_pub_business_relation')
        ->where('ct_vd_addr',$supp)->first();

        return view('lpb.preview',compact('tampil_tr','tangkap','tampil_supplier','tampil_data_rc','hitung_rc','tangkap','tampil_data_po_detail'));
    }
}
