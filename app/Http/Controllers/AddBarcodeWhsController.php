<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;

class AddBarcodeWhsController extends Controller
{
    public function index()
    {
        $url = "https://supplier.akebono-astra.co.id/wsatdw/loc_mstr.php";
            $data = loc_mstr($url);
        return view('add-barcode-whs.add',compact('data'));
    }

    public function get_item(Request $request)
    {
        if ($request->ajax()) {
            // web service
            // $item = pt_mstr_bydesc("");
            // $data = $item['ttItem_desc']['ttItem_descRow'];
            // table
            $data = DB::table('SOAP_pt_mstr_bydesc')->get();
            return DataTables::of($data)
            // ->addColumn('actione', function ($data) {
            //     return '
            //         <td color="white">
            //         <a name="add" id="added" data-itema="'.$data["item_number"].'" data-itemb="'.$data["deskripsi1"].'" class="btn btn-warning btn-sm"><i class="fas fa-check"></i></i></a>
            //         </td>
            //     ';
            // })
            // pakek table
            ->addColumn('actione', function ($data) {
                return '
                    <td color="white">
                    <a name="add" id="added" data-itema="'.$data->item_number.'" data-itemb="'.$data->deskripsi1.'" class="btn btn-warning btn-sm text-white">Click <i class="fas fa-check"></i></a>
                    </td>
                ';
            })
            ->rawColumns(['actione'])
            ->make(true);
        }
    }
    public function get_loc(Request $request)
    {
        if ($request->ajax()) {
            $url = "https://supplier.akebono-astra.co.id/wsatdw/loc_mstr.php";
            $data = loc_mstr($url);
            return DataTables::of($data)
            ->addColumn('actione', function ($data) {
                if(!empty($data['locLoc']) && (!empty($data['locDes'])) ) {
                return '
                    <td color="white">
                    <a name="add"  data-itema="'. $data["locLoc"].'" data-itemb="'.$data["locDesc"].'" class="add btn btn-warning text-white btn-sm">Click <i class="fas fa-check text-white"></i></i></a>
                    </td>
                ';
                }elseif(!empty($data['locLoc'])) {
                    return '
                    <td color="white">
                    <a name="add"  data-itema="'. $data["locLoc"].'" data-itemb="-" class="add btn btn-warning text-white btn-sm">Click <i class="fas fa-check text-white"></i></i></a>
                    </td>
                ';
                }elseif((!empty($data['locDes']))){
                    return '
                    <td color="white">
                    <a name="add"  data-itema="-" data-itemb="'.$data["locDesc"].'" class="add btn btn-warning text-white btn-sm">Click <i class="fas fa-check text-white"></i></i></a>
                    </td>
                ';
                }else{
                    return '
                    <td color="white">
                    <a name="add"  data-itema="-" data-itemb="-" class="add btn btn-warning text-white btn-sm">Click <i class="fas fa-check text-white"></i></i></a>
                    </td>
                ';
                }
            })
            ->addColumn('locloc', function ($data) {
                if (!empty($data['locLoc'])) {
                    return '
                    <td color="white">
                    '.$data['locLoc'].'
                    </td>
                ';
                }else{
                    return '
                    <td color="white">
                    -
                    </td>
                ';
                }
            })
            ->addColumn('locdesc', function ($data) {
                if (!empty($data['locDesc'])) {
                    return '
                    <td color="white">
                    '.$data['locDesc'].'
                    </td>
                ';
                }else{
                    return '
                    <td color="white">
                    -
                    </td>
                ';
                }
            })
            ->rawColumns(['actione','locdesc','locloc'])
            ->make(true);
        }
    }
    public function store(Request $request)
    {
        $itm_nbr = $request->itm_nbr;
        $itm_desc = $request->itm_desc;
        $type = $request->type;
        $snp = $request->snp;
        $box = $request->box;
        $loc_from = $request->loc_from;
        $loc_to = $request->loc_to;
        $common = $request->common;
        $lastbox = $request->lastbox;
        $backno = $request->backno;
        $cek_mstr = "select polibox_item from polibox_mstr where polibox_item='$itm_nbr' and polibox_loc_to='$loc_to' and polibox_type='$type' and polibox_snp='$snp'";
        $cek_mstr = DB::select($cek_mstr);
        $cekid = "select idbox from polibox_idbox where partno='$itm_nbr' and locto='$loc_to' and tipe='$type' and snp='$snp'";
        $cekid =DB::select($cekid);
        if(!empty($cekid)){
            $idbox = $cekid[0]->idbox;
        }
        if(empty($cek_mstr))
        {
            // return 'cek_mstr = 0';
            if(empty($idbox))
            {
                // return "idbox kosong";false;
                $id = "select isnull(max(substring(idbox,1,4)),0)+1 as nomer from polibox_idbox";
                $id = DB::select($id);
                $no = str_pad($id[0]->nomer, 4, "0", STR_PAD_LEFT);
                DB::table('polibox_idbox')->insert(
                    ['idbox' => $no, 'partno' => $itm_nbr,'locto' => $loc_to, 'tipe' => $type, 'snp' => $snp]
                );
            }
            // else
            // {

            // }
        }
        $cekidd = "select idbox from polibox_idbox where partno='$itm_nbr' and locto='$loc_to' and tipe='$type' and snp='$snp'";
        $cekidd =DB::select($cekidd);
        if(!empty($cekidd)){
            $idbox = $cekidd[0]->idbox;
        }
        //exit;
        //$del_data = mssql_query("delete polibox_mstr where polibox_item='$itm_nbr' and polibox_loc_from='$loc_from' and polibox_loc_to='$loc_to'");
        $last_sort = "select max(polibox_subbox) as lastbox from polibox_mstr where polibox_item='$itm_nbr' and
        polibox_loc_to='$loc_to' and polibox_snp='$snp' and polibox_comon='$common' and polibox_type='$type'";
        $last_box = DB::select($last_sort);
        $lastbox = $last_box[0]->lastbox;
        //exit;
        $no = 1;
        // if(empty($idbox)) {
        //     dd($lastbox);
        // }else{
        //     dd($idbox);
        // }
        for($i=$lastbox+1;$i<=$box+$lastbox;$i++)
        {
            if(empty($idbox) and empty($cek_mstr))
            {
                $nbr = $no."-".$i;
            }
            elseif(!empty($idbox) and !empty($cek_mstr))
            {
                $nbr = $idbox."-".$i;
            }
            else
            {
                $nbr = $idbox."-".$i;
                // $nbr = $itm_nbr.$i.$snp;
                // if($common=="True")
                // {
                    // $nbr = $nbr.$loc_to;
                // }
            }

            $data = "select * from polibox_mstr where polibox_nbr = '$nbr' and
            polibox_loc_to='$loc_to' and polibox_snp='$snp' and polibox_type='$type'";
            $data = DB::select($data);
            if(!empty($data))
            {
                DB::table('polibox_mstr')
                ->where('polibox_nbr',$nbr)
                ->update(['polibox_deskripsi' => $itm_desc,'polibox_snp' => $snp,'polibox_loc_from' => $loc_from,'polibox_loc_to',$loc_to,'polibox_nbr' => $nbr,'polibox_type' => $type,'polibox_comon' => $common, 'polibox_backno' => $backno]);
            }
            else
            {
                if($itm_nbr != null)
                {
                    $cek=  DB::table('polibox_mstr')->insert(
                        ['polibox_item' => $itm_nbr,'polibox_subbox' => $i,'polibox_deskripsi' => $itm_desc,'polibox_snp' => $snp,'polibox_loc_from' => $loc_from,'polibox_loc_to' => $loc_to,'polibox_nbr' => $nbr,'polibox_type' => $type,'polibox_comon' => $common,'polibox_creadate' => date('Y-m-d H:i'), 'polibox_backno' => $backno]
                    );
                    //exit;
                    if(!($cek))
                    {
                        // echo "<script language=\"javascript\"> alert('Gagal, ID Barcode sudah pernah dipakai !'); location.href='/add-barcode-whs';</script>";
                        // toast('Gagal, ID Barcode sudah pernah dipakai !','danger')->position('top-end')->autoClose(3000)->timerProgressBar();
                        alert()->success('danger', 'Added Barcode WHS Failed');
                        return back();
                    }
                }
            }

        }
        alert()->success('success', 'Added Barcode WHS successfully');
        return redirect()->route('detail-barcode-whs',[base64_encode($itm_nbr),base64_encode($loc_from),base64_encode($loc_to),base64_encode($snp),base64_encode($common)]);
        // echo "<script language=\"javascript\">  location.href='/add-barcode-whs/detail?pi=$itm_nbr&locfrom=$loc_from&locto=$loc_to&snp=$snp&comon=$common'; </script>";
        // return back();
    }

    public function view_detail(Request $request)
    {
        // dd($request);
        $pi = $request->pi;
        $locfrom = $request->locfrom;
        $locto = $request->locto;
        $snp = $request->snp;
        $comon = $request->comon;
        $select_q = "select * from polibox_mstr where (polibox_item = '".$pi."') and polibox_loc_from='".$locfrom."'
				and polibox_loc_to='".$locto."' and polibox_snp='".$snp."' and polibox_comon='".$comon."' order by polibox_item,polibox_type,polibox_subbox asc";
				// $select_e = mssql_query($select_q);
            $data = DB::select($select_q);
            dd($data);
        return view('add-barcode-whs.vw-barcode-detail',compact('data'));
    }
}
