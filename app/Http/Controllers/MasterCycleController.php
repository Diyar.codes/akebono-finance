<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\MasterCycle;

class MasterCycleController extends Controller
{
    public function index()
    {
        return view('master-cycles.index');
    }

    public function seconddatamastercycle(Request $request) {
        $query = $request->get('query');
        if($query != '') {
            $data = MasterCycle::where('cycle_nama_supplier', 'like', '%' . $query . '%')->select('cycle_kode_supplier', 'cycle_nama_supplier', 'cycle_start', 'cycle_end', 'cycle_cycle')->orderBy('cycle_cycle', 'asc')->get();
        } else {
            $tablenya = '';

            $tablenya .= '<tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>';

            return response($tablenya);
        }

        $tablenya = '';

        foreach($data as $row){
            $tablenya .= '<tr>
                            <td>'.$row->cycle_kode_supplier.'</td>
                            <td>'.$row->cycle_nama_supplier.'</td>
                            <td>'.$row->cycle_cycle.'</td>
                            <td><input type="time" class="form-control form-control-sm" name="start_time[]" value="'.\Carbon\Carbon::parse($row->cycle_start)->format('H:i').'"></td>
                            <td><input type="time" class="form-control form-control-sm" name="end_time[]" value="'.\Carbon\Carbon::parse($row->cycle_end)->format('H:i').'"></td>
                        </tr>';
        }

        return response($tablenya);
    }

    public function create()
    {
        return view('master-cycles.create');
    }

    public function store(Request $request)
    {
    }

    public function show(MasterCycle $masterCycle)
    {
    }

    public function edit(MasterCycle $masterCycle)
    {
    }

    public function update(Request $request, $id)
    {
        $data = MasterCycle::where('cycle_nama_supplier', 'like', '%' . $id . '%')->select('cycle_kode_supplier', 'cycle_nama_supplier', 'cycle_start', 'cycle_end', 'cycle_cycle')->orderBy('cycle_cycle', 'asc')->get();

        if(sizeof($data) == 0) {
            $response = [
                'status'     => 'failed',
                'message'    => 'order update failed',
            ];

            return response()->json($response, 404);
        }

        try {
            if(count($data) > 0) {
                foreach($data as $item=>$v) {
                    MasterCycle::where('cycle_cycle', $v->cycle_cycle)->update([
                        'cycle_start'         => $request->cycle_start[$item],
                        'cycle_end'           => $request->cycle_end[$item],
                    ]);
                }
            }

            $response = [
                'status'     => 'success',
                'message'    => 'Cycle update complete',
            ];

            return response()->json($response, 201);
        } catch (\Exception $e) {
            DB::rollBack();

            alert()->error('error',$e->getMessage());
            return redirect()->back();
        }
    }

    public function destroy(MasterCycle $masterCycle)
    {
    }
}
