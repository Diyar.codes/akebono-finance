<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PendingBarcodeWhs extends Controller
{
    public function index()
    {
        return view('pending-barcode-whs.index');
    }

    public function detailpending($item, $month, $year)
    {
        // $result = mssql_query("select trans_id,a.polibox_item_nbr, snp, creadate, note, locfrom, locto from polibox_trans a where
        // ((month(a.effdate_qad) = '$monthNow' and year(a.effdate_qad) = '$yearNow'  AND effdate_qad IS NOT NULL) OR (month(a.creadate) = '$monthNow' and year(a.creadate) = '$yearNow'  AND effdate_qad IS NULL)) 
        // and status in('ERROR','NG','ON PROGRESS') and polibox_item_nbr='$item' order by trans_id asc");

        $resitem = base64_decode($item);
        $resmonth = base64_decode($month);
        $resyear = base64_decode($year);

        $hitung = 1;
        $month = date("m");
        $month_search = $resmonth;
        $month_search_next = $resyear;
        $monthNow = $resmonth;
        $yearNow = $resyear;

        $result = DB::table('polibox_trans')
            ->whereRaw("((month(effdate_qad) = '$monthNow' and year(effdate_qad) = '$yearNow'  AND effdate_qad IS NOT NULL) OR (month(creadate) = '$monthNow' and year(creadate) = '$yearNow'  AND effdate_qad IS NULL))")
            ->where('polibox_item_nbr', $resitem)
            ->whereIn('status', ['ERROR', 'NG', 'ON PROGRESS'])
            ->orderBy('trans_id', 'ASC')
            ->get();

        return view('pending-barcode-whs.detail-pending', compact('result'));
    }

    public function datapendingbarcode(Request $request)
    {
        $month_date = date("Y-m-01");
        $month_search = date("Y") . "-" . date("m") . "-01 07:30";
        $month_search_next = date("Y-m-d", strtotime($month_search . " +1 month")) . " 07:30";
        $monthNow = date("m");
        $yearNow = date("Y");
        $hitung = 1;

        if ($request->bulan != '' || $request->tahun != '') {
            $month_date = date("Y-m-01");
            $month_search = $request->tahun . "-" . $request->bulan . "-01 07:30";
            $month_search_next = date("Y-m-d", strtotime($month_search . " +1 month")) . " 07:30";
            $lastMonth = date("m", strtotime($month_date . " -1 days"));
            $hitung = 1;
            $month = date("m");
            $monthNow = $request->bulan;
            $yearNow = $request->tahun;
        }

        $data = DB::table('polibox_trans')
            ->select('polibox_item_nbr', 'polibox_deskripsi', 'polibox_snp', 'locfrom', 'locto', DB::raw('sum(polibox_snp) as total'))
            ->join('polibox_mstr', 'polibox_trans.polibox_nbr', '=', 'polibox_mstr.polibox_nbr')
            ->whereBetween('creadate', [$month_search, $month_search_next])
            ->whereIn('status', ['ERROR', 'NG', 'ON PROGRESS'])
            ->whereYear('creadate', '>=', 2020)
            ->where('creadate', '>', '2015-04-07 21:40')
            ->where('polibox_mstr.polibox_item', '!=', '32130109')
            ->groupBy('polibox_item_nbr', 'polibox_deskripsi', 'polibox_snp', 'locfrom', 'locto')
            ->get();

        $tablenya = '';
        $no = 1;
        foreach ($data as $row) {

            $ptmstr = pt_mstr($row->polibox_item_nbr);

            // web services ld_det 
            $stok = 1;
            // ----------------------------
            $tablenya .= '<tr>
                <td align="center">' . $no . '</td>
                <td align="center">' . $row->polibox_item_nbr . '</td>
                <td align="center">' . $row->polibox_deskripsi . '</td>
                <td align="center">' . $ptmstr['lokasi_item'] . '</td>
                <td align="center">' . $ptmstr['buyer_planner'] . '</td>
                <td align="center">' . number_format($row->polibox_snp, 0) . '</td>
                <td align="right" bgcolor="orange">' . number_format($row->total, 0) . '</td>
                <td align="right" ' . (($row->total > $stok) ? 'bgcolor="red" ' : "") . '">' . number_format($row->total, 0) . '</td>
                <td align="center">' . $row->locfrom . '</td>
                <td align="center">' . $row->locto . '</td>
                <td align="center" align="right"><a href="' . route('detail-pending-barcode-whs', [base64_encode($row->polibox_item_nbr), base64_encode($monthNow), base64_encode($yearNow)]) . '" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> detail</a></td>
                </tr>';
            $no += 1;
        }

        $data1 = [
            'tablenya' => $tablenya,
            'monthNow' => $monthNow,
            'yearNow' => $yearNow,

        ];

        return response()->json($data1);
    }
}
