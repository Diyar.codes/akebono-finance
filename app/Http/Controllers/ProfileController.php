<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Session;

class ProfileController extends Controller
{
    public function editProfile() {
        $user = Auth::guard('pub_login')->user();

        return view('profile.edit-profile', compact('user'));
    }

    public function updateProfile(Request $request) {
        $request->validate([
            'username' => 'required',
        ]);

        $user = Auth::guard('pub_login')->user();

        $user->update([
            'username' => $request->username
        ]);

        toast('Update Profile Success','success')->position('top-end')->autoClose(3000)->timerProgressBar();
        return redirect()->back();
    }

    public function editPassword() {
        return view('profile.edit-password');
    }

    public function updatePassword(Request $request) {
        $user = Auth::guard('pub_login')->user();

        $request->validate([
            'old_password'     => 'required',
            'new_password'     => 'required|min:8|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/',
            'confirm_password' => 'required'
        ]);

        if(Hash::check($request->old_password, $user->password) == false) {
            alert()->error('Error','double check your password');
            return redirect()->back();
        } else if((Hash::check($request->new_password, $user->password)) == true) {
            alert()->error('Error','Old and new passwords cannot be the same');
            return redirect()->back();
        } else if($request->new_password != $request->confirm_password) {
            alert()->error('Error','The new passwords and password confirmations must match');
            return redirect()->back();
        } else {
            $user->update([
                'password'    => Hash::make($request->new_password)
            ]);

            alert()->success('Success','Password changed successfully');
            return redirect()->route('dashboard');
        }
    }
}
