<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;
use Yajra\DataTables\Facades\DataTables;

class DeliveryItemPendingController extends Controller
{
    public function index(Request $request)
    {
        $data = DB::table('supplier_DTL_suratJalan')
            ->join('Supplier_MSTR_suratJalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratJalan.no')
            ->join('SOAP_Pub_Business_Relation', 'Supplier_MSTR_suratJalan.tujuan', '=', 'SOAP_Pub_Business_Relation.ct_vd_addr')
            ->where('Supplier_MSTR_suratJalan.app_supplier', '=',  'Confirmed')
            ->where('supplier_DTL_suratJalan.qxInbound', '=',  'NOK')
            ->where('supplier_DTL_suratJalan.keterangan', 'LIKE', 'error%')
            ->whereMonth('Supplier_MSTR_suratJalan.tanggal', '=',  date('m'))
            ->whereYear('Supplier_MSTR_suratJalan.tanggal', '=',  date('Y'))
            ->whereNotNull('supplier_DTL_suratJalan.kode')
            ->select('Supplier_MSTR_suratJalan.*', 'supplier_DTL_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
            ->orderBy('Supplier_MSTR_suratJalan.no', 'ASC')
            ->get();
            // dd($data);

        if ($request->ajax()) {
            // dd($request->all());
            if (!empty($request->filter_id_supp && !empty($request->filter_no_surat))) {
                $data = DB::table('supplier_DTL_suratJalan')
                    ->join('Supplier_MSTR_suratJalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratJalan.no')
                    ->join('SOAP_Pub_Business_Relation', 'Supplier_MSTR_suratJalan.tujuan', '=', 'SOAP_Pub_Business_Relation.ct_vd_addr')
                    ->where('Supplier_MSTR_suratJalan.app_supplier', '=',  'Confirmed')
                    ->where('supplier_DTL_suratJalan.qxInbound', '=',  'NOK')
                    ->where('supplier_DTL_suratJalan.keterangan', 'LIKE', 'error%')
                    ->whereNotNull('supplier_DTL_suratJalan.kode')
                    ->where('Supplier_MSTR_suratJalan.tujuan', '=', $request->filter_id_supp)
                    ->where('Supplier_MSTR_suratJalan.no', 'like', '%' . $request->filter_no_surat . '%')
                    ->select('Supplier_MSTR_suratJalan.*', 'supplier_DTL_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('Supplier_MSTR_suratJalan.no', 'ASC')
                    ->get();
            } else if (!empty($request->filter_id_supp)) {
                $data = DB::table('supplier_DTL_suratJalan')
                    ->join('Supplier_MSTR_suratJalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratJalan.no')
                    ->join('SOAP_Pub_Business_Relation', 'Supplier_MSTR_suratJalan.tujuan', '=', 'SOAP_Pub_Business_Relation.ct_vd_addr')
                    ->where('Supplier_MSTR_suratJalan.app_supplier', '=',  'Confirmed')
                    ->where('supplier_DTL_suratJalan.qxInbound', '=',  'NOK')
                    ->where('supplier_DTL_suratJalan.keterangan', 'LIKE', 'error%')
                    ->whereNotNull('supplier_DTL_suratJalan.kode')
                    ->where('Supplier_MSTR_suratJalan.tujuan', '=', $request->filter_id_supp)
                    ->select('Supplier_MSTR_suratJalan.*', 'supplier_DTL_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('Supplier_MSTR_suratJalan.no', 'ASC')
                    ->get();
            } else if (!empty($request->filter_no_surat)) {
                $data = DB::table('supplier_DTL_suratJalan')
                    ->join('Supplier_MSTR_suratJalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratJalan.no')
                    ->join('SOAP_Pub_Business_Relation', 'Supplier_MSTR_suratJalan.tujuan', '=', 'SOAP_Pub_Business_Relation.ct_vd_addr')
                    ->where('Supplier_MSTR_suratJalan.app_supplier', '=',  'Confirmed')
                    ->where('supplier_DTL_suratJalan.qxInbound', '=',  'NOK')
                    ->where('supplier_DTL_suratJalan.keterangan', 'LIKE', 'error%')
                    ->whereNotNull('supplier_DTL_suratJalan.kode')
                    ->where('Supplier_MSTR_suratJalan.no', 'like', '%' . $request->filter_no_surat . '%')
                    ->select('Supplier_MSTR_suratJalan.*', 'supplier_DTL_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('Supplier_MSTR_suratJalan.no', 'ASC')
                    ->get();
            } else if (!empty($request->filter_month && !empty($request->filter_year))) {
                $data = DB::table('supplier_DTL_suratJalan')
                    ->join('Supplier_MSTR_suratJalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratJalan.no')
                    ->join('SOAP_Pub_Business_Relation', 'Supplier_MSTR_suratJalan.tujuan', '=', 'SOAP_Pub_Business_Relation.ct_vd_addr')
                    ->where('Supplier_MSTR_suratJalan.app_supplier', '=',  'Confirmed')
                    ->where('supplier_DTL_suratJalan.qxInbound', '=',  'NOK')
                    ->where('supplier_DTL_suratJalan.keterangan', 'LIKE', 'error%')
                    ->whereNotNull('supplier_DTL_suratJalan.kode')
                    ->whereMonth('Supplier_MSTR_suratJalan.tanggal', $request->filter_month)
                    ->whereYear('Supplier_MSTR_suratJalan.tanggal', $request->filter_year)
                    ->select('Supplier_MSTR_suratJalan.*', 'supplier_DTL_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('Supplier_MSTR_suratJalan.no', 'ASC')
                    ->get();
            } else if (!empty($request->filter_month)) {
                $data = DB::table('supplier_DTL_suratJalan')
                    ->join('Supplier_MSTR_suratJalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratJalan.no')
                    ->join('SOAP_Pub_Business_Relation', 'Supplier_MSTR_suratJalan.tujuan', '=', 'SOAP_Pub_Business_Relation.ct_vd_addr')
                    ->where('Supplier_MSTR_suratJalan.app_supplier', '=',  'Confirmed')
                    ->where('supplier_DTL_suratJalan.qxInbound', '=',  'NOK')
                    ->where('supplier_DTL_suratJalan.keterangan', 'LIKE', 'error%')
                    ->whereMonth('Supplier_MSTR_suratJalan.tanggal', $request->filter_month)
                    ->whereNotNull('supplier_DTL_suratJalan.kode')
                    ->select('Supplier_MSTR_suratJalan.*', 'supplier_DTL_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('Supplier_MSTR_suratJalan.no', 'ASC')
                    ->get();
            } else if (!empty($request->filter_year)) {
                $data = DB::table('supplier_DTL_suratJalan')
                    ->join('Supplier_MSTR_suratJalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratJalan.no')
                    ->join('SOAP_Pub_Business_Relation', 'Supplier_MSTR_suratJalan.tujuan', '=', 'SOAP_Pub_Business_Relation.ct_vd_addr')
                    ->where('Supplier_MSTR_suratJalan.app_supplier', '=',  'Confirmed')
                    ->where('supplier_DTL_suratJalan.qxInbound', '=',  'NOK')
                    ->where('supplier_DTL_suratJalan.keterangan', 'LIKE', 'error%')
                    ->whereYear('Supplier_MSTR_suratJalan.tanggal', $request->filter_year)
                    ->whereNotNull('supplier_DTL_suratJalan.kode')
                    ->select('Supplier_MSTR_suratJalan.*', 'supplier_DTL_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('Supplier_MSTR_suratJalan.no', 'ASC')
                    ->get();
            } else {
                $data = DB::table('supplier_DTL_suratJalan')
                    ->join('Supplier_MSTR_suratJalan', 'Supplier_MSTR_suratJalan.no', '=', 'supplier_DTL_suratJalan.no')
                    ->join('SOAP_Pub_Business_Relation', 'Supplier_MSTR_suratJalan.tujuan', '=', 'SOAP_Pub_Business_Relation.ct_vd_addr')
                    ->where('Supplier_MSTR_suratJalan.app_supplier', '=',  'Confirmed')
                    ->where('supplier_DTL_suratJalan.qxInbound', '=',  'NOK')
                    ->where('supplier_DTL_suratJalan.keterangan', 'LIKE', 'error%')
                    ->whereMonth('Supplier_MSTR_suratJalan.tanggal', '=',  date('m'))
                    ->whereYear('Supplier_MSTR_suratJalan.tanggal', '=',  date('Y'))
                    ->whereNotNull('supplier_DTL_suratJalan.kode')
                    ->select('Supplier_MSTR_suratJalan.*', 'supplier_DTL_suratJalan.*', 'SOAP_Pub_Business_Relation.ct_ad_name')
                    ->orderBy('Supplier_MSTR_suratJalan.no', 'ASC')
                    ->get();
            }

            if (count($data) < 1) {
                echo json_encode([
                    'status' => false,
                    'data' => 'data kosong'
                ]);
            } else {
                echo json_encode([
                    'status' => true,
                    'data' => $data
                ]);
            }
        } else {
            return view('delivery-item-pending.index', ['data' => $data]);
        }
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $cek = 0;
        $jumlah = $request->input('jumlah');
        // dd($jumlah);
        if(intval($jumlah) == 0){
            alert()->error('Error', 'enter one of the forms');
            return redirect()->back();
        }

        for($i = 0; $i <= $jumlah; $i++){
            if(!empty($request->input('check' . $i))){
                $cek++;
            }
        }

        if($cek == 0){
            alert()->error('Error', 'enter one check the forms');
            return redirect()->back();
        }

        for ($i = 1; $i <= $jumlah; $i++) {
            $check = $request->input('check' . $i);
            $no = $request->input('no' . $i); #no / kd
            $kode = $request->input('kode' . $i); #kode
            $qty = $request->input('qty' . $i); #qty / act_qty
            $tgl = $request->input('tgl' . $i);
            $keterangan = "diambil dari session";

            if ($check != "") {
                $qTanggal = DB::table('Supplier_MSTR_suratJalan')
                    ->where('no', $no)
                    ->first();

                $tanggal = date('Y-m-d', strtotime($qTanggal->tanggal));
                $tujuan = $qTanggal->tujuan;

                $qlokasi = DB::table('supplier_item_master')
                    ->where('item_no', $kode)
                    ->where('supplier_id', $tujuan)
                    ->first();

                if ($qlokasi == "") {
                    $lokfrom = '';
                    $lokto = '';
                    $item = '';
                }else{
                    $lokfrom = $qlokasi->location_from;
                    $lokto = $qlokasi->location_to;
                    $item = $qlokasi->item_no;
                }

                if ($item != '') {
                    if ($kode && $tanggal && $qty && $no && $lokfrom && $lokto) {
                        $cek = DB::table('supplier_DTL_suratJalan')->where('no', $no)->where('kode', $kode)->update([
                            'act_qty' => $qty,
                            'qxInbound' => 'OK',
                            'keterangan' => $keterangan
                        ]);
                        if (!$cek) {
                            echo 'Failed transfer single item';
                        }
                    } else {
                        $cek = DB::table('supplier_DTL_suratJalan')->where('no', $no)->where('kode', $kode)->update([
                            'act_qty' => $qty,
                            'qxInbound' => 'NOK',
                            'keterangan' => $keterangan
                        ]);
                        if (!$cek) {
                            echo "Failed Transfer Single Item line " . $kode . "";
                        }
                    }

                    alert()->success('Success', 'Check delivery item pending have been changed successfully');
                    return redirect()->route('delivery-item-pending');
                } else {
                    DB::table('supplier_DTL_suratJalan')->where('no', $no)->where('kode', $kode)->update([
                        'qxInbound' => 'NOK',
                        'keterangan' => 'Local Item'
                    ]);
                }
            }
        }
        alert()->success('Success', 'Check delivery item pending have been changed successfully');
        return redirect()->route('delivery-item-pending');
    }
}
