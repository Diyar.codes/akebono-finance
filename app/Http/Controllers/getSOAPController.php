<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\SOAPPubBusinessRelation;
use App\SOAPPtMstr;
use App\SOAPPtMstrByDesc;
class getSOAPController extends Controller
{
    public function getSupplier(Request $request) {
        // if ($request->ajax()) {

            // $starttime = microtime(true);
            $url = "https://supplier.akebono-astra.co.id/wsatdw/master_view_test.php";

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $headers = array(
                "Accept: application/xml",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $resp = curl_exec($curl);
            curl_close($curl);

            $find = "&lt;/soapenv:Envelope&gt;";
            $arr = explode($find, $resp, 2);
            $first = $arr[1];

            $env = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
            $arrfind = ["&amp;","&quot;","&gt;","&lt;","<pre>", "</pre>", "</SOAP-ENV:Body>","</SOAP-ENV:Envelope>","<SOAP-ENV:Body>", $env];
            $arrrep = ["&","\"",">","<", "", "","", "","", ""];
            $rep = str_replace($arrfind,$arrrep,$first);

            Storage::put('supplier.xml', $rep);

            libxml_use_internal_errors(TRUE);

            $objXmlDocument = simplexml_load_file(storage_path('app')."/supplier.xml");

            if ($objXmlDocument === FALSE) {
                echo "There were errors parsing the XML file.\n";
            foreach(libxml_get_errors() as $error) {
                echo $error->message;
            }
            exit;
            }

            $objJsonDocument = json_encode($objXmlDocument);
            $arrOutput = json_decode($objJsonDocument, TRUE);

            // $time = microtime(true) - $starttime/60;

            // return $time;

            $supplier = $arrOutput['ttSupplier']['ttSupplierRow'];
            
            return DataTables::of($supplier)
            ->addColumn('action', function ($data) {
                $b = $data['ct_vd_type'];
                if(!empty($b)) {
                    $a = $data['ct_vd_type'];
                } else {
                    $a = '-';
                }
                $button = '<a href="javascript:void(0)" data-type="'. $a .'" data-code="' . $data['ct_vd_addr'] . '" data-name="' . $data['ct_ad_name'] . '" class="btn btn-sm btn-warning" id="selectbusiness"><i class="fa fa-check"></i> Click</a>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
        // }
    }

    public function getSupplierLocal(Request $request) {
        $data = SOAPPubBusinessRelation::all();

        return DataTables::of($data)
            ->addColumn('action', function ($data) {
                $ct_vd_type = $data['ct_vd_type'];
                if(!empty($ct_vd_type)) {
                    $ct_vd_typeend = $data['ct_vd_type'];
                } else {
                    $ct_vd_typeend = '-';
                }

                $ct_vd_addr = $data['ct_vd_addr'];
                if(!empty($ct_vd_addr)) {
                    $ct_vd_addrend = $data['ct_vd_addr'];
                } else {
                    $ct_vd_addrend = '-';
                }

                $ct_ad_name = $data['ct_ad_name'];
                if(!empty($ct_ad_name)) {
                    $ct_ad_nameend = $data['ct_ad_name'];
                } else {
                    $ct_ad_nameend = '-';
                }

                $button = '<a href="javascript:void(0)" data-type="'. $ct_vd_typeend .'" data-code="' . $ct_vd_addr . '" data-name="' . $ct_ad_name . '" class="btn btn-sm btn-warning" id="selectbusiness"><i class="fa fa-check"></i> Click</a>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    // public function getSoapPtMstrLocal(Request $request) {
    //     $item_master = DB::table('SOAP_pt_mstr')->get();

    //     $main='';
    //     $hitung = 0;
    //     foreach($item_master as $item) {
    //         if($item->deskripsi2 == "-"){
    //             $cek_desc = $item->deskripsi1;
    //         }else{
    //             $cek_desc = $item->deskripsi1.' - '.$item->deskripsi2;
    //         }
    //         $main .= '<tr>
    //                         <td>'.$item->item_number.'</td>
    //                         <input type="hidden" name="item_no['.$hitung.']" id="item_no['.$hitung.']" value="'.$item->item_number.'" >
    //                         <input type="hidden" name="desc['.$hitung.']" id="desc['.$hitung.']" value="'.$cek_desc.'" >
    //                         <td>'.$cek_desc.'</td>
    //                         <td><a class="btn btn-warning" data-itemnumber="'. $item->item_number .'" data-desc="'. $cek_desc .'" data-um="'. $item->um .'" id="selectptmstr" style="color:white"><i class="fa fa-check"></i> Click</a></td>
    //                     </tr>';
    //     }
    //     $data = [
    //         'main' => $main,
    //     ];

    //     return response($data);
    // }

    public function getSoapPtMstrLocal(Request $request) {
        // $data = SOAPPtMstr::all();
        $data = SOAPPtMstrByDesc::all();

        return DataTables::of($data)
            ->addColumn('action', function ($data) {
                if($data['deskripsi2'] == "-"){
                    $cek_desc = $data['deskripsi1'];
                }else{
                    $cek_desc = $data['deskripsi1'].' - '.$data['deskripsi2'];
                }

                if(!empty($data['item_number'])) {
                    $item_numberend = $data['item_number'];
                } else {
                    $item_numberend = '-';
                }

                if(!empty($data['um'])) {
                    $umend = $data['um'];
                } else {
                    $umend = '-';
                }

                $button = '<a href="javascript:void(0)" data-itemnumber="'. $item_numberend .'" data-desc="' . $cek_desc . '" data-um="' . $umend . '" class="btn btn-sm btn-warning" id="selectptmstr"><i class="fa fa-check"></i> Click</a>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
