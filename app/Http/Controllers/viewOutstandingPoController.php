<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Helpers\lpb_helper;
class viewOutstandingPoController extends Controller
{

    public function index(){

        $hak    = Auth::guard('pub_login')->user()->previllage;
        $supp   = Auth::guard('pub_login')->user()->kode_supplier;
        $url    = 'https://supplier.akebono-astra.co.id/wsatdw/po_mstr.php';
        $get_po = po_mstr($url);
        $i = 0;
        if ($hak == 'Admin' || $hak == 'WHS'){
            
            foreach($get_po as $c){
                if($c['po_status'] != 'c'){
                    $tangkap[$i++] = array(
                        'po_number' => $c['po_number'],
                        'POdue_date' => $c['POdue_date'],
                        'po_status' => $c['po_status'],
                        'kode_supplier' => $c['kode_supplier'],
                        'pocurr' => $c['pocurr'],
                    );
                }
            }
		}else{
            foreach($get_po as $c){
                if($c['po_status'] != 'c' && $c['kode_supplier'] == $supp){
                    $tangkap[$i++] = array(
                        'po_number' => $c['po_number'],
                        'POdue_date' => $c['POdue_date'],
                        'po_status' => $c['po_status'],
                        'kode_supplier' => $c['kode_supplier'],
                        'pocurr' => $c['pocurr'],
                    );
                }
            }
		}

        $tablenya = '';
        if(count($tangkap) > 0){
            foreach($tangkap as $sql){
                $now        = time(); // ambil waktu sekarang
                $PO_date    = strtotime($sql['POdue_date']); //convert strtotime
                $datediff   = $now - $PO_date;
                $leadTime   = floor($datediff/(60*60*24));
                // $leadTime   = 8;
                if ($leadTime < 0) {
                        $leadTime = 0;
				}
                if ($leadTime > 7){
                    $color = 'red';
                }else{
                    $color = 'black';
                }

                $rev = DB::table('po_trans_mstr')
                ->select('po_revisi')
                ->where('po_nbr',$sql['po_number'])
                ->max('po_revisi');
                
                $data_rev = DB::table('po_trans_mstr')
                ->select('po_app2_effdate')
                ->where('po_nbr',$sql['po_number'])
                ->where('po_revisi',$rev)->first();
                
                if(empty($data_rev)){
                    $tgl = '1945-08-17';
                }else{
                    $tgl = $data_rev->po_app2_effdate;
                }

                $revisi = $rev;
                $bulan  = date('n',strtotime($tgl));
                $tahun  = date('Y',strtotime($tgl));
                $cari_supplier = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr' , $sql['kode_supplier'])->first();
                $user   = "$bulan$tahun";
                $tablenya .= '
                <tr>
                    <th><font color="'.$color.'">'.$sql['po_number'].'</font></th>
                    <th><font color="'.$color.'">'.$cari_supplier->ct_vd_addr.'</font></th>
                    <th><font color="'.$color.'">'.$cari_supplier->ct_ad_name.'</font></th>
                    <th><font color="'.$color.'">'.date('d-F-Y',strtotime($sql['POdue_date'])).'</font></th>
                    <th><font color="'.$color.'">'.number_format($leadTime).'</font></th>
                    <th><font color="'.$color.'">'.$sql['pocurr'].'</font></th>
                    <th><font color="'.$color.'"><a href="/detail-view-outstanding-po/'.base64_encode($sql['po_number']).'/'.base64_encode($cari_supplier->ct_vd_addr).'/'.base64_encode($cari_supplier->ct_ad_name).'" target="_blank"> Detail</a></font></th>
                    <th><a href="http://purchasing.akebono-astra.co.id/po/'.$user.'/'.$sql['po_number'].'-'.$revisi.'.pdf" target="_blank" class="btn btn-warning"><font color="black"><i class="fas fa-print"></i> <b>Print</b><font></a></th>
                </tr>'; 
            }
        }else{
            $tablenya .= ''; 
        }
        return view('view-outstanding-po.index',compact('tablenya'));
    }

    public function search(Request $request){
        $bulan      = $request->get('bulan');
        $tahun      = $request->get('tahun');
        $hak        = Auth::guard('pub_login')->user()->previllage;
        $supp       = Auth::guard('pub_login')->user()->kode_supplier;
        $url        = 'https://supplier.akebono-astra.co.id/wsatdw/po_mstr.php';
        $get_po     = po_mstr($url);
        $i          = 0;
        if ($hak == 'Admin' || $hak == 'WHS'){
            
            if($bulan != "" && $tahun != ""){
                foreach($get_po as $c){
                    if($c['po_status'] != 'c' && date('m',strtotime($c['POdue_date'])) == $bulan && date('Y',strtotime($c['POdue_date'])) == $tahun){
                        $tangkap[$i++] = array(
                            'po_number' => $c['po_number'],
                            'POdue_date' => $c['POdue_date'],
                            'po_status' => $c['po_status'],
                            'kode_supplier' => $c['kode_supplier'],
                            'pocurr' => $c['pocurr'],
                        );
                    }
                }
            }else if($bulan == "" && $tahun == ""){
                foreach($get_po as $c){
                    if($c['po_status'] != 'c'){
                        $tangkap[$i++] = array(
                            'po_number' => $c['po_number'],
                            'POdue_date' => $c['POdue_date'],
                            'po_status' => $c['po_status'],
                            'kode_supplier' => $c['kode_supplier'],
                            'pocurr' => $c['pocurr'],
                        );
                    }
                }
            }else if($bulan == ""){
                foreach($get_po as $c){
                    if($c['po_status'] != 'c' && date('Y',strtotime($c['POdue_date'])) == $tahun){
                        $tangkap[$i++] = array(
                            'po_number' => $c['po_number'],
                            'POdue_date' => $c['POdue_date'],
                            'po_status' => $c['po_status'],
                            'kode_supplier' => $c['kode_supplier'],
                            'pocurr' => $c['pocurr'],
                        );
                    }
                }
            }else if($tahun == ""){
                foreach($get_po as $c){
                    if($c['po_status'] != 'c' && date('m',strtotime($c['POdue_date'])) == $bulan){
                        $tangkap[$i++] = array(
                            'po_number' => $c['po_number'],
                            'POdue_date' => $c['POdue_date'],
                            'po_status' => $c['po_status'],
                            'kode_supplier' => $c['kode_supplier'],
                            'pocurr' => $c['pocurr'],
                        );
                    }
                }
            }else{
                foreach($get_po as $c){
                    if($c['po_status'] != 'c'){
                        $tangkap[$i++] = array(
                            'po_number' => $c['po_number'],
                            'POdue_date' => $c['POdue_date'],
                            'po_status' => $c['po_status'],
                            'kode_supplier' => $c['kode_supplier'],
                            'pocurr' => $c['pocurr'],
                        );
                    }
                }
            }
            
            
		}else{
            if($bulan != "" && $tahun != ""){
                foreach($get_po as $c){
                    if($c['po_status'] != 'c' && $c['kode_supplier'] == $supp && date('m',strtotime($c['POdue_date'])) == $bulan && date('Y',strtotime($c['POdue_date'])) == $tahun){
                        $tangkap[$i++] = array(
                            'po_number' => $c['po_number'],
                            'POdue_date' => $c['POdue_date'],
                            'po_status' => $c['po_status'],
                            'kode_supplier' => $c['kode_supplier'],
                            'pocurr' => $c['pocurr'],
                        );
                    }
                }
            }else if($bulan == "" && $tahun == ""){
                foreach($get_po as $c){
                    if($c['po_status'] != 'c' && $c['kode_supplier'] == $supp){
                        $tangkap[$i++] = array(
                            'po_number' => $c['po_number'],
                            'POdue_date' => $c['POdue_date'],
                            'po_status' => $c['po_status'],
                            'kode_supplier' => $c['kode_supplier'],
                            'pocurr' => $c['pocurr'],
                        );
                    }
                }
            }else if($bulan == ""){
                foreach($get_po as $c){
                    if($c['po_status'] != 'c' && $c['kode_supplier'] == $supp && date('Y',strtotime($c['POdue_date'])) == $tahun){
                        $tangkap[$i++] = array(
                            'po_number' => $c['po_number'],
                            'POdue_date' => $c['POdue_date'],
                            'po_status' => $c['po_status'],
                            'kode_supplier' => $c['kode_supplier'],
                            'pocurr' => $c['pocurr'],
                        );
                    }
                }
            }else if($tahun == ""){
                foreach($get_po as $c){
                    if($c['po_status'] != 'c' && $c['kode_supplier'] == $supp && date('m',strtotime($c['POdue_date'])) == $bulan){
                        $tangkap[$i++] = array(
                            'po_number' => $c['po_number'],
                            'POdue_date' => $c['POdue_date'],
                            'po_status' => $c['po_status'],
                            'kode_supplier' => $c['kode_supplier'],
                            'pocurr' => $c['pocurr'],
                        );
                    }
                }
            }else{
                foreach($get_po as $c){
                    if($c['po_status'] != 'c' && $c['kode_supplier'] == $supp){
                        $tangkap[$i++] = array(
                            'po_number' => $c['po_number'],
                            'POdue_date' => $c['POdue_date'],
                            'po_status' => $c['po_status'],
                            'kode_supplier' => $c['kode_supplier'],
                            'pocurr' => $c['pocurr'],
                        );
                    }
                }
            }
		}

        $tablenya = '';
        if(count($tangkap) > 0){
            foreach($tangkap as $sql){
                $now        = time(); // ambil waktu sekarang
                $PO_date    = strtotime($sql['POdue_date']); //convert strtotime
                $datediff   = $now - $PO_date;
                $leadTime   = floor($datediff/(60*60*24));
                // $leadTime   = 8;
                if ($leadTime < 0) {
                        $leadTime = 0;
				}
                if ($leadTime > 7){
                    $color = 'red';
                }else{
                    $color = 'black';
                }

                $rev = DB::table('po_trans_mstr')
                ->select('po_revisi')
                ->where('po_nbr',$sql['po_number'])
                ->max('po_revisi');
                
                $data_rev = DB::table('po_trans_mstr')
                ->select('po_app2_effdate')
                ->where('po_nbr',$sql['po_number'])
                ->where('po_revisi',$rev)->first();
                
                if(empty($data_rev)){
                    $tgl = '1945-08-17';
                }else{
                    $tgl = $data_rev->po_app2_effdate;
                }

                $revisi = $rev;
                $bulan  = date('n',strtotime($tgl));
                $tahun  = date('Y',strtotime($tgl));
                $cari_supplier = DB::table('SOAP_Pub_Business_Relation')->where('ct_vd_addr' , $sql['kode_supplier'])->first();
                $user   = "$bulan$tahun";
                $tablenya .= '
                <tr>
                    <th><font color="'.$color.'">'.$sql['po_number'].'</font></th>
                    <th><font color="'.$color.'">'.$cari_supplier->ct_vd_addr.'</font></th>
                    <th><font color="'.$color.'">'.$cari_supplier->ct_ad_name.'</font></th>
                    <th><font color="'.$color.'">'.date('d-F-Y',strtotime($sql['POdue_date'])).'</font></th>
                    <th><font color="'.$color.'">'.number_format($leadTime).'</font></th>
                    <th><font color="'.$color.'">'.$sql['pocurr'].'</font></th>
                    <th><font color="'.$color.'"><a href="/detail-view-outstanding-po/'.base64_encode($sql['po_number']).'/'.base64_encode($cari_supplier->ct_vd_addr).'/'.base64_encode($cari_supplier->ct_ad_name).'" target="_blank"> Detail</a></font></th>
                    <th><a href="http://purchasing.akebono-astra.co.id/po/'.$user.'/'.$sql['po_number'].'-'.$revisi.'.pdf" target="_blank" class="btn btn-warning"><font color="black"><i class="fas fa-print"></i> <b>Print</b><font></a></th>
                </tr>'; 
            }
        }else{
            $tablenya .= ''; 
        }

        return response($tablenya);
    }

    public function create(){
        //
    }

    public function detail($po,$kd,$nm){
        $ponya  = base64_decode($po);
        $hak    = Auth::guard('pub_login')->user()->previllage;
        getPODetailItem($ponya);
        
        $data =  DB::table('SOAP_po_detail')
        ->where('no_po',$ponya)
        ->orderBy('line','ASC')
        ->get();

        $tablenya = '';
        if(count($data) > 0){
            foreach($data as $sql){

                getDataPtMstr($sql->item_number);
                // $z = ['ttItem']['ttItemRow'];
                $data_item = DB::table('SOAP_pt_mstr')
                        ->where('item_number', $sql->item_number)
                        ->first();
            
                $b = $sql->line;
                $c = $sql->item_number;
                $e = $sql->qty_po;
                $f = $sql->qty_receive;
                //$f = odbc_result($rs_cek_rcvd,1);
                $g = $sql->po_um;
                $desc1 = $data_item->deskripsi1;
                $desc2 = $data_item->deskripsi2;
                $desc = $desc1." ".$desc2;
                
                if ($desc == " "){
                    $desc = $sql->item_deskripsi;
                }
                
                $qty_outstanding = $e - $f;	

                if ($qty_outstanding < 0){
                        $qty_outstanding = 0;
                        $class = 'bgcolor="white">';
                }else{
                    $class = '';
                }
                if ($qty_outstanding > 0){	
                    $tablenya .= '
                        <tr '.$class.'>
                            <td><font color="red">'.$b.'</font></td>
                            <td><font color="red">'.$c.'</font></td>
                            <td><font color="red">'.$desc.'</font></td>
                            <td><font color="red">'.$data_item->desc_type.'</font></td>
                            <td><font color="red">'.$g.'</font></td>
                            <td><font color="red">'.number_format($e).'</font></td>
                            <td><font color="red">'.number_format($f).'</font></td>
                            <td><font color="red">'.number_format($qty_outstanding).'</font></td>';
                            $persenReceived = floor(($f / $e) * 100);
                            if ($persenReceived > 100){
                                $persenReceived = 100;
                            }
                            $persenOutstanding = 100 - $persenReceived;
                            $tablenya .= '<td><table style="width:100%; border: red 1px solid;"><tr>';
                            if ($persenReceived <= 0){
                                    $tablenya .=  '<td style="background-color:yellow; height:15px; width:100%;"><b> 0% </b></td>';
                            }else{						
                                    $tablenya .=  '<td style="background-color:lightgreen; height:15px; width:'.$persenReceived.'%;"><b>'.$persenReceived.'%</b></td>';
                                    
                                if ($persenReceived <> 100){
                                    $tablenya .=  '<td style="background-color:yellow; height:15px; width:'.$persenOutstanding.'%;"></td>';
                                }
                            }
                    $tablenya .= '</tr></table></td>'; 
                }
                $tablenya .= '</tr>'; 
            }
        }else{
            $tablenya .= ''; 
        }

        if ($hak == 'Admin') { 
			$detail_judul = $ponya ." / ".$vendName." (".$vend.")";
		}else{
            $detail_judul = $ponya;
        }
        
        return view('view-outstanding-po.detail',compact('tablenya','detail_judul'));
    }

    public function store(Request $request){
        //
    }

    public function show($id){
        //
    }

    public function edit($id){
        //
    }

    public function update(Request $request, $id){
        //
    }

    public function destroy($id){
        //
    }
}
