<?php

namespace App\Http\Controllers;

use App\PackingSlip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Portal_MSTR;
use Carbon\Carbon;
use App\PortalDet;
use Artisan;

class PackingSlipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hak    = Auth::guard('pub_login')->user()->previllage;
        $supp   = Auth::guard('pub_login')->user()->kode_supplier;
        // $month  = date('m');
        $month = date('m');
        $year   = date('Y');
        if ($hak == 'Admin' OR $hak== 'WHS'){
            $sql_cek_mfg = DB::table('portal_mstr')
                   ->select('portal_tr_id','portal_po_nbr','portal_ps_nbr','portal_vend','portal_dlv_date','portal__char01','portal_cycle')
                   ->whereMonth('portal_dlv_date', $month)
                   ->whereYear('portal_dlv_date', $year)
                   ->get();
        }
        if($hak == 'Supplier'){
            $sql_cek_mfg = DB::table('portal_mstr')
                   ->select('portal_tr_id','portal_po_nbr','portal_ps_nbr','portal_vend','portal_dlv_date','portal__char01','portal_cycle')
                   ->whereMonth('portal_dlv_date', $month)
                   ->whereYear('portal_dlv_date', $year)
                   ->where('portal_vend',$supp)
                   ->get();
        }
        $tablenya = '';
        if(count($sql_cek_mfg) > 0){
            foreach($sql_cek_mfg as $sql){
                $cari_supp = DB::table('SOAP_pub_business_relation')->where('ct_vd_addr',$sql->portal_vend)->first();
                $tablenya .= '
                <tr>
                    <th>'.$sql->portal_tr_id.'</th>
                    <th>'.$sql->portal_po_nbr.'</th>
                    <th>'.$sql->portal_ps_nbr.'</th>
                    <th>'.date('d-F-Y',strtotime($sql->portal_dlv_date)).'</th>
                    <th>'.$cari_supp->ct_vd_addr.'</th>
                    <th>'.$cari_supp->ct_ad_name.'</th>
                    <th>'.$sql->portal_cycle.'</th>
                    <th>'.$sql->portal__char01.'</th>
                    <th><a href="/view-detail-packing-slip/'.base64_encode($sql->portal_tr_id).'" class="btn btn-info"><i class="fas fa-eye"></i> Detail</a></th>
                </tr>';
            }
        }else{
            $tablenya .= '';
        }

        return view('packing-slip.index',compact('tablenya'));
    }

    public function search_ps(Request $request){
        $po     = $request->get('item');
        $tr     = $request->get('tr_id');
        $ps     = $request->get('ps');
        $date   = $request->get('dlv_date');
        $min    = date('Y-m-d', strtotime( $date . " -2 days"));
        $plus   = date('Y-m-d', strtotime( $date . " +4 days"));

        if($po != ""){
            $data = DB::table('portal_mstr')
                   ->select('portal_tr_id','portal_po_nbr','portal_ps_nbr','portal_vend','portal_dlv_date','portal__char01','portal_cycle')
                   ->where('portal_po_nbr',$po)
                   ->get();
        }else if($tr != ""){
            $data = DB::table('portal_mstr')
                   ->select('portal_tr_id','portal_po_nbr','portal_ps_nbr','portal_vend','portal_dlv_date','portal__char01','portal_cycle')
                   ->where('portal_tr_id',$tr)
                   ->get();
        }else if($ps != ""){
            $data = DB::table('portal_mstr')
                   ->select('portal_tr_id','portal_po_nbr','portal_ps_nbr','portal_vend','portal_dlv_date','portal__char01','portal_cycle')
                   ->where('portal_ps_nbr',$ps)
                   ->get();
        }else if($date != ""){
            $data = DB::table('portal_mstr')
                   ->select('portal_tr_id','portal_po_nbr','portal_ps_nbr','portal_vend','portal_dlv_date','portal__char01','portal_cycle')
                //    ->whereBetween('portal_dlv_date', [$min, $plus])
                    ->where('portal_dlv_date',$date)
                   ->get();
        }else{
            $data = DB::table('portal_mstr')
                   ->select('portal_tr_id','portal_po_nbr','portal_ps_nbr','portal_vend','portal_dlv_date','portal__char01','portal_cycle')
                   ->whereMonth('portal_dlv_date', date('m'))
                   ->whereYear('portal_dlv_date', date('Y'))
                   ->get();
        }

        $tablenya = '';
        if(count($data) > 0){
            foreach($data as $sql){
                $cari_supp = DB::table('SOAP_pub_business_relation')->where('ct_vd_addr',$sql->portal_vend)->first();
                $tablenya .= '
                <tr>
                    <th>'.$sql->portal_tr_id.'</th>
                    <th>'.$sql->portal_po_nbr.'</th>
                    <th >'.$sql->portal_ps_nbr.'</th>
                    <th >'.date('d-F-Y',strtotime($sql->portal_dlv_date)).'</th>
                    <th >'.$cari_supp->ct_vd_addr.'</th>
                    <th >'.$cari_supp->ct_ad_name.'</th>
                    <th>'.$sql->portal_cycle.'</th>
                    <th >'.$sql->portal__char01.'</th>
                    <th ><a href="/view-detail-packing-slip/'.base64_encode($sql->portal_tr_id).'" class="btn btn-info"><i class="fas fa-eye"></i> Detail</a></th>
                </tr>';
            }
        }else{
            $tablenya .= '';
        }
        return response($tablenya);
    }

    public function vps_detail($id){

        $atas = Portal_MSTR::where('portal_tr_id',base64_decode($id))->first();
        $data = DB::table('portal_mstr')
                   ->join('portald_det','portald_tr_id','=','portal_tr_id')
                   ->select('portald_tr_id','portal_ps_nbr','portal_po_nbr','portald_qty_order','portald_qty_ship','portald_qty_confirm','portald_dlv_date','portald_confirm_date','portald_part')
                   ->where('portal_tr_id',base64_decode($id))
                   ->get();

        $tablenya = '';
        // dd($data);

        if(count($data) > 0){
            foreach($data as $sql){

                $item = pt_mstr($sql->portald_part);
                // $item = DB::table('SOAP_pt_mstr')->where('item_number',$sql->portald_part)->first();

                if($sql->portald_confirm_date == null){
                    $datenya = '-';
                }else{
                    $datenya = date('d-F-Y',strtotime($sql->portald_confirm_date));
                }

                $tablenya .= '
                <tr>
                    <td>'.$sql->portald_part.'</td>
                    <td>'.$item['deskripsi1'].'</td>
                    <td>'.number_format($sql->portald_qty_ship).'</td>
                    <td>'.number_format($sql->portald_qty_confirm).'</td>
                    <td>'.date('d-F-Y',strtotime($sql->portald_dlv_date)).'</td>
                    <td>'.$datenya.'</td>
                </tr>';
            }
        }else{
            $tablenya .= '';
        }
        return view('packing-slip.vps_detail',compact('atas','tablenya'));
    }

    public function packingSlipDN(Request $request)
    {
        $min = date('Y-m-d', strtotime('-7 day'));
        $plus = date('Y-m-d', strtotime('+7 day'));
        $supp = Auth::guard('pub_login')->user()->kode_supplier;
        if($request->ajax()) {
            $dn     = $request->get('dn_no');
            $month  = $request->get('bulan');
            $year   = $request->get('tahun');

            if($dn == ""){

                if($month == ""){
                    $data = DB::select("SELECT DISTINCT dn_tr_id,dn_po_nbr,dn_vend,dn_cycle_arrival,dn_cycle_order,dn_arrival_date,dn_order_date from dn_mstr,dnd_det WHERE dn_tr_id=dnd_tr_id AND  dn_vend = '$supp' AND year(dn_arrival_date) = '$year' and dn_po_nbr=dnd_po_nbr ORDER BY dn_arrival_date DESC");
               }else if($year == ""){
                    $data = DB::select("SELECT DISTINCT dn_tr_id,dn_po_nbr,dn_vend,dn_cycle_arrival,dn_cycle_order,dn_arrival_date,dn_order_date from dn_mstr,dnd_det WHERE dn_tr_id=dnd_tr_id AND  dn_vend = '$supp' AND month(dn_arrival_date) = '$month' and dn_po_nbr=dnd_po_nbr ORDER BY dn_arrival_date DESC");
               }else if($month != "" && $year != ""){
                    $data = DB::select("SELECT DISTINCT dn_tr_id,dn_po_nbr,dn_vend,dn_cycle_arrival,dn_cycle_order,dn_arrival_date,dn_order_date from dn_mstr,dnd_det WHERE dn_tr_id=dnd_tr_id AND  dn_vend = '$supp' AND month(dn_arrival_date) = '$month' AND year(dn_arrival_date) = '$year' and dn_po_nbr=dnd_po_nbr ORDER BY dn_arrival_date DESC");
               }else{
                   $data = 0;
               }
            }else{
                
                if($month == "" && $year == ""){
                    $data = DB::select("SELECT DISTINCT dn_tr_id,dn_po_nbr,dn_vend,dn_cycle_arrival,dn_cycle_order,dn_arrival_date,dn_order_date from dn_mstr,dnd_det WHERE dn_tr_id=dnd_tr_id AND dn_tr_id = '$dn' AND dn_vend = '$supp' and dn_po_nbr=dnd_po_nbr ORDER BY dn_arrival_date DESC");
                }else if($month != "" && $year != ""){
                    $data = DB::select("SELECT DISTINCT dn_tr_id,dn_po_nbr,dn_vend,dn_cycle_arrival,dn_cycle_order,dn_arrival_date,dn_order_date from dn_mstr,dnd_det WHERE dn_tr_id=dnd_tr_id AND dn_tr_id = '$dn' AND dn_vend = '$supp' AND month(dn_arrival_date) = '$month' AND year(dn_arrival_date) = '$year' and dn_po_nbr=dnd_po_nbr ORDER BY dn_arrival_date DESC");
                }else if($month == ""){
                    $data = DB::select("SELECT DISTINCT dn_tr_id,dn_po_nbr,dn_vend,dn_cycle_arrival,dn_cycle_order,dn_arrival_date,dn_order_date from dn_mstr,dnd_det WHERE dn_tr_id=dnd_tr_id AND dn_tr_id = '$dn' AND dn_vend = '$supp' AND year(dn_arrival_date) = '$year' and dn_po_nbr=dnd_po_nbr ORDER BY dn_arrival_date DESC");
                }else if($year == ""){
                    $data = DB::select("SELECT DISTINCT dn_tr_id,dn_po_nbr,dn_vend,dn_cycle_arrival,dn_cycle_order,dn_arrival_date,dn_order_date from dn_mstr,dnd_det WHERE dn_tr_id=dnd_tr_id AND dn_tr_id = '$dn' AND dn_vend = '$supp' AND month(dn_arrival_date) = '$month' and dn_po_nbr=dnd_po_nbr ORDER BY dn_arrival_date DESC");
                }else{
                   $data = 0;
                }
            }
            
            $urut = 1;
            $tablenya = '';

            if(count($data) > 0){
                $total_qty_dn = 0;

                foreach ($data as $d) {

                    $po = $d->dn_po_nbr;
                    $dn = $d->dn_tr_id;

                    $cek_data_po = DB::table('SOAP_po_mstr')
                    ->select('po_number','POdue_date','po_status')
                    ->where('po_domain','AAIJ')
                    ->where('po_number',$po)
                    ->first();

                    $status = $cek_data_po->po_status;
                    if ($status!="C"){
                        $status='Opened';
                    }else{
                        $status='Closed';
                    }
                    $dnd = DB::table('dnd_det')
                        ->select('dnd_qty_order')
                        ->where('dnd_tr_id',$d->dn_tr_id)
                        ->where('dnd_po_nbr',$d->dn_po_nbr)
                        ->first();
                    $qty_dn = $dnd->dnd_qty_order;
                    $cek_item = DB::table('dn_item_supp')
                        ->select('back_no','pcs_kanban','pallet_kanban')
                        ->where('kd_item',$po)
                        ->first();
                    if(empty($cek_item->pcs_kanban)){
                        $qty_kanban = 1;
                    }else{
                        $qty_kanban = $cek_item->pcs_kanban;
                    }

                    if ($qty_kanban == ''){
                        $qty_DN_pcs = ($qty_dn*1);
                    }else{
                        $qty_DN_pcs=($qty_kanban*$qty_dn);
                    }

                    $total_qty_dn = $total_qty_dn+$qty_DN_pcs;

                    $cek_data_DO =  DB::table('portald_det')
                            ->where('portald_no_dn', $dn)
                            ->where('portald_po_nbr', $po)
                            ->sum('portald_qty_ship');

                    $qty_DO = $cek_data_DO;

                    if ($qty_DO != $total_qty_dn){
                        $status_dn='Opened';
                        $class = "";
                    }else{
                        $status_dn='Closed';
                        $class = "style='background-color:yellow'";
                    }
                    if($status_dn = 'Opened'){
                        $url = '/delivery-order-dn/'.base64_encode($d->dn_tr_id).'/'.base64_encode($d->dn_po_nbr).'';
                        $act = '<a href="'.$url.'" class="btn btn-warning"><font color="black"><i class="fas fa-truck"></i> <b>Ship</b><font></a>';
                    }else{
                        $act = '-';
                    }
                    $tablenya .= '
                            <tr>
                                <td>'.$urut++.'</td>
                                <td>'.$d->dn_tr_id.'</td>
                                <td>'.$d->dn_po_nbr.'</td>
                                <td>'.$d->dn_cycle_order.'</td>
                                <td>'.date('d-F-Y',strtotime($d->dn_order_date)).'</td>
                                <td>'.$d->dn_cycle_arrival.'</td>
                                <td>'.date('d-F-Y',strtotime($d->dn_arrival_date)).'</td>
                                <td>'.date('d-F-Y',strtotime($cek_data_po->POdue_date)).'</td>
                                <td>'.$status.'</td>
                                <td>'.$status_dn.'</td>
                                <td>'.$act.'</td>
                            </tr>';
                }
            }else{
                $tablenya .= '';
            }


            return response($tablenya);
        }else{
            $urlpo = "https://supplier.akebono-astra.co.id/wsatdw/po_mstr.php";
            // getDataPO($urlpo);
            $month = date('m');
            $year = date('Y');
            $data = DB::select("SELECT DISTINCT dn_tr_id,dn_po_nbr,dn_vend,dn_cycle_arrival,dn_cycle_order,dn_arrival_date,dn_order_date from
            dn_mstr,dnd_det WHERE dn_tr_id=dnd_tr_id AND  dn_vend = '$supp' AND dn_arrival_date BETWEEN '$min' and '$plus' and dn_po_nbr=dnd_po_nbr ORDER BY dn_arrival_date DESC");
            $urut = 1;
            $tablenya2 = '';

            $total_qty_dn = 0;
            if(count($data) > 0){
            foreach ($data as $d) {

                $po = $d->dn_po_nbr;
                $dn = $d->dn_tr_id;
                $cek_data_po = DB::table('SOAP_po_mstr')
                    ->select('po_number','POdue_date','po_status')
                    ->where('po_domain','AAIJ')
                    ->where('po_number',$po)
                    ->first();
                $status = $cek_data_po->po_status;
                if ($status!="C"){
                    $status='Opened';
                }else{
                    $status='Closed';
                }
                $dnd = DB::table('dnd_det')
                    ->select('dnd_qty_order')
                    ->where('dnd_tr_id',$d->dn_tr_id)
                    ->where('dnd_po_nbr',$d->dn_po_nbr)
                    ->first();
                $qty_dn = $dnd->dnd_qty_order;
                $cek_item = DB::table('dn_item_supp')
                    ->select('back_no','pcs_kanban','pallet_kanban')
                    ->where('kd_item',$po)
                    ->first();
                if(empty($cek_item->pcs_kanban)){
                    $qty_kanban = 1;
                }else{
                    $qty_kanban = $cek_item->pcs_kanban;
                }

                if ($qty_kanban == ''){
                    $qty_DN_pcs = ($qty_dn*1);
                }else{
                    $qty_DN_pcs=($qty_kanban*$qty_dn);
                }

                $total_qty_dn = $total_qty_dn+$qty_DN_pcs;

                $cek_data_DO =  DB::table('portald_det')
                        ->where('portald_no_dn', $dn)
                        ->where('portald_po_nbr', $po)
                        ->sum('portald_qty_ship');

                $qty_DO = $cek_data_DO;

                if ($qty_DO != $total_qty_dn){
                    $status_dn='Opened';
                    $class = "";
                }else{
                    $status_dn='Closed';
                    $class = "style='background-color:yellow'";
                }
                if($status_dn = 'Opened'){
                    $url = '/delivery-order-dn/'.base64_encode($d->dn_tr_id).'/'.base64_encode($d->dn_po_nbr).'';
                    $act = '<a href="'.$url.'" class="btn btn-warning"><font color="black"><i class="fas fa-truck"></i> <b>Ship</b><font></a>';
                }else{
                    $act = '-';
                }
                $tablenya2 .= '
                    <tr>
                        <td>'.$urut++.'</td>
                        <td>'.$d->dn_tr_id.'</td>
                        <td>'.$d->dn_po_nbr.'</td>
                        <td>'.$d->dn_cycle_order.'</td>
                        <td>'.date('d-F-Y',strtotime($d->dn_order_date)).'</td>
                        <td>'.$d->dn_cycle_arrival.'</td>
                        <td>'.date('d-F-Y',strtotime($d->dn_arrival_date)).'</td>
                        <td>'.date('d-F-Y',strtotime($cek_data_po->POdue_date)).'</td>
                        <td>'.$status.'</td>
                        <td>'.$status_dn.'</td>
                        <td>'.$act.'</td>
                    </tr>';
            }
        }else{
            $tablenya2 .= '';
        }
            return view('packing-slip.byDN',compact('data','tablenya2'));
        }
    }

    public function editpackingslip($id,$po){
        $atas = DB::table('dn_mstr')->where('dn_tr_id',base64_decode($id))->first();
        $today = date('Y-m-d');
        $session        = Auth::guard('pub_login')->user()->kode_supplier;
        // $session = 'SR0085';
        $tahun      = date('Y');
        $tampung    = base64_decode($id);
        $sql_cek_mfg  = DB::select("SELECT ISNULL(MAX(substring(portal_tr_id,10,4)),0) AS hitung FROM portal_mstr WHERE year(portal_dlv_date)='".$tahun."'
                               AND portal_vend ='".$session."'");
        $a      = $sql_cek_mfg[0]->hitung;
        $b      = $a + 1;
        $jlm_b  = strlen($b);

        if ($jlm_b == 1){
            $b = "000".$b;
        }

        if ($jlm_b == 2){
            $b = "00".$b;
        }

        if ($jlm_b == 3){
            $b = "0".$b;
        }

        $seq        = $session.substr(date("Y"),2,2);
        $p_lama     = $seq.$a;
        $p_baru     = $seq.($b);
        $ponya = base64_decode($po);
        $dnnya = base64_decode($id);
        $ambil = DB::select("SELECT dnd_part,dnd_line,dnd_po_nbr,dnd_qty_order,dn_vend FROM dnd_det,dn_mstr
        WHERE dnd_tr_id='".$dnnya."' AND dn_tr_id=dnd_tr_id ORDER BY dnd_line ");
        $jml_row = count($ambil);
        $tablenya = '';
        $urut = 1;
        if(count($ambil) > 0){
            getPODetail($ponya);
            foreach($ambil as $am){
                $qty_order = $am->dnd_qty_order;
                $tampil_data_type = pt_mstr($am->dnd_part);
                // $tampil_data_type = DB::select("SELECT * from SOAP_pt_mstr WHERE pt_domain='AAIJ' and item_number='".$am->dnd_part."'");

                    //untuk menampilkan pcs Kanban
                $tampil_qty_kanban = DB::select("SELECT back_no,pcs_kanban,pallet_kanban FROM dn_item_supp
                                          WHERE kd_item='".$am->dnd_part."' AND kd_supp='".$session."'");

                if(empty($tampil_qty_kanban->pcs_kanban)){
                    $qty_kanban = '';
                }else{
                    $qty_kanban = $tampil_qty_kanban->pcs_kanban;
                }

                if ($qty_kanban == ''){
                    $qty_DN_pcs = ($qty_order*1);
                }
                else{
                    $qty_DN_pcs=($qty_kanban*$qty_order);
                }

                //Untuk mengecek sisa Qty DN yang sudah dibuat surat jalan
                $tampil_qty_sisaDN = DB::select("SELECT ISNULL(sum(portald_qty_ship),0) as hitung FROM portald_det WHERE portald_no_dn='".$tampung."' AND
                                        portald_po_nbr ='".$ponya."' AND portald_part ='".$am->dnd_part."' AND portald_line='".$am->dnd_line."'");

                $sisa_dn = $tampil_qty_sisaDN[0]->hitung;
                
                if ($sisa_dn == 0){
                    $sisa_qtyDN = $qty_DN_pcs;
                }else{
                    $sisa_qtyDN = $qty_DN_pcs - $sisa_dn ;
                }
                

                //untuk menampilkan qty po
                $tampil_qty_po = DB::select("SELECT qty_po,qty_receive,po_um FROM SOAP_po_detail WHERE no_po='".$ponya."'
                AND item_number='".$am->dnd_part."' AND line = ".$am->dnd_line." AND po_domain='AAIJ'");

                $qty_po     = $tampil_qty_po[0]->qty_po;
                $qty_rcvd   = $tampil_qty_po[0]->qty_receive;
                $um         = $tampil_qty_po[0]->po_um;

                //untuk menampilkan jumlah qty surat jalan
                $item_sj = DB::select("SELECT ISNULL(SUM(portald_qty_ship),0) AS hitung FROM portald_det WHERE portald_po_nbr='".$ponya."' AND portald_confirm_date IS NULL
                            AND portald_part='".$am->dnd_part."' AND portald_line='".$am->dnd_line."'");

                //untuk menampilkan jumlah yang di receive
                $sql_cek_rcvd = DB::select("SELECT isnull(sum(prhRcvd),0) AS hitung from SOAP_prh_hist
                                where prhNbr='".$ponya."' and prhPart='".$am->dnd_part."' and prhLine='".$am->dnd_line."'");


                $desc1      = $tampil_data_type['deskripsi1'];
                $f          = $sql_cek_rcvd[0]->hitung;
                $desc2      = $tampil_data_type['deskripsi2'];
                $item_desc  = $tampil_data_type['desc_type'];

                // $ket_um = DB::select("select code_fldname,code_cmmt,code_value from pub.code_mstr where
                //             code_fldname='pt_um' and code_value='".$um."' and code_domain='AAIJ' ");
                // $rs_ket_um = odbc_exec($conn_mfg, $ket_um);

                $over_po =($qty_po-($f)+ ($item_sj[0]->hitung));

                $tablenya .= '
                    <tr>
                        <td>'.$am->dnd_line .'</td>
                        <td>'.$am->dnd_part.'</td>
                        <td>'.$um.'</td>
                        <td>'.$desc1.'</td>
                        <td>'.$item_desc.'</td>
                        <td>'.$qty_po.'</td>
                        <td>'.$qty_rcvd.'</td>
                        <td>'.$item_sj[0]->hitung.'</td>
                        <td>'.$qty_DN_pcs.'</td>
                        <td>'.$sisa_qtyDN.'</td>
                        <input type="hidden" name="sisa_dn['.$urut.']" id="sisa_dn['.$urut.']" class="form-control" value="'.$sisa_qtyDN.'">
                        <td><input type="text" name="qty['.$urut.']" id="qty['.$urut.']" class="form-control" style="color:black" onchange="cekdata('.$urut.')"></td>
                        <td>PIECES</td>
                        <td><input type="text" name="lot[]" id="lot[]" class="form-control" style="color:black"></td>
                        <td><input type="text" name="productdate[]" id="productdate[]" class="form-control" style="color:black"></td>
                        <input type="hidden" name="item_number[]" id="item_number[]" class="form-control" value="'.$am->dnd_part.'">
                        <input type="hidden" name="line[]" id="line[]" class="form-control" value="'.$am->dnd_line.'">
                        <input type="hidden" name="po_um[]" id="po_um[]" class="form-control" value="'.$um.'">
                        <input type="hidden" name="tr_id" id="tr_id" value="'.$p_baru.'">
                        <input type="hidden" name="dn" id="dn" value="'.$dnnya.'">
                        <input type="hidden" name="po" id="po" value="'.$ponya.'">
                        <input type="hidden" name="jml_row" id="jml_row" value="'.$jml_row.'">
                        <td></td>
                    </tr>';
                    $urut++;
            } //end foreach
        }else{
            $tablenya .= '';
        }
        return view('delivery-order.delivery-order-dn',compact('atas','today','p_baru','tablenya'));
    }

    public function insert_do_dn(Request $request){
        $supplier   = Auth::guard('pub_login')->user()->kode_supplier;
        $username   = Auth::guard('pub_login')->user()->username;
        $order      = $request->get('qty');
        $itnum      = $request->get('item_number');
        $line       = $request->get('line');
        $lot        = $request->get('lot');
        $productdate= $request->get('productdate');

        $transaksi  = $request->get('tr_id');
        $delivery   = $request->get('tgl');
        $slip       = $request->get('ps');
        $po         = $request->get('po');
        $dn         = $request->get('dn');

        $um         = $request->get('po_um');
        $row        = $request->get('jml_row');

        //untuk mencari DN agar tidak duplikat
        $cek_dn = DB::table('portal_mstr')->where('portal_ps_nbr',$slip)->get();
        if(count($cek_dn) > 0){
            $result = 'duplicated';
        }else{
            $date = Carbon::now('Asia/Jakarta');
            $time = date('H:i',strtotime($date));
            DB::table('portal_mstr')->insert([
                'portal_tr_id'      => $transaksi,
                'portal_po_nbr'     => $po,
                'portal_ps_nbr'     => $slip,
                'portal_dlv_date'   => $delivery,
                'portal_vend'       => $supplier,
                'portal_user_id'    => $username,
                'portal__char01'    => $time,
            ]);

            for ($i=0; $i < $row; $i++) {
                if($order[$i] != ""){

                    $cek_dulu = DB::table('portald_det')
                    ->where('portald_part', $itnum[$i])
                    ->where('portald_po_nbr', $po)
                    ->where('portald_tr_id', $transaksi)
                    ->where('portald_line',$line[$i])
                    ->get();

                    if(count($cek_dulu) == 0){
                        $data = new PortalDet;
                        $data->portald_tr_id        = $transaksi;
                        $data->portald_po_nbr       = $po;
                        $data->portald_dlv_date     = $delivery;
                        $data->portald_part         = $itnum[$i];
                        $data->portald_line         = $line[$i];
                        $data->portald_qty_ship     = $order[$i];
                        $data->portald_user_id      = $username;
                        $data->portald_um           = $um[$i];
                        $data->portald_no_dn        = $dn;
                        $data->portald_no_lot       = $lot[$i];
                        $data->portald__char01      = 0;
                        $data->save();
                    }
                }
            }
            $result = "berhasil";

        }
        return response()->json($result);
    }

    public function print_po(){

        $session = Auth::guard('pub_login')->user()->kode_supplier;
        $Years = date('Y');
        $results = DB::select("SELECT DISTINCT top 10  a.* FROM (SELECT MAX(po_revisi) as revisi, po_nbr
        FROM po_trans_mstr WHERE year(po_effdate) IN ('".$Years."','2020') GROUP BY po_nbr ) aux
        INNER JOIN po_trans_mstr a ON a.po_nbr = aux.po_nbr WHERE aux.revisi=a.po_revisi AND a.po_app2_status='OK'
        AND po_vend='".$session."' AND year(po_effdate)='".$Years."'");
        return view('packing-slip.print-po',compact('results'));
    }

    public function act_print_po(Request $request){
        $po     = $request->get('po');
        $bulan  = $request->get('bulan');
        $tahun  = $request->get('tahun');
        $session = Auth::guard('pub_login')->user()->kode_supplier;
        $Years = date('Y');
        if($po == 0 && $bulan == 0 && $tahun == 0){
            
            $results = DB::select("SELECT DISTINCT top 10  a.* FROM (SELECT MAX(po_revisi) as revisi, po_nbr
            FROM po_trans_mstr WHERE year(po_effdate) IN ('".$Years."','2020') GROUP BY po_nbr ) aux
            INNER JOIN po_trans_mstr a ON a.po_nbr = aux.po_nbr WHERE aux.revisi=a.po_revisi AND a.po_app2_status='OK'
            AND po_vend='".$session."' AND year(po_effdate)='".$Years."'");
        }else if($po != "" || $bulan == "" || $tahun == ""){
            $results = DB::select("SELECT DISTINCT top 100  a.* FROM (SELECT MAX(po_revisi) as revisi, po_nbr
            FROM po_trans_mstr WHERE year(po_effdate) = '$Years' GROUP BY po_nbr ) aux
            INNER JOIN po_trans_mstr a ON a.po_nbr = aux.po_nbr WHERE aux.revisi=a.po_revisi AND a.po_app2_status='OK'
            AND po_vend='".$session."' AND (a.po_nbr like '%".$po."%') ");
        }else if($po == "" || $bulan != "" || $tahun == ""){
            $results = DB::select("SELECT DISTINCT top 100  a.* FROM (SELECT MAX(po_revisi) as revisi, po_nbr
            FROM po_trans_mstr WHERE year(po_effdate) IN ('".$tahun."','2021') GROUP BY po_nbr ) aux
            INNER JOIN po_trans_mstr a ON a.po_nbr = aux.po_nbr WHERE aux.revisi=a.po_revisi AND a.po_app2_status='OK'
            AND po_vend='".$session."' AND month(po_effdate)=".$bulan."");
        }else if($po == "" || $bulan == "" || $tahun != ""){
            $results = DB::select("SELECT DISTINCT top 100  a.* FROM (SELECT MAX(po_revisi) as revisi, po_nbr
            FROM po_trans_mstr WHERE year(po_effdate) IN ('".$tahun."','2020') GROUP BY po_nbr ) aux
            INNER JOIN po_trans_mstr a ON a.po_nbr = aux.po_nbr WHERE aux.revisi=a.po_revisi AND a.po_app2_status='OK'
            AND po_vend='".$session."' AND year(po_effdate)='".$tahun."'");
        }else if($po != "" || $bulan != "" || $tahun == ""){
            $results = DB::select("SELECT DISTINCT top 100  a.* FROM (SELECT MAX(po_revisi) as revisi, po_nbr
            FROM po_trans_mstr WHERE year(po_effdate) IN ('".$tahun."','2020') GROUP BY po_nbr ) aux
            INNER JOIN po_trans_mstr a ON a.po_nbr = aux.po_nbr WHERE aux.revisi=a.po_revisi AND a.po_app2_status='OK'
            AND po_vend='".$session."' AND month(po_effdate)=".$bulan." AND (a.po_nbr like '%".$po."%') ");
        }else if($po != "" || $bulan == "" || $tahun != ""){
            $results = DB::select("SELECT DISTINCT top 100  a.* FROM (SELECT MAX(po_revisi) as revisi, po_nbr
            FROM po_trans_mstr WHERE year(po_effdate) IN ('".$tahun."','2020') GROUP BY po_nbr ) aux
            INNER JOIN po_trans_mstr a ON a.po_nbr = aux.po_nbr WHERE aux.revisi=a.po_revisi AND a.po_app2_status='OK'
            AND po_vend='".$session."' AND year(po_effdate)='".$tahun."'  AND (a.po_nbr like '%".$po."%') ");
        }else if($po != "" || $bulan != "" || $tahun != ""){
            $results = DB::select("SELECT DISTINCT top 100  a.* FROM (SELECT MAX(po_revisi) as revisi, po_nbr
            FROM po_trans_mstr WHERE year(po_effdate) IN ('".$tahun."','2020') GROUP BY po_nbr ) aux
            INNER JOIN po_trans_mstr a ON a.po_nbr = aux.po_nbr WHERE aux.revisi=a.po_revisi AND a.po_app2_status='OK'
            AND po_vend='".$session."' AND month(po_effdate)=".$bulan." AND year(po_effdate)='".$tahun."'  AND (a.po_nbr like '%".$po."%') ");
        }else{
            $results = [];
        }
            $tablenya = '';
            if(count($results) > 0){
                $no     = 1;
                foreach($results as $rs){

                    $bln    = date('n',strtotime($rs->po_app2_effdate));
                    $tahun  = date('Y',strtotime($rs->po_app2_effdate));
                    $periode= $bln."".$tahun;
                    $url    = 'http://purchasing.akebono-astra.co.id/po/'.$periode.'/'.$rs->po_nbr.'-'.$rs->po_revisi.'.pdf';

                    $tablenya .= '
                        <tr>
                            <td>'.$no++ .'</td>
                            <td>'.$rs->po_nbr.'</td>
                            <td>'.date('d-F-Y',strtotime($rs->po_effdate)) .'</td>
                            <td><a href="'.$url.'" target="_blank" class="btn btn-info" title="Print"><i class="fas fa-print"></i></a></td>
                        </tr>';
                } //end foreach
            }else{
                $tablenya .= '';
            }
            return response($tablenya);
        // echo json_encode($results);
    }

    public function packingSlipBarcodeView(){
        return view('packing-slip.barcode-view');
    }

    public function print_packingslip_barcode(){
        $session = Auth::guard('pub_login')->user()->kode_supplier;
        $tampil=DB::select("SELECT distinct TOP 10 portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn from portal_mstr,portald_det where portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
        return view('packing-slip.print',compact('tampil'));
    }

    public function printpssupp($id){
        $tr = base64_decode($id);
        $delivery = DB::table('portald_det')->where('portald_tr_id',$tr)->get();
        $packingslipbarcode = DB::table('portal_mstr')->where('portal_tr_id',$tr)->first();
        return view('packing-slip.print_ps_supp',compact('tr','delivery','packingslipbarcode'));
    }

    public function print_direct_psb(Request $request){
        $from       = $request->get('from');
        $to         = $request->get('to');

        $session = Auth::guard('pub_login')->user()->kode_supplier;

        if ($from != "" AND $to == ""){
            $tampil=DB::select("SELECT distinct portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn from portal_mstr,portald_det WHERE portal_tr_id = '$from' AND portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
		}else if ($from == "" and $to != ""){
            $tampil=DB::select("SELECT distinct portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn from portal_mstr,portald_det WHERE portal_tr_id = '$to' AND portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
        }else if($from != "" and $to != ""){
            $tampil=DB::select("SELECT distinct portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn from portal_mstr,portald_det where portal_tr_id between '".$from."' and '".$to."' and portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
		}else{
            $tampil=DB::select("SELECT distinct portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn from portal_mstr,portald_det WHERE portal_tr_id = 'ZZ029AS' AND portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
		}

        if(count($tampil) > 0){
            $respon = url('cetak-ps-from-to/'.base64_encode($from).'/'.base64_encode($to));
        }else{
            $respon = 0;
        }
        return response($respon);
    }

    public function cetak_ps_from_to($from,$to){
        $id_from = base64_decode($from);
        $id_to   = base64_decode($to);
        $session = Auth::guard('pub_login')->user()->kode_supplier;
        $master  = DB::select("SELECT portal_tr_id,portal_vend from portal_mstr where portal_tr_id between '".$id_from."' and '".$id_to."' AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
        // dd($master);
        // if ($id_from != "" AND $id_to == ""){
        //     $tampil=DB::select("SELECT distinct portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn,portald_line,portald_part,portald_qty_ship from portal_mstr,portald_det WHERE portal_tr_id = '$id_from' AND portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
		// }else if ($id_from == "" and $id_to != ""){
        //     $tampil=DB::select("SELECT distinct portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn,portald_line,portald_part ,portald_qty_ship from portal_mstr,portald_det WHERE portal_tr_id = '$id_to' AND portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
        // }else if($id_from != "" and $id_to != ""){
        //     $tampil=DB::select("SELECT distinct portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn,portald_line,portald_part,portald_qty_ship from portal_mstr,portald_det where portal_tr_id between '".$id_from."' and '".$to."' and portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
		// }else{
        //     $tampil=DB::select("SELECT distinct portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn,portald_line from portal_mstr,portald_det WHERE portal_tr_id = 'ZZ029AS' AND portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
		// }

        return view('packing-slip.print_ps_supp_multiple',compact('master'));
    }

    public function search_pp(Request $request){
        $from       = $request->get('from');
        $to         = $request->get('to');

        $session = Auth::guard('pub_login')->user()->kode_supplier;

        if ($from != "" AND $to == ""){
            $tampil=DB::select("SELECT distinct portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn from portal_mstr,portald_det WHERE portal_tr_id = '$from' AND portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
		}else if ($from == "" and $to != ""){
            $tampil=DB::select("SELECT distinct portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn from portal_mstr,portald_det WHERE portal_tr_id = '$to' AND portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
        }else if($from != "" and $to != ""){
            $tampil=DB::select("SELECT distinct portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn from portal_mstr,portald_det where portal_tr_id between '".$from."' and '".$to."' and portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
		}else{
            $tampil=DB::select("SELECT distinct portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date ,portald_no_dn from portal_mstr,portald_det WHERE portal_tr_id = 'ZZ029AS' AND portal_tr_id = portald_tr_id AND portal_vend = '$session' ORDER BY portal_tr_id DESC");
		}
        $table = '';
        if(count($tampil) > 0){
            foreach($tampil as $t){
                $url = '<a href="/print-ps-supplier/'.base64_encode($t->portal_tr_id).'" target="_blank">'.$t->portal_tr_id.'</a>';
                $table .= '
                    <tr>
                        <td>'.$url.'</td>
                        <td>'.$t->portal_po_nbr.'</td>
                        <td>'.$t->portal_ps_nbr.'</td>
                        <td>'.date('d-F-Y',strtotime($t->portal_dlv_date)).'</td>
                    </tr>';
            }
        }else{
            $table .= '';
        }
        return response($table);
    }

    // diyar
    public function editDeliveryOrderDN() {

        $last       = date('Y-m-d', strtotime('-7 day'));
        $supplier   = Auth::guard('pub_login')->user()->kode_supplier;
        $data       = DB::select("SELECT DISTINCT portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date,portald_no_dn FROM portal_mstr,portald_det WHERE portal_tr_id=portald_tr_id
        AND portal_vend like '".$supplier."%' AND portal_dlv_date > '$last' AND portald_no_dn != ''");
        $tablenya = '';
        $hitung = 0;

        if(count($data) > 0){
            foreach($data as $dd){

                $act = '<a href="/delivery-order-dn-edit/'.base64_encode($dd->portal_tr_id).'" class="btn btn-xs btn-warning mb-1" title="Edit"><font color="white"><i class="fas fa-edit"></i></font></a>
                <a class="btn btn-xs btn-danger mb-1" title="Delete"  onclick="hapus_transaksi(\''.$dd->portal_tr_id.'\')"><font color="white"><i class="fas fa-trash"></i></font></a>';

                $tablenya .= '
                    <tr>
                        <td>'.$dd->portal_tr_id .'</td>
                        <td>'.$dd->portal_po_nbr.'</td>
                        <td>'.$dd->portald_no_dn.'</td>
                        <td>'.$dd->portal_ps_nbr.'</td>
                        <td>'.date('d-F-Y',strtotime($dd->portal_dlv_date)).'</td>
                        <td>'.$act.'</td>
                    </tr>';
            } //end foreach
        }else{
            $tablenya .= '';
        }
        return view('packing-slip.edit-delivery-order-dn',compact('tablenya'));
    }

    public function search_do_dn(Request $request){
        $bulan = $request->get('bulan');
        $tahun = $request->get('tahun');

        $last       = date('Y-m-d', strtotime('-7 day'));
        $supplier   = Auth::guard('pub_login')->user()->kode_supplier;
        if($bulan != "" && $tahun != ""){
            $data       = DB::select("SELECT DISTINCT portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date,portald_no_dn FROM portal_mstr,portald_det WHERE portal_tr_id=portald_tr_id
            AND portal_vend like '".$supplier."%' AND month(portal_dlv_date) = '$bulan' AND year(portal_dlv_date) = '$tahun' AND portald_no_dn != ''");
        }else if($bulan == "" && $tahun == ""){
            $data       = DB::select("SELECT DISTINCT portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date,portald_no_dn FROM portal_mstr,portald_det WHERE portal_tr_id=portald_tr_id
            AND portal_vend like '".$supplier."%' AND portal_dlv_date > '$last' AND portald_no_dn != ''");
        }else if($bulan == ""){
            $data       = DB::select("SELECT DISTINCT portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date,portald_no_dn FROM portal_mstr,portald_det WHERE portal_tr_id=portald_tr_id
            AND portal_vend like '".$supplier."%' AND year(portal_dlv_date) = '$tahun' AND portald_no_dn != ''");
        }else if($tahun == ""){
            $data       = DB::select("SELECT DISTINCT portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date,portald_no_dn FROM portal_mstr,portald_det WHERE portal_tr_id=portald_tr_id
            AND portal_vend like '".$supplier."%' AND month(portal_dlv_date) = '$bulan' AND portald_no_dn != ''");
        }else{
            $data       = DB::select("SELECT DISTINCT portal_tr_id,portal_po_nbr,portal_ps_nbr,portal_dlv_date,portald_no_dn FROM portal_mstr,portald_det WHERE portal_tr_id=portald_tr_id
            AND portal_vend like '".$supplier."%' AND portal_dlv_date > '$last' AND portald_no_dn != ''");
        }
        
        $tablenya = '';
        $hitung = 0;

        if(count($data) > 0){
            foreach($data as $dd){

                $act = '<a href="/delivery-order-dn-edit/'.base64_encode($dd->portal_tr_id).'" class="btn btn-xs btn-warning mb-1" title="Edit"><font color="white"><i class="fas fa-edit"></i></font></a>
                <a class="btn btn-xs btn-danger mb-1" title="Delete"  onclick="hapus_transaksi(\''.$dd->portal_tr_id.'\')"><font color="white"><i class="fas fa-trash"></i></font></a>';

                $tablenya .= '
                    <tr>
                        <td>'.$dd->portal_tr_id .'</td>
                        <td>'.$dd->portal_po_nbr.'</td>
                        <td>'.$dd->portald_no_dn.'</td>
                        <td>'.$dd->portal_ps_nbr.'</td>
                        <td>'.date('d-F-Y',strtotime($dd->portal_dlv_date)).'</td>
                        <td>'.$act.'</td>
                    </tr>';
            } //end foreach
        }else{
            $tablenya .= '';
        }

        return response($tablenya);
    }

    public function deliveryOrderDNEdit($id) {
        $atas       = Portal_MSTR::where('portal_tr_id',base64_decode($id))->first();
        $data       = PortalDet::where('portald_tr_id',base64_decode($id))
                    ->orderBy('portald_line','ASC')->get();
        $urut       = 0;
        $hitung     = 0;
        $tablenya   = '';
        $jml_row    = count($data);
        $url_prh    = "https://supplier.akebono-astra.co.id/wsatdw/prh_hist.php";
        // getPrhHist($url_prh);
        // getPODetail($data[0]->portald_po_nbr);
        if(count($data) > 0){
            $prh_hist   = prh_hist($atas->portal_po_nbr);
            $po_detail  = po_detail($atas->portal_po_nbr);
            foreach($data as $dd){
                $qty    = $dd->portald_qty_ship;
                $urut   = $urut + 1;
                // getDataPtMstr($dd->portald_part);
                $ptmstr = pt_mstr($dd->portald_part);

                $tampil_data_dn =  DB::table('dnd_det')
                ->select('dnd_qty_order')
                ->where('dnd_tr_id',$dd->portald_no_dn)
                ->where('dnd_part', $dd->portald_part)
                ->where('dnd_line', $dd->portald_line)
                ->first();
                $qty_dn = $tampil_data_dn->dnd_qty_order;

                $tampil_qty_sisaDN =  DB::table('portald_det')
                        ->where('portald_po_nbr', $dd->portald_po_nbr)
                        ->where('portald_part', $dd->portald_part)
                        ->where('portald_line', $dd->portald_line)
                        ->where('portald_no_dn',$dd->portald_no_dn)
                        ->sum('portald_qty_ship');

                $sisa_dn = $tampil_qty_sisaDN;
                
                if ($sisa_dn == 0){
                    $sisa_qtyDN = $qty;
                }else{
                    $sisa_qtyDN = $qty_dn - $sisa_dn ;
                }
                foreach($po_detail as $pd){
                    if($pd['item_number'] == $dd->portald_part && $pd['line'] == $dd->portald_line){
                        $tampil_qty_po = array(
                            'qty_po'        => $pd['qty_po'],
                            'qty_receive'   => $pd['qty_receive'],
                            'po_um'         => $pd['po_um'],
                        );
                    }
                }

                $qty_po     = $tampil_qty_po['qty_po'];
                $qty_rcvd   = $tampil_qty_po['qty_receive'];
                $um         = $tampil_qty_po['po_um'];

                $item_sj =  DB::table('portald_det')
                ->where('portald_po_nbr', $dd->portald_po_nbr)
                ->whereNull('portald_confirm_date')
                ->where('portald_part', $dd->portald_part)
                ->where('portald_line', $dd->portald_line)
                ->sum('portald_qty_ship');

                //untuk menampilkan jumlah yang di receive
                $sum = 0;
                foreach($prh_hist as $ph){
                    if($ph['prhPart'] == $dd->portald_part && $ph['prhLine'] == $dd->portald_line){
                        $sum += $ph['prhRcvd'];
                        $f = $sum;
                    }
                }
                // getCodeUm($dd->po_um);

                $code_um = DB::table('SOAP_code_mstr')
                    ->where('codeValue', $dd->portald_um)
                    ->first();

                $loop = $hitung++;
                if($dd->portald_qty_ship == 0){
                    $act = '<a class="btn btn-xs btn-danger mb-1" onclick="delete_ship('.$loop.')"><font color="white"><i class="fas fa-trash"></i></font></a>';
                }else{
                    $act = '-';
                }

                $tablenya .= '
                    <tr>
                        <td>'.$dd->portald_line.'</td>
                        <td>'.$dd->portald_part.'</td>
                        <td>'.$dd->portald_um.'</td>
                        <td>'.$ptmstr['deskripsi1'].'</td>
                        <td>'.$ptmstr['desc_type'].'</td>
                        <td>'.number_format($qty_po).'</td>
                        <td>'.number_format($f).'</td>
                        <td>'.number_format($item_sj).'</td>
                        <td>'.number_format($qty_dn).'</td>
                        <td>'.number_format($sisa_qtyDN).'</td>
                        <td><input type="text" id="qty['.$urut.']" style="color:black" class="form-control" value="'.$qty.'" onchange="cekdata('.$urut.')"></td>
                        <td>'.$code_um->codeDesc.'</td>
                        <input type="hidden" name="item_number[]" id="item_number[]" class="form-control" value="'.$dd->portald_part.'">
                        <input type="hidden" id="qty_old[]" class="form-control" value="'.$qty.'">
                        <input type="hidden" id="qty_sisa[]" class="form-control" value="'.$sisa_qtyDN.'">
                        <input type="hidden" name="line[]" id="line[]" class="form-control" value="'.$dd->portald_line.'">
                        <input type="hidden" name="tr_id" id="tr_id" value="'.$atas->portal_tr_id.'">
                        <input type="hidden" name="po" id="po" value="'.$atas->portal_po_nbr.'">
                        <input type="hidden" name="dn" id="dn" value="'.$atas->portald_no_dn.'">
                        <input type="hidden" name="jml_row" id="jml_row" value="'.$jml_row.'">
                        <td><input type="text" id="lot_no[]" style="color:black" class="form-control" value="'.$dd->portald_no_lot.'"></td>
                        <td>-</td>
                        <td>'.$act.'</td>
                    </tr>';
            } //end foreach
        }else{
            $tablenya .= '';
        }
        return view('packing-slip.delivery-order-dn-edit',compact('atas','tablenya'));
    }

    public function edit_deliveryorder_dn(Request $request) {
        $supplier   = Auth::guard('pub_login')->user()->kode_supplier;
        $username   = Auth::guard('pub_login')->user()->username;
        $transaksi  = $request->get('tr_id');
        $delivery   = $request->get('tgl');
        $slip       = $request->get('packingslip');
        $po         = $request->get('po');
        $dn         = $request->get('dn');
        $order      = $request->get('qty');
        $lot        = $request->get('lot_no');
        $itnum      = $request->get('item_number');
        $line       = $request->get('line');
        $row        = $request->get('jml_row');

        Portal_MSTR::where('portal_tr_id', $transaksi)
        ->where('portal_po_nbr', $po)
        ->update([
            'portal_ps_nbr' => $slip,
            'portal_dlv_date' => $delivery
        ]);

        for ($i=0; $i < $row; $i++) {
            if($order[$i] != ""){

                PortalDet::where('portald_tr_id', $transaksi)
                ->where('portald_po_nbr', $po)
                ->where('portald_part', $itnum[$i])
                ->where('portald_line', $line[$i])
                ->update([
                'portald_qty_ship' => $order[$i],
                'portald_no_lot' => $lot[$i]
                ]);
            }
        }
        $result = "berhasil";
        return response()->json($result);
        // return redirect('delivery-notes/create', compact('data'));
    }

    public function DeliveryOrderPO() {
        $session        = Auth::guard('pub_login')->user()->kode_supplier;
        // $bulan_skrg     = date("m");
        $bulan_skrg     = date('m');
        $tahun_skrg     = date("Y");
        $bulan_next     = $bulan_skrg + 1;

        $url = "https://supplier.akebono-astra.co.id/wsatdw/po_mstr.php";
        $master = po_mstr($url);
        $i = 0;
        foreach($master as $c){
            if($c['kode_supplier'] == $session && date('m',strtotime($c['POdue_date'])) == $bulan_next && date('Y',strtotime($c['POdue_date'])) == $tahun_skrg){
                $data[$i++] = array(
                    'po_number'     => $c['po_number'],
                    'POdue_date'    => $c['POdue_date'],
                    'kode_supplier' => $c['kode_supplier'],
                    'po_status'     => $c['po_status'],
                    'nama_supplier' => $c['nama_supplier'],
                    'pocurr'        => $c['pocurr'],
                );
            }
        }
        $tablenya = '';
        if(empty($data)){
            $tablenya .= '';
        }else{
            $i = 1;
                            
            foreach($data as $dt){
                if ($dt['po_status'] != "c"){
                    $status = "Opened";
                }else{
                    $status = "Closed";
                }
                $url = "/ship-delivery-order-po/".base64_encode($dt['po_number'])."";
                $act = '<a href="'.$url.'" class="btn btn-warning"><font color="black"><i class="fas fa-truck"></i> <b>Ship</b><font></a>';

                $tablenya .= 
                '<tr>
                    <td>'.$i++.'</td>
                    <td>'.$dt['po_number'] .'</td>
                    <td>'.date('d-F-Y',strtotime($dt['POdue_date'])) .'</td>
                    <td>'.$status .'</td>
                    <td>'.$status .'</td>
                    <td>'.$act.'</td>
                </tr>';
            }
        }

        // dd($data);
        return view('packing-slip.delivery-order-po',compact('tablenya'));
    }

    public function searchDeliveryOrderPO(Request $request){
        
        $session = Auth::guard('pub_login')->user()->kode_supplier;
        $bulan   = $request->get('bulan');
        $tahun   = $request->get('tahun');
        $bulan_next     = $bulan + 1;

        $url = "https://supplier.akebono-astra.co.id/wsatdw/po_mstr.php";
        $master = po_mstr($url);
        $i = 0;

        if($bulan == ""){
            foreach($master as $c){
                if($c['kode_supplier'] == $session || date('Y',strtotime($c['POdue_date'])) == $tahun){
                    $data[$i++] = array(
                        'po_number'     => $c['po_number'],
                        'POdue_date'    => $c['POdue_date'],
                        'kode_supplier' => $c['kode_supplier'],
                        'po_status'     => $c['po_status'],
                        'nama_supplier' => $c['nama_supplier'],
                        'pocurr'        => $c['pocurr'],
                    );
                }
            }
        }else if($tahun == ""){
            foreach($master as $c){
                if($c['kode_supplier'] == $session || date('m',strtotime($c['POdue_date'])) == $bulan && date('m',strtotime($c['POdue_date'])) == $bulan_next && date('Y',strtotime($c['POdue_date'])) == $tahun){
                    $data[$i++] = array(
                        'po_number'     => $c['po_number'],
                        'POdue_date'    => $c['POdue_date'],
                        'kode_supplier' => $c['kode_supplier'],
                        'po_status'     => $c['po_status'],
                        'nama_supplier' => $c['nama_supplier'],
                        'pocurr'        => $c['pocurr'],
                    );
                }
            }
        }else if($bulan == 0 && $tahun == 0){
            $bulan_skrg     = date("m");
            $tahun_skrg     = date("Y");
            $next     =     $bulan_skrg + 1;

            foreach($master as $c){
                if($c['kode_supplier'] == $session || date('m',strtotime($c['POdue_date'])) == $bulan_skrg && date('m',strtotime($c['POdue_date'])) == $bulan_next && date('Y',strtotime($c['POdue_date'])) == $tahun){
                    $data[$i++] = array(
                        'po_number'     => $c['po_number'],
                        'POdue_date'    => $c['POdue_date'],
                        'kode_supplier' => $c['kode_supplier'],
                        'po_status'     => $c['po_status'],
                        'nama_supplier' => $c['nama_supplier'],
                        'pocurr'        => $c['pocurr'],
                    );
                }
            }
        }else if($bulan != "" && $tahun != ""){
            foreach($master as $c){
                if($c['kode_supplier'] == $session || date('m',strtotime($c['POdue_date'])) == $bulan && date('m',strtotime($c['POdue_date'])) == $bulan_next && date('Y',strtotime($c['POdue_date'])) == $tahun){
                    $data[$i++] = array(
                        'po_number'     => $c['po_number'],
                        'POdue_date'    => $c['POdue_date'],
                        'kode_supplier' => $c['kode_supplier'],
                        'po_status'     => $c['po_status'],
                        'nama_supplier' => $c['nama_supplier'],
                        'pocurr'        => $c['pocurr'],
                    );
                }
            }
        }else{
            foreach($master as $c){
                if($c['kode_supplier'] == $session || date('m',strtotime($c['POdue_date'])) == $bulan_skrg && date('m',strtotime($c['POdue_date'])) == $next && date('Y',strtotime($c['POdue_date'])) == $tahun_skrg){
                    $data[$i++] = array(
                        'po_number'     => $c['po_number'],
                        'POdue_date'    => $c['POdue_date'],
                        'kode_supplier' => $c['kode_supplier'],
                        'po_status'     => $c['po_status'],
                        'nama_supplier' => $c['nama_supplier'],
                        'pocurr'        => $c['pocurr'],
                    );
                }
            }
        }

        $tablenya = '';
        $no = 1 ;
        if(count($data) > 0){
            foreach($data as $dt){
                if ($dt['po_status'] != "c"){
                    $status = "Opened";
                }else{
                    $status = "Closed";
                }
                $url = "/ship-delivery-order-po/".base64_encode($dt['po_number'])."";
                $tablenya .= '
                    <tr>
                        <td>'.$no++ .'</td>
                        <td>'.$dt['po_number'] .'</td>
                        <td>'.date('d-F-Y',strtotime($dt['POdue_date'])).'</td>
                        <td>'.$status.'</td>
                        <td>'.$status.'</td>
                        <td><a href="'.$url.'" class="btn btn-warning"><font color="black"><i class="fas fa-truck"></i> <b>Ship</b><font></a></td>
                    </tr>';
            } //end foreach
        }else{
            $tablenya .= '';
        }
        return response($tablenya);

    }

    public function shipDeliveryOrderPO($id) {
        $session        = Auth::guard('pub_login')->user()->kode_supplier;
        $tampung        = base64_decode($id);
        $today          = date('Y-m-d');
        $tahun          = date('Y');
        $sql_cek_mfg    = DB::select("SELECT ISNULL(MAX(substring(portal_tr_id,10,4)),0) AS hitung FROM portal_mstr WHERE year(portal_dlv_date)='".$tahun."' AND portal_vend ='".$session."'");
        $a      = $sql_cek_mfg[0]->hitung;
        $b      = $a + 1;
        $jlm_b  = strlen($b);

        if ($jlm_b == 1){
            $b = "000".$b;
        }

        if ($jlm_b == 2){
            $b = "00".$b;
        }

        if ($jlm_b == 3){
            $b = "0".$b;
        }

        $seq        = $session.substr(date("Y"),2,2);
        $p_lama     = $seq.$a;
        $p_baru     = $seq.($b);
        $tr_id      = $p_baru;

        return view('packing-slip.ship-delivery-order-po',compact('tr_id','today','tampung'));
    }

    public function generate_ship_order(Request $request){
        Artisan::call('cache:clear');
        $ponya   = base64_decode($request->get('po'));
        $tampung = base64_decode($request->get('po'));
        $p_baru  = base64_decode($request->get('tr'));
        set_time_limit(0);
        
        $session        = Auth::guard('pub_login')->user()->kode_supplier;
        
        $main = po_detail($ponya);
        
        // $main = DB::select("SELECT * FROM SOAP_po_detail a WHERE a.no_po = '$ponya' AND po_domain = 'AAIJ' ORDER BY a.line ASC");
        $tablenya = '';
        $jml_row = count($main);
        // dd($main);
        if(count($main) > 0){

            $total_out_sj = 0;
            $urut = 1;
            foreach($main as $m){
                // $urut = $urut + 1;
                $totalQty_SJ        = 0;
                $total_lpb_manual   = 0;

                $item_sj = DB::table('portal_mstr')
                ->select('portald_det.portald_qty_ship','portal_mstr.portal_ps_nbr')
                ->join('portald_det','portald_det.portald_tr_id','=','portal_mstr.portal_tr_id')
                ->where('portald_det.portald_po_nbr', $tampung)
                ->whereNull('portald_det.portald_confirm_date')
                ->where('portald_det.portald_part', $m['item_number'])
                ->where('portald_det.portald_line', $m['line'])
                ->first();

                if(empty($item_sj)){
                    $surat_jalan        = "A";
                    $qty_surat_jalan    = 0;
                }else{
                    $surat_jalan        = $item_sj->portal_ps_nbr;
                    $qty_surat_jalan    = $item_sj->portald_qty_ship;
                }

                $totalQty_SJ        = $totalQty_SJ  + $qty_surat_jalan;

                $sql_cek_rcvd = DB::table('SOAP_prh_hist')
                        ->select('prhRcvd')
                        ->where('prhNbr', $ponya)
                        ->where('prhPart', $m['item_number'])
                        ->where('prhLine', $m['line'])
                        ->where('prhDomain' , 'AAIJ')
                        ->sum('prhRcvd');
                        // dd($sql_cek_rcvd);

                if(empty($sql_cek_rcvd) || $sql_cek_rcvd == 0){
                    $qtyLpbManual = 0;
                }else{
                    $qtyLpbManual =  $sql_cek_rcvd;
                }

                $total_lpb_manual =  $total_lpb_manual + $qtyLpbManual;

                $qtyOutstandingSJ = $totalQty_SJ  - $total_lpb_manual;


                $tampil_no_lpb = DB::table('elpb_dtl')
                ->join('elpb_mstr','elpb_mstr.lpb_code','=','elpb_dtl.lpb_code')
                ->where('elpb_dtl.po', $tampung)
                ->whereNull('elpb_mstr.status_approval')
                ->whereNull('elpb_dtl.rc')
                ->where('elpb_dtl.item_number', $m['item_number'])
                ->where('elpb_dtl.line', $m['line'])
                ->sum('elpb_dtl.qty_received');

                $surat_jalan_elpb = DB::table('elpb_dtl')
                ->join('elpb_mstr','elpb_mstr.lpb_code','=','elpb_dtl.lpb_code')
                ->where('elpb_dtl.po', $tampung)
                ->whereNull('elpb_mstr.status_approval')
                ->whereNull('elpb_dtl.rc')
                ->where('elpb_dtl.item_number', $m['item_number'])
                ->where('elpb_dtl.line', $m['line'])
                ->select('elpb_dtl.packing_slip')->first();


                $item_mstr = pt_mstr($m['item_number']);

                $code_um = DB::table('SOAP_code_mstr')
                    ->where('codeValue', $m['po_um'])
                    ->first();

                if($item_mstr['deskripsi1'] == ""){
                    $desc = $m['item_deskripsi'];
                }else{
                    $desc = $item_mstr['deskripsi1'];
                }
                $over_po =(($m['qty_po'])-($sql_cek_rcvd + $total_out_sj));
                if ($over_po < 0){
                    $over = ($over_po*-1);
                    $ss = 'style="color:#FF0000;"';
                    $kolom_over = number_format($over);
                }else{
                    $ss = '';
                    $kolom_over = '0';
                }

                if ($surat_jalan_elpb == $surat_jalan){
                    $qty_open = (($m['qty_po'])-($sql_cek_rcvd)-($tampil_no_lpb));

                }else{
                    $qty_open = (($m['qty_po'])-($sql_cek_rcvd)-($qtyOutstandingSJ)-($tampil_no_lpb));
                }
                $tablenya .= '
                <tr>
                    <td><font size="2">'.$m['line'].'</font></td>
                    <td><font size="2">'.$m['item_number'].'</font></td>
                    <td><font size="2">'.$m['po_um'].'</font></td>
                    <td><font size="2">'.$desc.'</font></td>
                    <td><font size="2">'.$item_mstr['desc_type'].'</font></td>
                    <td><font size="2">'.number_format($m['qty_po']).'</font></td>
                    <td><font size="2">'.number_format($sql_cek_rcvd).'</font></td>
                    <td><font size="2">'.number_format($qtyOutstandingSJ).'</font></td>
                    <td><font size="2">'.number_format($tampil_no_lpb).'</font></td>
                    <td><font size="2">'.number_format($qty_open).'</font></td>
                    <input type="hidden" name="qty_open['.$urut.']" id="qty_open['.$urut.']" class="form-control" value="'.$qty_open.'">
                    <td><font size="2"><input type="text" name="qty_ship['.$urut.']" id="qty_ship['.$urut.']" onchange="cekdata('.$urut.')" class="form-control"></font></td>
                    <input type="hidden" name="item_number[]" id="item_number[]" class="form-control" value="'.$m['item_number'].'">
                    <input type="hidden" name="line[]" id="line[]" class="form-control" value="'.$m['line'].'">
                    <input type="hidden" name="po_um[]" id="po_um[]" class="form-control" value="'.$m['po_um'].'">
                    <input type="hidden" name="tr_id" id="tr_id" value="'.$p_baru.'">
                    <input type="hidden" name="po" id="po" value="'.$tampung.'">
                    <input type="hidden" name="jml_row" id="jml_row" value="'.$jml_row.'">
                    <td><font size="2">'.$code_um->codeDesc.'</font></td>
                    <td '.$ss.'><font size="2">'.$kolom_over.'</font></td>
                    <td><font size="2"><a href="/check-sheet/'.base64_encode($tampung).'/'.base64_encode($m['item_number']).'/'.base64_encode($p_baru).'/'.base64_encode($m['line']).'" target="_blank" class="btn btn-info"><i class="fa fa-check"><i></a></font></td>
                </tr>';

                $urut++;
            } //end foreach
        }else{
            $tablenya .= '';
        }
        return response($tablenya);
    }

    public function insert_deliveryorder_po(Request $request) {
        $supplier   = Auth::guard('pub_login')->user()->kode_supplier;
        $username   = Auth::guard('pub_login')->user()->username;
        $transaksi  = $request->get('tr_id');
        $delivery   = $request->get('tgl');
        $slip       = $request->get('packingslip');
        $po         = $request->get('po');
        $order      = $request->get('qty_ship');
        $cycle      = $request->get('cycle');
        $itnum      = $request->get('item_number');
        $line       = $request->get('line');
        $um         = $request->get('po_um');
        $row        = $request->get('jml_row');

        //untuk mencari DN agar tidak duplikat
        $cek_dn = DB::table('portal_mstr')->where('portal_ps_nbr',$slip)->get();
        if(count($cek_dn) > 0){
            $result = 'duplicated';
        }else{
            $date = Carbon::now('Asia/Jakarta');
            $time = date('H:i',strtotime($date));
            DB::table('portal_mstr')->insert([
                'portal_tr_id'      => $transaksi,
                'portal_po_nbr'     => $po,
                'portal_ps_nbr'     => $slip,
                'portal_dlv_date'   => $delivery,
                'portal_vend'       => $supplier,
                'portal_user_id'    => $username,
                'portal__char01'    => $time,
                'portal_cycle'      => $cycle
            ]);

            for ($i=0; $i < $row; $i++) {
                if($order[$i] != ""){

                    $cek_dulu = DB::table('portald_det')
                    ->where('portald_part', $itnum[$i])
                    ->where('portald_po_nbr', $po)
                    ->where('portald_tr_id', $transaksi)
                    ->where('portald_line',$line[$i])
                    ->get();

                    if(count($cek_dulu) == 0){
                        $data = new PortalDet;
                        $data->portald_tr_id        = $transaksi;
                        $data->portald_po_nbr       = $po;
                        $data->portald_dlv_date     = $delivery;
                        $data->portald_part         = $itnum[$i];
                        $data->portald_line         = $line[$i];
                        $data->portald_qty_ship     = $order[$i];
                        $data->portald_user_id      = $username;
                        $data->portald_um           = $um[$i];
                        $data->portald__char01      = 0;
                        $data->save();
                    }
                }
            }
            $result = "berhasil";

        }
        return response()->json($result);
        // return redirect('delivery-notes/create', compact('data'));
    }

    public function checkSheet($po,$item,$trans,$line){

        $po             = base64_decode($po);
        $item_number    = base64_decode($item);
        $tr_id          = base64_decode($trans);
        $line           = base64_decode($line);
        return view('packing-slip.ship-checksheet-po',compact('po','item_number','tr_id','line'));
    }

    public function searchEditDeliveryOrderPO(Request $request){
        $bulan      = $request->get('bulan');
        $tahun      = $request->get('tahun');
        $supplier   = Auth::guard('pub_login')->user()->kode_supplier;
        $last       = date('n');

        if($bulan == ""){    
            $data = DB::select("SELECT DISTINCT portal_tr_id,portal_po_nbr,portal_ps_nbr,portald_dlv_date
            FROM portal_mstr, portald_det WHERE portal_tr_id=portald_tr_id
            AND portal_vend LIKE '".$supplier."%'
            AND year(portal_dlv_date) = '$tahun' ");
        }else if($tahun == ""){
            $data = DB::select("SELECT DISTINCT portal_tr_id,portal_po_nbr,portal_ps_nbr,portald_dlv_date
            FROM portal_mstr, portald_det WHERE portal_tr_id=portald_tr_id
            AND portal_vend LIKE '".$supplier."%'
            AND month(portal_dlv_date) = '$bulan' ");
        }else if($bulan == 0 && $tahun == 0){
            $data = DB::select("SELECT DISTINCT portal_tr_id,portal_po_nbr,portal_ps_nbr,portald_dlv_date
            FROM portal_mstr, portald_det WHERE portal_tr_id=portald_tr_id
            AND portal_vend LIKE '".$supplier."%'
            AND month(portal_dlv_date) = '$last' ");
  
        }else if($bulan != "" && $tahun != ""){
            $data = DB::select("SELECT DISTINCT portal_tr_id,portal_po_nbr,portal_ps_nbr,portald_dlv_date
            FROM portal_mstr, portald_det WHERE portal_tr_id=portald_tr_id
            AND portal_vend LIKE '".$supplier."%'
            AND month(portal_dlv_date) = '$bulan' AND year(portal_dlv_date) = '$tahun'");

        }else{
            $data = DB::select("SELECT DISTINCT portal_tr_id,portal_po_nbr,portal_ps_nbr,portald_dlv_date
            FROM portal_mstr, portald_det WHERE portal_tr_id=portald_tr_id
            AND portal_vend LIKE '".$supplier."%'
            AND month(portal_dlv_date) = '$last' ");
        }        
        
        $tablenya = '';
        if(count($data) > 0){
            foreach($data as $am){
                $act_edit = '/delivery-order-po-edit/'.base64_encode($am->portal_tr_id).'';
                $tablenya .= '
                    <tr>
                        <td>'.$am->portal_tr_id .'</td>
                        <td>'.$am->portal_po_nbr.'</td>
                        <td>'.$am->portal_ps_nbr.'</td>
                        <td>'.date('d-F-Y',strtotime($am->portald_dlv_date)) .'</td>
                        <td><a href="'.$act_edit.'" class="btn btn-xs btn-warning mb-1" title="Edit"><i class="fas fa-edit"></i></a>
                        <a class="btn btn-xs btn-danger mb-1" onclick="hapus_transaksi(\''.$am->portal_tr_id.'\')"><font color="white"><i class="fas fa-trash"></i></font></a></td>
                    </tr>';
            } //end foreach
        }else{
            $tablenya .= '';
        }
        return response($tablenya);
    }

    public function editDeliveryOrderPO() {
        $last = date('Y-m-d', strtotime('-30 day'));
        // $last = date('Y-m-d',strtotime("2021-01-19"));
        $supplier   = Auth::guard('pub_login')->user()->kode_supplier;
        $ambil      = DB::select("SELECT DISTINCT portal_tr_id,portal_po_nbr,portal_ps_nbr,portald_dlv_date
        FROM portal_mstr, portald_det WHERE portal_tr_id=portald_tr_id
        AND portal_vend LIKE '".$supplier."%'
        AND portal_dlv_date > '$last' ");

        $tablenya = '';
        if(count($ambil) > 0){
            foreach($ambil as $am){
                $act_edit = '/delivery-order-po-edit/'.base64_encode($am->portal_tr_id).'';
                $tablenya .= '
                    <tr>
                        <td>'.$am->portal_tr_id .'</td>
                        <td>'.$am->portal_po_nbr.'</td>
                        <td>'.$am->portal_ps_nbr.'</td>
                        <td>'.date('d-F-Y',strtotime($am->portald_dlv_date)) .'</td>
                        <td><a href="'.$act_edit.'" class="btn btn-xs btn-warning mb-1" title="Edit"><i class="fas fa-edit"></i></a>
                        <a class="btn btn-xs btn-danger mb-1" onclick="hapus_transaksi(\''.$am->portal_tr_id.'\')"><font color="white"><i class="fas fa-trash"></i></font></a></td>
                    </tr>';
            } //end foreach
        }else{
            $tablenya .= '';
        }

    return view('packing-slip.edit-delivery-order-po',compact('tablenya'));
    }

    public function deliveryOrderPOEdit(Request $request, $id) {
        $dataMSTR = Portal_MSTR::where('portal_tr_id', base64_decode($id))->first();
        $dataDet = PortalDet::where('portald_tr_id', base64_decode($id))->orderBy('portald_line','ASC')->get();
        $jml_row = count($dataDet);
        $tablenya = '';
        $hitung = 0;
        if(count($dataDet) > 0){
            foreach($dataDet as $dd){

                getDataPtMstr($dd->portald_part);
                $ptmstr =  DB::table('SOAP_pt_mstr')
                ->where('pt_domain', 'AAIJ')
                ->where('item_number', $dd->portald_part)
                ->first();
                $loop = $hitung++;
                if($dd->portald_qty_ship == 0){
                    $act = '<a class="btn btn-xs btn-danger mb-1" onclick="delete_ship('.$loop.')"><font color="white"><i class="fas fa-trash"></i></font></a>';
                }else{
                    $act = '-';
                }

                $tablenya .= '
                    <tr>
                        <td>'.$dd->portald_line .'</td>
                        <td>'.$dd->portald_part.'</td>
                        <td>'.$ptmstr->deskripsi1.'</td>
                        <td>'.$ptmstr->deskripsi2.'</td>
                        <td>'.$ptmstr->desc_type.'</td>
                        <td>'.$dd->portald_um.'</td>
                        <td><input type="text" style="color:black" class="form-control" name="qty_ship[]" id="qty_ship[]" value="'.$dd->portald_qty_ship.'"</td>
                        <input type="hidden" name="item_number[]" id="item_number[]" class="form-control" value="'.$dd->portald_part.'">
                        <input type="hidden" name="line[]" id="line[]" class="form-control" value="'.$dd->portald_line.'">
                        <input type="hidden" name="tr_id" id="tr_id" value="'.$dataMSTR->portal_tr_id.'">
                        <input type="hidden" name="po" id="po" value="'.$dd->portald_po_nbr.'">
                        <input type="hidden" name="jml_row" id="jml_row" value="'.$jml_row.'">
                        <td>'.$act.'</td>
                    </tr>';
            } //end foreach
        }else{
            $tablenya .= '';
        }

        return view('packing-slip.delivery-order-po-edit', compact('dataMSTR', 'dataDet','tablenya'));
    }

    public function delete_transaction_do_po(Request $request){
        $id         = $request->get('id');
        $mstr       = Portal_MSTR::where('portal_tr_id' , $id);
        $mstr->delete();

        $det        = PortalDet::where('portald_tr_id' , $id);
        $det->delete();

        $result = "berhasil";
        return response()->json($result);
    }

    public function delete_transaction_do_dn(Request $request){
        $id         = $request->get('id');
        $mstr       = Portal_MSTR::where('portal_tr_id' , $id);
        $mstr->delete();

        $det        = PortalDet::where('portald_tr_id' , $id);
        $det->delete();

        $result = "berhasil";
        return response()->json($result);
    }

    public function delete_item_deliveryOrder_po(Request $request){
        $po         = $request->get('po');
        $itnum      = $request->get('tampung_item');
        $line       = $request->get('tampung_line');
        $transaksi  = $request->get('tr_id');

        $post = PortalDet::where('portald_tr_id' , $transaksi)
            ->where('portald_po_nbr' , $po)
            ->where('portald_line'   , $line)
            ->where('portald_part'   , $itnum);
        $post->delete();
        $result = "berhasil";
        return response()->json($result);
    }
    public function edit_deliveryorder_po(Request $request) {
        $supplier   = Auth::guard('pub_login')->user()->kode_supplier;
        $username   = Auth::guard('pub_login')->user()->username;
        $transaksi  = $request->get('tr_id');
        $delivery   = $request->get('tgl');
        $slip       = $request->get('packingslip');
        $po         = $request->get('po');
        $order      = $request->get('qty_ship');
        $itnum      = $request->get('item_number');
        $line       = $request->get('line');
        $row        = $request->get('jml_row');

        Portal_MSTR::where('portal_tr_id', $transaksi)
        ->where('portal_po_nbr', $po)
        ->update([
            'portal_ps_nbr' => $slip,
            'portal_dlv_date' => $delivery
        ]);

        for ($i=0; $i < $row; $i++) {
            if($order[$i] != ""){

                PortalDet::where('portald_tr_id', $transaksi)
                ->where('portald_po_nbr', $po)
                ->where('portald_part', $itnum[$i])
                ->where('portald_line', $line[$i])
                ->update([
                'portald_qty_ship' => $order[$i]
                ]);
            }
        }
        $result = "berhasil";
        return response()->json($result);
        // return redirect('delivery-notes/create', compact('data'));
    }
}
