<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Closure;

class CekRole
{
    public function handle($request, Closure $next)
    {
        if($request->ajax()){
            return $next($request);
        }

        $sidemenu = DB::table('sidemenu')->get();

        foreach ($sidemenu as $item) {
            $roleArray = preg_split("/[,]/",$item->role);
            if (in_array(Auth::guard('pub_login')->user()->previllage, $roleArray)) {
                $submenu = DB::table('submenu')
                ->get();

                $rute = [];
                foreach ($submenu as $items) {
                    $subRoleArray = preg_split("/[,]/",$items->role);
                    if (in_array(Auth::guard('pub_login')->user()->previllage, $subRoleArray)) {
                        $rute[] = $items->rute;
                        $cekin_rute = preg_split("/[,]/",$items->in_rute);
                        foreach ($cekin_rute as $i) {
                            $rute[] = $i;
                        }
                    }
                }
                if (in_array('/'.$request->path(), $rute)) {
                    return $next($request);
                } else {
                    // dd('sini');
                    return redirect()->route('not-found');
                }
            }
        }
    }
}
