<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PubDnMaster extends Model
{
    protected $table = 'dn_mstr';

    public $timestamps = false;

    protected $guarded = [];
}
