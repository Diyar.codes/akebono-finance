<?php

use Illuminate\Database\Seeder;

class poddet extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pod_det')->insert([
            [
                'pod_part'      => '51-A5418-57110',
                'pod_nbr'      => '01074A20',
                'pod_line'    => '1-1',
                'pod_qty_ord'    => '100',
                'pod_qty_rcvd'     => '1-123',
                'pod_req_nbr' => '1',
                'pod_desc'   => '1',
                'pod_qty_rtnd'  => '1',
                'pt_drwg_loc'  => '1 1',
                'pod_um'  => 'PC',
            ],
            [
                'pod_part'      => '51-A5418-58080',
                'pod_nbr'      => '01074A20',
                'pod_line'    => '1-1',
                'pod_qty_ord'    => '100',
                'pod_qty_rcvd'     => '1-123',
                'pod_req_nbr' => '1',
                'pod_desc'   => '1',
                'pod_qty_rtnd'  => '1',
                'pt_drwg_loc'  => '1 1',
                'pod_um'  => 'PC',
            ],
        ]);
    }
}
