<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class supplierItemMaster extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('supplier_item_master')->insert([
            [
                'supplier_id'   => 'SP0045',
                'supplier_name' => 'AT INDONESIA, PT',
                'item_no'       => '1213--e32ed',
                'description'   => 'Brake Spartpart',
                'type_item'     => 'Spartpart',
                'location_from' => 'Tegal',
                'location_to'   => 'Jakarta',
                'um'            => 'PC',
                'active'        => '1',
                'sort'          => '1',
                'type'          => 'Pallet',
                'line'          => '2',
                'snpbox'        => '10',
                'snpbox_pallet' => '5',
            ],
            [
                'supplier_id'   => 'SP0045',
                'supplier_name' => 'AT INDONESIA, PT',
                'item_no'       => '1213asd',
                'description'   => 'Run Spartpart',
                'type_item'     => 'Spartpart',
                'location_from' => 'Tegal',
                'location_to'   => 'Jakarta',
                'um'            => 'PC',
                'active'        => '1',
                'sort'          => '1',
                'type'          => 'Pallet',
                'line'          => '2',
                'snpbox'        => '10',
                'snpbox_pallet' => '5',
            ],
            [
                'supplier_id'   => 'SA0046I',
                'supplier_name' => 'AT INDONESIA, PT',
                'item_no'       => '1213--e32ed',
                'description'   => 'Kai Spartpart',
                'type_item'     => 'Spartpart',
                'location_from' => 'Tegal',
                'location_to'   => 'Jakarta',
                'um'            => 'PC',
                'active'        => '1',
                'sort'          => '1',
                'type'          => 'Pallet',
                'line'          => '2',
                'snpbox'        => '10',
                'snpbox_pallet' => '5',
            ],
        ]);
    }
}
