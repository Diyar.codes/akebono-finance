<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SupplierDTLSuratJalanSeeder extends Seeder
{
    public function run()
    {
        DB::table('supplier_DTL_suratJalan')->insert([
            [
                'no'         => 'SC21010171/WHS/PPC-AAIJ',
                'kode'       => 'S2-41603-62120-M',
                'urutan'     => '',
                'namabarang' => 'BODY W/CYL 080N/021N',
                'jumlah'     => '1152',
                'satuan'     => '',
                'act_qty'    => '1152',
                'qxInbound'  => 'NOK',
                'keterangan' => 'error',
                'remark'     => '',
                'qx_effdate' => '',
            ],
            [
                'no'         => 'SC21010171/WHS/PPC-AAIJ',
                'kode'       => 'S2-41603-62120-M',
                'urutan'     => '',
                'namabarang' => 'BODY W/CYL D300',
                'jumlah'     => '1152',
                'satuan'     => '',
                'act_qty'    => '1104',
                'qxInbound'  => 'NOK',
                'keterangan' => 'error',
                'remark'     => '',
                'qx_effdate' => '',
            ],
            [
                'no'         => 'SC21010170/WHS/PPC-AAIJ',
                'kode'       => 'S2-41603-62120-M',
                'urutan'     => '',
                'namabarang' => 'BODY W/CYL 080N/021N',
                'jumlah'     => '1152',
                'satuan'     => '',
                'act_qty'    => '1152',
                'qxInbound'  => 'NOK',
                'keterangan' => 'error',
                'remark'     => '',
                'qx_effdate' => '',
            ],
            [
                'no'         => 'SC20120462/WHS/PPC-AAIJ',
                'kode'       => 'S2-41603-62120-M',
                'urutan'     => '',
                'namabarang' => 'BODY W/CYL D300',
                'jumlah'     => '1152',
                'satuan'     => '',
                'act_qty'    => '1104',
                'qxInbound'  => 'NOK',
                'keterangan' => 'error',
                'remark'     => '',
                'qx_effdate' => '',
            ],
            [
                'no'         => 'SC20120462/WHS/PPC-AAIJ',
                'kode'       => 'S2-41603-62120-M',
                'urutan'     => '',
                'namabarang' => 'BODY W/CYL D300',
                'jumlah'     => '1152',
                'satuan'     => '',
                'act_qty'    => '1104',
                'qxInbound'  => 'NOK',
                'keterangan' => 'error',
                'remark'     => '',
                'qx_effdate' => '',
            ],
            [
                'no'         => 'SC20120462/WHS/PPC-AAIJ',
                'kode'       => 'S2-41603-62120-M',
                'urutan'     => '',
                'namabarang' => 'BODY W/CYL D300',
                'jumlah'     => '1152',
                'satuan'     => '',
                'act_qty'    => '1104',
                'qxInbound'  => 'NOK',
                'keterangan' => 'error',
                'remark'     => '',
                'qx_effdate' => '',
            ],
            [
                'no'         => 'SC20110109/WHS/PPC-AAIJ',
                'kode'       => 'S2-41603-62120-M',
                'urutan'     => '',
                'namabarang' => 'BODY W/CYL D300',
                'jumlah'     => '1152',
                'satuan'     => '',
                'act_qty'    => '1104',
                'qxInbound'  => 'NOK',
                'keterangan' => 'error',
                'remark'     => '',
                'qx_effdate' => '',
            ],
            [
                'no'         => 'SC20110545/WHS/PPC-AAIJ',
                'kode'       => 'S2-41603-62120-M',
                'urutan'     => '',
                'namabarang' => 'BODY W/CYL D300',
                'jumlah'     => '1152',
                'satuan'     => '',
                'act_qty'    => '1104',
                'qxInbound'  => 'NOK',
                'keterangan' => 'error',
                'remark'     => '',
                'qx_effdate' => '',
            ],
        ]);
    }
}
