<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SideMenuSeeder extends Seeder
{
    public function run()
    {
        DB::table('sidemenu')->insert([
            [
                'name'      => '',
                'icon'      => '',
                'route'     => '',
                'role'      => ''
            ],
            [
                'name'      => 'Transaction',
                'icon'      => 'ti-wallet sidemenu-icon',
                'route'     => '#',
                'role'      => 'WHS,Supplier,QC'
            ],
            [
                'name'      => 'Delivery Slip',
                'icon'      => 'ti-truck sidemenu-icon',
                'route'     => '#',
                'role'      => 'WHS'
            ],
            [
                'name'      => 'Print Report',
                'icon'      => 'ti-files sidemenu-icon',
                'route'     => '#',
                'role'      => 'Supplier,WHS'
            ],
            [
                'name'      => 'View',
                'icon'      => 'ti-view-list-alt sidemenu-icon',
                'route'     => '#',
                'role'      => 'Supplier,WHS'
            ],
            [
                'name'      => 'Drawing',
                'icon'      => 'ti-palette sidemenu-icon',
                'route'     => '#',
                'role'      => ''
            ],
            [
                'name'      => 'Intrans',
                'icon'      => 'ti-reload sidemenu-icon',
                'route'     => '#',
                'role'      => 'WHS'
            ],
            [
                'name'      => 'Stock Alert',
                'icon'      => 'ti-alert sidemenu-icon',
                'route'     => '#',
                'role'      => ''
            ],
            [
                'name'      => 'Barcode WHS',
                'icon'      => 'ti-desktop sidemenu-icon',
                'route'     => '#',
                'role'      => ''
            ],
            [
                'name'      => 'Forwader',
                'icon'      => 'ti-back-right sidemenu-icon',
                'route'     => '#',
                'role'      => ''
            ],
            [
                'name'      => 'BON Warehouse',
                'icon'      => 'ti-notepad sidemenu-icon',
                'route'     => '#',
                'role'      => ''
            ],
            [
                'name'      => 'Stock PPC',
                'icon'      => 'ti-package sidemenu-icon',
                'route'     => '#',
                'role'      => ''
            ],
        ]);
    }
}
