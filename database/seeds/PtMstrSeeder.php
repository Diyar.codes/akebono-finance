<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PtMstrSeeder extends Seeder
{
    public function run()
    {
        DB::table('pub_pt_mstr')->insert([
            [
                'pt_desc1'  => 'BODY CALIPER',
                'pt_desc2'  => 'CASTING',
                'pt_um'     => 'PC',
                'pt_part'   => '51-A5418-57110',
                'pt_domain' => 'AAIJ',
                'pt_drwg_loc' => 'IMP',
            ],
            [
                'pt_desc1'  => 'MOUNTING SUPPORT',
                'pt_desc2'  => 'CASTING',
                'pt_um'     => 'PC',
                'pt_part'   => '51-A5418-58080',
                'pt_domain' => 'AAIJ',
                'pt_drwg_loc' => 'IMP',
            ],
            [
                'pt_desc1'  => 'BODY CALIPER',
                'pt_desc2'  => 'CASTING',
                'pt_um'     => 'PC',
                'pt_part'   => '51-A5424-57110',
                'pt_domain' => 'AAIJ',
                'pt_drwg_loc' => 'IMP',
            ],
            [
                'pt_desc1'  => 'MOUNTING SUPPORT',
                'pt_desc2'  => 'CASTING',
                'pt_um'     => 'PC',
                'pt_part'   => '51-A5424-58080',
                'pt_domain' => 'AAIJ',
                'pt_drwg_loc' => 'IMP',
            ],
            [
                'pt_desc1'  => 'MOUNTING SUPPORT',
                'pt_desc2'  => 'CASTING',
                'pt_um'     => 'PC',
                'pt_part'   => '51-A5429-58080',
                'pt_domain' => 'AAIJ',
                'pt_drwg_loc' => 'IMP',
            ],
        ]);
    }
}
