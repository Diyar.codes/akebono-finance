<?php

use Illuminate\Database\Seeder;

class po_trans_mstrSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('po_trans_mstr')->insert([
            [
                'po_nbr'      => 'PO123',
                'po_login'      => '1',
                'po_effdate'    => '1-1',
                'po_ip'    => '1-123',
                'po_user_remark'     => '1-123',
                'po_app0' => '1',
                'po_app0_status'   => '1',
                'po_app0_remark'  => '1',
                'po_app0_effdate'  => '1 1',
                'po_app1'       => '1',
                'po_app1_status'     => '1',
                'po_app1_remark'    => '1',
                'po_app1_effdate'     => '1',
                'po_app2' => '1',
                'po_app2_status'   => '1',
                'po_app2_remark'  => '1',
                'po_app2_effdate'  => '1',
                'po_vend'       => '1',
                'po_email_status' => '1',
                'po_revisi'   => '2',
                'po_revisi_status'  => '1',
                'po_qxtend_date'  => '1',
                'po_qxtend_status'       => '1',
                'po_proses'     => '1',
                'po_qxtend_keterangan'    => '1',
                'po_it_received'     => '1',
                'po_dokumen_file' => '1'
            ],
        ]);
    }
}
