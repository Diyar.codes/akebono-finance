<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PubLoginSeeder extends Seeder
{
    public function run()
    {
        DB::table('pub_login')->insert([
            // [
            //     'username'      => 'majapahit',
            //     'password'      => bcrypt('password'),
            //     'previllage'    => 'Supplier',
            //     'status_log'    => 'Passw0rD',
            //     'tgl_akses'     => '1900-01-01 00:00:00.000',
            //     'kode_supplier' => '',
            //     'SUPP_FEXQMS'   => '',
            //     'Username_FEX'  => '',
            //     'Password_FEX'  => '',
            //     'effdate'       => '',
            //     'email_ppc'     => ''
            // ],
            [
                'username'      => 'whs',
                'password'      => bcrypt('password'),
                'previllage'    => 'WHS',
                'status_log'    => 'Passw0rD',
                'tgl_akses'     => '1900-01-01 00:00:00.000',
                'kode_supplier' => '',
                'SUPP_FEXQMS'   => '',
                'Username_FEX'  => '',
                'Password_FEX'  => '',
                'effdate'       => '2020-08-04 08:34:35.190',
                'email_ppc'     => ''
            ],
            [
                'username'      => 'supplier',
                'password'      => bcrypt('password'),
                'previllage'    => 'Supplier',
                'status_log'    => 'Passw0rD',
                'tgl_akses'     => '1900-01-01 00:00:00.000',
                'kode_supplier' => '',
                'SUPP_FEXQMS'   => '',
                'Username_FEX'  => '',
                'Password_FEX'  => '',
                'effdate'       => '2020-08-04 08:34:35.190',
                'email_ppc'     => ''
            ],
            [
                'username'      => 'qc',
                'password'      => bcrypt('password'),
                'previllage'    => 'QC',
                'status_log'    => 'Passw0rD',
                'tgl_akses'     => '1900-01-01 00:00:00.000',
                'kode_supplier' => '',
                'SUPP_FEXQMS'   => '',
                'Username_FEX'  => '',
                'Password_FEX'  => '',
                'effdate'       => '2020-08-04 08:34:35.190',
                'email_ppc'     => ''
            ],
        ]);
    }
}
