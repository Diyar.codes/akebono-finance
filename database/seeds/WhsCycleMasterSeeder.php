<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class WhsCycleMasterSeeder extends Seeder
{
    public function run()
    {
        DB::table('whs_cycle_master')->insert([
            [
                'cycle_kode_supplier' => 'SP0045',
                'cycle_nama_supplier' => 'PUTRA BHAKTI MULIA, PT',
                'cycle_start'         => '04:00:01',
                'cycle_end'           => '07:30:00',
                'cycle_cycle'         => '1',
                'cycle_batas_a'       => '1',
                'cycle_batas_b'       => '10',
                'cycle_batas_c'       => '5',
            ],
            [
                'cycle_kode_supplier' => 'SP0045',
                'cycle_nama_supplier' => 'PUTRA BHAKTI MULIA, PT',
                'cycle_start'         => '07:30:01',
                'cycle_end'           => '10:00:00',
                'cycle_cycle'         => '2',
                'cycle_batas_a'       => '1',
                'cycle_batas_b'       => '10',
                'cycle_batas_c'       => '5',
            ],
            [
                'cycle_kode_supplier' => 'SP0045',
                'cycle_nama_supplier' => 'PUTRA BHAKTI MULIA, PT',
                'cycle_start'         => '10:00:01',
                'cycle_end'           => '12:00:00',
                'cycle_cycle'         => '3',
                'cycle_batas_a'       => '1',
                'cycle_batas_b'       => '10',
                'cycle_batas_c'       => '5',
            ],
            [
                'cycle_kode_supplier' => 'SP0045',
                'cycle_nama_supplier' => 'PUTRA BHAKTI MULIA, PT',
                'cycle_start'         => '12:00:01',
                'cycle_end'           => '14:00:00',
                'cycle_cycle'         => '4',
                'cycle_batas_a'       => '1',
                'cycle_batas_b'       => '10',
                'cycle_batas_c'       => '5',
            ],
            [
                'cycle_kode_supplier' => 'SP0045',
                'cycle_nama_supplier' => 'PUTRA BHAKTI MULIA, PT',
                'cycle_start'         => '14:00:01',
                'cycle_end'           => '16:00:00',
                'cycle_cycle'         => '5',
                'cycle_batas_a'       => '1',
                'cycle_batas_b'       => '10',
                'cycle_batas_c'       => '5',
            ],
        ]);
    }
}
