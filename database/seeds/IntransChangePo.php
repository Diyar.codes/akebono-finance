<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IntransChangePo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('po_change_type')->insert([
            [
                'no_po' => '1111',
                'line_po' => '1000',
                'type_po' => '',
                'approval_po' => '',
                'approval_email' => 'josua@mail.com',
                'approval_status' => 'OK',
                'approval_date' => '',
                'po_deskripsi' => '',
                'effdate' => '2021-03-26',
                'type_po_rev' => '',
                'po_item' => '',
                'transaksi_id' => '026/AA1/03/2021',
                'approval_remark' => '',
                'no_pp' => '',
                'line_pp' => '',
                'type_pp' => '',
                'type_pp_revisi' => '',
            ],
            [
                'no_po' => '2222',
                'line_po' => '2000',
                'type_po' => '',
                'approval_po' => '',
                'approval_email' => 'oke@mail.com',
                'approval_status' => 'NOK',
                'approval_date' => '',
                'po_deskripsi' => '',
                'effdate' => '2021-03-27',
                'type_po_rev' => '',
                'po_item' => '',
                'transaksi_id' => '027/AA1/03/2021',
                'approval_remark' => '',
                'no_pp' => '',
                'line_pp' => '',
                'type_pp' => '',
                'type_pp_revisi' => '',
            ],
            [
                'no_po' => '3333',
                'line_po' => '3000',
                'type_po' => '',
                'approval_po' => '',
                'approval_email' => 'dolan@mail.com',
                'approval_status' => '',
                'approval_date' => '',
                'po_deskripsi' => '',
                'effdate' => '2021-04-26',
                'type_po_rev' => '',
                'po_item' => '',
                'transaksi_id' => '026/AA1/04/2021',
                'approval_remark' => '',
                'no_pp' => '',
                'line_pp' => '',
                'type_pp' => '',
                'type_pp_revisi' => '',
            ]
        ]);
    }
}
