<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class BusinesRelationSeeder extends Seeder
{
    public function run()
    {
        DB::table('pub_business_relation')->insert([
            [
                'businessRelationName1' => 'AAPICO FORGING PUBLIC CC',
                'BusinessRelationCode'  => 'SA0001U',
            ],
            [
                'businessRelationName1' => 'AB DIAN HACHI ,PT',
                'BusinessRelationCode'  => 'SA0094I',
            ],
            [
                'businessRelationName1' => 'ABADI CAKRA JATI, CV',
                'BusinessRelationCode'  => 'SA0002I',
            ],
            [
                'businessRelationName1' => 'ACCOUNTING',
                'BusinessRelationCode'  => 'SA0082I',
            ],
            [
                'businessRelationName1' => 'AAPICO FORGING PUBLIC C',
                'BusinessRelationCode'  => 'SDI001I',
            ],
            [
                'businessRelationName1' => 'AT INDONESIA, PT',
                'BusinessRelationCode'  => 'SA00461',
            ],
        ]);
    }
}
