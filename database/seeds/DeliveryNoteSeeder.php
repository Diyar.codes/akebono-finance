<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DeliveryNoteSeeder extends Seeder
{
    public function run()
    {
        DB::table('pub_dn_master')->insert([
            [
                'dn_tr_id' => 'SG0041I-01074A20-20200203-1',
                'dn_po_nbr' => '01074A20',
                'dn_vend' => 'SG0041I',
                'dn_user_id' => 'latif',
                'dn_arrival_date' => '2020-02-03',
                'dn_order_date' => '2020-02-02'
            ],
            [
                'dn_tr_id' => 'SA0025I-01214A20-20200203-1',
                'dn_po_nbr' => '01214A20',
                'dn_vend' => 'SA0025I',
                'dn_user_id' => 'whs',
                'dn_arrival_date' => '2020-02-03',
                'dn_order_date' => '2020-02-02'
            ],
            [
                'dn_tr_id' => 'SA0037I-01191A20-20200203-1',
                'dn_po_nbr' => '01191A20',
                'dn_vend' => 'SA0037I',
                'dn_user_id' => 'yudhi',
                'dn_arrival_date' => '2020-02-03',
                'dn_order_date' => '2020-02-02'
            ],
            [
                'dn_tr_id' => 'SA0039I-01240A20-20200203-1',
                'dn_po_nbr' => '01240A20',
                'dn_vend' => 'SA0039I',
                'dn_user_id' => 'yadi',
                'dn_arrival_date' => '2020-02-03',
                'dn_order_date' => '2020-02-02'
            ],
            [
                'dn_tr_id' => 'SA0046I-01064A20-20200203-2',
                'dn_po_nbr' => '01064A20',
                'dn_vend' => 'SA0046I',
                'dn_user_id' => 'yadi',
                'dn_arrival_date' => '2020-02-03',
                'dn_order_date' => '2020-02-02'
            ],
        ]);
    }
}
