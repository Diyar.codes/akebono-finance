<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        /// $this->call(ptmaster::class);;
        $this->call(PubLoginSeeder::class);
        // $this->call(ItemSuppSeeder::class);
        $this->call(PtMstrSeeder::class);
        $this->call(BusinesRelationSeeder::class);
        // $this->call(WhsCycleMasterSeeder::class);
        // $this->call(PubDnMstrSeeder::class);
        // $this->call(PubDndDet::class);
        $this->call(SideMenuSeeder::class);
        $this->call(SubMenuSeeder::class);
        // $this->call(SupplierMSTRSuratJalanSeeder::class);
        // $this->call(SupplierDTLSuratJalanSeeder::class);
        // $this->call(supplierItemMaster::class);
        // $this->call(elpb_dtlSeeder::class);
        // $this->call(po_trans_mstrSeeder::class);
        $this->call(pod_detSeeder::class);
        $this->call(portald_detSeeder::class);
        // $this->call(IntransSeeder::class);
        // $this->call(IntransChangePo::class);
        $url = "https://supplier.akebono-astra.co.id/wsatdw/master_view_test.php";
        get_supplier($url);
        // $url1 = "https://supplier.akebono-astra.co.id/wsatdw/po_mstr.php";
        // getDataPO($url1);
    }
}
