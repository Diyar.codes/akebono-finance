<?php

use Illuminate\Database\Seeder;

class PubDndDet extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dnd_det')->insert([
        
            [
                'dnd_tr_id' => 'SG0041I-01074A20-20200203-1',
                'dnd_line' => '2',
                'dnd_part' => '51-A5418-57110',
                'dnd_qty_order' => '100',
                'dnd_qty_ship' => '100',
                'dnd_qty_confirm' => '100',
                'dnd_um' => 'PC',
                'dnd_accum_confirm' => '1',
                'dnd_char01' => '',
                'dnd_dec01' => '',
                'dnd_dec02' => '',
                'dnd_date01' => '',
                'dnd_date2' => '',
                'dnd_log01' => '',
                'dnd_log02' => '',
                'dnd_po_nbr' => '01074A20',
                'dnd_confirm_date' => '',
                'dnd_dlv_date' => '',
                'dnd_user_id' => '',
                'dnd_ip' => '',
                'dn_cycle_arrival' => '1' ,
                'dn_cycle_order' => '1',
                'dnd_char02' => '',
            ],
            [
                'dnd_tr_id' => 'SG0041I-01074A20-20200203-1',
                'dnd_line' => '3',
                'dnd_part' => '51-A5418-58080',
                'dnd_qty_order' => '200',
                'dnd_qty_ship' => '200',
                'dnd_qty_confirm' => '200',
                'dnd_um' => 'PC',
                'dnd_accum_confirm' => '01',
                'dnd_char01' => '',
                'dnd_dec01' => '',
                'dnd_dec02' => '',
                'dnd_date01' => '',
                'dnd_date2' => '',
                'dnd_log01' => '',
                'dnd_log02' => '',
                'dnd_po_nbr' => '01074A20',
                'dnd_confirm_date' => '',
                'dnd_dlv_date' => '',
                'dnd_user_id' => '',
                'dnd_ip' => '',
                'dn_cycle_arrival' => '1',
                'dn_cycle_order' => '1' ,
                'dnd_char02' => '',
            ],
        ]);
    }
}
