<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IntransSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  int_confirm
        DB::table('intrans_trans')->insert([
            [
                'int_supp' => 'SP0046',
                'int_inv' => '10',
                'int_etd' => '2021-03-25',  #date
                'int_ship' => '1000',
                'int_po' => 'as',
                'int_line' => 1,
                'int_part' => 'as',
                'int_qty_rcp' => 1,
                'int_qxtend' => '', #date
                'int_qxtend_stat' => '',
                'int_cek' => '',
                'int_rmk' => '',
                'int_confirm' => '2021-03-25',
                'int_lot' => 'as',
                'int_effdate' => null, #date
                'int_input' => '',
                'int_no_bl' => '',
                'int_send_date' => null, #date
                'int_prc_send_ori' => null, #date
                'int_prc_pic' => '',
                'int_prc_receive_copy' => null,
                'int_prc_receive_ori' => null,
                'int_prc_not_receive' => null,
                'int_lpc' => '',
                'int_countainer_packaging' => '',
                'int_rencana_kedatangan' => null,
                'int_create_countainer' => '',
                'int_shift' => '',
                'int_penerima_barang' => '',
                'int_tanggal_actual' => null, #date
                'int_fcl_lcl' => '',
                'int_date_confirm' => null, #datetime
                'int_name_confirm' => '',
                'int_lokasi' => '',
                'int_fwd' => '',
                'int_fwd_tagihan' => '',
                'int_fwd_tanggal' => null,
                'int_fwd_amount' => '',
                'int_ppjk' => '',
                'int_ppjk_tagihan' => '',
                'int_ppjk_tanggal' => null,
                'int_ppjk_amount' => '',
                'int_lot_case' => '',
                'int_inv_master' => '',
                'int_COO' => '',
                'int_tgl_bl' => null, #date
                'int_confirm_whs_status' => '',
                'int_confirm_whs_ket' => '',
                'int_confirm_whs_date' => '', #datetime
                'int_lokasi_to' => '',
                'int_cek_whs' => '',
                'int_part_deskripsi' => '',
                'int_part_um' => '',
                'int_part_type' => '',
                'int_qty_actual' => 1,
                'int_status_retur' => '',
                'int_inventory_ket' => '',
                'int_date_retur' => null, #datetime
                'int_ket_retur' => ''
            ],
            [
                'int_supp' => 'SA0001',
                'int_inv' => '40',
                'int_etd' => '2021-03-25',  #date
                'int_ship' => '4000',
                'int_po' => 'as',
                'int_line' => 1,
                'int_part' => 'as',
                'int_qty_rcp' => 1,
                'int_qxtend' => '', #date
                'int_qxtend_stat' => '',
                'int_cek' => '',
                'int_rmk' => '',
                'int_confirm' => '2021-03-25',
                'int_lot' => 'as',
                'int_effdate' => null, #date
                'int_input' => '',
                'int_no_bl' => '',
                'int_send_date' => null, #date
                'int_prc_send_ori' => null, #date
                'int_prc_pic' => '',
                'int_prc_receive_copy' => null,
                'int_prc_receive_ori' => null,
                'int_prc_not_receive' => null,
                'int_lpc' => '',
                'int_countainer_packaging' => '',
                'int_rencana_kedatangan' => null,
                'int_create_countainer' => '',
                'int_shift' => '',
                'int_penerima_barang' => '',
                'int_tanggal_actual' => null, #date
                'int_fcl_lcl' => '',
                'int_date_confirm' => null, #datetime
                'int_name_confirm' => '',
                'int_lokasi' => '',
                'int_fwd' => '',
                'int_fwd_tagihan' => '',
                'int_fwd_tanggal' => null,
                'int_fwd_amount' => '',
                'int_ppjk' => '',
                'int_ppjk_tagihan' => '',
                'int_ppjk_tanggal' => null,
                'int_ppjk_amount' => '',
                'int_lot_case' => '',
                'int_inv_master' => '',
                'int_COO' => '',
                'int_tgl_bl' => null, #date
                'int_confirm_whs_status' => '',
                'int_confirm_whs_ket' => '',
                'int_confirm_whs_date' => '', #datetime
                'int_lokasi_to' => '',
                'int_cek_whs' => '',
                'int_part_deskripsi' => '',
                'int_part_um' => '',
                'int_part_type' => '',
                'int_qty_actual' => 1,
                'int_status_retur' => '',
                'int_inventory_ket' => '',
                'int_date_retur' => null, #datetime
                'int_ket_retur' => ''
            ],
            [
                'int_supp' => 'SP0041',
                'int_inv' => '20',
                'int_etd' => '2021-04-25',  #date
                'int_ship' => '2000',
                'int_po' => 'as',
                'int_line' => 1,
                'int_part' => 'as',
                'int_qty_rcp' => 1,
                'int_qxtend' => '', #date
                'int_qxtend_stat' => '',
                'int_cek' => '',
                'int_rmk' => '',
                'int_confirm' => '2021-03-25',
                'int_lot' => 'as',
                'int_effdate' => null, #date
                'int_input' => '',
                'int_no_bl' => '',
                'int_send_date' => null, #date
                'int_prc_send_ori' => null, #date
                'int_prc_pic' => '',
                'int_prc_receive_copy' => null,
                'int_prc_receive_ori' => null,
                'int_prc_not_receive' => null,
                'int_lpc' => '',
                'int_countainer_packaging' => '',
                'int_rencana_kedatangan' => null,
                'int_create_countainer' => '',
                'int_shift' => '',
                'int_penerima_barang' => '',
                'int_tanggal_actual' => null, #date
                'int_fcl_lcl' => '',
                'int_date_confirm' => null, #datetime
                'int_name_confirm' => '',
                'int_lokasi' => '',
                'int_fwd' => '',
                'int_fwd_tagihan' => '',
                'int_fwd_tanggal' => null,
                'int_fwd_amount' => '',
                'int_ppjk' => '',
                'int_ppjk_tagihan' => '',
                'int_ppjk_tanggal' => null,
                'int_ppjk_amount' => '',
                'int_lot_case' => '',
                'int_inv_master' => '',
                'int_COO' => '',
                'int_tgl_bl' => null, #date
                'int_confirm_whs_status' => '',
                'int_confirm_whs_ket' => '',
                'int_confirm_whs_date' => '', #datetime
                'int_lokasi_to' => '',
                'int_cek_whs' => '',
                'int_part_deskripsi' => '',
                'int_part_um' => '',
                'int_part_type' => '',
                'int_qty_actual' => 1,
                'int_status_retur' => '',
                'int_inventory_ket' => '',
                'int_date_retur' => null, #datetime
                'int_ket_retur' => ''
            ]
        ]);
    }
}
