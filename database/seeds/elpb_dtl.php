<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class elpb_dtl extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('elpb_dtl')->insert([
            [
                'lpb_code'      => '123',
                'trans_id'      => '1',
                'invoice'    => 'INV-123',
                'po'    => 'PO-123',
                'pp'     => 'PP-123',
                'pp_desc' => 'ppdesc',
                'packing_slip'   => '12',
                'item_number'  => '123-ITEM-123',
                'item_desc'  => 'ITEM DDESC',
                'loc'       => '1',
                'loc_cst'     => '1',
                'type'    => '1',
                'qty_po'     => '100',
                'qty_shipping' => '100',
                'qty_open_po'   => '100',
                'qty_received'  => '100',
                'qty_received_p1'  => '100',
                'qty_received_p2'       => '100',
                'qty_received_p3' => '100',
                'qty_received_p4'   => '100',
                'qty_received_p5'  => '100',
                'qty_received_p6'  => '100',
                'qty_received_p7'       => '100',
                'qty_received_p8'     => '100',
                'qty_received_p9'    => '100',
                'qty_received_pno'     => '100',
                'um' => 'PC',
                'memo'   => 'A',
                'pm_code'  => 'A',
                'rc'  => 'A',
                'status_rc_qxtend'       => '100',
                'bf_qxtend'     => '100',
                'status_bf_qxtend'    => '100',
                'status_item'     => '100',
                'outstanding_status' => '100',
                'outstanding_start'   => '100',
                'created_by'  => '100',
                'last_modified_by'  => '2020-08-04 08:34:35.190',
                'last_modified_date'       => '2020-08-04 08:34:35.190',
                'row_status'     => '100',
                'remarks'       => '2020-08-04 08:34:35.190'
            ],
        ]);
    }
}
