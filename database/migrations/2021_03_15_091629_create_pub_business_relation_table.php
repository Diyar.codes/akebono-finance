<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePubBusinessRelationTable extends Migration
{
    public function up()
    {
        Schema::create('pub_business_relation', function (Blueprint $table) {
            $table->id();
            $table->text('businessRelationName1')->nullable();
            $table->text('BusinessRelationCode')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pub_business_relation');
    }
}
