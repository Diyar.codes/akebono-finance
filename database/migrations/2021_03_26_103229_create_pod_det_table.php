<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePodDetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pod_det', function (Blueprint $table) {
            $table->id();
            $table->string('pod_part',50)->nullable();
            $table->string('pod_nbr',50)->nullable();
            $table->string('pod_line',50)->nullable();
            $table->string('pod_qty_ord',50)->nullable();
            $table->string('pod_qty_rcvd',50)->nullable();
            $table->string('pod_req_nbr',55)->nullable();
            $table->string('pod_desc',50)->nullable();
            $table->string('pod_qty_rtnd',50)->nullable();
            $table->string('pt_drwg_loc',55)->nullable();
            $table->string('pod_um',55)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pod_det');
    }
}
