<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoapPoDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SOAP_po_detail', function (Blueprint $table) {
            $table->id();
            $table->string('item_number',50)->nullable();
            $table->string('no_po',55)->nullable();
            $table->string('qty_po',50)->nullable();
            $table->string('qty_receive',50)->nullable();
            $table->string('no_pp',50)->nullable();
            $table->string('item_deskripsi',155)->nullable();
            $table->string('item_price',50)->nullable();
            $table->string('item_disc',50)->nullable();
            $table->string('po_status',50)->nullable();
            $table->string('po_um',55)->nullable();
            $table->string('podRequest',50)->nullable();
            $table->string('podLoc',55)->nullable();
            $table->string('line',55)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SOAP_po_detail');
    }
}
