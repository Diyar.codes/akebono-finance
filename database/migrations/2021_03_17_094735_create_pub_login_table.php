<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePubLoginTable extends Migration
{
    public function up()
    {
        Schema::create('pub_login', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->string('password');
            $table->string('previllage');
            $table->string('status_log');
            $table->datetime('tgl_akses');
            $table->string('kode_supplier')->nullable();
            $table->string('SUPP_FEXQMS')->nullable();
            $table->string('USERNAME_FEX')->nullable();
            $table->string('PASSWORD_FEX')->nullable();
            $table->datetime('effdate');
            $table->string('email_ppc')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pub_login');
    }
}
