<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoapPtMstrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SOAP_pt_mstr', function (Blueprint $table) {
            $table->id();
            $table->string('item_number',50)->nullable();
            $table->string('deskripsi1',155)->nullable();
            $table->string('deskripsi2',155)->nullable();
            $table->string('um',50)->nullable();
            $table->string('buyer_planner',50)->nullable();
            $table->string('desc_type',55)->nullable();
            $table->string('nama_supplier',50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SOAP_pt_mstr');
    }
}
