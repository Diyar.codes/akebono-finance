<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoapPrhHistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SOAP_prh_hist', function (Blueprint $table) {
            $table->id();
            $table->string('prhLine')->nullable()->default('-');
            $table->string('prhNbr')->nullable()->default('-');
            $table->string('prhVend')->nullable()->default('-');
            $table->string('prhPart')->nullable()->default('-');
            $table->string('prhReceiver')->nullable()->default('-');
            $table->string('prhPsNbr')->nullable()->default('-');
            $table->string('prhRcvd')->nullable()->default('-');
            $table->string('prhRcp_date')->nullable()->default('-');
            $table->string('prhReason')->nullable()->default('-');
            $table->string('prhDomain')->nullable()->default('-');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SOAP_prh_hist');
    }
}
