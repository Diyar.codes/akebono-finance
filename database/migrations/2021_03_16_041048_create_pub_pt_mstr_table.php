<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePubPtMstrTable extends Migration
{
    public function up()
    {
        Schema::create('pub_pt_mstr', function (Blueprint $table) {
            $table->id();
            $table->text('pt_desc1')->nullable();
            $table->text('pt_desc2')->nullable();
            $table->text('pt_um')->nullable();
            $table->text('pt_part')->nullable();
            $table->text('pt_domain')->nullable();
            $table->text('pt_drwg_loc')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pub_pt_mstr');
    }
}
