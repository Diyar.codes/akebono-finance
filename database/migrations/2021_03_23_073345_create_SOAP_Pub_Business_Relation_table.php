<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSOAPPubBusinessRelationTable extends Migration
{
    public function up()
    {
        Schema::create('SOAP_Pub_Business_Relation', function (Blueprint $table) {
            $table->id();
            $table->string('ct_vd_addr');
            $table->string('ct_vd_type')->nullable()->default('-');
            $table->string('ct_ad_name')->nullable()->default('-');
            $table->string('ct_ad_line1')->nullable()->default('-');
            $table->string('ct_ad_line2')->nullable()->default('-');
            $table->string('ct_ad_city')->nullable()->desdault('-');
        });
    }

    public function down()
    {
        Schema::dropIfExists('SOAP_Pub_Business_Relation');
    }
}
