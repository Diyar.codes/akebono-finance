<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSOAPPoMstrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SOAP_po_mstr', function (Blueprint $table) {
            $table->id();
            $table->string('po_number',50)->nullable();
            $table->date('POdue_date')->nullable();
            $table->string('kode_supplier',50)->nullable();
            $table->string('po_status',50)->nullable();
            $table->string('nama_supplier',155)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SOAP_po_mstr');
    }
}
